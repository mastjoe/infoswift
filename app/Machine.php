<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Machine extends Model
{
    //'
    protected $fillable = [
        'client_id',
        'branch_status',
        'branch_id',
        'region_id',
        'name',
        'terminal_id',
        'model_number',
        'serial_number',
        'machine_type_id',
        'machine_status_id',
        'machine_vendor_id',
        'ip_address',
        'is_pm',
        'is_asd',
        'location',
        'engineer_id'
    ];

    public function client()
    {
        return $this->belongsTo('App\Client');
    }

    public function branch()
    {
        return $this->belongsTo('App\Branch');
    }

    public function region()
    {
        return $this->belongsTo('App\Region');
    }

    public function state()
    {
        return $this->region->state();
    }

    public function country()
    {
        return $this->state->country();
    }

    public function machineType()
    {
        return $this->belongsTo('App\MachineType');
    }

    public function machineStatus()
    {
        return $this->belongsTo('App\MachineStatus');
    }

    public function machineVendor()
    {
        return $this->belongsTo('App\MachineVendor');
    }

    public function tickets()
    {
        return $this->hasMany('App\Ticket')->orderBy('id', 'desc');
    }

    public function engineer()
    {
        return $this->belongsTo('App\User', 'engineer_id');
    }

    public function deletable()
    {
        if (
            $this->tickets()->count() ||
            $this->pms()->count()     
        ) {
            return false;
        }
        return true;
    }

    public function audit_events()
    {
        return $this->morphMany('App\AuditEvent','auditable');
    }

    public function pms()
    {
        return $this->hasMany('App\Pm');
    }

    /**
     * get the pms of a machine for specific year
     * @param int $year
     */
    public function year_pms($year)
    {
        return $this->hasMany('App\Pm')->where('year', $year);
    }

    /**
     * get machine unresolved ticket
     */
    public function unresolvedTickets()
    {
        return $this->tickets->filter(function($ticket) {
            if (!$ticket->closed_at) {
                return $ticket;
            }
        });
    }

    /**
     * checks if terminal has unresolved tickets
     */
    public function hasUnresolvedTickets() {
        return $this->unresolvedTickets()->count();
    }

}
