<?php

namespace App\Listeners;

use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;
use App\Notifications\AdminCloseTicketNotification;

class NotifyAdminCloseTicketListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        //
        $ticket = $event->ticket;
        
        // notify admins with privilege to confirm ticket
        $admins = User::usersWithPrivileges('assess ticket closure');
        foreach ($admins as $admin) {
            $admin->notify(new AdminCloseTicketNotification($ticket));
        }

        // Notification::send(User::usersWithPrivileges('assess ticket closure'), new AdminCloseTicketNotification($ticket));
    }
}
