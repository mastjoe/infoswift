<?php

namespace App\Listeners;

use App\Notifications\EngineerDeclinePartOrderNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class NotifyEngineerDeclinePartsOrderListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        //
        $order = $event->order;
        $order->user->notify(new EngineerDeclinePartOrderNotification($order));
    }
}
