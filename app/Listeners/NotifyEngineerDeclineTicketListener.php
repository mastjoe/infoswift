<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notifications\EngineerDeclineTicketNotification;

class NotifyEngineerDeclineTicketListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        //
        $ticket = $event->ticket;
        if ($ticket->closed_by) {
            try {                
                $ticket->closer->notify(new EngineerDeclineTicketNotification($ticket));
            } catch (\Throwable $th) {}
        }
    }
}
