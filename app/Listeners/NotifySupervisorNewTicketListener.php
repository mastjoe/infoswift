<?php

namespace App\Listeners;

use App\Notifications\SupervisorNewTicketNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class NotifySupervisorNewTicketListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        //
        // send email and db notification to supervisor
        $ticket = $event->ticket;
        if ($ticket->machine->supervisor) {
            $ticket->client->supervisor->notify(new SupervisorNewTicketNotification($ticket));
        }
    }
}
