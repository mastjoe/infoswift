<?php

namespace App\Listeners;

use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;
use App\Notifications\AdminNewMachineOrderNotification;

class NotifyAdminNewMachinePartOrder
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        //
        $order = $event->order;
        // notify all admin with privilege to manage machine part order
        $order_managers = User::usersWithPrivileges('manage machine part orders');
        // send notification
        Notification::send($order_managers, new AdminNewMachineOrderNotification($order));
    }
}
