<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notifications\EngineerNewTicketNotification;

class NotifyEngineerNewTicketListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        //
        // notifies engineer of new ticket
        // send sms
        // send email
        if ($event->ticket->machine->engineer) {
            $event->ticket->machine->engineer->notify(new EngineerNewTicketNotification($event->ticket));
        }
    }
}
