<?php

namespace App\Listeners;

use App\Notifications\EngineerConfirmedClosedTicketNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class NotifyEngineerConfirmedCloseTicketListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        //
        $ticket = $event->ticket;
        // send notification to ticket closer
        try {            
            $ticket->closer->notify(new EngineerConfirmedClosedTicketNotification($ticket));
        } catch (\Throwable $th) {}
    }
}
