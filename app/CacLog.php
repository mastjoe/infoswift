<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CacLog extends Model
{
    //
    protected $guarded = [];

    public function client()
    {
        return $this->belongsTo('App\Client');
    }

    public function orderItem()
    {
        return $this->belongsTo('App\MachineOrderPartItem', 'order_item_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
