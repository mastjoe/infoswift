<?php

namespace App;

use App\Helpers\Util;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class PrivateMessage extends Model
{
    //
    protected $guarded = [];

    public static function boot()
    {
        parent::boot();

        static::creating(function($model) {
            $model->ref = Util::genPrivateMessageRef();
            $model->sender_id = Auth::user()->id;
        });
    }

    public function sender()
    {
        return $this->belongsTo('App\User', 'sender_id');
    }

    public function receiver()
    {
        return $this->belongsTo('App\User', 'receiver_id');
    }

    public function responses()
    {
        return $this->hasMany('App\PrivateMessage', 'response_to');
    }
}
