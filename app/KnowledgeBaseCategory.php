<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KnowledgeBaseCategory extends Model
{
    //
    protected $fillable = [
        'category',
        'slug',
        'description'
    ];

    public function posts()
    {
        return $this->hasMany('App\KnowledgeBase', 'category_id');
    }

    public function audit_events()
    {
        return $this->morphMany('App\AuditEvent', 'auditable');
    }

    public function deletable()
    {
        if ($this->posts->count() > 0) return false;
        return true;
    }
}
