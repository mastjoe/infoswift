<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    //
    protected $fillable = [
        'user_id',
        'likable_type',
        'likable_id',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function likable()
    {
        return $this->morphTo();
    }

    public function target()
    {
        return $this->likable_type::find($this->likable_id);
    }
}
