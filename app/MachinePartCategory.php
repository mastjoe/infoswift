<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MachinePartCategory extends Model
{
    //
    protected $fillable = [
        'category',
        'description'
    ];

    public function machine_parts()
    {
        return $this->hasMany('App\MachinePart', 'category_id');
    }

    public function deletable()
    {
        $cannot_delete = ['consumables', 'non-consumables'];

        if ($this->machine_parts->count() || in_array($this->category, $cannot_delete)) {
            return false;
        }
        return true;
    }
}
