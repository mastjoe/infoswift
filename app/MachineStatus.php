<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MachineStatus extends Model
{
    //
    protected $fillable = [
        'status',
        'description'
    ];

    public function machines()
    {
        return $this->hasMany('App\Machine', 'machine_status_id');
    }

    public function setStatusAttribute($value)
    {
        $this->attributes['status'] = strtolower($value);
    }

    public function deletable()
    {
        return true;
    }
}
