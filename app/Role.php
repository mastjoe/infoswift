<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //
    protected $fillable = [
        'role',
        'description',
        'cannot_delete'
    ];

    public function privileges()
    {
        return $this->belongsToMany('App\Privilege', 'role_privileges');
    }

    public function getRoleAttribute($value)
    {
        return ucwords(strtolower($value));
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'user_roles');
    }

    public function deletable()
    {
        $undeletable_roles = ['super admin', 'engineer', 'supervisor'];

        if ($this->users->count() || $this->cannot_delete || in_array(strtolower($this->role), $undeletable_roles)) {
            return false;
        }
        return true;
    }
}
