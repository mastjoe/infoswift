<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    //
    protected $fillable = [
        'state_id',
        'country_id',
        'region',
        'client_id'
    ];

    public function client()
    {
        return $this->belongsTo('App\Client');
    }

    public function audit_events()
    {
        return $this->morphMany('App\AuditEvent', 'auditable');
    }

    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    public function state()
    {
        return $this->belongsTo('App\CountryState');
    }

    public function branches()
    {
        return $this->hasMany('App\Branch', 'region_id');
    }

    public function machines()
    {
        return $this->hasMany('App\Machine');
    }

    public function deletable()
    {
        if ($this->branches->count()) {
            return false;
        }
        return true;
    }
}
