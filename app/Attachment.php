<?php

namespace App;

use App\Helpers\Util;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    //
    protected $fillable = [
        'attachable_id',
        'attachable_type',
        'name',
        'size',
        'url',
        'type',
        'uploaded_by',
        'ref'
    ];

    public function attachable()
    {
        return $this->morphTo();
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function($model) {
            $model->uploaded_by = Auth::user()->id;
            $model->ref = Util::genAttachmentRef();
        });
    }

    public function uploadedBy()
    {
        return $this->belongsTo('App\User', 'uploaded_by');
    }

    public function link()
    {
        return \asset($this->url);
    }
}
