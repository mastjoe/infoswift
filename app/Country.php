<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    //
    protected $fillable = [
        'country',
        'continent'
    ];

    public function setCountryAttribute($value)
    {
        $this->attributes['country'] = ucwords(strtolower($value));
    }

    public function getContinentAttribute($value)
    {
        return ucwords(strtolower($value));
    }

    public function states()
    {
        return $this->hasMany('App\CountryState');
    }

    public function regions()
    {
        return $this->hasMany('App\Region');
    }

    public function branches()
    {
        return $this->hasMany('App\Branch');
    }
    
    public function deletable()
    {
        if ($this->states->count()) {
            return false;
        }
        return true;
    }
}
