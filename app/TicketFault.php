<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketFault extends Model
{
    //
    protected $fillable = [
        'ticket_id',
        'fault_id',
        'solution',
        'solved_at',
        'solved_by',
    ];

    public function ticket()
    {
        return $this->belongsTo('App\Ticket');
    }

    public function fault()
    {
        return $this->belongsTo('App\MachineFault', 'fault_id');
    }

    public function solver()
    {
        return $this->belongsTo('App\User', 'solved_by');
    }
}
