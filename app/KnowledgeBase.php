<?php

namespace App;

use Str;
use App\Helpers\Util;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class KnowledgeBase extends Model
{
    //
    protected $fillable = [
        'category_id',
        'user_id',
        'title',
        'content',
        'ref',
        'read_count',
        'commentable',
        'reactable',
        'hidden',
        'featured_image',
    ];


    public static function boot()
    {
        parent::boot();

        static::creating(function($model) {
            $model->user_id = Auth::user()->id;
            $model->ref = Util::genKbRef();
        });
    }

    public function category()
    {
        return $this->belongsTo('App\KnowledgeBaseCategory', 'category_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function images()
    {
        return $this->morphMany('App\Image', 'imageable');
    }

    public function likes()
    {
        return $this->morphMany('App\Like', 'likable');
    }

    public function dislikes()
    {
        return $this->morphMany('App\Dislike', 'dislikable');
    }

    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }

    public function featured_image_link()
    {
        return \asset($this->featured_image);
    }

    public function content_intro()
    {
        return Str::limit(strip_tags($this->content), 100);
    }

    public function deletable()
    {
        if ($this->comments->count()) {
            return false;
        }
        return true;
    }

    public function audit_events()
    {
        return $this->morphMany('App\AuditEvent', 'auditable');
    }
    

}
