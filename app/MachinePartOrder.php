<?php

namespace App;

use App\Helpers\Util;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class MachinePartOrder extends Model
{
    //
    protected $fillable = [
        'user_id',
        'ticket_id',
        'comment',
        'ref',
        'status',
        'declination_remark',
        'approved_by',
        'approved_at',
    ];

    protected $dates = [
        'declined_at',
        'approved_at'
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function($model) {
            $model->ref     = Util::genPartOrderRef();
            $model->user_id = Auth::user()->id;
            $model->status  = "open";
        });
    }

    /**
     * get all open orders
     */
    public function scopeOpenOrders($query)
    {
        $query->where('status', 'open');
    }

    public function audit_events()
    {
        return $this->morphMany('App\AuditEvent', 'auditable');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function items()
    {
        return $this->hasMany('App\MachinePartOrderItem', 'order_id');
    }

    /**
     * get the items on cac
     */
    public function cac_items()
    {
        return $this->items()->whereNotNull('cac_reason');
    }

    /**
     * checks whether order has cac_items
     */
    public function has_cac_items()
    {
        return $this->cac_items()->count();
    }

    public function ticket()
    {
        return $this->belongsTo('App\Ticket');
    }

    public function client()
    {
        return $this->ticket->client;
    }

    public function dispatches()
    {
        return $this->hasMany('App\MachinePartDispatch', 'order_id');
    }

    public function declinedBy()
    {
        return $this->belongsTo('App\User', 'declined_by');
    }

    public function approvedBy()
    {
        return $this->belongsTo('App\User', 'approved_by');
    }

    public function can_decline()
    {
        if ($this->status == "open" && !$this->declined_at && !$this->approved_at) {
            return true;
        }
        return false;
    }

    public function can_approve()
    {
        if (!$this->approved_at) {
            return true;
        }
        return false;
    }
}
