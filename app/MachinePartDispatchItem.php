<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MachinePartDispatchItem extends Model
{
    //
    protected $fillable = [
        'dispatch_id',
        'part_stock_id',
        'stock_supply_id',
        'quantity',
    ];

    public function dispatch()
    {
        return $this->belongsTo('App\MachinePartDispatch', 'dispatch_id');
    }

    public function stock()
    {
        return $this->belongsTo('App\MachinePartStock', 'part_stock_id');
    }

    public function supply()
    {
        return $this->belongsTo('App\MachinePartStockSupply', 'stock_supply_id');
    }
}
