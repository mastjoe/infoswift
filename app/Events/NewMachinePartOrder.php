<?php

namespace App\Events;

use App\MachinePartOrder;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class NewMachinePartOrder
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $order;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(MachinePartOrder $order)
    {
        //
        $this->order = $order;
        $this->handleNewOrderStuff($order);
    }

    /**
     * handles stuff when an order is made
     * 
     * @param \App\MachinePartOrder
     * @return null
     */
    public function handleNewOrderStuff(MachinePartOrder $order)
    {
        // change ticket status to part ordered
        $order->ticket->update(['status' => 'part ordered']);

        // log audit event
        $order->audit_events()->create([
            'event'       => 'updated',
            'description' => 'created a machine parts order, '.$order->ref
        ]);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
