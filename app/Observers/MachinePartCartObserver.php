<?php

namespace App\Observers;

use App\Helpers\Upload;
use App\MachinePartCart;

class MachinePartCartObserver
{
    /**
     * Handle the machine part cart "created" event.
     *
     * @param  \App\MachinePartCart  $machinePartCart
     * @return void
     */
    public function created(MachinePartCart $machinePartCart)
    {
        //
    }

    /**
     * Handle the machine part cart "updated" event.
     *
     * @param  \App\MachinePartCart  $machinePartCart
     * @return void
     */
    public function updated(MachinePartCart $machinePartCart)
    {
        //
    }

    /**
     * Handle the machine part cart "deleted" event.
     *
     * @param  \App\MachinePartCart  $machinePartCart
     * @return void
     */
    public function deleted(MachinePartCart $machinePartCart)
    {
        //
        // Upload::deleteUploadedImages($machinePartCart);
    }

    /**
     * Handle the machine part cart "restored" event.
     *
     * @param  \App\MachinePartCart  $machinePartCart
     * @return void
     */
    public function restored(MachinePartCart $machinePartCart)
    {
        //
    }

    /**
     * Handle the machine part cart "force deleted" event.
     *
     * @param  \App\MachinePartCart  $machinePartCart
     * @return void
     */
    public function forceDeleted(MachinePartCart $machinePartCart)
    {
        //
    }
}
