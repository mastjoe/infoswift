<?php

namespace App\Observers;

use App\MachinePartSupplier;

class MachinePartSupplierObserver
{
    /**
     * Handle the machine part supplier "created" event.
     *
     * @param  \App\MachinePartSupplier  $machinePartSupplier
     * @return void
     */
    public function created(MachinePartSupplier $machinePartSupplier)
    {
        //
        $machinePartSupplier->audit_events()->create([
            'event'       => "created",
            'description' => "new machine part supplier, {$machinePartSupplier->name}",
        ]);
    }

    /**
     * Handle the machine part supplier "updated" event.
     *
     * @param  \App\MachinePartSupplier  $machinePartSupplier
     * @return void
     */
    public function updated(MachinePartSupplier $machinePartSupplier)
    {
        //
        $machinePartSupplier->audit_events()->create([
            'event' => 'updated',
            'description' => 'machine part supplier, '.$machinePartSupplier->name.' updated'
        ]);
    }

    /**
     * Handle the machine part supplier "deleted" event.
     *
     * @param  \App\MachinePartSupplier  $machinePartSupplier
     * @return void
     */
    public function deleted(MachinePartSupplier $machinePartSupplier)
    {
        //
        $machinePartSupplier->audit_events()->create([
            'event' => 'deleted',
            'description' => 'machine part supplier, '.$machinePartSupplier->name.' was deleted'
        ]);
    }

    /**
     * Handle the machine part supplier "restored" event.
     *
     * @param  \App\MachinePartSupplier  $machinePartSupplier
     * @return void
     */
    public function restored(MachinePartSupplier $machinePartSupplier)
    {
        //
    }

    /**
     * Handle the machine part supplier "force deleted" event.
     *
     * @param  \App\MachinePartSupplier  $machinePartSupplier
     * @return void
     */
    public function forceDeleted(MachinePartSupplier $machinePartSupplier)
    {
        //
    }
}
