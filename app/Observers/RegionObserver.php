<?php

namespace App\Observers;

use App\Region;

class RegionObserver
{
    /**
     * Handle the region "created" event.
     *
     * @param  \App\Region  $region
     * @return void
     */
    public function created(Region $region)
    {
        //
        $region->audit_events()->create([
            'event' => 'created',
            'description' => 'new region, '.$region->region.' created'
        ]);
    }

    /**
     * Handle the region "updated" event.
     *
     * @param  \App\Region  $region
     * @return void
     */
    public function updated(Region $region)
    {
        //
        $region->audit_events()->create([
            'event' => 'updated',
            'description' => 'region, '.$region->region.' updated' 
        ]);
    }

    /**
     * Handle the region "deleted" event.
     *
     * @param  \App\Region  $region
     * @return void
     */
    public function deleted(Region $region)
    {
        //
        $region->audit_events()->create([
            'event' => 'deleted',
            'description' => 'region '.$region->region.' deleted' 
        ]);
    }

    /**
     * Handle the region "restored" event.
     *
     * @param  \App\Region  $region
     * @return void
     */
    public function restored(Region $region)
    {
        //
    }

    /**
     * Handle the region "force deleted" event.
     *
     * @param  \App\Region  $region
     * @return void
     */
    public function forceDeleted(Region $region)
    {
        //
    }
}
