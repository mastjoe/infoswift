<?php

namespace App\Observers;

use App\MachinePartOrderItem;
use Illuminate\Support\Facades\Auth;

class MachinePartOrderItemObserver
{
    /**
     * Handle the machine part order item "created" event.
     *
     * @param  \App\MachinePartOrderItem  $machinePartOrderItem
     * @return void
     */
    public function created(MachinePartOrderItem $machinePartOrderItem)
    {
        //
    }

    /**
     * Handle the machine part order item "updated" event.
     *
     * @param  \App\MachinePartOrderItem  $machinePartOrderItem
     * @return void
     */
    public function updated(MachinePartOrderItem $machinePartOrderItem)
    {
        //
    }

    /**
     * Handle the machine part order item "deleted" event.
     *
     * @param  \App\MachinePartOrderItem  $machinePartOrderItem
     * @return void
     */
    public function deleted(MachinePartOrderItem $machinePartOrderItem)
    {
        //
    }

    /**
     * Handle the machine part order item "restored" event.
     *
     * @param  \App\MachinePartOrderItem  $machinePartOrderItem
     * @return void
     */
    public function restored(MachinePartOrderItem $machinePartOrderItem)
    {
        //
    }

    /**
     * Handle the machine part order item "force deleted" event.
     *
     * @param  \App\MachinePartOrderItem  $machinePartOrderItem
     * @return void
     */
    public function forceDeleted(MachinePartOrderItem $machinePartOrderItem)
    {
        //
    }
}
