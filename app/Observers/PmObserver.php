<?php

namespace App\Observers;

use App\Pm;
use App\Helpers\Upload;

class PmObserver
{
    /**
     * Handle the pm "created" event.
     *
     * @param  \App\Pm  $pm
     * @return void
     */
    public function created(Pm $pm)
    {
        //
    }

    /**
     * Handle the pm "updated" event.
     *
     * @param  \App\Pm  $pm
     * @return void
     */
    public function updated(Pm $pm)
    {
        //
    }

    /**
     * Handle the pm "deleted" event.
     *
     * @param  \App\Pm  $pm
     * @return void
     */
    public function deleted(Pm $pm)
    {
        //
        Upload::deleteUploadedAttachment($pm);
    }

    /**
     * Handle the pm "restored" event.
     *
     * @param  \App\Pm  $pm
     * @return void
     */
    public function restored(Pm $pm)
    {
        //
    }

    /**
     * Handle the pm "force deleted" event.
     *
     * @param  \App\Pm  $pm
     * @return void
     */
    public function forceDeleted(Pm $pm)
    {
        //
    }
}
