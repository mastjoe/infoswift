<?php

namespace App\Observers;

use App\Client;
use App\Helpers\Util;

class ClientObserver
{
    /**
     * Handle the client "created" event.
     *
     * @param  \App\Client  $client
     * @return void
     */
    public function created(Client $client)
    {
        //
        $client->audit_events()->create([
            'event'       => "created",
            'description' => "new client, {$client->name}",
        ]);
    }

    /**
     * Handle the client "updated" event.
     *
     * @param  \App\Client  $client
     * @return void
     */
    public function updated(Client $client)
    {
        //
        if ($client->isDirty('supervisor_id')) {
            $client->audit_events()->create([
                'event'       => 'updated',
                'description' => 'staff, ' . $client->supervisor->full_name() . 'was assigned to client, ' . $client->short_name . ' as a supervisor'
            ]);
        } else if ($client->getDirty()) {
            $client->audit_events()->create([
                'event'       => "updated",
                'description' => "updated client, {$client->name}",
            ]);
        }
    }

    /**
     * Handle the client "deleted" event.
     *
     * @param  \App\Client  $client
     * @return void
     */
    public function deleted(Client $client)
    {
        //
        $client->audit_events()->create([
            'event' => 'deleted',
            'description' => 'deleted client '.$client->name
        ]);
        Util::auditNullify('Client', $client->id);
    }

    /**
     * Handle the client "restored" event.
     *
     * @param  \App\Client  $client
     * @return void
     */
    public function restored(Client $client)
    {
        //
    }

    /**
     * Handle the client "force deleted" event.
     *
     * @param  \App\Client  $client
     * @return void
     */
    public function forceDeleted(Client $client)
    {
        //
    }
}
