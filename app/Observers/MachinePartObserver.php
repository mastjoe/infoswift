<?php

namespace App\Observers;

use App\MachinePart;
use App\Helpers\Util;

class MachinePartObserver
{
    /**
     * Handle the machine part "created" event.
     *
     * @param  \App\MachinePart  $machinePart
     * @return void
     */
    public function created(MachinePart $machinePart)
    {
        //
        // log audit
        $machinePart->audit_events()->create([
            'event'       => 'created',
            'description' => 'new machine part, '.$machinePart->part.' was created'
        ]);
    }

    /**
     * Handle the machine part "updated" event.
     *
     * @param  \App\MachinePart  $machinePart
     * @return void
     */
    public function updated(MachinePart $machinePart)
    {
        //
        if ($machinePart->isDirty()) {
            $machinePart->audit_events()->create([
                'event'       => 'updated',
                'description' => 'machine part '.$machinePart->part.' was updated'
            ]);
        }
    }

    /**
     * Handle the machine part "deleted" event.
     *
     * @param  \App\MachinePart  $machinePart
     * @return void
     */
    public function deleted(MachinePart $machinePart)
    {
        //
        if ($machinePart->images->count()) {
            foreach ($machinePart->images as $image) {
                $image->delete();
            }
        }

        $machinePart->audit_events()->create([
            'event'       => 'deleted',
            'description' => 'machine part, '.$machinePart->part.' was deleted'
        ]);

        Util::auditNullify('MachinePart', $machinePart->id);
    }

    /**
     * Handle the machine part "restored" event.
     *
     * @param  \App\MachinePart  $machinePart
     * @return void
     */
    public function restored(MachinePart $machinePart)
    {
        //
    }

    /**
     * Handle the machine part "force deleted" event.
     *
     * @param  \App\MachinePart  $machinePart
     * @return void
     */
    public function forceDeleted(MachinePart $machinePart)
    {
        //
    }
}
