<?php

namespace App\Observers;

use App\MachinePartStockSupply;

class MachinePartStockSupplyObserver
{
    /**
     * Handle the machine part stock supply "created" event.
     *
     * @param  \App\MachinePartStockSupply  $machinePartStockSupply
     * @return void
     */
    public function created(MachinePartStockSupply $machinePartStockSupply)
    {
        //
        $machinePartStockSupply->update([
            'initial_quantity' => $machinePartStockSupply->quantity
        ]);
    }

    /**
     * Handle the machine part stock supply "updated" event.
     *
     * @param  \App\MachinePartStockSupply  $machinePartStockSupply
     * @return void
     */
    public function updated(MachinePartStockSupply $machinePartStockSupply)
    {
        //
        // $machinePartStockSupply->initial_quantity = $machinePartStockSupply->quantity;
        // $machinePartStockSupply->save();
    }

    /**
     * Handle the machine part stock supply "deleted" event.
     *
     * @param  \App\MachinePartStockSupply  $machinePartStockSupply
     * @return void
     */
    public function deleted(MachinePartStockSupply $machinePartStockSupply)
    {
        //
    }

    /**
     * Handle the machine part stock supply "restored" event.
     *
     * @param  \App\MachinePartStockSupply  $machinePartStockSupply
     * @return void
     */
    public function restored(MachinePartStockSupply $machinePartStockSupply)
    {
        //
    }

    /**
     * Handle the machine part stock supply "force deleted" event.
     *
     * @param  \App\MachinePartStockSupply  $machinePartStockSupply
     * @return void
     */
    public function forceDeleted(MachinePartStockSupply $machinePartStockSupply)
    {
        //
    }
}
