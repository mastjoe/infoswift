<?php

namespace App\Observers;

use App\Helpers\Util;
use App\MachinePartDispatch;
use Illuminate\Support\Facades\Auth;

class MachinePartDispatchObserver
{
    /**
     * Handle the machine part dispatch "creating" event.
     *
     * @param  \App\MachinePartDispatch  $machinePartDispatch
     * @return void
     */
    public function creating(MachinePartDispatch $machinePartDispatch)
    {
        $machinePartDispatch->ref = Util::genPartDispatchRef();
        $machinePartDispatch->created_by = Auth::user()->id;
    }

    /**
     * Handle the machine part dispatch "created" event.
     *
     * @param  \App\MachinePartDispatch  $machinePartDispatch
     * @return void
     */
    public function created(MachinePartDispatch $machinePartDispatch)
    {
        //log audit event...
        $machinePartDispatch->order->audit_events()->create([
            "event" => "created",
            "description" => 
            "a dispatch session, {$machinePartDispatch->ref} was created for order, {$machinePartDispatch->order->ref}"
        ]);
    }

    /**
     * Handle the machine part dispatch "updated" event.
     *
     * @param  \App\MachinePartDispatch  $machinePartDispatch
     * @return void
     */
    public function updated(MachinePartDispatch $machinePartDispatch)
    {
        //
    }

    /**
     * Handle the machine part dispatch "deleted" event.
     *
     * @param  \App\MachinePartDispatch  $machinePartDispatch
     * @return void
     */
    public function deleted(MachinePartDispatch $machinePartDispatch)
    {
        //
        //log audit event...
        $machinePartDispatch->order->audit_events()->create([
            "event" => "deleted",
            "description" =>
            "a dispatch session, {$machinePartDispatch->ref} for order, {$machinePartDispatch->order->ref} was deleted"
        ]);
    }

    /**
     * Handle the machine part dispatch "restored" event.
     *
     * @param  \App\MachinePartDispatch  $machinePartDispatch
     * @return void
     */
    public function restored(MachinePartDispatch $machinePartDispatch)
    {
        //
    }

    /**
     * Handle the machine part dispatch "force deleted" event.
     *
     * @param  \App\MachinePartDispatch  $machinePartDispatch
     * @return void
     */
    public function forceDeleted(MachinePartDispatch $machinePartDispatch)
    {
        //
    }
}
