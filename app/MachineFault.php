<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MachineFault extends Model
{
    //
    protected $capitals = [
        'atm',
        'fdk',
        'gop',
        'cop',
        'epp'
    ];
    protected $fillable = [
        'fault',
        'description',
        'new'
    ];

    public function setFaultAttribute($value)
    {
        $lower = strtolower($value);
        return $this->attributes['fault'] = $lower;
    }

    public function ticketFaults()
    {
        return $this->hasMany('App\TicketFault', 'fault_id')->with('ticket');
    }

    public function ticketFaultSolutions()
    {
        return $this->ticketFaults()->whereNotNull('solution')->distinct('solution');
    }

    public function scopeNewFaults($query)
    {
        return $query->where('new', true);
    }

    public function deletable()
    {
        if ($this->ticketFaults()->count()) {
            return false;
        }
        return true;
    }

}
