<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EngineerState extends Model
{
    //
    protected $fillable = [
        'user_id',
        'country_state_id'
    ];

}
