<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MachineType extends Model
{
    //
    protected $fillable = [
        'model',
        'class',
        'type'
    ];

    public function machines()
    {
        return $this->hasMany('App\Machine', 'machine_type_id');
    }

    public function deletable()
    {
        if ($this->machines->count()) {
            return false;
        }
        return true;
    }
}
