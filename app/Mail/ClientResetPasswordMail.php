<?php

namespace App\Mail;

use App\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ClientResetPasswordMail extends Mailable
{
    use Queueable, SerializesModels;

    public $client;
    public $password;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Client $client, $password)
    {
        //
        $this->client = $client;
        $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Reset Password')
                    ->to('mastjoe@yahoo.com')
                    ->from('admin@newbbmservers.com', 'Global Infoswift Technology')
                    ->view('emails.clients.reset-password')
                    ->to($this->client->personnel_email, $this->client->personnel_name);
    }
}
