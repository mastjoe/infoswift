<?php

namespace App\Mail;

use App\Ticket;
use App\Helpers\Util;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EngineerDeclineTicketMail extends Mailable
{
    use Queueable, SerializesModels;

    public $ticket;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Ticket $ticket)
    {
        //
        $this->ticket = $ticket;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.admins.engineer-decline-ticket')
            ->subject('Declined Ticket')
            ->to($this->ticket->closer->email, $this->ticket->closer->full_name())
            ->from(Util::emailSettings("server_email"), Util::emailSettings("sender_name"));
    }
}
