<?php

namespace App\Mail;

use App\Ticket;
use App\Helpers\Util;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SupervisorNewTicketMail extends Mailable
{
    use Queueable, SerializesModels;

    public $ticket;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Ticket $ticket)
    {
        //
        $this->ticket = $ticket;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.admins.supervisor-new-ticket')
            ->subject('New Ticket')
            ->from(Util::emailSettings("server_email"), Util::emailSettings("sender_name"))
            ->to($this->ticket->machine->engineer->email, $this->ticket->machine->engineer->full_name());
    }
}
