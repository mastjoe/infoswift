<?php

namespace App\Mail;

use App\Ticket;
use App\Helpers\Util;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EngineerConfirmCloseTicketMail extends Mailable
{
    use Queueable, SerializesModels;

    public $ticket;
    public $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Ticket $ticket, $user)
    {
        //
        $this->ticket = $ticket;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.admins.engineer-confirmed-closed-ticket')
            ->subject('Confirmed Closed Ticket')
            ->to($this->user->email, $this->user->full_name())
            ->from(Util::emailSettings("server_email"), Util::emailSettings("sender_name"));
    }
}
