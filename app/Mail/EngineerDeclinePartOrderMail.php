<?php

namespace App\Mail;

use App\Helpers\Util;
use App\MachinePartOrder;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EngineerDeclinePartOrderMail extends Mailable
{
    use Queueable, SerializesModels;

    public $order;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(MachinePartOrder $order)
    {
        //
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.admins.declined-part-order')
            ->subject('Parts Order Declined')
            ->to($this->order->user->email, $this->order->user->full_name())
            ->from(Util::emailSettings("server_email"), Util::emailSettings("sender_name"));
    }
}
