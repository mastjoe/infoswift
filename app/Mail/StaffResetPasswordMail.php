<?php

namespace App\Mail;

use App\User;
use App\Helpers\Util;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class StaffResetPasswordMail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $password;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, string $password)
    {
        // 
        $this->user = $user;
        $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Reset Password')
            ->to($this->user->email, $this->user->full_name())
            ->from(Util::emailSettings("server_email"), Util::emailSettings("sender_name") )
            ->view('emails.admins.reset-password');
    }
}
