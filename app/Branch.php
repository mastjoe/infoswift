<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    //
    protected $fillable = [
        'branch',
        'client_id',
        'state_id',
        'region_id',
        'country_id',
        'sol_id',
        'address',
        'phones',
        'emails'
    ];

    public function client()
    {
        return $this->belongsTo('App\Client');
    }

    public function state()
    {
        return $this->belongsTo('App\CountryState');
    }

    public function region()
    {
        return $this->belongsTo('App\Region');
    }

    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    public function custodians()
    {
        return $this->hasMany('App\BranchCustodian');
    }

    public function machines()
    {
        return $this->hasMany('App\Machine');
    }

    public function deletable()
    {
        if ($this->machines->count()) {
            return false;
        }
        return true;
    }
}
