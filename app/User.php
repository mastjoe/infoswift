<?php

namespace App;

use App\Helpers\Util;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $guard = "user";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'middle_name',
        'last_name',
        'dob',
        'address',
        'photo',
        'phone',
        'gender',
        'company_email',
        'email',
        'password',
        'last_login',
        'current_login',
        'last_change_password',
        'suspended_at',
        'bio',
        'socials',
        'ref'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $dates = [
        'last_login',
        'current_login',
        'last_change_password',
        'dob'
    ];

    protected $appends = [
        "full_name"
    ];

    protected static function boot()
    {
        parent::boot();
        
        static::creating(function($model) {
            $model->ref = Util::genUserRef();
        });
    }

    public function getFullnameAttribute()
    {
        return $this->full_name();
    }

    public function setFirstnameAttribute($value)
    {
        $this->attributes['first_name'] = ucfirst(strtolower($value));
    }

    public function setMiddlenameAttribute($value)
    {
        $this->attributes['middle_name'] = ucfirst(strtolower($value));
    }

    public function setLastnameAttribute($value) 
    {
        $this->attributes['last_name'] = ucfirst(strtolower($value));
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = \bcrypt($value);
    }

    public function avatar()
    {
        if ($this->photo && file_exists(\public_path($this->photo))) {
            return \asset($this->photo);
        }
        return asset('images/avatar16.jpg');
    }

    public function full_name()
    {
        return $this->first_name.' '.$this->last_name;
    }

    public function isOnline()
    {
        return Cache::has('users_online_' . $this->id);
    }

    public function deletable()
    {
        if (
            $this->machines()->count() ||
            $this->guarantors()->count() ||
            $this->supervisedClients()->count()
        ) {
            return false;
        }
        return true;
    }

    public function guarantors()
    {
        return $this->hasMany('App\UserGuarantor');
    }

    public function supervisedClients()
    {
        return $this->hasMany('App\Client', 'supervisor_id');
    }

    /**
     * users cart items
     */
    public function cart()
    {
        return $this->hasMany('App\MachinePartCart');
    }

    /**
     * cash and charge parts in cart
     */
    public function cart_cac()
    {
        return $this->cart()->whereNotNull('cac_reason');
    }

    /**
     * sum of quantities of cart items
     */
    public function cart_sum()
    {
        return $this->cart->sum('quantity');
    }

    /**
     * determines if user has cac part in current cart
     */
    public function cart_has_cac()
    {
       return $this->cart_cac()->count();
    }

    public function roles()
    {
        return $this->belongsToMany('App\Role', 'user_roles');
    }

    public function audit_events()
    {
        return $this->morphMany('App\AuditEvent', 'auditable');
    }

    public function roles_array()
    {
        if ($this->roles->count()) {
            return $this->roles->pluck('role')->toArray();
        }
        return [];
    }

    public function privileges()
    {
        return $this->belongsToMany('App\Privilege', 'user_privileges');
    }

    public function privileges_array()
    {
        if ($this->privileges->count()) {
            return $this->privileges->pluck('privilege')->toArray();
        }
        return [];
    }

    public function hasPrivilege($privilege)
    {
        $privilege = strtolower($privilege);
        $privileges_array = array_map('strtolower', $this->privileges_array());

        if (in_array($privilege, $privileges_array)) {
            return true;
        }
        return false;
    }

    public function hasRole($role)
    {
        $roles_array = array_map('strtolower',  $this->roles_array());
        $role = strtolower($role);

        if (in_array($role, $roles_array)) {
            return true;
        }
        return false;
    }

    public static function usersWithRole($role)
    {
        return User::all()->filter(function($user) use($role) {
            if ($user->hasRole($role)) {
                return $user;
            }
        });
    }

    public static function usersWithPrivileges($privilege)
    {
        return self::all()->filter(function($user) use($privilege) {
            if ($user->hasPrivilege($privilege)) {
                return $user;
            }
        });
    }

    /**
     * tells if a user has a super admin role
     */
    public function isSuperAdmin()
    {
        return $this->hasRole("super admin");
    }

    public function states()
    {
        return $this->belongsToMany('App\CountryState', 'engineer_states')
            ->withTimestamps();
    }


    public function machines()
    {
        return $this->hasMany('App\Machine', 'engineer_id');
    }

    /**
     * get all machines within users' assigned state
     */
    public function all_state_machines()
    {
        return $this->states->map(function($state) {
            return $state->machines;
        })->filter()->flatten();
    }

    /**
     * get all machine tickets within user's assigned states
     */
    public function all_state_machine_tickets()
    {
        return $this->all_state_machines()->map(function($machine) {
           return $machine->tickets;
        })->flatten()->filter();
    }

    /**
     * get all machine tickets within user's assigned state
     * with specific status
     */
    public function all_state_machine_tickets_with_status($status)
    {
        $status = strtolower($status);

        return $this->all_state_machine_tickets()->filter(function($ticket) use($status) {
            if ($ticket->status == $status) {
                return $ticket;
            }
        });
    }

    /**
     * get all machines within user's specified state
     */
    public function state_machines($state_id)
    {
        $all_state_machines = CountryState::find($state_id)->machines;
        $engineer_machine_ids = $this->machines->pluck('id')->toArray();
        return $all_state_machines->filter(function($machine) use($engineer_machine_ids) {
            if (in_array($machine->id, $engineer_machine_ids)) {
                return $machine;
            }
        });
    }

    /**
     * get user social information
     */
    public function getSocials()
    {
        if ($this->socials) {
            return json_decode($this->socials, true);
        }
        return [];
    }


    public function machine_part_orders()
    {
        return $this->hasMany('App\MachinePartOrder');
    }

    /**
     * get all user's inboxes
     */
    public function inboxes()
    {
        return $this->hasMany('App\PrivateMessage', 'receiver_id');
    }

    /**
     * get all user's unread inboxes
     */
    public function unread_inboxes()
    {
        return $this->inboxes()->whereNull('read_at');
    }

    /**
     * get all user's read inboxes
     */
    public function read_inboxes()
    {
        return $this->inboxes()->whereNotNull('read_at');
    }

    /**
     * get all user's sent messages
     */
    public function outboxes()
    {
        return $this->hasMany('App\PrivateMessage', 'sender_id');
    }

    /**
     * determines if a user is messageable
     */
    public function messageable()
    {
        if ($this->id != Auth::user()->id) {
            return true;
        }
        return false;
    }

}
