<?php

namespace App;

use App\Helpers\Util;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class MachinePartStockSupply extends Model
{
    //
    protected $guarded = [];

    public function setCostpriceAttribute($val)
    {
        $this->attributes['cost_price'] = Util::normalizeAmount($val);
    }

    public function setSellingPriceAttribute($val)
    {
        $this->attributes['selling_price'] = Util::normalizeAmount($val);
    }

    public static function boot()
    {
        parent::boot();
        static::creating(function($model) {
            $model->reference = Util::genPartStockSupplyRef();
            $model->created_by = Auth::user()->id;
        });
    }

    public function stock()
    {
        return $this->belongsTo('App\MachinePartStock', 'stock_id');
    }

    public function supplier()
    {
        return $this->belongsTo('App\MachinePartSupplier', 'supplier_id');
    }

    public function creator()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function deletable()
    {
        if ($this->initial_quantity != $this->quantity) {
            return false;
        }
        return true;
    }

    public function editable()
    {
        if ($this->quantity == 0) {
            return false;
        }
        return true;
    }

}
