<?php

namespace App\Repositories;

use App\Image;
use App\MachinePart;
use App\Helpers\Upload;
use App\MachinePartCategory;
use Illuminate\Http\Request;

class MachinePartRepository
{
    /**
     * create new machine part
     * 
     * @param \Illuminate\Http\Request $request
     * @return \App\MachinePart
     */
    public function create(Request $request)
    {
        $request->validate([
            'category'                 => 'required',
            'part'                     => 'required|unique:machine_parts,part',
            'description'              => 'nullable|string',
            'technical_specifications' => 'nullable|string',
            'documentation'            => 'nullable|string',
            'images.*'                 => 'min:1|image|max:500'
        ]);

        $category = MachinePartCategory::findOrFail($request->category);
        $part     = $category->machine_parts()->create($request->all());
        Upload::machinePartsImage($request, $part);
        return $part;
    }

    /**
     * update machine part
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \App\MachinePart
     */
    public function update(Request $request, $id)
    {   
        $request->validate([
            'category'                 => 'required',
            'part'                     => 'required|unique:machine_parts,part,'.$request->part.',part',
            'description'              => 'nullable|string',
            'technical_specifications' => 'nullable|string',
            'documentation'            => 'nullable|string'
        ]);

        $part = MachinePart::findOrFail($id);
        $part->category_id = $request->category;
        $part->part = $request->part;
        $part->description = $request->description;
        $part->technical_specifications = $request->technical_specifications;
        $part->documentation = $request->documentation;
        $part->save();
        return $part;
    }

    /**
     * delete machine part
     * 
     * @param \App\MachinePart
     * @return bool
     */
    public function delete(machinePart $part)
    {
        if ($part->deletable()) {
            if ($part->delete()){
                return true;
            }
        }
        return false;
    }

    /**
     * add machine part images
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \App\MachinePart
     */
    public function addPartImages(Request $request, $id)
    {
        $request->validate([
            'images.*' => 'min:1|image|max:500'
        ]);

        $part = MachinePart::findOrFail($id);
        Upload::machinePartsImage($request, $part);
        $part->images;

        // log audit
        $part->audit_events()->create([
            'event'       => 'updated',
            'description' => 'images added to part, '.$part->part
        ]);

        return $part;
    }

    /**
     * delete part image
     * 
     * @param \App\MachinePart
     * @param int $image_id
     * @return \App\Image
     */
    public function removeImage(MachinePart $part, int $image_id)
    {
        $image = Image::findOrFail($image_id);

        if ($image->delete()) {

            // log audit
            $part->audit_events()->create([
                'event'       => 'deleted',
                'description' => 'image of machine part, '.$part->part.' was deleted'
            ]);

            return $image;
        }
        return false;
    }
}