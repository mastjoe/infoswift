<?php

namespace App\Repositories;

use App\User;
use Carbon\Carbon;
use App\Helpers\Util;
use App\PrivateMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use App\Notifications\AdminNewPrivateMessageNotification;



class PrivateMessageRepository
{
    /**
     * get logged in user
     * 
     * @return \App\User
     */
    protected function user()
    {
        return Auth::user();
    }

    /**
     * get message by reference
     * 
     * @param string $ref
     * @return \App\PrivateMessage
     */
    public function findByRef($ref)
    {
        return PrivateMessage::with(['sender', 'receiver'])->where('ref', $ref)->firstOrFail();
    }

    public function inboxes()
    {
        return $this->user()->inboxes;
    }

    public function outboxes()
    {
        return $this->user()->outboxes;
    }

    public function unread_inboxes()
    {
        return $this->user()->unread_inboxes;
    }

    protected function messages()
    {
        return PrivateMessage::query()
            ->orWhere('sender_id', $this->user()->id)
            ->orWhere('receiver_id', $this->user()->id)
            ->get();
    }

    public function conversations()
    {
        return $this->messages();
    }

    /**
     * get possible recipients
     */
    public function getPossibleRecipients()
    {
        return User::where('id', '<>', $this->user()->id)->get();
    }

    /**
     * send message to recipients
     * 
     * @param \Illuminate\Http\Request $request
     * @return Collection
     */
    public function sendMessage(Request $request)
    {
        $request->validate([
            "subject"      => "required|string|max:100",
            "message"      => "required|string",
            "recipients.*" => "required",
            "recipients"   => "array|min:1"
        ]);

        $messages = [];

        foreach ($request->recipients as $recipient) {
            $message = $this->pushMessage($request, $recipient);
            array_push($messages, $message);
        }

        return new Collection($messages);
    }

    /**
     * push message to a single recipient
     * 
     */
    protected function pushMessage(Request $request, int $recipient_id)
    {
        $message = $this->user()->outboxes()->create([
            "subject"     => $request->subject,
            "message"     => $request->message,
            "receiver_id" => $recipient_id,
        ]);

        // notify recipient
        $message->receiver->notify(new AdminNewPrivateMessageNotification($message));

        return $message;
    }


    /**
     * read message
     * 
     * @param string $ref
     * @return \Illuminate\Http\Response
     */
    public function readMessage($ref)
    {
        $message = $this->findByRef($ref);

        if ($this->user()->is($message->receiver)) {
            $message->update(["read_at" => Carbon::now()]);
        }
        return $message;
    }

    /**
     * send message to specific staff
     */
    public function messageStaff(Request $request, int $staff_id)
    {
        $request->validate([
            "subject" => "required|string|max:100",
            "message" => "required|string",
        ]);

        if ($this->user()->id  == $staff_id) {
            Util::pushError(['sender' => 'can\'t send message to self']);
        }
        return $this->pushMessage($request, $staff_id);
    }
}