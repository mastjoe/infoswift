<?php

namespace App\Repositories;

use App\Client;
use App\Ticket;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class ReportRepository
{
    
    /**
     * get downlist for specific date and client
     * 
     * @param \App\Client
     * @param string $date
     * @return Collection
     */
    public function getClientDownlist(Client $client, $date)
    {
        return Ticket::with(['machine', 'machine.engineer', 'machine.branch'])
            ->where('client_id', $client->id)
            ->where(function($query) use($date) {
                $query->whereDate('created_at', '<=', $date)
                    ->whereNull('closed_at');
            })
            ->orWhere(function($query) use($date) {
                $query->where('created_at', '<=', $date)
                    ->whereNotNull('closed_at')
                    ->whereDate('closed_at', '<', $date);
            })
            ->get();
    }
}