<?php

namespace App\Repositories;

use Str;
use App\User;
use Carbon\Carbon;
use App\Helpers\Util;
use App\Helpers\Photo;
use App\Notifications\StaffResetPasswordNotification;
use App\UserGuarantor;
use Illuminate\Http\Request;

class UserRepository
{
    /**
     * create user
     * @param \Illuminate\Http\Request $request
     * @return \App\User
     */
    public function create(Request $request)
    {
        $request->validate([
            'first_name'  => 'required|string',
            'middle_name' => 'nullable|string',
            'last_name'   => 'required|string',
            'gender'      => 'required',
            'email'       => 'required|email|unique:users,email',
            'phone'       => 'required|digits_between:8,15',
            'dob'         => 'required|date',
            'address'     => 'nullable|string',
            'roles'       => 'required|min:1',
            'bio'         => 'nullable|string',
        ],
        [
            // 'dob.required' => 'Date of birth is required'
        ],
        [
            'dob' => 'date of birth'
        ]);

        $user = User::create([
            'first_name'  => $request->first_name,
            'middle_name' => $request->middle_name,
            'last_name'   => $request->last_name,
            'email'       => $request->email,
            'phone'       => $request->phone,
            'dob'         => Carbon::parse($request->dob),
            'address'     => $request->address,
            'gender'      => $request->gender,
            'password'    => "password",
            'bio'         => $request->bio,
            'socials'     => json_encode($request->socials)
        ]);

        Util::attachUserRoles($user->id, $request->roles);
        return $user;
    }

    /**
     * update user data
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \App\User
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'first_name'  => 'required|string',
            'middle_name' => 'nullable|string',
            'last_name'   => 'required|string',
            'gender'      => 'required',
            'email'       => 'required|email|unique:users,email,' . $request->email . ',email',
            'phone'       => 'required|digits_between:8,15',
            'dob'         => 'nullable|date',
            'address'     => 'nullable|string',
            'bio'         => 'nullable|string'
        ], [], [
            'dob' => 'date of birth'
        ]);

        $user              = User::findOrFail($id);
        $user->first_name  = $request->first_name;
        $user->middle_name = $request->middle_name;
        $user->last_name   = $request->last_name;
        $user->gender      = $request->gender;
        $user->email       = $request->email;
        $user->phone       = $request->phone;
        $user->dob         = Carbon::parse($request->dob);
        $user->address     = $request->address;
        $user->bio         = $request->bio;
        $user->socials     = json_encode($request->socials);
        $user->save();

        if ($user->getDirty()) {            
            $user->audit_events()->create([
                'event'       => 'updated',
                'description' => 'updated staff, '.$user->full_name().'\'s profile'
            ]);
        }
        return $user;
    }

    /**
     * delete user record
     * 
     * @param int $id
     * @return bool
     */
    public function delete($id)
    {   
        $user = User::findOrFail($id);
        if ($user->deletable()) {

            Util::detachAllUserRoles($user->id);
            Util::auditNullify('User', $user->id);

            if ($user->delete()) {
                return true;
            }
        }
        return false;
    }

    /**
     * suspend user
     * 
     * @param int $id
     * @return \App\User
     */
    public function suspend($id)
    {
        $user = User::findOrFail($id);
        $user->update([
            'suspended_at' => Carbon::now()
        ]);

        // log audit
        $user->audit_events()->create([
            'event'       => 'updated',
            'description' => 'staff, '.$user->full_name().' was suspended'
        ]);

        return $user;
    }

    /**
     * unsuspend user
     * 
     * @param int $id
     * @return \App\User
     */
    public function unsuspend($id)
    {
        $user = User::findOrFail($id);
        $user->update([
            'suspended_at' => null
        ]);

        // log audit
        $user->audit_events()->create([
            'event'       => 'updated',
            'description' => 'staff, '.$user->full_name().' was unsuspended'
        ]);

        return $user;

    }

    /**
     * stores users guarantor
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id 
     * @return \App\UserGuarantor
     */
    public function storeGuarantor(Request $request, $id)
    {
        $request->validate([
            'name'         => 'required|string',
            'phone'        => 'required|digits_between:8,15',
            'email'        => 'nullable|email',
            'relationship' => 'required|string',
            'occupation'   => 'nullable|string',
            'address'      => 'nullable|string',
            'photo'        => 'nullable|image|mimes:png,jpg,gif,jpeg|max:500'
        ]);

        $user = User::findOrFail($id);

        $guarantor = $user->guarantors()->create([
            'name'         => $request->name,
            'phone'        => $request->phone,
            'email'        => $request->email,
            'relationship' => $request->relationship,
            'occupation'   => $request->occupation,
            'address'      => $request->address
        ]);

        // log audit
        $guarantor->audit_events()->create([
            'event'       => 'created',
            'description' => 'new guarantor, '.$guarantor->name.' was added for staff, '.$user->full_name(),
        ]);

        Photo::handleGuarantorPhotoUpload($request, $guarantor->id);
        return $guarantor;
    }

    /**
     * deletes user gauarantor
     * 
     * @param int $guarantor_id
     * @return  bool true|false
     */
    public function deleteGuarantor($guarantor_id)
    {
        $g = UserGuarantor::findOrFail($guarantor_id);
        $user = $g->user;
        if ($g->deletable()) {
            Photo::deleteGuarantorPhoto($g->id);
            $g->audit_events()->create([
                'event'       => 'deleted',
                'description' => $g->name.', a guarantor to staff, '.$user->full_name().' was deleted'
            ]);

            Util::auditNullify('UserGuarantor', $guarantor_id);

            // log audit
            if ($g->delete()) {
                return true;
            }
        }
        return false;

    }

    /**
     * updates user's guarantor record
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $guarantor_id
     * @return \App\UserGuarantor
     */
    public function updateGuarantor(Request $request, $guarantor_id)
    {
        $request->validate([
            'name'         => 'required|string',
            'phone'        => 'required|digits_between:8,15',
            'email'        => 'nullable|email',
            'relationship' => 'required|string',
            'occupation'   => 'nullable|string',
            'address'      => 'nullable|string',
            'photo'        => 'nullable|image|mimes:png,jpg,gif,jpeg|max:500'
        ]);
        $g = UserGuarantor::findOrFail($guarantor_id);

        $g->name         = $request->name;
        $g->phone        = $request->phone;
        $g->email        = $request->email;
        $g->relationship = $request->relationship;
        $g->occupation   = $request->relationship;
        $g->occupation   = $request->address;

        Photo::updateGuarantorPhoto($request, $guarantor_id);
        $g->save();

        // log audit
        $g->audit_events()->create([
            'event'       => 'updated',
            'description' => 'staff, '.$g->user->full_name().'\'s guarantor '.$g->name.' was updated'
        ]);

        return $g;
    }

    /**
     * reset user password
     * @param \App\User
     * @return string $password
     */
    public function resetPassword(User $user)
    {
        $password = Str::random(7);
        $hashed_password = \bcrypt($password);

        $user->update(['password' =>  $hashed_password]);

        // notify user
        $user->notify(new StaffResetPasswordNotification($user, $password));
        return $password;
    }
}