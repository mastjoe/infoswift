<?php

namespace App\Repositories;

use App\Helpers\Upload;
use App\MachinePartCart;
use Illuminate\Http\Request;
use App\MachinePartOrderItem;
use App\Events\NewMachinePartOrder;
use Illuminate\Support\Facades\Auth;

class MachinePartCartRepository
{

    /**
     * add new machine part item to cart
     * 
     * @param \Illuminate\Http\Request $request
     * @return \App\MachinePartCart
     */
    public function addToCart(Request $request)
    {
        $request->validate([
            'part'       => 'required',
            'quantity'   => 'nullable|numeric',
            'cac_reason' => 'required_if:cac,=,1',
            'images*.'   => 'nullable|image|mimes:png,jpg,jpeg,gif|max:500',
            'images'     => 'max:3'
        ], [
            'images.max' => 'images should be at most 3'
        ], [
            'cac_reason' => 'cash and charge reason',
            'cac'        => 'cash and charge'
        ]);

        $quantity = 1;
        if ($request->quantity) {
            $quantity = $request->quantity;
        }

        if ($this->inCart($request->part)) {
            $cartItem = $this->inCart($request->part);
            $cartItem->increment('quantity', $quantity);
        } else {
            $cartItem = MachinePartCart::create([
                'user_id'    => $this->user()->id,
                'part_id'    => $request->part,
                'quantity'   => $quantity,
                'cac_reason' => $request->cac_reason
            ]);
        }

        // upload images if exist
        Upload::uploadMultipleImages($request, $cartItem, 'images');

        $cartItem->machine_part;
        return $cartItem;
    }

    /**
     * remove machine part item from cart
     * 
     * @param \App\MachinePartCart
     * @return bool
     */
    public function removeFromCart(MachinePartCart $cartItem)
    {
        if ($cartItem->user->is($this->user())) {
            Upload::deleteUploadedImages($cartItem);
            if ($cartItem->delete()) {
                return true;
            }
        }
        return false;
    }

    /**
     * update a cart item
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \App\MachinePartCart
     */
    public function updateCartItem(Request $request, $id)
    {
        $request->validate([
            'quantity'   => 'required|min:1',
            'cac_reason' => 'nullable|string',
        ], [], [
            'cac_reason' => 'cash and charge reason'
        ]);

        $cartItem = MachinePartCart::findOrFail($id);
        $cartItem->update([
            'quantity'   => $request->quantity,
            'cac_reason' => $request->cac_reason
        ]);
        return $cartItem;
    }

    /**
     * checks out cart
     * 
     * @param \Illuminate\Http\Request $request
     * @return \App\MachinePartOrder
     */
    public function checkoutCart(Request $request)
    {
        $request->validate([
            'comment' => 'nullable|string',
            'ticket' => 'required'
        ]);
        
        if ($this->user()->cart->count()) {

            $order = $this->user()->machine_part_orders()->create([
                'comment' => $request->comment,
                'ticket_id' => $request->ticket
            ]);

            foreach ($this->user()->cart as $cart_item) {
                
                $order_item = $order->items()->create([
                    'part_id'    => $cart_item->part_id,
                    'quantity'   => $cart_item->quantity,
                    'cac_reason' => $cart_item->cac_reason,
                ]);

                $this->transferCartItemImages($cart_item, $order_item);
                $cart_item->delete(); //delete cart item
            }

            event(new NewMachinePartOrder($order));
            return $order;
        }

        return null;
    }

    /**
     * transfer images from cart item to order item
     * 
     * @param \App\MachinePartCart
     * @param \App\MachinePartOrderItem
     * @return null
     */
    private function transferCartItemImages(MachinePartCart $cartItem, MachinePartOrderItem $orderItem)
    {
        if ($cartItem->images->count()) {
            foreach ($cartItem->images as $image) {
                $image->update([
                    "imageable_type" => get_class($orderItem),
                    "imageable_id" => $orderItem->id,
                ]);
            }
        }
    }

    /**
     * get logged user
     */
    private function user()
    {
        return Auth::user();
    }

    /**
     * check if item is in cart
     * @param int $part_id
     * @return \App\MachinePartCart
     */
    protected function inCart($part_id)
    {
        $target = MachinePartCart::where('part_id', $part_id)
            ->where('user_id', $this->user()->id);
        if ($target->count()) {
            return $target->first();
        }
        return false;
    }
}