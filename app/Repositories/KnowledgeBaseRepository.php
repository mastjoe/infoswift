<?php 
 namespace App\Repositories;

use Storage; 
use App\Comment;
use App\Helpers\Util;
use App\KnowledgeBase;
use App\Helpers\Upload;
use Illuminate\Http\Request;
use App\KnowledgeBaseCategory;
use Illuminate\Support\Facades\Auth;

class KnowledgeBaseRepository
 {
     
    /**
     * get specific kb by reference
     * 
     * @param string $ref
     * @return \App\KnowledgeBase
     */
    public function findByRef($ref)
    {
        return KnowledgeBase::with(['category', 'likes', 'dislikes', 'comments'])
            ->where('ref', $ref)
            ->firstOrFail();
    }

    /**
     * get all kb categories
     * 
     * @return Collection
     */
    public function categories()
    {
        return KnowledgeBaseCategory::all();
    }

    /**
     * get specific number of recent posts
     * 
     * @param int $limit
     * @return Collection
     */
    public function getRecentPosts(int $limit = 5)
    {
        return KnowledgeBase::where('hidden', false)->orderBy('created_at', 'desc')
            ->take($limit)
            ->get();
    }

    /**
     * stores new post
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "category"       => "required",
            "title"          => "required|string|max:120",
            "content"        => "required",
            "commentable"    => "nullable",
            "reactable"      => "nullable",
            "hidden"         => "nullable",
            "featured_image" => "required|file|image|max:1024|mimes:png,jpg,gif,jpeg"
        ]);

        $category = KnowledgeBaseCategory::findOrFail($request->category);
        $post = $category->posts()->create([
            "title"          => $request->title,
            "content"        => $this->saveContentImages($request->content),
            "commentable"    => $request->commentable,
            "reactable"      => $request->reactable,
            "hidden"         => $request->hidden ?? false,
            "featured_image" => $this->uploadFeaturedImage($request),
        ]);

        // log into audit
        $post->audit_events()->create([
            "event" => "created",
            "description" => "new post, {$post->ref} with title, \"{$post->title}\" was created"
        ]);

        return $post;
                
    }

    /**
     * update kb post
     * 
     * @param \Illuminate\Http\Request $request
     * @param \App\KnowledgeBase
     * @return bool
     */
    public function update(Request $request, KnowledgeBase $kb)
    {
        $request->validate([
            "category"       => "required",
            "title"          => "required|string|max:120",
            "content"        => "required",
            "commentable"    => "nullable",
            "reactable"      => "nullable",
            "hidden"         => "nullable",
            "featured_image" => "nullable|file|image|max:1024|mimes:png,jpg,gif,jpeg"
        ]);

        $featured_image = $kb->featured_image;

        if ($request->featured_image) {
            Upload::deleteUploadedFile($kb->featured_image);
            $featured_image = $this->uploadFeaturedImage($request);
        }

        $kb->update([
            "category_id"    => $request->category,
            "title"          => $request->title,
            "content"        => $this->saveContentImages($request->content),
            "commentable"    => $request->commentable,
            "reactable"      => $request->reactable,
            "hidden"         => $request->hidden ?? false,
            "featured_image" => $featured_image
        ]);

        // log audit
        if ($kb->getDirty()) {
            $kb->audit_events()->create([
                "event"       => "updated",
                "description" => "post, {$kb->ref} was updated"
            ]);
        }
        return true;
    }

    /**
     * upload featured image
     * 
     * @param \Illuminate\Http\Request $request
     * @param string 
     */
    protected function uploadFeaturedImage(Request $request)
    {
        if ($request->hasFile('featured_image')) {
            $image = $request->featured_image;
            $extension = $image->extension();
            $path = "editors/featured/".uniqid().".$extension";
            Storage::put($path, file_get_contents($image));
            return "uploads/".$path;
        }
        return null;
    }


    /**
     * save content images to database for and replace image source with link
     * 
     * @param string $content
     * @return string
     */
    protected function saveContentImages($content)
    {
        libxml_use_internal_errors(true);
        $dom = new \DOMdocument();
        $dom->loadHtml($content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $images = $dom->getElementsByTagName('img');
        
        foreach ($images as $key => $image) {
            $src = $image->getAttribute('src');
            if (preg_match('/data:image/', $src)) {
                preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                $mimeType = $groups['mime'];
                $path = 'editors/' . uniqid().'.'.$mimeType;
                Storage::disk('uploads')->put($path, file_get_contents($src));
                $image->removeAttribute('src');
                $image->setAttribute('src', asset("uploads/".$path));
            }
        }
        return $dom->saveHTML();
    }

    /**
     * get kb post details
     * 
     * @param string $ref
     * @return \App\KnowledgeBase
     */
    public function readPost($ref)
    {
        $post = $this->findByRef($ref);
        $post->increment("read_count");
        return $post;
    }

    /**
     * get post details as json
     * 
     * @param string $ref
     * @return
     */
    public function postJson($ref)
    {
        $post = $this->findByRef($ref);
        $this->mapLikersAndDislikers($post);
        return $post;
    }

    /**
     * likes a post
     * 
     * @param string $ref
     * @return \App\KnowledgeBase
     */
    public function likePost($ref)
    {
        $post = $this->findByRef($ref);
        // if has a dislike, remove before liking
        $user = Auth::user();
        $dislikers_ids = $post->dislikes->pluck('user_id')->toArray();
        $likers_ids = $post->likes->pluck('user_id')->toArray();

        if (in_array($user->id, $dislikers_ids)) {
            $post->dislikes->where('user_id', $user->id)->first()->delete();
        }
        if (!in_array($user->id, $likers_ids)) {
            $post->likes()->create(["user_id"=>$user->id]);
        }

        return $post;
    }

    /**
     * dislikes a post
     * 
     * @param string $ref
     * @return \App\KnowledgeBase
     */
    public function dislikePost($ref)
    {
        $post = $this->findByRef($ref);
        $user = Auth::user();
        $likers_ids = $post->likes->pluck('user_id')->toArray();
        $dislikers_ids = $post->dislikes->pluck('user_id')->toArray();

        if (in_array($user->id, $likers_ids)) {
            $post->likes->where('user_id', $user->id)->first()->delete();
        }

        if (!in_array($user->id, $dislikers_ids)){
            $post->dislikes()->create(["user_id" => $user->id]);
        }

        return $post;
    }

    /**
     * get dislikers and likers list
     */
    protected function mapLikersAndDislikers(KnowledgeBase $kb)
    {
        $you_like = false;
        $you_dislike = false;
        $user = Auth::user();

        $likers_ids = $kb->likes->pluck('user_id')->toArray();
        $dislikers_ids = $kb->dislikes->pluck('user_id')->toArray();

        if (in_array($user->id, $likers_ids)) { $you_like = true; }
        if (in_array($user->id, $dislikers_ids)) { $you_dislike = true; }


        $likers = $kb->likes->map(function($like) {
            return $like->user->full_name();
        });

        $dislikers = $kb->dislikes->map(function($dislike) {
            return $dislike->user->full_name();
        });

        $kb->setAttribute('likers', $likers);
        $kb->setAttribute('dislikers', $dislikers);
        $kb->setAttribute('you_dislike', $you_dislike);
        $kb->setAttribute('you_like', $you_like);
    }

    /**
     * make comment on a kb post
     * 
     * @param \Illuminate\Http\Request $request
     * @param string $ref
     * @return \App\KnowledgeBase
     */
    public function makeCommentOnPost(Request $request, $ref)
    {
        $request->validate([
            "comment" => "required|string"
        ]);

        $post = $this->findByRef($ref);
        $comment = $post->comments()->create($request->only('comment'));

        // log into audit
        $post->audit_events()->create([
            "event"       => "created",
            "description" => "comment was made on post, {$post->ref} by {$comment->user->full_name()}"
        ]);

        return $post;
    }

    /**
     * get all comments to a kb
     * 
     * @param string $ref
     * @return \App\KnowledgeBase
     */
    public function getPostComments($ref)
    {
        $post = $this->findByRef($ref);
        $post->comments->map(function($p) {
            $p->setAttribute('diff_for_humans', $p->created_at->diffForHumans());
            $p->user->setAttribute('avatar', $p->user->avatar());
            return $p->user;
        });

        return $post;
    }

    /**
     * delete a kb post
     * 
     * @param \App\KnowledgeBase
     * @return bool
     */
    public function deletePost(KnowledgeBase $kb)
    {
        if ($kb->deletable()) {
            // audit log
            $kb->audit_events()->create([
                "event"       => "deleted",
                "description" => 
                "knowledge base, {$kb->ref} with title, {$kb->title} was deleted",
            ]);

            Util::auditNullify('KnowledgeBase', $kb->id);
            $kb->likes()->delete();
            $kb->dislikes()->delete();
            $kb->delete();

            // delete uploaded file
            Upload::deleteUploadedFile($kb->featured_image);
            return true;
        }
        return false;
    }

    /**
     * delete specific post's comment
     * 
     * @param \App\KnowledgeBase
     * @param int $id
     * @return bool
     */
    public function deletePostComment(KnowledgeBase $kb, $id)
    {
        $comment = Comment::with(['user', 'likes', 'dislikes'])->findOrFail($id);
        $comment->dislikes()->delete();
        $comment->likes()->delete();
        
        // log into audit
        $kb->audit_events()->create([
            "event"       => "deleted",
            "description" => "deleted a comment made by {$comment->user->full_name} on post {$kb->ref}"
        ]);

        $comment->delete();
        return true;
    }

    /**
     * update a post comment
     * 
     * @param \Illuminate\Http\Request $request
     * @param \App\KnowledgeBase
     * @param int $comment_id
     * @return bool
     */
    public function updatePostComment(Request $request, KnowledgeBase $kb, $comment_id)
    {
        $request->validate([
            "comment" => "required"
        ]);

        $comment = Comment::findOrFail($comment_id);
        if ($comment->can_edit()) {
            $comment->update(["comment" => $request->comment]);
            return true;
        }
        return false;
    }

    /**
     * get details of specific category
     * 
     * @param string $slug
     * @return \App\KnowledgeBaseCategory
     */
    public function getCategory($slug)
    {
        return KnowledgeBaseCategory::with(['posts'])->where('slug', $slug)->firstOrFail();
    }

    /**
     * creates a new kb category
     * 
     * @param \Illuminate\Http\Request $request
     * @return \App\KnowledgeBaseCategory
     */
    public function createNewCategory(Request $request)
    {
        $request->validate([
            "category"    => "required|unique:knowledge_base_categories,category",
            "slug"        => "required|string",
            "description" => "nullable|string"
        ]);

        $category = KnowledgeBaseCategory::create($request->all());
        // log audit
        $category->audit_events()->create([
            "event"       => "created",
            "description" => "knowlegde base category {$category->category} was created"
        ]);

        return $category;
    }

    /**
     * delete specific category
     * @param \App\KnowledgeBaseCategory
     * @return bool
     */
    public function deleteCategory(KnowledgeBaseCategory $category)
    {
        if ($category->deletable()) {
            // log audit
            $category->audit_events()->create([
                "event"       => "deleted",
                "description" => "Knowledge base category, {$category->category} was deleted"
            ]);
            Util::auditNullify('KnowledgeBaseCategory', $category->id);
            
            $category->delete();
            return true;
        }
        return false;
    }

    /**
     * update category
     * 
     * @param \Illuminate\Http\Request $request
     * @param string $slug
     */
    public function updateCategory(Request $request, $slug)
    {
        $request->validate([
            "category"    => "required|unique:knowledge_base_categories,category,".$request->category.",category",
            "slug"        => "required|string",
            "description" => "nullable|string"
        ]);

        $category = $this->getCategory($slug);
        $category->update([
            "category"    => $request->category,
            "slug"        => $request->slug,
            "description" => $request->description
        ]);

        // log audit
        if ($category->getDirty()) {

            $category->audit_events()->create([
                "event"       => "updated",
                "description" => "updated knowledge base category, {$category->category}"
            ]);
        }
        return $category;
    }
 }
