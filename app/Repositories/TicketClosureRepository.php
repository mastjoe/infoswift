<?php

namespace App\Repositories;

use App\Ticket;
use Carbon\Carbon;
use App\TicketFault;
use App\Helpers\Util;
use App\Events\CloseTicket;
use Illuminate\Http\Request;
use App\Events\DeclineTicket;
use App\Events\ConfirmCloseTicket;
use Illuminate\Support\Facades\Auth;

class TicketClosureRepository
{
    /**
     * close ticket by engineer
     * 
     * @param \Illuminate\Http\Request $request
     * @param \App\Ticket $ticket
     * @return \App\Ticket
     */
    public function closeTicket(Request $request, Ticket $ticket)
    {
        $request->validate([
            'solutions.*' => 'required|min:1',
            'comment'     => 'nullable|string'
        ], [
            'solutions.*' => 'Provide solution to terminal fault(s)',
        ]);

        if (!$this->hasSla($ticket)) {
            Util::pushError([
                'sla' => 'No Sla timing found! Kindly contact admin'
            ]);
        }

        // store solutions
        $this->resolveTicketFaults($request, $ticket->id);

        $ticket->closed_by      = Auth::user()->id;
        $ticket->closed_at      = Carbon::now();
        $ticket->closure_comment = $request->comment;
        $ticket->status               = "closed";
        $ticket->save();

         // trigger ticket close event
        event(new CloseTicket($ticket));
        
        return $ticket;
    }

    /**
     * appends solutions to terminal faults
     */
    protected function resolveTicketFaults(Request $request, $id)
    {
        if (count($request->solutions)) {

            foreach ($request->solutions as $key => $solution) {
                $target = TicketFault::where('ticket_id', $id)
                    ->where('fault_id', $key)
                    ->first();
                $target->solution = $solution;
                $target->save();
            }
        }
    }

    /**
     * get sla timing for a ticket
     * @param \App\Ticket $ticket
     * @return object
     */
    private function getSla(Ticket $ticket)
    {
        return $ticket->state->slas->where('client_id', $ticket->client_id)->first();
    }


    /**
     * checks if a ticket has sla for computation
     * @param \App\Ticket $ticket
     * @return bool
     */
    private function hasSla(Ticket $ticket)
    {
        if ($this->getSla($ticket)) {
            return true;
        }
        return false;
    }

    /**
     * computes the sla rating for specific ticket
     * 
     * @param \App\Ticket $ticket
     */
    protected function computeSlaRating(Ticket $ticket)
    {
        $sla = $this->getSla($ticket);

        $ticket_best_time = $ticket->created_at->addHours($sla->best_hour);
        $ticket_moderate_time = $ticket->created_at->addHours($sla->moderate_hour);
        $current_time = Carbon::now();

        if ($current_time->lessThanOrEqualTo($ticket_best_time)) {
            $rating = 3;
        } else if (
            $current_time->greaterThan($ticket_best_time) &&
            $current_time->lessThanOrEqualTo($ticket_moderate_time)
        ) {
            $rating = 2;
        } else {
            $rating = 1;
        }
        $ticket->update(['sla_rating' => $rating]);
    }

    /**
     * reclose a declined ticket
     * 
     * @param \Illuminate\Http\Request $request
     * @param \App\Ticket
     * @return \App\Ticket
     */
    public function reCloseTicket(Request $request, Ticket $ticket)
    {
        $request->validate([
            'solutions.*' => 'required|min:1',
            'comment'     => 'nullable|string'
        ], [
            'solutions.*' => 'Provide solution to terminal fault(s)',
        ]);

        $ticket->update([
            'closure_comment' => $request->comment,
            'declined_at'     => null,
            'status'          => 'closed'
        ]);

        $this->resolveTicketFaults($request, $ticket->id);
        
        event(new CloseTicket($ticket));
        return $ticket;
    }

    /**
     * the approval of the closure of a ticket to resolution
     * 
     * @param \Illuminate\Http\Request $request
     * @param \App\Ticket $ticket
     * @return \App\Ticket
     */
    public function approveTicketClosure(Request $request, Ticket $ticket)
    {
        $request->validate([
            'comment' => 'nullable|string',
            'rating'  => 'required'
        ]);

        $ticket->update([
            'confirmed_at'         => Carbon::now(),
            'confirmed_by'         => Auth::user()->id,
            'confirmation_Comment' => $request->comment,
            'status'               => 'resolved',
            'declined_at'          => null,
            'rating'               => $request->rating
        ]);

        $ticket->audit_events()->create([
            'event'       => 'updated',
            'description' => 'ticket closure was confirmed'
        ]);

        event(new ConfirmCloseTicket($ticket));
        return $ticket;
    }

    /**
     * decline or disapprove ticket closure
     * 
     * @param \Illuminate\Http\Request $request
     * @param \App\Ticket
     * @return \App\Ticket
     */
    public function disapproveTicketClosure(Request $request, Ticket $ticket)
    {
        $request->validate([
            'comment' => 'nullable|string'
        ]);
        
        $ticket->update([
            'declination_comment' => $request->comment,
            'declined_at'         => Carbon::now(),
            'declined_by'         => Auth::user()->id,
            'status'              => 'declined'
        ]);

        $ticket->audit_events()->create([
            'event'       => 'updated',
            'description' => 'ticket closure was declined'
        ]);

        // fire event for ticket declination
        event(new DeclineTicket($ticket));
        return $ticket;
    }

}