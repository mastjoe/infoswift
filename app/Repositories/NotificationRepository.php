<?php

namespace App\Repositories;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Notifications\DatabaseNotificationCollection;

class NotificationRepository {

    /**
     * get logged in user
     */
    protected function user()
    {
        return Auth::user();
    }

    /**
     * read notifications
     * 
     * @param string $id
     * @return DatabaseNotificationCollection
     */
    public function readNotification($id)
    {
        $notification = $this->user()->notifications->where('id', $id)->first();
        $notification->update(["read_at" => Carbon::now()]);
        return $notification;
    }

    /**
     * checks if notification id exist
     * 
     * @param string $id
     * @return bool
     */
    public function idExists($id)
    {
        return $this->user()->notifications->where('id', $id)->count();
    }

    /**
     * delete notifications
     * 
     * @param string $id
     * @return bool
     */
    public function deleteNotification($id)
    {
        $notification = $this->user()->notifications()->where('id', $id)->first();
        if ($notification->delete()) {
            return true;
        }
        return false;        
    }

    /**
     * gets user unread notifications
     * 
     * @return DatabaseNotificationCollection
     */
    public function getUnreadNotifications()
    {
        return $this->user()->notifications()->orderBy('created_at', 'desc')
        ->whereNull('read_at')->get();
    }

}