<?php
namespace App\Repositories;

use App\Branch;
use App\Machine;
use App\Helpers\Util;
use Illuminate\Http\Request;

class MachineRepository
{
    /**
     * create new machine record
     * 
     * @param \Illuminate\Http\Request $request
     * @return \App\Machine
     */
    public function create(Request $request)
    {
        $request->validate([
            'name'           => 'nullable|string',
            'terminal_id'    => 'required|string|unique:machines,terminal_id',
            'branch_status'  => 'required',
            'model_number'   => 'nullable|string',
            'serial_number'  => 'nullable|string',
            'ip_address'     => 'nullable|ip',
            'client'         => 'required',
            'branch'         => 'required_if:branch_status,==,inbranch',
            'address'        => 'nullable|string',
            'machine_type'   => 'required',
            'machine_status' => 'required',
            'machine_vendor' => 'required'
        ]);

        $branch  = Branch::findOrFail($request->branch);
        $machine = $branch->machines()->create([
            'terminal_id'       => $request->terminal_id,
            'branch_status'     => $request->branch_status,
            'model_number'      => $request->model_number,
            'serial_number'     => $request->serial_number,
            'ip_address'        => $request->ip_address,
            'client_id'         => $branch->client_id,
            'region_id'         => $branch->region_id,
            'location'          => $request->address,
            'machine_type_id'   => $request->machine_type,
            'machine_vendor_id' => $request->machine_vendor,
            'machine_status_id' => $request->machine_status,
            'is_pm'             => true,
            'name'              => $request->name
        ]);

        return $machine;
    }

    /**
     * assign engineer to a machine
     * 
     * @param \Illuminate\Http\Request
     * @return \App\Machine
     */
    public function assignEngineerToMachine(Request $request, Machine $machine)
    {
        $request->validate([
            'engineer' => 'required',
        ]);

        if ($machine->engineer_id == $request->engineer) {
            Util::pushError(['engineer' => 'engineer already assigned to machine']);
        }

        $machine->update(["engineer_id" => $request->engineer]);
        // capture audit
        $machine->audit_events()->create([
            "event" => "updated",
            "description" => "machine, {$machine->terminal_id} successfully assigned to engineer {$machine->engineer->full_name()}"
        ]);

        return  $machine;
    }


}