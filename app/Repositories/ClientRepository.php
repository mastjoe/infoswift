<?php

namespace App\Repositories;

use Str;
use App\Client;
use App\Helpers\Util;
use App\Helpers\Photo;
use Illuminate\Http\Request;
use App\Notifications\ClientPasswordResetNotification;

class ClientRepository
{
    /**
     * creat new client
     * 
     * @param \Illuminate\Http\Request $request
     * @return \App\Client
     */
    public function create(Request $request)
    {
        $request->validate([
            'name'       => 'required|unique:clients,name',
            'short_name' => 'required|unique:clients,short_name',
            'code'       => 'required|unique:clients,code|max:10',
            'color'      => 'required|unique:clients,color',
            'email'      => 'required|unique:clients,email',
            'address'    => 'nullable|string',
            'website'    => 'nullable|url'
        ]);

        $client = Client::create([
            'name'       => $request->name,
            'short_name' => $request->short_name,
            'code'       => $request->code,
            'color'      => $request->color,
            'email'      => $request->email,
            'address'    => $request->address,
            'website'    => $request->website,
            'password'   => 'password'
        ]);
        return $client;
    }

    /**
     * update client record
     * 
     * @param \Illuminate\Http\Request
     * @param int $id
     * @return \App\Client
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'       => 'required|unique:clients,name,'.$request->name.',name',
            'short_name' => 'required|unique:clients,short_name,'.$request->short_name.',short_name',
            'code'       => 'required|unique:clients,code,'.$request->code.',code|max:10',
            'color'      => 'required|unique:clients,color,'.$request->color.',color',
            'email'      => 'required|unique:clients,email,'.$request->email.',email',
            'address'    => 'nullable|string',
            'website'    => 'nullable|url'
        ]);

        $client             = Client::findOrFail($id);
        $client->name       = $request->name;
        $client->short_name = $request->short_name;
        $client->code       = $request->code;
        $client->color      = $request->color;
        $client->email      = $request->email;
        $client->address    = $request->address;
        $client->website    = $request->website;

        $client->save();
        return $client;
    }

    /**
     * delete client record
     * 
     * @param \App\Client
     * @return bool
     */
    public function delete(Client $client)
    {
        if ($client->deletable()) {
            if ($client->delete()) {
                return true;
            }
        }
        return false;
    }

    /**
     * add technical personeel
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \App\Client
     */
    public function updatePersonnel(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|string',
            'phone' => 'required|digits_between:6,15',
            'email' => 'required|email'
        ]);

        $client                  = Client::findOrFail($id);
        $client->personnel_name  = $request->name;
        $client->personnel_phone = $request->phone;
        $client->personnel_email = $request->email;
        $client->save();

        if ($client->getDirty()) {            
            $client->audit_events()->create([
                'event'       => 'updated',
                'description' => 'client, '.$client->short_name.'\'s personnel updated to '.$client->personnel_name
            ]);
        }

        return $client;
    }

    /**
     * update client logo
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \App\Client
     */
    public function updateLogo($request, $id)
    {
        $request->validate([
            'logo' => 'required|image|mimes:png,jpeg,svg,gif|max:500',
        ]);

        $client = Client::findOrFail($id);
        $upload = Photo::uploadClientLogo($request, $id);

        $client->audit_events()->create([
            'event'       => 'updated',
            'description' => 'updated logo for client, '.$client->short_name
        ]);

        if ($upload) { return $client; }
        return false;
    }

    /**
     * update supervisor for specific client
     * 
     * @param \Illuminate\Http\Request $request
     * @param  int $id
     * @return \App\Client
     */
    public function updateSupervisor(Request $request, $id)
    {
        $request->validate([
            'supervisor' => 'required'
        ]);

        $client = Client::findOrFail($id);

        if ($client->supervisor_id == $request->supervisor) {
            Util::pushError(['supervisor' => 'already assigned']);
        }

        $client->update([
            'supervisor_id' => $request->supervisor
        ]);

        return $client;
    }

    /**
     * reset client password
     * 
     * @param \App\Client
     * @return string $password
     */
    public function resetPassword(Client $client)
    {
        $password = Str::random(7);
        $hashed_password = \bcrypt($password);

        $client->update(['password' => $hashed_password]);
        // notify client
        $client->notify(new ClientPasswordResetNotification($client, $password));
        return $password;
    }

}