<?php

namespace App\Repositories;

use App\Client;
use App\Ticket;
use App\Machine;
use App\Helpers\Util;
use App\MachineFault;
use App\Helpers\Upload;
use App\Events\NewTicket;
use App\TicketEscalation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Notifications\EngineerEscalatedTicketNotification;

class TicketRepository
{
    /**
     * find ticket by ref
     * 
     * @param string $ref
     * @return \App\Ticket
     */
    public function findRef(string $ref)
    {
        return Ticket::with([
            'machine',
            'closer',
            'confirmer',
            'documents',
            'field_report_form',
            'logs',
        ])
            ->where('ref', $ref)->firstOrFail();
    }

    /**
     * create new ticket for specific client
     * 
     * @param \Illuminate\Http\Request $request
     * @return \App\Ticket
     */
    public function create(Request $request)
    {
        $request->validate([
            'client'   => 'required',
            'machine'  => 'required',
            'faults.*' => 'required|min:1'
        ]);

        $machine = Machine::findOrFail($request->machine);
        $client  = Client::findOrFail($request->client);

        if ($machine->hasUnresolvedTickets()) {
            Util::pushError(["machine" => "machine, {$machine->terminal_id} has an unresolved ticket"]);
        }

        $ticket = $client->tickets()->create([
            'machine_id' => $request->machine,
        ]);

        foreach ($request->faults as $fault) {
            $ticket->faults()->create([
                'fault_id' => $this->resolveNewFault($fault)
            ]);
        }

        event(new NewTicket($ticket));

        return $ticket;
    }

    /**
     * all CE closed and unattended tickets
     * 
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function closedTickets()
    {
        return Ticket::closedTickets()->get();
    }

    /**
     * get fault id if exist or create new fault and get id
     * 
     * @param string $fault
     * @return int $fualt_id
     */
    protected function resolveNewFault($fault): int
    {
        $target = MachineFault::where('id', $fault);

        if ($target->count()) {
            return $target->first()->id;
        }

        return MachineFault::create([
            'fault' => $fault,
            'new'   => true
        ])->id;
    }

    /**
     * upload field report for a ticket
     * 
     * @param \Illuminate\Http\Request $request
     * @param \App\Ticket $ticket
     * @return \App\Ticket
     */
    public function uploadFieldReportForm(Request $request, Ticket $ticket)
    {
        $request->validate([
            'document' => 'required|file|mimes:png,jpg,jpeg,gif,pdf,doc,docx|max:500'
        ]);
        
        $frf = Upload::uploadTicketDocument($request, $ticket, "frf");

        return $ticket;
    }

    /**
     * upload job completion form
     * 
     * @param \Illuminate\Http\Request $request
     * @param \App\Ticket $ticket
     * @return \App\Ticket
     */
    public function uploadJobCompletionForm(Request $request, Ticket $ticket)
    {
        $request->validate([
            'document' => 'required|file|mimes:png,jpg,jpeg,gif,pdf,doc,docx|max:500'
        ]);
        $frf = Upload::uploadTicketDocument($request, $ticket, "jcf");
        return $ticket;
    }


    /**
     *  escalate ticket
     * 
     * @param Illuminate\Http\Request $request
     * @param \App\Ticket
     * @return \App\TicketEscalation
     */
    public function escalateTicket(Request $request, $ticket)
    {
        $request->validate([
            'escalate_to' => 'required',
            'reason'      => 'nullable|string'
        ]);

        $escalation = $ticket->escalations()->create([
            'escalated_to' => $request->escalate_to,
            'reason'       => $request->reason
        ]);

        // change ticket status to "escalated"...
        $ticket->update(['status' => 'escalated']);

        // capture in audit
        $ticket->audit_events()->create([
            'event'       => 'created',
            'description' => "ticket escalated to {$escalation->escalatedTo->full_name()}"
        ]);

        // notify whom ticket was escalated to
        $escalated_to = $ticket->lastEscalatedTo();
        $escalated_to->notify(new EngineerEscalatedTicketNotification($ticket));

        return $escalation;
    }


    /**
     * get escalation details
     * 
     * @param int $escalation_id
     * @return  \App\TicketEscalation
     */
    public function getEscalation($escalation_id)
    {
        return TicketEscalation::with([
            'escalator',
            'escalatedTo',
            'escalatedFrom'
            ])->findOrFail($escalation_id);
    }

    /**
     * make a new log on ticket
     * 
     * @param \Illuminate\Http\Request $request
     * @param \App\Ticket $ticket
     * @return App\TicketLog
     */
    public function createTicketLog(Request $request, $ticket)
    {
        $request->validate([
            'message' => 'required',
        ]);
        
        // esnure log can be made on ticket
        if (!$ticket->can_log()) {
            Util::pushError(["message" => "log is prohibited"]);
        }

        $user_id = $client_id = null;

        if (Auth::guard("client")->check()) {
            $client_id = Auth::user()->id;
        } else {
            $user_id = Auth::user()->id;
        }    

        $log = $ticket->logs()->create([
            'client_id' => $client_id,
            'user_id'   => $user_id,
            'message'   => Util::processEditorImage($request->message)
        ]);

        // add to event audit
        $ticket->audit_events()->create([
            "event"       => "create",
            "description" => "a new log was made on ticket, {$ticket->ref}"
        ]);

        return $log;
    }

    
}