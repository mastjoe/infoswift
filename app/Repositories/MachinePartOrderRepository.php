<?php
namespace App\Repositories;

use Carbon\Carbon;
use App\Helpers\Util;
use App\MachinePartOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Events\DeclineMachinePartOrder;
use App\MachinePartCart;
use App\MachinePartDispatch;

class MachinePartOrderRepository
{

    /**
     * get specific order by reference
     * 
     * @param string $ref
     * @return \App\MachinePartOrder
     */
    public function findRef(string $ref)
    {
        return MachinePartOrder::with(['items', 'ticket', 'dispatches', 'user'])
            ->where('ref', $ref)->firstOrFail();
    }

    /**
     * alias to findRef
     * @param string $ref
     * @return \App\MachinePartOrder
     */
    public function getOrderByRef(string $ref)
    {
        return $this->findRef($ref);
    }

    /**
     * decline machine part order
     * 
     * @param \Illuminate\Http\Request $request
     * @param \App\MachinePartOrder
     * @return bool
     */
    public function declineOrder(Request $request, $order)
    {
        $request->validate([
            'remark' => 'nullable|string',
        ]);

        if (!$order->can_decline()) {
            Util::pushError(['error' => 'cannot decline order']);
        }

        $order->update(["declination_remark" => $request->remark]);

        // in declining part order, order is deleted, and order items is returned to engineer's cart
        $this->transferOrderItemsToCart($order);

        // log audit
        $order->audit_events()->create([
            "event"       => "updated",
            "description" => "order {$order->ref} was declined",
        ]);

        event(new DeclineMachinePartOrder($order));
        $order->delete();

        return true;
    }

    /**
     * transfer order items to engineer's cart
     * 
     * @param \App\MachinePartOrder
     * @return 
     */
    protected function transferOrderItemsToCart(MachinePartOrder $order)
    {
        if ($order->items->count()) {
            foreach ($order->items as $item) {
                $in_cart_target = MachinePartCart::where('user_id', $order->user_id)
                    ->where('part_id', $item->part_id);

                if ($in_cart_target->count()) {
                    $in_cart_target->first()->update(["quantity" => $item->quantity]);
                    $cartitem = $in_cart_target->first();
                } else {
                    $cartitem = $order->user->cart()->create([
                        "part_id"    => $item->part_id,
                        "quantity"   => $item->quantity,
                        "cac_reason" => $item->cac_reason
                    ]);
                }

                // check if order item has image, transfer to cart item
                if ($item->images->count()) {
                    foreach ($item->images as $image) {
                        $image->update([
                            "imageable_type" => get_class($cartitem),
                            "imageable_id"   => $cartitem->id
                        ]);
                    }
                }

                // delete each order item
                $item->delete();
            }
        }
    }

    /**
     * notify client about cac item
     * 
     * @param \App\MachinePartOrderItem
     * @return 
     */
    public function notifyClientCAC()
    {

    }

    /**
     * get dispatch details by reference
     * 
     * @param string $ref
     * @return \App\MachinePartDispatch
     */
    public function getDispatchByRef(string $ref)
    {
        return MachinePartDispatch::with(["order", "dispatcher", "receiver", "creator"])
            ->where('ref', $ref)
            ->firstOrFail();
    }


    /**
     * deletes a dispatch session
     * 
     * @param \App\MachinePartDispatch $dispatch
     * @return bool
     */
    public function deleteOrderDispatch(MachinePartDispatch $dispatch)
    {
        if ($dispatch->deletable()) {
            $dispatch->delete();
            return true;
        }
        return false;
    }

    /**
     * create dispatch session for order
     * 
     * @param \App\MachinePartOrder $order
     * @return \App\MachinePartDispatch $dispatch
     */
    public function createOrderDispatch(MachinePartOrder $order)
    {
        $dispatch =  $order->dispatches()->create([
            "status" => "open"
        ]);

        return $dispatch;
    }
}