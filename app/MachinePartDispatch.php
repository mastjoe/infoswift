<?php

namespace App;

use App\Helpers\Util;
use Illuminate\Database\Eloquent\Model;

class MachinePartDispatch extends Model
{
    //
    protected $fillable = [
        'ref',
        'order_id',
        'status',
        'dispatched_by',
        'dispatch_comment',
        'dispatch_company',
        'dispatch_method',
        'dispatch_identifier',
        'receiver_comment',
        'received_at',
        'received_by',
        'created_by',
    ];


    public function items()
    {
        return $this->hasMany('App\MachinePartDispatchItem', 'dispatch_id');
    }

    public function order()
    {
        return $this->belongsTo('App\MachinePartOrder', 'order_id');
    }

    public function dispatcher()
    {
        return $this->belongsTo('App\User', 'dispatched_by');
    }

    public function receiver()
    {
        return $this->belongsTo('App\User',  'received_by');
    }

    public function creator()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    /**
     * checks if model can be deleted
     */
    public function deletable()
    {
        if ($this->items->count()) {
            return false;
        }
        return true;
    }
}
