<?php

namespace App;

use Auth;
use App\Helpers\Util;
use Illuminate\Database\Eloquent\Model;

class Pm extends Model
{
    //
    protected $guarded = [];

    protected $casts = [
        'year' => 'integer'
    ];

    protected $dates = [
        'confirmed_at',
        'declined_at'
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function($model) {
            $model->ref = Util::genPmRef();
            $model->done_by = Auth::user()->id;
            $model->status = "sent";
        });
    }


    public function audit_events()
    {
        return $this->morphMany('App\AuditEvent', 'auditable');
    }


    public function attachment()
    {
        return $this->morphOne('App\Attachment', 'attachable');
    }

    public function machine()
    {
        return $this->belongsTo('App\Machine');
    }

    public function doneBy()
    {
        return $this->belongsTo('App\User', 'done_by');
    }

    public function confirmedBy()
    {
        return $this->belongsTo('App\User', 'confirmed_by');
    }

    public function deletable()
    {
        if ($this->confirmed_at || $this->declined_at) {
            return false;
        }
        return true;
    }

    public function editable()
    {
        if ($this->confirmed_at) {
            return false;
        }
        return true;
    }
}
