<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Client extends Authenticatable
{
    //
    use Notifiable;

    protected $guard = "client";
    
    protected $fillable = [
        'code',
        'name',
        'short_name',
        'color',
        'email',
        'website',
        'address',
        'password',
        'personnel_name',
        'personnel_phone',
        'personnel_email',
        'email_verified_at',
        'last_change_password',
        'current_login',
        'last_login',
        'suspended',
        'suspended_at',
        'logo',
        'supervisor_id'
    ];

    protected $hidden = [
        'password',
        'remember_token'
    ];

    protected $dates = [
        'current_login',
        'last_login',
        'last_change_password'
    ];

    public function setCodeAttribute($value)
    {
        $this->attributes['code'] = strtoupper($value);
    }

    public function setEmailAttribute($value)
    {
        $this->attributes['email'] = strtolower($value);
    }
    
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = \bcrypt($value);
    }

    public function audit_events()
    {
        return $this->morphMany('App\AuditEvent', 'auditable');
    }

    public function logo()
    {
        if ($this->logo) {
            return asset($this->logo);
        }
        return asset('images/clientLogo.png');
    }

    public function deletable()
    {
        if (
            $this->machines->count() ||
            $this->branches->count() || 
            $this->regions->count()
        ) {
            return false;
        }
        return true;
    }

    public function machines()
    {
        return $this->hasMany('App\Machine');
    }

    public function pms()
    {
        return $this->hasMany('App\Pm');
    }

    /**
     * get client supervisor
     */
    public function supervisor()
    {
        return $this->belongsTo('App\User', 'supervisor_id');
    }

    /**
     * get pms for specific year
     */
    public function yearPms(int $year)
    {
        return $this->pms->where('year', $year);
    }

    public function branches()
    {
        return $this->hasMany('App\Branch');
    }

    public function regions()
    {
        return $this->hasMany('App\Region');
    }

    public function tickets()
    {
        return $this->hasMany('App\Ticket');
    }

    public function open_tickets()
    {
        return $this->tickets()->where('status', 'open');
    }

    public function slas()
    {
        return $this->hasMany('App\Sla');
    }
}
