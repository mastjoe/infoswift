<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sla extends Model
{
    //
    protected $guarded = [];
    protected $dates = [
        'start_time',
        'stop_time'
    ];

    // protected $casts = [
    //     'start_time' => 'datetime: h'
    // ];

    public function state()
    {
        return $this->belongsTo('App\CountryState', 'state_id');
    }

    public function client()
    {
        return $this->belongsTo('App\Client', 'client_id');
    }
}
