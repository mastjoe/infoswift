<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //
    protected $fillable = [
        'user_id',
        'comment',
        'commentable_type',
        'commentable_id',
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function($model) {
            $model->user_id = Auth::user()->id;
        });
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function commentable()
    {
        return $this->morphTo();
    }

    public function likes()
    {
        return $this->morphMany('App\Like', 'likable');
    }

    public function dislikes()
    {
        return $this->morphMany('App\Dislike', 'dislikable');
    }

    public function can_edit()
    {
        if (Auth::user()->is($this->user)) {
            return true;
        }
        return false;
    }
}
