<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class TicketLog extends Model
{
    //
    protected $fillable = [
        'ticket_id',
        'client_id',
        'user_id',
        'message',
        'edited',
        'reply_to'
    ];

    public function ticket()
    {
        return $this->belongsTo('App\Ticket');
    }

    public function client()
    {
        return $this->belongsTo('App\Client', 'client_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function reply() {
        return $this->hasOne('App\TicketLog', 'reply_to');
    }

    public function author()
    {
        if ($this->client_id){
            return $this->client;
        }
        return $this->user;
    }

    public function author_avatar()
    {
        if ($this->client_id) {
            return $this->author()->logo();
        } else {
            return $this->author()->avatar();
        }
        
    }

    public function isYou()
    {
        return Auth::user()->is($this->author());
    }
}
