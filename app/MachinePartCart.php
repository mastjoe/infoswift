<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MachinePartCart extends Model
{
    //
    protected $fillable = [
        'user_id',
        'part_id',
        'quantity',
        'cac_reason'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function machine_part()
    {
        return $this->belongsTo('App\MachinePart', 'part_id');
    }

    public function images()
    {
        return $this->morphMany('App\Image', 'imageable');
    }
}
