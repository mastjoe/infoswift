<?php

namespace App\Notifications;

use App\Ticket;
use App\Helpers\SMS;
use App\Channels\CustomSMS;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class EngineerEscalatedTicketNotification extends Notification
{
    use Queueable;

    public $ticket;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Ticket $ticket)
    {
        //
        $this->ticket = $ticket;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', CustomSMS::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    public function toCustomSMS($notifiable)
    {
        $ticket = $this->ticket;
        
        SMS::send([
            "to"      => $notifiable->phone,
            "message" => "ticket {$ticket->ref} was escalated to you on terminal {$ticket->machine->terminal_id}"
        ]);
    }

    public function toDatabase($notifiable)
    {
        return [
            "category" => "ticket",
            "message" => "ticket, {$this->ticket->ref} was escalated to you on terminal {$this->ticket->machine->terminal_id}"
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
