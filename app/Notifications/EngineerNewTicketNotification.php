<?php

namespace App\Notifications;

use App\Ticket;
use App\Helpers\SMS;
use App\Channels\CustomSMS;
use App\Mail\EngineerNewTicketMail;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class EngineerNewTicketNotification extends Notification
{
    use Queueable;

    public $ticket;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Ticket $ticket)
    {
        //
        $this->ticket = $ticket;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', CustomSMS::class, 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new EngineerNewTicketMail($this->ticket));
    }

    public function toCustomSMS($notifiable)
    {
        $ticket = $this->ticket;
        SMS::send([
            "to"      => $ticket->machine->engineer->phone,
            "message" => "new ticket {$ticket->ref} raised for faults on terminal {$ticket->machine->terminal_id}"
        ]);
    }

    public function toDatabase($notifiable)
    {
        $ticket = $this->ticket;
        return [
            "category" => "ticket",
            "message" => "New ticket {$ticket->ref} raised for machine, {$ticket->machine->terminal_id} in branch, {$ticket->machine->branch->branch}"
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
