<?php

namespace App\Notifications;

use App\Client;
use Illuminate\Bus\Queueable;
use App\Mail\ClientResetPasswordMail;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ClientPasswordResetNotification extends Notification
{
    use Queueable;

    public $password;
    public $client;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Client $client, string $password)
    {
        //
        $this->client = $client;
        $this->password = $password;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        // return (new MailMessage)
        return (new ClientResetPasswordMail($notifiable, $this->password));
                    
    }

    public function toDatabase()
    {
        return [
            "message" => "your password was successfully reset to {$this->password}",
            "category" => "password reset"
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
