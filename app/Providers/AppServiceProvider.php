<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use App\Repositories\KnowledgeBaseRepository;
use App\Repositories\PrivateMessageRepository;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Schema::defaultStringLength(191);

        // extend validation to add new rule
        Validator::extend('greater_than_field', function ($attribute, $value, $parameters, $validator) {
            $min_field = $parameters[0];
            $data = $validator->getData();
            $min_value = $data[$min_field];
            return $value > $min_value;
        });

        Validator::replacer('greater_than_field', function ($message, $attribute, $rule, $parameters) {
            return sprintf('%s must be greater than %s', str_replace("_", " ", $attribute), str_replace("_", " ", $parameters[0]));
        });

        // view composer
        
        view()->composer('clients.*', function($view) {
            if (Auth::check()) {
                $view->with('client', Auth::user());
            }
        });

        // side content knowledge base
        view()->composer('admin.kb.side-content', function($view) {
            $kbRepo = new KnowledgeBaseRepository;
            $view->with('categories',$kbRepo->categories())
                ->with('recent_posts', $kbRepo->getRecentPosts());
        });

        // compose message modal
        view()->composer('admin.messaging.pm.compose', function($view) {
            $pmRepo = new PrivateMessageRepository;
            $view->with('recipients', $pmRepo->getPossibleRecipients());
        });
        
    }
}
