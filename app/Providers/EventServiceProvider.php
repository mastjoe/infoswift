<?php

namespace App\Providers;

use App\Pm;
use App\User;
use App\Image;
use App\Client;
use App\Region;
use App\MachinePart;
use App\MachinePartCart;
use App\Events\NewTicket;
use App\MachinePartDispatch;
use App\MachinePartSupplier;
use App\Events\DeclineTicket;
use App\MachinePartOrderItem;
use App\Observers\PmObserver;
use App\MachinePartStockSupply;
use App\Observers\UserObserver;
use App\Observers\ImageObserver;
use App\Observers\ClientObserver;
use App\Observers\RegionObserver;
use App\Events\ConfirmCloseTicket;
use App\Events\NewMachinePartOrder;
use Illuminate\Support\Facades\Event;
use App\Observers\MachinePartObserver;
use Illuminate\Auth\Events\Registered;
use App\Events\DeclineMachinePartOrder;
use App\Observers\MachinePartCartObserver;
use App\Observers\MachinePartDispatchObserver;
use App\Observers\MachinePartSupplierObserver;
use App\Observers\MachinePartOrderItemObserver;
use App\Listeners\NotifyAdminCloseTicketListener;
use App\Listeners\NotifyAdminNewMachinePartOrder;
use App\Observers\MachinePartStockSupplyObserver;
use App\Listeners\NotifyEngineerNewTicketListener;
use App\Listeners\NotifySupervisorNewTicketListener;
use App\Listeners\NotifyEngineerDeclineTicketListener;
use App\Listeners\NotifyEngineerDeclinePartsOrderListener;
use App\Listeners\NotifyEngineerConfirmedCloseTicketListener;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        NewTicket::class => [
            NotifyEngineerNewTicketListener::class,
            NotifySupervisorNewTicketListener::class,
        ],
        CloseTicket::class =>[
            NotifyAdminCloseTicketListener::class,
        ],
        DeclineTicket::class => [
            NotifyEngineerDeclineTicketListener::class,
        ],
        ConfirmCloseTicket::class => [
            NotifyEngineerConfirmedCloseTicketListener::class,
        ],
        NewMachinePartOrder::class => [
            NotifyAdminNewMachinePartOrder::class,
        ],
        DeclineMachinePartOrder::class => [
            NotifyEngineerDeclinePartsOrderListener::class,
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
        User::observe(UserObserver::class);
        Client::observe(ClientObserver::class);
        MachinePart::observe(MachinePartObserver::class);
        Image::observe(ImageObserver::class);
        MachinePartStockSupply::observe(MachinePartStockSupplyObserver::class);
        MachinePartSupplier::observe(MachinePartSupplierObserver::class);
        Region::observe(RegionObserver::class);
        MachinePartOrderItem::observe(MachinePartOrderItemObserver::class);
        Pm::observe(PmObserver::class);
        MachinePartCart::observe(MachinePartCartObserver::class);
        MachinePartDispatch::observe(MachinePartDispatchObserver::class);
    }
}
