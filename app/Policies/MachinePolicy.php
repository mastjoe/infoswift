<?php

namespace App\Policies;

use App\Machine;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class MachinePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any machines.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
        return $user->hasPrivilege('view machine');
    }

    /**
     * Determine whether the user can view the machine.
     *
     * @param  \App\User  $user
     * @param  \App\Machine  $machine
     * @return mixed
     */
    public function view(User $user, Machine $machine)
    {
        //allow super admin to view all machines
        if ($user->hasRole('super admin')) {
            return true;
        }

        $all_state_machine_ids = $user->all_state_machines()->pluck('id')->toArray();
        if (
            $user->hasPrivilege('view machine') &&
            in_array($machine->id, $all_state_machine_ids)
        ) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can create machines.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
        return $user->hasPrivilege('create machine');
    }

    /**
     * Determine whether the user can update the machine.
     *
     * @param  \App\User  $user
     * @param  \App\Machine  $machine
     * @return mixed
     */
    public function update(User $user, Machine $machine)
    {
        //
        return $user->hasPrivilege('update machine');
    }

    /**
     * Determine whether the user can delete the machine.
     *
     * @param  \App\User  $user
     * @param  \App\Machine  $machine
     * @return mixed
     */
    public function delete(User $user, Machine $machine)
    {
        //
        return $user->hasPrivilege('delete machine');
    }

    /**
     * Determine whether the user can restore the machine.
     *
     * @param  \App\User  $user
     * @param  \App\Machine  $machine
     * @return mixed
     */
    public function restore(User $user, Machine $machine)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the machine.
     *
     * @param  \App\User  $user
     * @param  \App\Machine  $machine
     * @return mixed
     */
    public function forceDelete(User $user, Machine $machine)
    {
        //
    }

    /**
     * determine whether user can manage machine assignment
     * 
     * @param \App\User $user
     */
    public function assign(User $user)
    {
        return $user->hasPrivilege('manage machine assignment');
    }

    /**
     * determines if user can manage machine faults
     * 
     * @param \App\User $user
     */
    public function manageFaults(User $user)
    {
        return $user->hasPrivilege('manage machine faults');
    }

    /**
     * determines if user can manage machine vendors
     * 
     * @param \App\User $user
     */
    public function manageVendors(User $user) 
    {
        return $user->hasPrivilege('manage machine vendors');
    }

    /**
     * determines if user can manage machine types
     * 
     * @param \App\User $user
     */
    public function manageTypes(User $user)
    {
        return $user->hasPrivilege('manage machine types');
    }

    /**
     * determines if user can manage machine statuses
     * 
     * @param \App\User $user
     */
    public function manageStatuses(User $user)
    {
        return $user->hasPrivilege('manage machine statuses');
    }

    /**
     * deteremines if user can create a pm record for a machine
     * @param \App\User $user
     * @param \App\Machine $machine
     */
    public function createPM(User $user, Machine $machine)
    {
        return $user->hasPrivilege('create pm');
    }


    /**
     * determines if user can assess pm of a machine
     * @param \App\User $user 
     */
    public function assessPM(User $user)
    {
        return $user->hasPrivilege('assess pm');
    }
}
