<?php

namespace App\Policies;

use App\MachineType;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class MachineTypePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any machine types.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
        $user->hasPrivilege('manage machine types');
    }

    /**
     * Determine whether the user can view the machine type.
     *
     * @param  \App\User  $user
     * @param  \App\MachineType  $machineType
     * @return mixed
     */
    public function view(User $user, MachineType $machineType)
    {
        //
        $user->hasPrivilege('manage machine types');
    }

    /**
     * Determine whether the user can create machine types.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
        $user->hasPrivilege('manage machine types');
    }

    /**
     * Determine whether the user can update the machine type.
     *
     * @param  \App\User  $user
     * @param  \App\MachineType  $machineType
     * @return mixed
     */
    public function update(User $user, MachineType $machineType)
    {
        //
        $user->hasPrivilege('manage machine types');
    }

    /**
     * Determine whether the user can delete the machine type.
     *
     * @param  \App\User  $user
     * @param  \App\MachineType  $machineType
     * @return mixed
     */
    public function delete(User $user, MachineType $machineType)
    {
        //
        $user->hasPrivilege('manage machine types');
    }

    /**
     * Determine whether the user can restore the machine type.
     *
     * @param  \App\User  $user
     * @param  \App\MachineType  $machineType
     * @return mixed
     */
    public function restore(User $user, MachineType $machineType)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the machine type.
     *
     * @param  \App\User  $user
     * @param  \App\MachineType  $machineType
     * @return mixed
     */
    public function forceDelete(User $user, MachineType $machineType)
    {
        //
    }
}
