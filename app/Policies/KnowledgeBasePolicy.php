<?php

namespace App\Policies;

use App\KnowledgeBase;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class KnowledgeBasePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
        return $user->hasPrivilege('manage knowledge base');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\KnowledgeBase  $knowledgeBase
     * @return mixed
     */
    public function view(User $user, KnowledgeBase $knowledgeBase)
    {
        //
        return $user->hasPrivilege('manage knowledge base');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
        return $user->hasPrivilege('manage knowledge base');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\KnowledgeBase  $knowledgeBase
     * @return mixed
     */
    public function update(User $user, KnowledgeBase $knowledgeBase)
    {
        //
        return $user->hasPrivilege('manage knowledge base');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\KnowledgeBase  $knowledgeBase
     * @return mixed
     */
    public function delete(User $user, KnowledgeBase $knowledgeBase)
    {
        //
        return $user->hasPrivilege('manage knowledge base');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\KnowledgeBase  $knowledgeBase
     * @return mixed
     */
    public function restore(User $user, KnowledgeBase $knowledgeBase)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\KnowledgeBase  $knowledgeBase
     * @return mixed
     */
    public function forceDelete(User $user, KnowledgeBase $knowledgeBase)
    {
        //
    }

    /**
     * determine if admin can manage model
     * 
     * @param \App\User
     * @return mixed
     */
    public function manage(User $user)
    {
        return $user->hasPrivilege('manage knowledge base');
    }
}
