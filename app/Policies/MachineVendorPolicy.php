<?php

namespace App\Policies;

use App\MachineVendor;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class MachineVendorPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any machine vendors.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
        $user->hasPrivilege('manage machine vendors');
    }

    /**
     * Determine whether the user can view the machine vendor.
     *
     * @param  \App\User  $user
     * @param  \App\MachineVendor  $machineVendor
     * @return mixed
     */
    public function view(User $user, MachineVendor $machineVendor)
    {
        //
        $user->hasPrivilege('manage machine vendors');
    }

    /**
     * Determine whether the user can create machine vendors.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
        $user->hasPrivilege('manage machine vendors');
    }

    /**
     * Determine whether the user can update the machine vendor.
     *
     * @param  \App\User  $user
     * @param  \App\MachineVendor  $machineVendor
     * @return mixed
     */
    public function update(User $user, MachineVendor $machineVendor)
    {
        //
        $user->hasPrivilege('manage machine vendors');
    }

    /**
     * Determine whether the user can delete the machine vendor.
     *
     * @param  \App\User  $user
     * @param  \App\MachineVendor  $machineVendor
     * @return mixed
     */
    public function delete(User $user, MachineVendor $machineVendor)
    {
        //
        $user->hasPrivilege('manage machine vendors');
    }

    /**
     * Determine whether the user can restore the machine vendor.
     *
     * @param  \App\User  $user
     * @param  \App\MachineVendor  $machineVendor
     * @return mixed
     */
    public function restore(User $user, MachineVendor $machineVendor)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the machine vendor.
     *
     * @param  \App\User  $user
     * @param  \App\MachineVendor  $machineVendor
     * @return mixed
     */
    public function forceDelete(User $user, MachineVendor $machineVendor)
    {
        //
    }
}
