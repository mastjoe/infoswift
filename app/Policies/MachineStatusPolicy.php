<?php

namespace App\Policies;

use App\MachineStatus;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class MachineStatusPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any machine statuses.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
        $user->hasPrivilege('manage machine statuses');
    }

    /**
     * Determine whether the user can view the machine status.
     *
     * @param  \App\User  $user
     * @param  \App\MachineStatus  $machineStatus
     * @return mixed
     */
    public function view(User $user, MachineStatus $machineStatus)
    {
        //
        $user->hasPrivilege('manage machine statuses');
    }

    /**
     * Determine whether the user can create machine statuses.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
        $user->hasPrivilege('manage machine statuses');
    }

    /**
     * Determine whether the user can update the machine status.
     *
     * @param  \App\User  $user
     * @param  \App\MachineStatus  $machineStatus
     * @return mixed
     */
    public function update(User $user, MachineStatus $machineStatus)
    {
        //
        $user->hasPrivilege('manage machine statuses');
    }

    /**
     * Determine whether the user can delete the machine status.
     *
     * @param  \App\User  $user
     * @param  \App\MachineStatus  $machineStatus
     * @return mixed
     */
    public function delete(User $user, MachineStatus $machineStatus)
    {
        //
        $user->hasPrivilege('manage machine statuses');
    }

    /**
     * Determine whether the user can restore the machine status.
     *
     * @param  \App\User  $user
     * @param  \App\MachineStatus  $machineStatus
     * @return mixed
     */
    public function restore(User $user, MachineStatus $machineStatus)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the machine status.
     *
     * @param  \App\User  $user
     * @param  \App\MachineStatus  $machineStatus
     * @return mixed
     */
    public function forceDelete(User $user, MachineStatus $machineStatus)
    {
        //
    }
}
