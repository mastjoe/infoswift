<?php

namespace App\Policies;

use App\MachinePart;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class MachinePartPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any machine parts.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
        return $user->hasPrivilege('view machine part');
    }

    /**
     * Determine whether the user can view the machine part.
     *
     * @param  \App\User  $user
     * @param  \App\MachinePart  $machinePart
     * @return mixed
     */
    public function view(User $user, MachinePart $machinePart)
    {
        //
        return $user->hasPrivilege('view machine part');
    }

    /**
     * Determine whether the user can create machine parts.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
        return $user->hasPrivilege('create machine part');
    }

    /**
     * Determine whether the user can update the machine part.
     *
     * @param  \App\User  $user
     * @param  \App\MachinePart  $machinePart
     * @return mixed
     */
    public function update(User $user, MachinePart $machinePart)
    {
        //
        return $user->hasPrivilege('update machine part');
    }

    /**
     * Determine whether the user can delete the machine part.
     *
     * @param  \App\User  $user
     * @param  \App\MachinePart  $machinePart
     * @return mixed
     */
    public function delete(User $user, MachinePart $machinePart)
    {
        //
        return $user->hasPrivilege('delete machine part');
    }

    /**
     * Determine whether the user can restore the machine part.
     *
     * @param  \App\User  $user
     * @param  \App\MachinePart  $machinePart
     * @return mixed
     */
    public function restore(User $user, MachinePart $machinePart)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the machine part.
     *
     * @param  \App\User  $user
     * @param  \App\MachinePart  $machinePart
     * @return mixed
     */
    public function forceDelete(User $user, MachinePart $machinePart)
    {
        //
    }

    /**
     * determines whether the user can order for machine parts
     * 
     * @param \App\User $user
     * @return mixed
     */
    public function order(User $user)
    {
        return $user->hasPrivilege('order machine parts');
    }

    public function manageSupplier(User $user)
    {
        return $user->hasPrivilege('manage machine suppliers');
    }
    
    public function manageSupply(User $user)
    {
        return $user->hasPrivilege('manage machine part supply');
    }

    public function manageCategories(User $user)
    {
        return $user->hasPrivilege('manage machine categories');
    }

    public function manageOrders(User $user)
    {
        return $user->hasPrivilege('manage machine part orders');
    }

    public function manageStocks(User $user)
    {
        return $user->hasPrivilege('manage machine part stocks');
    }
}
