<?php

namespace App\Policies;

use App\Ticket;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TicketPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
        return $user->hasPrivilege('view ticket');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\Ticket  $ticket
     * @return mixed
     */
    public function view(User $user, Ticket $ticket)
    {
        //
        if ($user->hasRole('super admin')) {
            return true;
        }

        $users_tickets = $user->all_state_machine_tickets()->pluck('id')->toArray();

        if ($user->hasPrivilege('view ticket') && in_array($ticket->id, $users_tickets)) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
        return $user->hasPrivilege('open ticket');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\Ticket  $ticket
     * @return mixed
     */
    public function update(User $user, Ticket $ticket)
    {
        //
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Ticket  $ticket
     * @return mixed
     */
    public function delete(User $user, Ticket $ticket)
    {
        //
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\Ticket  $ticket
     * @return mixed
     */
    public function restore(User $user, Ticket $ticket)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Ticket  $ticket
     * @return mixed
     */
    public function forceDelete(User $user, Ticket $ticket)
    {
        //
    }

    /**
     * determine whether the user can close a ticket
     * @param \App\User $user
     * @param \App\Ticket $ticket
     * @return mixed
     */
    public function close(User $user, Ticket $ticket)
    {
        return $user->hasPrivilege('close ticket');
    }

    /**
     * determines whether the user can confirm ticket closure
     * @param \App\User $user
     * @param \App\Ticket $ticket
     * @return mixed
     */
    public function assessClosure(User $user, Ticket $ticket)
    {
        return $user->hasPrivilege('assess ticket closure');
    }

    /**
     * determines whether the user can escalate ticket
     * @param \App\User $user
     * @param \App\Ticket $ticket
     * @return mixed
     */
    public function escalate(User $user, Ticket $ticket)
    {
        return $user->hasPrivilege('escalate ticket');
    }
}
