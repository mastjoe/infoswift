<?php

namespace App\Helpers;

use App\Pm;
use App\Role;
use App\Ticket;
use App\UserRole;
use Carbon\Carbon;
use App\AuditEvent;
use App\EngineerState;
use App\UserPrivilege;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;

class Util
{
    public static function getTableNextId($table)
    {
        $sql = "SHOW TABLE STATUS LIKE '$table'";
        $result = DB::select($sql);
        return $result[0]->Auto_increment;
    }

    protected static function genRef($id=null)
    {
        $rand = rand(111, 999);
        $timestamp = str_replace(".", "", microtime(true));
        $string = $rand.$timestamp.$id;
        return strtoupper(base_convert($string, 10, 35));
    }

    public static function genUserRef()
    {
        return self::genRef(self::getTableNextId('users'));
    } 

    public static function genTicketRef()
    {
        return self::genRef(self::getTableNextId('tickets'));
    }

    public static function genEscalationRef()
    {
        return self::genRef(self::getTableNextId('ticket_escalations'));
    }

    public static function genImageRef()
    {
        return self::genRef(self::getTableNextId('images'));
    }

    public static function genPartStockSupplyRef()
    {
        return self::genRef(self::getTableNextId('machine_part_stock_supplies'));
    }

    public static function genPartDispatchRef()
    {
        return self::genRef(self::getTableNextId('machine_part_dispatches'));
    }

    public static function genPartOrderRef()
    {
        return self::genRef(self::getTableNextId('machine_part_orders'));
    }

    public static function genAttachmentRef()
    {
        return "ATTACH-".sprintf('%03d', self::getTableNextId('attachments'));
    }

    public static function genPmRef()
    {
        return "PM-".sprintf('%03d', self::getTableNextId('pms'));
    }

    public static function genKbRef()
    {
        return self::genRef(self::getTableNextId("knowledge_bases"));
    }

    public static function genPrivateMessageRef()
    {
        return self::genRef(self::getTableNextId("private_messages"));
    } 

    public static function pushError(array $data)
    {
        throw ValidationException::withMessages($data);
    }

    public static function normalizeAmount($value)
    {
        return \str_replace(",", "", $value);
    }

    public static function assignUserPrivilege(int $user_id, int $privilege_id)
    {
        $q = UserPrivilege::where('user_id', $user_id)
            ->where('privilege_id', $privilege_id);
        if (!$q->count()) {
            UserPrivilege::create([
                'user_id'      => $user_id,
                'privilege_id' => $privilege_id
            ]);
        }
    }

    public static function attachUserRolePrivileges(int $user_id, int $role_id)
    {
       
        UserRole::create([
            'user_id' => $user_id,
            'role_id' => $role_id
        ]);

        $role = Role::with('privileges')->find($role_id);
        $privileges = $role->privileges;

        if ($privileges->count()) {
            foreach ($privileges as $privilege) {
                self::assignUserPrivilege($user_id, $privilege->id);            
            }
        }

    }

    public static function attachUserRoles(int $user_id, array $role_ids)
    {
        foreach ($role_ids as $role_id) {
            self::attachUserRolePrivileges($user_id, $role_id);
        }
    }

    public static function detachAllUserRoles($user_id)
    {
        $roles = UserRole::where('user_id', $user_id);
        $roles->delete();
        $privileges = UserPrivilege::where('user_id', $user_id);
        $privileges->delete();
    }

    public static function detachAllUserPrivileges($user_id)
    {
        UserPrivilege::where('user_id', $user_id)->delete();
    }

    public static function attachUserPrivileges(int $user_id, array $privilege_ids)
    {
        foreach ($privilege_ids as $privilege_id) {
            self::assignUserPrivilege($user_id, $privilege_id);
        }
    }

    public static function assignEngineersToState($state_id, array $engineer_ids)
    {
        $target = EngineerState::where('country_state_id', $state_id);
        if ($target->count()) {
            $target->delete();
        }

        foreach ($engineer_ids as $engineer_id) {
            EngineerState::create([
                'country_state_id' => $state_id,
                'user_id' => $engineer_id
            ]);
        }
    }

    public static function userIPAddress()
    {
        if (!empty($_SESSION['HTTP_CLIENT_IP'])) {
            return $_SESSION['HTTP_CLIENT_IP'];
        } else if (!empty($_SESSION['HTTP_X_FORWARDED_FOR'])) {
            return $_SESSION['HTTP_X_FORWARDED_FOR'];
        } else if (!empty($_SESSION['REMOTE_ADDR'])){
            return $_SESSION['REMOTE_ADDR'];
        }
        return  null;
    }

    public static function userAgent()
    {
        return $_SERVER['HTTP_USER_AGENT'] ?? null;
    }

    public static function processEditorImage($content)
    {
        libxml_use_internal_errors(true);
        $dom = new \DOMdocument();
        $dom->loadHtml($content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $images = $dom->getElementsByTagName('img');

        foreach ($images as $count => $image) {
            $src = $image->getAttribute('src');
            if (preg_match('/data:image/', $src)) {
                preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                $mimeType = $groups['mime'];
                $path = 'uploads/editors/' . uniqid() . '.' . $mimeType;
                Storage::disk('uploads')->put($path, file_get_contents($src));
                $image->removeAttribute('src');
                $image->setAttribute('src', asset($path));
            }
        }
        return $dom->saveHTML();
    }

    public static function getRoleUsers(string $role)
    {
        $target = Role::where('role', $role);
        if ($target->count()) {
            return $target->first()->users;
        }
        return [];
    }

    public static function counterPlus(int $input, $limit=10)
    {
        if ($input > $limit) {
            return $limit."+";
        }
        return $input;
    }

    public static function numberOrdinal($number) 
    {
        $order = null;
        switch ($number) {
            case 1:
                $order = "first";
                break;
            case 2:
                $order = "second";
                break;
            case 3:
                $order = "third";
                break;
            case 4:
                $order = "fourth";
                break;
        }
        return $order;
    }

    public static function pmYears() :array
    {
        $current_year = Carbon::now()->year;
        $pm_years = Pm::select('year')->distinct('year')->pluck('year')->toArray();
        array_unshift($pm_years, $current_year);
        return array_unique($pm_years);
    }

    public static function ticketYears() :array
    {
        $current_year = Carbon::now()->year;
        $ticket_years = Ticket::select('created_at')->get()->map(function($time) {
            return $time->created_at->format('Y');
        })->toArray();
        array_unshift($ticket_years, $current_year);
        return array_unique($ticket_years);
    }

    /**
     * get all months in a year in required format
     * @param string $format
     */
    public static function months($format = "F") :array
    {
        $months = [];
        for ($i = 1; $i <= 12; $i++) {
            $months[] = date($format, mktime(0, 0, 0, $i, 1));
        }
        return $months;
    }

    /**
     * nullifies auditable_id  field on audit event table during model deletion
     * @param string $model
     * @param int $id
     */
    public static function auditNullify(string $model, int $id)
    {
        $actual_model = "App\\{$model}";
        AuditEvent::where('auditable_type', $actual_model)
            ->where('auditable_id', $id)
            ->update(['auditable_id' => null]);
    }

    /**
     * get email settings
     */
    public static function emailSettings(string $key=null)
    {
    
        $data =  [
            "server_email" => "admin@newbbmservers.com",
            "sender_name" => 'Global Infoswift Technology'
        ];

        if ($key) {
            return $data[$key];
        }
        return $data;
    }
}