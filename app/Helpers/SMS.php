<?php

namespace App\Helpers;

use Unirest;

class SMS
{
    protected static $host;
    protected static $key;
    protected static $username;
    protected static $message;
    protected static $action = "compose";
    protected static $mms = '0';
    protected static $unicode = 0;
    protected static $media = null;
    protected static $to;
    protected static $sender;

    protected static $numbers;

    private static function init()
    {
        self::$host     = config('sms.route1.host');
        self::$key      = config('sms.route1.api_key');
        self::$username = str_replace("_", " ", config('sms.route1.username'));
        self::$sender   = config('sms.route1.sender_id');
    }

    protected static function make(array $data)
    {
        self::filterNumbers($data['to']);
        self::$message = $data['message'];
        if (array_key_exists('senderId', $data)) {
            self::$sender = $data['senderId'];
        }
    }

    public function setRecepients($recepients)
    {
        self::filterNumbers($recepients);
    }

    public function setMessage(string $message)
    {
        self::$message = $message;
    }

    private static function filterNumbers($numbers)
    {
        $valid_numbers = [];

        if (!is_array($numbers)) {
            $numbers = explode(", ", $numbers);
        }

        foreach ($numbers as $number) {
            $f3d = substr($number, 0, 3);
            $f1d = substr($number, 0, 1);

            if ($f3d == "234" || $f1d == "0") {
                if ($f3d != "234") {
                    $number = "234" . substr($number, 1, strlen($number));
                }
                $valid_numbers[] = $number;
            }
        }
        $number_string = implode(",", $valid_numbers);
        self::$to = $number_string;
    }

    public static function send(array $data=[])
    {
        self::init();
        if (count($data)) {
            self::make($data);
        }

        $query = [
            'action'   => self::$action,
            'username' => self::$username,
            'api_key'  => self::$key,
            'to'       => urlencode(self::$to),
            'mms'      => self::$mms,
            'unicode'  => self::$unicode,
            'media'    => self::$media,
            'sender'   => urlencode(self::$sender),
            'message'  => urlencode(self::$message)
        ];

        $header = [
            'Accept' => 'application/json'
        ];

        try {
            Unirest\Request::get(self::$host, $header, $query);
        } catch (\Throwable $th) {
        }
    }
}
