<?php
namespace App\Helpers;

use File;
use Image;
use App\Ticket;
use App\Helpers\Util;
use App\MachinePart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class Upload {

    public static function uploadTicketDocument(Request $request, Ticket $ticket, $type)
    {
        if ($request->hasFile('document')) {
            $doc = $request->document;
            $extension = $doc->extension();
            $prefix = strtoupper($type);
            $ref = strtoupper($ticket->ref);

            $name = "$prefix-{$ref}.$extension";
            $path = "documents/tickets/{$name}";
            Storage::put($path, file_get_contents($doc));
            return $ticket->documents()->create([
                'type'      => $type,
                'file_size' => $doc->getSize(),
                'file_type' => $doc->getMimeType(),
                'url'       => "uploads/".$path
            ]);
        }
        return null;
    } 

    protected static function uploadAnImage(object $image, object $model, string $path, array $options = [])
    {
        $extension  = $image->extension();
        $image_name = $image->getClientOriginalName();
        $image_size = $image->getSize();
        $image_type = $image->getMimeType();
        $path       = $path.Util::genImageRef().".".$extension;
        $img = Image:: make($image);
        if (array_key_exists("height", $options) && array_key_exists("width", $options)) {
            $img->resize($options["width"], $options["height"], function($i) { $i->aspectRatio(); });
        }
            $img->save(\public_path($path));
        return $model->images()->create([
            'label' => $image_name,
            'size'  => $image_size,
            'url'   => $path,
            'type'  => $image_type
        ]);
    }
    

    /**
     * upload machine part image
     */
    public static function machinePartsImage(Request $request, MachinePart $model)
    {
        $path = "uploads/parts/";
        if ($request->has('images') && count($request->images)) {
            foreach ($request->file('images') as $image_request) {
                self::uploadAnImage($image_request, $model, $path, ["width" => 800, "height" => 500]);
            }
        } 
    }

    /**
     * upload an image for a model
     */
    public static function uploadImage(object $image_request, object $model, string $name)
    {
        $path = "uploads/images/";
        return self::uploadAnImage($image_request, $model, $path);
    }

    /**
     * upload multiple images for a model
     * @param \Illuminate\Http\Request $request
     * @param object $model
     * @param string $name
     */
    public static function uploadMultipleImages(Request $request, object $model, string $name)
    {
        if ($request->{$name}) {
            foreach ($request->file($name) as $image_request) {
                self::uploadImage($image_request, $model, $name);
            }
        }
    }

    /**
     * delete an uploaded image
     */
    public static function deleteUploadedImages(Object $model) 
    {
        if ($model->images->count()) {
            foreach ($model->images as $image) {
                self::deleteUploadedFile($image->url);
                $image->delete();
            }
        }
    }   

    /**
     * delete an uploaded file
     * @param string $path
     */
    public static function deleteUploadedFile(string $path)
    {
        $actual_path = \public_path($path);
        if (File::exists($actual_path)) {
            File::delete($actual_path);
        }
    }

    public static function uploadAttachment(Request $request, object $model, string $name)
    {
        if ($request->hasFile($name)) {
            $attachment = $request->{$name};
            $extension  = $attachment->extension();
            $file_name  = $attachment->getClientOriginalName();
            $file_size  = $attachment->getSize();
            $file_type  = $attachment->getMimeType();
            $ref        = Util::genAttachmentRef();

            $path = "documents/attachments/".$ref.".".$extension;
            Storage::put($path, file_get_contents($attachment));

            $attach = $model->attachment()->create([
                'name' => $file_name,
                'size' => $file_size,
                'url'  => "uploads/".$path,
                'type' => $file_type,
            ]);
            return $attach;
        }
    }

    public static function uploadMultipleAttachments(Request $request, object $model, string $name)
    {
        if ($request->{$name}) {
            foreach ($request->file($name) as $file_request) {
                self::uploadAttachment($file_request, $model, $name);
            }
        }
    }

    public static function deleteUploadedAttachment(object $model)
    {
        if ($model->attachment) {
            $actual_path = \public_path($model->attachment->url);
            if (File::exists($actual_path)) {
                File::delete($actual_path);
                $model->attachment->delete();
            }
        }   
    }
}