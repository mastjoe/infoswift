<?php

namespace App\Helpers;

use Image;
use File;
use Avatar;
use App\User;
use App\Client;
use App\UserGuarantor;
use Illuminate\Http\Request;

class Photo
{
    public static $user_avatar_path = "uploads/profiles/users/";

    public static $client_avatar_path = "uploads/profiles/clients/";

    public static function uploadAvatar($category, $id)
    {
        $label = "profile-" . $id;

        if ($category == "client") {
            $model = Client::find($id);

            Avatar::create($model->full_name())
                ->save(public_path(self::$client_avatar_path.$label));
            $path = self::$client_avatar_path.$label;

        } else if ($category == "user") {
            $model = User::find($id);

            Avatar::create($model->full_name())
                ->save(public_path(self::$user_avatar_path.$label));
            $path = self::$user_avatar_path.$label;
        }

        $model->photo = $path;
        $model->save();

    }

    public static function handleGuarantorPhotoUpload(Request $request, $guarantor_id)
    {
        if ($request->hasFile('photo')) {
            $photo = $request->photo;
            $extension = $photo->extension();
            $photo_name = \uniqid().'.'.$extension;
            $path = "uploads/profiles/users/guarantors/".$photo_name;
            Image::make($photo)->resize(120, 120, function($i) {
                $i->aspectRatio();
            })
                ->save(public_path($path));
            $g = UserGuarantor::find($guarantor_id);
            $g->photo = $path;
            $g->save();
            return $path;
        }
        return null;
    }


    public static function deleteGuarantorPhoto($guarantor_id) {
        $g = UserGuarantor::find($guarantor_id);
        if ($g->photo) {
            $photo_path = public_path($g->photo);
            File::delete($photo_path);
        }
    }

    public static function updateGuarantorPhoto(Request $request, $guarantor_id)
    {
        if ($request->hasFile('photo')) {
            self::deleteGuarantorPhoto($guarantor_id);
            self::handleGuarantorPhotoUpload($request, $guarantor_id);
        }
    }


    public static function uploadClientLogo(Request $request, $client_id)
    {
        if ($request->hasFile('logo')) {
            $logo = $request->logo;
            $extension = $logo->extension();
            $logo_name = "clientLogo-{$client_id}.".$extension;
            $path = "uploads/profiles/clients/logos/".$logo_name;
            Image::make($logo)->resize(200, 200, function ($i) {
                $i->aspectRatio();
            })
                ->save(\public_path($path));
            
            $client = Client::find($client_id);
            $client->logo = $path;
            $client->save();
            return true;
        }
        return false;
    }

    public static function uploadStaffPhoto(Request $request, $user_id)
    {
        if ($request->hasFile('image')) {
            $image = $request->image;
            $extension = $image->extension();
            $image_name = "user-{$user_id}." . $extension;
            $path = "uploads/profiles/users/" . $image_name;
            Image::make($image)->resize(200, 200, function ($i) {
                $i->aspectRatio();
            })
                ->save(\public_path($path));

            $user = User::find($user_id);
            $user->update(['photo' => $path]);
            return $user;
        }
        return false;
    }

    public static function deleteStaffPhoto(User $user)
    {
        if ($user->photo and File::exists(\public_path($user->photo))) {
            File::delete(\public_path($user->photo));
        }
    }

}