<?php
namespace App\Helpers;

class UI {
    public static function pmStars($num)
    {
        $con = "";
        $total_num = 5;
        for ($i=0; $i < $num; $i++) { 
            $con .= '<span class="fa fa-star text-warning"></span>';
        }
        $diff = $total_num - $num;
        for ($i=0; $i < $diff; $i++) { 
            $con .= '<span class="fa fa-star text-secondary"></span>';
        }
        return $con;
    }

    public static function ticketStatusBadge($status)
    {
        $status = strtolower($status);
        if ($status == "open") {
            return '<span class="badge badge-dark">open</span>';
        } else if($status == "declined") {
            return '<span class="badge badge-primary">declined</span>';
        } else if ($status == "closed") {
            return '<span class="badge badge-danger">closed</span>';
        } else if ($status == "resolved") {
            return '<span class="badge badge-success">resolved</span>';
        } else {
            return '<span class="badge badge-warning">'.$status.'</span>';
        }
    }

    /**
     * get the ui badge form for machine part status order
     * @param string $status
     */
    public static function orderStatusBadge(string $status)
    {
        switch ($status) {
            case 'open':
                $badge = '<span class="badge badge-dark">open</span>';
                break;
            case 'declined':
                $badge = '<span class="badge badge-primary">declined</span>';
                break;
            case 'dispatched':
                $badge = '<span class="badge badge-danger">dispatched</span>';
                break;
            case 'received':
                $badge = '<span class="badge badge-success">received</span>';
                break;
            default:
                $badge = '<span class="badge badge-warning">pending</span>';
                break;
        }

        return $badge;
    }

    /**
     * dispatch status badge
     * @param string $status
     * @return string $status
     */
    public static function dispatchStatusBadge(string $status)
    {
        switch ($status) {
            case 'open':
                $badge = '<span class="badge badge-warning">open</span>';
                break;
            case 'sent':
                $badge = '<span class="badge badge-primary">sent</span>';
                break;
            case 'received':
                $badge = '<span class="badge badge-success">received</span>';
                break;
            default:
                # code...
                break;
        }
        return $badge;
    }
}