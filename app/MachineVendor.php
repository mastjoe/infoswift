<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MachineVendor extends Model
{
    //
    protected $fillable = [
        'vendor',
        'description'
    ];


    public function machines()
    {
        return $this->hasMany('App\Machine', 'machine_vendor_id');
    }

    public function deletable()
    {
        return true;
    }
}
