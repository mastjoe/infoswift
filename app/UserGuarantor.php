<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserGuarantor extends Model
{
    //
    protected $fillable = [
        'name',
        'phone',
        'email',
        'address',
        'photo',
        'user_id',
        'relationship',
        'occupation'
    ];

    public function getNameAttribute($value) 
    {
        return ucwords(strtolower($value));
    }

    public function audit_events()
    {
        return $this->morphMany('App\AuditEvent', 'auditable');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function deletable()
    {
        return true;
    }

    public function avatar()
    {
        if ($this->photo) {
            return asset($this->photo);
        }
        return asset('images/avatar16.jpg');
    }
}
