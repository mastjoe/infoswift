<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class CheckOnlineGuard
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $expiry_time = Carbon::now()->addMinutes(3);
        if (Auth::check()) {
            $user = Auth::user();
            Cache::put(
                'users_online_' . $user->id,
                true,
                $expiry_time
            );
        }
        return $next($request);
    }
}
