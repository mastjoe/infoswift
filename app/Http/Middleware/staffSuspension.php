<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class staffSuspension
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->suspended_at) {
                $message = "Sorry {$user->full_name()}! you have been suspended! Please contact admin";
                Session::flash('suspended', $message);
                Auth::logout();
                return redirect()->route('login')->withInput();

            }
        }

        return $next($request);
    }
}
