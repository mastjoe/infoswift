<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class ShareViewData
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {

            $user = Auth::user();
            if (Auth::guard('client')->check()) {
                view()->share('client', $user);
            } else if (Auth::guard('web')->check()) {
                view()->share('user', $user);
            }
        }
        return $next($request);
    }
}
