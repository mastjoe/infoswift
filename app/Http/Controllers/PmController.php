<?php

namespace App\Http\Controllers;

use App\Pm;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class PmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $pms = Pm::orderBy('created_at', 'desc')->get();

        return view('admin.machines.pms.index')
            ->with('pms', $pms);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $ref
     * @return \Illuminate\Http\Response
     */
    public function show($ref)
    {
        //
        $pm = Pm::with(['doneBy', 'machine'])->where('ref', $ref)->firstOrFail();
        return view('admin.machines.pms.show')
            ->with('pm', $pm);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * decline PM 
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function declinePm(Request $request, $id)
    {
        $request->validate([
            'remark' => 'nullable|string'
        ]);

        $pm = Pm::findOrFail($id);
        $pm->update([
            'declined_at'        => Carbon::now(),
            'declination_remark' => $request->remark,
            'status'             => 'declined'
        ]);

        $pm->audit_events()->create([
            'event'       => 'updated',
            'description' => 'PM report was declined'
        ]);

        $message = "PM {$pm->ref} was successfully declined";
        Session::flash('pm_declined', $message);
        return response()->json([
            'status'   => 'success',
            'message'  => $message,
            'redirect' => route('machine.pms.details', $pm->ref)
        ]); 
    }

    /**
     * accept Pm
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function acceptPm(Request $request, $id)
    {
        $request->validate([
            'remark' => 'nullable|string',
            'rating'   => 'required'
        ]);

        $pm = Pm::findOrFail($id);
        $pm->update([
            'confirmed_at'        => Carbon::now(),
            'confirmation_remark' => $request->remark,
            'status'              => 'accepted',
            'rating'              => $request->rating
        ]);

        $pm->audit_events()->create([
            'event'       => 'updated',
            'description' => 'PM report was accepted and confirmed'
        ]);

        $message = "Pm {$pm->ref} was successfully accepted";
        Session::flash('pm_accepted', $message);
        return response()->json([
            'status'   => 'success',
            'message'  => $message,
            'redirect' => route('machine.pms.details', $pm->ref)
        ]);
    }
}
