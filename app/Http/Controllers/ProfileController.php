<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class ProfileController extends Controller
{
    //
    public function index()
    {
        $user = Auth::user();

        return view('admin.profile.index')
            ->with('user', $user);
    }

    public function password()
    {
        $user = Auth::user();

        return view('admin.profile.password')
            ->with('user', $user);
    }

    public function update(Request $request)
    {
        $request->validate([
            'first_name'  => 'required|string',
            'middle_name' => 'nullable|string',
            'last_name'   => 'required|string',
            'gender'      => 'required',
            'dob'         => 'nullable|date',
            'email'       => 'required|email',
            'phone'       => 'required|digits_between:8,15',
            'address'     => 'nullable|string'
        ], [], [
            'dob' => 'birth date'
        ]);

        $user              = Auth::user();
        $user->first_name  = $request->first_name;
        $user->middle_name = $request->middle_name;
        $user->last_name   = $request->last_name;
        $user->gender      = $request->gender;
        $user->email       = $request->email;
        $user->phone       = $request->phone;
        $user->address     = $request->address;
        $user->save();
        
        $message = "Your profile was successfully updated";
        return redirect()->route('admin.profile')
            ->with('updated', $message);
    }

    public function updatePassword(Request $request)
    {
        $request->validate([
            'old_password'     => 'required',
            'password'         => 'required|required_with:confirm_password|same:confirm_password|min:6',
            'confirm_password' => 'required'
        ]);

        $user = Auth::user();
        if (Hash::check($request->old_password, $user->password)) {
            Util::pushError([
                'old_password' => 'old password is incorrect'
            ]);
        }

        $user->update([
            'password' => \bcrypt($request->password)
        ]);
        
        $message = "Your password has been updated";

        return redirect()->route('admin.password')
            ->with('updated', $message);
    }

    public function bio()
    {
        $user = Auth::user();

        return view('admin.profile.bio')
            ->with('user', $user);
    }

    public function updateBio(Request $request)
    {
        $request->validate([
            'bio' => 'required|string'
        ]);

        $user = Auth::user();
        $user->update(['bio' => $request->bio]);
        
        $message = "Your bio was successfully updated";
        
        return redirect()->route('admin.bio')
            ->with('updated', $message);

    }
}
