<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Repositories\PrivateMessageRepository;

class PrivateMessageController extends Controller
{
    //
    public $repo;

    public function __construct(PrivateMessageRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * landing for private message
     * 
     * @return Illuminate\Http\Response
     */
    public function index()
    {
        $inboxes = $this->repo->inboxes();

        return view('admin.messaging.pm.index')
            ->with('inboxes', $inboxes);
    }

    /**
     * sent messages
     */
    public function sent()
    {
        $outboxes = $this->repo->outboxes();

        return view('admin.messaging.pm.sent')
            ->with('outboxes', $outboxes);
    }

    /**
     * trashed messages
     */
    public function trash()
    {
        return view('admin.messaging.pm.trash');
    }

    /**
     * conversations
     */
    public function conversations()
    {
        $conversations = $this->repo->conversations();
        return $conversations;

        return view('admin.messaging.pm.conversations');
    }

    /**
     * send message to recipients
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function sendMessage(Request $request)
    {
        $messages = $this->repo->sendMessage($request);

        $msg = 
        number_format($messages->count())." message".($messages->count() > 1 ? "s were ":" was")." sent";
        Session::flash('message_sent', $msg);

        return response()->json([
            "status"   => "success",
            "message"  => $msg,
            "redirect" => route('admin.sent.private.messages')
        ]);
    }

    /**
     * read private message
     * 
     * @param string $ref
     * @return \Illuminate\Http\Response
     */
    public function readMessage($ref)
    {
        $message = $this->repo->readMessage($ref);

        return view('admin.messaging.pm.show')
            ->with('message', $message);
    }

    /**
     * show a form to private message a staff
     * 
     * @param int $staff_id
     * @return \Illuminate\Http\Response
     */
    public function showStaffPrivateMessageForm($staff_id)
    {
        $staff = User::findOrFail($staff_id);

        return view('admin.messaging.pm.message-staff')
            ->with('staff', $staff);
    }

    /**
     * send message to specific staff
     * @param \Illuminate\Http\Request $request
     * @param int $staff_id
     * @return \Illuminate\Http\Response
     */
    public function sendMessageToStaff(Request $request, $staff_id)
    {
        $msg = $this->repo->messageStaff($request, $staff_id);
        $message = "Message successfully sent to {$msg->receiver->full_name()}";

        return response()->json([
            "status" => "success",
            "message" => $message,
        ]);
    }
}
