<?php

namespace App\Http\Controllers;

use Session;
use App\Branch;
use App\BranchCustodian;
use App\Region;
use App\Helpers\Util;
use App\MachineStatus;
use App\MachineType;
use App\MachineVendor;
use Illuminate\Http\Request;

class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $branches = Branch::all();
        $regions = Region::all();

        return view('admin.settings.locations.branches.index')
            ->with('branches', $branches)
            ->with('regions', $regions);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return redirect()->route('all.branches')
            ->with('new', true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'branch' => 'required|string',
            'sol_id' => 'nullable|string|unique:branches,sol_id',
            'address' => 'nullable|string',
            'emails' => 'nullable|string',
            'phone_numbers' => 'nullable|string',
        ]);

        $request->request->add(['region_id' => $request->region]);
        $this->validateBranch($request);

        $region = Region::findOrFail($request->region);
        $branch = $region->branches()->create([
            'branch'     => $request->branch,
            'sol_id'     => $request->sol_id,
            'address'    => $request->address,
            'emails'     => $this->resolveEmails($request->emails),
            'phones'     => $this->resolvePhoneNumbers($request->phone_numbers),
            'client_id'  => $region->client_id,
            'state_id'   => $region->state_id,
            'country_id' => $region->country_id
        ]);

        $message = "Branch, ".$branch->branch." was successfully created";
        Session::flash('created', $message);
        return response()->json([
            'status'       => 'success',
            'message'      => $message,
            'redirect_url' => route('all.branches')
        ]);

    }

    public function resolveEmails(string $emails = null)
    {
        if ($emails) {
            $email_array = explode(",", preg_replace('/\s/', "", $emails));
            foreach ($email_array as $email) {
                if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    Util::pushError([
                        'emails' => 'invalid email found'
                    ]);
                }
            }
            return implode(",", $email_array);
        }
        return null;
    }

    public function resolvePhoneNumbers(string $numbers = null) 
    {
        if ($numbers) {
            $number_array = explode(",", preg_replace('/\s/', "", $numbers));
            foreach ($number_array as $number) {
                if (strlen($number) < 8 || strlen($number) > 15) {
                    Util::pushError([
                        'phone_numbers' => 'invalid phone number found'
                    ]);
                }
            }
            return implode(",", $number_array);
        }
        return null;
    }

    public function validateBranch(Request $request, int $edit=null)
    {
        $branch    = $request->branch;
        $region_id = $request->region_id;
        $region    = Region::findOrFail($region_id);
        $client_id = $region->client_id;

        $target = Branch::where('branch', $branch)
            ->where('region_id', $region_id)
            ->where('client_id', $client_id);
        
        if ($edit) {
            if ($target->first()->id != $edit) {
                Util::pushError([
                    'branch' => 'branch is already taken for same client and in same region!'
                ]);
            }
        } else {
            if ($target->count()) {
                Util::pushError([
                    'branch' => 'branch is already taken for same client and in same region!'
                ]);
            }
        }
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $branch = Branch::with([
            'region',
            'state',
            'country',
            'machines',
            'custodians',
            'client'
        ])->findOrFail($id);

        $regions = Region::all();
        $machine_statuses = MachineStatus::all();
        $machine_vendors = MachineVendor::all();
        $machine_types = MachineType::all();

        return view('admin.settings.locations.branches.show')
            ->with('branch', $branch)
            ->with('regions', $regions)
            ->with('machine_statuses', $machine_statuses)
            ->with('machine_vendors', $machine_vendors)
            ->with('machine_types', $machine_types);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        Branch::findOrFail($id);
        return redirect()->route('show.branch', $id)
            ->with('edit', true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'branch'        => 'required|string',
            'sol_id'        => 'nullable|string|unique:branches,sol_id,'.$request->sol_id.',sol_id',
            'address'       => 'nullable|string',
            'emails'        => 'nullable|string',
            'phone_numbers' => 'nullable|string',
        ]);

        $branch = Branch::findOrFail($id);
        $region = Region::findOrFail($branch->region_id);
    
        $request->request->add(['region_id' => $region->id]);
        $this->validateBranch($request, $id);

        $branch->branch     = $request->branch;
        $branch->address    = $request->address;
        $branch->sol_id     = $request->sol_id;
        $branch->emails     = $this->resolveEmails($request->emails);
        $branch->phones     = $this->resolvePhoneNumbers($request->phones);
        $branch->region_id  = $request->region;
        $branch->country_id = $region->country_id;
        $branch->state_id   = $region->state_id;
        $branch->client_id  = $region->client_id;
        $branch->save();

        $message = "Branch, ".$branch->branch." was updated!";
        Session::flash('updated', $message);
        return response()->json([
            'status'       => 'success',
            'message'      => $message,
            'redirect_url' => route('show.branch', $id)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $branch = Branch::findOrFail($id);
        if ($branch->deletable()) {
            $message = "Branch, ".$branch->branch." was successfully deleted";
            if ($branch->delete()) {
                Session::flash('deleted', $message);
                return response()->json([
                    'status' => 'success',
                    'message' => $message,
                    'redirect_url' => route('all.branches')
                ]);
            }
        }
        return response()->json([
            'status' => 'error',
            'message' => 'Branch was not deleted! Try again!'
        ]);
    }

    public function createMachine($id)
    {
        $branch = Branch::findOrFail($id);
        return redirect()->route('show.branch', $id)
            ->with('create_machine', true);
    }

    public function storeMachine(Request $request, $id)
    {
        $request->validate([
            'name'           => 'nullable|string',
            'terminal_id'    => 'required|unique:machines,terminal_id,'.$request->terminal_id.',id',
            'serial_number'  => 'nullable|string',
            'machine_vendor' => 'required',
            'machine_type'   => 'required',
            'ip_address'     => 'nullable|ip',
            'model_number'   => 'nullable|string',
            'branch_status'  => 'required'
        ]);

        $branch = Branch::findOrFail($id);

        $machine = $branch->machines()->create([
            'terminal_id'       => $request->terminal_id,
            'serial_number'     => $request->serial_number,
            'machine_vendor_id' => $request->machine_vendor,
            'machine_status_id' => $request->machine_status,
            'machine_type_id'   => $request->machine_type,
            'ip_address'        => $request->ip_address,
            'model_number'      => $request->model_number,
            'region_id'         => $branch->region_id,
            'client_id'         => $branch->client_id,
            'branch_status'     => $request->branch_status,
            'is_asd'            => true
        ]);

        $message = "Machine, ".$machine->terminal_id." was successfully created!";
        Session::flash('created_machine', $message);
        return response()->json([
            'status' => 'success',
            'message' => $message,
            'redirect_url' => route('show.branch', $id)
        ]); 
    }

    public function createCustodian($id)
    {
        $branch = Branch::findOrFail($id);
        return redirect()->route('show.branch', $id)
            ->with('create_custodian', true);
    }

    public function storeCustodian(Request $request, $id)
    {
        $request->validate([
            'name'  => 'required|string',
            'phone' => 'required|digits_between:8,15',
            'email' => 'required|email|string'
        ]);
        $this->validateCustodianToBranch($request->name, $id);
        $branch    = Branch::findOrFail($id);
        $custodian = $branch->custodians()->create([
            'name'  => $request->name,
            'phone' => $request->phone,
            'email' => $request->email
        ]);

        $message = "Custodian {$custodian->name} successfully added to {$branch->branch}";
        Session::flash('created_custodian', $message);

        return response()->json([
            'status' => 'success',
            'message' => $message,
            'redirect_url' => route('show.branch', $id)
        ]);
    }

    protected function validateCustodianToBranch(string $name, $id, $edit_id="null")
    {
        $target = BranchCustodian::where('name', $name)->where('branch_id', $id);

        if ($target->count()) {
            if ($edit_id) {
                if ($target->first()->id != $edit_id) {
                    Util::pushError([
                        'name' => 'name already exist for branch'
                    ]);
                }
            } else {
                Util::pushError([
                    'name' => 'name already exist for branch'
                ]);
            }
        }
    }

    public function editCustodian($id, $custodian_id)
    {
        $custodian = BranchCustodian::with('branch')->findOrFail($custodian_id);
        return view('admin.settings.locations.branches.edit-custodian')
            ->with('custodian', $custodian)
            ->with('branch', $custodian->branch);
    }

    public function updateCustodian(Request $request, $id, $custodian_id)
    {
        $request->validate([
            'name'  => 'required|string',
            'phone' => 'required|digits_between:8,15',
            'email' => 'required|email|string'
        ]);
        $this->validateCustodianToBranch($request->name, $id, $custodian_id);
        $custodian        = BranchCustodian::findOrFail($custodian_id);
        $custodian->name  = $request->name;
        $custodian->phone = $request->phone;
        $custodian->email = $request->email;
        $custodian->save();

        $message = "Branch custodian, {$custodian->name} successfully updated";
        Session::flash('updated_custodian', $message);
        
        return response()->json([
            'status'       => 'success',
            'message'      => $message,
            'redirect_url' => route('show.branch', $id)
        ]);
    }

    public function deleteCustodian($id, $custodian_id)
    {
        $custodian = BranchCustodian::with('branch')->findOrFail($custodian_id);
        if ($custodian->deletable()) {

            $message = "Custodian {$custodian->name} of branch, {$custodian->branch->branch} was ".
            "successfully removed!";

            Session::flash('deleted_custodian', $message);
            if ($custodian->delete()) {
                return response()->json([
                    'status'       => 'success',
                    'message'      => $message,
                    'redirect_url' => route('show.branch', $id)
                ]);
            }
        }
        return response()->json([
            'status'  => 'error',
            'message' => 'Custodian was not deleted! Try again'
        ]);
    }
}
