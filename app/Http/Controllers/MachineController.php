<?php

namespace App\Http\Controllers;

use App\Pm;
use Session;
use App\Branch;
use App\Client;
use App\Machine;
use Carbon\Carbon;
use App\MachineType;
use App\Helpers\Util;
use App\MachineStatus;
use App\MachineVendor;
use App\Helpers\Upload;
use App\Repositories\MachineRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MachineController extends Controller
{
    public $machineRepo;

    public function __construct(MachineRepository $machineRepo)
    {
        $this->machineRepo = $machineRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $this->authorize('viewAny', Machine::class);
        
        if ($this->user()->hasRole('super admin')) {
            $machines = Machine::all();
        } else {
            $machines = $this->user()->all_state_machines();
        }
        
        return view('admin.machines.index')
            ->with('machines', $machines);
    }

    protected function user()
    {
        return Auth::user();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $this->authorize('create', Machine::class);

        $branches = Branch::all();
        $machine_vendors = MachineVendor::all();
        $machine_types = MachineType::all();
        $machine_statuses = MachineStatus::all();
        $clients = Client::all();

        return view('admin.machines.create')
            ->with('branches', $branches)
            ->with('machine_vendors', $machine_vendors)
            ->with('machine_statuses', $machine_statuses)
            ->with('machine_types', $machine_types)
            ->with('clients', $clients);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
       $machine = $this->machineRepo->create($request);

        $message = "Machine with terminal id, ".$machine->terminal_id.
        " was successfully created!";
        
        return redirect()->route('show.machine', $machine->id)
            ->with('created', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $machine = Machine::with(
            'branch',
            'client',
            'region',
            'region.state',
            'machineType',
            'machineStatus',
            'machineVendor',
            'tickets'
        )
        ->findOrFail($id);

        $this->authorize('view', $machine);

        return view('admin.machines.show')
            ->with('machine', $machine);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $machine = Machine::findOrFail($id);
        $this->authorize('update', $machine);

        $machine_types = MachineType::all();
        $machine_statuses = MachineStatus::all();
        $machine_vendors = MachineVendor::all();
        $branches = Branch::all();
        $clients = Client::all();

        return view('admin.machines.edit')
            ->with('machine', $machine)
            ->with('machine_vendors', $machine_vendors)
            ->with('machine_statuses', $machine_statuses)
            ->with('machine_types', $machine_types)
            ->with('branches', $branches)
            ->with('clients', $clients);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'name'           => 'nullable|string',
            'terminal_id'    => 'required|string|unique:machines,terminal_id,'.$request->terminal_id.',terminal_id',
            'branch_status'  => 'required',
            'model_number'   => 'nullable|string',
            'serial_number'  => 'nullable|string',
            'ip_address'     => 'nullable|ip',
            'client'         => 'required',
            'branch'         => 'required_if:branch_status,inbranch',
            'address'        => 'nullable|string',
            'machine_type'   => 'required',
            'machine_status' => 'required',
            'machine_vendor' => 'required'
        ]);

        $branch = Branch::findOrFail($request->branch);
        $machine = Machine::findOrFail($id);
        $machine->name = $request->name;
        $machine->branch_id = $request->branch;
        $machine->region_id = $branch->region_id;
        $machine->terminal_id = $request->terminal_id;
        $machine->branch_status = $request->branch_status;
        $machine->model_number = $request->model_number;
        $machine->serial_number = $request->serial_number;
        $machine->ip_address = $request->ip_address;
        $machine->client_id = $request->client;
        $machine->location = $request->address;
        $machine->machine_type_id = $request->machine_type;
        $machine->machine_vendor_id = $request->machine_vendor;
        $machine->machine_status_id = $request->machine_status;
        $machine->save();

        $message =  "Machine with terminal ID, {$machine->terminal_id} was successfully updated!";
        
        return redirect()->route('show.machine', $id)
            ->with('updated', $message);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $machine = Machine::findOrFail($id);
        if ($machine->deletable()) {
            $message = "Machine with terminal ID, {$machine->terminal_id} ".
            "was successfully deleted!";
            if ($machine->delete()) {
                Session::flash('deleted', $message);
                return response()->json([
                    'status' => 'success',
                    'message' => $message,
                    'redirect' => route('all.machines')
                ]);
            }
        }
        return response()->json([
            'status' => 'error',
            'message' => 'Machine was not deleted! Try again'
        ]);
    }

    /**
     * show ticket history of a machine
     * 
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function tickets($id)
    {
        $machine = Machine::findOrFail($id);

        return view('admin.machines.tickets')
            ->with('machine', $machine);
    }

    /**
     * shows a machine pm history
     * 
     * @param int $id
     * @param int $year
     * @return \Illuminate\Http\Response
     */
    public function pms($id, $year=null)
    {  
        $machine  = Machine::findOrFail($id);

        $this->authorize('view', $machine);
        $this->authorize('createPM', $machine);
        $current_year = Carbon::now()->year;
        if (!$year) {
            $year = $current_year;
        }
        $year_pms = $machine->year_pms($year)->get();

        return view('admin.machines.pms')
            ->with('year', $year)
            ->with('year_pms', $year_pms)
            ->with('machine', $machine)
            ->with('pm_years', Util::pmYears());
    }

    /**
     * show specific quarter from a machine pm record
     * @param int $id
     * @param int $year
     * @param int $quarter
     * @return \Illuminate\Http\Response
     */
    public function showPmQuarter($id, $year, $quarter)
    {
        $machine = Machine::findOrFail($id);
        $this->authorize('view', $machine);
        $this->authorize('createPM', $machine);

        $pm = Pm::where('machine_id', $id)->where('year', $year)->where('quarter', $quarter)
                ->with(['attachment', 'doneBy'])
                ->firstOrFail();

        return view('admin.machines.pm-details')
            ->with('machine', $machine)
            ->with('pm', $pm)
            ->with('year', $year);

    }

    /**
     * stores machine pm report for a quarter
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function storePm(Request $request, $id)
    {
        $request->validate([
            'quarter' => 'required',
            'note'    => 'nullable|string',
            'form'    => 'required|file|mimes:png,jpg,jpeg,pdf,docx,doc|max:1024'
        ]);

        $machine = Machine::findOrFail($id);
        $pm = $machine->pms()->create([
            'client_id' => $machine->client_id,
            'year'      => $request->year,
            'quarter'   => $request->quarter,
            'note'      => $request->note,
        ]);
        
        // upload attachment to pm
        Upload::uploadAttachment($request, $pm, "form");

        if ($pm->quarter == 1) {
            $order = "first";
        } else if ($pm->quarter == 2) {
            $order = "second";
        } else if ($pm->quarter == 3) {
            $order = "third";
        } else {
            $order = "fourth";
        }

        $message = "pm report for {$order} quarter successfully received!";
        Session::flash('pm_report', $message);

        return response()->json([
            'status' => 'success',
            'message' => $message,
            'data' => $pm,
            'redirect' => route('show.machine.pms.quarter', [$id, $pm->year, $pm->quarter]),
        ]);
    }

    /**
     * delete pm report
     * @param int $pm_id
     * @return \Illuminate\Http\Response
     */
    public function deletePm($pm_id)
    {
        $pm = Pm::findOrFail($pm_id);
        if ($pm->deletable()) {
            $order = Util::numberOrdinal($pm->quarter);
            $message = "PM report for {$order} quarter for ".$pm->year." was successfully deleted";
            if ($pm->delete()) {
                Session::flash('pm_deleted', $message);
                return response()->json([
                    'status' => 'success',
                    'message' => $message,
                    'redirect' => route('show.machine.pms', $pm->machine_id)
                ]);
            }
        }
        return response()->json([
            'status' => 'error',
            'message' => 'could not delete pm! Check and try again'
        ]);
    }

    /**
     * show edit pm form
     * @param int $pm_id
     * @return \Illuminate\Http\Response
     */
    public function editPm($pm_id)
    {   
        $pm = Pm::findOrFail($pm_id);

        return view('admin.machines.pm-edit')
            ->with('pm', $pm);
    }

    /**
     * update pm details
     * @param \Illuminate\Http\Request $request
     * @param int $pm_id
     * @return \Illuminate\Http\Response
     */
    public function updatePm(Request $request, $pm_id)
    {
        $request->validate([
            'quarter' => 'required',
            'note'    => 'nullable|string',
            'form'    => 'nullable|file|mimes:png,jpg,jpeg,pdf,docx,doc|max:1024'
        ]);

        $pm = Pm::findOrFail($pm_id);
        $pm->update([
            'quarter' => $request->quarter,
            'note' => $request->note,
        ]);

        if ($request->hasFile('form')) {
            Upload::deleteUploadedAttachment($pm);
            Upload::uploadAttachment($request, $pm, 'form');
        }

        $message = "PM report successfully updated!";
        Session::flash('pm_updated', $message);

        return response()->json([
            'status' => 'success',
            'message' => $message,
            'redirect' => route('show.machine.pms.quarter', [$pm->machine_id, $pm->year, $pm->quarter])
        ]);
    }

    /**
     * assign engineer to machine
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @param \Illuminate\Http\Response
     */
    public function assignEngineer(Request $request, $id)
    {
        $machine = Machine::findOrFail($id);
        $this->machineRepo->assignEngineerToMachine($request, $machine);

        $message = "machine, {$machine->terminal_id} was successfully assigned to engineer, {$machine->engineer->full_name()}";

        Session::flash('assigned_engineer', $message);

        return response()->json([
            "status" => "success",
            "message" => $message,
        ]);

    }
}
