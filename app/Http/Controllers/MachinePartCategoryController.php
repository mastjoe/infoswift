<?php

namespace App\Http\Controllers;

use App\MachinePartCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class MachinePartCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categories = MachinePartCategory::all();

        return view('admin.settings.machines.part-categories.index')
            ->with('categories', $categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return redirect()->route('all.parts.categories')
            ->with('new', true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'category'    => 'required|unique:machine_part_categories,category',
            'description' => 'nullable|string'
        ]);

        $category = MachinePartCategory::create($request->all());
        $message  = "Machine parts category {$category->category} was successfully created";
        Session::flash('created', $message);

        return response()->json([
            'status' => 'success',
            'message' => $message,
            'redirect' => route('all.parts.categories')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $category = MachinePartCategory::findOrFail($id);

        return view('admin.settings.machines.part-categories.show')
            ->with('category', $category);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        return redirect()->route('show.parts.category', $id)
            ->with('edit', true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'category'    => 'required|unique:machine_part_categories,category,'.$request->category.',category',
            'description' => 'nullable|string'
        ]);

        $category = MachinePartCategory::findOrFail($id);
        $category->update($request->all());
        $message = "Machine parts category {$category->category} was successfully updated";
        Session::flash('updated', $message);
        return response()->json([
            'status'   => 'success',
            'message'  => $message,
            'redirect' => route('show.parts.category', $id)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $category = MachinePartCategory::findOrFail($id);
        $message = "Machine parts category {$category->category} successfully deleted";

        if ($category->deletable()) {
            if ($category->delete()) {
                Session::flash('deleted', $message);

                return response()->json([
                    'status' => 'success',
                    'message' => $message,
                    'redirect_url' => route('all.parts.categories')
                ]);
            }
        }

        return response()->json([
            'status' => 'error',
            'message' => 'something went wrong. Try again'
        ]);
    }
}
