<?php

namespace App\Http\Controllers;

use App\KnowledgeBase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Repositories\KnowledgeBaseRepository;

class KnowledgeBaseController extends Controller
{
    //

    public $repo;

    public function __construct(KnowledgeBaseRepository $repo)
    {
        $this->repo = $repo;
    }
    /**
     * show all kb
     * 
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kbs = KnowledgeBase::where('hidden', false)->orderBy('created_at', 'desc')->paginate(5);

        return view('admin.kb.index')
            ->with('kbs', $kbs);
    }

    protected function categories()
    {
        return $this->repo->categories();
    }

    /**
     * show specific kb
     * 
     * @param string $ref
     * @return \Illuminate\Http\Response
     */
    public function show($ref)
    {
        $kb = $this->repo->readPost($ref);
        return view('admin.kb.show')
            ->with('kb', $kb);
    }

    /**
     * create new kb post
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $category = null;
        if ($request->category) {
            $category = $request->category;
        }

        return view('admin.kb.create')
            ->with('categories', $this->repo->categories())
            ->with('category_id', $category);
    }

    /**
     * store new kb post
     * @param \Illuminate\Http\Request $request
     * @param \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = $this->repo->store($request);
        $message = "Post with title \"{$post->title}\" was successfully made!";

        return redirect()->route('admin.show.kb', $post->ref)
            ->with('created', $message);
    }


    /**
     * edit knowledge base
     * 
     * @param string $ref
     */
    public function edit($ref)
    {
        $kb = $this->repo->findByRef($ref);
        $categories = $this->repo->categories();

        return view('admin.kb.edit')
            ->with('kb', $kb)
            ->with('categories', $categories);
    }

    /**
     * update kb post
     * 
     * @param \Illuminate\Http\Request $request
     * @param string $ref
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $ref)
    {
        $post = $this->repo->findByRef($ref);
        if ($this->repo->update($request, $post)) {
            $message = "post {$post->ref} updated";
            return redirect()->route('admin.show.kb', $ref)
                ->with('post_updated', $message);
        }
    }

    /**
     * like post
     * 
     * @param string $ref
     * @return \Illuminate\Http\Response
     */
    public function likePost($ref)
    {
        $post = $this->repo->likePost($ref);
        $message = "post successfully liked";

        return response()->json([
            "status"  => "success",
            "message" => $message,
            "data"    => $post
        ]);
    }

    /**
     * dislike post
     * 
     * @param string $ref
     * @return \Illuminate\Http\Response
     */
    public function dislikePost($ref)
    {
        $post = $this->repo->dislikePost($ref);
        $message = "post successfully disliked";

        return response()->json([
            "status"  => "success",
            "message" => $message,
            "data"    => $post
        ]);
    }

    /**
     * get details of a kb post as json
     */
    public function json($ref)
    {
        return $this->repo->postJson($ref);
    }

    /**
     * make a comment 
     * 
     * @param \Illuminate\Http\Request $request
     * @param string $ref
     * @return \illuminate\Http\Response
     */
    public function makeComment(Request $request, $ref)
    {
        $post = $this->repo->makeCommentOnPost($request, $ref);
        
        $message = "comment successfully posted";
        Session::flash('commented', $message);

        return response()->json([
            "status" => "success",
            "message" => $message,
        ]);
    }

    /**
     * get all comments to a kb
     * 
     * @param string $ref
     * @return \Illuminate\Http\Response
     */
    public function getComments($ref)
    {
        $post = $this->repo->getPostComments($ref);
        return response()->json([
            "status" => "success",
            "message" => "comments successfully fetched",
            "data" => $post,
        ]);
    }

    /**
     * delete post
     * 
     * @param string $ref
     * @return \Illuminate\Http\Response
     */
    public function deletePost($ref)
    {
        $post = $this->repo->findByRef($ref);
        $message = "Post, {$post->ref} was successfully deleted";
        if ($this->repo->deletePost($post)) {
            Session::flash('post_deleted', $message);

            return response()->json([
                "status" => "success",
                "message" => $message,
                "redirect_url" => route('admin.kb')
            ]);
        }
        return response()->json([
            "status" => "error",
            "message" => "something went wrong! Try again"
        ]);
    }


    /**
     * delete a post comment
     * 
     * @param string $ref
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function deletePostComment($ref, $id)
    {
        $post = $this->repo->findByRef($ref);
        $message = "comment was successfully deleted";

        if ($this->repo->deletePostComment($post, $id)) {

            Session::flash("comment_deleted", $message);
            return response()->json([
                "status"       => "success",
                "message"      => $message,
                "redirect_url" => route('admin.show.kb', $ref)
            ]);
        }

        return response()->json([
            "status"  => "error",
            "message" => "comment was not deleted! Try again"
        ]);
    }

    /**
     * update post comment
     * 
     * @param \Illuminate\Http\Request $request
     * @param string $ref
     * @param int $comment_id
     * @return \Illuminate\Http\Response
     */
    public function updatePostComment(Request $request, $ref, $comment_id)
    {
        $post = $this->repo->findByRef($ref);
        if ($this->repo->updatePostComment($request, $post, $comment_id)) {
            $message = "comment successfully updated";
            Session::flash('comment_updated', $message);
            
            return response()->json([
                "status" => "success",
                "message" => $message,
            ]);
        }
        return response()->json([
            "status" => "error",
            "message" => "comment not updated! Try again!"
        ]);
    }
}
