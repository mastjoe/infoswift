<?php

namespace App\Http\Controllers;

use App\User;
use App\Client;
use App\BranchCustodian;
use App\Helpers\Util;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
    //
    public function sms()
    {
        $users = User::all();
        $custodians = BranchCustodian::all();
        $clients = Client::all();

        return view('admin.messaging.sms')
            ->with('users', $users)
            ->with('custodians', $custodians)
            ->with('clients', $clients);
    }

    public function email()
    {
        return view('admin.messaging.email');
    }

    public function pm()
    {
        return view('admin.messaging.pm');
    }

    public function chat()
    {
        return view('admin.messaging.chats.index')
            ->with('other_staff', $this->other_staff());
    }

    protected function other_staff()
    {
        $user = Auth::user();
        return User::all()->filter(function($u) use($user) {
            if ($u->id != $user->id) {
                return $u;
            }
        });
    }
}
