<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    //

    public function showLocations()
    {
        $this->canManageLocation();
        return view('admin.settings.locations.index');
    }

    public function showMachines()
    {
        $this->canManageMachine();
        return view('admin.settings.machines.index');
    }

    /**
     * ensures user is authorize to manage client locations
     */
    protected function canManageLocation() {
        $user = Auth::user();
        if (!$user->hasPrivilege('manage client locations')) {
            abort(403);
        }
    }

    /**
     * ensures user is authorize to manage any of machine settings
     */
    protected function canManageMachine() {
        $user  = Auth::user();
        if (
            !$user->hasPrivilege('manage machine faults') &&
            !$user->hasPrivilege('manage machine vendors') &&
            !$user->hasPrivilege('manage machine statuses') &&
            !$user->hasPrivilege('manage machine types') &&
            !$user->hasPrivilege('manage machine part categories')
        ) {
            abort(403);
        }
    }
}
