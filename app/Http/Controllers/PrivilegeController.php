<?php

namespace App\Http\Controllers;

use App\Privilege;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class PrivilegeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $privileges = Privilege::all();

        return view('admin.staff.privileges.index')
            ->with('privileges', $privileges);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $privilege = Privilege::findOrFail($id);

        return view('admin.staff.privileges.show')
            ->with('privilege', $privilege);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'description' => 'nullable|string'
        ]);

        $privilege = Privilege::findOrFail($id);
        $privilege->update(['description' => $request->description]);
        $message = "Privilege {$privilege->privilege} was successfully updated!";
        Session::flash('privilege_updated', $message);

        return response()->json([
            'status' => 'success',
            'message' => $message
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
