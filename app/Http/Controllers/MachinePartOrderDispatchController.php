<?php

namespace App\Http\Controllers;

use App\MachinePart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Repositories\MachinePartOrderRepository;

class MachinePartOrderDispatchController extends Controller
{
    //
    public $repo;

    public function __construct(MachinePartOrderRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * show all dispatches for specific order
     * 
     * @param string $order_ref
     * @return \Illuminate\Http\Response
     */
    public function index($order_ref)
    {
        $this->authorize('manageOrders', MachinePart::class);
        $order = $this->repo->getOrderByRef($order_ref);

        return view('admin.machines.orders.dispatches.index')
        ->with('order', $order);
    }

    /**
     * create a new dispatch session for order
     * 
     * @param string $ref
     * @return \Illuminate\Http\Response
     */
    public function store($order_ref)
    {
        $order = $this->repo->getOrderByRef($order_ref);
        $dispatch = $this->repo->createOrderDispatch($order);

        $message = "dispatch session, {$dispatch->ref} successfully created for order, {$order->ref}";
        Session::flash("dispatch_created", $message);

        return response()->json([
            "status"       => "success",
            "message"      => $message,
            "redirect_url" => route('show.machine.parts.order.dispatch', $dispatch->ref)
        ]);
    }

    /**
     * show details of a specific order dispatch
     * 
     * @param string $ref
     * @return \Illuminate\Http\Response
     */
    public function show($ref)
    {
        $dispatch = $this->repo->getDispatchByRef($ref);

        return view('admin.machines.orders.dispatches.show')
            ->with('order', $dispatch->order)
            ->with('dispatch', $dispatch);

    }


    /**
     * delete parts order dispatch
     * 
     * @param string $ref
     * @return \Illuminate\Http\Response
     */
    public function destroy($ref)
    {
        $dispatch = $this->repo->getDispatchByRef($ref);
        if ($this->repo->deleteOrderDispatch($dispatch)) {
            $message = "dispatch session, {$dispatch->ref} was successfully deleted";
            Session::flash('dispatch_deleted', $message);

            return response()->json([
                "status"  => "success",
                "message" => $message,
                'redirect_url' => route('show.machine.parts.order.dispatches', $dispatch->order->ref)
            ]);
        }
        return response()->json([
            "status"  => "error",
            "message" => "dispatch session was not deleted! Try again"
        ]);
    }
}
