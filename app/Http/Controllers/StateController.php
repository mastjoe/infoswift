<?php

namespace App\Http\Controllers;

use Session;
use App\Client;
use App\Country;
use App\CountryState;
use App\EngineerState;
use App\Helpers\Util;
use App\User;
use Illuminate\Http\Request;

class StateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $states = CountryState::all();
        $countries = Country::all();

        return view('admin.settings.locations.states.index')
            ->with('states', $states)
            ->with('countries', $countries);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return redirect()->route('all.states')
            ->with('new', true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'country' => 'required',
            'state'   => 'required|string',
            'zone'    => 'nullable|string'
        ]);

        $country = Country::findOrFail($request->country);
        $this->validateStateForACountry($country, $request->state);

        $state = $country->states()->create([
            'state' => $request->state,
            'zone'  => $request->zone
        ]);

        $message = "State, ".$state->state." successfully added to ".$country->country;
        Session::flash('created', $message);

        return response()->json([
            'status'       => 'success',
            'message'      => $message,
            'redirect_url' => route('all.states')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $state = CountryState::with(['country', 'engineers'])->findOrFail($id);
        $countries = Country::all();
        $clients = Client::all();
        $engineers = User::usersWithRole('engineer');


        return view('admin.settings.locations.states.show')
            ->with('state', $state)
            ->with('countries', $countries)
            ->with('clients', $clients)
            ->with('engineers', $engineers);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        CountryState::findOrFail($id);
        return redirect()->route('show.state', $id)
            ->with('edit', true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'state' => 'required|string',
            'zone' => 'nullable|string'
        ]);

        $state = CountryState::findOrFail($id);

        // $this->validateStateForACountry($state->country, $request->state, $id);

        $state->state = $request->state;
        $state->zone = $request->zone;
        $state->save();

        $message = "State, ".$state->state." was successfully updated!";
        Session::flash('updated', $message);

        return response()->json([
            'status' => 'success',
            'message' => $message,
            'redirect_url' => route('show.state', $id)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $state = CountryState::findOrFail($id);

        if ($state->deletable()) {
            $message = "State, ".$state->state." was successfully deleted!";
            if ($state->delete()) {
                Session::flash('deleted', $message);
                return response()->json([
                    'status'       => 'success',
                    'message'      => $message,
                    'redirect_url' => route('all.states')
                ]);
            }
        }
    }

    public function validateStateForACountry(Country $country, string $state, int $update=null)
    {
        $t =  CountryState::where('country_id', $country->id)->where('state', $state);
        $count = $t->count();

        if ($update) {
            $first = $t->first();
            // if ($first->id != $update && $count) {
            //     Util::pushError([
            //         'state' => 'state already exist in country'
            //     ]);
            // }
        }
        if ($count) {
            Util::pushError([
                'state' => 'state already exist in country'
            ]);
        }
    }

    public function createRegion($id)
    {
        $state = CountryState::findOrFail($id);
        return redirect()->route('show.state', $id)
            ->with('create_region', true);
    }

    public function storeRegion(Request $request, $id)
    {
        $request->validate([
            'client' => 'required',
            'region' => 'required|string'
        ]);

        $state = CountryState::findOrFail($id);
        $region = $state->regions()->create([
            'country_id' => $state->country->id,
            'region'     => $request->region,
            'client_id'  => $request->client,
        ]);
        
        $message = 
        "Region, ".$region->region." was successfully added to state, ".$state->state."!";
        Session:: flash('created_region', $message);
        return response()->json([
            'status'       => 'success',
            'message'      => $message,
            'redirect_url' => route('show.state', $id)
        ]);
    }

    public function assignEngineerForm($id)
    {
        $state = CountryState::findOrFail($id);
        return redirect()->route('show.state', $id)
            ->with('assign_engineer', true);
    }

    public function assignEngineer(Request $request, $id)
    {
        $request->validate([
            'engineers' => 'required|min:1'
        ]);

        Util::assignEngineersToState($id, $request->engineers);

        $message = 'Engineers were successfully assigned to state';
        Session::flash('assigned_engineers', $message);

        return response()->json([
            'status'=> 'success',
            'message' => $message,
            'redirect_url' => route('show.state', $id)
        ]);
    }

    public function dropEngineer($id, $engineer_id)
    {
        $state = CountryState::findOrFail($id);
        $engineer = User::findOrFail($engineer_id);

        $target = EngineerState::where('country_state_id', $id)
            ->where('user_id', $engineer_id);
        $message = "Engineer, {$engineer->full_name()}  was successfully dropped from {$state->state} state";
        if ($target->count()) {
            $target->delete();
            Session::flash('dropped_engineer', $message);
            return response()->json([
                'status' => 'success',
                'message' => $message,
                'redirect_url' => route('show.state', $id)
            ]);
        }
    }
}
