<?php

namespace App\Http\Controllers;

use Session;
use App\MachineStatus;
use Illuminate\Http\Request;

class MachineStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $statuses = MachineStatus::all();

        return view('admin.settings.machines.statuses.index')
            ->with('statuses', $statuses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return redirect()->route('all.machine.statuses')
            ->with('new', true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'status'      => 'required|string|unique:machine_statuses,status',
            'description' => 'nullable|string'
        ]);

        $status = MachineStatus::create([
            'status'      => $request->status,
            'description' => $request->description
        ]);

        $message = "Machine status, ".$status->status." was successfully created!";

        Session::flash('created', $message);
        return response()->json([
            'status'       => 'success',
            'message'      => $message,
            'redirect_url' => route('all.machine.statuses')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $status = MachineStatus::findOrFail($id);

        return view('admin.settings.machines.statuses.show')
            ->with('status', $status);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $status = MachineStatus::findOrFail($id);

        return redirect()->route('show.machine.status', $status->id)
            ->with('edit', true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'status' => 'required|string|unique:machine_statuses,status,'.$request->status.',status',
            'description' => 'nullable|string',
        ]);

        $status              = MachineStatus::findOrFail($id);
        $status->status      = $request->status;
        $status->description = $request->description;
        $status->save();

        $message = "Machine status, ".$status->status." was successfully updated!";

        Session:: flash('updated', $message);

        return response()->json([
            'status'       => 'success',
            'message'      => $message,
            'redirect_url' => route('show.machine.status', $status->id)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $status = MachineStatus::findOrFail($id);
        if ($status->deletable()) {
            if ($status->delete()) {
                $message = "Machine status, ".$status->status." was successfully deleted!";
                Session:: flash('deleted', $message);
                return response()->json([
                    'status'       => 'success',
                    'message'      => $message,
                    'redirect_url' => route('all.machine.statuses')
                ]);
            }
        }
        return response()->json([
            'status'  => 'error',
            'message' => 'status could not be deleted! Try again!'
        ]);
    }
}
