<?php

namespace App\Http\Controllers;

use App\KnowledgeBase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Repositories\KnowledgeBaseRepository;

class KnowledgeBaseCategoryController extends Controller
{

    public $repo;

    public function __construct(KnowledgeBaseRepository $repo)
    {
        $this->repo = $repo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $kbs = KnowledgeBase::all();
        $categories = $this->repo->categories();

        return view('admin.kb.categories.index')
            ->with('kbs', $kbs)
            ->with('categories', $categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return redirect()->route('admin.kb.categories')
            ->with('create_new', true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $category = $this->repo->createNewCategory($request);
        $message = "new category successfully created";
        Session::flash('new_category', $message);

        return response()->json([
            "status" => "success",
            "message" => $message,
            "redirect" => route('admin.kb.show.category', $category->slug)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        //
        $category = $this->repo->getCategory($slug);
        $kbs = KnowledgeBase::all();
        $categories = $this->repo->categories();
        $posts = $category->posts()->orderBy('created_at', 'desc')->paginate(5);

        return view('admin.kb.categories.show')
            ->with('kbs', $kbs)
            ->with('categories', $categories)
            ->with('category', $category)
            ->with('posts', $posts);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        //
        $category = $this->repo->getCategory($slug);

        return redirect()->route('admin.kb.show.category', $category->slug)
            ->with('edit_category', true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        //
        $category = $this->repo->updateCategory($request, $slug);
        $message = "category, {$category->category} successfully updated!";
        Session::flash('category_updated', $message);

        return response()->json([
            "status" => "success",
            "message" => $message,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        //
        $category = $this->repo->getCategory($slug);
        if ($this->repo->deleteCategory($category)) {
            $message = "category {$category->category} was successfully deleted";
            Session::flash('category_deleted', $message);

            return response()->json([
                "status" => "success",
                "message" => $message,
                "redirect_url" => route('admin.kb.categories')
            ]);
        }
        return response()->json([
            "status" => "error",
            "message" => "category was not deleted! Try again"
        ]);
    }
}
