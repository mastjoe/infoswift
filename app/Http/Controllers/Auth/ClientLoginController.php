<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ClientLoginController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->middleware('guest:client')->except('logout');
    }

    public function showLoginForm()
    {
        return view('auth.client-login');
    }

    public function login(Request $request)
    {
        $request->validate([
            'user_code' => 'required|string',
            'password' => 'required|min:6'
        ]);

        $credentials = [
            'code' => $request->user_code,
            'password' => $request->password,
        ];

        if (Auth::guard('client')->attempt($credentials, $request->remember)) {
            return redirect()->intended(route('client.dashboard'));
        }
        
        $errors = new MessageBag();
        $errors->add('user_code', 'Invalid client login credentials');
        return back()->withInput($request->only('user_code', 'remember'))
            ->withErrors($errors);
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return $request->wantsJson()
            ? new Response('', 204)
            : redirect()->route('client.login.form');
    }

    protected function guard()
    {
        return Auth::guard();
    }

}
