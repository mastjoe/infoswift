<?php

namespace App\Http\Controllers;

use Session;
use App\Client;
use App\Region;
use App\CountryState;
use App\Helpers\Util;
use Illuminate\Http\Request;

class RegionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $regions = Region::all();
        $clients = Client::all();
        $states = CountryState::all();

        return view('admin.settings.locations.regions.index')
            ->with('regions', $regions)
            ->with('clients', $clients)
            ->with('states', $states);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return redirect()->route('all.regions')
            ->with('new', true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'region' => 'required|string',
            'state'  => 'required',
            'client' => 'required'
        ]);

        $target = Region::where('client_id', $request->client)
            ->where('region', $request->region);
            
        if ($target->count()) {
            Util::pushError([
                'region' => 'region already exist for client'
            ]);
        }

        $state = CountryState::with(['country'])->findOrFail($request->state);
        $region = $state->regions()->create([
            'region'     => $request->region,
            'client_id'  => $request->client,
            'country_id' => $state->country->id
        ]);

        $message = "Region ".$region->region." was successfully created!";
        Session::flash('created', $message);
        return response()->json([
            'status' => 'success',
            'message' => $message,
            'redirect_url' => route('all.regions')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $region = Region::with(['country'])->findOrFail($id);
        $states = CountryState::all();
        $clients = Client::all();

        return view('admin.settings.locations.regions.show')
            ->with('region', $region)
            ->with('states', $states)
            ->with('clients', $clients);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $region = Region::findOrFail($id);
        return redirect()->route('show.region', $id)
            ->with('edit', true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'region' => 'required|string',
            'state'  => 'required',
            'client' => 'required'
        ]);

        $old_region = Region::where('region', $request->region)
            ->where('client_id', $request->client)
            ->where('state_id', $request->state);
        if ($old_region->count()) {
            if ($old_region->first()->id != $id) {
                Util::pushError([
                    'region' => 'region has been taken for state'
                ]);
            }
        }

        $region = Region::findOrFail($id);
        $region->region = $request->region;
        $region->state_id = $request->state;
        $region->client_id = $request->client;
        $region->save();
        
        $message = "Region, ".$region->region." was successfully updated!";
        Session::flash('updated', $message);
        return response()->json([
            'status' => 'success',
            'message' => $message,
            'redirect_url' => route('show.region', $id)
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $region = Region::findOrFail($id);

        $message = "Region ".$region->region." was successfully deleted";
        if ($region->deletable()) {
            if ($region->delete()) {
                Session::flash('deleted', $message);
                return response()->json([
                    'status'       => 'success',
                    'message'      => $message,
                    'redirect_url' => route('all.regions')
                ]);
            }
        }

        return response()->json([
            'status'  => 'error',
            'message' => 'Region was not deleted! Try again!'
        ]);
    }

    public function createBranch($id)
    {
        $region = Region::findOrFail($id);

        return redirect()->route('show.region', $id)
            ->with('create_branch', true);
    }

    public function storeBranch(Request $request, $id)
    {
        $request->validate([
            'branch'        => 'required',
            'sol_id'        => 'nullable|string|unique:branches,sol_id',
            'phone_numbers' => 'nullable|string',
            'emails'        => 'nullable|string',
            'address'       => 'nullable|string'
        ]);

        $region = Region::findOrFail($id);
        $bc = new BranchController;
        $request->request->add(['region_id' => $id]);
        $bc->validateBranch($request);
        
        $branch = $region->branches()->create([
            'branch'     => $request->branch,
            'sol_id'     => $request->sol_id,
            'phones'     => $bc->resolvePhoneNumbers($request->phone_numbers),
            'emails'     => $bc->resolveEmails($request->emails),
            'country_id' => $region->country_id,
            'region_id'  => $region->id,
            'state_id'   => $region->state_id,
            'client_id'  => $region->client_id,
            'address'    => $request->address
        ]);

        $message = "Branch, ".$branch->branch." was successfully created!";
        Session::flash('created_branch', $message);
        return response()->json([
            'status'       => 'success',
            'message'      => $message,
            'redirect_url' => route('show.region', $id)
        ]);

    }
}
