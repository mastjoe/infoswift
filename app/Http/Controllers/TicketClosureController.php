<?php

namespace App\Http\Controllers;

use App\Ticket;
use Carbon\Carbon;
use App\TicketFault;
use App\Helpers\Util;
use App\MachineFault;
use App\Events\CloseTicket;
use Illuminate\Http\Request;
use App\Events\DeclineTicket;
use App\Events\ConfirmCloseTicket;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Repositories\TicketClosureRepository;

class TicketClosureController extends Controller
{
    //

    public $ticketClosureRep;

    public function __construct(TicketClosureRepository $ticketClosureRep)
    {
        $this->ticketClosureRep = $ticketClosureRep;
    }

    public function show($id)
    {
        $ticket = Ticket::findOrFail($id);
       
        return view('admin.tickets.close')
            ->with('ticket', $ticket);
    }

    public function close(Request $request, $id)
    {
        $ticket = Ticket::findOrFail($id);
        $this->ticketClosureRep->closeTicket($request, $ticket);

        $message = "Ticket successfully closed";
        return redirect()->route('show.ticket', $ticket->id)
            ->with('first_closed', $message);
    }

    /**
     * confirm closure ticket form
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function confirmCloseForm($id)
    {
        $ticket = Ticket::findOrFail($id);

        if ($ticket->closed_at && !$ticket->confirmed_at) {
            return redirect()->route('close.ticket', $ticket->id)
                ->with('confirm_closure_form', true);
        }
        return redirect()->back();
    }

    /**
     * confirm closure of ticket
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function confirmClose(Request $request, $id)
    {        
        $ticket = Ticket::findOrFail($id);
        
        $this->ticketClosureRep->approveTicketClosure($request, $ticket);

        $message = "ticket closure successfully confirmed";
        Session::flash('confirmed', $message);
        return response()->json([
            'status'   => 'success',
            'message'  => $message,
            'redirect' => route('show.close.ticket.form', $ticket->id)
        ]);
    }

    /**
     * get declined closure form
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function declineCloseForm($id)
    {
        $ticket = Ticket::findOrFail($id);
        if ($ticket->can_decline()) {
            return redirect()->route('close.ticket', $id)
                ->with('decline_closure_form', $id);
        }
        return redirect()->back();
    }

    /**
     * decline ticket closure
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function declineClose(Request $request, $id)
    {
        $ticket = Ticket::findOrFail($id);
        $this->ticketClosureRep->disapproveTicketClosure($request, $ticket);

        $message = "ticket {$ticket->ref} was successfully declined";
        Session::flash('declined', $message);

        return response()->json([
            'status' => 'success',
            'message' => $message,
            'redirect' => route('close.ticket', $ticket->id)
        ]);
    }

    /**
     * reclose ticket
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function reCloseForm($id)
    {
        $ticket = Ticket::findOrFail($id);
        if ($ticket->declined_at) {
            return redirect()->route('close.ticket', $id)
                ->with('reclose', true);
        }
        return redirect()->back();
    }

    /**
     * update ticket closure information
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ticket = Ticket::findOrFail($id);

        $this->ticketClosureRep->recloseTicket($request, $ticket);
        $message = "ticket was successfully reclosed";
        session::flash('reclosed', $message);

        return response()->json([
            'status'   => 'success',
            'message'  => $message,
            'redirect' => route('show.ticket', $ticket->id)
        ]);
    }

    /**
     * gets solutions to specific terminal fault
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function machineFaultSolutions($id)
    {
        $machine_fault = MachineFault::findOrFail($id);
        $solutions = $machine_fault->ticketFaultSolutions;

        return view('admin.tickets.old-solutions')
            ->with('machine_fault', $machine_fault)
            ->with('solutions', $solutions);
    }

}
