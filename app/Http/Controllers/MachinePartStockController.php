<?php

namespace App\Http\Controllers;

use App\MachinePart;
use App\Helpers\Util;
use App\MachinePartStock;
use App\MachinePartSupplier;
use Illuminate\Http\Request;
use App\MachinePartStockSupply;
use App\Policies\MachinePartPolicy;
use Illuminate\Support\Facades\Session;

class MachinePartStockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $stock = MachinePartStock::all();
        $this->authorize('manageStocks', MachinePart::class);

        return view('admin.machines.stocks.index')
            ->with('stocks', $stock);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $parts = MachinePart::all();
        $suppliers = MachinePartSupplier::all();
        $this->authorize('manageStocks', MachinePart::class);

        return view('admin.machines.stocks.create')
            ->with('parts', $parts)
            ->with('suppliers', $suppliers);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'part'          => 'required',
            'part_number'   => 'required|string|unique:machine_part_stocks,part_number',
            'supplier'      => 'nullable',
            'model'         => 'nullable|string',
            'serial_number' => 'nullable|string',
            'cost_price'    => 'required',
            'selling_price' => 'required',
            'quantity'      => 'required'
        ]);

       $part = MachinePart::findOrFail($request->part);
       $stock = $part->stocks()->create([
           'part_number'      => $request->part_number,
           'serial_number'    => $request->serial_number,
           'model'            => $request->model,
       ]);

       // create supply record...
       $supply = $stock->supplies()->create([
           'quantity'         => $request->quantity,
           'initial_quantity' => $request->quantity,
           'supplier_id'      => $request->supplier,
           'selling_price'    => Util::normalizeAmount($request->selling_price),
           'cost_price'       => Util::normalizeAmount($request->cost_price)
       ]);

       $message = "Part successfully added to stock";

       return redirect()->route('show.machine.stock', $stock->id)
        ->with('created', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $stock = MachinePartStock::with(['machine_part'])
            ->findOrFail($id);

        $this->authorize('manageStocks', MachinePart::class);
        $suppliers = MachinePartSupplier::all();

        return view('admin.machines.stocks.show')
            ->with('stock', $stock)
            ->with('part', $stock->machine_part)
            ->with('suppliers', $suppliers);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $stock = MachinePartStock::findOrFail($id);
        $this->authorize('manageStocks', MachinePart::class);

        return redirect()->route('show.machine.stock', $id)
            ->with('edit_stock', true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'part_number' => 
            'required|string|unique:machine_part_stocks,part_number,'.$request->part_number.',part_number',
            'model'         => 'nullable|string',
            'serial_number' => 'nullable|string',
        ]);

        $stock = MachinePartStock::findOrFail($id);
        $stock->update([
            'part_number'   => $request->part_number,
            'model'         => $request->model,
            'serial_number' => $request->serial_number
        ]);

        $message = "Machine part stock, {$stock->part_number} was successfully updated!";
        Session::flash('updated', $message);

        return response()->json([
            'status' => 'success',
            'message' => $message,
            'redirect' => route('show.machine.stock', $id)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $stock = MachinePartStock::findOrFail($id);
        $this->authorize('manageStocks', MachinePart::class);

        if ($stock->deletable()) {
            $message = "Machine Stock {$stock->part_number} was successfully deleted";

            if ($stock->delete()) {
                Session::flash('deleted', $message);
                
                return response()->json([
                    'status'       => 'success',
                    'message'      => $message,
                    'redirect_url' => route('all.machine.stock')
                ]);
            }
        }

        return response()->json([
            'status'  => 'error',
            'message' => 'something went wrong! Try again!'
        ]);
    }

    /**
     * create supply for stock
     * 
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function createSupply($id)
    {
        $stock = MachinePartStock::findOrFail($id);
        $this->authorize('manageSupply', MachinePart::class);

        return redirect()->route('show.machine.stock', $id)
            ->with('add_supply', true);
    }

    /**
     * stores stock supply
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function storeSupply(Request $request, $id)
    {
        $request->validate([
            'cost_price'    => 'required',
            'selling_price' => 'required',
            'quantity'      => 'required|min:1',
            'supplier'      => 'nullable'
        ]);

        $stock = MachinePartStock::findOrFail($id);
        $supply =$stock->supplies()->create([
            'cost_price'    => $request->cost_price,
            'selling_price' => $request->selling_price,
            'quantity'      => $request->quantity,
            'supplier_id'      => $request->supplier
        ]);

        $message = "Supply successfully added to stock {$stock->part_number}";
        Session::flash('supply_added', $message);

        return response()->json([
            'status' => 'success',
            'message' => $message,
            'redirect' => route('show.machine.stock', $id)
        ]);
    }

    /**
     * show supply details
     * 
     * @param int $id
     * @param int $supply_id
     * @return \Illuminate\Http\Response
     */
    public function showSupply($id, $supply_id)
    {
        $stock = MachinePartStock::findOrFail($id);
        $supply = MachinePartStockSupply::with(['stock', 'stock.machine_part'])
            ->findOrFail($supply_id);
        $suppliers = MachinePartSupplier::all();

        $this->authorize('manageSupply', MachinePart::class);

        return view('admin.machines.stocks.show-supply')
            ->with('stock', $supply->stock)
            ->with('supply', $supply)
            ->with('part', $supply->stock->machine_part)
            ->with('suppliers', $suppliers);
    }

    /**
     * edit supply details
     * @param int $id
     * @param int $supply_id
     */
    public function editSupply($id, $supply_id)
    {
        $supply = MachinePartStockSupply::findOrFail($supply_id);

        $this->authorize('manageSupply', MachinePart::class);

        return redirect()->route('show.stock.supply', [$id, $supply_id])
            ->with('edit_supply', true);
    }

    /**
     * update supply details
     * 
     * @param \Illuminate\Http\Request
     * @param int $id
     * @param int $supply_id
     * @return \Illuminate\Http\Response
     */
    public function updateSupply(Request $request, $id, $supply_id)
    {
        $request->validate([
            'cost_price'    => 'required',
            'selling_price' => 'required',
            'quantity'      => 'required|min:1',
            'supplier'      => 'nullable'
        ]);

        $supply = MachinePartStockSupply::findOrFail($supply_id);
        $supply->update([
            'cost_price'       => $request->cost_price,
            'selling_price'    => $request->selling_price,
            'supplier_id'      => $request->supplier,
            'quantity'         => $request->quantity,
            'initial_quantity' => $request->quantity
        ]);

        $message = "supply {$supply->reference} was successfully updated";
        Session::flash('supply_updated', $message);

        return response()->json([
            'status' => 'success',
            'message' => $message,
            'redirect' => route('show.stock.supply', [$supply->stock_id, $supply_id])
        ]);
    }

    /**
     * delete supply
     * 
     * @param int $id
     * @param int $supply_id
     * @return \Illuminate\Http\Response
     */
    public function deleteSupply($id, $supply_id)
    {
        $supply = MachinePartStockSupply::findOrFail($supply_id);
        $this->authorize('manageSupply', MachinePart::class);

        if ($supply->deletable()) {
            $message = "supply {$supply->reference} was successfully deleted!";
            if ($supply->delete()) {
                Session::flash('supply_deleted', $message);
                return response()->json([
                    'status'       => 'success',
                    'message'      => $message,
                    'redirect_url' => route('show.machine.stock', $id)
                ]);
            }
        }
        return response()->json([
            'status' => 'success',
            'message' => 'something went wrong! try again!'
        ]);
    }
}
