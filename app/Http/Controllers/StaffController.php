<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use App\UserRole;
use Carbon\Carbon;
use App\Helpers\Util;
use App\Helpers\Photo;
use App\UserGuarantor;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Session;

class StaffController extends Controller
{
    public $userRepo;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepo = $userRepo;
    } 

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $this->authorize('viewAny', User::class);
        $users = User::paginate(9);

        return view('admin.staff.index')
            ->with('users', $users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $roles = Role::all();
        $this->authorize('create', User::class);

        return view('admin.staff.add')
            ->with('roles', $roles);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $user = $this->userRepo->create($request);      
        
        return redirect()->route('show.staff', $user->id)
            ->with(
                'new_staff',
                'New Staff, '.$user->full_name().' was successfully created!'
            );
        
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $user = User::findOrFail($id);
        $this->authorize('view', $user);
        
        return view('admin.staff.show')
            ->with('user', $user);        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $user = User::findOrFail($id);
        $this->authorize('update', $user);

        return view('admin.staff.edit')
            ->with('user', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $user = $this->userRepo->update($request, $id);
        return redirect()->route('show.staff', $user->id)
            ->with(
                'updated_staff',
                'Staff, '.$user->full_name().' was successfully updated!'
            );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user = User::findOrFail($id);
        $this->authorize('delete', $user);

        if ($this->userRepo->delete($id)) {
            $message = 'Staff, ' . $user->full_name() . ' was successfully deleted!';
            Session::flash('deleted_staff', $message);

            return response()->json([
                'status'       => 'success',
                'message'      => $message,
                'redirect_url' => route('all.staff')
            ]);
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Staff could not be deleted!'
        ]);
    }

    /**
     * suspends staff
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function suspend($id)
    {
        $user = $this->userRepo->suspend($id);
        
        $message = "staff, {$user->full_name()} was successfully suspended";
        Session::flash('suspended', $message);
        return response()->json([
            'status'       => 'success',
            'message'      => $message,
            'redirect_url' => route('show.staff', $user->id)
        ]);
    }

    /**
     * unsuspend staff
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function unsuspend($id)
    {
        $user = $this->userRepo->unsuspend($id);

        $message = "staff, {$user->full_name()} was successfully unsuspended";
        session::flash('unsuspended', $message);

        return response()->json([
            'status' => 'success',
            'message' => $message,
            'redirect_url' => route('show.staff', $user->id)
        ]);
    }

    public function showGuarantors($id)
    {
        $user = User::with(['guarantors'])->findOrFail($id);

        return view('admin.staff.guarantors')
            ->with('user', $user);
    }

    public function storeGuarantors(Request $request, $id)
    {
       $user = User::findOrFail($id);
       $guarantor = $this->userRepo->storeGuarantor($request, $id);

        return response()->json([
            'status' => 'success',
            'message' => 'Guarantor\'s record was successfully added! '
        ]);
    }

    public function deleteGuarantor($guarantor_id)
    {
        $g = UserGuarantor::with('user')->findOrFail($guarantor_id);
        if ($this->userRepo->deleteGuarantor($guarantor_id)) {
            $message = 'Guarantor, ' . $g->name . ' successfully deleted';
            Session::flash('deleted_guarantor', $message);
            return response()->json([
                'status'  => 'success',
                'message' => $message,
                'redirect_url' => route('show.staff.guarantors', $g->user->id)
            ]);
        }

        return response()->json([
            'status'  => 'error',
            'message' => 'Guarantor was not deleted! Try again!'
        ]);
    }

    public function editGuarantor($guarantor_id)
    {
        $g = UserGuarantor::findOrFail($guarantor_id);

        return view('admin.staff.edit-guarantor')
            ->with('g', $g);
    }

    public function updateGuarantor(Request $request, $guarantor_id)
    {
       $g = $this->userRepo->updateGuarantor($request, $guarantor_id);
        return response()->json([
            'status'  => 'success',
            'message' => 'Guarantor, '.$g->name.' was successfully updated!'
        ]);
    }

    public function showRoles($id)
    {
        $user = User::with(['roles', 'privileges'])->findOrFail($id);
        $roles = Role::all();
        $roles_privileges =  $this->getRolePrivileges($user->roles);
        $unassigned_privileges = $this->getUnassignedPrivileges($user);

        return view('admin.staff.show-roles')
            ->with('user', $user)
            ->with('roles', $roles)
            ->with('roles_privileges', $roles_privileges)
            ->with('unassigned_privileges', $unassigned_privileges);
    }

    protected function getRolePrivileges(Collection $roles)
    {
        return $roles->map(function($role) {
            return $role->privileges;
        })
        ->flatten()->unique('privilege');
    }

    protected function getUnassignedPrivileges(User $user)
    {
        $assigned_privileges   = $user->privileges;
        $assignable_privileges = $this->getRolePrivileges($user->roles);
        $assigned_ids          = $assigned_privileges->pluck('id')->toArray();
        $unassigned            = [];
        foreach ($assignable_privileges as $p) {
            if (!in_array($p->id, $assigned_ids)) {
                $unassigned[] = $p;
            }
        }

        return collect($unassigned);
    }

    public function updateRoles(Request $request, $id)
    {
        $request->validate([
            'roles' => 'required'
        ]);

        $user = User::findOrFail($id);
        Util::detachAllUserRoles($id);
        Util::attachUserRoles($id, $request->roles);
        $message = "Staff roles were successfully updated!";

        return redirect()->route('show.staff.roles', $id)
            ->with('updated_roles', $message);
    }

    public function updatePrivileges(Request $request, $id)
    {
        $request->validate([
            'privileges' => 'required'
        ]);

        $user = User::findOrFail($id);
        Util::detachAllUserPrivileges($id);
        Util::attachUserPrivileges($id, $request->privileges);
        $message = "Staff privileges were successfully updated!";

        return redirect()->route('show.staff.roles', $id)
            ->with('updated_privileges', $message);

    }

    /**
     * upload staff image
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function updatePhoto(Request $request, $id)
    {
        $request->validate([
            'image' => 'required|image|mimes:png,jpg,gif,jpeg|max:500'
        ]);

        $user = User::findOrFail($id);
        if (Photo::uploadStaffPhoto($request, $id)) {
            $message = "staff photo was successfully updated";
            Session::flash('photo_updated', $message);

            return response()->json([
                'status'   => 'success',
                'message'  => $message,
                'redirect' => route('show.staff', $id)
            ]);
        }
        return response()->json([
            'status'  => 'error',
            'message' => 'staff photo was not updated! Try again'
        ]);
    }

    /**
     * show staff terminals for engineering staff
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function showMachines($id)
    {
        $user = User::findOrFail($id);
        $this->authorize('view', $user);
        $machines = $user->all_state_machines();

        return view('admin.staff.machines')
            ->with('user', $user)
            ->with('machines', $machines);
    }

    /**
     * reset staff password
     * 
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function resetPassword($id)
    {
        $user = User::findOrFail($id);
        $password = $this->userRepo->resetPassword($user);
        if ($password) {
            $message = "Staff password successfully reset to {$password}";
            Session::flash('password_resetted', $message);

            return response()->json([
                'status' => 'success',
                'message' => $message,
                'redirect_url' => route('show.staff', $id)
            ]);
        }
        return response()->json([
            'status' => 'error',
            'message' => 'password reset failed! Try again'
        ]);
    }


    /**
     * assign all privileges from staff role to staff
     * 
     * @param int $id
     * @return Illuminate\Http\Response
     */
    public function assignAllPrivileges($id)
    {
        $staff = User::findOrFail($id);
        // get all staff roles
        $roles_ids = $staff->roles->pluck('id')->toArray();
        Util::detachAllUserRoles($staff->id);
        Util::attachUserRoles($staff->id, $roles_ids);

        $message = "Staff, {$staff->full_name()} 's role was successfully updated";
        return redirect()->back()->with('assigned_privileges', $message);
    }

}
