<?php

namespace App\Http\Controllers;

use Session;
use App\User;
use App\Client;
use App\Helpers\Photo;
use Illuminate\Http\Request;
use App\Repositories\ClientRepository;

class ClientController extends Controller
{
    public $clientRepo;

    public function __construct(ClientRepository $clientRepo)
    {
        $this->clientRepo = $clientRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $this->authorize('viewAny', Client::class);
        $clients = Client::paginate(6);

        return view('admin.clients.index')
            ->with('clients', $clients);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $this->authorize('create', Client::class);
        return view('admin.clients.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $client = $this->clientRepo->create($request);

        return redirect()->route('show.client', $client->id)
            ->with(
                'new_client',
                'New Client, '.$client->short_name.' was successfully added!'
            );

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $client = Client::findOrFail($id);
        $this->authorize('view', $client);
        $supervisors = User::usersWithRole('supervisor');

        return view('admin.clients.show')
            ->with('client', $client)
            ->with('supervisors', $supervisors);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $client = Client::findOrFail($id);
        $this->authorize('update', $client);

        return view('admin.clients.edit')
            ->with('client', $client);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $client = $this->clientRepo->update($request, $id);

        return redirect()->route('show.client', $id)
            ->with(
                "updated_client",
                "Client, $client->short_name was successfully updated"
            );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $client = Client::findOrFail($id);
        $this->authorize('delete', $client);
        
        if ($this->clientRepo->delete($client)) {
            $message = 'Client, ' . $client->short_name . ' was successfully deleted!';
            Session::flash('deleted_client', $message);
            return response()->json([
                'status'       => 'success',
                'message'      => $message,
                'redirect_url' => route('all.clients')
            ]);
        }

        return response()->json([
            'status'  => 'error',
            'message' => 'Client could not be deleted! Try again!'
        ]);
    }

    public function storePersonnel(Request $request, $id)
    {
        $client = $this->clientRepo->updatePersonnel($request, $id);

        return response()->json([
            'status'  => 'success',
            'message' => 'Personnel successfully added for client, '.$client->short_name
        ]);
    }  
    
    public function editLogo($id)
    {
        $client = Client::findOrFail($id);

        return redirect()->route('show.client', $id)
            ->with('edit_logo', true);
    }

    public function storeLogo(Request $request, $id)
    {
        
       if ($this->clientRepo->updateLogo($request, $id)) {
           $message = "Logo successfully uploaded";
            Session::flash('logo_uploaded', $message);
           return response()->json([
               'status' => 'success',
               'message' => $message,
               'redirect_url' => route('show.client', $id)
           ]);
       }
       return response()->json([
           'status' => 'error',
           'message' => 'Logo was not successfully updated!'
       ]);
    }

    public function showMachines($id)
    {
        $client = Client::with('machines')->findOrFail($id);

        return view('admin.clients.machines')
            ->with('client', $client);
    }

    public function showRegions($id)
    {
        $client = Client::with('regions')->findOrFail($id);

        return view('admin.clients.regions')
            ->with('client', $client);
    }

    public function showBranches($id)
    {
        $client = Client::with('branches')->findOrFail($id);

        return view('admin.clients.branches')
            ->with('client', $client);
    }

    /**
     * get machines of specific client in JSON format
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function machinesJSON(Request $request)
    {
        if (!$request->ajax()) {
            abort(404);
        }

        $client = Client::findOrFail($request->id);

        $machines = $client->machines;
        $machines->map(function($machine) {
            $machine->region;
            $machine->branch;
        });

        return response()->json([
            'status'  => 'success',
            'data'    => $machines,
            'message' => 'machines fetched successfully'
        ]);
    }

    /**
     * show or trigger assign supervisor form
     * 
     * @param int $id
     * @return 
     */
    public function assignSupervisorForm($id)
    {
        $client = Client::findOrFail($id);

        return redirect()->route('show.client', $id)->with('assign_supervisor', true);
    }

    /**
     * update client supervisor
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function updateSupervisor(Request $request, $id)
    {
        $updated_client = $this->clientRepo->updateSupervisor($request, $id);
        if ($updated_client) {
            $message = "{$updated_client->supervisor->full_name()} assigned as supervisor";
            Session::flash('assigned_supervisor', $message);

            return response()->json([
                'status' => 'success',
                'message' => $message
            ]);
        }

        return response()->json([
            'status' => 'error',
            'message' => "something went wrong! Try again"
        ]);
    }

    /**
     * reset client password
     * 
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function resetPassword($id)
    {
        $client = Client::findOrFail($id);
        $password = $this->clientRepo->resetPassword($client);
        
        if ($password) {
            $message = "client's password successfully reset to {$password}";
            Session::flash('password_resetted', $message);

            return response()->json([
                'status'       => 'success',
                'message'      => $message,
                'redirect_url' => route('show.client', $id)
            ]);
        }
        return response()->json([
            'status' => 'error',
            'message' => "client password not reset! Try again" 
        ]);
    }
}
