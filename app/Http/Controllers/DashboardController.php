<?php

namespace App\Http\Controllers;

use App\Pm;
use App\User;
use App\Client;
use App\Ticket;
use App\Machine;
use Carbon\Carbon;
use App\Helpers\Util;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    //

    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $staff          = User::all();
        $tickets        = Ticket::all();
        $closed_tickets = Ticket::closedTickets();
        $open_tickets   = Ticket::openTickets();
        $machines       = Machine::all();
        $clients        = Client::all();
        $pms            = Pm::all();

        
        return view('admin.dashboard.index')
            ->with('staff', $staff)
            ->with('tickets', $tickets)
            ->with('closed_tickets', $closed_tickets)
            ->with('open_tickets', $open_tickets)
            ->with('clients', $clients)
            ->with('machines', $machines)
            ->with('pms', $pms)
            ->with('ticket_years', Util::ticketYears());
    }

    /**
     * clients's ticket chart
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function ticketChartData(Request $request)
    {
        $year = Carbon::now()->year;
        if ($request->year) {
            $year = Carbon::parse($request->year)->format('Y');
        }
        
        $clients = Client::all()->map(function($client) use($year) {

            $ticket_data = [];
            for ($i=0; $i < 12; $i++) {
                $month = $year."-".($i+1);
                $ticket_data[] = $this->getClientMonthTickets($client->id, $month)->count();
            }

            return [
                'client'  => $client->short_name,
                'color'   => $client->color,
                'tickets' => $ticket_data
            ];
        }); 

        return response()->json([
            "year"    => $year,
            "months"  => Util::months(),
            "clients" => $clients
        ]);
    }

    /**
     * get a clients ticket for specific months
     */
    protected function getClientMonthTickets(int $client_id, string $month)
    {
        return Ticket::where('client_id', $client_id)
            ->where('created_at', 'LIKE', Carbon::parse($month)->format('Y-m').'%')
            ->get();
    }
}
