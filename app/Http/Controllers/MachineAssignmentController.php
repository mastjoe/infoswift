<?php

namespace App\Http\Controllers;

use App\AuditEvent;
use App\CountryState;
use App\Machine;
use App\User;
use Illuminate\Http\Request;

class MachineAssignmentController extends Controller
{
    //
    public function index()
    {
        $this->authorize('assign', Machine::class);

        $engineers = User::usersWithRole('engineer');
        $states = CountryState::with(['country'])->get();

        return view('admin.machines.assignment.index')
            ->with('engineers', $engineers)
            ->with('states', $states);
    }

    public function show($id)
    {
        $state = CountryState::findOrFail($id);
        $this->authorize('assign', Machine::class);
        $engineers = User::usersWithRole('engineer');

        return view('admin.machines.assignment.show')
            ->with('state', $state)
            ->with('engineers', $engineers);
    }

    public function assignMachineForm($id)
    {
        $state = CountryState::findOrFail($id);
        $this->authorize('assign', Machine::class);

        return view('admin.machines.assignment.assign')
            ->with('state', $state);
    }

    public function assignMachine(Request $request, $state_id)
    {
        $request->validate([
            'engineer' => 'required|max:1',
            'machines' => 'required|min:1'
        ]);

        $state = CountryState::findOrFail($state_id);
        $engineer = User::findOrFail($request->engineer);

        foreach ($request->machines as $machine_id) {
            $machine = Machine::find($machine_id);
            $machine->engineer_id = $request->engineer;
            $machine->save();

            $machine->audit_events()->create([
                'event' => 'updated',
                'description' => 
                "machine, {$machine->terminal_id} was successfully assigned to {$engineer->full_name()}"
            ]);
        }

        $machine_count = count($request->machines);
        $append = $machine_count > 1 ? $machine_count." machines were" : "a machine was";
        $message= $append." successfully assigned to engineer, ".$engineer->full_name();

        return redirect()->route('machine.assignment.state.assign.form', $state->id)
            ->with('machines_assigned', $message);
    }
}
