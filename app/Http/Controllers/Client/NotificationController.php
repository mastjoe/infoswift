<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Repositories\NotificationRepository;

class NotificationController extends Controller
{
    public $repo;

    public function __construct(NotificationRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * show all notifications
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->id) {
            if ($this->repo->idExists($request->id)) {
                return redirect()->route('client.notifications')
                    ->with('selected_id', route('client.show.notification', $request->id));
            }
        }
        $client = Auth::user();
        $grouped_notifications =  $client->notifications()
            ->get()
            ->groupBy(function ($not) {
                return $not->created_at->format('Y-m-d');
            })
            ->sortKeysDesc();
        
        return view('client.notifications.index')
            ->with('grouped_notifications', $grouped_notifications);
    }

    /**
     * show notifications details
     */
    public function show($id)
    {
        $notification = $this->repo->readNotification($id);
        return view('client.notifications.show')
            ->with('notification', $notification);
    }

    /**
     * get unread notifications in json format
     * 
     * @return \Illuminate\Http\Response
     */
    public function api()
    {
        $notifications = $this->repo->getUnreadNotifications();   

        return response()->json([
            "status" => "success",
            "data" => $notifications,
            "message" => "notifications fetched"
        ]);
    }

    /**
     * delete notifications
     * 
     * @param string $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        if ($this->repo->deleteNotification($id)) {
            return response()->json([
                "status"  => "success",
                "message" => "notification successfully deleted"
            ]);
        }
        return response()->json([
            "status"  => "error",
            "message" => "sorry notification not deleted! Try again"
        ]);
    }
}
