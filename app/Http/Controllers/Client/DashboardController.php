<?php

namespace App\Http\Controllers\Client;

use App\Ticket;
use Carbon\Carbon;
use App\Helpers\Util;
use App\MachineStatus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    //
    public function index()
    {
        $client = $this->client();


        return view('client.dashboard.index')
            ->with('client', $client)
            ->with('ticket_years', Util::ticketYears())
            ->with('pm_years', Util::pmYears());
    }

    private function client()
    {
        return Auth::user();
    }

    /**
     * get ticket chart data for client
     * @param \ILLuminate\Http\Request $request
     * @return \ILLuminate\Http\Response
     */
    public function ticketChartData(Request $request)
    {
        $year = Carbon::now()->year;
        if ($request->year) {
            $year = Carbon::parse($request->year)->format('Y');
        }

        // compute for open tickets
        $monthly_open_tickets = [];
        for ($i = 0; $i < 12; $i++) {
            $month = "$year-".($i+1);
            $monthly_open_tickets[] = Ticket::where('client_id', $this->client()->id)
                ->where('created_at', 'LIKE', Carbon::parse($month)->format('Y-m').'%')->count();
        }

        // compute for closed tickets
        $monthly_closed_tickets = [];
        for ($i = 0; $i < 12; $i++) {
            $month = "$year-" . ($i + 1);
            $monthly_closed_tickets[] = Ticket::where('client_id', $this->client()->id)
                ->whereNotNull('confirmed_at')
                ->where('created_at', 'LIKE', Carbon::parse($month)->format('Y-m') . '%')->count();
        }

        return response()->json([
            "year"    => $year,
            "months"  => Util::months(),
            "openTickets" => $monthly_open_tickets,
            "closedTickets" => $monthly_closed_tickets
        ]);
    }

    /**
     * get pm chart data
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function pmChartData(Request $request)
    {
        $year = Carbon::now()->year;
        if ($request->year) {
            $year = Carbon::parse()->format('Y');
        }

        $pms            = $this->client()->yearPms($year);
        $first_quarter  = $pms->where('quarter', 1)->count();
        $second_quarter = $pms->where('quarter', 2)->count();
        $third_quarter  = $pms->where('quarter', 3)->count();
        $fourth_quarter = $pms->where('quarter', 4)->count();

        return response()->json([
            "year" => $year,
            "pms" => [
                ["quarter"  => "first", "pms" => $first_quarter],
                ["quarter"  => "second", "pms" => $second_quarter],
                ["quarter"  => "third", "pms" => $third_quarter],
                ["quarter"  => "fourth", "pms" => $fourth_quarter],
            ]
        ]);
    }

    /**
     * get machine status chart data
     */
    public function machinesStatusesData()
    {
        $client = $this->client();
        $statuses = MachineStatus::with('machines')->get()->map(function($status) use($client) {
            return [
                "status" => ucfirst($status->status),
                "machines" => $status->machines->where('client_id', $client->id)->count()
            ];
        });
        
        return response()->json($statuses);
    }
}
