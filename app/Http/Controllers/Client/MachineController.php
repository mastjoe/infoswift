<?php

namespace App\Http\Controllers\Client;

use App\Machine;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class MachineController extends Controller
{
    //
    public function index()
    {
        $machines = $this->client()->machines;

        return view('client.machines.index')
            ->with('machines', $machines);
    }

    protected function client()
    {
        return Auth::user();
    }

    /**
     * show details of a machine
     */
    public function show($id)
    {
        $machine = Machine::with([
            'branch',
            'region',
            'client',
            'engineer'
        ])->findOrFail($id);

        if (!$this->machineIsClients($id)) {
            abort(403);
        }

        return view('client.machines.show')
            ->with('machine', $machine);
    }

    /**
     * checks if client owns machine
     * @param int $id
     * @return boolean
     */
    protected function machineIsClients($id)
    {
        $machine_ids = $this->client()->machines->pluck('id')->toArray();
        if (in_array($id, $machine_ids)) {
            return true;
        }
        return false;
    }

    /**
     * show tickets of a machine
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function showTickets($id)
    {
        $machine = Machine::findOrFail($id);

        return view('client.machines.tickets')
            ->with('machine', $machine);
    }

    /**
     * show pm records for a machine
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function showPMs($id)
    {
        $machine = Machine::findOrFail($id);

        return view('client.machines.pms')
            ->with('machine', $machine);

    }
}
