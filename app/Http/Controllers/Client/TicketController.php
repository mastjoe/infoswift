<?php

namespace App\Http\Controllers\Client;

use App\Ticket;
use App\Machine;
use App\TicketFault;
use App\MachineFault;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Repositories\TicketRepository;
use Illuminate\Support\Facades\Session;

class TicketController extends Controller
{
    //

    public $ticketRepo;

    public function __construct(TicketRepository $ticketRepo)
    {
        $this->ticketRepo = $ticketRepo;
    }

    public function client()
    {
        return Auth::user();
    }

    public function index()
    {
        $tickets = $this->client()->tickets()->orderBy('id', 'desc')->get();

        return view('client.tickets.index')
            ->with('tickets', $tickets);
    }

    /**
     * @param \Illuminate\Http\Request $request
     */
    public function create(Request $request)
    {
        $target_machine = null;
        if ($request->machine) {
            $target_machine = $request->machine;
        }

        $machines = $this->client()->machines;
        $machine_faults = MachineFault::all();

        return view('client.tickets.create')
            ->with('machine_faults', $machine_faults)
            ->with('machines', $machines)
            ->with('target_machine', $target_machine);
    }

    public function store(Request $request)
    {
        $request->request->add(['client' => $this->client()->id]);
       
        $ticket = $this->ticketRepo->create($request);

        $message = "Ticket was successfully created for machine, {$ticket->machine->terminal_id}";

        return redirect()->route('client.tickets')
            ->with('created', $message);
    }

    protected function resolveNewFault($fault) :int
    {
        $target = MachineFault::where('id', $fault);

        if ($target->count()) {
            return $target->first()->id;
        }

        return MachineFault::create([
            'fault' => $fault,
            'new'   => true
        ])->id;
    }

    public function show($ref)
    {
        $ticket = Ticket::findRef($ref);

        return view('client.tickets.show')
            ->with('ticket', $ticket);
    }

    /**
     * edit ticket
     * 
     * @param string $ref
     * @return \Illuminate\Http\Response
     */
    public function edit($ref)
    {
        $ticket = Ticket::findRef($ref);
        $machines = $this->client()->machines;
        $machine_faults = MachineFault::all();

        if (!$ticket->editable()) {
            $message = "Cannot edit ticket!";
            return redirect()->route('client.show.ticket', $ref)
                ->with('cannot_edit', $message);
        }

        return view('client.tickets.edit')
            ->with('ticket', $ticket)
            ->with('machines', $machines)
            ->with('machine_faults', $machine_faults);
    }

    /**
     * update ticket
     * 
     * @param \Illuminate\Http\Request $request
     * @param string $ref
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $ref)
    {
        $request->validate([
            'machine' => 'required',
            'faults.*' => 'required|min:1'
        ]);
        
        $ticket = Ticket::findRef($ref);

        $ticket->update(['machine_id' => $request->machine]);
        $ticket->faults()->delete();

        foreach ($request->faults as $fault) {
            $ticket->faults()->create([
                'fault_id' => $this->resolveNewFault($fault)
            ]);
        }
        
        $message = "Ticket {$ticket->ref} successfully updated";

        return redirect()->route('client.show.ticket', $ref)
            ->with('updated', $message);

    }

    /**
     * delete ticket
     * 
     * @param string $ref
     * @return \Illuminate\Http\Response
     */
    public function destroy($ref)
    {
        $ticket = Ticket::findRef($ref);

        if ($ticket->deletable()) {
            $message = "ticket {$ref} successfully deleted";

            $ticket->faults()->delete();
            $ticket->delete();

            Session::flash('ticket_deleted', $message);

            return response()->json([
                'status' => 'success',
                'message' => $message,
                'redirect_url' => route('client.tickets')
            ]);
        }
        return response()->json([
            'status' => 'error',
            'message' => 'something went wrong! Try again'
        ]);
    }

    /**
     * show logs of specific ticket
     * 
     * @param string $ref
     * @return Collection
     */
    public function showLogs($ref)
    {
        $ticket = $this->ticketRepo->findRef($ref);

        return view('client.tickets.logs')
            ->with('ticket', $ticket);
    }

    /**
     * create new ticket log
     * 
     * @param \Illuminate\Http\Request $request
     * @param string $ref
     * @return \Illuminate\Http\Response
     */
    public function storeLog(Request $request, $ref)
    {
        $ticket = $this->ticketRepo->findRef($ref);
        $log = $this->ticketRepo->createTicketLog($request, $ticket);

        $message = "your message was successfully logged! we shall be in touch shortly";
        Session::flash("message_logged", $message);

        return response()->json([
            "status" => "success",
            "message" => $message
        ]);
    }
}
