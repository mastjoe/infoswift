<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    //
    /**
     * client profile
     */
    public function index()
    {
        return view('client.profile.index');
    }

    /**
     * view all regions
     */
    public function regions()
    {
        return view('client.profile.regions');
    }

    /**
     * client's branches
     */
    public function branches()
    {
        return view('client.profile.branches');
    }

    /**
     * client's machines
     */
    public function machines()
    {
        return view('client.profile.machines');
    }
}
