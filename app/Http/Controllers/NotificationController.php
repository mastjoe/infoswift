<?php

namespace App\Http\Controllers;

use App\Repositories\NotificationRepository;
use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    //
    public $repo;

    public function __construct(NotificationRepository $repo)
    {
        $this->repo = $repo;
    }
    /**
     * show all users notifications
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->id) {
            if ($this->repo->idExists($request->id)) {
                return redirect()->route('admin.notifications')
                    ->with('selected_id', route('admin.show.notification', $request->id));
            }
        }
        $user = Auth::user();
        $grouped_notifications =  $user->notifications()
            ->get()
            ->groupBy(function($not) {
                return $not->created_at->format('Y-m-d');
            })
            ->sortKeysDesc();

        return view('admin.notifications.index')
            ->with('user', $user)
            ->with('grouped_notifications', $grouped_notifications);
    }

    /**
     * read specific user notification
     * 
     * @param string $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $notification = $this->repo->readNotification($id);

        return view('admin.notifications.show')
            ->with('notification', $notification);
    }


    /**
     * get unread notifications in json format
     * 
     * @return \Illuminate\Http\Response
     */
    public function api()
    {
        $notifications = $this->repo->getUnreadNotifications();

        return response()->json([
            "status" => "success",
            "data" => $notifications,
            "message" => "notifications fetched"
        ]);
    }

    /**
     * delete notifications
     * 
     * @param string $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        if ($this->repo->deleteNotification($id)) {
            return response()->json([
                "status"  => "success",
                "message" => "notification successfully deleted"
            ]);
        }
        return response()->json([
            "status"  => "error",
            "message" => "sorry notification not deleted! Try again"
        ]);
    }
}
