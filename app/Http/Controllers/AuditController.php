<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\AuditEvent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuditController extends Controller
{
    //
    public function index()
    {
        $user = Auth::user();
        if (!$user->isSuperAdmin()) abort(403);
        
        return view('admin.audits.index');
    }

    /**
     * show audit for specific date
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $date = Carbon::today();
        if ($request->date) {
            $date = Carbon::parse($request->date);
        }

        $day_audits = AuditEvent::dayAudits($date)->get();
        
        return view('admin.audits.show')
            ->with('date', $date)
            ->with('day_audits', $day_audits);
    }

    /**
     * show details of specific audit item
     * 
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function showDetails($id)
    {
        $audit = AuditEvent::with(['user'])->findOrFail($id);

        return view('admin.audits.details')
            ->with('audit', $audit);
    }
}
