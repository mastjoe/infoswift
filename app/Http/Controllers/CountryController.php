<?php

namespace App\Http\Controllers;

use Session;
use App\Country;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $countries = Country::all();

        return view('admin.settings.locations.countries.index')
            ->with('countries', $countries);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return redirect()->route('all.countries')
            ->with('new', true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'country'   => 'required|string|unique:countries,country',
            'continent' => 'required|string'
        ]);

        $country = Country::create([
            'country'   => $request->country,
            'continent' => $request->continent
        ]);

        $message = "Country, ".$country->country." successfully added";

        Session::flash('created', $message);

        return response()->json([
            'status'       => 'success',
            'message'      => $message,
            'redirect_url' => route('all.countries')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $country = Country::findOrFail($id);

        return view('admin.settings.locations.countries.show')
            ->with('country', $country);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $country = Country::findOrFail($id);
        return redirect()->route('show.country', $id)
            ->with('edit', true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'country' => 'required|string|unique:countries,country,'.$request->country.',country',
            'continent' => 'required|string'
        ]);

        $country = Country::findOrFail($id);
        $country->country = $request->country;
        $country->continent = $request->continent;
        $country->save();

        $message = "Country, ".$country->country." was successfully updated!";
        Session::flash('updated', $message);

        return response()->json([
            'status' => 'success',
            'message' => $message,
            'redirect_url' => route('show.country', $country->id)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $country = Country::findOrFail($id);
        if ($country->deletable()) {
            $message = "Country, ".$country->country." was successfully deleted!";

            if ($country->delete()) {
                Session::flash('deleted', $message);
                return response()->json([
                    'status' => 'success',
                    'message' => $message,
                    'redirect_url' => route('all.countries')
                ]);
            }
        }
        return response()->json([
            'status' => 'error',
            'message' => 'Country was not deleted! Try again!'
        ]);
    }

    public function addState($id)
    {
        $country = Country::findOrFail($id);

        return redirect()->route('show.country', $country->id)
            ->with('new_state', true);
    }

    public function storeState(Request $request, $id)
    {
        $request->validate([
            'state' => 'required|string',
            'zone' => 'nullable|string'
        ]);

        $country = Country::findOrFail($id);
        $sc = new StateController;
        $sc->validateStateForACountry($country, $request->state);
        
        $state = $country->states()->create([
            'state' => $request->state,
            'zone' => $request->zone
        ]);

        $message = 
        "State, ".$state->state." was successfully added to country, ".$country->country;
        Session::flash('created_state', $message);
        return response()->json([
            'status' => 'success',
            'message' => $message,
            'redirect_url' => route('show.country', $id)
        ]);
    }
}
