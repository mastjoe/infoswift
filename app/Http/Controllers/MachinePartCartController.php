<?php

namespace App\Http\Controllers;

use App\Machine;
use App\MachinePartCart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Repositories\MachinePartCartRepository;

class MachinePartCartController extends Controller
{
    public $machinePartCartRepo;

    public function __construct(MachinePartCartRepository $machinePartCartRepo)
    {
        $this->machinePartCartRepo = $machinePartCartRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $user = Auth::user();
        return view('admin.machines.cart.index');
    }

    /**
     * add machine part to cart
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request)
    {
       
        $cart = $this->machinePartCartRepo->addToCart($request);
        $part = $cart->machine_part->part;

        $message = "Item, {$part} was successfully added to cart";

        return response()->json([
            'status' => 'success',
            'message' => $message,
        ]);
    }


    /**
     * check if item is in cart
     * @param int $part_id
     */
    protected function inCart($part_id)
    {
        $user = Auth::user();
        $target = MachinePartCart::where('part_id', $part_id)
            ->where('user_id', $user->id);
        if ($target->count()) {
            return $target->first();
        }
        return false;
    }

    /**
     * remove machine part from cart
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function remove(Request $request, $id)
    {
        $cart = MachinePartCart::with(['machine_part'])->findOrFail($id);
        
        if ($this->machinePartCartRepo->removeFromCart($cart)) {
            $message =
                "Item, {$cart->machine_part->part}, was successfully removed from cart";
                Session::flash('deleted', $message);
                return response()->json([
                    'status'   => 'success',
                    'message'  => $message,
                    'redirect_url' => route('machine.parts.cart')
                ]);
        }
        
        return response()->json([
            'status'  => 'error',
            'message' => 'something went wrong! Try again!'
        ]);
    }

    public function getData()
    {
        $user = Auth::user();

        return response()->json([
            'status' => 'success',
            'data' => [
                'count' => $user->cart_sum()
            ]
        ]);
    }

    /**
     * update an item in user cart
     * 
     * @param \Illuminate\Http\Request
     * @param int $id
     * @return \illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->machinePartCartRepo->updateCartItem($request, $id);
        $message = "Cart item updated successfully";
        Session::flash('updated', $message);

        return response()->json([
            'status' => 'success',
            'message' => $message,
            'redirect' => route('machine.parts.cart')
        ]);
    }


    /**
     * checkout cart 
     * 
     * @param \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function checkout(Request $request)
    {
        $order = $this->machinePartCartRepo->checkoutCart($request);
        if ($order) {

            $message = "You order {$order->ref} has been successfully sent!";

            Session::flash('checked_out', $message);
            return response()->json([
                'status'   => 'success',
                'message'  => $message,
                'redirect' => route('show.my.machine.part.orders')
            ]);
        }
        
        return response()->json([
            'status'  => 'error',
            'message' => 'checkout failed! Check and Try again!'
        ]);
    }
}
