<?php

namespace App\Http\Controllers;

use Session;
use App\MachineVendor;
use Illuminate\Http\Request;

class MachineVendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $vendors = MachineVendor::all();

        return view('admin.settings.machines.vendors.index')
            ->with('vendors', $vendors);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return redirect()->route('all.machine.vendors')
            ->with('new', true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'vendor'      => 'required|unique:machine_vendors,vendor',
            'description' => 'nullable|string'
        ]);

        $vendor = MachineVendor::create([
            'vendor'      => $request->vendor,
            'description' => $request->description
        ]);

        $message = 'Machine vendor, "'.$vendor->vendor.'" successfully created!';
        Session::flash('created', $message);
        return response()->json([
            'status'       => 'success',
            'message'      => $message,
            'redirect_url' => route('all.machine.vendors')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $vendor = MachineVendor::findOrFail($id);

        return view('admin.settings.machines.vendors.show')
            ->with('vendor', $vendor);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $vendor = MachineVendor::findOrFail($id);

        return redirect()->route('show.machine.vendor', $vendor->id)
            ->with('edit', true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'vendor'      => 'required|unique:machine_vendors,vendor,'.$request->vendor.',vendor',
            'description' => 'nullable|string'
        ]);

        $vendor              = MachineVendor::findOrFail($id);
        $vendor->vendor      = $request->vendor;
        $vendor->description = $request->description;
        $vendor->save();
        
        $message = 'Machine vendor, '.$vendor->vendor.' was successfully updated!';
        Session:: flash('updated', $message);
        return response()->json([
            'status'       => 'success',
            'message'      => $message,
            'redirect_url' => route('show.machine.vendor', $vendor->id)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $vendor = MachineVendor::findOrFail($id);
        if ($vendor->deletable()) {
            if ($vendor->delete()) {
                $message = 'Machine vendor, '.$vendor->vendor.' was successfully deleted!';
                Session::flash('deleted', $message);
                return response()->json([
                    'status'       => 'success',
                    'message'      => $message,
                    'redirect_url' => route('all.machine.vendors')
                ]);
            }
        }

        return response()->json([
            'status'  => 'error',
            'message' => 'Machine vendor was not deleted! Try again!'
        ]);
    }
}
