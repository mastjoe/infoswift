<?php

namespace App\Http\Controllers;

use App\Client;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Repositories\ReportRepository;


class ReportController extends Controller
{
    //
    public $reportRepo;

    public function __construct(ReportRepository $reportRepo)
    {
        $this->reportRepo = $reportRepo;
    }

    public function index()
    {

    }

    public function downlistClients()
    {
        $clients = Client::paginate(12);

        return view('admin.reports.downlist')
            ->with('clients', $clients);
    }

    /**
     * get downlist for specific date
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $client_id
     * @return \Illuminate\Http\Response 
     */
    public function clientDownlistReport(Request $request, $client_id)
    {
        $date = Carbon::now();
        if ($request->date) {
            $date = Carbon::parse($request->date);
        }

        $client = Client::findOrFail($client_id);
        $downlist =  $this->reportRepo->getClientDownlist($client, $date);

        return view('admin.reports.client-downlist')
            ->with('downlist', $downlist)
            ->with('date', $date)
            ->with('client', $client);
    }

    /**
     * get productivity report for a date
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function productivityReport(Request $request)
    {
        
    }
}
