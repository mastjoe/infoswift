<?php

namespace App\Http\Controllers;

use App\Image;
use App\MachinePart;
use App\Helpers\Upload;
use App\MachinePartStock;
use App\MachinePartCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use App\Repositories\MachinePartRepository;

class MachinePartController extends Controller
{
    public $machinePartRepo;

    public function __construct(MachinePartRepository $machinePartRepo)
    {
        $this->machinePartRepo = $machinePartRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $per_page = 12;
        $parts = MachinePart::paginate($per_page);
        $query = null;
        $searched_parts = null;
        // $this->authorize('viewAny', MachinePart::class);
        if (Gate::denies('order', MachinePart::class) && Gate::denies('viewAny', MachinePart::class)){
            abort(403);
        }   

        if ($request->has('q') && $request->q) {
            $query = $request->q;
            $searched_parts = $this->search($request, $per_page);
        }

        return view('admin.machines.parts.index')
            ->with('parts', $parts)
            ->with('query', $query)
            ->with('searched_parts', $searched_parts);
    }

    protected function search(Request $request,int $per_page=12)
    {
        if ($request->q) {
            $q = $request->q;
            return MachinePart::with(['category'])->where('part', 'LIKE', $q.'%')
                ->orWhere('description', 'LIKE', $q.'%')
                ->orWhere('technical_specifications', 'LIKE', $q.'%')
                ->orWhere('documentation', 'LIKE', $q.'%')
                ->orWhereHas('category', function($query) use($q) {
                    $query->where('category', 'LIKE', $q.'%')
                    ->orWhere('description', 'LIKE', $q.'%');
                })
                ->paginate($per_page);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = MachinePartCategory::all();
        $this->authorize('create', MachinePart::class);

        return view('admin.machines.parts.create')
            ->with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // 
       $part = $this->machinePartRepo->create($request);

        $message = "Machine part, {$part->part} was successfully created";
        
        return redirect()->route('show.machine.part', $part->id)
            ->with('created', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $part = MachinePart::findOrFail($id);
        $this->authorize('view', $part);

        return view('admin.machines.parts.show')
            ->with('part', $part);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $part = MachinePart::findOrFail($id);
        $categories = MachinePartCategory::all();
        $this->authorize('update', $part);

        return view('admin.machines.parts.edit')
            ->with('part', $part)
            ->with('categories', $categories);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $part = $this->machinePartRepo->update($request, $id);

        $message = "Machine Part, {$part->part} was successfully updated";
        return redirect()->route('show.machine.part', $id)
            ->with('updated', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $part = MachinePart::findOrFail($id);
        $this->authorize('delete', $part);

        if ($this->machinePartRepo->delete($part)) {

            $message = "Machine part, {$part->part} successfully deleted!";
            Session::flash('deleted', $message);

            return response()->json([
                'status'       => 'success',
                'message'      => $message,
                'redirect_url' => route('all.machine.parts')
            ]);
        }

        return response()->json([
            'status' => 'error',
            'message' => 'something went wrong! Try again'
        ]);
    }


    /**
     * show images for a machine part
     * 
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function showImages($id)
    {
        $part = MachinePart::findOrFail($id);

        return view('admin.machines.parts.images')
            ->with('part', $part);
    }

    /**
     * show form for adding machine part
     * 
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function addImage($id)
    {
        $part = MachinePart::findOrFail($id);

        return redirect()->route('show.machine.part.images', $id)
            ->with('add_image', true);
    }

    /**
     * stores machine part image
     * 
     * @param \illuminate\Http\Request $request
     * @param int $id
     * @return \Ilumminate\Http\Response
     */
    public function storeImage(Request $request, $id)
    {
        $this->machinePartRepo->addPartImages($request, $id);
        $message = "Image successfully uploaded!";
        Session::flash('image_uploaded', $message);

        return response()->json([
            'status' => 'success',
            'message' => $message,
            'redirect' => route('show.machine.part.images', $id)
        ]);
    }

    /**
     * delete specific machine part image
     * 
     * @param int $id
     * @param int $image_id
     * @return \Illuminate\Http\Response
     */
    public function deleteImage($id, $image_id)
    {
        $part  = MachinePart::findOrFail($id);
        $deleted_image = $this->machinePartRepo->removeImage($part, $image_id);

        if ($deleted_image) {
            $message = "Image, {$deleted_image->label} of part, {$part->part} was successfully deleted";

            Session::flash('image_deleted', $message);
            return response()->json([
                'status'       => 'success',
                'message'      => $message,
                'redirect_url' => route('show.machine.part.images', $id)
            ]);
        }

        return response()->json([
            'status'  => 'error',
            'message' => 'something went wrong! Try again'
        ]);
    }
}
