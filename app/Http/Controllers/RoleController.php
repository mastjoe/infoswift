<?php

namespace App\Http\Controllers;

use Session;
use App\Role;
use App\Privilege;
use App\RolePrivilege;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $roles = Role::all();

        return view('admin.staff.roles.index')
            ->with('roles', $roles);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $privileges = Privilege::all();
        $categories = $this->categorizedPrivileges();
        

        return view('admin.staff.roles.add')
            ->with('privileges', $privileges)
            ->with('categories', $categories);
    }

    private function categorizedPrivileges()
    {
        $privileges = Privilege::all();
        $categories = $privileges->pluck('category')
            ->unique()->toArray();
        $arr = [];

        foreach ($categories as $key => $category) {
            $arr[$category] = $privileges->where('category', $category);
        }
        return $arr;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'role'        => 'required|unique:roles,role|string',
            'description' => 'nullable|string',
            'privileges'  => 'required'
        ]);
        
        $role = Role::create([
            'role'        => $request->role,
            'description' => $request->description
        ]);

       $this->attachPrivileges($role->id, $request->privileges);

        return redirect()->route('show.staff.role', $role->id)
            ->with('new', 'New role, '.$role->role.' was successfully created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $role = Role::findOrFail($id);

        return view('admin.staff.roles.show')
            ->with('role', $role);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $role = Role::findOrFail($id);

        return view('admin.staff.roles.edit')
            ->with('role', $role)
            ->with('categories', $this->categorizedPrivileges());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'role'        => 'required|unique:roles,role,'.$request->role.',role|string',
            'description' => 'nullable|string',
            'privileges'  => 'required'
        ]);

        $role = Role::findOrFail($id);

        $role->role        = $request->role;
        $role->description = $request->description;
        if ($role->save()) {
            $this->detachPrivileges($id);
            $this->attachPrivileges($id, $request->privileges);
            return redirect()->route('show.staff.role', $id)
                ->with('updated', 'Role, '.$role->role.' was successfully updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $role = Role::findOrFail($id);

        if ($role->deletable()) {
            // delete role privileges...
            $this->detachPrivileges($id);

            if ($role->delete()) {

                Session::flash(
                    'deleted',
                    'Role, '.$role->role.' was successfully deleted!'
                );
                
                return response()->json([
                    'status'       => 'success',
                    'message'      => $role->role.' was successfully deleted!',
                    'redirect_url' => route('all.staff.roles')
                ]);
            }
        }
        return response()->json([
            'status'  => 'error',
            'message' => 'Role, '.$role->role.' was not deleted! Try again!'
        ]);
    }

    protected function detachPrivileges($role_id)
    {
        RolePrivilege::where('role_id', $role_id)->delete();
    }

    protected function attachPrivileges($role_id, array $privileges)
    {
        foreach ($privileges as $privilege) {
            RolePrivilege::create([
                'role_id'      => $role_id,
                'privilege_id' => $privilege
            ]);
        }
    }

    /**
     * get donut chart data
     */
    public function donut()
    {
        return Role::get()->map(function($role) {
            return [
                'role' => $role->role,
                'staff' => $role->users->count()
            ];
        });
    }
}
