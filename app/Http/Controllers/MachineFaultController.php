<?php

namespace App\Http\Controllers;

use Session;
use App\MachineFault;
use Illuminate\Http\Request;

class MachineFaultController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $faults = MachineFault::all();
        $new_faults = MachineFault::newFaults()->get();

        return view('admin.settings.machines.faults.index')
            ->with('faults', $faults)
            ->with('new_faults', $new_faults);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return redirect()->route('all.machine.faults')
            ->with('create', true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'fault'       => 'required|unique:machine_faults,fault',
            'description' => 'nullable|string'
        ]);

        $fault = MachineFault::create([
            'fault'       => $request->fault,
            'description' => $request->description
        ]);
        
        $message = "Machine Fault, ".$fault->fault." successfully added!";
        Session::flash('new', $message);

        return response()->json([
            'status' => 'success',
            'message' => $message,
            'redirect_url' => route('all.machine.faults')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $fault = MachineFault::findOrFail($id);

        return view('admin.settings.machines.faults.show')
            ->with('fault', $fault);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $fault = MachineFault::findOrFail($id);
        return redirect()->route('show.machine.fault', $fault->id)
            ->with('edit', true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'fault' => 'required|unique:machine_faults,fault,'.$request->fault.',fault',
            'description' => 'nullable|string'
        ]);

        $fault              = MachineFault::findOrFail($id);
        $fault->fault       = $request->fault;
        $fault->description = $request->description;
        $fault->new         = false;
        $fault->save();
        
        $message = 'Machine fault, "'.$fault->fault.'" was successfully updated!';
        Session::flash('updated', $message);

        return response()->json([
            'status'       => 'success',
            'message'      => $message,
            'redirect_url' => route('show.machine.fault', $fault->id)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $fault = MachineFault::findOrFail($id);

        if ($fault->deletable()) {
            if ($fault->delete()) {
                $message = 'Machine fault, "'.$fault->fault.'" was deleted!';
                Session::flash('deleted', $message);
                return response()->json([
                    'status' => 'success',
                    'message' => $message,
                    'redirect_url' => route('all.machine.faults')
                ]);
            }
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Machine fault was not deleted! Try again!'
        ]);
    }
}
