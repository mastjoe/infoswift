<?php

namespace App\Http\Controllers;

use App\Client;
use App\Ticket;
use App\Machine;
use Carbon\Carbon;
use App\TicketFault;
use App\Helpers\Util;
use App\MachineFault;
use App\Helpers\Upload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Repositories\TicketRepository;
use Illuminate\Support\Facades\Session;

class TicketController extends Controller
{
    public $ticketRepo;

    public function __construct(TicketRepository $ticketRepo)
    {
        $this->ticketRepo = $ticketRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $filter = null;
        $user = Auth::user();

        if ($user->hasRole('super admin')) {
            $tickets = Ticket::with(['client', 'machine'])
                ->orderBy('id', 'desc')->get();
        } else {
            $tickets = $user->all_state_machine_tickets();
        } 
        
        $tickets_count = $tickets->count();

        if ($request->filter) {
            $filter = $request->filter;
            $tickets = $this->filter($request, $tickets);
        }

        return view('admin.tickets.index')
            ->with('tickets', $tickets)
            ->with('filter', $filter)
            ->with('tickets_count', $tickets_count);
    }

    protected function filter(Request $request, $tickets)
    {
        if ($request->has('filter')) {
            $filter = strtolower($request->filter);
            if ($filter == "open") {
                return $tickets->where('status', 'open');
            } else if ($filter == "closed") {
                return $tickets->where('status', 'closed');
            } else if ($filter == "resolved") {
                return $tickets->where('status', 'resolved');
            } else if ($filter == "escalated") {
                return $tickets->where('status', 'escalated');
            } else if ($filter == "declined") {
                return $tickets->where('status', 'declined');
            } else if ($tickets->where('status', 'part ordered')) {
                return $tickets->where('status', 'part ordered');
            }
        }
        return $tickets;
    }


    /**
     * show all ticket closure requests
     * 
     * @return \Illuminate\Http\Client\Response
     */
    public function showClosureRequests()
    {
        $tickets = $this->ticketRepo->closedTickets();

        return view('admin.tickets.closure-requests')
            ->with('closed_tickets', $tickets);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $this->authorize('create', Ticket::class);

        $clients = Client::all();
        $machine_faults = MachineFault::all();
        $machine_id = $client_id = null;
        if ($request->machine) { $machine_id = $request->machine;}
        if ($request->client) { $client_id = $request->client;}


        return view('admin.tickets.create')
            ->with('clients', $clients)
            ->with('machine_faults', $machine_faults)
            ->with('machine_id', $machine_id)
            ->with('client_id', $client_id);
    }


    /**
     * stores ticket
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ticket = $this->ticketRepo->create($request);

        $message = "Ticket was successfully raised for terminal, {$ticket->machine->terminal_id}";

        return redirect()->route('show.ticket', $ticket->id)
            ->with('created', $message);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeLog(Request $request, $id)
    {
        //
        $ticket = Ticket::findOrFail($id);
        $log = $this->ticketRepo->createTicketLog($request, $ticket);

        $message = "Your message log was successfully sent";

        return response()->json([
            'status' => 'success',
            'message' => $message
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $ticket = Ticket::with([
            'client',
            'machine',
            'field_report_form',
            'job_completion_form'
            ])
            ->findOrFail($id);

        $this->authorize('view', $ticket);

        return view('admin.tickets.show')
            ->with('ticket', $ticket);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function showLogs($id)
    {
        $ticket = Ticket::with(['machine','logs'])->findOrFail($id);

        return view('admin.tickets.logs')
            ->with('ticket', $ticket);
    }

    
    public function uploadFRF(Request $request, $id)
    {

        $ticket = Ticket::findOrFail($id);
        $this->ticketRepo->uploadFieldReportForm($request, $ticket);

        $message = "Field report form was successfully uploaded for ticket, {$ticket->ref}";
        Session::flash('uploaded_frf', $message);

        return response()->json([
            'status' => 'success',
            'message' => $message,
            'redirect_url' => route('show.ticket', $ticket->id)
        ]);
    }

    public function uploadJCF(Request $request, $id)
    {
        

        $ticket = Ticket::findOrFail($id);
        $this->ticketRepo->uploadJobCompletionForm($request, $ticket);

        $message = "Job completion form was successfully uploaded for ticket, {$ticket->ref}";
        Session::flash('uploaded_jcf', $message);

        return response()->json([
            'status' => 'success',
            'message' => $message,
            'redirect_url' => route('show.ticket', $ticket->id)
        ]);
    }

    public function showEscalation($id)
    {
        $ticket = Ticket::with(['escalations'])->findOrFail($id);
        $engineers = Util::getRoleUsers("engineer");

        if ($ticket->escalate_from()) {
            $engineers = $engineers->filter(function($eng) use($ticket) {
                if ($ticket->escalate_from()->id != $eng->id) {
                    return $eng;
                }
            });
        }

        return view('admin.tickets.escalations')
            ->with('ticket', $ticket)
            ->with('engineers', $engineers);
    }

    /**
     * to escalate ticket
     * 
     * @param \Illuminate\Http\Request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function escalate(Request $request, $id)
    {
        $ticket = Ticket::findOrFail($id);
        $escalation = $this->ticketRepo->escalateTicket($request, $ticket);

        $message = "Ticket successfully escalated to {$escalation->escalatedTo}";
        Session::flash('escalated', $message);

        return response()->json([
            'status' => 'success',
            'message' => $message,
            'redirect' => route('show.ticket.escalations', $id)
        ]);
    }

    /**
     * show escalation details
     * 
     * @param int $escalation_id
     * @return \Illuminate\Http\Response
     */
    public function showEscalationDetails($escalation_id)
    {
        $escalation = $this->ticketRepo->getEscalation($escalation_id);

        return view('admin.tickets.escalation-details')
            ->with('escalation', $escalation);
    }
}
