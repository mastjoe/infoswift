<?php

namespace App\Http\Controllers;

use App\Sla;
use App\Client;
use App\CountryState;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SlaController extends Controller
{
    //
    /**
     * show all clients for sla 
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::paginate(12);

        return view('admin.settings.sla.index')
            ->with('clients', $clients);
    }

    /**
     * show all states in specific client for sla assigment
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $client = Client::findOrFail($id);
        $states = CountryState::all();

        return view('admin.settings.sla.show')
            ->with('client', $client)
            ->with('states', $states);
    }

    /**
     * assigns sla time settings
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function assign(Request $request, $id)
    {
        $request->validate([
            'start_time'    => 'required|date_format:H:i',
            'stop_time'     => 'required|date_format:H:i',
            'best_hour'     => 'required|numeric',
            'moderate_hour' => 'required|numeric|greater_than_field:best_hour',
            'description'   => 'nullable|string',
            'states.*'      => 'required|min:1'
        ]);

        $client = Client::findOrFail($id);
        $count = 0;

        foreach ($request->states as $key => $state) {
            $target = Sla::where('client_id', $id)->where('state_id', $state);
            if ($target->count()) {
                $sla =  $target->first()->update([
                    'best_hour'     => $request->best_hour,
                    'moderate_hour' => $request->moderate_hour,
                    'description'   => $request->description,
                    'state_id'      => $state,
                    'client_id'     => $id,
                    'start_time'    => $request->start_time,
                    'stop_time'     => $request->stop_time,
                ]);
            } else {
                $sla = $client->slas()->create([
                    'best_hour'     => $request->best_hour,
                    'moderate_hour' => $request->moderate_hour,
                    'description'   => $request->description,
                    'state_id'      => $state,
                    'start_time'    => $request->start_time,
                    'stop_time'     => $request->stop_time,
                ]);
            }
            $count++;
        }

        $message = $count." state".($count > 1 ? "s " :" ")." SLA timing ".($count > 1 ? "were ": "was ")." successfully updated";
        Session::flash('updated', $message);

        return response()->json([
            'status'  => 'success',
            'message' => $message,
        ]);

    }
}
