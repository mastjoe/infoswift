<?php

namespace App\Http\Controllers;

use App\MachinePartSupplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class MachinePartSupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $suppliers = MachinePartSupplier::all();

        return view('admin.settings.machines.suppliers.index')
            ->with('suppliers', $suppliers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.settings.machines.suppliers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name'      => 'required|string',
            'email'     => 'required|email|unique:machine_part_suppliers,email',
            'phone'     => 'required|digits_between:8,15',
            'address'   => 'nullable|string',
            'more_info' => 'nullable|string'
        ]);

        $supplier = MachinePartSupplier::create($request->all());

        $message = "Supplier {$supplier->name} successfully added";
        return redirect()->route('all.parts.suppliers')
            ->with('created', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $supplier = MachinePartSupplier::findOrFail($id);
        
        return view('admin.settings.machines.suppliers.show')
            ->with('supplier', $supplier);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $supplier = MachinePartSupplier::findOrFail($id);

        return view('admin.settings.machines.suppliers.edit')
            ->with('supplier', $supplier);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'name'      => 'required|string',
            'email'     => 'required|email|unique:machine_part_suppliers,email,'.$request->email.',email',
            'phone'     => 'required|digits_between:8,15',
            'address'   => 'nullable|string',
            'more_info' => 'nullable|string'
        ]);

        $supplier = MachinePartSupplier::findOrFail($id);
        $supplier->update($request->all());

        $message = "Supplier, {$supplier->name} was successfully updated";
        return redirect()->route('show.parts.supplier', $id)
            ->with('updated', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $supplier = MachinePartSupplier::findOrFail($id);

        if ($supplier->deletable()) {
            $message = "Supplier, {$supplier->name} was successfully deleted";
            if ($supplier->delete()) {
                Session::flash('deleted', $message);
                return response()->json([
                    'status' => 'success',
                    'message' => $message,
                    'redirect_url' => route('all.parts.suppliers') 
                ]);
            }
        }

        return response()->json([
            'status' => 'error',
            'message' => 'delete operation failed'
        ]);
    }

    /**
     * show supply history of a supplier
     * 
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function showSupplyHistory($id)
    {
        $supplier = MachinePartSupplier::findOrFail($id);

        return view('admin.settings.machines.suppliers.supply-history')
            ->with('supplier', $supplier);
    }
}
