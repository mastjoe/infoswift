<?php

namespace App\Http\Controllers;

use Session;
use App\MachineType;
use Illuminate\Http\Request;

class MachineTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $types = MachineType::all();

        return view('admin.settings.machines.types.index')
            ->with('types', $types);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return redirect()->route('all.machine.types')
            ->with('new', true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'type' => 'required|string|unique:machine_types,type',
            'model' => 'required|string',
            'class' => 'required|string'
        ]);

        $type = MachineType::create([
            'model' => $request->model,
            'class' => $request->class,
            'type' => $request->type
        ]);
        $message = "Machine type, ".$type->type." added!";
        Session::flash('created', $message);
        return response()->json([
            'status' => 'success',
            'message' => $message,
            'redirect_url' => route('all.machine.types')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $type = MachineType::findOrFail($id);

        return view('admin.settings.machines.types.show')
            ->with('type', $type);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $type = MachineType::findOrFail($id);

        return view('admin.settings.machines.types.edit')
            ->with('type', $type);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'type' => 'required|string|unique:machine_types,type,'.$request->type.',type',
            'model' => 'required|string',
            'class' => 'required|string'
        ]);

        $type = MachineType::findOrFail($id);
        $type->model = $request->model;
        $type->class = $request->class;
        $type->type = $request->type;
        $type->save();

        $message = "Machine type, ".$type->type." was successfully updated!";
        Session::flash('updated', $message);

        return response()->json([
            'status' => 'success',
            'message' => $message,
            'redirect_url' => route('all.machine.types')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $type = MachineType::findOrFail($id);
        if ($type->deletable()) {
            $message = "Machine type successfully deleted!";
            if ($type->delete()) {
                Session::flash('deleted', $message);
                return response()->json([
                    'status' => 'success',
                    'message' => $message,
                    'redirect_url' => route('all.machine.types')
                ]);
            }
        }
        return response()->json([
            'status' => 'error',
            'message' => "Machine type was not deleted! Try again!"
        ]);
    }
}
