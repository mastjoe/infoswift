<?php

namespace App\Http\Controllers;

use App\MachinePart;
use App\MachinePartOrder;
use Illuminate\Http\Request;
use App\MachinePartOrderItem;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Repositories\MachinePartOrderRepository;

class MachinePartOrderController extends Controller
{

    public $repo;

    public function __construct(MachinePartOrderRepository $repo)
    {
        $this->repo = $repo;
    }
    //
    /**
     * show all order records
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = MachinePartOrder::all();
        return view('admin.machines.orders.index')
            ->with('orders', $orders);
    }

    /**
     * show specific order
     * 
     * @param string $ref
     * @return \Illuminate\Http\Response
     */
    public function show($ref)
    {
        $order = $this->repo->findRef($ref);
        
            return view('admin.machines.orders.show')
                ->with('order', $order);
    }

    /**
     * show machine part order history for user
     */
    public function showMyOrder()
    {
        $this->authorize('order', MachinePart::class);

        return view('admin.machines.parts.orders');
    }

    /**
     * show machine part order details
     * @param string $ref
     * @return \Illuminate\Http\Response
     */
    public function showMyOrderDetails($ref)
    {
        $this->authorize('order', MachinePart::class);

        $order = $this->repo->findRef($ref);
    
        return view('admin.machines.parts.show-order')
            ->with('order', $order);
    }

    /**
     * show cash and charge order items of order here
     * 
     * @param string $ref
     * @return \Illuminate\Http\Response
     */
    public function showCAC($ref)
    {
        $order = $this->repo->findRef($ref);

        return view('admin.machines.orders.show-cac')
            ->with('order', $order);
    }

    /**
     * show details of an ordered item
     * 
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function showItemDetails($item_id)
    {
        $item = MachinePartOrderItem::with(['order'])->findOrFail($item_id);
        
        return view('admin.machines.orders.show-item-details')
            ->with('item', $item);
    }

    /**
     * decline machine part order
     * 
     * @param \Illuminate\Http\Request $request
     * @param string $ref
     * @return \Illuminate\Http\Response
     */
    public function declineOrder(Request $request, $ref)
    {
        $order = $this->repo->findRef($ref);
        $declined = $this->repo->declineOrder($request, $order);

        if ($declined) {
            $message = "machine part order, {$order->ref} was successfully declined";
            Session::flash('order_declined', $message);

            return response()->json([
                'status'  => 'success',
                'message' => $message,
                'redirect'   => route('all.machine.parts.order')
            ]);
        }

        $message = "decliation of order failed! Try again!";
        
        return response()->json([
            'status'  => 'error',
            'message' => $message,
        ]);

    }

    /**
     * @param int $item_id
     */
    public function notifyClientCAC($item_id)
    {
        $item = MachinePartOrderItem::with(['order'])->findOrFail($item_id);
    }

    /**
     * create dispatch session
     * 
     * @param int $order_id
     * @return \Illuminate\Http\Response
     */
    public function createDispatchSession($order_id)
    {
        
    }



}
