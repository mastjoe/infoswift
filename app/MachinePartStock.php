<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MachinePartStock extends Model
{
    //
    protected $guarded = [];

    public function machine_part()
    {
        return $this->belongsTo('App\MachinePart');
    }

    public function supplies()
    {
        return $this->hasMany('App\MachinePartStockSupply', 'stock_id');
    }

    public static function boot()
    {
        parent::boot();
    }

    public function deletable()
    {
        if ($this->supplies->count()) {
            return false;
        }
        return true;
    }

    public function total_initial_quantity()
    {
        return $this->supplies->sum('initial_quantity');
    }

    public function total_quantity()
    {
        return $this->supplies->sum('quantity');
    }
    
}
