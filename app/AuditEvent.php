<?php

namespace App;

use App\Helpers\Util;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class AuditEvent extends Model
{
    //
    protected $fillable = [
        'auditable_id',
        'auditable_type',
        'user_id',
        'old_values',
        'new_values',
        'event',
        'description',
        'ip_address',
        'user_agent',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function auditable()
    {
        return $this->morphTo();
    }

    public function scopeDayAudits($query, $date)
    {
        return $query->whereDate('created_at', $date);
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function($model) {
            $model->ip_address = request()->ip();
            $model->user_agent = Util::userAgent();
            $model->user_id = Auth::user()->id ?? 1;
        });
    }

    public function target()
    {
        return $this->auditable_type::find($this->auditable_id);
    }
}
