<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Privilege extends Model
{
    //
    protected $fillable = [
        'privilege',
        'description',
        'category'
    ];

    public function roles()
    {
        return $this->belongsToMany('App\Role', 'role_privileges');
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'user_privileges');
    }

}
