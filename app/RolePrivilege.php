<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RolePrivilege extends Model
{
    //

    protected $fillable = [
        'role_id',
        'privilege_id'
    ];
}
