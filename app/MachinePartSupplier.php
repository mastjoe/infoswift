<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MachinePartSupplier extends Model
{
    //
    protected $fillable = [
        'name',
        'email',
        'phone',
        'address',
        'more_info'
    ];

    public function audit_events()
    {
        return $this->morphMany('App\AuditEvent', 'auditable');
    } 

    public function stock_supplies()
    {
        return $this->hasMany('App\MachinePartStockSupply', 'supplier_id');
    }

    public function deletable()
    {
        // if ($this->stocks->count()) {
        //     return false;
        // }
        return true;
    }
}
