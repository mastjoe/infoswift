<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPrivilege extends Model
{
    //

    protected $fillable = [
        'user_id',
        'privilege_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function privilege()
    {
        return $this->belongsTo('App\Privilege');
    }
}
