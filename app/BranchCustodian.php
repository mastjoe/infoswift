<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BranchCustodian extends Model
{
    //
    protected $fillable = [
        'branch_id',
        'name',
        'phone',
        'email'
    ];

    public function getEmailAttribute($value)
    {
        return strtolower($value);
    }

    public function branch()
    {
        return $this->belongsTo('App\Branch');
    }

    public function bank()
    {
        return $this->branch->bank();
    }

    public function deletable()
    {
        return true;
    }
}
