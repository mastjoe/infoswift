<?php

namespace App\Channels;

use Illuminate\Notifications\Notification;

class CustomSMS
{
    public function send($notifiable, Notification $notification)
    {
        $sms = $notification->toCustomSMS($notifiable);
    }
}