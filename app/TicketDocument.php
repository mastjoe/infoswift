<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class TicketDocument extends Model
{
    //
    protected $fillable = [
        'ticket_id',
        'type',
        'url',
        'uploaded_by',
        'file_size',
        'file_type'
    ];

    // type can either be jcf or frf
    public function ticket()
    {
        return $this->belongsTo('App\Ticket');
    }

    public function setTypeAttribute($type) {
        $this->attributes['type'] = strtolower($type);
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function($model) {
            $model->uploaded_by = Auth::user()->id;
        });
    }

    public function uploader()
    {
        return $this->belongsTo('App\User', 'uploaded_by');
    }

    public function link()
    {
        return \asset($this->url);
    }

}
