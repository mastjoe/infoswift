<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    //
    protected $fillable = [
        'imageable_type',
        'imageable_id',
        'size',
        'url',
        'type',
        'label'
    ];

    public function imageable()
    {
        return $this->morphTo();
    }

    public function link()
    {
        return asset($this->url);
    }
}
