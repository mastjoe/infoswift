<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MachinePart extends Model
{
    //
    protected $fillable = [
        'part',
        'category_id',
        'description',
        'technical_specifications',
        'documentation'
    ];

    public function category()
    {
        return $this->belongsTo('App\MachinePartCategory', 'category_id');
    }

    public function audit_events()
    {
        return $this->morphMany('App\AuditEvent', 'auditable');
    }

    public function images()
    {
        return $this->morphMany('App\image', 'imageable');
    }

    public function stocks()
    {
        return $this->hasMany('App\MachinePartStock');
    }

    public function display_image()
    {
        $default = asset('images/machine-part.png');
        if ($this->images->count()) {
            $image_array = $this->images->pluck('url')->toArray();
            $image_count = count($image_array);

            return asset($image_array[mt_rand(0,$image_count-1)]);
        }
        return $default;
    }

    public function deletable()
    {
        return true;
    }
}
