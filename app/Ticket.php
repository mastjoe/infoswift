<?php

namespace App;

use Carbon\Carbon;
use App\Helpers\Util;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    //
    protected $fillable = [
        'ref',
        'machine_id',
        'client_id',
        'status',
        'more_info',
        'closed_by',
        'closed_at',
        'confirmed_by',
        'confirmed_at',
        'closure_comment',
        'confirmation_comment',
        'declined_at',
        'declination_comment',
        'declined_by',
        'rating',
        'sla_rating'
    ];

    protected $dates = [
        'closed_at',
        'declined_at',
        'confirmed_at'
    ];

    public static function findRef($ref)
    {
        return static::where('ref', $ref)->firstOrFail();
    }

    public function machine()
    {
        return $this->belongsTo('App\Machine');
    }

    public function audit_events()
    {
        return $this->morphMany('App\AuditEvent', 'auditable');
    }

    public function state()
    {
        return $this->machine->state();
    }

    public function closer()
    {
        return $this->belongsTo('App\User', 'closed_by');
    }

    public function confirmer()
    {
        return $this->belongsTo('App\User', 'confirmed_by');
    }

    public function decliner()
    {
        return $this->belongsTo('App\User', 'declined_by');
    }

    public function documents()
    {
        return $this->hasMany('App\TicketDocument');
    }

    public function field_report_form()
    {
        return $this->hasOne('App\TicketDocument')->where('type', 'frf');
    }

    public function job_completion_form()
    {
        return $this->hasOne('App\TicketDocument')->where('type', 'jcf');
    }

    public function faults()
    {
        return $this->hasMany('App\TicketFault');
    }

    /**
     * get faults in array form
     */
    public function faults_array()
    {
        if ($this->faults->count()) {
            return $this->faults->map(function($fault) {
                return $fault->fault->fault;
            })->toArray();
        }
        return [];
    }

    public function client()
    {
        return $this->belongsTo('App\Client');
    }

    public function logs()
    {
        return $this->hasMany('App\TicketLog');
    }

    public function escalations()
    {
        return $this->hasMany('App\TicketEscalation');
    }

    public function last_escalation()
    {
        return $this->hasOne('App\TicketEscalation')
            ->orderBy('id', 'desc')->latest();
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function($model) {
            $model->ref = Util::genTicketRef();
        });
    }

    public function deletable()
    {
        if (
            $this->closed_at ||
            $this->documents->count() || 
            $this->escalations->count() ||
            $this->status != "open"
        ) {
            return false;
        }
        return true;
    }

    public function editable()
    {
        return $this->deletable();
    }

    public function scopeClosedTickets($query)
    {
        return $query->with([
            'machine',
            'client',
            'machine.branch',
            'closer',
            ])
            ->whereNotNull('closed_at')
            ->whereNull('confirmed_at')
            ->whereNull('declined_at');
    }

    public function scopeConfirmClosedTickets($query)
    {
        return $query->whereNotNull('closed_at')
            ->whereNotNull('confirmed_at');
    }

    public function scopeOpenTickets($query)
    {
        return $query->whereNull('closed_at')
            ->whereNull('confirmed_at');
    }

    public function scopeEscalatedTickets($query)
    {
        return $query->where('status', 'escalated');
    }

    public function can_upload_frf()
    {
        $target_statuses = ['closed'];
        if (
            (!$this->field_report_form && $this->status == "open") || 
            (!in_array($this->status, $target_statuses))
        ) {
            return true;
        }
        return false;
    }

    public function can_upload_jcf()
    {
        if ($this->field_report_form && !$this->closed_at) {
            return true;
        }
        return false;
    }

    public function can_escalate()
    {
        if ($this->status == "open" || $this->status == "escalated") {
            return true;
        }
        return false;
    }

    public function has_confirmation_request()
    {
        if ($this->closed_at && !$this->confirmed_at) {
            return true;
        }
        return false;
    }

    public function state_engineers()
    {
        return $this->state->engineers;
    }

    public function escalate_from()
    {
        if ($this->escalations->count()) {
            return $this->last_escalation->escalatedTo;
        }
        if ($this->machine->engineer) {
            return $this->machine->engineer;
        } 
        return null;
    }

    public function part_orders()
    {
        return $this->hasMany('App\MachinePartOrder');
    }

    /**
     * get the terminal downtime, i.e diff in hour of time btwn ticket creation and resolution
     */
    public function downtime()
    {
        if ($this->closed_at) {
            return $this->closed_at->diffInHours($this->created_at);
        } else {
            return Carbon::now()->diffInHours($this->created_at);
        }
    }

    /**
     * tells if ticket can be declined
     */
    public function can_decline()
    {
        if (!$this->declined_at && !$this->confirmed_at && $this->status != "open") {
            return true;
        }
        return false;
    }

    /**
     * tells if ticket can be confirmed
     */
    public function can_confirm()
    {
        if (!$this->confirmed_at && $this->closed_at) {
            return true;
        }
        return false;
    }

    /**
     * get engineer last escalated to
     */
    public function lastEscalatedTo()
    {
        if ($this->escalations->count()) {
            return $this->escalations()->with('escalatedTo')
                ->get()->last()->escalatedTo;
        }
    }

    /**
     * determine if log can be made on ticket
     */
    public function can_log()
    {
        return true;
    }
    
}
