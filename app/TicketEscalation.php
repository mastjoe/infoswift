<?php

namespace App;

use App\Helpers\Util;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class TicketEscalation extends Model
{
    //
    protected $fillable = [
        'ticket_id',
        'ref',
        'reason',
        'escalated_by',
        'escalated_to',
        'escalated_from'
    ];

    public function escalator()
    {
        return $this->belongsTo('App\User', 'escalated_by');
    }

    public function escalatedTo()
    {
        return $this->belongsTo('App\User', 'escalated_to');
    }
    
    public function escalatedFrom()
    {
        return $this->belongsTo('App\User', 'escalated_from');
    }

    public static function boot()
    {
        parent::boot();
        static::creating(function($model) {
            $model->ref = Util::genEscalationRef();
            $model->escalated_by = Auth::user()->id;
        });
    }
}
