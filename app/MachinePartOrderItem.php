<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MachinePartOrderItem extends Model
{
    //
    protected $fillable = [
        'order_id',
        'part_id',
        'quantity',
        'price',
        'cac_reason',
        'cac_status',
        'client_cac_comment',
        'cac_received_at',
        'cac_received_by',
        'cac_document',
    ];

    public function order()
    {
        return $this->belongsTo('App\MachinePartOrder', 'order_id');
    }

    public function machine_part()
    {
        return $this->belongsTo('App\MachinePart', 'part_id');
    }

    public function client()
    {
        return $this->order->ticket->client;
    }

    public function images()
    {
        return $this->morphMany('App\image', 'imageable');
    }

    public function isCac()
    {
        return $this->cac_reason;
    }

    public function stock()
    {
        return $this->belongsTo('App\MachinePartStock', 'part_stock_id');
    }

    public function cac_logs()
    {
        return $this->hasMany('App\CacLogs', 'order_item_id');
    }
}
