<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CountryState extends Model
{
    //
    protected $fillable = [
        'country_id',
        'state',
        'zone',
    ];

    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    public function deletable()
    {
        if ($this->regions->count()) {
            return false;
        }
        return true;
    }

    public function regions()
    {
        return $this->hasMany('App\Region', 'state_id');
    }

    public function branches()
    {
        return $this->hasMany('App\Branch', 'state_id');
    }

    public function getMachines()
    {
        return $this->branches->map(function($branch) {
            return $branch->machines;
        })->flatten();
    }

    public function getMachinesAttribute()
    {
        return $this->getMachines();
    }

    public function engineers()
    {
        return $this->belongsToMany('App\User', 'engineer_states')
            ->withTimestamps();
    }

    public function slas()
    {
        return $this->hasMany('App\Sla', 'state_id');
    }

    public function clientSla($client_id)
    {
        return $this->slas()->where('client_id', $client_id)->first();
    }

}
