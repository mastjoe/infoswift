<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dislike extends Model
{
    //
    protected $fillable = [
        'user_id',
        'dislikable_type',
        'dislikable_id',
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function dislikable()
    {
        return $this->morphTo();
    }
}
