(function($) {
    $.fn.dropUpload = function(options) {
        var settings = $.extend({
            text     : 'Drop Files to upload',
            textIcon : 'fa fa-upload',
            browse   : true,
            fileIcon : 'far fa-file',
            videoIcon: 'fas fa-video',
            audioIcon : 'far fa-file-audio',
            imageIcon: 'fa fa-image'

        }, options);

        var audioFormats = ['mp3', 'wma', 'aac', 'mp2', 'wav', 'ogg'];
        var videoFormats = ['mp4', 'ogv', 'webm', 'vob', 'ogg', 'wmv', 'mpg', 'mpeg', '3gp', 'svi'];
        var imageFormats = ['jpg', 'jpeg', 'gif', 'png', 'svg'];

        // wrap element with new parent material
        this.wrap('<div class="drop_upload"></div>');
        $('<div class="container"></div>').insertBefore(this);
        var textIconHTML = '<span class="'+settings.textIcon+' mr-2"></span>';

        var browseBtn = "";
        if (settings.browse) {
            browseBtn = '<button class="btn btn-primary btn-sm select_btn" type="button">Browse</button>';
        }
        var textHTML = '<h4 class="hero_text">'+textIconHTML+' '+settings.text+' '+browseBtn+'</h4>';
        
        // target container...
        var targetContainer = this.parents('.drop_upload').find('.container');
        // append hero text
        targetContainer.append(textHTML);
        // file thumb container
        var filesContainer = '<ul class="files"></ul>';
        targetContainer.append(filesContainer);
        targetContainer.append('<div class="text-right p-1"><button type="button" class="btn btn-sm btn-link text-primary d-none _clear_btn">clear</button></div>');

        var thisInput = this;
        // on change event
        $(this).on('change', function() {
            var fileLists = this.files;
            if (fileLists.length) {
                clearUploader();
                for (i = 0; i < fileLists.length; i++) {
                    var f = fileLists[i];
                    var titleBlock = '<span class="title" title="'+f.name+'">'+stringLimit(f.name)+'</span>';
                    if (isFileImage(f)) {
                        var fileHTML = $('<li><div class="thumbnail"></div>' + titleBlock + '</li>');
                        showImageBg(f, fileHTML);
                    } else {
                        var fileHTML = $('<li><div class="thumbnail"><i class="'+getFileIcon(f)+'"></i></div>'+titleBlock+'</li>');
                    }
                    // $('.files').append(fileBlock)
                    fileHTML.appendTo($('.files'));
                }
            }

            clearableMonitor();
        });

        $('.drop_upload').find('.select_btn').click(function(e) {
            $(this).parents('.drop_upload').find('input[type="file"]').trigger('click');
        });

        $('.drop_upload').find('._clear_btn').click(function(e) {
            clearUploader();
            $(this).parents('.drop_upload').find('input[type="file"]').val('');
            $(this).addClass('d-none');
        });

        var getFileExtension = function(name) {
            return name.toLowerCase().split('.').pop();
        }

        var isFileImage = function(file) {
            var extension = getFileExtension(file.name);
            if ($.inArray(extension, imageFormats) > -1) { return true }
            return false;
        }

        var getFileIcon = function(file) {
            extension = getFileExtension(file.name);
            if ($.inArray(extension, imageFormats) > -1) { 
                return settings.imageIcon;
            } else if ($.inArray(extension, videoFormats) > -1) {
                return settings.videoIcon;
            } else if ($.inArray(extension. audioFormats) > -1) {
                return settings.audioIcon;
            } else {
                return settings.fileIcon
            }
        }

        var stringLimit = function(name, limit=8) {
            if (name.length > 8) {
                return name.substring(0,8)+' ...';
            }
            return name;
        }
        
        var showImageBg = function (file, fileHTML) {
            var reader = new FileReader();
            reader.onload = function (e) {
                fileHTML.find('.thumbnail').attr('style', 'background-image: url("' + e.target.result + '")');
                // .append('<div class="hover_layer"></div><i class="fa fa-trash"></i>')
            }
            reader.readAsDataURL(file);
        }

        
        var clearUploader = function() {
           thisInput.parents('.drop_upload').find('.files').empty();
        }

        var clearableMonitor = function() {
            var files = thisInput.parents('.drop_upload').find('.files');
            var clearBtn = thisInput.parents('.drop_upload').find('._clear_btn');
            clearBtn.addClass('d-none');
            if (files.length) { clearBtn.removeClass('d-none') }
        }

        return this;
    }
} (jQuery))