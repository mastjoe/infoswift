let pushRequest = function (data) {
    if (!objectHasProp('dialogTitle', data)) {
        data.dialogTitle = 'Are you sure?';
    }

    if (!objectHasProp('dialogText', data)) {
        data.dialogText = `you are about deleting item`;
    }

    if (!objectHasProp('dialogType', data)) {
        data.dialogType = `question`;
    }

    if (!objectHasProp('confirmButtonText', data)) {
        data.confirmButtonText = `Yes, Delete`;
    }

    if (!objectHasProp('confirmBtnClass', data)) {
        data.confirmBtnClass = `btn-primary`;
    }

    if (!objectHasProp('successTitle', data)) {
        data.successTitle = 'Delete Successful';
    }

    if (!objectHasProp('method', data)) {
        data.method = "post";
    }

    if (objectHasProp('html', data)) {
        data.text = null;
    }

    let errorAlert = function () {
        return swal.fire(
            'Oops Error',
            'Something went wrong!',
            'error',
        );
    }

    swal.fire({
            title: data.dialogTitle,
            text: data.dialogText,
            html: data.html,
            icon: data.dialogType,
            showCancelButton: true,
            showLoaderOnConfirm: true,
            confirmButtonText: data.confirmButtonText,
            customClass: {
                confirmButton: 'btn mr-2 ' + data.confirmBtnClass,
                cancelButton: 'btn btn-secondary',
            },
            buttonsStyling: false,
            preConfirm: function () {
                return axios[data.method](data.url)
                    .then((data) => {
                        return data;
                    })
                    .catch((err) => {
                        console.log(err.response);
                        return errorAlert();
                    });
            },
            allowOutsideClick: () => !swal.isLoading()
        })
        .then((res) => {
            if (res.value) {
                // swal.fire(
                //     data.successTitle,
                //     res.value.data.message,
                //     'success'
                // );
                if (objectHasProp('redirect_url', res.value.data)) {
                    // delayRedirect(res.value.data.redirect_url, 4000);
                    delayRedirect(res.value.data.redirect_url, 0);
                }
                if (objectHasProp('callback', data)) {
                    data.callback();
                }
            }
        });
}