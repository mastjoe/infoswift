
const auxModal = $('#aux__modal');

// jquery extensions
$.fn.hasAttr = function (name) {
    return this.attr(name) !== undefined;
};

function dataCounter() {
    $('.data-counter').each(function () {
        var $this = $(this),
            countTo = $this.attr('data-count');
        $(this).text('');
        $({
            countNum: $this.text()
        }).animate({
            countNum: countTo
        }, {
            duration: 5000,
            easing: 'linear',
            step: function () {
                $this.text(numberFormat(Math.floor(this.countNum)));
            },
            complete: function () {
                $this.text(numberFormat(this.countNum));
            }
        });
    });
}


function formLabelFocus(textClass = 'text-primary') {
    $(document).on('focus', '.card .form-control , .modal .form-control', function () {
        var targetLabel = $(this).parents('.form-group').find('label');
        targetLabel.addClass(`font-weight-bold ${textClass}`);
    });

    $(document).on('blur', '.card .form-control, .modal .form-control', function () {
        var targetLabel = $(this).parents('.form-group').find('label');
        targetLabel.removeClass(`font-weight-bold ${textClass}`);
    });
}

function numberFormat(n, c, d, t) {
    var c = isNaN(c = Math.abs(c)) ? 0 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
        j = (j = i.length) > 3 ? j % 3 : 0;

    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

// to refresh current page...
function refreshPage(time = '') {
    if (time == '') {
        window.location.reload();
    }
    setTimeout(function () {
        window.location.reload();
    }, time);
}

// refresh a target form..
function resetForm(target) {
    var target = $(target)[0].reset();
}

// scroll to top of modal...
function scrollTopModal(time = 600) {
    $('.modal').animate({
        scrollTop: $('.modal').offset().top
    });
}

// delay redirect to specific page..
function delayRedirect(url, time = 600) {
    setTimeout(function () {
        window.location.replace(url);
    }, time);
}

function inArray(needle, haystack) {
    if ($.inArray(needle, haystack) > -1) {
        return true;
    }
    return false;
}

function removeFromArray(needle, haystack) {
    return haystack.filter(function (x) {
        return x != needle;
    });
}

function objectHasProp(prop, obj) {
    for (p in obj) {
        if (prop == p) {
            return true;
        }
    }
    return false;
}

function clearFormErrors(form, reportSpace = "") {
    $(form).find('.form-control').each(function (ind, el) {
        $(el).removeClass('is-invalid');
        $(el).next('span.invalid-feedback').empty();
    });
    if (reportSpace.length) {
        reportSpace.empty();
    }
}

function handleAxiosFormError(form, response, reportSpace = "") {
    clearFormErrors(form, reportSpace);
    let genericErrorStr = `Oops! Seems something went wrong! Try again!`;

    if (response.status == 422) {
        let errors = response.data.errors;
        let errorKeys = Object.keys(errors);
        $(form).find('.form-group > .form-control').each(function (ind, el) {
            let elem = $(el);
            let name = elem.attr('name');
            if (inArray(name, errorKeys)) {
                let txtNode = errors[name][0];
                let txtElement =
                    $(`<span class="invalid-feedback">${txtNode}</span>`);
                elem.addClass('is-invalid');
                // txtElement.insertAfter(elem);
                elem.after(txtElement);
            }
        });

        genericErrorStr = errors[errorKeys[0]][0];
    }

    let errMsg =
        `<div class="alert alert-primary alert-dismissible">
        ${genericErrorStr}
        <button class="close" type="button" data-dismiss="alert">
            <span>&times;</span>
        </button>
    </div>`;

    if (reportSpace) {
        reportSpace.html(errMsg);
    } else {
        $.notify(
            {
                title: "<b>Error!</b>",
                message: genericErrorStr,
                timer: 0
            },
            {
                type: "primary",
                z_index: 3000,
                animate: {
                    enter: "animated fadeInDown",
                    exit: "animated fadeOutUp"
                },
            }
        );
    }
}

function attachmentPreview(target) {
    const t = $(target);
    const btn = t.parent();
    $('.chip-collection').remove();
    if (target.files) {
        $('<div class="chip-collection"></div>').insertAfter(btn);

        target.files.forEach(function (el, ind) {
            const chip =
                `<span class="chip">
                ${el.name}
                <span></span>
            </span>`;
            $('.chip-collection').append(chip);
        });

        if (target.files.length) {
            const clearAllBtn =
                `<button class="btn btn-sm btn-danger"
                type="button"
                onclick="removeAllAttachedFiles(this)"
            >
                Clear All <i class="fa fa-trash-alt"></i>
            </button>`;
            $('.chip-collection').append(clearAllBtn);
        }
    }
}

function removeAllAttachedFiles(target) {
    const t = $(target);
    const inputTarget = t.parents('.form-group').find('input[type=file]');
    inputTarget.val("");
    $('.chip-collection').remove();
}

function pushAuxModalLoad(data) {
    const auxModalDialog = auxModal.find('.modal-dialog');
    const auxModalContent = auxModal.find('.modal-content');

    const spinner = `
    <div class="text-center my-5">
        <div class="lds-ripple"><div></div><div></div></div>
    </div>`;

    const errorMsg = 
    `<div class="text-center my-5">
        <h5 class="text-primary">An Error occurred! Try again!</h5>
        <div class="mt-3">
            <button class="btn btn-sm btn-secondary waves-effect px-4 retry_btn mr-2">
                Retry
            </button>
            <button class="btn btn-sm btn-default waves-effect px-4" data-dismiss="modal">
                Dismiss
            </button>
        </div>
    </div>`;

    // console.log(data);
    const loadFn = function() {
        auxModal.modal({
            backdrop: 'static'
        });
        auxModalDialog.removeClass('modal-xl modal-lg').addClass('modal-sm')
        auxModalContent.html(spinner);

        let dialogClass = null;
        if (objectHasProp('dialogClass', data)) {
            dialogClass = data.dialogClass;
        }

        axios.get(data.url)
        .then(function({data}) {
            auxModalDialog.removeClass('modal-sm').addClass(dialogClass);
            auxModalContent.html(data);
        })
        .catch(function({response}) {
            auxModalContent.html(errorMsg);
            console.log(response);
        })
        .finally(function() {
             if (objectHasProp('injectAvatar', data)) {
                 injectAvatarInForm(data.injectAvatar);
             }
             if(objectHasProp('callback', data)) {
                 data.callback();
             }
        });
    }
    loadFn();
    $(document).on('click', '#aux__modal .retry_btn', function() {
        loadFn()
    });
}

function simpleDataTable(target, options=null) {
    $(target).addClass('nowrap dt-responsive');
    var dt = $(target).DataTable({
        ...options,
        language: {
            search: '',
            searchPlaceholder: 'Search...',
            lengthMenu: "_MENU_",
            info: "_PAGE_ to _PAGES_ of _TOTAL_"
        },
    });
}

function simpleEditor(target, options=null) {
    $(target).summernote({
        ...options,
        height: 200,
        toolbar:[
             ['style', ['style']],
             ['font', ['bold', 'italic', 'underline']],
             ['fontname', ['fontname']],
             ['color', ['color']],
             ['para', ['ul', 'ol', 'paragraph']],
             ['table', ['table']],
             ['insert', ['link', 'video']],
             ['view', ['fullscreen','help']],
        ]
    });
}

function logOutApp (e) {
    e.preventDefault();
    const form = document.querySelector('#_log_out_form');
    form.submit();
}

function bootNotify(message, type, title=null) {
    $.notify({
        title: title,
        message: message,
        timer: 0
    }, {
        type: type,
        z_index: 3000,
        animate: {
            enter: "animated fadeInDown",
            exit: "animated fadeOutUp"
        },
        offset: 4
    });
}

function starSelection(styleClass='text-warning') {
    const targetInput = $('.star--group .star--input');
    $('.star--group .star').on('click', function() {
        const starIndexVal = $(this).attr('data-index');
        targetInput.val(starIndexVal);

        $(this).parents('.star--group').find('.star').removeClass(styleClass);
        $(this).parents('.star--group').find('.star').each(function(index, el) {
            const starElement = $(el)
            const starIndex = starElement.attr('data-index');
            if (starIndex <= starIndexVal) {
                starElement.addClass(styleClass);
            }
        });
    });
}

function hexToRgbA(hex, a=1) {
    var c;
    if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
        c = hex.substring(1).split('');
        if (c.length == 3) {
            c = [c[0], c[0], c[1], c[1], c[2], c[2]];
        }
        c = '0x' + c.join('');
        return 'rgba(' + [(c >> 16) & 255, (c >> 8) & 255, c & 255].join(',') + ','+a+')';
    }
    throw new Error('Bad Hex');
}


function numberPositionOrdinal(num) {
    let position;

    switch (num) {
        case 1:
            position = "first";
            break;
        case 2:
            position = "second";
            break;
        case 3:
            position = "third";
            break;
        case 4:
            position = "fourth";
            break
        default:
            break;
    }
}

