function injectAvatarInForm(target) {
    const t = $(target);
    const default_image = t.attr('data-image');
    const name = t.attr('data-name');

    const template =
        `<div class="avatar__container">
        <div class="avatar__holder" style="background-image:url('${default_image}')">
        </div>
        <label class="avatar__upload">
            <i class="mdi mdi-pencil"></i>
            <input type = "file"
            class = "profile_avatar"
            name = "${name}"
            accept = "image/*">
        </label>
        <span class="avatar__cancel">
            <i class="fa fa-times"></i>
        </span>
    </div>`;

    t.html(template);

    const avatarHolder = document.querySelector(target + ' .avatar__holder');
    const closeBtn = t.find('.avatar__cancel');

    $(document).on('change', target+' .profile_avatar', function() {
        const avatarHolder = document.querySelector(target+' .avatar__holder');
        if (this.files.length) {
            const reader = new FileReader();
             
            reader.onload = function(e) {
                console.log(avatarHolder);
                avatarHolder.style.backgroundImage = `url(${e.target.result})`;
                closeBtn.show();                
            }
            reader.readAsDataURL(this.files[0]);
        }
    });

    $(document).on('click', target+' .avatar__cancel', function() {
        $(avatarHolder).parent().find('.profile_avatar').val('');
        avatarHolder.style.backgroundImage = `url(${default_image})`;
        closeBtn.hide();
    })

}