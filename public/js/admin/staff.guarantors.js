const addGuarantorBtn = $('.add_guarantor_btn');
const addGuarantorModal = $('#add_guarantor_modal');
const addGuarantorForm = $('#add_guarantor_form');

const deleteGuarantorBtn = $('.delete_guarantor_btn');
const editGuarantorBtn = $('.edit_guarantor_btn');

const updateGuarantorForm = $('#update_guarantor_form');


$(document).ready(function() {
    
    // add guarantor modal
    addGuarantorBtn.on('click', function() {
        addGuarantorModal.modal({
            backdrop: 'static'
        });
    });

    // guarantor avatar
    injectAvatarInForm('.g_image_inject');
    // add guarantor form...
    addGuarantorForm.on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        resolveForm({
            form: thisForm,
            method:'post'
        })
        .then(function(data) {
            if (data.status == "success") {
                addGuarantorModal.modal('hide');
                thisForm[0].reset();
                swal.fire(
                    'Guarantor Added!',
                    data.message,
                    'success'
                );
                refreshPage(3500);
            }
        })
        .catch(function(response) {
            console.log(response);
        })
    });

    // delete guarantor...
    deleteGuarantorBtn.on('click', function() {
        const url  = $(this).attr('data-url');
        const name = $(this).attr('data-name');

        pushRequest({
            url         : url,
            method      : 'delete',
            dialogText  : 'You are about deleting guarantor, '+name,
            successTitle: 'Guarantor Deleted!'
        })
    });

    editGuarantorBtn.on('click', function() {
        pushAuxModalLoad({
            url: $(this).attr('data-url'),
            method: 'get',
            injectAvatar: '.g_image_inject1'
        });
    });

    // update guarantor form...
    $(document).on('submit', '#update_guarantor_form', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        resolveForm({
            form: thisForm,
            method: 'post',
        })
        .then(function(data) {
            if (data.status == "success") {

                auxModal.modal('hide');
                Swal.fire(
                    'Guarantor Updated!',
                    data.message,
                    'success'
                );
                refreshPage(3000);
            }
        })
        .catch(function(resp) {
            console.log(resp)
        });
    });
});