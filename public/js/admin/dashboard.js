const ticketChartTarget = $('#client_tickets_chart');

const loadTicketChart = function(year="") {
    const yearTagTarget = $('.ticket_year_tag');

    const plotChart = function(labels, datacollection) {
        let datasets = [];
        datacollection.map(function(d) {
            datasets.push({
                label:d.client,
                data: d.tickets,
                fill: 'true',
                borderColor: hexToRgbA(d.color, 0.65),
                backgroundColor: hexToRgbA(d.color, 0.65),
                borderWidth: 0.6,
                lineTension: 0.5,
                borderCapStyle: "butt",
                borderJoinStyle: "miter",
            });
        });
        const ctx = ticketChartTarget.find('canvas').get(0).getContext('2d')
        const chart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: labels,
                datasets: datasets,
                options: {
                    responsive: true,
                    maintainAspectRatio: true,
                    scales: {
                        yAxes: [{
                            ticks: {
                                // max: 100,
                                min: 0,
                                stepSize: 5
                            },
                            gridLines: {
                                display: false,
                            }
                        }]
                    }
                }
            }
        })
    }
    // plotChart()
    axios.get(ticketChartTarget.data('url'), {  params: {'year': year} })
        .then(function({data}) {
            yearTagTarget.text(data.year);
            plotChart(data.months, data.clients);
        })
        .catch(function(e) {
            console.log(e)
        });
}


$(document).ready(function() {
    loadTicketChart();

    $('.ticket-year-dropdown .dropdown-item').on('click', function() {
        loadTicketChart($(this).text())
    });
});

