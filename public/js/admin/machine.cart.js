const cartTarget = $('.cart_target');
const updateCartItemModal = $('#update_cart_item_modal');
const updateCartItemBtn = $('.update_cart_item_btn');

const removeCartItemBtn = $('.remove_cart_item_btn');

const checkoutBtn = $('.checkout_btn');
const checkoutModal = $('#cart_checkout_modal');
const checkoutForm = $('#cart_checkout_form');

const addCartItemModal = $('#add_cart_item_modal');
const addCartItemForm = $('#add_cart_item_form');
const addToCartBtn = $('.add_to_cart');

const showCartDialog = function() {
    addCartItemModal.modal({backdrop: 'static'});
}

const modifyCartDialogModal = function(target) {
    const t = $(target);
    const partId = t.data('part');
    const partName = t.data('name');
    
    addCartItemModal.find('input[name="part"]').val(partId);
    addCartItemModal.find('input[name="machine_part"]').val(partName);

}

const showCartCounter = function() {
    const url = cartTarget.attr('data-url');
    axios.get(url)
    .then(function(data) {
        const d =  data.data;
        if (d.status == "success") {
            if (d.data.count) {
                cartTarget.find('.counter').removeClass('d-none');
                cartTarget.find('.counter').text(d.data.count)
            } else {
                cartTarget.find('.counter').addClass('d-none');
            }
        }
    })
    .catch(function(e) {
        console.log(e);
    });
}

const addItemToCart = function(target) {
    const t = $(target);
    const url = t.attr('data-url');
    const part  = t.attr('data-part');
    const oldContent = t.html();
    t.html('...');
    
    axios.post(url, {
        part: part
    })
        .then(function (data) {
            const d = data.data;
            console.log(data);
            if (d.status == "success") {
                bootNotify(d.message, 'success');
            } else {
                bootNotify('Oops something went wrong! Try again', 'primary')
            }
            showCartCounter();
        }) 
        .catch(function(e) {
            bootNotify('Oops something went wrong! Try again', 'primary')
            console.log(e.response)
        })
        .then(function () {
            t.html(oldContent);
        });
}

const updateCartItem = function(target) {
    const t         = $(target);
    const cartModal = updateCartItemModal;
    const cartForm  = updateCartItemModal.parents('form');
    const url       = t.attr('data-url');
    cartForm.attr('action', url);

    const nameTarget     = cartModal.find('input.name-target');
    const quantityTarget = cartModal.find('input.quantity-target');
    const cacTarget      = cartModal.find('textarea');

    cacTarget.parents('.form-group').addClass('d-none');
    cacTarget.prop('required', false).val('');

    if (t.data('cac') != "") {
        cacTarget.parents('.form-group').removeClass('d-none');
        cacTarget.prop('required', true).val(t.data('cac'));
    }

    nameTarget.val(t.attr('data-name'));
    quantityTarget.val(t.attr('data-quantity'));
    cartModal.modal({backdrop: 'static'});
}

const removeCartItem = function(target) {
    const t = $(target);
    const url = t.attr('data-url');
    const name = t.attr('data-name');

    pushRequest({
        url: url,
        method: 'delete',
        dialogText: 'You are about to remove item, '+name+'?',
        successTitle: 'Item Removed',
    });
}

const showCheckoutModal = function() {
    checkoutModal.modal({
        backdrop: 'static'
    });
}


$(document).ready(function() {
    showCartCounter();

    //  handle add to cart dialog
    
    const handleAddToCartDialog = function() {
        const dialogSection = addCartItemModal.find('.dialog-section');
        const formFieldSection = addCartItemModal.find('.form-fields');
        const submitActions = addCartItemModal.find('.submit-action');
        const cacFields = addCartItemModal.find('.cac-field');
        const cacInput = addCartItemModal.find('input[name="cash_and_charge"]');
        const hideClass = 'd-none';

        const yesBtn = addCartItemModal.find('.yes-btn');
        const noBtn = addCartItemModal.find('.no-btn');
        const backBtn = addCartItemModal.find('.back-btn');

        addToCartBtn.on('click', function() {
             resetCartDialogState();
             modifyCartDialogModal(this);
             showCartDialog()
        });

        // yes btn
        yesBtn.on('click', function() {
            dialogSection.addClass(hideClass);
            formFieldSection.removeClass(hideClass);
            submitActions.removeClass(hideClass);
            cacFields.removeClass(hideClass);
            cacInput.val(1);
            formFieldSection.find('textarea').prop('required', true)
        });

        // no btn
        noBtn.on('click', function() {
            dialogSection.addClass(hideClass);
            formFieldSection.removeClass(hideClass);
            submitActions.removeClass(hideClass);
            cacFields.addClass(hideClass);
            cacInput.val('');
            formFieldSection.find('textarea').prop('required', false);
        });

        // back btn
        backBtn.on('click', function() {
            dialogSection.removeClass(hideClass);
            formFieldSection.addClass(hideClass);
            submitActions.addClass(hideClass)
        });

        // resets cart dialog state
        const resetCartDialogState = function () {
            dialogSection.removeClass(hideClass);
            formFieldSection.addClass(hideClass);
            submitActions.addClass(hideClass);
        }
    }


    handleAddToCartDialog();

    // submission of add cart form
    addCartItemForm.on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');

        resolveForm({
            method: 'post',
            btnLoadingText: '...',
            url: action,
            form: thisForm
        })
            .then(function(data){
                if (data.status == "success") {
                    addCartItemModal.modal('hide');
                    bootNotify(data.message, 'success');
                } else {
                    bootNotify('Oops something went wrong! Try again', 'primary')
                }
            })
            .catch(function(e) {
                console.log(e);
            })
            .finally(function() {
                showCartCounter();
            });
    })

    updateCartItemBtn.on('click', function() {
        updateCartItem(this);
    });

    updateCartItemModal.closest('form').on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');
        console.log(thisForm);

        resolveForm({
            url   : action,
            form  : thisForm,
            method: 'post',
            btnLoadingText: 'Updating...'
        })
            .then(function(data) {
                if (data.status == "success") {
                    delayRedirect(data.redirect, 100);
                }
            })
            .catch(function(e) {
                console.log(e);
            });
    });

    // remove item from cart 
    removeCartItemBtn.on('click', function() {
        removeCartItem(this);
    });

    // checkout btn
    checkoutBtn.on('click', function() {
        showCheckoutModal();
    });

    // submission of checkout form
    checkoutForm.on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');
        
        resolveForm({
            form: thisForm,
            url: action,
            method: 'post',
            btnLoadingText: '...'
        })
            .then(function(data) {
                if (data.status == "success") {
                    delayRedirect(data.redirect, 100);
                }
            })
            .catch(function(e) {
                console.log(e);
            });
    });
});