const addStateBtn = $('.add_state_btn');
const addStateModal = $('#add_state_modal');
const addStateForm = $('#add_state_form');

const editStateBtn = $('.edit_state_btn');
const editStateModal = $('#edit_state_modal');
const editStateForm = $('#edit_state_form');

const deleteStateBtn = $('.delete_state_btn');

const addRegionBtn = $('.add_region_btn');
const addRegionModal = $('#add_region_modal');
const addRegionForm = $('#add_region_form');

const assignEngineerBtn = $('.assign_engineer_btn');
const assignEngineerModal = $('#assign_engineer_modal');
const assignEngineerForm = $('#assign_engineer_form');

const dropEngineerBtn = $('.drop_engineer_btn');


const openAddStateModal = function() {
    addStateModal.modal({
        backdrop: 'static'
    });
}

const openEditStateModal = function() {
    editStateModal.modal({
        backdrop: 'static'
    });
}

const openAddRegionModal = function() {
    addRegionModal.modal({
        backdrop: 'static'
    });
}

const openAssignEngineerModal = function() {
    assignEngineerModal.modal({
        backdrop: 'static'
    });
}

$(document).ready(function() {
    addStateBtn.on('click', function() {
        openAddStateModal();
    });

    addStateForm.on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');

        resolveForm({
            url: action,
            form: thisForm,
            btnLoadingText: 'Adding...',
            method: 'post'
        })
            .then(function(data) {
                if(data.status == "success") {
                    delayRedirect(data.redirect_url, 100);
                }
            })
            .catch(function(err) {
                console.log(err);
            });
    });

    // edit modal
    editStateBtn.on('click', function() {
        openEditStateModal();
    });

    // submission of edit form
    editStateForm.on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');

        resolveForm({
            url: action,
            form: thisForm,
            method: 'put',
            btnLoadingText: 'Saving...'
        })
            .then(function(data) {
                if (data.status == "success") {
                    delayRedirect(data.redirect_url, 100);
                }
            })
            .catch(function(err) {
                console.log(err);
            })
    });

    // delete function 
    deleteStateBtn.on('click', function() {
        const url = $(this).attr('data-url');
        const state = $(this).attr('data-state');
        pushRequest({
            url: url,
            method: 'delete',
            dialogText: 'You are about to delete state, '+state+'?',
            successTitle: 'State Deleted!'
        });
    });

    // add region btn...
    addRegionBtn.on('click', function() {
        openAddRegionModal();
    });

    // submission of add region form...
    addRegionForm.on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');

        resolveForm({
            method: 'post',
            form: thisForm,
            url: action,
            btnLoadingText: 'Adding...'
        })
            .then(function(data) {
                if(data.status == "success") {
                    delayRedirect(data.redirect_url, 100);
                }
            })
            .catch(function(err) {
                console.log(err);
            })
    });

    // assign engineer modal
    assignEngineerBtn.on('click', function() {
        openAssignEngineerModal();
    });

    // submission of assign engineer form...
    assignEngineerForm.on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');

        resolveForm({
            url: action,
            form: thisForm,
            btnLoadingText: 'Assigning...',
            method: 'post',
        })
            .then(function(data) {
                if (data.status == "success") {
                    delayRedirect(data.redirect_url, 100);
                }
            })
            .catch(function(e) {
                console.log(e);
            });
    });

    // drop engineer..
    dropEngineerBtn.on('click', function() {
        const url = $(this).attr('data-url');
        const name = $(this).attr('data-name');

        pushRequest({
            url: url,
            dialogText: 'Drop engineer, '+name+' from this state ?',
            method: 'post',
            successTitle: 'Engineer Dropped from State',
            confirmButtonText: 'Drop Engineer'
        });
    });

});