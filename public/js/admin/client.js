const addPersonnelModal = $('#add_personnel_modal');
const addPersonnelForm = $('#add_personnel_form');
const addPersonnelBtn = $('.add_personnel_btn');

const deleteClientBtn = $('.delete_client_btn');

const editClientLogoBtn = $('.edit_client_logo_btn');

const editClientLogoForm = $('#edit_client_logo_form');
const editClientLogoModal = editClientLogoForm.find('.modal');

const assignSupervisorBtn = $('.assign_supervisor_btn');
const assignSupervisorModal = $('#assign_supervisor_modal');

const resetClientPasswordBtn = $('.reset_client_password_btn');

const openEditClientLogoForm = function() {
    editClientLogoModal.modal({
        backdrop: 'static'
    });
}

const showAssignSupervisorModal = function() {
    assignSupervisorModal.modal({backdrop: 'static'});
}

$(document).ready(function() {
    addPersonnelBtn.on('click', function() {
        addPersonnelModal.modal({
            backdrop: 'static'
        });
    });
    
    // submission of personnel form..
   addPersonnelForm.on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        resolveForm({
            form: thisForm,
            btnLoadingText: "...",
            method: "put"
        })
            .then(function(data) {
                if (data.status == "success") {
                    addPersonnelModal.modal('hide');
                    thisForm[0].reset();
                    Swal.fire(
                        'Technical Personnel Update!',
                        data.message,
                        'success'
                    );
                    refreshPage(3000);
                }
            })
            .catch(function(err) {
                console.log(err);
            });
   });

    //    delete functionality
    deleteClientBtn.on('click', function() {
        const url = $(this).attr('data-url');
        pushRequest({
            url: url,
            dialogText: 'You are about deleting client',
            successTitle: 'Client Deleted!',
            method: 'delete'
        });
    });

    // edit client logo btn
    editClientLogoBtn.on('click', function() {
        openEditClientLogoForm();
    });

    // submission of client logo form
    editClientLogoForm.on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');
        resolveForm({
            url: action,
            form: thisForm,
            btnLoadingText: 'Saving...',
            method: 'post'
        })
            .then(function(d) {
                if (d.status == "success") {
                    delayRedirect(d.redirect_url, 100);
                }
            })
            .catch(function(e) {
                console.log(e);
            })
    });

    // assign supervisor modal
    assignSupervisorBtn.on('click', function() {
        showAssignSupervisorModal();
    });

    // submission of assign supervisor form
    assignSupervisorModal.closest('form').submit(function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');

        resolveForm({
            url: action,
            form: thisForm,
            btnLoadingText: '...',
            method: 'post'
        })
            .then(function(data) {
                if (data.status == "success") {
                    refreshPage(50);
                }
            })
            .catch(function(e) {
                console.log(e)
            })
    });

    // reset client password
    resetClientPasswordBtn.on('click', function() {
        const url = $(this).data('url');
        pushRequest({
            url: url,
            dialogText: 'You are about reset client\'s password',
            successTitle: 'Client Password Resetted!',
            confirmBtnClass: 'btn-dark',
            confirmButtonText: 'Yes, Reset',
            method: 'post'
        });
    });
});