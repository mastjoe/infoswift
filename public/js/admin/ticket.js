const logModal = $('#log_modal');
const addLogBtn = $('.add_log_btn');
const frfModal = $('#FRFModal');
const jcfModal = $('#JCFModal');
const uploadFRFBtn = $('.upload_frf_btn');
const uploadJCFBtn = $('.upload_jcf_btn');

const escalateBtn = $('.escalate_btn');
const escalateModal = $('#escalateModal');

const checkSolutionBtn = $('.solution_check_btn');

const confirmationModal = $('#ticket_confirmation_modal');
const confirmationBtn = $('.ticket_confirmation_btn');

const declinationBtn = $('.ticket_declination_btn');
const declinationModal = $('#ticket_declination_modal');

const breakDownBtn = $('.breakdown_btn');
const breakDownModal = $('#breakdown_modal');

const updateCloseBtn = $('.update_close_btn');
const updateCloseModal = $('#update_close_modal');

const showLogModal = function() {
    logModal.modal({
        backdrop: 'static'
    });
}

const showFRFModal = function() {
    frfModal.modal({
        backdrop: 'static'
    });
}

const showJCFModal = function () {
    jcfModal.modal({
        backdrop: 'static'
    });
}

const showEscalateModal = function() {
    escalateModal.modal({
        backdrop: 'static'
    });
}

const showConfirmationModal = function() {
    confirmationModal.modal({
        backdrop: 'static'
    });
}

const showDeclinationModal = function() {
    declinationModal.modal({
        backdrop: 'static'
    });
}

const showBreakDownModal = function() {
    breakDownModal.modal({backdrop: 'static'});
}

const showUpdateCloseModal = function() {
    updateCloseModal.modal({backdrop: 'static'});
}

const closeTicket = function (event) {
    const t = $(event.target);
    const url = t.attr('data-url');
    const name = t.attr('data-name');
    pushRequest({
        url: url,
        method: 'post',
        html: '<h4>You are about closing ticket, ' + name + '?</h4>'+
        '<p>Kindly Ensure that Field report form is uploaded if necessary</p>'
    });
}


$(document).ready(function() {
    formLabelFocus();
    starSelection();

    // editor field
    const editorInit = function () {
        // $('.editor-field').summernote({
        //     height: 230,
        //     airMode: false,
        //     placeholder: 'Type message here...',
        //     dialogsInBody: true,
        //     toolbar: [
        //         // [groupName, [list of button]]
        //         ['style', ['bold', 'italic', 'underline']],
        //         ['fontsize', ['fontsize']],
        //         ['color', ['color']],
        //         ['para', ['ul', 'ol', 'paragraph']]
        //     ]
        // });
        simpleEditor('.editor-field')
    }

    editorInit();

    logModal.on('shown.bs.modal', function() {
        editorInit();
        // logModal.find('.note-editable').height(80)
    });

    addLogBtn.on('click', function() {
        showLogModal();
    });

    // submit log form...
    logModal.find('form').on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const url = thisForm.attr('action');

        resolveForm({
            url: url,
            form: thisForm,
            btnLoadingText: '...',
            method: 'post'
        })
            .then(function(data) {
                if (data.status == "success") {
                    logModal.modal('hide');
                    $('.editor-field').summernote('reset');
                    refreshPage(100);
                }
            })
            .catch(function(e) {
                console.log(e);
            });
    });


    // upload frf modal
    uploadFRFBtn.on('click', function(){
        showFRFModal();
    });

    // submission of frf form
    frfModal.find('form').on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const url = thisForm.attr('action');

        resolveForm({
            form:  thisForm,
            url:  url,
            method: 'post',
            btnLoadingText: 'Uploading...'
        })
            .then(function(data) {
                if (data.status == "success") {
                    delayRedirect(data.redirect_url, 10);
                }
            })
            .catch(function(e) {
                console.log(e);
            });
    });

    // upload frf modal
    uploadJCFBtn.on('click', function(){
        showJCFModal();
    });

    // submission of frf form
    jcfModal.find('form').on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const url = thisForm.attr('action');

        resolveForm({
            form:  thisForm,
            url:  url,
            method: 'post',
            btnLoadingText: 'Uploading...'
        })
            .then(function(data) {
                if (data.status == "success") {
                    delayRedirect(data.redirect_url, 10);
                }
            })
            .catch(function(e) {
                console.log(e);
            });
    });

    // escalate modal
    escalateBtn.on('click', function() {
        showEscalateModal();
    });

    // submit ticket escalation form.
    escalateModal.find('form').on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');

        resolveForm({
            url: action,
            method: 'post',
            form: thisForm,
            btnLoadingText: 'Escalating...'
        })
            .then(function(data) {
                if (data.status == "success") {
                    delayRedirect(data.redirect, 100);
                }
            })
            .catch(function(e) {
                console.log(e);
            });
    });

    // old solution button
    checkSolutionBtn.on('click', function() {
        const url = $(this).attr('data-url');
        pushAuxModalLoad({
            url: url,
        })
    });

    // ticket confirmation modal
    confirmationBtn.on('click', function() {
        showConfirmationModal();
    });

    // confirm ticket closure submission
    confirmationModal.closest('form').on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');
        const starInput = thisForm.find('.star--input');
        if (starInput.val() == "") {
            bootNotify('kindly rate closure', 'primary');
            return
        }

        resolveForm({
            url: action,
            method: 'post',
            btnLoadingText: 'Confirming...',
            form: thisForm,
        })
            .then(function(data) {
                if(data.status == "success") {
                    delayRedirect(data.redirect, 50)
                }
            })
            .catch(function(e) {
                console.log(e)
            })
    });

    // show declination modal
    declinationBtn.on('click', function() {
        showDeclinationModal();
    });

    // submission of declination form
    declinationModal.closest('form').on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');

        resolveForm({
            url: action,
            form: thisForm,
            method: 'post',
            btnLoadingText: '...'
        })
            .then(function(data) {
                if(data.status == "success") {
                    refreshPage(50)
                }
            })
            .catch(function(e) {
                console.log(e)
            });
    });

    // show break down modal
    breakDownBtn.on('click', function() {
        showBreakDownModal();
    });

    // show update close modal
    updateCloseBtn.on('click', function() {
        showUpdateCloseModal();
    });

    // submission of ticket
    updateCloseModal.closest('form').on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');

        resolveForm({
            form: thisForm,
            url: action,
            btnLoadingText: '...',
            method: 'post'
        })
            .then(function(data) {
                if(data.status == "success") {
                    delayRedirect(data.redirect, 50)
                }
            })
            .catch(function(e) {
                console.log(e)
            })
    });
});