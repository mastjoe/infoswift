
$(document).ready(function() {
    formLabelFocus();
});

const passwordFieldTypeToggler = function(event) {
    const t = $(event.target);
    const formControl = t.parents('.form-group').find('input.form-control');
    const icon = t.parents('.input-group').find('.fa');
    // console.log(formControl);
    if (formControl.attr('type') == "text") {
        formControl.attr('type', 'password');
        icon.removeClass('fa-eye-slash').addClass('fa-eye')
    } else {
        formControl.attr('type', 'text');
        icon.removeClass('fa-eye').addClass('fa-eye-slash');
    }
}