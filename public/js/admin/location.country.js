const addCountryBtn = $('.add_country_btn');
const addCountryModal = $('#add_country_modal');
const addCountryForm = $('#add_country_form');

const editCountryBtn = $('.edit_country_btn');
const editCountryForm = $('#edit_country_form');
const editCountryModal = $('#edit_country_modal');

const deleteCountryBtn = $('.delete_country_btn');

const addStateBtn = $('.add_state_btn');
const addStateForm = $('#add_state_form');
const addStateModal = $('#add_state_modal');

const openAddCountryModal = function() {
    addCountryModal.modal({
        backdrop: 'static'
    });
}

const openEditCountryModal = function() {
    editCountryModal.modal({
        backdrop: 'static'
    });
}

const openAddStateModal = function() {
    addStateModal.modal({
        backdrop: 'static'
    });
}

$(document).ready(function() {

    formLabelFocus();
    // add country modal
    addCountryBtn.on('click', function() {
        openAddCountryModal();
    });

    // submission of add country form
    addCountryForm.on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');

        resolveForm({
            url: action,
            method: 'post',
            form: thisForm,
            btnLoadingText: 'Adding'
        })
            .then(function(data) {
                if (data.status == "success") {
                    delayRedirect(data.redirect_url, 50);
                }
            })
            .catch(function(err) {
                console.log(err)
            });
    });

    // open edit country modal
    editCountryBtn.on('click', function() {
        openEditCountryModal();
    });

    // submission of edit country form
    editCountryForm.on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');

        resolveForm({
            url: action,
            method: 'put',
            form: thisForm,
            btnLoadingText: 'Saving...'
        })
            .then(function(data) {
                if (data.status == "success") {
                    delayRedirect(data.redirect_url, 50);
                }
            })
            .catch(function(err) {
                console.log(err);
            });
    });

    // delete country function
    deleteCountryBtn.on('click', function(){
        const thisBtn = $(this);
        const url = thisBtn.attr('data-url');
        const country = thisBtn.attr('data-country');
        pushRequest({
            url: url,
            dialogText: 'You are about to delete country, '+country+'?',
            method: 'delete',
            successTitlte: 'Country Deleted'
        });
    });

    // add state modal
    addStateBtn.on('click', function() {
        openAddStateModal();
    });

    // submission of state form
    addStateForm.on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');
        resolveForm({
            url: action,
            method: 'post',
            form: thisForm,
            btnLoadingText: 'Adding...'
        })
            .then(function(data) {
                if (data.status == "success") {
                    delayRedirect(data.redirect_url, 50);
                }
            })
            .catch(function(err){
                console.log(err);
            })
    });
});