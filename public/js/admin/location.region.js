const addRegionBtn = $('.add_region_btn');
const addRegionModal = $('#add_region_modal');
const addRegionForm = $('#add_region_form');

const deleteRegionBtn = $('.delete_region_btn');
const editRegionBtn = $('.edit_region_btn');
const editRegionModal = $('#edit_region_modal');
const editRegionForm = $('#edit_region_form');

const addBranchBtn = $('.add_branch_btn');
const addBranchModal = $('#add_branch_modal');
const addBranchForm = $('#add_branch_form');

const openAddRegionModal = function() {
    addRegionModal.modal({
        backdrop: 'static'
    });
}

const openEditRegionModal = function() {
    editRegionModal.modal({
        backdrop: 'static'
    });
}

const openAddBranchModal = function() {
    addBranchModal.modal({
        backdrop: 'static'
    });
}

$(document).ready(function() {
    // add region modal
    addRegionBtn.on('click', function() {
        openAddRegionModal();
    });

    // submission of add region form
    addRegionForm.on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');

        resolveForm({
            url: action,
            method: 'post',
            btnLoadingText: 'Adding...',
            form: thisForm
        })
            .then(function(data) {
                if (data.status == "success") {
                    delayRedirect(data.redirect_url, 100);
                }
            })
            .catch(function(err) {
                console.log(err);
            });
    });

    // add edit modal...
    editRegionBtn.on('click', function() {
        openEditRegionModal();
    });

    // edit modal ...
    editRegionForm.on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');

        resolveForm({
            url: action,
            form: thisForm,
            btnLoadingText: '...',
            method: 'put'
        })
            .then(function(data) {
                if (data.status == "success") {
                    delayRedirect(data.redirect_url, 50);
                }
            })
            .catch(function(err) {
                console.log(err)
            });
    });

    // delete action...
    deleteRegionBtn.on('click', function() {
        const url = $(this).attr('data-url');
        const name = $(this).attr('data-region');
        pushRequest({
            url: url,
            method: 'delete',
            dialogText: 'You are about delete region, '+name+'?',
            successTitle: 'Region Deleted'
        });
    });

    // add branch modal
    addBranchBtn.on('click', function() {
        openAddBranchModal();
    });

    addBranchForm.on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');
        
        resolveForm({
            url: action,
            form: thisForm,
            btnLoadingText: '...',
            method: 'post'
        })
            .then(function(data) {
                console.log(data)
                if (data.status == "success") {
                    delayRedirect(data.redirect_url, 100);
                }
            })
            .catch(function(err) {
                console.log(err);
            });
    });
});