const addImageModal = $('#add_image_modal');
const addImageForm = $('#add_image_form');

const editStockBtn = $('.edit_stock_btn');
const editStockModal = $('#edit_stock_modal');
const editStockForm = $('#edit_stock_form');

const deleteStockBtn = $('.delete_stock_btn');

const addSupplyBtn = $('.add_supply_btn');
const addSupplyForm = $('#add_supply_form');
const addSupplyModal = $('#add_supply_modal');

const editSupplyBtn = $('.edit_supply_btn');
const updateSupplyForm = $('#update_supply_form');
const updateSupplyModal = $('#update_supply_modal');

const deleteSupplyBtn = $('.delete_supply_btn');


const deleteMachinePart = function(target) {
    const t = $(target);
    const url = t.attr('data-url');
    const name = t.attr('data-name');

    pushRequest({
        url: url,
        method: 'delete',
        dialogTitle: 'Delete Machine Part',
        dialogText: 'you are about to delete machine part, '+name+'?',
        successTitle: 'Machine Part Deleted'
    });
}

const deleteMachinePartImage = function(target) {
    const t = $(target);
    const url = t.attr('data-url');
    const name = t.attr('data-name');
    
    pushRequest({
        url: url,
        method: 'delete',
        dialogTitle: 'Delete Machine Part Image',
        dialogText: 'you are about to delete part image, ' + name + '?',
        successTitle: 'Machine Part Image Deleted'
    });
}

const deleteMachinePartStock = function(target) {
    const t = $(target);
    const url = t.attr('data-url');
    const name = t.attr('data-name');

    pushRequest({
        url: url,
        method: 'delete',
        dialogTitle: 'Delete Machine Part Stock',
        dialogText: 'you are about to delete stock, ' + name + '?',
        successTitle: 'Machine Part Stock Deleted'
    });
}

const deleteSupplyStock = function(target) {
    const t = $(target);
    const url = t.attr('data-url');
    const name = t.attr('data-name');

    pushRequest({
        url: url,
        method: 'delete',
        dialogTitle: 'Delete Stock Supply',
        dialogText: 'you are about to delete supply, ' + name + '?',
        successTitle: 'Stock Deleted'
    });
}

const openAddImageModal = function() {
    addImageModal.modal({
        backdrop: 'static'
    });
}


const openEditStockModal = function() {
    editStockModal.modal({
        backdrop: 'static'
    });
}

const openAddSupplyModal = function() {
    addSupplyModal.modal({
        backdrop: 'static'
    });
}

const openUpdateSupplyModal = function() {
    updateSupplyModal.modal({
        backdrop: 'static'
    });
}

$(document).ready(function() {

    formLabelFocus();

    addImageForm.on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');

        resolveForm({
            url: action,
            form: thisForm,
            method: "post",
            btnLoadingText: 'Adding...'
        })
            .then(function(data) {
                if (data.status == "success") {
                    delayRedirect(data.redirect, 100);
                }
            })
            .catch(function(e) {
                console.log(e);
            });
    });

    // edit stock btn function 
    editStockBtn.on('click', function() {
        openEditStockModal();
    });

    // submission of edit stock form
    editStockForm.on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');

        resolveForm({
            form          : thisForm,
            url           : action,
            method        : 'put',
            btnLoadingText: 'Saving...'
        })
            .then(function(data) {
                if (data.status == "success") {
                    delayRedirect(data.redirect, 100);
                }
            })
            .catch(function(e) {
                console.log(e);
            })
    });

    // delete machine stock
    deleteStockBtn.on('click', function() {
        deleteMachinePartStock(this);
    });

    // add supply modal
    addSupplyBtn.on('click', function() {
        openAddSupplyModal();
    });

    // add supply form submission
    addSupplyForm.on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');

        resolveForm({
            form: thisForm,
            method: 'post',
            url: action,
            btnLoadingText: 'Adding...'
        })
            .then(function(data) {
                if (data.status == "success") {
                    delayRedirect(data.redirect, 100);
                }
            })
            .catch(function(e) {
                console.log(e)
            })
    });

    // edit supply form
    editSupplyBtn.on('click', function() {
        openUpdateSupplyModal();
    });
    
    // submission of edit supply form
    updateSupplyForm.on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');

        resolveForm({
            form: thisForm,
            url: action,
            method: 'put',
            btnLoadingText: 'Saving...'
        })
            .then(function(data) {
                if (data.status == "success") {
                    delayRedirect(data.redirect, 100)
                }
            })
            .catch(function(e) {
                console.log(e)
            });
    });

    // delete supply function 
    deleteSupplyBtn.on('click', function() {
        deleteSupplyStock(this);
    });

});