const addBranchBtn = $('.add_branch_btn');
const addBranchModal = $('#add_branch_modal');
const addBranchForm = $('#add_branch_form');

const editBranchBtn = $('.edit_branch_btn');
const editBranchModal = $('#edit_branch_modal');
const editBranchForm = $('#edit_branch_form');

const deleteBranchBtn = $('.delete_branch_btn');

const addMachineBtn = $('.add_machine_btn');
const addMachineForm = $('#add_machine_form');
const addMachineModal = $('#add_machine_modal');

const addCustodianModal = $('#add_custodian_modal');
const addCustodianForm = $('#add_custodian_form');
const addCustodianBtn = $('.add_custodian_btn');

const deleteCustodianBtn = $('.delete_custodian_btn');
const editCustodianBtn = $('.edit_custodian_btn');



const openAddBranchModal = function() {
    addBranchModal.modal({
        backdrop: 'static'
    });
}

const openEditBranchModal = function() {
    editBranchModal.modal({
        backdrop: 'static'
    });
}

const regionSelectReaction = function(target) {
    const t = $(target);
    const opt = t.find('option:selected');
    const rd = t.parents('form').find('.rd');

    rd.addClass('d-none');

    if (t.val() != "") {
        const p = t.parents('form');
        p.find('#country_field').val(opt.attr('data-country'));
        p.find('#state_field').val(opt.attr('data-state'));
        p.find('#client_field').val(opt.attr('data-client'));
        rd.removeClass('d-none');
    }
}

const openAddMachineModal = function() {
    addMachineModal.modal(function() {
        backdrop: 'static'
    });
}

const openAddCustodianModal = function() {
    addCustodianModal.modal({
        backdrop: 'static'
    });
}

const updateCustodianForm = function(event) {
    event.preventDefault();
    const thisForm = $(event.target);
    const action = thisForm.attr('action');
    resolveForm({
        url: action,
        form: thisForm,
        btnLoadingText: 'Saving...',
        method: 'put',
    })
        .then(function(data) {
            if (data.status == "success") {
                delayRedirect(data.redirect_url, 100);
            }
        })
        .catch(function(e) {
            console.log(e);
        });
}

$(document).ready(function() {
    addBranchBtn.on('click', function() {
        openAddBranchModal();
    });

    // submission of add branch form
    addBranchForm.on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');

        resolveForm({
            form: thisForm,
            url: action,
            btnLoadingText: 'Adding...',
            method: 'post'
        })
            .then(function(data) {
                console.log(data)
                if (data.status == "success") {
                    delayRedirect(data.redirect_url, 100);
                }
            })
            .catch(function(err) {
                console.log(err);
            })
    });

    // edit branch modal
    editBranchBtn.on('click', function() {
        openEditBranchModal();
    });

    // submission of edit form
    editBranchForm.on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');

        resolveForm({
            url: action,
            form: thisForm,
            btnLoadingText: 'Saving...',
            method: 'put'
        })
            .then(function(data) {
                if (data.status == "success") {
                    delayRedirect(data.redirect_url, 100);
                }
            })
            .catch(function(err) {
                console.log(err)
            });
    });

    // delete branch function 
    deleteBranchBtn.on('click', function() {
        const url = $(this).attr('data-url')
        const name = $(this).attr('data-branch');

        pushRequest({
            url: url,
            method: 'delete',
            dialogText: 'You are about to delete branch, '+name+'?',
            successTitle: 'Branch Deleted!'
        });
    });

    // add machine modal
    addMachineBtn.on('click', function() {
        openAddMachineModal();
    });

    // add machine form submission
    addMachineForm.on('submit', function(e){
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');
        resolveForm({
            url: action,
            method: 'post',
            btnLoadingText: 'Adding...',
            form: thisForm
        })
            .then(function(data) {
                if (data.status == "success") {
                    delayRedirect(data.redirect_url, 100);
                }
            })
            .catch(function(err) {
                console.log(err)
            });
    });

    // add custodian modal
    addCustodianBtn.on('click', function() {
        openAddCustodianModal();
    });

    // submit add custodian form
    addCustodianForm.on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');
        console.log(action);

        resolveForm({
            url: action,
            method: 'post',
            btnLoadingText: '...',
            form: thisForm
        })
            .then(function(data) {
                if (data.status == "success") {
                    delayRedirect(data.redirect_url, 100);
                }
            })
            .catch(function(e) {
                console.log(e);
            });
    });

    deleteCustodianBtn.on('click', function() {
        const name = $(this).attr('data-name');
        const url = $(this).attr('data-url');

        pushRequest({
            url: url,
            dialogText: 'You are about to delete custodian, '+name+'?',
            method: 'delete',
            successTitle: 'Custodian Deleted'
        });
    });

    editCustodianBtn.on('click', function() {
        const url = $(this).attr('data-url');

        pushAuxModalLoad({
            url: url,
            dialogClass: "modal-dialog-centered"
        });
    });
});