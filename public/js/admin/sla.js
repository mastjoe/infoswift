const assignModal = $('#assign_modal');
const assignBtn = $('.assign_btn');
const updateSlaBtn = $('.update_sla_btn');

const showAssignModal = function() {
    assignModal.modal({backdrop: 'static'});
    assignModal.closest('form').get(0).reset();
    $("#states > option").prop("selected", false);
    $('#states').trigger('change');
}

$(document).ready(function() {
    assignBtn.on('click', function() {
        showAssignModal();
    });

    $('#states').select2({
        placeholder: 'Select State'
    });

    $("#allCheck").click(function () {
        if ($("#allCheck").is(':checked')) {
            $("#states > option").prop("selected", "selected");
            $("#states").trigger("change");
        } else {
            $("#states > option").prop("selected", false);
            $("#states").trigger("change");
        }
    });

    // assign form submission
    assignModal.closest('form').on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');
        resolveForm({
            form: thisForm,
            url: action,
            btnLoadingText: '...',
            method: 'post'
        })
            .then(function(data) {
                console.log(data)
                if (data.status == "success") {
                    refreshPage(100);
                }
            })
            .catch(function(e) {
                console.log(e)
            })
    });

    // update sla selection
    updateSlaBtn.on('click', function() {
        showAssignModal();
        assignModal.find('input[name="best_hour"]').val($(this).data('best'))
        assignModal.find('input[name="moderate_hour"]').val($(this).data('moderate'))
        assignModal.find('input[name="start_time"]').val($(this).data('start'))
        assignModal.find('input[name="stop_time"]').val($(this).data('stop'))
        $('#states').val($(this).data('state'));
        $('#states').trigger('change');
        $('textarea[name="description"]').val($(this).data('description'))
    })
})