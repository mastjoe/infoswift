const createPmModal = $('#create_pm_modal');
const createPmBtn = $('.create_pm_btn');
const createPmForm = $('#create_pm_form');

const deletePmBtn = $('.delete_pm_btn');

const editPmBtn = $('.edit_pm_btn');

const acceptPmBtn = $('.accept_pm_btn');
const acceptPmModal = $('#accept_pm_modal');
const declinePmBtn = $('.decline_pm_btn');
const declinePmModal = $('#decline_pm_modal');

const showCreatePmModal = function() {
    createPmModal.modal({backdrop: 'static'})
}

const showAcceptPmModal = function() {
    acceptPmModal.modal({backdrop: 'static'});
    acceptPmModal.find('input[type=hidden]').val('')
}

const showDeclinePmModal = function() {
    declinePmModal.modal({backdrop: 'static'});
}

$(document).ready(function() {

    formLabelFocus();
    starSelection('text-warning')

    createPmBtn.on('click', function() {
        showCreatePmModal();
    });

    // submission of create pm form
    createPmForm.on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');

        resolveForm({
            url: action,
            form: thisForm,
            method: "post",
            btnLoadingText: '...'
        })
            .then(function(data) {
                if (data.status == "success") {
                    delayRedirect(data.redirect, 100);
                }
            })
            .catch(function(e) {
                console.log(e);
            });
    });

    // delete pm function 
    deletePmBtn.on('click', function() {
        const url = $(this).attr('data-url');
        const quarter = $(this).attr('data-quarter');
        const year = $(this).attr('data-year');
        const msg = 'you are about to delete PM report for '+quarter+' quarter for '+year;

        pushRequest({
            method: 'delete',
            url: url,
            dialogText: msg+'?',
            successTitle: 'Machine Vendor Deleted'
        });
    });

    // edit pm btn
    editPmBtn.on('click', function() {
        const url = $(this).attr('data-url');
        pushAuxModalLoad({
            url: url
        });
    });

    // update pm form
    $(document).on('submit', '#pm_update_form', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');
        resolveForm({
                url: action,
                form: thisForm,
                method: "post",
                btnLoadingText: 'Saving...'
            })
            .then(function (data) {
                if (data.status == "success") {
                    delayRedirect(data.redirect, 100);
                }
            })
            .catch(function (e) {
                console.log(e);
            });
    });

    // accept pm modal
    acceptPmBtn.on('click', function() {
        showAcceptPmModal();
    });

    // submission of accept pm form
    acceptPmModal.closest('form').on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');
        if (thisForm.find('.star--input').val() == "") {
            bootNotify('Kindly rate report', 'primary')
            return false;
        }

        resolveForm({
            url: action,
            form: thisForm,
            method: 'post',
            btnLoadingText: '...',
        })
            .then(function(data) {
                if(data.status == "success") {
                    delayRedirect(data.redirect, 50)
                }
            })
            .catch(function(e) {
                console.log(e)
            })
    })

    // decline pm modal
    declinePmBtn.on('click', function() {
        showDeclinePmModal();
    });

    // submission of decline pm form
    declinePmModal.closest('form').on('submit', function (e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');

        resolveForm({
                url: action,
                form: thisForm,
                method: 'post',
                btnLoadingText: '...',
            })
            .then(function (data) {
                if (data.status == "success") {
                    delayRedirect(data.redirect, 50)
                }
            })
            .catch(function (e) {
                console.log(e)
            })
    })
});