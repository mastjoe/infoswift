const deleteStaffBtn = $('.delete_staff_btn');
const updatePhotoForm = $('#update_staff_photo_form');
const updatePhotoModal = $('#update_photo_modal');
const updatePhotoBtn = $('.update_photo_btn');

const suspendBtn = $('.suspend_staff_btn');
const unsuspendBtn = $('.unsuspend_staff_btn');

const assignSupervisorBtn = $('.assign_supervisor_btn');
const assignSupervisorModal = $('#assign_supervisor_modal');

const resetPasswordBtn = $('.reset_password_btn');


const showUpdatePhotoModal = function() {
    updatePhotoModal.modal({backdrop: 'static'});
}

const showAssignSupervisorModal = function() {
    assignSupervisorModal.modal({backdrop:'static'})
}

$(document).ready(function() {
    
    // formlabel focus
    formLabelFocus();
    injectAvatarInForm('._image_inject');
    
    // delete staff btn
    deleteStaffBtn.on('click', function(){
        const url = $(this).attr('data-url');
        pushRequest({
            url: url,
            method: 'delete',
            dialogText: 'You are about deleting staff',
            successTitle: 'Staff Deleted!'
        });
    });

    updatePhotoBtn.on('click', function() {
        showUpdatePhotoModal();
    });

    // update photo form submission
    updatePhotoForm.on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');

        resolveForm({
            url: action,
            form: thisForm,
            btnLoadingText: 'Saving...',
            method: 'post',
        })
            .then(function(data) {
                if (data.status == "success") {
                    updatePhotoModal.modal('hide');
                    delayRedirect(data.redirect, 50);
                } else {
                    bootNotify(data.message, 'danger');
                }
            })
            .catch(function(e) {
                console.log(e)
            });
    });

    // suspend staff
    suspendBtn.on('click', function() {
        const url = $(this).attr('data-url');
        pushRequest({
            url: url,
            method: 'post',
            dialogText: 'You are about suspending staff',
            successTitle: 'Staff Suspended',
            confirmButtonText: 'Yes, Suspend'
        });
    });

    unsuspendBtn.on('click', function() {
        const url = $(this).attr('data-url');
        pushRequest({
            url: url,
            method: 'post',
            dialogText: 'You are about unsuspending staff',
            successTitle: 'Staff Unsuspended',
            confirmButtonText: 'Yes, Unsuspend',
            confirmBtnClass: 'btn-success'
        });
    });

    // assign supervisor modal
    assignSupervisorBtn.on('click', function() { 
        showAssignSupervisorModal();
    });

    // reset staff password
    resetPasswordBtn.on('click', function() {
        const url = $(this).data('url');
        pushRequest({
            url: url,
            method: 'post',
            dialogText: 'You are about to reset staff password',
            successTitle: 'Staff Unsuspended',
            confirmButtonText: 'Yes, Reset',
            confirmBtnClass: 'btn-dark'
        });
    })
});