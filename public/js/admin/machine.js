const addFaultBtn   = $('.add_fault_btn');
const addFaultModal = $('#add_fault_modal');
const addFaultForm  = $('#add_fault_form');

const deleteFaultBtn = $('.delete_fault_btn');

const editFaultBtn = $('.edit_fault_btn');
const editFaultModal = $('#edit_fault_modal');
const editFaultForm = $('#edit_fault_form');

const addVendorBtn = $('.add_vendor_btn');
const addVendorModal = $('#add_vendor_modal');
const addVendorForm = $('#add_vendor_form');

const editVendorBtn = $('.edit_vendor_btn');
const editVendorModal = $('#edit_vendor_modal');
const editVendorForm = $('#edit_vendor_form');

const deleteVendorBtn = $('.delete_vendor_btn');

const addStatusBtn = $('.add_status_btn');
const addStatusModal = $('#add_status_modal');
const addStatusForm = $('#add_status_form');

const editStatusBtn = $('.edit_status_btn');
const editStatusModal = $('#edit_status_modal');
const editStatusForm = $('#edit_status_form');

const deleteStatusBtn = $('.delete_status_btn');

const addTypeBtn = $('.add_type_btn');
const addTypeModal = $('#add_type_modal');
const addTypeForm = $('#add_type_form');

const editTypeBtn = $('.edit_type_btn');

const deleteTypeBtn = $('.delete_type_btn');

const deleteSupplierBtn = $('.delete_supplier_btn');

const addPartsCategoryBtn = $('.add_parts_category_btn');
const addPartsCategoryModal = $('#parts_category_modal');
const addPartsCategoryForm = $('#parts_category_form');
const deletePartsCategoryBtn = $('.delete_parts_category_btn');

const updatePartsCategoryForm = $('#update_parts_category_form');
const updatePartsCategoryModal = $('#update_parts_category_modal');
const updatePartsCategoryBtn = $('.update_parts_category_btn');


const assignEngineerModal = $('#assign_engineer_modal');
const assignEngineerBtn = $('.assign_engineer_btn');
const assignEngineerForm = $('#assign_engineer_form');


const openFaultModal = function() {
    addFaultModal.modal({
        backdrop: 'static'
    });
}

const openEditFaultModal = function() {
    editFaultModal.modal({
        backdrop: 'static'
    });
}

const openAddVendorModal = function() {
    addVendorModal.modal({
        backdrop: 'static'
    });
}

const openEditVendorModal = function() {
    editVendorModal.modal({
        backdrop: 'static'
    });
}

const openAddStatusModal = function() {
    addStatusModal.modal({
        backdrop: 'static'
    });
}

const openEditStatusModal = function() {
    editStatusModal.modal({
        backdrop: 'static'
    });
}

const openAddTypeModal = function() {
    addTypeModal.modal({
        backdrop: 'static'
    });
}

const openAddCategoryModal = function() {
    addPartsCategoryModal.modal({
        backdrop: 'static'
    });
}

const openUpdateCategoryModal = function() {
    updatePartsCategoryModal.modal({
        backdrop: 'static'
    });
}

const openAssignEngineerModal = function() {
    assignEngineerModal.modal({
        backdrop: 'static'
    });
}

const deleteMachine = function(target) {
    const t = $(target);
    const name = t.attr('data-name');
    const url = t.attr('data-url');
    
    pushRequest({
        url: url,
        dialogText: 'You are about to delete machine, '+name+'?',
        method: 'delete',
        successTitle: 'Machine Deleted!'
    });
}

const deleteSupplier = function(target) {
    const t = $(target);
    const name = t.attr('data-supplier');
    const url = t.attr('data-url');

    pushRequest({
        url: url,
        dialogText: 'You are about to delete supplier, ' + name + '?',
        method: 'delete',
        successTitle: 'Supplier Deleted!'
    });
}

const updateType = function(e) {
    e.preventDefault();
    const thisForm = $(e.target);
    const action = thisForm.attr('action');
    resolveForm({
        url: action,
        method: 'put',
        btnLoadingText: 'Saving...',
        form: thisForm
    })
        .then(function(data) {
            if (data.status == "success") {
                delayRedirect(data.redirect_url, 100);
            }
        })
        .catch(function(err) {
            console.log(err);
        });
}

$(document).ready(function() {

    formLabelFocus();
    addFaultBtn.on('click', function() {
        openFaultModal();
    });

    // submission of add fault form
    addFaultForm.on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');

        resolveForm({
            form: thisForm,
            method: 'post',
            btnLoadingText: 'Adding...'
        })
        .then(function(data) {
            if (data.status == "success") {
                delayRedirect(data.redirect_url, 0)
            }
        })
        .catch(function(err) {
            console.log(err)
        });
    });

    // delete fault function 
    deleteFaultBtn.on('click', function() {
        const thisBtn = $(this);
        const fault = thisBtn.attr('data-fault');
        const url = thisBtn.attr('data-url');

        pushRequest({
            url: url,
            method: 'delete',
            dialogText: 'you are about deleting machine fault, "'+fault+'"?',
            successTitle: 'Machine Fault Deleted'
        });
    });

    // edit fault modal...
    editFaultBtn.on('click', function() {
        openEditFaultModal();
    });

    // submission of update form...
    editFaultForm.on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');
        resolveForm({
            url: action,
            method: 'put',
            btnLoadingText: 'Saving...',
            form: thisForm
        })
        .then(function(data) {
            delayRedirect(data.redirect_url, 0);
        })
        .catch(function(err) {
            console.log(err);
        });
    });

    // add vendor btn
    addVendorBtn.on('click', function() {
        openAddVendorModal();
    });

    // submission of machine vendor form...
    addVendorForm.on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');
        resolveForm({
            url: action,
            form: thisForm,
            btnLoadingText: 'Adding...',
            method: 'post'
        })
        .then(function(data) {
            if (data.status == "success") {
                delayRedirect(data.redirect_url, 0);
            }
        })
        .catch(function(err) {
            console.log(err);
        });
    });

    // edit vendor modal
    editVendorBtn.on('click', function() {
        openEditVendorModal();
    });

    // submission of edit vendor form..
    editVendorForm.on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');

        resolveForm({
            url: action,
            method: 'put',
            form: thisForm,
            btnLoadingText: 'Saving...'
        })
        .then(function(data) {
            if (data.status == "success") {
                delayRedirect(data.redirect_url, 50);
            }
        })
        .catch(function(err) {
            console.log(err);
        })
    });

    // delete vendor function...
    deleteVendorBtn.on('click', function() {
        const vendor = $(this).attr('data-vendor');
        const url = $(this).attr('data-url');
        pushRequest({
            method: 'delete',
            url: url,
            dialogText: 'You are about deleting vendor, '+vendor+'?',
            successTitle: 'Machine Vendor Deleted'
        });
    });

    // add status modal...
    addStatusBtn.on('click', function() {
        openAddStatusModal();
    });

    // add status form submission
    addStatusForm.on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');
        resolveForm({
            url: action,
            method: 'post',
            btnLoadingText: 'Adding...',
            form: thisForm
        })
        .then(function(data) {
            if (data.status == "success") {
                delayRedirect(data.redirect_url, 50);
            }
        })
        .catch(function(err) {
            console.log(err);
        });
    });

    // edit status modal...
    editStatusBtn.on('click', function() {
        openEditStatusModal();
    });

    // submission of status edit form..
    editStatusForm.on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');

        resolveForm({
            method: 'put',
            url: action,
            form: thisForm,
            btnLoadingText: 'Saving...'
        })
        .then(function(data) {
            if (data.status == "success") {
                delayRedirect(data.redirect_url, 50);
            }
        })
        .catch(function(err) {
            console.log(err);
        })
    });

    // delete status btn...
    deleteStatusBtn.on('click', function() {
        const url = $(this).attr('data-url');
        const status = $(this).attr('data-status');

        pushRequest({
            url: url,
            dialogText: 'You are about to delete machine status, '+status+'?',
            method: 'delete',
            successTitle: 'Machine Status Deleted'
        });
    });

    // add type modal...
    addTypeBtn.on('click', function() {
        openAddTypeModal();
    });
    // submission of machine type form
    addTypeForm.on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');
        resolveForm({
            url: action,
            form: thisForm,
            btnLoadingText: 'Adding...',
            method: 'post'
        })
            .then(function(data) {
                if (data.status == "success") {
                    delayRedirect(data.redirect_url, 100);
                }
            })
            .catch(function(err) {
                console.log(err);
            });
    });

    // edit type function
    editTypeBtn.on('click', function() {
        const url = $(this).attr('data-url');
        pushAuxModalLoad({
            url: url,
            dialogClass: 'modal-dialog-centered'
        });
    });

    // submit update form...

    // delete type function
    deleteTypeBtn.on('click', function() {
        const url = $(this).attr('data-url');
        const type = $(this).attr('data-type');
        pushRequest({
            url: url,
            method: 'delete',
            dialogText: 'You are about deleting machine type, '+type+'?',
            successTitle: 'Machine Type deleted!'
        });
    });

    // delete parts supplier...
    deleteSupplierBtn.on('click', function() {
        deleteSupplier(this);
    });

    // add parts category modal
    addPartsCategoryBtn.on('click', function() {
        openAddCategoryModal();
    });

    // add part category form submission...
    addPartsCategoryForm.on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');
        resolveForm({
            form: thisForm,
            url: action,
            btnLoadingText: 'Adding...',
            method: 'post'
        })
            .then(function(data) {
                if(data.status == "success") {
                    delayRedirect(data.redirect, 100);
                }
            })
            .catch(function(e) {
                console.log(e);
            });
    });

    // delete parts category btn
    deletePartsCategoryBtn.on('click', function() {
        const name = $(this).attr('data-name');
        const url = $(this).attr('data-url');
        pushRequest({
            url: url,
            dialogText: 'You are about to delete parts category,'+name+'?',
            method: 'delete',
            successTitle: 'Machine Parts Category Deleted'
        });
    });

    // update parts category btn
    updatePartsCategoryBtn.on('click', function() {
        openUpdateCategoryModal();
    });

    // submission of parts category form
    updatePartsCategoryForm.on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');
        resolveForm({
            method: 'put',
            form: thisForm,
            url: action,
            btnLoadingText: 'Saving...'
        })
            .then(function(data) {
                if (data.status == "success") {
                    delayRedirect(data.redirect, 100);
                }
            })
            .catch(function(e) {
                console.log(e);
            })
    });

    // assign engineer modal
    assignEngineerBtn.on('click', function() {
        openAssignEngineerModal();
    });

    // submission of assign engineer form
    assignEngineerForm.on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');
        
        resolveForm({
            form:  thisForm,
            url:  action,
            btnLoadingText: '...',
            method: 'post',
        })
            .then(function(data) {
                if (data.status == "success") {
                    refreshPage();
                }
            })
            .catch(function(e) {
                console.log(e.response)
            });
    });
});