const itemDetailsBtn = $('.order_item_details_btn');
const notifyClientBtn = $('.notify_client_cac_btn');

const declineOrderBtn = $('.decline_order_btn');
const declineOrderModal = $('#decline_order_modal');

const createDispatchBtn = $('.create_dispatch_btn');

const deleteDispatchBtn = $('.delete_dispatch_btn');

const showDeclineOrderModal = function() {
    declineOrderModal.modal(function() {
        declineOrderModal.modal({
            backdrop: 'static'
        });;
    });
}

$(document).ready(function() {

    // show item details
    itemDetailsBtn.on('click', function() {
        const url = $(this).data('url');
        pushAuxModalLoad({
            url: url
        });
    });

    // show decline modal
    declineOrderBtn.on('click', function() {
        showDeclineOrderModal();
    });

    // submission of decline form
    declineOrderModal.closest('form').submit(function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');

        resolveForm({
            url: action,
            form: thisForm,
            btnLoadingText: '...',
            method: 'post'
        })
            .then(function(data) {
                if (data.status == "success") {
                    delayRedirect(data.redirect, 50);
                } else {
                    bootNotify('Something went wrong! Try again', 'error');
                }
            })
            .catch(function(e) {
                console.log(e)
            });
    });

    // create new dispatch function 
    createDispatchBtn.on('click', function() {
        const thisBtn = $(this);
        const url = thisBtn.data('url');
        pushRequest({
            url: url,
            method: 'post',
            dialogTitle: 'New Session',
            dialogText: 'You are about to create new dispatch session',
            confirmButtonText: 'Yes, Create',
            confirmBtnClass: 'btn-success',
            successTitle: 'Dispatch Session Created'
        });
    });

    // delete dispatch btn
    deleteDispatchBtn.on('click', function() {
        const url = $(this).data('url');
        const session = $(this).data('session');

        pushRequest({
            url: url,
            method: 'delete',
            dialogTitle: 'Delete Dispatch Session',
            dialogText: 'You are about to delete dispatch session, '+session,
            confirmButtonText: 'Yes, Delete',
            confirmBtnClass: 'btn-primary',
            successTitle: 'Dispatch Session Deleted'
        });
    });
});