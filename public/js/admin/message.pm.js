const composeModal = $('#compose_message_modal');
const composeBtn = $('.compose_message_btn');
let selectedRows = [];

const removeTableRow = function(rowId) {
    selectedRows = selectedRows.filter(function(x) {
        return x != rowId
    });
}

const toggleTableRow = function(rowId) {
    if (!selectedRows.includes(rowId)) {
        selectedRows.push(rowId);
    } else {
        removeTableRow(rowId);
    }
}

const highlightTable = function(table) {
    table.find('tbody tr').each(function(index, elem) {
        const id = $(elem).data('id');
    
        if (selectedRows.includes(id)) {
            $(elem).addClass('selected');
        } else {
            $(elem).removeClass('selected');
        }
    });
}


$(document).ready(function() {

    composeModal.find('select[name="recipients[]"]').select2({
        placeholder: "select recipients"
    });

   simpleEditor('.summernote');
   
   simpleDataTable('.table.dt', {ordering: false})
    // compose message modal
    composeBtn.on('click', function() {
        composeModal.find('form').get(0).reset();
        composeModal.modal({backdrop:'static'});
    });

    // submission of compose message form
    composeModal.find('form').on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');
        resolveForm({
            url: action,
            form: thisForm,
            btnLoadingText: 'Sending...',
            method: 'post'
        })
            .then(function(data) {
                if (data.status == "success") {
                    delayRedirect(data.redirect);
                }
            })
            .catch(function(e) {
                console.log(e)
            });
    });

    // selection of rows
    $('.table tr input[type="checkbox"]').on('change', function() {
        const rowId = $(this).parents('tr').data('id');
        toggleTableRow(rowId);
        highlightTable($(this).parents('.table'))
    });
});