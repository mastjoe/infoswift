$(document).ready(function() {
    $(document).on('submit', '#send_message_form', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');

        resolveForm({
            url: action,
            form: thisForm,
            btnLoadingText: 'Sending...',
            method: 'post',
        })
            .then(function(data) {
                if (data.status) {
                    auxModal.modal('hide');
                    Swal.fire('Message Sent',data.message, 'success');
                    refreshPage(4000);
                }
            })
            .catch(function(e) {
                console.log(e)
            });
    });

    $('.message_staff_btn').on('click', function() {
        const url = $(this).data('url');
        pushAuxModalLoad({
            url: url,
            callback: function() {
                simpleEditor('textarea.summernote');
            }
        });
    });
});