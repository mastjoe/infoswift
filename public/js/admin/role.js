const deleteRoleBtn = $('.delete_role_btn');

$(document).ready(function() {
    // delete function
    deleteRoleBtn.on('click', function() {
        const role = $(this).attr('data-role');
        const url = $(this).attr('data-url');

        pushRequest({
            url: url,
            dialogText: 'you are about deleting role, '+role,
            method: 'delete',
            successTitle: 'Role Deleted'
        });
    });
});