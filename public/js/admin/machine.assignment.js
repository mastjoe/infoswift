const selectInteraction = function () {
    const table = $('.sel');
    activeClass = "selected";
    let selectedMachines = [];
    let selectedEngineers = [];
    const machineAction = $('.machine-action');
    const engineerAction = $('.engineer-action');

    const tableChecker = function () {
        
        table.find('tr').each(function(index, el) {
            const check = $(el).find('input[type="checkbox"], input[type="radio"]');
            
            if (check.is(':checked')) {
                $(el).addClass(activeClass);
                sortRow(el, true)
            } else {
                $(el).removeClass(activeClass);
                sortRow(el);
            }
        });

        controlActions()
    }

    const sortRow = function(row, checked=false) {
        const table = $(row).parents('table.table');
        const status = table.attr('data-table');
        const id = $(row).attr('data-id');
        if (status == "machines") {
            if (checked) {
                selectedMachines.push(id);
            } else {
                selectedMachines = removeFromArray(id, selectedMachines);
            }
        } else {
            if (checked) {
                selectedEngineers.push(id);
            } else {
                selectedEngineers = removeFromArray(id, selectedEngineers);
            }
        }
    }

  

    const controlActions = function() {
        if (selectedMachines.length) {
            machineAction.removeClass('d-none');
        } else {
            machineAction.addClass('d-none');
        }

        if (selectedEngineers.length) {
            engineerAction.removeClass('d-none');
        } else {
            engineerAction.addClass('d-none');
        }
    }


    table.find('input[type="checkbox"], input[type="radio"]').on('change', function() {
        tableChecker();
    });

    tableChecker();
}

const dropAllChecked = function (event) {
    const table = $(event.target).parents('.card').find('table.table');
    table.find('tr').each(function (ind, el) {
        $(el).find('input[type="checkbox"], input[type="radio"]').prop('checked', false);
    });
    selectInteraction();
}

const assignEngineerBtn = $('.assign_engineer_btn');
const assignEngineerModal = $('#assign_engineer_modal');
const assignEngineerForm = $('#assign_engineer_form');

const showAssignEngineerModal = function() {
    assignEngineerModal.modal({backdrop:'static'})
}

$(document).ready(function() {

    $('#engineer_select').select2({placeholder:'Select Engineers'})
    // assign engineer btn
    assignEngineerBtn.on('click', function() {
        showAssignEngineerModal();
    });

    // submission of assign engineer form
    assignEngineerForm.on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');

        resolveForm({
            url: action,
            form: thisForm,
            btnTextLoading: '...',
            method: 'post'
        })
            .then(function(data) {
                if (data.status == "success") {
                    refreshPage(100);
                }
            })
            .catch(function(e) {
                console.log(e)
            });
    })
});