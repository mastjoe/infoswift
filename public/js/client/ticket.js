const selectTicketMachine = function(target) {
    const t = $(target);
    console.log(t);
    const selectedOption = t.find('option:selected');
    const branchTarget = t.parents('form').find('#branch');
    const regionTarget = t.parents('form').find('#region');
    const showTarget = t.parents('form').find('.machine_depend');

    showTarget.addClass('d-none');

    if (t.val() != "") {
        branchTarget.val(selectedOption.attr('data-branch'));
        regionTarget.val(selectedOption.attr('data-region'));
        showTarget.removeClass('d-none');
    }
}

const addFaultField = function() {
    const appendPoint = $('.append-point');
    const inputTag = 
    '<div class="form-group row">'+
    '<label class="col-md-3 col-form-label text-md-right"></label>'+
    '<div class="col-md-9">'+
    '<input class="form-control" style="width:94%; display:inline-block" type="text" name="faults[]" required >'+
    '<span class="drop_btn ml-2" title="drop"><i class="fa fa-times-circle"></i></span>'+
    '</div>';
    const htmlInputTag = $(inputTag);
    const dropBtn = htmlInputTag.find('.drop_btn')[0].addEventListener('click', dropFaultField);
    appendPoint.append(htmlInputTag);
}

const dropFaultField = function() {
    const parent = $(event.target).parents('.form-group');
    parent.remove();
}


const deleteTicket = function(target) {
    const t = $(target);
    const url = t.attr('data-url');
    const name = t.attr('data-name');

    pushRequest({
        url: url,
        method: 'delete',
        dialogTitle: 'Delete Ticket',
        dialogText: 'You are about to delete ticket '+name+'?',
        successTitle: 'Ticket Deleted',
    });
}

$(document).ready(function() {
    const addLogModal = $('#add_log_modal');
    const addLogBtn = $('.add_log_btn');

    addLogBtn.on('click', function() {
        addLogModal.modal({backdrop: 'static'});
    });

    simpleEditor('#add_log_modal .summernote', {placeholder: 'message here...'});

    // submission of add log form
    addLogModal.find('form').on('submit', function(e){
        e.preventDefault();
        const thisForm = $(this);
        const action = thisForm.attr('action');

        if (thisForm.find('.summernote').summernote('isEmpty')) {
            bootNotify('message can not be empty', 'primary');
            return
        }
        
        resolveForm({
            url: action,
            form: thisForm,
            btnLoadingText: '...',
            method: 'post'
        })
            .then(function(data) {
                if (data.status == "success") {
                    refreshPage();
                }
            })
            .catch(function(e) {
                console.log(e);
            });
    });
});