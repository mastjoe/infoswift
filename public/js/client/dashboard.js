const monthTicketChartTarget = $('#month_ticket_chart');
const pmChartTarget = $('#pm_chart_target');
const machineStatusChartTarget = $('#machine_status_chart_target');

const loadMonthTicketChartData = function(year = "") {
    const url = monthTicketChartTarget.data('url');
    const plotChart = function(data) {
        const ctx = monthTicketChartTarget.find('canvas').get(0).getContext('2d');
        const chart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: data.months,
                datasets: [{
                    label:'Raised Tickets',
                    data: data.openTickets,
                    fill: true,
                    lineTension: .5,
                    borderCapStyle: "butt",
                    borderJoinStyle: "miter",
                    backgroundColor: "rgba(241, 108, 105, 0.2)",
                    borderColor: "#f16c69",
                    pointBorderColor: "#f16c69",
                    pointBackgroundColor: "#fff",
                    borderWidth: 0.5,
                },{
                    label:'Resolved Tickets',
                    data: data.closedTickets,
                    fill: true,
                    lineTension: .5,
                    borderCapStyle: "butt",
                    borderJoinStyle: "miter",
                    backgroundColor: "rgba(124, 191, 91, 0.5)",
                    borderColor: "#a3d38d",
                    pointBorderColor: "#a3d38d",
                    pointBackgroundColor: "#fff",
                    borderWidth: 0.5,
                }],
            },
            options: {
                responsive: true,
                maintainAspectRatio: true,
                scales: {
                    yAxes: [{
                        ticks: {
                            // max: 100,
                            min: 0,
                            stepSize: 5
                        }
                    }]
                }
            }
        });

        $('.ticket_year_tag').text(data.year);
    }

    axios.get(url, {params: {'year': year}})
        .then(function({data}) {
            plotChart(data);
        })
        .catch(function(e) {
            console.log(e);
        });
}

const loadPmPolarChart = function(year="") {
    const url = pmChartTarget.data('url');
    const plotChart = function(data) {
        const chartData = {};
        chartData.datasets = [{data:[], backgroundColor:[]}];
        chartData.labels = [];

        data.pms.map(function(d) {
            chartData.datasets[0].data.push(d.pms);
            chartData.labels.push(d.quarter);
        });
        chartData.datasets[0].backgroundColor = ["#ff4040", "#4BC0C0", "#FFCE56", "#E7E9ED"];
        chartData.datasets[0].label = "Quarters";

        console.log(chartData);
        const ctx = pmChartTarget.find('canvas').get(0).getContext('2d');
        const chart = new Chart(ctx, {
            type: 'polarArea',
            data: chartData
        });
        $('. pm-year-dropdown').text(data.year)
    }

    axios.get(url, {params: {year: year}})
        .then(function({data}) {
            // console.log(data)
            plotChart(data);
        })
        .catch(function(e) {
            console.log(e)
        });
}

const loadMachineStatusChart = function() {
    const url = machineStatusChartTarget.data('url');
    const plotChart = function(data) {
        chartData = [];
        data.map(function(d) {
            chartData.push({label:d.status, value:d.machines});
        });

        const donut = Morris.Donut({
            element: machineStatusChartTarget.attr('id'),
            data: chartData
        });
    }

    axios.get(url)
        .then(function({data}) {
            console.log(data)
            plotChart(data);
        })
        .catch(function(e) {
            console.log(e)
        })
}

$(document).ready(function() {
    loadMonthTicketChartData();

    $('.ticket-year-dropdown a.dropdown-item').on('click', function() {
        loadMonthTicketChartData($(this).text())
    });

    loadPmPolarChart();

    $('.pm-year-dropdown a.dropdown-item').on('click', function () {
        loadPmPolarChart($(this).text())
    });

    loadMachineStatusChart();
})