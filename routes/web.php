<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function() {
    return redirect()->route('login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// client auth
Route::group([
    'namespace' => 'Auth',
    'prefix' => 'clients'
], function() {

    Route::get('login', 'ClientLoginController@showLoginForm')->name('client.login.form');
    Route::post('login', 'ClientLoginController@login')->name('client.login');
    Route::post('logout', 'ClientLoginController@logout')->name('client.logout');
    
    Route::get('password/reset', 'ClientForgotPasswordController@showLinkRequestForm')->name('client.password.request');
    Route::post('password/email', 'ClientForgotPasswordController@sendResetLinkEmail')->name('client.password.email');
    Route::get('password/reset/{token}', 'ClientResetPasswordController@showResetForm')->name('client.password.reset.form');
    Route::post('password/reset', 'ClientResetPasswordController@reset')->name('client.password.reset');
});

// client account
Route::group([
    'middleware' => ['auth:client', 'share'],
    'prefix' => 'account',
    'namespace' => 'Client'
], function() {

    // dashboard
    Route::redirect('/', 'account/dashboard/')->name('client.index');
    Route::get('dashboard/', 'DashboardController@index')->name('client.dashboard');
    Route::get('dashboard/ticket-chart-data', 'DashboardController@ticketChartData')->name('client.ticket.chart.data');
    Route::get('dashboard/pm-chart-data', 'DashboardController@pmChartData')->name('client.pm.chart.data');
    Route::get('dashboard/machine-status-data', 'DashboardController@machinesStatusesData')->name('client.machines.status.chart.data');

    // tickets...
    Route::get('tickets', 'TicketController@index')->name('client.tickets');
    Route::get('tickets/create', 'TicketController@create')->name('client.create.ticket');
    Route::post('tickets/store', 'TicketController@store')->name('client.store.ticket');
    Route::get('tickets/{ref}', 'TicketController@show')->name('client.show.ticket');
    Route::get('tickets/{ref}/edit', 'TicketController@edit')->name('client.edit.ticket');
    Route::put('tickets/{ref}/update', 'TicketController@update')->name('client.update.ticket');
    Route::delete('tickets/{ref}/delete', 'TicketController@destroy')->name('client.delete.ticket');
    Route::get('tickets/{ref}/logs', 'TicketController@showLogs')->name('client.ticket.logs');
    Route::post('tickets/{ref}/logs', 'TicketController@storeLog')->name('client.create.ticket.log');

    // machines
    Route::get('machines', 'MachineController@index')->name('client.machines');
    Route::get('machines/{id}', 'MachineController@show')->name('client.show.machine');
    Route::get('machines/{id}/tickets', 'MachineController@showTickets')->name('client.show.machine.tickets');
    Route::get('machines/{id}/pms', 'MachineController@showPMs')->name('client.show.machine.pms');

    // profile
    Route::get('profile', 'ProfileController@index')->name('client.profile');
    Route::get('profile/regions', 'ProfileController@regions')->name('client.profile.regions');
    Route::get('profile/branches', 'ProfileController@branches')->name('client.profile.branches');
    Route::get('profile/machines', 'ProfileController@machines')->name('client.profile.machines');

    // notifications
    Route::get('notifications', 'NotificationController@index')->name('client.notifications');
    Route::get('notifications/api', 'NotificationController@api')->name('client.notifications.api');
    Route::get('notifications/{id}', 'NotificationController@show')->name('client.show.notification');
    Route::delete('notifications/{id}/delete', 'NotificationController@delete')->name('client.delete.notification');
});

Route::group([
    'middleware' => ['auth', 'share'],
    'prefix' => 'admin'
], function() {
    // dashboard
    Route::redirect('/', 'admin/dashboard/')->name('admin.index');
    Route::get('dashboard/', 'DashboardController@index')->name('admin.dashboard');
    Route::get('dashboard/tickets-data', 'DashboardController@ticketChartData')->name('dashboard.ticket.chart.data');

    // clients...
    Route::get('clients', 'ClientController@index')->name('all.clients');
    Route::get('clients/create', 'ClientController@create')->name('create.client');
    Route::post('clients/store', 'ClientController@store')->name('store.client');
    Route::get('clients/machines/json', 'ClientController@machinesJSON')->name('json.client.machines');
    Route::get('clients/{id}', 'ClientController@show')->name('show.client');
    Route::get('clients/{id}/edit', 'ClientController@edit')->name('edit.client');
    Route::post('clients/{id}/update', 'ClientController@update')->name('update.client');
    Route::delete('clients/{id}/delete', 'ClientController@destroy')->name('delete.client');
    Route::get('clients/{id}/logo', 'ClientController@editLogo')->name('edit.client.logo');
    Route::post('clients/{id}/logo', 'ClientController@storeLogo')->name('update.client.logo');
    Route::get('clients/{id}/machines', 'ClientController@showMachines')->name('show.client.machines');
    Route::get('clients/{id}/regions', 'ClientController@showRegions')->name('show.client.regions');
    Route::get('clients/{id}/branches', 'ClientController@showBranches')->name('show.client.branches');

    Route::get('clients/{id}/assign-supervisor', 'ClientController@assignSupervisorForm')->name('assign.client.supervisor.form');
    Route::put('clients/{id}assign-supervisor', 'ClientController@updateSupervisor')->name('assign.client.supervisor');

    Route::post('clients/{id}/reset-password', 'ClientController@resetPassword')->name('reset.client.password');
    
    Route::put('clients/{id}/personnel/store', 'ClientController@storePersonnel')->name('store.client.personnel');

    // staff roles
    Route::get('staff/roles', 'RoleController@index')->name('all.staff.roles');
    Route::get('staff/roles/create', 'RoleController@create')->name('create.staff.role');
    Route::post('staff/roles/store', 'RoleController@store')->name('store.staff.role');
    Route::get('staff/roles/donut', 'RoleController@donut')->name('staff.roles.donut');
    Route::get('staff/roles/{id}', 'RoleController@show')->name('show.staff.role');
    Route::get('staff/roles/{id}/edit', 'RoleController@edit')->name('edit.staff.role');
    Route::put('staff/roles/{id}/update', 'RoleController@update')->name('update.staff.role');
    Route::delete('staff/roles/{id}/delete', 'RoleController@destroy')->name('delete.staff.role');

    // staff privileges
    Route::get('staff/privileges', 'PrivilegeController@index')->name('all.privileges');
    Route::get('staff/privileges/{id}', 'PrivilegeController@show')->name('show.privilege');
    Route::put('staff/privileges/{id}', 'PrivilegeController@update')->name('update.privilege');

    // staff
    Route::get('staff', 'StaffController@index')->name('all.staff');
    Route::get('staff/create', 'StaffController@create')->name('create.staff');
    Route::post('staff/store', 'StaffController@store')->name('store.staff');
    Route::get('staff/{id}', 'StaffController@show')->name('show.staff');
    Route::get('staff/{id}/terminals', 'StaffController@showMachines')->name('show.staff.machines');
    Route::get('staff/{id}/edit', 'StaffController@edit')->name('edit.staff');
    Route::put('staff/{id}/update', 'StaffController@update')->name('update.staff');
    Route::post('staff/{id}/update-photo', 'StaffController@updatePhoto')->name('update.staff.photo');
    Route::delete('staff/{id}/delete', 'StaffController@destroy')->name('delete.staff');
    Route::post('staff/{id}/suspend', 'StaffController@suspend')->name('suspend.staff');
    Route::post('staff/{id}/unsuspend', 'StaffController@unsuspend')->name('unsuspend.staff');
    Route::get('staff/{id}/roles', 'StaffController@showRoles')->name('show.staff.roles');
    Route::put('staff/{id}/roles/update', 'StaffController@updateRoles')->name('update.staff.roles');
    Route::put('staff/{id}/privileges/update', 'StaffController@updatePrivileges')->name('update.staff.privileges');
    Route::post('staff/{id}/privileges/assign-all', 'StaffController@assignAllPrivileges')->name('assign.staff.all.privileges');
    Route::post('staff/{id}/reset-password', 'StaffController@resetPassword')->name('reset.staff.password');

    // staff guarantors
    Route::get('staff/{id}/guarantors', 'StaffController@showGuarantors')->name('show.staff.guarantors');
    Route::post('staff/{id}/guarantors/store', 'StaffController@storeGuarantors')->name('store.staff.guarantor');
    Route::delete('staff/guarantors/{id}/delete', 'StaffController@deleteGuarantor')->name('delete.staff.guarantor');
    Route::get('staff/guarantors/{id}/edit', 'StaffController@editGuarantor')->name('edit.staff.guarantor');
    Route::post('staff/guarantors/{id}/update', 'StaffController@updateGuarantor')->name('update.staff.guarantor');

    // machine assignment
    Route::get('machines/assignment', 'MachineAssignmentController@index')->name('machine.assignment');
    Route::get('machines/assignment/states/{id}', 'MachineAssignmentController@show')->name('machine.assignment.state');
    Route::get('machines/assignment/states/{id}/assign-machines', 'MachineAssignmentController@assignMachineForm')->name('machine.assignment.state.assign.form');
    Route::post('machines/assignment/states/{id}/assign-machines', 'MachineAssignmentController@assignMachine')->name('machine.assignment.state.assign');

    // machine parts...
    Route::get('machines/parts', 'MachinePartController@index')->name('all.machine.parts');
    Route::get('machines/parts/create', 'MachinePartController@create')->name('create.machine.part');
    Route::post('machines/parts/store', 'MachinePartController@store')->name('store.machine.part');

    // machine parts order
    Route::get('machines/parts/orders', 'MachinePartOrderController@index')->name('all.machine.parts.order');
    Route::get('machines/parts/orders/{ref}', 'MachinePartOrderController@show')->name('show.machine.parts.order');
    Route::get('machines/part/orders/{ref}/dispatches', 'MachinePartOrderDispatchController@index')->name('show.machine.parts.order.dispatches');
    Route::get('machines/part/orders/{ref}/cac', 'MachinePartOrderController@showCAC')->name('show.machine.parts.order.cac');

    // part order dispatch
    Route::get('machines/part/orders/dispatches/{ref}', 'MachinePartOrderDispatchController@show')->name('show.machine.parts.order.dispatch');
    Route::post('machines/part/orders/dispatches/{ref}/create', 'MachinePartOrderDispatchController@store')->name('create.machine.parts.order.dispatch');
    Route::delete('machines/part/orders/dispatches/{ref}/delete', 'MachinePartOrderDispatchController@destroy')->name('delete.machine.parts.order.dispatch');

    Route::post('machines/part/orders/{ref}/decline', 'MachinePartOrderController@declineOrder')->name('decline.machine.parts.order');
    Route::get('machines/part/orders/item/{id}', 'MachinePartOrderController@showItemDetails')->name('show.machine.parts.order.item-details');
    Route::get('machines/part/orders/item/{id}/notify-client', 'MachinePartOrderController@notifyClientCAC')->name('machine.parts.order.item.notify-client');
    
    // machine parts order history
    Route::get('machines/parts/my-orders', 'MachinePartOrderController@showMyOrder')->name('show.my.machine.part.orders');
    Route::get('machines/parts/my-orders/{ref}', 'MachinePartOrderController@showMyOrderDetails')->name('show.my.machine.part.orders.details');

    Route::get('machines/parts/{id}', 'MachinePartController@show')->name('show.machine.part');
    Route::get('machines/parts/{id}/edit', 'MachinePartController@edit')->name('edit.machine.part');
    Route::post('machines/parts/{id}/update', 'MachinePartController@update')->name('update.machine.part');
    Route::delete('machines/parts/{id}/delete', 'MachinePartController@destroy')->name('delete.machine.part');
    Route::get('machines/parts/{id}/images', 'MachinePartController@showImages')->name('show.machine.part.images');
    Route::get('machines/parts/{id}/images/create', 'MachinePartController@addImage')->name('add.machine.part.images');
    Route::post('machines/parts/{id}/images/store', 'MachinePartController@storeImage')->name('store.machine.part.images');
    Route::delete('machines/parts/{id}/images/{image_id}', 'MachinePartController@deleteImage')->name('delete.machine.part.image');

    // machine stocks...
    Route::get('machines/stocks', 'MachinePartStockController@index')->name('all.machine.stock');
    Route::get('machines/stocks/create', 'MachinePartStockController@create')->name('create.machine.stock');
    Route::post('machines/stocks/store', 'MachinePartStockController@store')->name('store.machine.stock');
    Route::get('machines/stocks/{id}', 'MachinePartStockController@show')->name('show.machine.stock');
    Route::get('machines/stocks/{id}/edit', 'MachinePartStockController@edit')->name('edit.machine.stock');
    Route::put('machines/stocks/{id}/update', 'MachinePartStockController@update')->name('update.machine.stock');
    Route::delete('machines/stocks/{id}/delete', 'MachinePartStockController@destroy')->name('delete.machine.stock');

    Route::get('machine/stocks/{id}/supplies/create', 'MachinePartStockController@createSupply')->name('add.stock.supply');
    Route::post('machine/stocks/{id}/supplies/store', 'MachinePartStockController@storeSupply')->name('store.stock.supply');
    Route::get('machine/stocks/{id}/supplies/{supply_id}', 'MachinePartStockController@showSupply')->name('show.stock.supply');

    Route::get('machine/stocks/{id}/supplies/{supply_id}/edit', 'MachinePartStockController@editSupply')->name('edit.stock.supply');
    Route::put('machine/stocks/{id}/supplies/{supply_id}/edit', 'MachinePartStockController@updateSupply')->name('update.stock.supply');
    Route::delete('machine/stocks/{id}/supplies/{supply_id}/delete', 'MachinePartStockController@deleteSupply')->name('delete.stock.supply');


    // machine part cart...
    Route::get('machines/part/cart', 'MachinePartCartController@index')->name('machine.parts.cart');
    Route::get('machines/part/cart/data', 'MachinePartCartController@getData')->name('machine.parts.cart.data');
    Route::post('machine/part/cart/add', 'MachinePartCartController@add')->name('add.to.machine.parts.cart');
    Route::post('machine/part/cart/checkout', 'MachinePartCartController@checkout')->name('checkout.machine.parts.cart');
    Route::post('machine/part/cart/{id}/update', 'MachinePartCartController@update')->name('update.machine.parts.cart.item');
    Route::delete('machine/part/cart/{id}/remove', 'MachinePartCartController@remove')->name('remove.from.machine.parts.cart');

    // machine status...
    Route::get('machines/statuses', 'MachineStatusController@index')->name('all.machine.statuses');
    Route::get('machines/statuses/create', 'MachineStatusController@create')->name('create.machine.status');
    Route::post('machines/statuses/store', 'MachineStatusController@store')->name('store.machine.status');
    Route::get('machines/statuses/{id}', 'MachineStatusController@show')->name('show.machine.status');
    Route::get('machines/statuses/{id}/edit', 'MachineStatusController@edit')->name('edit.machine.status');
    Route::put('machines/statuses/{id}/update', 'MachineStatusController@update')->name('update.machine.status');
    Route::delete('machines/statuses/{id}/delete', 'MachineStatusController@destroy')->name('delete.machine.status');

    // machine parts...
    // Route::get('machines/parts', 'MachineParts');

    // machine pms
    Route::get('machines/pms', 'PmController@index')->name('all.machines.pms');
    Route::get('machines/pms/{ref}', 'PmController@show')->name('machine.pms.details');
    Route::post('machines/pms/{id}/accept', 'PmController@acceptPm')->name('accept.machine.pms');
    Route::post('machines/pms/{id}/decline', 'PmController@declinePm')->name('decline.machine.pms');

    // machines
    Route::get('machines', 'MachineController@index')->name('all.machines');
    Route::post('machines/store', 'MachineController@store')->name('store.machine');
    Route::get('machines/create', 'MachineController@create')->name('create.machine');
    Route::get('machines/{id}', 'MachineController@show')->name('show.machine');
    Route::get('machines/{id}/edit', 'MachineController@edit')->name('edit.machine');
    Route::put('machines/{id}/update', 'MachineController@update')->name('update.machine');
    Route::delete('machines/{id}/delete', 'MachineController@destroy')->name('delete.machine');
    Route::get('machines/{id}/tickets', 'MachineController@tickets')->name('show.machine.tickets');
    Route::post('machines/{id}/assign-engineer', 'MachineController@assignEngineer')->name('assign.engineer.to.machine');
    Route::post('machines/{id}/pms', 'MachineController@storePm')->name('store.machine.pm');
    Route::get('machines/{id}/pms/{year?}', 'MachineController@pms')->name('show.machine.pms');
    Route::get('machines/{id}/pms/{year}/quarter/{quarter}', 'MachineController@showPmQuarter')->name('show.machine.pms.quarter');
    Route::delete('machine/pms/{id}/delete', 'MachineController@deletePm')->name('delete.machine.pm');
    Route::get('machine/pms/{id}/edit', 'MachineController@editPm')->name('edit.machine.pm');
    Route::post('machine/pms/{id}/update', 'MachineController@updatePm')->name('update.machine.pm');

    // tickets
    Route::get('tickets', 'TicketController@index')->name('all.tickets');
    Route::get('tickets/create', 'TicketController@create')->name('create.ticket');
    Route::get('tickets/closure-requests', 'TicketController@showClosureRequests')->name('show.ticket.closure.requests');
    Route::post('tickets/store', 'TicketController@store')->name('store.ticket');
    Route::get('tickets/{id}', 'TicketController@show')->name('show.ticket');
    Route::get('tickets/{id}/logs', 'TicketController@showLogs')->name('show.ticket.logs');
    Route::post('tickets/{id}/log', 'TicketController@storeLog')->name('store.ticket.log');
    Route::get('tickets/{id}/close', 'TicketClosureController@show')->name('show.close.ticket.form');
    Route::post('tickets/{id}/close', 'TicketClosureController@close')->name('close.ticket');
    Route::get('tickets/{id}/re-close', 'TicketClosureController@reCloseForm')->name('reclose.ticket');
    Route::post('tickets/{id}/re-close', 'TicketClosureController@update')->name('update.ticket');
    Route::get('tickets/{id}/confirm-close', 'TicketClosureController@confirmCloseForm')->name('confirm.ticket.closure.form');
    Route::post('tickets/{id}/confirm-close', 'TicketClosureController@confirmClose')->name('confirm.ticket.closure');
    Route::post('tickets/{id}/confirm-close', 'TicketClosureController@confirmClose')->name('confirm.ticket.closure');
    Route::post('tickets/{id}/decline-close', 'TicketClosureController@declineClose')->name('decline.ticket.closure');
    Route::get('tickets/{id}/decline-close', 'TicketClosureController@declineCloseForm')->name('decline.ticket.closure.form');
    Route::get('tickets/{id}/escalations', 'TicketController@showEscalation')->name('show.ticket.escalations');
    Route::post('tickets/{id}/escalate', 'TicketController@escalate')->name('escalate.ticket');
    Route::post('tickets/{id}/upload/frf', 'TicketController@uploadFRF')->name('upload.ticket.frf');
    Route::post('tickets/{id}/upload/jcf', 'TicketController@uploadJCF')->name('upload.ticket.jcf');

    Route::get('machine-faults/{id}/solutions', 'TicketClosureController@machineFaultSolutions')->name('get.machine-fault.solutions');

    // settings 
    // locations
    Route::get('settings/location', 'SettingController@showLocations')->name('settings.locations');
    // countries
    Route::get('settings/location/countries', 'CountryController@index')->name('all.countries');
    Route::get('settings/location/countries/create', 'CountryController@create')->name('create.country');
    Route::post('settings/location/countries/store', 'CountryController@store')->name('store.country');
    Route::get('settings/location/countries/{id}', 'CountryController@show')->name('show.country');
    Route::get('settings/location/countries/{id}/edit', 'CountryController@edit')->name('edit.country');
    Route::put('settings/location/countries/{id}/update', 'CountryController@update')->name('update.country');
    Route::delete('settings/location/countries/{id}/delete', 'CountryController@destroy')->name('delete.country');
    Route::get('settings/location/countries/{id}/create/state', 'CountryController@addState')->name('create.country.state');
    Route::post('settings/location/countries/{id}/store/state', 'CountryController@storeState')->name('store.country.state');
    // states
    Route::get('settings/location/states', 'StateController@index')->name('all.states');
    Route::get('settings/location/states/create', 'StateController@create')->name('create.state');
    Route::post('settings/location/states/store', 'StateController@store')->name('store.state');
    Route::get('settings/location/states/{id}', 'StateController@show')->name('show.state');
    Route::get('settings/location/states/{id}/edit', 'StateController@edit')->name('edit.state');
    Route::put('settings/location/states/{id}/update', 'StateController@update')->name('update.state');
    Route::delete('settings/location/states/{id}/delete', 'StateController@destroy')->name('delete.state');
    Route::get('settings/location/states/{id}/create/region', 'StateController@createRegion')->name('create.state.region');
    Route::post('settings/location/states/{id}/store/region', 'StateController@storeRegion')->name('store.state.region');
    Route::get('settings/location/states/{id}/engineer/assign', 'StateController@assignEngineerForm')->name('assign.state.engineer.form');
    Route::post('settings/location/states/{id}/engineer/assign', 'StateController@assignEngineer')->name('assign.state.engineer');
    Route::post('settings/location/states/{id}/engineer/{engineer}/drop/', 'StateController@dropEngineer')->name('drop.state.engineer');

    // regions
    Route::get('settings/location/regions', 'RegionController@index')->name('all.regions');
    Route::get('settings/location/regions/create', 'RegionController@create')->name('create.region');
    Route::post('settings/location/regions/store', 'RegionController@store')->name('store.region');
    Route::get('settings/location/regions/{id}', 'RegionController@show')->name('show.region');
    Route::get('settings/location/regions/{id}/edit', 'RegionController@edit')->name('edit.region');
    Route::put('settings/location/regions/{id}/update', 'RegionController@update')->name('update.region');
    Route::delete('settings/location/regions/{id}/delete', 'RegionController@destroy')->name('delete.region');
    Route::get('settings/location/regions/{id}/create/branch', 'RegionController@createBranch')->name('create.region.branch');
    Route::post('settings/location/regions/{id}/store/branch', 'RegionController@storeBranch')->name('store.region.branch');

    // branches
    Route::get('settings/location/branches', 'BranchController@index')->name('all.branches');
    Route::get('settings/location/branches/create', 'BranchController@create')->name('create.branch');
    Route::post('settings/location/branches/store', 'BranchController@store')->name('store.branch');
    Route::get('settings/location/branches/{id}', 'BranchController@show')->name('show.branch');
    Route::get('settings/location/branches/{id}/edit', 'BranchController@edit')->name('edit.branch');
    Route::put('settings/location/branches/{id}/update', 'BranchController@update')->name('update.branch');
    Route::delete('settings/location/branches/{id}/delete', 'BranchController@destroy')->name('delete.branch');
    Route::get('settings/location/branches/{id}/create/machine', 'BranchController@createMachine')->name('create.branch.machine');
    Route::post('settings/location/branches/{id}/store/machine', 'BranchController@storeMachine')->name('store.branch.machine');
    Route::get('settings/location/branches/{id}/create/custodian', 'BranchController@createCustodian')->name('create.branch.custodian');
    Route::post('settings/location/branches/{id}/store/custodian', 'BranchController@storeCustodian')->name('store.branch.custodian');
    Route::delete('settings/location/branches/{id}/delete/custodian/{custodian}/', 'BranchController@deleteCustodian')->name('delete.branch.custodian');
    Route::get('settings/location/branches/{id}/edit/custodian/{custodian}/', 'BranchController@editCustodian')->name('edit.branch.custodian');
    Route::put('settings/location/branches/{id}/update/custodian/{custodian}/', 'BranchController@updateCustodian')->name('update.branch.custodian');

    // settings machines
    Route::get('settings/machines', 'SettingController@showMachines')->name('settings.machines');

    // machine faults
    Route::get('settings/machines/faults', 'MachineFaultController@index')->name('all.machine.faults');
    Route::get('settings/machines/faults/create', 'MachineFaultController@create')->name('create.machine.fault');
    Route::post('settings/machines/faults/store', 'MachineFaultController@store')->name('store.machine.fault');
    Route::get('settings/machines/faults/{f}', 'MachineFaultController@show')->name('show.machine.fault');
    Route::get('settings/machines/faults/{f}/edit', 'MachineFaultController@edit')->name('edit.machine.fault');
    Route::put('settings/machines/faults/{f}/update', 'MachineFaultController@update')->name('update.machine.fault');
    Route::delete('settings/machines/faults/{f}/delete', 'MachineFaultController@destroy')->name('delete.machine.fault');

    // machine vendors
    Route::get('settings/machines/vendors', 'MachineVendorController@index')->name('all.machine.vendors');
    Route::get('settings/machines/vendors/create', 'MachineVendorController@create')->name('create.machine.vendor');
    Route::post('settings/machines/vendors/store', 'MachineVendorController@store')->name('store.machine.vendor');
    Route::get('settings/machines/vendors/{v}', 'MachineVendorController@show')->name('show.machine.vendor');
    Route::get('settings/machines/vendors/{v}/edit', 'MachineVendorController@edit')->name('edit.machine.vendor');
    Route::put('settings/machines/vendors/{v}/update', 'MachineVendorController@update')->name('update.machine.vendor');
    Route::delete('settings/machines/vendors/{v}/delete', 'MachineVendorController@destroy')->name('delete.machine.vendor');


    // machine types...
    Route::get('settings/machines/types', 'MachineTypeController@index')->name('all.machine.types');
    Route::get('settings/machines/types/create', 'MachineTypeController@create')->name('create.machine.type');
    Route::post('settings/machines/types/store', 'MachineTypeController@store')->name('store.machine.type');
    Route::get('settings/machines/types/{id}', 'MachineTypeController@show')->name('show.machine.type');
    Route::get('settings/machines/types/{id}/edit', 'MachineTypeController@edit')->name('edit.machine.type');
    Route::put('settings/machines/types/{id}/update', 'MachineTypeController@update')->name('update.machine.type');
    Route::delete('machines/types/{id}/delete', 'MachineTypeController@destroy')->name('delete.machine.type');

    // machine parts suppliers...
    Route::get('settings/machines/part-suppliers', 'MachinePartSupplierController@index')->name('all.parts.suppliers');
    Route::get('settings/machines/part-suppliers/create', 'MachinePartSupplierController@create')->name('create.parts.supplier');
    Route::post('settings/machines/part-suppliers/store', 'MachinePartSupplierController@store')->name('store.parts.supplier');
    Route::get('settings/machines/part-suppliers/{id}', 'MachinePartSupplierController@show')->name('show.parts.supplier');
    Route::get('settings/machines/part-suppliers/{id}/edit', 'MachinePartSupplierController@edit')->name('edit.parts.supplier');
    Route::get('settings/machines/part-suppliers/{id}/supply-history', 'MachinePartSupplierController@showSupplyHistory')->name('show.parts.supplier.history');
    Route::put('settings/machines/part-suppliers/{id}/update', 'MachinePartSupplierController@update')->name('update.parts.supplier');
    Route::delete('settings/machines/part-suppliers/{id}/delete', 'MachinePartSupplierController@destroy')->name('delete.parts.supplier');

    // machine part supplier 
    Route::get('settings/machines/part-categories', 'MachinePartCategoryController@index')->name('all.parts.categories');
    Route::get('settings/machines/part-categories/create', 'MachinePartCategoryController@create')->name('create.parts.category');
    Route::post('settings/machines/part-categories/store', 'MachinePartCategoryController@store')->name('store.parts.category');
    Route::get('settings/machines/part-categories/{id}', 'MachinePartCategoryController@show')->name('show.parts.category');
    Route::get('settings/machines/part-categories/{id}/edit', 'MachinePartCategoryController@edit')->name('edit.parts.category');
    Route::put('settings/machines/part-categories/{id}/update', 'MachinePartCategoryController@update')->name('update.parts.category');
    Route::delete('settings/machines/part-categories/{id}/delete', 'MachinePartCategoryController@destroy')->name('delete.parts.category');

    // sla
    Route::get('settings/sla/clients', 'SlaController@index')->name('sla.clients');
    Route::get('settings/sla/clients/{id}', 'SlaController@show')->name('show.sla.client');
    Route::post('settings/sla/clients/{id}/assign', 'SlaController@assign')->name('update.sla.states');

    // profile
    Route::get('/profile', 'ProfileController@index')->name('admin.profile');
    Route::get('/profile/password', 'ProfileController@password')->name('admin.password');
    Route::get('/profile/bio', 'ProfileController@bio')->name('admin.bio');
    Route::post('/profile/password', 'ProfileController@updatePassword')->name('admin.update.password');
    Route::post('/profile/update', 'ProfileController@update')->name('admin.update.profile');
    Route::post('/profile/bio', 'ProfileController@updateBio')->name('admin.update.bio');

    // reports
    Route::get('/reports', 'ReportController@index')->name('all.reports');
    Route::get('/reports/downlist', 'ReportController@downlistClients')->name('downlist.report');
    Route::get('/reports/downlist/clients/{id}', 'ReportController@clientDownlistReport')->name('client.downlist.report');
    Route::get('/reports/productivity', 'ReportController@productivityReport')->name('productivity.report');
    
    // audits
    Route::get('audits', 'AuditController@index')->name('all.audits');
    Route::get('audits/content', 'AuditController@show')->name('show.audit');
    Route::get('audits/{id}', 'AuditController@showDetails')->name('show.audit.details');

    // knowledge base categories
    Route::get('/kb/categories', 'KnowledgeBaseCategoryController@index')->name('admin.kb.categories');
    Route::get('/kb/categories/create', 'KnowledgeBaseCategoryController@create')->name('admin.kb.create.category');
    Route::post('/kb/categories/store', 'KnowledgeBaseCategoryController@store')->name('admin.kb.store.category');
    Route::get('kb/categories/{slug}', 'KnowledgeBaseCategoryController@show')->name('admin.kb.show.category');
    Route::get('kb/categories/{slug}/edit', 'KnowledgeBaseCategoryController@edit')->name('admin.kb.edit.category');
    Route::post('kb/categories/{slug}/update', 'KnowledgeBaseCategoryController@update')->name('admin.kb.update.category');
    Route::delete('kb/categories/{slug}/delete', 'KnowledgeBaseCategoryController@destroy')->name('admin.kb.delete.category');

    // knowledge base
    Route::get('kb', 'KnowledgeBaseController@index')->name('admin.kb');
    Route::get('kb/create', 'KnowledgeBaseController@create')->name('admin.create.kb');
    Route::post('kb/store', 'KnowledgeBaseController@store')->name('admin.store.kb');
    Route::get('kb/{ref}', 'KnowledgeBaseController@show')->name('admin.show.kb');
    Route::get('kb/{ref}/json', 'KnowledgeBaseController@json')->name('admin.json.kb');
    Route::get('kb/{ref}/edit', 'KnowledgeBaseController@edit')->name('admin.edit.kb');
    Route::post('kb/{ref}/update', 'KnowledgeBaseController@update')->name('admin.update.kb');
    Route::post('kb/{ref}/like', 'KnowledgeBaseController@likePost')->name('admin.like.kb');
    Route::post('kb/{ref}/dislike', 'KnowledgeBaseController@dislikePost')->name('admin.dislike.kb');
    Route::delete('kb/{ref}/delete', 'KnowledgeBaseController@deletePost')->name('admin.delete.kb');
    Route::post('kb/{ref}/comments', 'KnowledgeBaseController@makeComment')->name('admin.kb.comment');
    Route::get('kb/{ref}/comments', 'KnowledgeBaseController@getComments')->name('admin.get.kb.comments');
    Route::delete('kb/{ref}/comments/{id}', 'KnowledgeBaseController@deletePostComment')->name('admin.delete.kb.comment');
    Route::put('kb/{ref}/comments/{id}', 'KnowledgeBaseController@updatePostComment')->name('admin.update.kb.comment');

    // messaging
    Route::get('messages/sms', 'MessageController@sms')->name('message.sms');
    Route::post('messages/sms', 'MessageController@sendSMS')->name('send.sms');
    Route::get('messages/email', 'MessageController@email')->name('message.email');
    // pm
    // Route::get('messages/chat/{e?}', 'MessageController@chat')->where('e', '.+')->name('message.chat');
    Route::get('messages/pm', 'PrivateMessageController@index')->name('admin.private.messages');
    Route::get('messages/pm/sent', 'PrivateMessageController@sent')->name('admin.sent.private.messages');
    Route::get('messages/pm/trash', 'PrivateMessageController@trash')->name('admin.trashed.private.messages');
    Route::get('messages/pm/conversations', 'PrivateMessageController@conversations')->name('admin.private.messages.conversations');
    Route::post('messages/pm/send-message', 'PrivateMessageController@sendMessage')->name('admin.send.private.message');
    Route::get('messages/pm/{ref}', 'PrivateMessageController@readMessage')->name('admin.read.private.message');
    Route::get('messages/pm/staff/{staff_id}', 'PrivateMessageController@showStaffPrivateMessageForm')->name('admin.send.staff.private.message.form');
    Route::post('messages/pm/staff/{staff_id}', 'PrivateMessageController@sendMessageToStaff')->name('admin.send.staff.private.message');

    // notifications
    Route::get('notifications', 'NotificationController@index')->name('admin.notifications');
    Route::get('notifications/api', 'NotificationController@api')->name('admin.notifications.api');
    Route::get('notifications/{id}', 'NotificationController@show')->name('admin.show.notification');
    Route::delete('notifications/{id}/delete', 'NotificationController@delete')->name('admin.delete.notification');
});