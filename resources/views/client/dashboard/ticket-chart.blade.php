{{-- year monthly ticket  --}}
@component('components.card')
    @slot('card_body')
        <h4 class="mt-0 header-title mb-4">
            <span class="badge badge-warning ml-2 ticket_year_tag"></span>
            Tickets 

            <div class="btn-group float-right">
                <button class="btn btn-default btn-sm waves-effect waves-light">
                    Year
                </button>
                <button type="button" class="btn btn-default waves-effect waves-light dropdown-toggle dropdown-toggle-split"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="sr-only">Toggle Dropdown</span>
                </button>
                <div class="dropdown-menu ticket-year-dropdown">
                    @foreach ($ticket_years as $year)
                    <a class="dropdown-item">{{ $year }}</a>
                    @endforeach
                </div>
            </div>

        </h4>
        <div id="month_ticket_chart" data-url="{{ route('client.ticket.chart.data') }}">
            <canvas></canvas>
        </div>
    @endslot
@endcomponent