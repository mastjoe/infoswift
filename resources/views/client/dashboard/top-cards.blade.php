{{--  open tickets  --}}
<div class="col-md-3">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-3 text-center">
                    <img src="{{ asset('images/ticket.png') }}">
                </div>
                <div class="col-9 text-center">
                    <h3>{{ number_format($client->open_tickets->count()) }}</h3>
                    <h5 class="text-primary">Open Tickets</h5>
                </div>
            </div>
        </div>
    </div>
</div>
{{--  closed tickets  --}}
<div class="col-md-3">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-3">
                    <img src="{{ asset('images/invoice.png') }}">
                </div>
                <div class="col-9 text-center">
                    <h3>0</h3>
                    <h5 class="text-primary">Close Tickets</h5>
                </div>
            </div>
        </div>
    </div>
</div>
{{--  regions  --}}
<div class="col-md-3">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-3">
                    <img src="{{ asset('images/district.png') }}">
                </div>
                <div class="col-9 text-center">
                    <h3 class="data-counter" data-count="{{ $client->regions->count() }}">0</h3>
                    <h5 class="text-primary">Regions</h5>
                </div>
            </div>
        </div>
    </div>
</div>
{{--  branches  --}}
<div class="col-md-3">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-3">
                    <img src="{{ asset('images/factory.png') }}">
                </div>
                <div class="col-9 text-center">
                    <h3 class="data-counter" data-count="{{ $client->branches->count() }}">0</h3>
                    <h5 class="text-primary">Branches</h5>
                </div>
            </div>
        </div>
    </div>
</div>
{{--  branch custodians  --}}
<div class="col-md-3">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-3">
                    <img src="{{ asset('images/team.png') }}">
                </div>
                <div class="col-9 text-center">
                    <h3 class="data-counter">0</h3>
                    <h5 class="text-primary">Branch Custodians</h5>
                </div>
            </div>
        </div>
    </div>
</div>
{{--  machines  --}}
<div class="col-md-3">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-3">
                    <img src="{{ asset('images/atm-1.png') }}">
                </div>
                <div class="col-9 text-center">
                    <h3 class="data-counter" data-count="{{ $client->machines->count() }}">0</h3>
                    <h5 class="text-primary">Machines</h5>
                </div>
            </div>
        </div>
    </div>
</div>