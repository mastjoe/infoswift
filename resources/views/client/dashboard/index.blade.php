@extends('layouts.client.app')

@section('title', 'Dashboard')

@push('css')
    
@endpush

@section('content')
    <div class="row">
        @include('client.dashboard.top-cards')
    </div>
    <div class="row">
        <div class="col-12">
            @include('client.dashboard.ticket-chart')
        </div>
    </div>
    <div class="row">
        @include('client.dashboard.pm-chart')
    </div>
@endsection

@push('js')
    <script src="{{asset('plugins/raphael.min.js')}}"></script>
    <script src="{{ asset('js/client/dashboard.js') }}"></script>
    <script>
        $(document).ready(function() {
            dataCounter()
        })
    </script>
@endpush