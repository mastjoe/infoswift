{{-- polar area chart for PM --}}
<div class="col-md-6 col-12 d-flex">
    @component('components.card', ['card_class' => 'flex-fill'])
        @slot('card_body')
            <h4 class="header-title mb-4 mt-0">
                PM Reports
                <div class="btn-group float-right">
                    <button class="btn btn-default btn-sm waves-effect waves-light">
                        Year
                    </button>
                    <button type="button" class="btn btn-default waves-effect waves-light dropdown-toggle dropdown-toggle-split"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <div class="dropdown-menu pm-year-dropdown">
                        @foreach ($pm_years as $year)
                        <a class="dropdown-item">{{ $year }}</a>
                        @endforeach
                    </div>
                </div>
            </h4>
            <div id="pm_chart_target" data-url="{{ route('client.pm.chart.data') }}">
                <canvas></canvas>
            </div>
        @endslot
    @endcomponent
</div>

{{-- machine status --}}
<div class="col-md-6 col-12 d-flex">
    @component('components.card', ['card_class'=>'flex-fill'])
        @slot('card_body')
            <h4 class="header-title mb-4 mt-0">
                Machine Status
            </h4>
            <div id="machine_status_chart_target" data-url="{{ route('client.machines.status.chart.data') }}">
            </div>
        @endslot
    @endcomponent
</div>