 @component('components.card')
                @slot('card_body')
                    <h5 class="card-title">
                        {{ $machine->terminal_id }}
                    </h5>
                    <hr>
                    {{-- name --}}
                    <strong class="text-primary">
                        Name
                    </strong>
                    <p class="text-muted">
                        @isset($machine->name)
                            {{ $machine->name }}
                        @else
                            <small>Not Available</small>
                        @endisset
                    </p>
                    <hr>
                    <p class="text-muted">
                        @isset($machine->serial_number)
                            {{ $machine->serial_number }}
                        @else
                            <small>Not Available</small>
                        @endisset
                    </p>
                    <hr>
                    {{-- model number --}}
                    <strong class="text-primary">
                        Model Number
                    </strong>
                    <p class="text-muted">
                        @isset($machine->model_number)
                        @else
                            <small>Not Available</small>
                        @endisset
                    </p>
                    <hr>
                    {{-- serial number --}}
                    <strong class="text-primary">
                        Ip Address
                    </strong>
                    <p class="text-muted">
                        @isset($machine->ip_address)
                        @else
                            <small>Not Available</small>
                        @endisset
                    </p>
                    <hr>
                    {{-- type --}}
                    <strong class="text-primary">
                        Type
                    </strong>
                    <p class="text-muted">
                        @if($machine->machineType)
                            {{ $machine->machineType->type }}
                        @else
                            <small>Not Available</small>
                        @endif
                    </p>
                    <hr>
                    {{-- status --}}
                    <strong class="text-primary">
                        Status
                    </strong>
                    <p class="text-muted">
                        @if($machine->machineStatus)
                            {{ $machine->machineStatus->status }}
                        @else
                            <small>Not Available</small>
                        @endif
                    </p>
                    <hr>
                    {{-- vendor --}}
                    <strong class="text-primary">
                        Vendor
                    </strong>
                    <p class="text-muted">
                        @if($machine->machineVendor)
                            {{ $machine->machineVendor->vendor }}
                        @else
                            <small>Not Available</small>
                        @endif
                    </p>
                    <hr>
                @endslot
            @endcomponent