@extends('layouts.client.app')

@section('title', 'All Machines')

@section('page_title', 'All Machines')

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('client.machines') }}">Machines</a>
    </li>
    <li class="breadcrumb-item active">{{ $machine->terminal_id }}</li>
@endsection

@push('css')

@endpush

@section('content')
    <div class="row">
      <div class="col-12">
          @component('components.card')
              @slot('card_header')
                    <h5 class="card-title">
                        Ticket History
                    </h5>
              @endslot
              @slot('card_body')
                    <div class="">
                        <table class="table table-bordered table-hover data_table">
                            <thead>
                                <tr>
                                    <th>Reference</th>
                                    <th>Faults</th>
                                    <th>Status</th>
                                    <th>Raised</th>
                                    <th>Closed</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($machine->tickets->sortByDesc('id') as $ticket)
                                    <tr>
                                        <td>
                                            <a href="{{ route('client.show.ticket', $ticket->ref) }}">{{ $ticket->ref }}</a>
                                        </td>
                                        <td>
                                            <span class="badge badge-primary">
                                                {{ $ticket->faults->count() }}
                                            </span>
                                        </td>
                                        <td>
                                            @if ($ticket->status == "open")
                                                <span class="badge badge-primary">
                                                    open
                                                </span>
                                            @elseif ($ticket->status == "confirm closed")
                                                <span class="badge badge-success">
                                                    confirmed closed
                                                </span>
                                            @else
                                                <span class="badge badge-warning">
                                                    {{ $ticket->status }}
                                                </span>
                                            @endif
                                        </td>
                                        <td>
                                            {{ $ticket->created_at->format('jS M, Y') }}
                                        </td>
                                        <td>
                                            @if ($ticket->confirmed_closed_at)
                                                {{ $ticket->confirmed_closed_at->format('jS M, Y') }}
                                            @else
                                                -
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
              @endslot
          @endcomponent
      </div>
    </div>
@endsection

@push('js')
    <script>
        $(document).ready(function() {
            simpleDataTable('.data_table');
        });
    </script>
@endpush