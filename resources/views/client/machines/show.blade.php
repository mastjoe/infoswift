@extends('layouts.client.app')

@section('title', 'All Machines')

@section('page_title', 'All Machines')

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('client.machines') }}">Machines</a>
    </li>
    <li class="breadcrumb-item active">{{ $machine->terminal_id }}</li>
@endsection

@push('css')

@endpush

@section('content')
    <div class="row">
        <div class="col-lg-4">
           @include('client.machines.info')
        </div>
        <div class="col-lg-8">
            {{-- ticket --}}
            @component('components.card')
                @slot('card_header')
                    <h5 class="card-title">
                        Ticket History
                    </h5>
                @endslot
                @slot('card_body')
                    @if ($machine->tickets->count())
                        <div>
                            <table class="table table-bordered data_table">
                                <thead>
                                    <tr>
                                        <th>Reference</th>
                                        <th>Faults</th>
                                        <th>Status</th>
                                        <th>Closed</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $latest_tickets = $machine->tickets->sortByDesc('id')->take(5);
                                    @endphp
                                    @foreach ($latest_tickets as $ticket)
                                        <tr>
                                            <td>{{ $ticket->ref }}</td>
                                            <td>
                                                <span class="badge badge-primary">
                                                    {{ $ticket->faults->count() }}
                                                </span>
                                            </td>
                                            <td>
                                                @if ($ticket->status == "open")
                                                    <span class="badge badge-primary">
                                                        open
                                                    </span>
                                                @elseif ($ticket->status == "confirm closed")
                                                    <span class="badge badge-success">
                                                        confirmed closed
                                                    </span>
                                                @else
                                                    <span class="badge badge-warning">
                                                        {{ $ticket->status }}
                                                    </span>
                                                @endif
                                            </td>
                                            <td>
                                                @if ($ticket->status == "confirm closed")
                                                    {{ $ticket->confirmed_closed_at->diffForHumans() }}
                                                @else
                                                    -
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <hr>
                        <div class="text-center">
                            <a href="{{ route('client.show.machine.tickets', $machine->id) }}">See More</a>
                        </div>
                    @else
                        <div class="text-center py-3">
                            <h3 class="text-muted">
                                No ticket found for machine
                            </h3>
                            <a class="btn btn-primary waves-effect waves-light"
                                href="{{ route('client.create.ticket', ['machine'=> $machine->terminal_id]) }}"
                            >
                                Raise a Ticket
                            </a>
                        </div>
                    @endif
                @endslot
            @endcomponent

            {{-- Pm history --}}
            @component('components.card')
                @slot('card_header')
                    <h5 class="card-title">
                        PM History
                    </h5>
                @endslot
                @slot('card_body')
                    @if ($machine->pms->count())
                        <div>

                        </div>
                        <hr>
                        <div class="text-center">
                            <a href="">See More</a>
                        </div>
                    @else
                        <div class="text-center py-3">
                            <h3 class="text-muted">
                                No PM record found for machine
                            </h3>
                        </div>
                    @endif
                @endslot
            @endcomponent
        </div>
    </div>
@endsection

@push('js')
    <script>
        $(document).ready(function() {
            // simpleDataTable('.data_table');
        });
    </script>
@endpush