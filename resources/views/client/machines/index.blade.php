@extends('layouts.client.app')

@section('title', 'All Machines')

@section('page_title', 'All Machines')

@section('crumbs')
    <li class="breadcrumb-item active">Machines</li>
@endsection

@push('css')
@endpush

@section('content')
    @if ($machines->count())
        @component('components.card')
            @slot('card_header')
                <h4 class="card-title">
                    All Machines
                </h4>
                <div class="card-options">
                </div>
            @endslot
            @slot('card_body')
                <div>
                    <table class="table table-bordered machines_table">
                        <thead>
                            <tr>
                                <th>Terminal ID</th>
                                <th>Branch</th>
                                <th>Region</th>
                                <th>Tickets</th>
                                <th>PMs</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($machines as $machine)
                                <tr>
                                    <td>
                                        <a href="{{ route('client.show.machine', $machine->id) }}">
                                            {{ $machine->terminal_id }}
                                        </a>
                                    </td>
                                    <td>
                                        {{ $machine->branch->branch }}
                                    </td>
                                    <td>
                                        {{ $machine->region->region }}
                                    </td>
                                    <td>
                                        <span class="badge badge-primary">
                                            {{ number_format($machine->tickets->count()) }}
                                        </span>
                                    </td>
                                    <td>
                                        <span class="badge badge-secondary">
                                            {{ number_format($machine->pms->count()) }}
                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @endslot
        @endcomponent
    @else
        @component('components.card')
            @slot('card_body')
                <div class="text-center py-4 text-primary">
                    <h3>No Machine found!</h3>
                </div>
            @endslot
        @endcomponent
    @endif
@endsection

@push('js')
    <script>
        $(document).ready(function() {
            simpleDataTable('.machines_table');
        });
    </script>
@endpush