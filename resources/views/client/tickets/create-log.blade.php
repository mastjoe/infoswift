{{-- create log form --}}
<div class="modal fade" role="document" id="add_log_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form method="POST" action="{{ route('client.create.ticket.log', $ticket->ref) }}">
                @csrf
                <div class="modal-header">
                    <h3 class="modal-title">
                        <i class="mr-2 text-primary mdi mdi-comment-account"></i>
                        New Log
                    </h3>
                    <button class="close" type="button" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <textarea class="form-control summernote" name="message"></textarea>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" type="button" data-dismiss="modal">
                        Exit
                    </button>
                    <button class="btn btn-primary" type="submit">
                        Submit
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>