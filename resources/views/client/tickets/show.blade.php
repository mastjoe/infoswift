@extends('layouts.client.app')

@section('title', 'Tickets | '.$ticket->ref)

@section('page_title', 'Tickets | '.$ticket->ref)

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('client.tickets') }}">Tickets</a>
    </li>
    <li class="breadcrumb-item active">
        {{ $ticket->ref }}
    </li>
@endsection

@push('css')

@endpush

@section('content')
   <div class="row">
      <div class="col-lg-3">
            {{-- ticket info --}}
            @component('components.card')
                @slot('card_body')
                    <h5 class="card-title">
                        Ticket {{ $ticket->ref }}
                    </h5>
                    <hr>
                    <strong class="text-primary">Reference</strong>
                    <p class="text-muted">
                        {{ $ticket->ref }}
                    </p>
                    <hr>
                    <strong class="text-primary">Machine</strong>
                    <p class="text-muted">
                        {{ $ticket->machine->terminal_id }}
                    </p>
                    <hr>
                    <strong class="text-primary">Created</strong>
                    <p class="text-muted">
                        {{ $ticket->created_at->format('jS M, Y | h:i a') }}
                    </p>
                    <hr>
                    <strong class="text-primary">Status</strong>
                    <p class="text-muted">
                        @if ($ticket->status == "closed")
                            <div type="button" class="bg-warning w-100 p-2 text-center rounded">
                                CLOSED
                            </div>
                        @elseif($ticket->status == "open")
                            <div type="button" class="bg-primary w-100 p-2 text-center text-white rounded">
                                OPEN
                            </div>
                        @elseif($ticket->status == "resolved")
                            <div class="bg-success w-100 p-2 text-center text-white rounded" type="button">
                                RESOLVED
                            </div>
                        @else
                            <div type="button" class="bg-warning w-100 p-2 text-center rounded">
                                {{ strtoupper($ticket->status) }}
                            </div>
                        @endif
                    </p>
                    <hr>
                    <div>
                        <a class="btn btn-dark btn-block my-2" href="{{ route('client.ticket.logs', $ticket->ref) }}">
                            Logs
                        </a>
                        @if ($ticket->editable())
                            <a class="btn btn-warning"
                                title="Edit"
                                data-toggle="tooltip"
                                href="{{ route('client.edit.ticket', $ticket->ref) }}"
                            >
                                <i class="mdi mdi-circle-edit-outline"></i>
                            </a>
                        @endif
                        
                        @if ($ticket->deletable())
                            <button class="btn btn-primary" 
                                title="Delete"
                                data-toggle="tooltip"
                                data-name="{{ $ticket->ref }}"
                                data-url="{{ route('client.delete.ticket', $ticket->ref) }}" 
                                onclick="deleteTicket(this)"
                            >
                                <i class="mdi mdi-delete"></i>
                            </button>
                        @endif
                    </div>
                @endslot
            @endcomponent

            {{-- ticket faults --}}
            @component('components.card')
                @slot('card_header')
                    <h5 class="card-title">
                        Ticket Faults
                    </h5>
                @endslot
                @slot('card_body')
                    <ul class="list-group list-group-flush">
                        @foreach ($ticket->faults as $fault)
                            <li class="list-group-item">
                                <span class="mdi mdi-alarm-light mr-1 text-primary"></span>
                                {{ $fault->fault->fault }}
                            </li>
                        @endforeach
                    </ul>
                @endslot
            @endcomponent
      </div>
      <div class="col-lg-9">
          {{-- machine info --}}
          @component('components.card')
              @slot('card_header')
                  <h5 class="card-title">Machine Info</h5>
              @endslot
              @slot('card_body')
                  <div class="row">
                      <div class="col-lg-4">
                          <div>
                              <h6 class="text-primary">
                                  Terminal ID
                              </h6>
                              <h5 class="text-muted">
                                  {{ $ticket->machine->terminal_id }}
                              </h5>
                          </div>
                      </div>
                      <div class="col-lg-4">
                          <div>
                              <h6 class="text-primary">
                                  Serial Number
                              </h6>
                              <h5 class="text-muted">
                                    @isset($ticket->machine->serial_number)
                                        {{ $ticket->machine->serial_number }}
                                    @else
                                        <small>Not Available</small>
                                    @endisset
                              </h5>
                          </div>
                      </div>
                      <div class="col-lg-4">
                          <div>
                              <h6 class="text-primary">
                                  Status
                              </h6>
                              <h5 class="text-muted">
                                    {{ $ticket->machine->machineStatus->status }}
                              </h5>
                          </div>
                      </div>
                      <hr>
                      <div class="text-center mt-2 col-12">
                          <a href="">See More</a>
                      </div>
                  </div>
              @endslot
          @endcomponent

          {{-- branch info --}}
          @component('components.card')
              @slot('card_header')
                  <h5 class="card-title">
                      Branch Info
                  </h5>
              @endslot
              @slot('card_body')
                  <div class="row">
                      <div class="col-lg-4">
                          <div>
                              <h6 class="text-primary">
                                  Branch
                              </h6>
                              <h5>
                                  <a href="">{{ $ticket->machine->branch->branch }}</a>
                              </h5>
                          </div>
                      </div>
                      <div class="col-lg-4">
                          <div>
                              <h6 class="text-primary">
                                  Region
                              </h6>
                              <h5>
                                  <a href="">{{ $ticket->machine->region->region }}</a>
                              </h5>
                          </div>
                      </div>
                      <div class="col-lg-4">
                          <div>
                              <h6 class="text-primary">
                                  State
                              </h6>
                              <h5>
                                  <a href="">
                                      {{ $ticket->machine->state->state }}
                                      <span>({{ $ticket->machine->country->country }})</span>
                                  </a>
                              </h5>
                          </div>
                      </div>

                  </div>
              @endslot
          @endcomponent
      </div>
   </div>
@endsection

@push('js')
   <script src="{{ asset('js/client/ticket.js') }}"></script>
    <script>
        $(document).ready(function() {
            @if (session('created'))
                Swal.fire(
                    'Ticket Created',
                    '{{ session('created') }}',
                    'success'
                );
            @endif
            
            @if (session('updated'))
                Swal.fire(
                    'Ticket Updated',
                    '{{ session('updated') }}',
                    'success'
                );
            @endif

            @if (session('cannot_edit'))
                Swal.fire(
                    'Update Prohibited!',
                    '{{ session('cannot_edit') }}',
                    'warning'
                );
            @endif
        });
    </script>
@endpush