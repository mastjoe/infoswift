@extends('layouts.client.app')

@section('title', 'Tickets | '.$ticket->ref)

@section('page_title', 'Tickets | '.$ticket->ref)

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('client.tickets') }}">Tickets</a>
    </li>
    <li class="breadcrumb-item active">
        {{ $ticket->ref }}
    </li>
@endsection

@push('css')

@endpush

@section('content')
   <div class="row">
      <div class="col-12 mt-5">
            @if ($ticket->logs->count())
                @foreach ($ticket->logs as $log)
                    @include('partials.ticket-log')
                @endforeach
                <div class="text-center my-2">
                    <button class="btn btn-primary waves-effect add_log_btn">
                        Add to Log
                    </button>
                </div>
            @else
                <div class="text-center my-5" style="margin-top:150px;">
                    <h2 class="text-muted">No log found for ticket!</h2>
                    <button class="btn btn-primary waves-effect my-2 add_log_btn">
                        Make a log on ticket
                    </button>
                </div>
            @endif
      </div>
   </div>

   {{-- modal --}}
   @include('client.tickets.create-log')
@endsection

@push('js')
   <script src="{{ asset('js/client/ticket.js') }}"></script>
    <script>
        $(document).ready(function() {
            @if (session('cannot_edit'))
                Swal.fire(
                    'Update Prohibited!',
                    '{{ session('cannot_edit') }}',
                    'warning'
                );
            @endif
        });
    </script>
@endpush