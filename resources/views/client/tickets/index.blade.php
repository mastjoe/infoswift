@php
    use App\Helpers\UI;
@endphp
@extends('layouts.client.app')

@section('title', 'All Tickets')

@section('page_title', 'All Tickets')

@section('crumbs')
    <li class="breadcrumb-item active">Tickets</li>
@endsection

@push('css')

@endpush

@section('content')
    @if ($tickets->count())
        @component('components.card')
            @slot('card_header')
                <h4 class="card-title">
                    All Tickets
                </h4>
                <div class="card-options">
                    <a href="{{ route('client.create.ticket') }}" class="btn-block-option add_custodian_btn">
                        <i class="mdi mdi-new-box mdi-24px"></i>
                    </a>
                </div>
            @endslot
            @slot('card_body')
                <div>
                    <table class="table table-bordered tickets_table">
                        <thead>
                            <tr>
                                <th>Reference</th>
                                <th>Terminal Id</th>
                                <th>Branch</th>
                                <th>Faults</th>
                                <th>Status</th>
                                <th>Created</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($tickets as $ticket)
                                <tr>
                                    <td>
                                        <a href="{{ route('client.show.ticket', $ticket->ref) }}">{{ $ticket->ref }}</a>
                                    </td>
                                    <td>{{ $ticket->machine->terminal_id }}</td>
                                    <td>{{ $ticket->machine->branch->branch }}</td>
                                    <td>
                                        <span class="badge badge-primary">
                                            {{ number_format($ticket->faults->count()) }}
                                        </span>
                                    </td>
                                    <td>
                                        {!! UI::ticketStatusBadge($ticket->status) !!}
                                    </td>
                                    <td>
                                        {{ $ticket->created_at->diffForHumans() }}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @endslot
        @endcomponent
    @else
        @component('components.card')
            @slot('card_body')
                <div class="text-center py-4 text-primary">
                    <span>
                        
                    </span>
                    <h3>No Ticket found!</h3>
                    <a href="{{ route('client.create.ticket') }}" class="btn waves-effect btn-primary my-2">
                        Create New Ticket
                    </a>
                </div>
            @endslot
        @endcomponent
    @endif
@endsection

@push('js')
    <script>
        $(document).ready(function() {
            @if (session('created'))
                Swal.fire(
                    'Ticket Created',
                    '{{ session('created') }}',
                    'success'
                );
            @endif

            simpleDataTable('.tickets_table');

            @if (session('ticket_deleted'))
                Swal.fire(
                    'Ticket Delete',
                    '{{ session('ticket_deleted') }}',
                    'success'
                );
            @endif
        });
    </script>
@endpush