@extends('layouts.client.app')

@section('title', 'Update Ticket | '.$ticket->ref)

@section('page_title', 'Tickets | '.$ticket->ref.' | Edit')

@section('crumbs')
    <li class="breadcrumb-item ">
        <a href="{{ route('client.tickets') }}">Tickets</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('client.show.ticket', $ticket->ref) }}">{{ $ticket->ref }}</a>
    </li>
    <li class="breadcrumb-item active">Edit</li>
@endsection

@push('css')

@endpush

@section('content')
    <div class="">
        <div class="row">
                <div class="offset-md-1 col-md-10">
                    @if ($errors->count())
                        <div class="alert alert-danger">
                            <button class="close" type="button"><span>&times;</span></button>
                            {{ $errors->first() }}
                        </div>
                    @endif
                    <form method="POST" action="{{ route('client.update.ticket', $ticket->ref) }}">
                        @csrf
                        @method('put')
                        @component('components.card')
                            @slot('card_header')
                                <h4 class="card-title">
                                    Update Ticket - <span class="text-muted">{{ $ticket->ref }}</span>
                                </h4>
                            @endslot
                            @slot('card_body')
                                {{-- machines --}}
                                @component('components.form.select-form-group', [
                                    'name'             => 'machine',
                                    'label'            => 'machine',
                                    'id'               => 'machine',
                                    'label_class'      => 'col-md-3 col-form-label text-md-right',
                                    'input_wrap_class' => 'col-md-9',
                                    'form_group_class' => 'row',
                                    'required' => false,
                                    'props' => ['onchange' => "selectTicketMachine(this)"]
                                ])
                                    @slot('options')
                                        <option value="">Choose Machine</option>
                                        @foreach ($machines as $machine)
                                            <option value="{{ $machine->id }}"
                                                data-region="{{ $machine->region->region }}"
                                                data-branch="{{ $machine->branch->branch }}"
                                                {{ $machine->id == $ticket->machine_id ? "selected" : null }}
                                            >
                                                {{ $machine->terminal_id }}
                                            </option>
                                        @endforeach
                                    @endslot
                                @endcomponent
    
                                {{-- branch --}}
                                @component('components.form.input-form-group', [
                                    'name'             => 'branch',
                                    'label'            => 'Branch',
                                    'id'               => 'branch',
                                    'label_class'      => 'col-md-3 col-form-label text-md-right',
                                    'input_wrap_class' => 'col-md-9',
                                    'form_group_class' => 'row machine_depend d-none',
                                    'read_only' => true
                                ])
                                @endcomponent
    
                                {{-- region --}}
                                 @component('components.form.input-form-group', [
                                    'name'             => 'region',
                                    'label'            => 'Region',
                                    'id'               => 'region',
                                    'label_class'      => 'col-md-3 col-form-label text-md-right',
                                    'input_wrap_class' => 'col-md-9',
                                    'form_group_class' => 'row machine_depend d-none',
                                    'read_only' => true
                                ])
                                @endcomponent
    
                                {{-- faults --}}
                                @php
                                    $fault_ids = $ticket->faults->pluck('id')->toArray();
                                @endphp
                                @component('components.form.select-form-group', [
                                    'name'             => 'faults[]',
                                    'label'            => 'Faults',
                                    'id'               => 'faults',
                                    'label_class'      => 'col-md-3 col-form-label text-md-right',
                                    'input_wrap_class' => 'col-md-9',
                                    'form_group_class' => 'row',
                                    'required' => true,
                                    'props' => ['multiple' => true]
                                ])
                                    @slot('options')
                                        <option value="">Choose Machine Faults</option>
                                        @foreach ($machine_faults as $fault)
                                            <option value="{{ $fault->id }}"
                                                {{ in_array($fault->id, $fault_ids) ? "selected" : null }}    
                                            >
                                                {{ $fault->fault }}
                                            </option>
                                        @endforeach
                                    @endslot
                                @endcomponent
                                <div class="form-group row">
                                    <div class="offset-md-3 col-md-9">
                                        <h6>
                                            Enter new fault if not existing
                                            <button class="btn btn-primary btn-sm"
                                                type="button"
                                                onclick="addFaultField()"
                                            >
                                                New Fault
                                            </button>
                                        </h6>
                                    </div>
                                </div>
                                <div class="append-point"></div>
                                <hr>
                                <div class="text-center py-2">
                                    <button class="btn btn-primary px-4">
                                        Save Ticket
                                    </button>
                                </div>
                            @endslot
                        @endcomponent
                    </form>
                </div>
            </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/client/ticket.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#faults').select2({
                multiple: true,
                placeholder: 'Choose Machine Faults'
            });
            selectTicketMachine('#machine');

            formLabelFocus();
        });
    </script>
@endpush