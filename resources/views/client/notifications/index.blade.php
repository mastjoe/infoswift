@php
    use Carbon\Carbon;
@endphp
@extends('layouts.client.app')

@section('page_title', 'Notifications')

@section('title', 'Notifications')

@section('crumbs')
    <li class="breadcrumb-item active">Notifications</li>
@endsection

@push('css')
    <style>
        .read-notify {
            background: #efefef;
        }
    </style>
@endpush

@section('content')
   <div class="row">
      <div class="offset-md-1 col-md-10 col-sm-12">
          @if ($grouped_notifications->count())
                <div class="py-1 card-body">
                    @foreach ($grouped_notifications as $date =>  $notifications)
                    <div class="col-12">
                        <span class="bg-primary p-2 rounded d-inline-block text-white mb-2 mt-3">
                            @if (Carbon::parse($date)->isToday())
                                Today
                            @else                            
                                {{ Carbon::parse($date)->format('jS F, Y') }}
                            @endif
                        </span>
                        @foreach ($notifications as $notification)
                            <div class="card my-0 mb-1 py-0 {{ $notification->read_at ? "read-notify" : null}}">
                                <div class="card-body notify-item" style="cursor: pointer" data-url="{{ route('client.show.notification', $notification->id) }}">
                                    {{ Str::limit($notification->data['message'],90, '...') }}
                                    <span class="text-primary float-right delete_notify_item_btn" data-content="{{ $notification->data['message'] }}"
                                        data-url="{{ route('client.delete.notification', $notification->id) }}">
                                        <i class="mdi mdi-delete"></i>
                                    </span>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    @endforeach
                </div>
          @else
              <div class="text-center my-5">
                  <h2 class="text-muted">No notification found!</h2>
              </div>
          @endif
      </div>
   </div>
@endsection

@push('js')
    <script src="{{asset('plugins/raphael.min.js')}}"></script>
    <script src="{{ asset('js/client/dashboard.js') }}"></script>
    <script>
        $(document).ready(function() {

            const notifyItem = $('.notify-item');
            const deleteNotifyBtn = $('.delete_notify_item_btn');

            notifyItem.on('click', function() {
                const url = $(this).data('url');
                showNotificationDetails(url);
            });

            const showNotificationDetails = function(url) {
                //get id from url
                const id = url.split("/")[url.split("/").length - 1];
                pushAuxModalLoad({
                    url: url,
                    callback: function() {
                        markNotifyItemAsRead(id);
                    }
                });
            }

            const markNotifyItemAsRead = function(id) {
                notifyItem.each(function(index, el) {
                    const element = $(el);
                    const elementId = element.data('id');
                    if (elementId == id) {
                        element.addClass('read-notify');
                    }
                });
            }

            @if (session('selected_id'))
                showNotificationDetails('{{ session('selected_id') }}');
            @endif

            deleteNotifyBtn.on('click', function(e) {
                e.stopPropagation();
                const url = $(this).data('url');
                const content = $(this).data('content');
                const card = $(this).parents('.card');

                pushRequest({
                    url: url,
                    dialogText: 'You are about to delete this notification, "'+content+'"?',
                    method: 'delete',
                    successTitle: 'Notification Deleted!',
                    callback: function() {
                        card.remove();
                        Swal.fire(
                            'Notification Deleted',
                            'notification was successfully deleted!',
                            'success'
                        );
                    }
                });
            });
        })
    </script>
@endpush