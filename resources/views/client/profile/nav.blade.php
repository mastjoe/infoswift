<div class="card directory-card m-b-20">
    <div class="card-body">
        <div class="clearfix">
            <div class="directory-img text-center">
                <div class="avatar">
                    <img class="rounded-circle thumb-lg img-thumbnail" src="{{ $client->logo() }}"
                        alt="{{ $client->name }}">
                </div>
            </div>
            <h5 class="text-center mt-2">{{ $client->name }}</h5>
            <p class="text-muted text-center">{{ $client->address }}</p>
        </div>
    </div>
</div>

<div>
    <a href="{{ route('client.profile') }}" class="btn btn-secondary btn-block my-2">Profile Details</a>
    <a href="{{ route('client.profile.regions') }}" class="btn btn-secondary btn-block my-2">Regions</a>
    <a href="{{ route('client.profile.branches') }}" class="btn btn-secondary btn-block my-2">Branches</a>
    <a href="{{ route('client.profile.machines') }}" class="btn btn-secondary btn-block my-2">Terminals</a>
</div>