@extends('layouts.client.app')

@section('page_title', 'Profile')

@section('title', 'Profile')

@section('crumbs')
    <li class="breadcrumb-item active">profile</li>
@endsection

@push('css')
    
@endpush

@section('content')
   <div class="row">
       <div class="col-md-3">
           @include('client.profile.nav')
       </div>

       <div class="col-md-9">
           {{--  client info  --}}
           @component('components.card')
               @slot('card_body')
                   <h4 class="text-muted">Profile Details</h4>
                   <hr>
                   <table class="table table-sm table-borderless">
                       <thead>
                           <th width="150"></th>
                           <th></th>
                       </thead>
                       <tbody>
                           <tr>
                               <td><b class="text-primary">Code</b></td>
                               <td>{{ $client->code }}</td>
                           </tr>
                           <tr>
                               <td><b class="text-primary">Email Address</b></td>
                               <td>{{ $client->email }}</td>
                           </tr>
                           <tr>
                               <td><b class="text-primary">Website</b></td>
                               <td><a href="{{ $client->website }}" target="_blank">{{ $client->website }}</a></td>
                           </tr>
                           <tr>
                               <td><b class="text-primary">Address</b></td>
                               <td>{{ $client->address }}</td>
                           </tr>
                       </tbody>
                   </table>
               @endslot
           @endcomponent

           {{--  info boxes  --}}
       </div>
   </div>
@endsection

@push('js')
    <script src="{{asset('plugins/raphael.min.js')}}"></script>
    <script src="{{ asset('js/client/dashboard.js') }}"></script>
    <script>
        $(document).ready(function() {
            dataCounter()
        })
    </script>
@endpush