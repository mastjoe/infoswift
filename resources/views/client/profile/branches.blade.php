@extends('layouts.client.app')

@section('page_title', 'Profile | Branches')

@section('title', 'Profile | Branches')

@section('crumbs')
    <li class="breadcrumb-item active">branches</li>
@endsection

@push('css')
    
@endpush

@section('content')
   <div class="row">
       <div class="col-md-3">
           @include('client.profile.nav')
       </div>

       <div class="col-md-9">
           {{--  client info  --}}
           @component('components.card')
               @slot('card_body')
                   <h4 class="text-muted">Branches</h4>
                   <hr>
                   <table class="table table-bordered table-hover dt">
                       <thead class="thead-light">
                            <tr>
                                <th>Branch</th>
                                <th>Region</th>
                                <th>Custodian</th>
                                <th>Machines</th>
                            </tr>
                       </thead>
                       <tbody>
                            @foreach ($client->branches as $branch)
                                <tr>
                                    <td>{{ $branch->branch }}</td>
                                    <td>{{ $branch->region->region }}</td>
                                    <td>
                                        <div class="btn-group-sm">
                                            <button class="btn btn-primary btn-sm px-3"
                                                data-toggle="dropdown"
                                                title="Custodians"
                                            >
                                                <i class="fa fa-address-book"></i>
                                            </button>
                                            <div class="dropdown-menu">
                                                @if ($branch->custodians->count())
                                                    @foreach ($branch->custodians as $key => $custodian)
                                                        <a class="dropdown-item">
                                                            {{ $key+1 }}.
                                                            {{ $custodian->name }}
                                                        </a>
                                                    @endforeach
                                                @else
                                                    <a class="dropdown-item">
                                                        <span class="text-muted">No custodian found</span>
                                                    </a>
                                                @endif
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        {{ number_format($branch->machines->count()) }}
                                    </td>
                                </tr>
                            @endforeach
                       </tbody>
                   </table>
               @endslot
           @endcomponent

           {{--  info boxes  --}}
       </div>
   </div>
@endsection

@push('js')
    <script src="{{asset('plugins/raphael.min.js')}}"></script>
    <script src="{{ asset('js/client/dashboard.js') }}"></script>
    <script>
        $(document).ready(function() {
            simpleDataTable('.dt')
        })
    </script>
@endpush