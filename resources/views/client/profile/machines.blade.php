@extends('layouts.client.app')

@section('page_title', 'Profile | Terminals')

@section('title', 'Profile | Terminals')

@section('crumbs')
    <li class="breadcrumb-item active">terminals</li>
@endsection

@push('css')
@endpush

@section('content')
   <div class="row">
       <div class="col-md-3">
           @include('client.profile.nav')
       </div>

       <div class="col-md-9">
           {{--  client info  --}}
           @component('components.card')
               @slot('card_body')
                   <h4 class="text-muted">Terminals</h4>
                   <hr>
                   <table class="table table-bordered table-hover dt">
                       <thead class="thead-light">
                            <tr>
                                <th>Terminals</th>
                                <th>Branch</th>
                                <th>Region</th>
                            </tr>
                       </thead>
                       <tbody>
                            @foreach ($client->machines as $machine)
                                <tr>
                                    <td><a href="{{ route('client.show.machine', $machine->id) }}">{{ $machine->terminal_id }}</a></td>
                                    <td>{{ $machine->branch->branch }}</td>
                                    <td>{{ $machine->region->region }}</td>
                                </tr>
                            @endforeach
                       </tbody>
                   </table>
               @endslot
           @endcomponent

           {{--  info boxes  --}}
       </div>
   </div>
@endsection

@push('js')
    <script src="{{asset('plugins/raphael.min.js')}}"></script>
    <script src="{{ asset('js/client/dashboard.js') }}"></script>
    <script>
        $(document).ready(function() {
            simpleDataTable('.dt')
        })
    </script>
@endpush