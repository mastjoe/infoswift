@extends('layouts.client.app')

@section('page_title', 'Profile | Regions')

@section('title', 'Profile | Regions')

@section('crumbs')
    <li class="breadcrumb-item active">profile</li>
@endsection

@push('css')
    
@endpush

@section('content')
   <div class="row">
       <div class="col-md-3">
           @include('client.profile.nav')
       </div>

       <div class="col-md-9">
           {{--  client info  --}}
           @component('components.card')
               @slot('card_body')
                   <h4 class="text-muted">Regions</h4>
                   <hr>
                   <table class="table table-bordered table-hover dt">
                       <thead class="thead-light">
                            <tr>
                                <th>Region</th>
                                <th>State</th>
                                <th>Country</th>
                                <th>Machines</th>
                            </tr>
                       </thead>
                       <tbody>
                            @foreach ($client->regions as $region)
                                <tr>
                                    <td>{{ $region->region }}</td>
                                    <td>{{ $region->state->state }}</td>
                                    <td>{{ $region->country->country }}</td>
                                    <td>
                                        {{ $region->machines->count() }}
                                    </td>
                                </tr>
                            @endforeach
                       </tbody>
                   </table>
               @endslot
           @endcomponent

           {{--  info boxes  --}}
       </div>
   </div>
@endsection

@push('js')
    <script src="{{asset('plugins/raphael.min.js')}}"></script>
    <script src="{{ asset('js/client/dashboard.js') }}"></script>
    <script>
        $(document).ready(function() {
            simpleDataTable('.dt')
        })
    </script>
@endpush