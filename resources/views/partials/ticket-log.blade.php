@component('components.card')
    @slot('card_body')
        <a href="javascript:;" class="media">
            <img class="d-flex mr-3 rounded-circle" src="{{ $log->author_avatar() }}" 
                @if ($log->user_id)
                    alt="{{ $log->user->full_name() }}"
                @else
                    alt="{{ $log->client->name }}"
                @endif
                height="36"
            >
            <div class="media-body chat-user-box">
                <p class="user-title m-0">
                    @if ($log->user_id)
                        @if (Auth::user()->id == $log->user_id)
                            You
                        @else
                            {{ $log->user->full_name() }}
                        @endif
                    @else
                        @if (Auth::user()->id == $log->client_id)
                            You
                        @else
                            {{ $log->client->name }}
                        @endif
                    @endif
                </p>
                {!! $log->message !!}
            </div>
        </a>
    @endslot
@endcomponent