<!DOCTYPE html>
<html>

<head>
    <title>Global Infoswift Technologies</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <style type="text/css">
        @import url(https://fonts.googleapis.com/css?family=Lato:400,900,700,300,300italic,700italic,900italic,400italic);
        @import url(https://fonts.googleapis.com/css?family=Rufina:400,700);
        @import url(https://fonts.googleapis.com/css?family=Montserrat:400,700);

        * {
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            -webkit-text-size-adjust: none;
        }

        @media only screen and (max-width:800px) {
            td[class="nl_spacer"] {
                width: 10px;
            }

            td[class="nl_column"] {
                width: 100%;
                display: block;
                -moz-box-sizing: border-box;
                -webkit-box-sizing: border-box;
                box-sizing: border-box;
            }

            table[class="nl_center"] {
                margin: 0px auto;
            }

            img[class="nl_center_img"] {
                width: 100%;
                height: auto;
                margin: 0px auto;
                display: block;
            }

            .footer_height {
                height: 35px !important;
            }
        }

        @media only screen and (max-width:600px) {
            td[class="nl_column"] {
                width: 100%;
                display: block;

                -moz-box-sizing: border-box;
                -webkit-box-sizing: border-box;
                box-sizing: border-box;
            }

            table[class="nl_center"] {
                margin: 0px auto;
            }

            td[class="nl_spacer"] {
                width: 10px;
            }

            img[class="nl_center_img"] {
                width: 100%;
                height: auto;
                margin: 0px auto;
                display: block;
            }

            .footer_height {
                height: 35px !important;
            }
        }
    </style>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" yahoo="fix" style="margin:0px; padding:0px;">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="max-width:800px;">
        <tr vailgn="top">
            <td>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr vailgn="top">
                        <td bgcolor="#ffffff" style="background-color:#ffffff;">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr valign="middle">
                                    <td width="100" class="nl_spacer"></td>
                                    <td>
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                            <tr valign="top">
                                                <td style="height:89px;">
                                                    <div style="height:89px;"></div>
                                                </td>
                                            </tr>
                                            <tr valign="top">
                                                <td align="center">
                                                    <img src="{{ URL::asset('images/global-logo.png') }}"
                                                        style="height:80px; width:auto;">
                                                </td>
                                            </tr>
                                            <tr valign="top">
                                                <td style="height:60px;">
                                                    <div style="height:60px;"></div>
                                                </td>
                                            </tr>
                                            <tr valign="top">
                                                <td align="">
                                                    <span
                                                        style="font-family: 'Montserrat', sans-serif; font-size:30px; color:#ee3336; font-weight:700">
                                                        @yield('title')
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr valign="top">
                                                <td style="height:25px;">
                                                    <div style="height:25px;"></div>
                                                </td>
                                            </tr>
                                            <tr valign="top">
                                                <td>
                                                    @yield('content')
                                                </td>
                                            </tr>
                                            <tr valign="top">
                                                <td style="height:34px;">
                                                    <div style="height:34px;"></div>
                                                </td>
                                            </tr>
                                            <tr valign="top">
                                                <td align="center">
                                                    @yield('action')
                                                    {{--  <a href="{{ route('contact.show.ticket', $ticket->id) }}"
                                                    style=" font-family: 'Lato', sans-serif; font-size:14px; color:#fff;
                                                    font-weight:400; width:250px;; height:46px; line-height:46px;
                                                    background-color: #094874;display: inline-block;border-radius:
                                                    5px;text-align: center;text-decoration: none;">View
                                                    Ticket</a> --}}
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="100" class="nl_spacer"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td style="height:100px;">
                            <div style="height:100px;"></div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr valign="top">
            <td>
                <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#f2f2f2"
                    style="background-color:#f2f2f2;">
                    <tr valign="top">
                        <td>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr valign="top">
                                    <td width="96" class="nl_spacer"></td>
                                    <td width="80%" class="nl_column">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                            <tr valign="top">
                                                <td>
                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                        <tr valign="top">
                                                            <td style="height:68px;">
                                                                <div style="height:68px;"></div>
                                                            </td>
                                                        </tr>
                                                        <tr valign="top">
                                                            <td style="text-align: center">
                                                                <img src="{{ URL::asset('images/global-logo.png') }}"
                                                                    alt="" border="0" style="height:40px; width:auto;">
                                                            </td>
                                                        </tr>
                                                        <tr valign="top">
                                                            <td style="height:12px;">
                                                                <div style="height:12px;"></div>
                                                            </td>
                                                        </tr>
                                                        <tr valign="top">
                                                            <td style="text-align: center">
                                                                <span
                                                                    style="font-family: 'Lato', sans-serif; color:#7f8c8d; font-size:14px; font-weight: 400; line-height:20px;">
                                                                    {{--  We are a group of experts sharing a belief in the
                                                                    positive potential of technology, and a commitment
                                                                    to innovation, and excellence.  --}}
                                                                </span>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="96" class="nl_spacer"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr valign="top">
                                    <td width="99" class="nl_spacer"></td>
                                    <td class="nl_column">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                            <tr valign="top">
                                                <td style="height:30px">
                                                    <div style="height:30px"></div>
                                                </td>
                                            </tr>
                                            <tr valign="top">
                                                <td class="nl_column" style="text-align: center;">
                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                        <tr valign="top">
                                                            <td>
                                                                <a href="#"
                                                                    style="font-family: 'Lato', sans-serif; color:#838383; font-size: 13px; font-weight: 400; text-decoration: none;">
                                                                    About</a>
                                                                <span
                                                                    style="font-family: 'Lato', sans-serif; color:#838383; font-size: 13px; font-weight: 400; padding:0px 7px; text-decoration: none;">
                                                                    | </span>
                                                                <a href="#"
                                                                    style="font-family: 'Lato', sans-serif; color:#838383; font-size: 13px; font-weight: 400;text-decoration: none;">
                                                                    Services</a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="99" class="nl_spacer"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr valign="top">
                                    <td width="99" class="nl_spacer"></td>
                                    <td class="nl_column">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                            <tr valign="top">
                                                <td style="height:20px">
                                                    <div style="height:20px"></div>
                                                </td>
                                            </tr>
                                            <tr valign="top">
                                                <td class="nl_column" style="text-align: center">
                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                        <tr valign="top">
                                                            <td>
                                                                <span
                                                                    style="font-family: 'Lato', sans-serif; color:#838383; font-size: 13px; font-weight: 400;">All
                                                                    Rights Reserved {{ date('Y') }}&copy; Global Infoswift Technologies Limited</span>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="99" class="nl_spacer"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td style="height:45px">
                            <div style="height:45px"></div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>


    </table>
</body>

</html>