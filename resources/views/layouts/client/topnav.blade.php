<header id="topnav">
    <div class="topbar-main">
        <div class="container-fluid">
            <!-- Logo container-->
            <div class="logo">
                <a href="{{ route('client.dashboard') }}" class="logo">
                    <img src="{{ asset("images/global-logo.png") }}" 
                        alt="InfoSwift"
                        class="logo-small"> 
                    <img src="{{ asset("images/global-logo.png") }}"
                        alt="InfoSwift" 
                        class="logo-large">
                </a>
            </div>
            <!-- End Logo container-->
            <div class="menu-extras topbar-custom">
                <ul class="navbar-right d-flex list-inline float-right mb-0">
                    <li class="dropdown notification-list d-none d-sm-block">
                        <form role="search" class="app-search">
                            <div class="form-group mb-0">
                                <input type="text" class="form-control" placeholder="Search..">
                                <button type="submit"><i class="fa fa-search"></i></button>
                            </div>
                        </form>
                    </li>
                    <li class="dropdown notification-list">
                        <notification-dropdown 
                            url="{{ route('client.notifications.api') }}" 
                            view-all="{{ route('client.notifications') }}"
                            item-base-url="{{ route('client.notifications') }}"
                        >
                        </notification-dropdown>
                    </li>
                    <li class="dropdown notification-list">
                        <div class="dropdown notification-list">
                            <a class="dropdown-toggle nav-link arrow-none waves-effect nav-user waves-light"
                                data-toggle="dropdown" href="#" role="button" aria-haspopup="false"
                                aria-expanded="false">
                                <img
                                    src="{{ $client->logo() }}" alt="{{ $client->code }}"
                                    class="rounded-circle">
                            </a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown">
                                <!-- item-->
                                <a class="dropdown-item" href="{{ route('client.profile') }}">
                                    <i class="mdi mdi-account-circle m-r-5"></i> Profile
                                </a> 
                                <a class="dropdown-item" href="{{ route('client.notifications') }}">
                                    <i class="mdi mdi-bell-ring-outline m-r-5"></i> Notifications
                                </a>
                                <a class="dropdown-item" href="#">
                                    <i class="mdi mdi-lock-open-outline m-r-5"></i> Lock screen
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item text-danger" href="javascript:void(0)" onclick="logOutApp(event)">
                                    <i class="mdi mdi-power text-danger"></i> Logout
                                </a>
                            </div>
                        </div>
                    </li>
                    <li class="menu-item list-inline-item">
                        <!-- Mobile menu toggle-->
                        <a class="navbar-toggle nav-link">
                            <div class="lines"><span></span> <span></span> <span></span></div>
                        </a>
                        <!-- End mobile menu toggle-->
                    </li>
                </ul>
            </div>
            <!-- end menu-extras -->
            <div class="clearfix"></div>
        </div>
        <!-- end container -->
    </div>
    <!-- end topbar-main -->
    <!-- MENU Start -->
    <div class="navbar-custom">
        <div class="container-fluid">
            <div id="navigation">
                <!-- Navigation Menu-->
                <ul class="navigation-menu">
                    <li class="has-submenu"><a href="{{ route('client.dashboard') }}"><i class="mdi mdi-home"></i>Dashboard</a></li>

                    <li class="has-submenu {{ Request::segment(2) == "tickets" ? "active" : null }}"><a href="#"><i class="mdi mdi-ticket-confirmation"></i>Tickets</a>
                        <ul class="submenu">
                            <li {{ Route::currentRouteName() == "client.tickets" ? "active" : null }}><a href="{{ route('client.tickets') }}">All Tickets</a></li>
                            <li {{ Route::currentRouteName() == "client.create.tickets" ? "active" : null }}><a href="{{ route('client.create.ticket') }}">New Ticket</a></li>
                        </ul>
                    </li>
                    <li class="has-submenu"><a href="#"><i class="mdi mdi-laptop-chromebook"></i>Machines</a>
                        <ul class="submenu">
                            <li><a href="{{ route('client.machines') }}">All Machines</a></li>
                        </ul>
                    </li>
                </ul>
                <!-- End navigation menu -->
            </div>
            <!-- end #navigation -->
        </div>
        <!-- end container -->
    </div>
    <!-- end navbar-custom -->
</header>