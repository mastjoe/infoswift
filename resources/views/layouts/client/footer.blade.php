<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                Newbbmserver solution &copy; {{ date('Y') }}<span class="d-none d-sm-inline-block">- Crafted with <i
                        class="mdi mdi-heart text-danger"></i></span></div>
        </div>
    </div>
</footer>