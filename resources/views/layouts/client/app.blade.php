<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
    <title> @yield('title') | {{ $client->name }} - Client Account | Info Swift</title>
    <meta content="" name="">
    <meta content="" name="">
    <link rel="shortcut icon" href="">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/client-style.css') }}" rel="stylesheet" type="text/css">
    @stack('css')
</head>

<body>
    <div id="wrapper">
        {{-- topnav header --}}
        @include('layouts.client.topnav')
        <!-- Begin page -->
        <div class="wrapper">
            <div class="page-title-box">
                <div class="container-fluid">
                    @if (Route::currentRouteName() == "client.dashboard")
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="state-information d-none d-sm-block">
                                    <div class="state-graph">
                                        <div id="header-chart-1"></div>
                                        <div class="info">Balance $ 2,317</div>
                                    </div>
                                    <div class="state-graph">
                                        <div id="header-chart-2"></div>
                                        <div class="info">Item Sold 1230</div>
                                    </div>
                                </div>
                                <h4 class="page-title">Dashboard</h4>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item active">Welcome to infoswift Dashboard</li>
                                </ol>
                            </div>
                        </div>
                    @else
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="page-title">@yield('page_title')</h4>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="{{ route('client.dashboard') }}">Dashboard</a>
                                    </li>
                                    @yield('crumbs')
                                </ol>
                            </div>
                        </div>
                    @endif
                </div>
                <!-- end container-fluid -->
            </div>
            <!-- page-title-box -->
            <div class="page-content-wrapper">
                <div class="container-fluid">
                   @yield('content')
                </div>
                <!-- end container-fluid -->
            </div>
            <!-- end page content-->
            {{--  logout form  --}}
            <form id="_log_out_form" style="display: none" method="POST" action="{{ route('client.logout') }}">
                @csrf
            </form>
        </div>
        @include('layouts.client.footer')
        <!-- END wrapper -->
        {{--  aux modal  --}}
        @include('partials.aux-modal')
    </div>
    <!-- jQuery  -->
    <script src=" {{ asset('js/app.js') }} "></script>
    <script src=" {{ asset('js/client.app.js') }} "></script>
    <script src=" {{ asset('js/client.dashboard.js') }} "></script>
    <script src=" {{ asset('js/util.js') }} "></script>
    <script src=" {{ asset('js/util.request.js') }} "></script>
    <script src=" {{ asset('js/util.form.js') }} "></script>
    @stack('js')
</body>

</html>