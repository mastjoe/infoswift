@php
    $user = Auth::user();
@endphp
{{-- top bar --}}
<div class="topbar">
    <!-- LOGO -->
    <div class="topbar-left">
        <a href="{{ route('admin.dashboard') }}" class="logo">
            <span>
                {{-- <img src="assets/images/logo.png" alt="" height="24"> --}}
                <img src=" {{ asset('images/global-logo.png') }} " alt="" height="53">
            </span>
            <i>
                {{-- <img src="assets/images/logo-sm.png" alt="" height="22"> --}}
                <img src="{{ asset('images/global-logo.png') }}" alt="" height="22">
            </i>
        </a>
    </div>
    <nav class="navbar-custom">
        <ul class="navbar-right d-flex list-inline float-right mb-0">
            <li class="dropdown notification-list d-none d-sm-block">
                <form role="search" class="app-search">
                    <div class="form-group mb-0"><input type="text" class="form-control" placeholder="Search..">
                        <button type="submit"><i class="fa fa-search"></i></button></div>
                </form>
            </li>
            <li class="dropdown notification-list">
                    <notification-dropdown 
                        url="{{ route('admin.notifications.api') }}"
                        view-all="{{ route('admin.notifications') }}"
                        item-base-url="{{ route('admin.notifications') }}"
                        ></notification-dropdown>
                {{-- <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg">
                    <!-- item-->
                    <h6 class="dropdown-item-text">Notifications (37)</h6>
                    <div class="slimscroll notification-item-list">
                        <!-- item--> 
                        <a href="javascript:void(0);" class="dropdown-item notify-item active">
                            <div class="notify-icon bg-success">
                                <i class="mdi mdi-cart-outline"></i>
                            </div>
                            <p class="notify-details">
                                Your order is placed
                                <span class="text-muted">Dummy text of the printing and typesetting industry.</span></p>
                        </a>
                        <!-- item--> 
                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                            <div class="notify-icon bg-warning">
                                <i class="mdi mdi-message"></i>
                            </div>
                            <p class="notify-details">
                                New Message received
                                <span class="text-muted">You have 87 unread messages</span>
                            </p>
                        </a>
                    </div><!-- All--> 
                    <a href="javascript:void(0);" class="dropdown-item text-center text-primary">
                        View all <i class="fi-arrow-right"></i>
                    </a>
                </div> --}}
            </li>
            <li class="dropdown notification-list">
                <div class="dropdown notification-list nav-pro-img">
                    <a class="dropdown-toggle nav-link arrow-none waves-effect nav-user waves-light"
                        data-toggle="dropdown" href="javascript:;" role="button" aria-haspopup="false" aria-expanded="false">
                        <img src="{{ Auth::user()->avatar() }}" alt="user" class="rounded-circle">
                    </a>
                    <div class="dropdown-menu dropdown-menu-right profile-dropdown">
                        <!-- item--> 
                        <a class="dropdown-item" href="{{ route('admin.profile') }}">
                            <i class="mdi mdi-account-circle m-r-5"></i>
                            Profile
                        </a> 
                        <a class="dropdown-item d-block" href="#">
                            <span class="badge badge-success float-right">11</span>
                            <i class="mdi mdi-settings m-r-5"></i>
                            Settings
                        </a> 
                        <a class="dropdown-item" href="{{ route('admin.notifications') }}">
                            <i class="mdi mdi-bell-ring-outline m-r-5"></i> 
                            Notifications
                        </a>
                        <a class="dropdown-item" href="{{ route('admin.notifications') }}">
                            <i class="mdi mdi-message-alert m-r-5"></i> 
                            P. Messages
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item text-danger" href=""
                            onclick="logOutApp(event)"
                        >
                            <i class="mdi mdi-power text-danger"></i> Logout
                        </a>
                    </div>
                </div>
            </li>
        </ul>
        <ul class="list-inline menu-left mb-0">
            <li class="float-left">
                <button class="button-menu-mobile open-left waves-effect waves-light">
                    <i class="mdi mdi-menu"></i>
                </button>
            </li>
            @if (
                $user->hasPrivilege('create client') || 
                $user->hasPrivilege('create machine') || 
                $user->hasPrivilege('create machine part') ||
                $user->hasPrivilege('create staff') ||
                $user->hasPrivilege('open ticket')
            )      
                <li class="d-none d-sm-block">
                    <div class="dropdown pt-3 d-inline-block">
                        <a class="btn btn-header waves-effect waves-light dropdown-toggle" 
                            href="#" 
                            role="button"
                            id="dropdownMenuLink" 
                            data-toggle="dropdown" 
                            aria-haspopup="true" aria-expanded="false"
                        >
                            Create New
                        </a>
                        <div class="dropdown-menu" 
                            aria-labelledby="dropdownMenuLink"
                        >
                            @can('create', App\Client::class)                            
                                <a class="dropdown-item" href="{{ route('create.client') }}">Client</a> 
                            @endcan
                            @can('create', App\User::class)                            
                                <a class="dropdown-item" href="{{ route('create.staff') }}">Staff</a> 
                            @endcan
                            @can('create', App\Machine::class)                            
                                <a class="dropdown-item" href="{{ route('create.machine') }}">Machine</a> 
                            @endcan
                            @can('create', App\MachinePart::class)                            
                                <a class="dropdown-item" href="{{ route('create.machine.part') }}">Machine Part</a>
                            @endcan
                            @can('create', App\Ticket::class)
                                <a class="dropdown-item" href="{{ route('create.ticket') }}">Ticket</a>
                            @endcan
                            {{-- <div class="dropdown-divider"></div> --}}
                            {{-- <a class="dropdown-item" href="#">Separated link</a> --}}
                        </div>
                    </div>
                </li>
            @endif
        </ul>
    </nav>
</div>