{{-- side nav --}}
@php
    use App\Helpers\Util;
    use App\MachinePartOrder;
    use App\Ticket;

    $user = Auth::user();

    $open_orders = 0;
    $closure_req_count = 0;
    // machine variables
    if ($user->hasPrivilege('manage machine part orders')) {        
        $open_orders = MachinePartOrder::openOrders()->count();
    }

    if ($user->hasPrivilege('assess ticket closure')) {
        $closure_req_count = Ticket::closedTickets()->count();
    }

    $cart_count = $user->cart_sum();
@endphp
<div class="left side-menu">
    <div class="slimscroll-menu" id="remove-scroll">
        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu" id="side-menu">
                <li class="menu-title">Main</li>
                {{--  dashboard  --}}
                <li>
                    <a href=" {{ route('admin.index') }} " class="waves-effect">
                        <i class="mdi mdi-home"></i>
                        <span class="badge badge-primary float-right">3</span>
                        <span>Dashboard</span></a>
                </li>
                @can('viewAny', App\Client::class)                    
                <li>
                    <a href="javascript:void(0);" class="waves-effect {{ Request::segment(2) == "clients" ? "active" : null }}">
                        <i class="mdi mdi-account-multiple"></i>
                        <span> 
                            Clients <span class="float-right menu-arrow"><i class="mdi mdi-plus"></i></span>
                        </span>
                    </a>
                    <ul class="submenu">
                        <li class="{{ Route::currentRouteName() == "all.clients" ? "active" : null}}" >
                            <a href=" {{ route('all.clients') }} ">All Clients</a>
                        </li>

                        @can('create', App\Client::class)
                            <li class="{{ Route::currentRouteName() == "create.client" ? "active" : null }}" >
                                <a href=" {{ route('create.client') }} ">New Client</a>
                            </li>
                        @endcan
                    </ul>
                </li>
                @endcan
                @can('viewAny', App\User::class)                    
                <li>
                    <a href="javascript:void(0);" class="waves-effect {{ Request::segment(2) == "staff" ? "active" : null }}">
                        <i class="mdi mdi-buffer"></i> 
                        <span>
                            Staff <span class="float-right menu-arrow"><i class="mdi mdi-plus"></i></span>
                        </span>
                    </a>
                    <ul class="submenu">
                        <li class="{{ Route::currentRouteName() == "all.staff" ? "active" : null }}">
                            <a href="{{ route('all.staff') }}">All Staff</a>
                        </li>
                        <li class="{{ Route::currentRouteName() == "create.staff" ? "active" : null }}">
                            <a href="{{ route('create.staff') }}">New Staff</a>
                        </li>
                        <li class="{{ Request::segment(2) == "staff" && Request::segment(3) == "roles" ? "active" : null }}">
                            <a href="{{ route('all.staff.roles') }}">Roles</a>
                        </li>
                        <li class="{{ in_array(Route::currentRouteName(), [ "all.privileges", "show.privilege"]) }}">
                            <a href="{{ route('all.privileges') }}">Privileges</a>
                        </li>
                    </ul>
                </li>
                @endcan
                @if (
                    $user->hasPrivilege('view machine') || 
                    $user->hasPrivilege('view machine part') ||
                    $user->hasPrivilege('manage machine faults') ||
                    $user->hasPrivilege('manage machine vendors') ||
                    $user->hasPrivilege('manage machine types') ||
                    $user->hasPrivilege('manage machine faults') ||
                    $user->hasPrivilege('asses pm') ||
                    $user->hasPrivilege('manage machine part orders')
                )
                    
                    <li>
                        <a href="javascript:void(0);" class="waves-effect {{ Request::segment(2) == "machines" ? "active" : null }}">
                            <i class="mdi mdi-laptop-mac"></i> 
                            <span>
                                Machines <span class="float-right menu-arrow">
                                    @php
                                        $machine_count_sum = $cart_count + $open_orders;
                                    @endphp
                                    @if ($machine_count_sum)
                                        <span class="badge badge-secondary float-right">
                                            {{ Util::counterPlus($machine_count_sum, 100) }}
                                        </span>
                                    @else                                        
                                        <i class="mdi mdi-plus"></i></span>
                                    @endif
                            </span>
                        </a>
                        <ul class="submenu">
                            @can('viewAny', App\Machine::class)                            
                            <li class="{{ Request::segment(3) == "machines" && Route::currentRouteName() == "all.machines" ? "active" : null }}">
                                <a href="{{ route('all.machines') }}">All Machines</a>
                            </li>
                            @endcan

                            @can('assign', App\Machine::class)
                            <li class="{{ Request::segment(3) == "assignment" && Request::segment(2) == "machines" ? "active" : null }}">
                                <a href="{{ route('machine.assignment') }}">Assignment</a>
                            </li>
                            @endcan

                            @can('assessPm', App\Machine::class)
                            <li class="{{ Request::segment(3) == "parts" && Request::segment(4) == "orders" ? "active" : null }}">
                                <a href="{{ route('all.machines.pms') }}">
                                    PM
                                </a>
                            </li>
                            @endcan
                            

                            @if ($user->hasPrivilege('view machine part') || $user->hasPrivilege('order machine parts'))
                            <li class="{{ Request::segment(3) == "parts" && Request::segment(2) == "machines" && Request::segment(4) == null  ? "active" : null }}">
                                <a href="{{ route('all.machine.parts') }}">
                                    Parts Hub
                                </a>
                            </li>
                            @endif

                            @if ($user->cart->count())
                            <li class="{{ Request::segment(3) == "cart" && Request::segment(2) == "machines" ? "active" : null }}">
                                <a href="{{ route('machine.parts.cart') }}">
                                    Cart
                                    @if ($cart_count)
                                        <span class="badge badge-primary float-right">
                                            {{ Util::counterPlus($cart_count, 10) }}    
                                        </span>
                                    @endif
                                </a>
                            </li>
                            @endif
                            
                            @can('manageStocks', App\MachinePart::class)                                
                            <li class="{{ Request::segment(3) == "parts" && Request::segment(2) == "stocks" ? "active" : null }}">
                                <a href="{{ route('all.machine.stock') }}">
                                Stocks
                                </a>
                            </li>
                            @endcan

                            @can('order', App\MachinePart::class)
                            <li class="{{ Request::segment(3) == "parts" && Request::segment(2) == "orders" ? "active" : null }}">
                                <a href="{{ route('show.my.machine.part.orders') }}">
                                    My Order
                                </a>
                            </li>
                            @endcan
                            
                            @can('manageOrders', App\MachinePart::class)
                            <li class="{{ Request::segment(3) == "parts" && Request::segment(4) == "orders" ? "active" : null }}">
                                <a href="{{ route('all.machine.parts.order') }}">
                                    Part Orders
                                    @if ($open_orders)
                                        <span class="badge badge-warning float-right">
                                            {{ Util::counterPlus($open_orders, 10) }}    
                                        </span>
                                    @endif
                                </a>
                            </li>
                            @endcan
                        </ul>
                    </li>
                @endif
                
                @if (
                    $user->hasPrivilege('manage machine faults') ||
                    $user->hasPrivilege('manage machine vendors') ||
                    $user->hasPrivilege('manage machine statuses') ||
                    $user->hasPrivilege('manage machine types') ||
                    $user->hasPrivilege('manage machine part categories') ||
                    $user->hasPrivilege('manage client locations') || 
                    $user->hasPrivilege('manage client slas')
                )                    
                <li>
                    <a href="javascript:void(0);" class="waves-effect {{ Request::segment(2) == "settings" ? "active" : null }}">
                        <i class="mdi mdi-settings"></i> 
                        <span>
                            Settings <span class="float-right menu-arrow"><i class="mdi mdi-plus"></i></span>
                        </span>
                    </a>
                    <ul class="submenu">
                        @if ($user->hasPrivilege('manage client locations'))
                        <li class="">
                            <a href="{{ route('settings.locations') }}">Locations</a>
                        </li>                            
                        @endif

                        @if (
                            $user->hasPrivilege('manage machine faults') ||
                            $user->hasPrivilege('manage machine vendors') ||
                            $user->hasPrivilege('manage machine statuses') ||
                            $user->hasPrivilege('manage machine types') ||
                            $user->hasPrivilege('manage machine part categories')
                        )                            
                        <li class="{{ Request::segment(3) == "settings" && Request::segment(4) == "machines" ? "active" : null }}">
                            <a href="{{ route('settings.machines') }}">Machines</a>
                        </li>
                        @endif

                        @if ($user->hasPrivilege('manage client slas'))
                        <li class="{{ Request::segment(3) == "settings" && Request::segment(4) == "sla" }}">
                            <a href="{{ route('sla.clients') }}">SLA</a>
                        </li>
                        @endif
                    </ul>
                </li>
                @endif

                @if (
                    $user->hasPrivilege('view ticket') ||
                    $user->hasPrivilege('open ticket')
                )
                    
                <li>
                    <a href="javascript:void(0);" class="waves-effect {{ Request::segment(2) == "tickets" ? "active" : null }}">
                        <i class="mdi mdi-ticket"></i>
                        <span>
                            Ticket 
                            <span class="float-right menu-arrow">
                                @if ($closure_req_count)
                                <span class="badge badge-primary float-right">
                                    {{ Util::counterPlus($closure_req_count, 100) }}
                                </span>
                                @else
                                <i class="mdi mdi-plus"></i></span>
                                @endif
                            </span>
                        </span>
                    </a>
                    <ul class="submenu">
                        @can('viewAny', App\Ticket::class)                            
                        <li class="{{ Route::currentRouteName() == "all.tickets" ? "active" : null }}">
                            <a href="{{ route('all.tickets') }}">All Tickets</a>
                        </li>
                        @endcan

                        @if($user->hasPrivilege('assess ticket closure'))                            
                        <li class="{{ Route::currentRouteName() == "show.ticket.closure.requests" ? "active" : null }}">
                            <a href="{{ route('show.ticket.closure.requests') }}">
                                Closure Requests
                                @if ($closure_req_count)
                                    <span class="badge badge-secondary float-right">
                                        {{ Util::counterPlus($closure_req_count, 10) }}
                                    </span>
                                @endif
                            </a>
                        </li>
                        @endif

                        @can('create', App\Ticket::class)                            
                        <li class="{{ Route::currentRouteName() == "create.ticket" ? "active" : null }}">
                            <a href="{{ route('create.ticket') }}">New Ticket</a>
                        </li>
                        @endcan
                    </ul>
                </li>
                @endif
                <li class="{{ Request::segment(2) == "kb" ? "active" : null }}">
                    <a href="{{ route('admin.kb') }}" class="{{ Request::segment(2) == "kb" ? "active" : null }}" class="waves-effect">
                        <i class="mdi mdi-book-multiple"></i>
                        <span>
                            Knowledge Base
                        </span>
                    </a>
                </li>

                <li class="menu-title">Extras</li>
                <li class="{{ Request::segment(2) == "reports" ? "active" : null }}">
                    <a href="javascript:void(0);" class="waves-effect">
                        <i class="mdi mdi-calendar-edit"></i>
                        <span> Reports 
                            <span class="float-right menu-arrow"><i class="mdi mdi-plus"></i></span>
                        </span>
                    </a>
                    <ul class="submenu">
                        <li class="{{ Request::segment(2) == "reports" ? "active" : null }}"><a href="{{ route('downlist.report') }}">Downlist</a></li>
                        <li><a href="{{ route('productivity.report') }}">Productivity</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0);" class="waves-effect">
                        <i class="mdi mdi-message-text-outline"></i>
                        <span> Messaging 
                            <span class="badge badge-warning float-right"></span>
                        </span>
                    </a>
                    <ul class="submenu">
                        <li><a href="{{ route('admin.private.messages') }}">Private Messages</a></li>
                        <li><a href="{{ route('message.sms') }}">SMS</a></li>
                        <li><a href="{{ route('message.email') }}">Email</a></li>
                    </ul>
                </li>
                @if ($user->isSuperAdmin())                    
                    <li>
                        <a href="{{ route('all.audits') }}" class="waves-effect {{ Request::segment(2) == "audits" ? "active" : null }}">
                            <i class="mdi mdi-calendar-multiple"></i>
                            <span>
                                Audit 
                            </span>
                        </a>
                    </li>
                @endif
    
            </ul>
        </div><!-- Sidebar -->
        <div class="clearfix"></div>
    </div><!-- Sidebar -left -->
</div>