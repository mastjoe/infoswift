<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
    <title>@yield('title') | Info Swift</title>
    <meta content="Admin Dashboard" name="description">
    <meta content="Themesbrand" name="author">
    <link rel="shortcut icon" href="assets/images/favicon.ico">
    <link href=" {{ asset('template/css/bootstrap.min.css') }} " rel="stylesheet" type="text/css">
    <link href=" {{ asset('template/css/metismenu.min.css') }} " rel="stylesheet" type="text/css">
    <link href=" {{ asset('template/css/icons.css') }} " rel="stylesheet" type="text/css">
    <link href=" {{ asset('template/css/style.css') }} " rel="stylesheet" type="text/css">
    @stack('css')
</head>

<body>
    <!-- Background -->
    <div class="account-pages"></div>
    <!-- Begin page -->
    <div class="wrapper-page">
        @yield('content')
    </div>
    <!-- END wrapper -->
    <script src=" {{ asset('js/j.js') }} "></script>
</body>

</html>