<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
    <title> @yield('title') | Info Swift</title>
    <meta content="" name="">
    <meta content="" name="">
    <link rel="shortcut icon" href="">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css">
    @stack('css')
</head>

<body>
    <!-- Begin page -->
    <div id="wrapper">
        <!-- Top Bar Start -->
        @include('layouts.admin.topbar')
        <!-- Top Bar End -->
        <!-- ========== Left Sidebar Start ========== -->
        @include('layouts.admin.sidenav')
        <!-- Left Sidebar End -->
        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="page-title-box page-title-box-white">
                                <ol class="breadcrumb float-right">
                                   @if (Request::segment(2) != "dashboard")
                                    <li class="breadcrumb-item"><a href=" {{ route('admin.dashboard') }} ">Dashboard</a></li>
                                    @endif
                                    @yield('crumbs')
                                </ol>
                                <h4 class="page-title">@yield('page')</h4>
                            </div>
                        </div>
                    </div>

                    {{--  <div class="row">
                        <div class="col-sm-12">
                            <div class="page-title-box">
                                <h4 class="page-title">
                                    @yield('page')
                                </h4>
                                <ol class="breadcrumb">
                                    @if (Request::segment(2) != "dashboard") 
                                        <li class="breadcrumb-item"><a href=" {{ route('admin.dashboard') }} ">Dashboard</a></li>
                                    @endif
                                    @yield('crumbs')
                                </ol>
                                <div class="state-information d-none d-sm-block">
                                    <div class="state-graph">
                                        <div id="header-chart-1"></div>
                                        <div class="info">Balance $ 2,317</div>
                                    </div>
                                    <div class="state-graph">
                                        <div id="header-chart-2"></div>
                                        <div class="info">Item Sold 1230</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>  --}}
                    <!-- end row -->
                    <div class="page-content-wrapper mt-0">
                        @yield('content')
                    </div>
                    <!-- end page content-->
                </div><!-- container-fluid -->
                {{--  aux modal  --}}
                @include('partials.aux-modal')
            </div><!-- content -->
            @include('layouts.admin.footer')
        </div><!-- ============================================================== -->
        <!-- End Right content here -->
        <!-- ============================================================== -->
        {{--  logout form  --}}
        <form id="_log_out_form" style="display: none" method="POST" action="{{ route('logout') }}">
            @csrf
        </form>
    </div><!-- END wrapper -->
    <!-- jQuery  -->
    <script src=" {{ asset('js/app.js') }} "></script>
    <script src=" {{ asset('js/util.js') }} "></script>
    <script src=" {{ asset('js/util.request.js') }} "></script>
    <script src=" {{ asset('js/util.form.js') }} "></script>
    <script src="{{ asset('js/admin/staff.pm.js') }}"></script>
    @stack('js')
</body>

</html>