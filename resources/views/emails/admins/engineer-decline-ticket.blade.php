@extends('layouts.email.master')
@section('title', 'Part Order Declined')
@section('content')
   <p>Hello {{ $ticket->closer->first_name }}!</p>
   <p>Closure Request on {{ $ticket->ref }} was declined.</p>
   @if ($ticket->declination_comment)
       <p>{{ $ticket->declination_comment }}</p>
   @endif
@endsection

@section('action')
    <a href="{{ route('show.close.ticket.form', $ticket->id) }}"
    style=" font-family: 'Lato', sans-serif; font-size:14px; color:#fff;
    font-weight:400; width:250px;; height:46px; line-height:46px;
    background-color: #f16c69;display: inline-block;border-radius:
    5px;text-align: center;text-decoration: none;">See Closure Details</a>
@endsection