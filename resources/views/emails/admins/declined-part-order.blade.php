@extends('layouts.email.master')
@section('title', 'Parts Order Declined')
@section('content')
   <p>Hello {{ $order->user->first_name }}!</p>
   <p>Your machine part order with reference {{ $order->ref }} has been declined</p>
   <p><b>Administrator Declination Response:</b></p>
   <p>{{ $order->declination_remark }}</p>
@endsection