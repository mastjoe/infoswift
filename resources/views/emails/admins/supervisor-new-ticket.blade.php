@extends('layouts.email.master')
@section('title', 'New Ticket '.$ticket->ref)
@section('content')
   <p>Hello {{ $ticket->client->supervisor->first_name }}!</p>
   <p>
       New ticket with reference, <b>{{ $ticket->ref }}</b> was raised on <b>{{ $ticket->created_at->format('jS F, Y @ h:i a') }}</b> 
    </p>
    <p>
        <h4>Machine Details</h4>
        <table>
            <thead>
                <th width="150"></th>
                <th></th>
            </thead>
            <tbody>
                <tr>
                    <td><b>Terminal ID:</b></td>
                    <td>{{ $ticket->machine->terminal_id }}</td>
                </tr>
                <tr>
                    <td><b>Client:</b></td>
                    <td>{{ $ticket->machine->client->name }}</td>
                </tr>
                <tr>
                    <td><b>Branch:</b></td>
                    <td>{{ $ticket->machine->branch->branch }}</td>
                </tr>
                <tr>
                    <td><b>Region:</b></td>
                    <td>{{ $ticket->machine->region->region }}</td>
                </tr>
            </tbody>
        </table>
    </p>
    @if ($ticket->faults->count())        
        <p>
            <h4>Terminal Faults</h4>
            <ol>
                @foreach ($ticket->faults as $fault)
                <li>{{ $fault->fault }}</li>
                @endforeach
            </ol>
        </p>
    @endif
@endsection