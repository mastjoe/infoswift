@extends('layouts.email.master')
@section('title', 'Reset Password')
@section('content')
   <p>Hello Esteemed Client!</p>
   <p>Your password has been successfully reset!</p>
   <p>Your new password is <b>{{ $password }}</b></p>
@endsection