@extends('layouts.admin.ext')
@section('title', 'Client Login')

@section('content')
<div class="m-2">
    <div class="card">
        <div class="card-body">
            <h3 class="text-center m-0">
                <a href="index.html" class="logo logo-admin">
                    <img src=" {{ asset('images/global-logo.png') }} " height="75" alt="logo">
                </a>
            </h3>
            <div class="p-3">
                <h4 class="text-muted font-18 m-b-5 text-center">Welcome Back Dear Client !</h4>
                <p class="text-muted text-center">Sign in to continue to <span
                        class="text-primary">Global</span>Info<span class="text-dark">Swift</span></p>
                <form class="form-horizontal m-t-30" action=" {{ route('client.login') }} " method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="email">User Code</label>
                        <input type="text" class="form-control @error('user_code') is-invalid @enderror" id="user_code"
                            name="user_code" placeholder="Enter User Code" autofocus autocomplete="user_code" required
                            value="{{ old('user_code') }}">
                        @error('user_code')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" name="password"
                            class="form-control @error('password') is-invalid @enderror" id="password"
                            placeholder="Enter password" autocomplete="current-password" required>
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group row m-t-20">
                        <div class="col-6">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="customControlInline"
                                    name="remember" {{ old('remember')  ? 'checked' : null }}>
                                <label class="custom-control-label" for="customControlInline">Remember me</label>
                            </div>
                        </div>
                        <div class="col-6 text-right">
                            <button class="btn btn-primary w-md waves-effect waves-light" type="submit">Log
                                In</button>
                        </div>
                    </div>
                    <div class="form-group m-t-10 mb-0 row">
                        <div class="col-12 m-t-20">
                            <a href="{{ route('password.request') }}" class="text-muted">
                                <i class="mdi mdi-lock"></i> Forgot your password?</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="m-t-40 text-center">

    <p class="text-muted">© 2020 <i class="mdi mdi-heart text-primary"></i>
    </p>
</div>
@endsection