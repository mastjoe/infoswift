<div class="modal-header">
    <h4 class="modal-title">
        <span class="mr-2">
            @switch($notification->data['category'])
                 @case("ticket")
                    <span class="mdi mdi-ticket text-primary"></span>
                    @break
                @case("parts order")
                    <span class="mdi mdi-shopping text-primary"></span>
                    @break
                @case("message")
                    <span class="mdi mdi-message-reply-text text-primary"></span>
                    @break
                @case("password reset")
                    <span class="mdi mdi-key-change text-primary"></span>
                    @break
                @default
                    
            @endswitch
        </span>
        {{ $notification->data['category'] }}
    </h4>
    <button class="close" type="button" data-dismiss="modal"><span>&times;</span></button>
</div>
<div class="modal-body">
    {{ $notification->data['message'] }}
</div>

<div class="modal-footer">
    <button class="btn btn-default" type="button" data-dismiss="modal">
        Exit
    </button>
</div>