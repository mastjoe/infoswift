@php
    use Carbon\Carbon;
@endphp
@extends('layouts.admin.app')
@section('title', 'Notifications')

@section('page', 'Notifications')

@push('css')
    <style>
        .read-notify {
            background: #efefef;
        }
    </style>
@endpush

@section('crumbs')
    <li class="breadcrumb-item active">
        Notifications
    </li>
@endsection

@section('content')
    <div class="row container">
        @if ($grouped_notifications->count())
            @foreach ($grouped_notifications as $date =>  $notifications)
                <div class="col-12 pb-3 notify-item-group">
                    <span class="bg-primary p-2 rounded d-inline-block text-white mb-2 mt-3">
                        @if (Carbon::parse($date)->isToday())
                            Today
                        @else                            
                            {{ Carbon::parse($date)->format('jS F, Y') }}
                        @endif
                    </span>
                    @foreach ($notifications as $notification)
                        <div class="card my-0 mb-1 py-0 {{ $notification->read_at ? "read-notify" : null}}">
                            <div class="card-body notify-item" style="cursor: pointer" data-url="{{ route('admin.show.notification', $notification->id) }}" data-id="{{ $notification->id }}">
                                {{ Str::limit($notification->data['message'],90, '...') }}
                                <span class="text-primary float-right delete_notify_item_btn"
                                    data-content="{{ $notification->data['message'] }}"
                                    data-url="{{ route('admin.delete.notification', $notification->id) }}"
                                >
                                    <i class="mdi mdi-delete"></i>
                                </span>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endforeach
        @else
            <div class="col-12 text-center my-5">
                <span class="fa fa-bell-slash fa fa-5x"></span>
                <h2 class="text-muted">
                    No notification found
                </h2>
            </div>
        @endif
    </div>
@endsection

@push('js')
    <script>
        $(document).ready(function() {
            const notifyItem = $('.notify-item');
            const deleteNotifyBtn = $('.delete_notify_item_btn');
            const notifyItemGroup = $('.notify-item-group');

            notifyItem.on('click', function() {
                const url = $(this).data('url');
                showNotificationDetails(url);
            });

            const showNotificationDetails = function(url) {
                //get id from url
                const id = url.split("/")[url.split("/").length - 1];
                pushAuxModalLoad({
                    url: url,
                    callback: function() {
                        markNotifyItemAsRead(id);
                    }
                });
            }

            const markNotifyItemAsRead = function(id) {
                notifyItem.each(function(index, el) {
                    const element = $(el);
                    const elementId = element.data('id');
                    if (elementId == id) {
                        element.addClass('read-notify');
                    }
                });
            }

            @if (session('selected_id'))
                showNotificationDetails('{{ session('selected_id') }}');
            @endif

            deleteNotifyBtn.on('click', function(e) {
                e.stopPropagation();
                const url = $(this).data('url');
                const content = $(this).data('content');
                const card = $(this).parents('.card');

                pushRequest({
                    url: url,
                    dialogText: 'You are about to delete this notification, "'+content+'"?',
                    method: 'delete',
                    successTitle: 'Notification Deleted!',
                    callback: function() {
                        card.remove();
                        Swal.fire(
                            'Notification Deleted',
                            'notification was successfully deleted!',
                            'success'
                        );
                    }
                });
            });
        })
    </script>
@endpush