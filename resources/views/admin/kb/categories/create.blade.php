{{-- create category modal --}}
<form method="post" action="{{ route('admin.kb.store.category') }}">
    @component('components.modal', [
        'modal_dialog_class' => 'modal-dialog-centered animated zoomIn',
        'modal_id' => 'new_category_modal',
        'modal_class' => 'animated zoomIn'
    ])
        @slot('modal_header')
            <h5>
                <i class="mdi mdi-new-box text-primary mr-2"></i>
                New Category
            </h5>
            <button class="close" data-dismiss="modal" type="button"><span>&times;</span></button>
        @endslot
    
        @slot('modal_body')
            {{-- category --}}
            @component('components.form.input-form-group', [
                'name' => 'category',
                'label' => 'Category',
                'id' => 'name',
                'required' => true,
                ])
            @endcomponent

            {{-- slug --}}
            @component('components.form.input-form-group', [
                'name' => 'slug',
                'label' => 'Slug',
                'id' => 'slug',
                'required' => true,
                ])
            @endcomponent

            {{-- description --}}
            @component('components.form.textarea-form-group', [
                'name' => 'description',
                'label' => 'Description',
                'id' => 'description',
                'required' => false,
                'placeholder' => 'description of category if any...'
                ])
            @endcomponent

        @endslot
        
        @slot('modal_footer')
            <button class="btn btn-default" type="button" data-dismiss="modal">
                Exit
            </button>
            <button class="btn btn-primary" type="submit">
                Create
            </button>
        @endslot
    @endcomponent
</form>