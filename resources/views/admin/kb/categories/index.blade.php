@extends('layouts.admin.app')
@section('title', 'Categories - Knowledge Base')

@section('page', 'Knowledge Base | Categories')

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.kb') }}">Knowledge Base</a>
    </li>
    <li class="breadcrumb-item active">
        <a>Categories</a>
    </li>
@endsection

@section('content')
    <div class="row">
        @if ($categories->count())
            
            <div class="{{ $kbs->count() ? "col-md-8" : "col-12"  }}">

                @can('manage', App\KnowledgeBase::class)                    
                <div class="text-right mb-2">
                    <button class="btn btn-primary waves-effect new_category_btn">
                        New Category
                    </button>
                </div>
                @endcan

                @component('components.card')
                    @slot('card_body')                        
                        <table class="table table-hover table-striped dt">
                            <thead>
                                <tr>
                                    <th>Category</th>
                                    <th>Slug</th>
                                    <th>Posts</th>
                                    <th>Description</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($categories as $cat)
                                    <tr>
                                        <td><a href="{{ route('admin.kb.show.category', $cat->slug) }}">{{ $cat->category }}</a></td>
                                        <td>{{ $cat->slug }}</td>
                                        <td>{{ number_format($cat->posts->count()) }}</td>
                                        <td>
                                            {{ $cat->description }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @endslot
                @endcomponent
            </div>

            @if ($kbs->count())
                
            <div class="col-md-4">
                @include('admin.kb.side-content')
            </div>
            @endif
        @else
            <div class="col-12">
                <div class="my-5 text-center">
                    <h3 class="text-muted my-5">
                        No category found
                    </h3>
                    <button class="btn btn-primary waves-effect my-2 new_category_btn">
                        Add New Category
                    </button>
                </div>
            </div>
        @endif
    </div>

    {{-- modals --}}
    @include('admin.kb.categories.create')
@endsection

@push('js')
    <script>
        $(document).ready(function() {
            simpleDataTable('.dt');
            formLabelFocus();

            const newCategoryBtn = $('.new_category_btn');
            const newCategoryModal = $('#new_category_modal');
            const newCategoryForm = newCategoryModal.closest('form');


            const showNewCategoryModal = function() {
                newCategoryModal.modal({backdrop: 'static'});
            }

            newCategoryBtn.on('click', function() {
                showNewCategoryModal();
            });

            @if(session('create_new'))
                showNewCategoryModal();
            @endif

             @if(session('category_deleted'))
                Swal.fire(
                    'Category Deleted',
                    '{{ session('category_deleted') }}',
                    'success'
                );
            @endif

            // submission of new category form
            newCategoryForm.on('submit', function(e) {
                e.preventDefault();
                const thisForm = $(this);
                const url = thisForm.attr('action');
                console.log(url)
                resolveForm({
                    url: url,
                    form: thisForm,
                    btnLoadingText: 'Creating...',
                    method: 'post',
                })
                    .then(function(data) {
                        if (data.status == "success") {
                            delayRedirect(data.redirect)
                        }
                    })
                    .catch(function(e) {
                        console.log(e.response)
                    });
            });
        });
    </script>
@endpush