@extends('layouts.admin.app')
@section('title', 'Categories | '.$category->category.' - Knowledge Base')

@section('page', 'Knowledge Base | Categories | '.$category->category)

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.kb') }}">Knowledge Base</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('admin.kb.categories') }}">Categories</a>
    </li>
    <li class="breadcrumb-item active">
        <a>{{ $category->category }}</a>
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="text-right mb-2">
                @if ($posts->count())                    
                    @can('manage', App\KnowledgeBase::class)
                    <a class="btn btn-dark waves-effect m-1" href="{{ route('admin.create.kb', ['category' => $category->id]) }}">
                        New Post
                    </a>
                    @endcan
                @endif
                
                @can('manage', App\KnowledgeBase::class)                    
                <button class="btn btn-warning waves-effect m-1 edit_category_btn">
                    Edit 
                </button>
                @endcan

                @if ($category->deletable())
                    <button class="btn btn-primary waves-effect delete_category_btn m-1"
                        data-url="{{ route('admin.kb.delete.category', $category->slug) }}"
                    >
                        Delete
                    </button>
                @endif
            </div>
        </div>
        @if ($posts->count())
            
            <div class="{{ $kbs->count() ? "col-md-8" : "col-12"  }}">
               @foreach ($posts as $kb)
                    <a href="{{ route('admin.show.kb', $kb->ref) }}">
                        @include('admin.kb.kb-card')
                    </a>
               @endforeach
               <div class="mt-3">
                   {{ $posts->links() }}
               </div>
            </div>

            @if ($kbs->count())
                
            <div class="col-md-4">
                @include('admin.kb.side-content')
            </div>
            @endif
        @else
            <div class="col-12">
                <div class="my-5 text-center">
                    <h1 class="text-muted">
                        No post found in {{ $category->category }} category
                    </h1>

                    @can('manage', App\KnowledgeBase::class)                        
                        <a class="btn btn-primary waves-effect my-1" href="{{ route('admin.create.kb', ['category' => $category->id]) }}">
                            Add New Post
                        </a>
                    @endcan
                </div>
            </div>
        @endif
    </div>

    {{-- modals --}}
    @include('admin.kb.categories.edit')
@endsection

@push('js')
    <script>
        $(document).ready(function() {
            simpleDataTable('.dt');
            formLabelFocus();

            const editCategoryBtn = $('.edit_category_btn');
            const editCategoryModal = $('#edit_category_modal');
            const editCategoryForm = editCategoryModal.closest('form');

            const deleteCategoryBtn = $('.delete_category_btn');


            const showEditCategoryModal = function() {
                editCategoryModal.modal({backdrop: 'static'});
            }

            editCategoryBtn.on('click', function() {
                showEditCategoryModal();
            });

            @if(session('edit_category'))
                showEditCategoryModal();
            @endif

            @if(session('category_updated'))
                Swal.fire(
                    'Category Updated',
                    '{{ session('category_updated') }}',
                    'success'
                );
            @endif

            @if (session('new_category'))
                Swal.fire(
                    'New Category',
                    '{{ session('new_category') }}',
                    'success'
                );
            @endif

            // submission of new category form
            editCategoryForm.on('submit', function(e) {
                e.preventDefault();
                const thisForm = $(this);
                const url = thisForm.attr('action');
                resolveForm({
                    url: url,
                    form: thisForm,
                    btnLoadingText: 'Saving...',
                    method: 'post',
                })
                    .then(function(data) {
                        if (data.status == "success") {
                            refreshPage()
                        }
                    })
                    .catch(function(e) {
                        console.log(e)
                    });
            });

            // delete category function
            deleteCategoryBtn.on('click', function() {
                const url = $(this).data('url');
                 pushRequest({
                    url: url,
                    dialogText: 'You are about to delete category {{ $category->category }}?',
                    method: 'delete',
                    successTitle: 'Category Deleted!'
                });
            });

        });
    </script>
@endpush