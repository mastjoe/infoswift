<div class="card m-b-30">
    <img class="img-fluid card-img-top" src="{{ $kb->featured_image_link() }}">
    <div class="card-body">
        <h4 class="card-title font-16 text-dark">{{ $kb->title }}</h4>
        <p class="card-text text-muted">{{ $kb->content_intro() }}</p>
        <div class="text-secondary">
            <span class="d-inline-block ml-2">
                <span class="mdi mdi-eye-outline text-primary"></span>
                {{ number_format($kb->read_count) }}
            </span>
            <span class="d-inline-block ml-2">
                <span class="mdi mdi-account text-primary"></span>
                {{ $kb->user->first_name }}
            </span>
            <span class="d-inline-block ml-2">
                <span class="mdi mdi-calendar-clock text-primary"></span>
                <span title="{{ $kb->created_at->format('jS M, Y @ h:i a') }}">{{ $kb->created_at->format('jS M, Y') }}</span>
            </span>
            @if ($kb->reactable)
            <span class="d-inline-block ml-2">
                <span class="mdi mdi-thumb-up-outline text-primary"></span>
                {{ number_format($kb->likes->count()) }}
            </span>
            <span class="d-inline-block ml-2">
                <span class="mdi mdi-thumb-down-outline text-primary"></span>
                {{ number_format($kb->dislikes->count()) }}
            </span>
            @endif
            @if ($kb->commentable)
            <span class="d-inline-block ml-2">
                <span class="mdi mdi-comment-text text-primary"></span>
                {{ number_format($kb->comments->count()) }}
            </span>
            @endif
        </div>
    </div>
</div>