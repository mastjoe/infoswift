@extends('layouts.admin.app')
@section('title', 'Knowledge Base | '.$kb->title)

@section('page', 'Knowledge Base | '.$kb->ref)

@push('css')
    <style>
    </style>
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.kb') }}">Knowledge Base</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('admin.kb.show.category', $kb->category->slug) }}">{{ $kb->category->category }}</a>
    </li>
    <li class="breadcrumb-item active">
        <a>{{ $kb->ref }}</a>
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="col-8">
            {{--  title  --}}
            <h3>{{ $kb->title }}</h3>
        </div>
        <div class="col-4 text-right">
            @can('manage', App\KnowledgeBase::class)                
                <div class="">
                    <a href="{{ route('admin.edit.kb', $kb->ref) }}" class="btn btn-sm waves-effect btn-warning" title="Edit">
                        <i class="mdi mdi-pencil-circle"></i>
                        Edit
                    </a>
                    <button class="btn btn-primary btn-sm waves-effect delete_kb_btn" 
                        title="Delete" 
                        data-deletable="{{ $kb->deletable() ? true: false }}"
                        data-url="{{ route('admin.delete.kb', $kb->ref) }}"
                    >
                        <i class="mdi mdi-delete"></i>
                        Delete
                    </button>
                </div>
            @endcan
        </div>
        <div class="col-12">
           {{--  author  --}}
           <div class="my-3">
               <a href="#" class="media">
                    <img class="d-flex mr-3 rounded-circle" src="{{ $kb->user->avatar() }}" alt="Generic placeholder image" height="36">
                    <div class="media-body chat-user-box">
                        <p class="user-title m-0">
                            {{ $kb->user->full_name() }}
                        </p>
                        <p class="text-muted">
                            <span>
                                <span class="mdi mdi-calendar-clock text-primary mr-1"></span>
                                {{ $kb->created_at->format('jS F, Y @ h:i a') }}
                            </span>
                            <span class="ml-2">
                                <span class="mdi mdi-eye-outline text-primary"></span>
                                {{ number_format($kb->read_count) }}
                            </span>
                        </p>
                        <p>
                            <a href="{{ route('admin.kb.show.category', $kb->category->slug) }}">
                                <span class="badge badge-dark px-2 p-1">{{ $kb->category->category }}</span>
                            </a>
                        </p>
                    </div>
                </a>
           </div>
           <div class="w-100 text-center">
               <img class="img-fluid" src="{{ $kb->featured_image_link() }}" style="width:100%">
           </div>
           <div class="">
               {!! $kb->content !!}
           </div>
           <div>
               @if ($kb->reactable)
                   <kb-reaction 
                        like-url="{{ route('admin.like.kb', $kb->ref) }}"
                        dislike-url="{{ route('admin.dislike.kb', $kb->ref) }}"
                        post-url="{{ route('admin.json.kb', $kb->ref) }}"
                    ></kb-reaction>
               @endif
           </div>
           <div>
               @if ($kb->commentable)
                   <h3 class="text-primary d-inline-block bg-light p-2 px-4" data-toggle="collapse" data-target="#comments-content">
                       <span class="mdi mdi-18px mdi-comment-text mr-2"></span>
                       Comments ({{ number_format($kb->comments->count()) }})
                    </h3>
                   <div class="collapse show" id="comments-content">
                      <div>
                            @foreach ($kb->comments as $comment)
                                <div>
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="text-right">
                                                <div class="dropdown mo-mb-2">
                                                    <span class="btn shadow-none waves-effect waves-light" type="button" id="dropdownMenuButton"
                                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="fa fa-ellipsis-v"></i>
                                                    </span>
                                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                        @can('manage', App\KnowledgeBase::class)
                                                        <a href="javascript:void(0)" 
                                                            class=" dropdown-item text-primary delete_comment_btn"
                                                            data-url="{{ route('admin.delete.kb.comment', [$kb->ref, $comment->id]) }}"
                                                            data-user="{{ $comment->user->full_name() }}">
                                                            Delete
                                                        </a>
                                                        @endcan
                                                        @if ($comment->can_edit())
                                                            <a href="javascript:void(0)" class="dropdown-item edit_comment_btn text-dark" 
                                                                data-url="{{ route('admin.update.kb.comment', [$kb->ref, $comment->id]) }}"
                                                                data-comment="{{ $comment->comment }}"
                                                            >
                                                                Edit
                                                            </a>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="javascript:;" class="text-dark d-block">
                                                <div class="inbox-item">
                                                    <div class="inbox-item-img float-left mr-3">
                                                        <img src="{{ $comment->user->avatar() }}" class="thumb-md rounded-circle" alt="{{ $comment->user->first_name }}">
                                                    </div>
                                                    <h6 class="inbox-item-author mt-0 mb-1">
                                                        @if ($comment->user->is($user))
                                                            You
                                                        @else                                          
                                                            {{ $comment->user->full_name }}
                                                        @endif
                                                    </h6>
                                                    <p class="inbox-item-text text-muted mb-0">{{ $comment->comment }}</p>
                                                    <p class="inbox-item-date text-muted">
                                                        {{ $comment->created_at->diffForHumans() }}
                                                    </p>
                                                </div>
                                            </a>
                                            <div class="text-right py-0">
                                                                                               
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                      </div>
                      <div>
                          <form method="POST" id="comment_form" action="{{ route('admin.kb.comment', $kb->ref) }}">
                                @csrf
                                <textarea class="form-control bg-transparent" 
                                    placeholder="make a comment"
                                    name="comment"
                                    required
                                ></textarea>
                                <div class="text-right my-2">
                                    <button class="btn btn-primary waves-effect" title="submit comment" type="submit" disabled>
                                        Submit
                                    </button>
                                </div>
                          </form>
                      </div>
                   </div>

               @endif
           </div>
       </div>
    </div>

    {{--  update comment modal  --}}
    <div class="modal fade" id="comment_update_modal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <form>
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title">
                            <span class="mr-2 text-primary fa fa-pencil-alt"></span>
                            Edit Comment
                        </h5>
                        <button class="close" type="button" data-dismiss="modal"><span>&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class=form-group>
                            <textarea class="form-control"
                                placeholder="comment here..."
                                name="comment"
                                required
                            ></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-default" data-dismiss="modal" type="button">
                            Exit
                        </button>
                        <button class="btn btn-primary" type="submit">
                            Save
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        $(document).ready(function() {
            const commentForm = $('#comment_form');
            const textField = commentForm.find('textarea[name="comment"]');
            const commentBtn = textField.parent('form').find('button[type="submit"]');

            const deletePostBtn = $('.delete_kb_btn');
            const deletePostCommentBtn = $('.delete_comment_btn');

            const editCommentBtn = $('.edit_comment_btn');
            const commentUpdateModal = $('#comment_update_modal');
            const commentUpdateForm = commentUpdateModal.find('form');

            textField.on('change keyup', function() {
                const content = $(this).val().replace(/\s/g, "");
                commentBtn.prop('disabled', true);
                if (content.length) {
                    commentBtn.prop('disabled', false);
                }
            });

            // submission of comment form
            commentForm.on('submit', function(e) {
                e.preventDefault();
                const thisForm = $(this);
                const action = thisForm.attr('action');
                const method = thisForm.attr('method');

                resolveForm({
                    form: thisForm,
                    url: action,
                    method: 'post',
                    btnLoadingText: '...',
                })
                    .then(function(data) {
                        commentForm.get(0).reset();
                        refreshPage();
                    })
                    .catch(function(e) {
                        console.log(e);
                    });
            });

            // update comment modal
            editCommentBtn.on('click', function() {
                const url = $(this).data('url');
                const comment=$(this).data('comment');

                commentUpdateForm.attr('action', url);
                commentUpdateForm.find('textarea').val(comment);
                commentUpdateModal.modal({backdrop: 'static'});
            });

            // submission of update comment form
            commentUpdateForm.on('submit', function(e) {
                e.preventDefault();
                const thisForm = $(this);
                const url = thisForm.attr('action');

                resolveForm({
                    form: thisForm,
                    method:'put',
                    btnLoadingText: 'Saving...',
                    url: url
                })
                    .then(function(data) {
                        if (data.status == "success") {
                            refreshPage()
                        }
                    })
                    .catch(function(e) {
                        console.log(e)
                    });
            })

            @if(session('commented'))
                bootNotify('{{ session('commented') }}', 'success');
            @endif

            @if(session('post_updated'))
                swal.fire(
                    'Post Updated',
                    '{{ session('post_updated') }}',
                    'success'
                );
            @endif

            @if(session('post_deleted'))
                swal.fire(
                    'Post Deleted',
                    '{{ session('post_deleted') }}',
                    'success'
                );
            @endif

            @if (session('comment_deleted'))
                swal.fire(
                    'Comment Deleted',
                    '{{ session('comment_deleted') }}',
                    'success'
                );
            @endif
            
            @if (session('comment_updated'))
                bootNotify('{{ session('comment_updated') }}', 'success');
            @endif

            // delete post
            deletePostBtn.on('click', function() {
                const url = $(this).data('url');
                const canDelete = $(this).data('deletable');
    
                if (canDelete) {
                    pushRequest({
                        url: url,
                        dialogText: 'You are about to delete post {{ $kb->ref }}?',
                        method: 'delete',
                        successTitle: 'Post Deleted!'
                    });
                } else{
                    swal.fire(
                        'Cannot Delete',
                        "Post has some comment and cannot be deleted!",
                        "error"
                    );
                }
            });

            deletePostCommentBtn.on('click', function() {
                const url = $(this).data('url');
                const user = $(this).data('user');

                pushRequest({
                    url: url,
                    dialogText: 'You are about to delete a comment made by '+user+'?',
                    method: 'delete',
                    successTitle: 'Comment Deleted!'
                });
            });
        });
    </script>
@endpush