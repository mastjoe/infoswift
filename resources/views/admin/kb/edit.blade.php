@extends('layouts.admin.app')
@section('title', 'Edit - '.$kb->title)

@section('page', 'New Knowledge  | Edit | '.$kb->ref)

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.kb') }}">Knowledge Base</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('admin.show.kb', $kb->ref) }}">{{ $kb->ref }}</a>
    </li>
    <li class="breadcrumb-item active">
        <a>Edit</a>
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <form method="POST" action="{{ route('admin.update.kb', $kb->ref) }}" enctype="multipart/form-data">
                @csrf
                @component('components.card')
                    @slot('card_body')
                        <h4>Edit Post</h4>
                        <hr>
                        {{--  category  --}}
                        @component('components.form.select-form-group', [
                                'name' => 'category',
                                'input_wrap_class' => 'col-md-8',
                                'form_group_class' => 'row',
                                'label' => 'Category',
                                'label_class' => 'col-md-3 text-md-right col-form-label'
                            ])
                            @slot('options')
                                @if ($categories->count())
                                    <option>Choose a category</option>
                                    @foreach ($categories as $category)
                                        <option value="{{ $category->id }}" {{ $category->id == $kb->category_id ? "selected" : null }} >{{ $category->category }}</option>
                                    @endforeach
                                @endif
                            @endslot
                        @endcomponent

                        {{--  title  --}}
                        @component('components.form.input-form-group', [
                                'name' => 'title',
                                'input_wrap_class' => 'col-md-8',
                                'form_group_class' => 'row',
                                'label' => 'Title',
                                'label_class' => 'col-md-3 text-md-right col-form-label',
                                'required' => true,
                                'value' => $kb->title,
                                'props' => ['maxlength' => 120]
                            ])
                        @endcomponent

                        {{--  content  --}}
                        @component('components.form.textarea-form-group', [
                                'name' => 'content',
                                'input_wrap_class' => 'col-md-8',
                                'form_group_class' => 'row',
                                'label' => 'Content',
                                'label_class' => 'col-md-3 text-md-right col-form-label',
                                'required' => true,
                                'value' => $kb->content
                            ])
                        @endcomponent

                        {{--  author  --}}
                        @component('components.form.input-form-group', [
                                'name' => 'author',
                                'input_wrap_class' => 'col-md-8',
                                'form_group_class' => 'row',
                                'label' => 'Author',
                                'value' => $user->full_name(),
                                'label_class' => 'col-md-3 text-md-right col-form-label',
                                'read_only' => true,
                            ])
                        @endcomponent

                        {{--  featured images  --}}
                         @component('components.form.input-form-group', [
                                'name' => 'featured_image',
                                'input_wrap_class' => 'col-md-8',
                                'form_group_class' => 'row',
                                'label' => 'Feature Image',
                                'type' => "file",
                                'label_class' => 'col-md-3 text-md-right col-form-label',
                             ])
                        @endcomponent

                        {{--  commentable  --}}
                        <div class="form-group row">
                            <div class="offset-md-3 col-md-8 form-check">
                                <div>
                                    <input type="checkbox" id="commentable" value="1" class="form-check-input" {{ $kb->commentable ? "checked": null }} name="commentable">
                                    <label for="commentable" class="form-check-label">Post should be commentable</label>
                                </div>
                            </div>
                        </div>

                        {{--  reactable  --}}
                        <div class="form-group row">
                            <div class="offset-md-3 col-md-8 form-check">
                                <div>
                                    <input type="checkbox" id="reactable" {{ $kb->reactable ? "checked" : null }} value="1" class="form-check-input" name="reactable">
                                    <label class="form-check-label" for="reactable">Post should be reactable</label>
                                </div>
                            </div>
                        </div>

                        {{--  show or hide post  --}}
                        <div class="form-group row">
                            <div class="offset-md-3 col-md-8 form-check">
                                <div>
                                    <input type="checkbox" id="hidden" value="1" class="form-check-input" name="hidden" {{ $kb->hidden ? "checked": null }}>
                                    <label class="form-check-label" for="hidden">Hide post</label>
                                </div>
                            </div>
                        </div>
                        <div class="text-center my-2 border-top py-3">
                            <button class="btn btn-primary btn-lg waves-effect px-4" type="submit">
                                Save Post
                            </button>
                        </div>
                    @endslot
                @endcomponent
            </form>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/summernote-gallery.min.js') }}"></script>
    <script>
        $(document).ready(function() {
           
            formLabelFocus();
            const editor = $('#content').summernote({
                placeholder: 'Post content here...',
                spellcheck: true,
                height: 200,
                toolbar: [
                    ['style', ['fontname', 'fontsize']],
                    ['style', ['bold', 'italic', 'underline']],
                    ['style', ['color']],
                    [ 'style',['strikethrough', 'superscript', 'subscript', 'clear']],
                    ['paragraph style', ['style', 'ol', 'ul', 'paragraph', 'hr', 'height']],
                    ['insert', ['picture', 'link', 'video', 'table', 'gallery']],
                    ['misc', ['fullscreen', 'codeview', 'undo', 'redo', 'help']],
                    // ['extensions', ['gallery']],
                ],
                gallery: {
                    source: {
                        // data: [],
                        url: 'http://eissasoubhi.github.io/summernote-gallery/server/example.json',
                        responseDataKey: 'data',
                        nextPageKey: 'links.next',
                    },
                    modal: {
                        loadOnScroll: true,
                        maxHeight: 300,
                        title: "The Image Gallery",
                        close_text: 'Close',
                        ok_text: 'Add Image',
                        selectAll_text: 'Select All',
                        deselectAll_text: 'Deselect All',
                        noImageSelected_msg: 'No image was selected, please select one by clicking it!',
                    }
                },
            });
        });
    </script>
@endpush