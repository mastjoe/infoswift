{{-- recent posts --}}
@component('components.card')
    @slot('card_header')
        <b>Recent Posts</b>
    @endslot
    @slot('card_body')
        <div>
            @if ($recent_posts->count())
                @foreach ($recent_posts as $post)                    
                <div class="py-2 {{ !$loop->last ? "border-bottom": null }}">
                    <h6 title="{{ $post->title }}">
                        <a href="{{ route('admin.show.kb', $post->ref) }}">{{ Str::limit($post->title, 50, '...') }}</a>
                    </h6>
                </div>
                @endforeach
            @else
                <div class="text-center">
                    <h6 class="text-muted">
                        No post found
                    </h6>
                </div>
            @endif
        </div>
    @endslot
@endcomponent

{{-- categories --}}
@component('components.card')
    @slot('card_header')
        <b>Categories</b>
    @endslot
    @slot('card_body')
        <div class="">
            @if ($categories->count())
                <ul class="list-group list-group-flush">
                    @foreach ($categories as $cat)
                        <a class="list-group-item">
                            {{ $cat->category }}
                            <span class="badge badge-secondary float-right">{{ number_format($cat->posts->count()) }}</span>
                        </a>
                    @endforeach
                </ul>
            @else
                <div class="text-center">
                    <h6 class="text-muted">
                        No category found!
                    </h6>
                </div>
            @endif
        </div>
    @endslot
@endcomponent