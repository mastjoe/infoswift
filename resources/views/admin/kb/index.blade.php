@extends('layouts.admin.app')
@section('title', 'Knowledge Base')

@section('page', 'Knowledge Base')

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item active">
        <a>Knowledge Base</a>
    </li>
@endsection

@section('content')
    <div class="row">
        @if ($kbs->count())
            <div class="col-12 text-right mb-2">
                @can('manage', App\KnowledgeBase::class)
                <a class="btn btn-primary waves-effect" href="{{ route('admin.create.kb') }}">
                    New Post
                </a>
                <a class="btn btn-dark waves-effect ml-2" href="{{ route('admin.kb.categories') }}">
                    Categories
                </a>
                @endcan
            </div>
            <div class="col-md-8">
                <div class="row">
                    @foreach ($kbs as $kb)
                    <div class="col-12">
                        <a href="{{ route('admin.show.kb', $kb->ref) }}">
                           @include('admin.kb.kb-card')
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-4">
                @include('admin.kb.side-content')
            </div>
        @else
            <div class="col-12 mt-5">
                <div class="text-center my-5">
                    <h1 class="text-muted">No post found</h1>
                    <a href="{{ route('admin.create.kb') }}" class="btn btn-primary waves-effect btn-lg mt-3">
                        Make New Post
                    </a>
                </div>
            </div>
        @endif
    </div>
@endsection

@push('js')
@endpush