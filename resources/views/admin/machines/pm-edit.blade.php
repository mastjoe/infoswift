{{-- edit pm form --}}
<form method="POST" action="{{ route('update.machine.pm', $pm->id) }}" enctype="multipart/form-data" id="pm_update_form">
    <div class="modal-header">
        <h5 class="modal-title">
            <i class="mdi mdi-file text-primary"></i>
            Update PM Report
        </h5>
        <button class="close" type="button" data-dismiss="modal">
            <span>&times;</span>
        </button>
    </div>
    <div class="modal-body">
         @csrf
            <div class="form_response">
                <div class="alert alert-warning alert-dismissible">
                    <button class="close" data-dismiss="alert" type="button">&times;</button>
                    Form document should either be of type pdf, doc, docx, jpeg, jpg, png and should not
                    be more than 1MB
                </div>
            </div>

            {{-- machine --}}
            @component('components.form.input-form-group', [
                'label' => 'Machine',
                'name' => 'machine',
                'value' => $pm->machine->terminal_id,
                'read_only' => true,
            ])
            @endcomponent

            {{-- quarter --}}
            @component('components.form.select-form-group', [
                'label' => 'Quarter',
                'name' => 'quarter',
                'required' => true,
                'value' => $pm->quarter,
            ])
                @slot('options')
                    <option value="">Choose Quarter</option>
                    <option value="1" {{ $pm->quarter == 1 ? "selected":null }}>First Quarter (January - March)</option>
                    <option value="2" {{ $pm->quarter == 2 ? "selected":null }}>Second Quarter (April - June)</option>
                    <option value="3" {{ $pm->quarter == 3 ? "selected":null }}>Third Quarter (July - September)</option>
                    <option value="4" {{ $pm->quarter == 4 ? "selected":null }}>Fourth Quarter (October - December)</option>
                @endslot
            @endcomponent

            {{--  Note  --}}
            @component('components.form.textarea-form-group', [
                'label' => 'Note',
                'name' => 'note',
                'placeholder' => 'brief note on maintenance (optional)',
                'value' => $pm->note
            ])
            @endcomponent

            {{-- form file --}}
            <div class="form-group">
                <label>Upload Report Form</label>
                <input type="file" 
                    name="form" 
                    class="filestyle" 
                    data-buttonname="btn-secondary"
                >
            </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-default" type="button" data-dismiss="modal">
            Exit
        </button>
        <button class="btn btn-primary" type="submit">
            Save
        </button>
    </div>
</form>