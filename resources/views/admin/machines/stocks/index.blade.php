@extends('layouts.admin.app')
@section('title', 'Machines | Parts Stock')

@section('page', 'Machines | Parts Stock')

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('all.machines') }}">Machines</a>
    </li>
    <li class="breadcrumb-item active">
        Parts Stock
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            @if ($stocks->count())
                <div class="my-2 text-right">
                    <a href="{{ route('create.machine.stock') }}" 
                        class="btn btn-primary waves-effect waves-light"
                    >
                        Add New Stock
                    </a>
                </div>
                @component('components.card')
                    @slot('card_body')
                        <h4 class="card-title">
                            All Machines Part Stock
                        </h4>
                        <hr>
                        <div class="">
                            <table class="table table-bordered stock_table">
                                <thead>
                                    <tr>
                                        <th>Number</th>
                                        <th>Part</th>
                                        <th>Quantity</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($stocks as $stock)
                                        <tr>
                                            <td>
                                                <a class="font-weight-bold" href="{{ route('show.machine.stock', $stock->id) }}">
                                                    {{ $stock->part_number }}
                                                </a>
                                            </td>
                                            <td>
                                                {{ $stock->machine_part->part }}
                                            </td>
                                            <td>
                                                {{ number_format($stock->total_quantity()) }}
                                            </td>
                                            <td>
                                                @if ($stock->total_quantity() > 0)
                                                    <span class="badge badge-success">
                                                        available
                                                    </span>
                                                @else
                                                    <span class="badge badge-dark">not available</span>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endslot
                @endcomponent
            @else
                @component('components.empty')
                    @slot('text')
                       Stock is empty
                    @endslot
                    @slot('body')
                        <a href="{{ route('create.machine.stock') }}" 
                            class="btn btn-primary btn-lg waves-effect"
                        >
                            Add Parts to Stock
                        </a>
                    @endslot
                @endcomponent
            @endif
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/admin/machine.parts.js') }}"></script>
    <script>
        $(document).ready(function() {
            simpleDataTable('.stock_table');
            @if (session('deleted'))
                Swal.fire(
                    'Machine Part Stock Deleted',
                    '{{ session('deleted') }}',
                    'success'
                )
            @endif
        });
    </script>
@endpush