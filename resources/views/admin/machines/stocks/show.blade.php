@extends('layouts.admin.app')
@section('title', 'Machines | Parts Stock | '.$part->part)

@section('page', 'Machines | Parts Stock | '.$part->part)

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('all.machines') }}">Machines</a>
    </li>
    <li class="breadcrumb-item active">
        <a href="{{ route('all.machine.stock') }}">Parts Stock</a>
    </li>
    <li class="breadcrumb-item active">
        <a href="">
            {{ Str::limit($part->part, 15, '...') }}
        </a>
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="col-12 text-right my-2">
            @if ($stock->deletable()) 
            <button class="btn btn-primary waves-effect waves-light delete_stock_btn"
                data-url="{{ route('delete.machine.stock', $stock->id) }}"
                data-name="{{ $stock->part_number }}"
            >
                Delete
            </button>
            @endif
            <button class="btn btn-warning waves-light waves-effect ml-2 edit_stock_btn">
                Edit
            </button>
            @can('manageSupply', App\MachinePart::class)                
            <button class="btn btn-secondary waves-light waves-effect ml-2 add_supply_btn">
                New Supply
            </button>
            @endcan
        </div>
        <div class="col-md-4">
            @component('components.card')
                @slot('card_body')
                    <h5 class="card-title">
                        {{ $part->part }}
                    </h5>
                    <hr>
                    <strong class="text-primary">Part Number</strong>
                    <p>{{ $stock->part_number }}</p>
                    <hr>
                    <strong class="text-primary">Serial Number</strong>
                    <p>
                        @isset($stock->serial_number)
                            {{ $stock->serial_number }}
                        @else
                            <small class="text-muted">Not Available</small>
                        @endisset
                    </p>
                    <hr>
                    <strong class="text-primary">Model</strong>
                    <p>
                        @isset($stock->model)
                            {{ $stock->model }}
                        @else
                            <small class="text-muted">Not Available</small>
                        @endisset
                    </p>
                    <hr>
                @endslot
            @endcomponent
        </div>
        <div class="col-md-8">
            {{-- supplies --}}
            @component('components.card')
                @slot('card_header')
                    <h5 class="card-title">
                        Supplies
                    </h5>
                    <div class="card-options"></div>
                @endslot
                @slot('card_body')
                    @if ($stock->supplies->count())
                       <table class="table table-bordered supply_table">
                            <thead>
                                <tr>
                                    <th>Reference</th>
                                    <th>Supplier</th>
                                    <th width="20">Initial Qty</th>
                                    <th width="20">Qty</th>
                                    <th>Supplied</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($stock->supplies as $supply)
                                    <tr>
                                        <td>
                                            <a href="{{ route('show.stock.supply', [$stock->id, $supply->id]) }}">{{ $supply->reference }}</a>
                                        </td>
                                        <td>
                                            @if ($supply->supplier_id)
                                                {{ $supply->supplier->name }}
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td>
                                            {{ $supply->initial_quantity }}
                                        </td>
                                        <td>
                                            {{ $supply->quantity }}
                                        </td>
                                        <td>
                                            {{ $supply->created_at->diffForHumans() }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table> 
                    @else
                        <div class="text-center text-muted my-4">
                            <h4>No supplies found</h4>
                            <button class="btn btn-primary waves-effect waves-light my-2 add_supply_btn">
                                Add Supply
                            </button>
                        </div>
                    @endif
                @endslot
            @endcomponent
        </div>
    </div>

    {{-- modals --}}
    @include('admin.machines.stocks.edit')
    @include('admin.machines.stocks.add-supply')
@endsection

@push('js')
    <script src="{{ asset('js/admin/machine.parts.js') }}"></script>
    <script>
        $(document).ready(function() {

            $('.amount-input').maskNumber();
            simpleDataTable('.supply_table');

            @if (session('supply_deleted'))
                Swal.fire(
                    'Stock Supply Deleted',
                    '{{ session('supply_deleted') }}',
                    'success'
                )
            @endif

            @if (session('updated'))
                Swal.fire(
                    'Machine Part Updated',
                    '{{ session('updated') }}',
                    'success'
                )
            @endif
            
            @if (session('supply_added'))
                Swal.fire(
                    'Supply Added to Stock',
                    '{{ session('supply_added') }}',
                    'success'
                )
            @endif

            @if (session('add_supply'))
               openAddSupplyModal();
            @endif

            @if (session('edit_stock'))
                openEditStockModal();
            @endif
        });
    </script>
@endpush