{{--  edit machine part stock  --}}
<form method="POST" action="{{ route('update.machine.stock', $stock->id) }}" id="edit_stock_form">
    @component('components.modal', [
        'modal_dialog_class' => 'modal-dialog-centered animated bounceInRight',
        'modal_id' => 'edit_stock_modal',
        'modal_top_dismiss' => true,
    ])
        @slot('modal_header')
            <h5>
                <i class="mdi mdi-library-plus ml-2 text-primary"></i>
                Update Stock
            </h5>
        @endslot

        @slot('modal_body')
            @csrf
            @method('put')
            <div class="form_response"></div>
            {{--  number field  --}}
            @component('components.form.input-form-group', [
                'label' => 'Part Number',
                'name' => 'part_number',
                'placeholder' => 'number',
                'value' => $stock->part_number,
                'required' => true,
            ])
                
            @endcomponent

            {{-- serial number --}}
             @component('components.form.input-form-group', [
                'label' => 'Serial Number',
                'name' => 'serial_number',
                'placeholder' => 'serial number',
                'value' => $stock->serial_number,
                'required' => false,
            ])
                
            @endcomponent

            {{-- model field --}}
             @component('components.form.input-form-group', [
                'label' => 'Model',
                'name' => 'model',
                'placeholder' => 'model here',
                'value' => $stock->model,
                'required' => false,
            ])
            @endcomponent
           

        @endslot

        @slot('modal_footer')
            <button class="btn btn-default" type="button" data-dismiss="modal">
                Exit
            </button>
            <button class="btn btn-primary" type="submit">
                Save
            </button>
        @endslot
    @endcomponent
</form>