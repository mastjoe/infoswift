{{--  add stock supply modal  --}}
<form method="POST" action="{{ route('update.stock.supply', [$stock->id, $supply->id]) }}" id="update_supply_form">
    @component('components.modal', [
        'modal_dialog_class' => 'modal-dialog-centered animated bounceInLeft',
        'modal_id' => 'update_supply_modal',
        'modal_top_dismiss' => true,
    ])
        @slot('modal_header')
            <h5>
                <i class="mdi mdi-pencil mr-2 text-primary"></i>
                Update Supply
            </h5>
        @endslot

        @slot('modal_body')
            @csrf
            @method('put')
            <div class="form_response"></div>
            {{--  stock part  --}}
            @component('components.form.input-form-group', [
                'label' => 'Reference',
                'name' => 'reference',
                'placeholder' => 'reference',
                'value' => $supply->reference,
                'required' => true,
                'read_only' => true
            ])
                
            @endcomponent

            {{-- part number --}}
            @component('components.form.input-form-group', [
                'label' => 'Part Number',
                'name' => 'part_number',
                'value' => $stock->part_number,
                'required' => false,
                'read_only' => true,
            ])
                
            @endcomponent

            {{-- supplier --}}
            @if ($suppliers->count())                
                @component('components.form.select-form-group', [
                    'label' => 'Supplier',
                    'name' => 'supplier',
                    'value' => $stock->model,
                    'required' => false,
                ])
                    @slot('options')
                        <option value="">Choose supplier</option>
                        @foreach ($suppliers as $supplier)
                            <option value="{{ $supplier->id }}"
                                {{ $supply->supplier_id == $supplier->id ? "selected" : null }}    
                            >
                                {{ $supplier->name }}
                            </option>
                        @endforeach
                    @endslot
                @endcomponent
            @endif
           
            {{-- cost price --}}
            @component('components.form.input-form-group', [
                'label' => 'Cost Price',
                'name' => 'cost_price',
                'input_class' => 'amount-input',
                'value' => number_format($supply->cost_price, 2),
                'required' => false,
            ])
                
            @endcomponent

            {{-- selling price --}}
            @component('components.form.input-form-group', [
                'label' => 'Selling Price',
                'name' => 'selling_price',
                'input_class' => 'amount-input',
                'value' => number_format($supply->selling_price),
                'required' => false,
            ])
                
            @endcomponent

            {{-- quantity --}}
            @component('components.form.input-form-group', [
                'label' => 'Quantity',
                'name' => 'quantity',
                'required' => false,
                'type' => 'number',
                'value' => $supply->quantity,
                'props' => ['min' => '1'],
                'required' => true
            ])
                
            @endcomponent
        @endslot

        @slot('modal_footer')
            <button class="btn btn-default" type="button" data-dismiss="modal">
                Exit
            </button>
            <button class="btn btn-primary" type="submit">
                Save
            </button>
        @endslot
    @endcomponent
</form>