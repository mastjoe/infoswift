@extends('layouts.admin.app')
@section('title', 'Machines | Parts Stock | New')

@section('page', 'Machines | Parts Stock | New')

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('all.machines') }}">Machines</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('all.machine.stock') }}">Parts Stock</a>
    </li>
    <li class="breadcrumb-item active">
        Add
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            @if ($parts->count())
                <form method="POST" action="{{ route('store.machine.stock') }}">
                    @csrf
                    @if ($errors->any())
                        <div class="alert alert-primary alert-dismissable">
                            <button class="close" type="button" data-dismiss="alert"><span>&times;</span></button>
                            {{ $errors->first() }}
                        </div>
                    @else
                        @if (!$suppliers->count())
                            <div class="alert alert-dark alert-dismissible">
                                <button class="close" type="button" data-dismiss="alert">
                                    <span>&times;</span>
                                </button>
                                No supplier available
                                @can('manageSupplier', App\MachinePart::class)
                                    <a class="alert-link" href="{{ route('create.parts.supplier') }}">Add Supplier</a>
                                @endcan
                            </div>
                        @endif
                    @endif
                    @component('components.card')
                        @slot('card_header')
                            <h5 class="card-title">
                                Add New Stock
                            </h5>
                        @endslot
                        @slot('card_body')
                            {{-- machine parts --}}
                            @component('components.form.select-form-group', [
                                'name' => 'part',
                                'label' => 'Machine Part',
                                'id' => 'part',
                                'label_class' => 'col-md-3 col-form-label text-md-right',
                                'input_wrap_class' => 'col-md-9',
                                'form_group_class' => 'row',
                                'required' => true,
                            ])
                                @slot('options')
                                    
                                    <option value="">Choose machine part</option>
                                    @foreach ($parts as $part)
                                        <option value="{{ $part->id }}">{{ $part->part }}</option>
                                    @endforeach
                                @endslot
                            @endcomponent

                            {{-- part number --}}
                            @component('components.form.input-form-group', [
                                'name' => 'part_number',
                                'label' => 'Part Number',
                                'id' => 'part_number',
                                'label_class' => 'col-md-3 col-form-label text-md-right',
                                'input_wrap_class' => 'col-md-9',
                                'form_group_class' => 'row',
                                'required' => true,
                                'placeholder' => 'unique number for part'
                            ])
                            @endcomponent

                            {{-- supplier --}}
                            @if ($suppliers->count())                                
                                @component('components.form.select-form-group', [
                                    'name' => 'supplier',
                                    'label' => 'Part Supplier',
                                    'id' => 'part',
                                    'label_class' => 'col-md-3 col-form-label text-md-right',
                                    'input_wrap_class' => 'col-md-9',
                                    'form_group_class' => 'row',
                                    'required' => false,
                                ])
                                    @slot('options')
                                        
                                        <option value="">Choose part supplier</option>
                                        @foreach ($suppliers as $supplier)
                                            <option value="{{ $supplier->id }}">{{ $supplier->name }}</option>
                                        @endforeach
                                    @endslot
                                @endcomponent
                            @endif

                            {{-- serial number --}}
                            @component('components.form.input-form-group', [
                                'name' => 'serial_number',
                                'label' => 'Serial Number',
                                'id' => 'serial_number',
                                'label_class' => 'col-md-3 col-form-label text-md-right',
                                'input_wrap_class' => 'col-md-9',
                                'form_group_class' => 'row',
                                'required' => false,
                                'placeholder' => 'serial number'
                            ])
                            @endcomponent

                            {{-- model --}}
                            @component('components.form.input-form-group', [
                                'name' => 'model',
                                'label' => 'Model',
                                'id' => 'model',
                                'label_class' => 'col-md-3 col-form-label text-md-right',
                                'input_wrap_class' => 'col-md-9',
                                'form_group_class' => 'row',
                                'required' => false,
                                'placeholder' => 'model'
                            ])
                            @endcomponent

                            {{-- cost price --}}
                            @component('components.form.input-form-group', [
                                'name' => 'cost_price',
                                'label' => 'Cost Price',
                                'id' => 'cost_price',
                                'input_class'=> 'amount-input',
                                'label_class' => 'col-md-3 col-form-label text-md-right',
                                'input_wrap_class' => 'col-md-9',
                                'form_group_class' => 'row',
                                'required' => true,
                                'placeholder' => 'cost price'
                            ])
                            @endcomponent

                            {{-- selling price --}}
                            @component('components.form.input-form-group', [
                                'name' => 'selling_price',
                                'label' => 'Selling Price',
                                'id' => 'selling_price',
                                'input_class'=> 'amount-input',
                                'label_class' => 'col-md-3 col-form-label text-md-right',
                                'input_wrap_class' => 'col-md-9',
                                'form_group_class' => 'row',
                                'required' => true,
                                'placeholder' => 'selling price'
                            ])
                            @endcomponent

                            {{-- quantity --}}
                            @component('components.form.input-form-group', [
                                'name' => 'quantity',
                                'label' => 'Quantity',
                                'id' => 'quantity',
                                'type' => 'number',
                                'input_class'=> '',
                                'label_class' => 'col-md-3 col-form-label text-md-right',
                                'input_wrap_class' => 'col-md-9',
                                'form_group_class' => 'row',
                                'required' => true,
                                'placeholder' => 'quantity here',
                                'props' => ['min' => 1]
                            ])
                            @endcomponent

                            <hr>
                            <div class="text-center my-3">
                                <button class="btn btn-primary waves-effect waves-light">
                                    Add To Stock
                                </button>
                            </div>
                        @endslot
                    @endcomponent
                </form>
            @else
                @component('components.empty')
                    @slot('text')
                       No machine part found
                    @endslot
                    @slot('body')
                        @can('create', App\MachinePart::class)                            
                            <a href="{{ route('create.machine.part') }}" 
                                class="btn btn-primary btn-lg waves-effect"
                            >
                                Add Machine Part
                            </a>
                        @endcan
                    @endslot
                @endcomponent
            @endif
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/admin/machine.parts.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.amount-input').maskNumber();
        });
    </script>
@endpush