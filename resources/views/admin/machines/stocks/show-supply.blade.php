@extends('layouts.admin.app')
@section('title', 'Machines | Parts Supply | '.$supply->reference)

@section('page', 'Machines | Parts Supply | '.$supply->reference)

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('all.machines') }}">Machines</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('all.machine.stock') }}">Parts Stock</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('show.machine.stock', $stock->id) }}">
            {{ Str::limit($part->part, 15, '...') }}
        </a>
    </li>
    <li class="breadcrumb-item active">
       {{ $supply->reference }}
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="col-12 text-right my-2">
            @if ($supply->deletable()) 
            <button class="btn btn-primary waves-effect waves-light delete_supply_btn"
                data-url="{{ route('delete.stock.supply', [$stock->id, $supply->id]) }}"
                data-name="{{ $supply->reference }}"
            >
                Delete
            </button>
            @endif
            @if ($supply->editable())
                
            <button class="btn btn-warning waves-light waves-effect ml-2 edit_supply_btn">
                Edit
            </button>
            @endif
        </div>
        <div class="col-md-4">
            @component('components.card')
                @slot('card_body')
                    <h6 class="card-title">
                        Supply {{ $supply->reference }}
                    </h6>
                    <hr>
                    <strong class="text-primary">Part</strong>
                    <p>{{ $part->part }}</p>
                    <hr>
                    <strong class="text-primary">Part Number</strong>
                    <p>{{ $stock->part_number }}</p>
                    <hr>
                    <strong class="text-primary">Serial Number</strong>
                    <p>
                        @isset($stock->serial_number)
                            {{ $stock->serial_number }}
                        @else
                            <small class="text-muted">Not Available</small>
                        @endisset
                    </p>
                    <hr>
                    <strong class="text-primary">Model</strong>
                    <p>
                        @isset($stock->model)
                            {{ $stock->model }}
                        @else
                            <small class="text-muted">Not Available</small>
                        @endisset
                    </p>
                    <hr>
                @endslot
            @endcomponent
        </div>
        <div class="col-md-8">
            {{-- supply info --}}
            @component('components.card')
                @slot('card_header')
                    <b>Supply Info</b>
                @endslot
                @slot('card_body')
                    <div class="">
                        <table class="table table-sm table-borderless">
                            <tbody>
                                <tr>
                                    <td>
                                        <b class="text-primary">Reference</b>
                                    </td>
                                    <td>
                                        {{ $supply->reference }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b class="text-primary">Supplied Quantity</b>
                                    </td>
                                    <td>
                                        {{ $supply->initial_quantity }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b class="text-primary">Available Quantity</b>
                                    </td>
                                    <td>
                                        {{ $supply->quantity }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b class="text-primary">Unit Cost Price</b>
                                    </td>
                                    <td>
                                        {{ number_format($supply->cost_price, 2) }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b class="text-primary">Unit Selling Price</b>
                                    </td>
                                    <td>
                                        {{ number_format($supply->selling_price, 2) }}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                @endslot
            @endcomponent

            {{-- supplier info --}}
            @if ($supply->supplier)
                @component('components.card')
                    @slot('card_header')
                        <b>Supplier Info</b>
                    @endslot
                    @slot('card_body')
                        <table class="table table-sm table-borderless">
                            <thead>
                                <th width="150"></th>
                                <th></th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><b class="text-primary">Name</b></td>
                                    <td>{{ $supply->supplier->name }}</td>
                                </tr>
                                <tr>
                                    <td><b class="text-primary">Email</b></td>
                                    <td>{{ $supply->supplier->email }}</td>
                                </tr>
                                <tr>
                                    <td><b class="text-primary">Phone</b></td>
                                    <td>{{ $supply->supplier->phone }}</td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="text-center">
                            @can('manageSupplier', App\MachinePart::class)
                                <a href="{{ route('show.parts.supplier', $supply->supplier_id) }}">See More</a>
                            @endcan
                        </div>
                    @endslot
                @endcomponent
            @endif
        </div>
    </div>

    {{-- modals --}}
    @include('admin.machines.stocks.edit-supply')
@endsection

@push('js')
    <script src="{{ asset('js/admin/machine.parts.js') }}"></script>
    <script>
        $(document).ready(function() {

            $('.amount-input').maskNumber();

            @if (session('deleted'))
                Swal.fire(
                    'Machine Part Deleted',
                    '{{ session('deleted') }}',
                    'success'
                )
            @endif

            @if (session('supply_updated'))
                Swal.fire(
                    'Supply Updated',
                    '{{ session('supply_updated') }}',
                    'success'
                )
            @endif
            
            @if (session('supply_added'))
                Swal.fire(
                    'Supply Added to Stock',
                    '{{ session('supply_added') }}',
                    'success'
                )
            @endif

            @if (session('edit_supply'))
                openUpdateSupplyModal();
            @endif
        });
    </script>
@endpush