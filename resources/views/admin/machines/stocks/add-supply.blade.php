{{--  add stock supply modal  --}}
<form method="POST" action="{{ route('store.stock.supply', $stock->id) }}" id="add_supply_form">
    @component('components.modal', [
        'modal_dialog_class' => 'modal-dialog-centered animated bounceInLeft',
        'modal_id' => 'add_supply_modal',
        'modal_top_dismiss' => true,
    ])
        @slot('modal_header')
            <h5>
                <i class="mdi mdi-library-plus ml-2 text-primary"></i>
                New Supply
            </h5>
        @endslot

        @slot('modal_body')
            @csrf
            <div class="form_response"></div>
            {{--  stock part  --}}
            @component('components.form.input-form-group', [
                'label' => 'Part',
                'name' => 'part',
                'placeholder' => 'number',
                'value' => $stock->machine_part->part,
                'required' => true,
                'read_only' => true
            ])
                
            @endcomponent

            {{-- part number --}}
            @component('components.form.input-form-group', [
                'label' => 'Part Number',
                'name' => 'part_number',
                'value' => $stock->part_number,
                'required' => false,
                'read_only' => true,
            ])
                
            @endcomponent

            {{-- supplier --}}
            @if ($suppliers->count())                
                @component('components.form.select-form-group', [
                    'label' => 'Supplier',
                    'name' => 'supplier',
                    'value' => $stock->model,
                    'required' => false,
                ])
                    @slot('options')
                        <option value="">Choose supplier</option>
                        @foreach ($suppliers as $supplier)
                            <option value="{{ $supplier->id }}">{{ $supplier->name }}</option>
                        @endforeach
                    @endslot
                @endcomponent
            @endif
           
            {{-- cost price --}}
            @component('components.form.input-form-group', [
                'label' => 'Cost Price',
                'name' => 'cost_price',
                'input_class' => 'amount-input',
                'required' => false,
            ])
                
            @endcomponent

            {{-- selling price --}}
            @component('components.form.input-form-group', [
                'label' => 'Selling Price',
                'name' => 'selling_price',
                'input_class' => 'amount-input',
                'required' => false,
            ])
                
            @endcomponent

            {{-- quantity --}}
            @component('components.form.input-form-group', [
                'label' => 'Quantity',
                'name' => 'quantity',
                'required' => false,
                'type' => 'number',
                'props' => ['min' => '1'],
                'required' => true
            ])
                
            @endcomponent
        @endslot

        @slot('modal_footer')
            <button class="btn btn-default" type="button" data-dismiss="modal">
                Exit
            </button>
            <button class="btn btn-primary" type="submit">
                Add
            </button>
        @endslot
    @endcomponent
</form>