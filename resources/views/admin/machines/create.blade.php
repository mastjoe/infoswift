@extends('layouts.admin.app')
@section('title', 'Machines | New')

@section('page', 'Machine | New')

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('all.machines') }}">Machines</a>
    </li>
    <li class="breadcrumb-item active">
        New
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            @if ($branches->count())     
                <form method="POST" action="{{ route('store.machine') }}">
                    @csrf
                    @component('components.card')
                        
                        @slot('card_body')
                            <h4 class="card-title">
                                New Machine Form
                            </h4>
                            <hr>
                            @if ($errors->count())
                                @component('components.alert', [
                                    'alert_class' => 'alert-primary',
                                    'alert_dismiss' => true,
                                    'text' => $errors->first()
                                ])
                                    
                                @endcomponent
                            @endif
                            {{--  name  --}}
                            @component('components.form.input-form-group', [
                                'name'             => 'name',
                                'label'            => 'Name',
                                'id'               => 'name',
                                'label_class'      => 'col-md-3 col-form-label text-md-right',
                                'input_wrap_class' => 'col-md-9',
                                'form_group_class' => 'row',
                                'required' => false
                            ])
                            @endcomponent

                            {{--  terminal id  --}}
                            @component('components.form.input-form-group', [
                                'name'             => 'terminal_id',
                                'label'            => 'Terminal ID',
                                'id'               => 'terminal_id',
                                'label_class'      => 'col-md-3 col-form-label text-md-right',
                                'input_wrap_class' => 'col-md-9',
                                'form_group_class' => 'row',
                                'required' => true
                            ])
                            @endcomponent

                            {{--  branch status  --}}
                            @component('components.form.select-form-group', [
                                'name' => 'branch_status',
                                'label' => 'Branch Status',
                                'id' => 'branch_status',
                                'label_class' => 'col-md-3 col-form-label text-md-right',
                                'input_wrap_class' => 'col-md-9',
                                'form_group_class' => 'row',
                                'required' => true,
                                'props' => ['onchange' => 'branchStatusSelection(this)']
                            ])
                                @slot('options')
                                    <option value="">Choose Branch Status</option>
                                    <option value="inbranch">Inbranch</option>
                                    <option value="offsite">Offsite</option>
                                @endslot
                            @endcomponent

                            {{--  client  --}}
                            @component('components.form.select-form-group', [
                                'label' => 'Client',
                                'id' => 'client',
                                'name' => 'client',
                                'type' => 'text',
                                'read_only' => true,
                                'label_class' => 'col-md-3 col-form-label text-md-right',
                                'input_wrap_class' => 'col-md-9',
                                'form_group_class' => 'row',
                                'props' => ["onchange"=>"clientBranchSelection(this)"]
                            ])
                                @slot('options')
                                    <option value="">Choose Client</option>
                                    @foreach ($clients as $client)
                                        <option value="{{ $client->id }}">
                                            {{ $client->short_name }}
                                        </option>
                                    @endforeach
                                @endslot
                            @endcomponent

                            {{--  branches  --}}
                            @component('components.form.select-form-group', [
                                'name' => 'branch',
                                'label' => 'Branch',
                                'id' => 'branches',
                                'label_class' => 'col-md-3 col-form-label text-md-right',
                                'input_wrap_class' => 'col-md-9',
                                'form_group_class' => 'row',
                                'required' => true,
                                'props' => ['onchange' => 'branchRegionSelection(this)']
                            ])
                                @slot('options')
                                    <option value="">Choose Branch</option>
                                @endslot
                            @endcomponent

                            {{--  region  --}}
                            @component('components.form.input-form-group', [
                                'label' => 'Region',
                                'name' => 'region',
                                'id' => 'region',
                                'type' => 'text',
                                'read_only' => true,
                                'label_class' => 'col-md-3 col-form-label text-md-right',
                                'input_wrap_class' => 'col-md-9',
                                'form_group_class' => 'row',
                            ])
                            @endcomponent

                            {{--  address  --}}
                            @component('components.form.textarea-form-group', [
                                'name' => 'address',
                                'label' => 'Location Address',
                                'id' => 'address',
                                'label_class' => 'col-md-3 col-form-label text-md-right',
                                'input_wrap_class' => 'col-md-9',
                                'form_group_class' => 'row at d-none',
                                'required' => false
                            ])
                            @endcomponent

                            {{--  model number  --}}
                            @component('components.form.input-form-group', [
                                'name' => 'model_number',
                                'label' => 'Model Number',
                                'id' => 'model_number',
                                'type' => 'text',
                                'label_class' => 'col-md-3 col-form-label text-md-right',
                                'input_wrap_class' => 'col-md-9',
                                'form_group_class' => 'row',
                                'required' => false
                            ])
                            @endcomponent

                            {{--  serial number  --}}
                            @component('components.form.input-form-group', [
                                'name' => 'serial_number',
                                'label' => 'Serial Number',
                                'id' => 'serial_number',
                                'type' => 'text',
                                'label_class' => 'col-md-3 col-form-label text-md-right',
                                'input_wrap_class' => 'col-md-9',
                                'form_group_class' => 'row',
                                'required' => false
                            ])
                            @endcomponent

                            {{--  ip address  --}}
                            @component('components.form.input-form-group', [
                                'name' => 'ip_address',
                                'label' => 'IP Address',
                                'id' => 'ip_address',
                                'type' => 'text',
                                'label_class' => 'col-md-3 col-form-label text-md-right',
                                'input_wrap_class' => 'col-md-9',
                                'form_group_class' => 'row',
                                'required' => false
                            ])
                            @endcomponent
                            
                            {{--  machine type  --}}
                            @component('components.form.select-form-group', [
                                'label' => 'Machine Type',
                                'id' => 'machine_type',
                                'name' => 'machine_type',
                                'label_class' => 'col-md-3 col-form-label text-md-right',
                                'input_wrap_class' => 'col-md-9',
                                'form_group_class' => 'row'
                            ])
                                @slot('options')
                                    <option value="">Choose Machine Type</option>
                                    @foreach ($machine_types as $type)
                                        <option value="{{ $type->id }}">
                                            {{ $type->type }}
                                        </option>
                                    @endforeach
                                @endslot
                            @endcomponent

                            {{--  machine vendor  --}}
                            @component('components.form.select-form-group', [
                                'label' => 'Machine Vendor',
                                'id' => 'machine_vendor',
                                'name' => 'machine_vendor',
                                'label_class' => 'col-md-3 col-form-label text-md-right',
                                'input_wrap_class' => 'col-md-9',
                                'form_group_class' => 'row'
                            ])
                                @slot('options')
                                    <option value="">Choose Machine Vendor</option>
                                    @foreach ($machine_vendors as $vendor)
                                        <option value="{{ $vendor->id }}">
                                            {{ $vendor->vendor }}
                                        </option>
                                    @endforeach
                                @endslot
                            @endcomponent

                            {{--  machine status  --}}
                            @component('components.form.select-form-group', [
                                'label' => 'Machine Status',
                                'id' => 'machine_status',
                                'name' => 'machine_status',
                                'label_class' => 'col-md-3 col-form-label text-md-right',
                                'input_wrap_class' => 'col-md-9',
                                'form_group_class' => 'row'
                            ])
                                @slot('options')
                                    <option value="">Choose Machine Status</option>
                                    @foreach ($machine_statuses as $status)
                                        <option value="{{ $status->id }}">
                                            {{ $status->status }}
                                        </option>
                                    @endforeach
                                @endslot
                            @endcomponent
                            
                            <hr>
                            <div class="form-group my-3 pb-2 text-center">
                                <button class="btn btn-primary px-5" type="submit">
                                    Save
                                </button>
                                <button class="btn btn-secondary px-5 ml-2" type="reset">
                                    Clear
                                </button>
                            </div>
                            
                        @endslot
                    @endcomponent
                </form>
            @elseif (!$machine_types->count())
                @component('components.empty')
                    @slot('text')
                        No machine type found!
                    @endslot
                    @slot('body')
                        <a class="btn btn-primary btn-lg waves-effect" href="{{ route('create.machine.type') }}">
                            Add New Machine Type
                        </a>
                    @endslot
                @endcomponent
            @elseif (!$machine_vendors->count())
                @component('components.empty')
                    @slot('text')
                        No machine vendor found!
                    @endslot
                    @slot('body')
                        <a class="btn btn-primary btn-lg waves-effect" href="{{ route('create.machine.vendor') }}">
                            Add New Machine Vendor
                        </a>
                    @endslot
                @endcomponent
            @elseif (!$machine_statuses->count())
                @component('components.empty')
                    @slot('text')
                        No machine status found!
                    @endslot
                    @slot('body')
                        <a class="btn btn-primary btn-lg waves-effect" href="{{ route('create.machine.status') }}">
                            Add New Machine Status
                        </a>
                    @endslot
                @endcomponent
            @else
                @component('components.empty')
                    @slot('text')
                        No client branch found!
                    @endslot
                    @slot('body')
                        <a class="btn btn-primary btn-lg waves-effect" href="{{ route('create.branch') }}">
                            Add New Branch
                        </a>
                    @endslot
                @endcomponent
            @endif
        </div>
    </div>
@endsection

@push('js')
    <script>
        const branches = [
            @foreach ($branches as $branch) 
                { id: "{{ $branch->id }}", branch: "{{ $branch->branch }}",region:" {{ $branch->region->region }}",clientId: "{{ $branch->client_id }}" },
            @endforeach
        ];

        const clientBranchSelection = function(target) {
            const t = $(target);
            const branchesField = $('#branches');

            if (t.val() != "") {
                const selectedOption = t.find('option:selected');
                const filteredBranches = branches.filter(x => x.clientId == t.val());
                if (filteredBranches.length) {
                    filteredBranches.forEach(x => {
                        branchesField.append( `<option value="${x.id}" data-region="${x.region}">${x.branch}</option>`);
                    });
                }
            }
        }

        const branchRegionSelection = function(target) {
            const t = $(target);
            const regionField = $('#region');
            const selectedOption = t.find('option:selected');
            if (t.val() != "") {
                regionField.val(selectedOption.attr('data-region'))
            }
        }

        const branchStatusSelection = function(target) {
            const t = $(target);
            const addressTarget = $('.at');
            const branchTarget = $('.bt');
            if (t.val() != "") {
            
                if (t.val().toLowerCase() == "offsite") {
                    addressTarget.removeClass('d-none');
                    branchTarget.addClass('d-none');
                } else {
                    addressTarget.addClass('d-none');
                    branchTarget.removeClass('d-none');
                }
            }
        }

        $(document).ready(function() {
            clientBranchSelection('#client');
            branchRegionSelection('#branch');
            branchStatusSelection('#branch_status');
        });
        
    </script>
@endpush