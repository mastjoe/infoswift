<a class="btn btn-dark btn-block my-2 waves-effect" href="{{ route('show.machine', $machine->id) }}">
    Machine Info
</a>
<a class="btn btn-warning btn-block my-2 waves-effect" href="{{ route('show.machine.tickets', $machine->id) }}">
    Ticket History
</a>
<a class="btn btn-secondary btn-block my-2 waves-effect add_personnel_btn" href="{{ route('show.machine.pms', $machine->id) }}">
    Preventive Maintenance
</a>
@if ($machine->deletable())
    @can('delete', $machine)        
        <button class="btn btn-primary btn-block my-2 waves-effect delete_client_btn"
            onclick="deleteMachine(this)"
            data-url="{{ route('delete.machine', $machine->id) }}"
            data-name="{{ $machine->terminal_id }}"
        >
            Delete
        </button>
    @endcan
@endif