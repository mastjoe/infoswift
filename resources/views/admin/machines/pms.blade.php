@php
    use App\Helpers\Util;
@endphp

@extends('layouts.admin.app')
@section('title', 'Machines | Terminal '.$machine->terminal_id. ' | '.$year.' - PM')

@section('page', 'Machine | Terminal '.$machine->terminal_id. ' | '.$year.' - PM')

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('all.machines') }}">Machines</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('show.machine', $machine->id) }}">{{ $machine->terminal_id }}</a>
    </li>
    <li class="breadcrumb-item active">
        PM
    </li>
@endsection

@section('content')
    @include('admin.machines.hero')

    <div class="row mt-0">
        <div class="col-md-9 order-12 order-md-1">
            <div class="row">
                <div class="col-md-8 col-12">
                    <h4 class="text-primary mb-0">{{ $year }} - Preventive Maintenance</h4>
                </div>
                <div class="col-md-4 col-12 text-right">
                    <button class="btn btn-primary create_pm_btn waves-effect">
                        New Report
                    </button>
                </div>
            </div>
            <hr>
            @if ($year_pms->count())
                @foreach ($year_pms as $pm)
                    @component('components.card')
                        @slot('card_header')
                            <b>{{ ucfirst(Util::numberOrdinal($pm->quarter)) }} Quarter Report</b>
                        @endslot
                        @slot('card_body')
                            <div>
                                @if ($pm->status == "accepted")
                                    <div class="alert alert-success">
                                        Pm report was confirmed and accepted {{ $pm->confirmed_at->diffForHumans() }}!
                                    </div>
                                @elseif($pm->status == "declined")
                                    <div class="alert alert-danger">
                                        Pm report was declined {{ $pm->declined_at->diffForHumans() }}!
                                    </div>
                                @else
                                    <div class="alert alert-warning">
                                        Pm report was sent {{ $pm->created_at->diffForHumans() }}! Report is yet to be approval!
                                    </div>
                                @endif
                                <a class="btn btn-dark btn-sm waves-effect"
                                    href="{{ route('show.machine.pms.quarter', [$pm->machine_id, $pm->year, $pm->quarter]) }}"
                                >
                                    See More 
                                </a>
                                @if ($pm->deletable())                                    
                                    <button class="btn btn-primary btn-sm waves-effect delete_pm_btn"
                                        data-url="{{ route('delete.machine.pm', $pm->id) }}"
                                        data-quarter="{{ Util::numberOrdinal($pm->quarter) }}"
                                        data-year="{{ $pm->year }}"
                                    >
                                        Delete
                                    </button>
                                @endif
                            </div>
                        @endslot
                    @endcomponent
                @endforeach
            @else
                <div class="text-center my-5">
                    <h2 class="text-muted">No preventive maintenance report was found!</h2>
                    <button class="btn btn-primary btn-lg mt-2 waves-effect create_pm_btn">
                        Create PM Report
                    </button>
                </div>
            @endif
            <br>
            <h4>Machine PM Archive</h4>
            <hr>
            {{-- pm years --}}
            <div class="row">
                @forelse ($pm_years as $yr)
                    <div class="col-md-2 col-3">
                        <b>
                            @if ($year == $yr )
                                <a>{{ $yr }}</a>
                            @else
                                <a href="{{ route('show.machine.pms', [$pm->machine_id, $yr]) }}">{{ $yr }}</a>
                            @endif
                        </b>
                    </div>
                @empty
                    <div class="col-12 text-center">
                        No Pm Years
                    </div>
                @endforelse
            </div>
        </div>
        <div class="col-md-3 order-1 order-md-12 my-2">
           @include('admin.machines.nav')
        </div>
    </div>

    {{-- modal --}}
    @include('admin.machines.add-pm-report')
@endsection

@push('js')
    <script src="{{ asset('js/admin/machine.js') }}"></script>
    <script src="{{ asset('js/admin/machine.pm.js') }}"></script>
    <script>
        $(document).ready(function() {
            @if (session('pm_deleted'))
                Swal.fire('PM Report Deleted', '{{ session('pm_deleted') }}', 'success');
            @endif
        })
    </script>
@endpush