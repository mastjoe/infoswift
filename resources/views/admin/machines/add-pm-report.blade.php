{{-- add pm report modal --}}
<form method="POST" enctype="multipart/form-data" action="{{ route('store.machine.pm', $machine->id) }}" id="create_pm_form" >
        @component('components.modal', [
            'modal_dialog_class' => 'animated zoomIn',
            'modal_id' => 'create_pm_modal',
            'modal_top_dismiss' => true,
        ])
            @slot('modal_header')
                <h5>
                    <i class="mdi mdi-file text-primary"></i>
                    PM Report
                </h5>
            @endslot

            @slot('modal_body')
                @csrf
                <div class="form_response">
                    <div class="alert alert-warning alert-dismissible">
                        <button class="close" data-dismiss="alert" type="button">&times;</button>
                        Form document should either be of type pdf, doc, docx, jpeg, jpg, png and should not
                        be more than 1MB
                    </div>
                </div>

                {{-- machine --}}
                @component('components.form.input-form-group', [
                    'label' => 'Machine',
                    'name' => 'machine',
                    'value' => $machine->terminal_id,
                    'read_only' => true,
                ])
                @endcomponent

                {{-- quarter --}}
                @php
                    $quarters_taken = $year_pms->pluck('quarter')->toArray();
                @endphp
                @component('components.form.select-form-group', [
                    'label' => 'Quarter',
                    'name' => 'quarter',
                    'required' => true,
                ])
                    @slot('options')
                        <option value="">Choose Quarter</option>
                        <option value="1" {{ in_array("1", $quarters_taken) ? "disabled":null }}>First Quarter (January - March)</option>
                        <option value="2" {{ in_array("2", $quarters_taken) ? "disabled":null }}>Second Quarter (April - June)</option>
                        <option value="3" {{ in_array("3", $quarters_taken) ? "disabled":null }}>Third Quarter (July - September)</option>
                        <option value="4" {{ in_array("4", $quarters_taken) ? "disabled":null }}>Fourth Quarter (October - December)</option>
                    @endslot
                @endcomponent

                {{--  Note  --}}
                @component('components.form.textarea-form-group', [
                    'label' => 'Note',
                    'name' => 'note',
                    'placeholder' => 'brief note on maintenance (optional)'
                ])
                @endcomponent

                {{-- form file --}}
                <div class="form-group">
                    <label>Upload Report Form</label>
                    <input type="file" 
                        name="form" 
                        required 
                        class="filestyle" 
                        data-buttonname="btn-secondary"
                    >
                </div>

                <input type="hidden" name="year" value="{{$year}}">

            @endslot

            @slot('modal_footer')
                <button class="btn btn-default" type="button" data-dismiss="modal">
                    Exit
                </button>
                <button class="btn btn-primary" type="submit">
                    Create
                </button>
            @endslot
        @endcomponent
    </form>