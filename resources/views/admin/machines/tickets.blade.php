@extends('layouts.admin.app')
@section('title', 'Machines | Terminal '.$machine->terminal_id. ' | Ticket History')

@section('page', 'Machine | Terminal '.$machine->terminal_id. ' | Ticket History')

@push('css')
    <style>
        .list-group li {
            list-style: none
        }
    </style>
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('all.machines') }}">Machines</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('show.machine', $machine->id) }}">{{ $machine->terminal_id }}</a>
    </li>
    <li class="breadcrumb-item active">
        Tickets
    </li>
@endsection

@section('content')
    @include('admin.machines.hero')

    <div class="row mt-0">
        <div class="col-md-9 order-12 order-md-1">
            {{-- machine tickets --}}
            @if ($machine->tickets->count())
                @component('components.card')
                    @slot('card_header')
                        <b>Ticket History</b>
                    @endslot
                    @slot('card_body')
                        <table class="table table-bordered table-hover tickets-table">
                            <thead>
                                <tr>
                                    <th>Reference</th>
                                    <th>Status</th>
                                    <th>Faults</th>
                                    <th>Raised</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($machine->tickets as $ticket)
                                    <tr>
                                        <td>
                                            @can('view', $ticket)
                                                <a href="{{ route('show.ticket', $ticket->id) }}">{{ $ticket->ref }}</a>
                                            @else
                                                {{ $ticket->ref }}
                                            @endcan
                                        </td>
                                        <td>
                                            {{ $ticket->status }}
                                        </td>
                                        <td>
                                            <button class="btn btn-sm btn-primary px-3" data-toggle="tooltip"
                                                data-html="true"
                                                title='<ul class="list-group list-group-flush">@foreach($ticket->faults as $fault)<li>{{ $fault->fault->fault }}</li>@endforeach</ul>'
                                            >
                                                {{ $ticket->faults->count() }}
                                                <span class="fa fa-exclamation-triangle"></span>
                                            </button>
                                        </td>
                                        <td>
                                            {{ $ticket->created_at->format('jS M, Y') }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @endslot
                @endcomponent
            @else
                <div class="text-center my-5">
                    <h1 class="text-muted">
                        No ticket found for machine
                    </h1>
                    @can('create', App\Ticket::class)
                        <a class="btn btn-primary waves-effect mt-2" 
                            href="{{ route('create.ticket',['machine' => $machine->id, 'client' => $machine->client_id]) }}"
                        >
                            New Ticket
                        </a>
                    @endcan
                </div>
            @endif
        </div>
        <div class="col-md-3 order-1 order-md-12 my-2">
           @include('admin.machines.nav')
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/admin/machine.js') }}"></script>
    <script>
        $(document).ready(function() {
            simpleDataTable('.tickets-table')
        })
    </script>
@endpush