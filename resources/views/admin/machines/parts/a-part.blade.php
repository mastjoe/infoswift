{{--  a single card unit of a part  --}}
<div class="card m-b-30">
    <img class="card-img-top img-fluid card_image" src="{{ $part->display_image() }}" alt="{{ $part->part }}">
    <div class="card-body">
        <h4 class="card-title font-16 mt-0">{{ $part->part }}</h4>
        <p class="card-text">{{ Str::limit(strip_tags($part->description), 20, '...') }}</p>
        @can('order', App\MachinePart::class)
            <button class="btn btn-outline-primary add_to_cart mr-2"
                data-part="{{ $part->id }}"
                data-name="{{ $part->part }}"
            >
                Add to Cart
            </button>
        @endcan
        <a class="btn btn-primary" href="{{ route('show.machine.part', $part->id) }}">
            View
        </a>
    </div>
</div>
