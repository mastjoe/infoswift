{{--  add cart item dialog  --}}
{{--  add machine part image  --}}
<form method="POST" action="{{ route('add.to.machine.parts.cart') }}" id="add_cart_item_form" enctype="multipart/form-data">
    @component('components.modal', [
        'modal_dialog_class' => 'modal-dialog-centered animated zoomIn',
        'modal_id' => 'add_cart_item_modal',
        'modal_top_dismiss' => true,
    ])
        @slot('modal_header')
            <h5>
                <i class="mdi mdi-cart-plus mr-2 text-primary"></i>
                Add Item to cart
            </h5>
        @endslot

        @slot('modal_body')
            @csrf
            <div>
                <div class="form_response"></div>
                <input type="hidden" name="part">
                <input type="hidden" name="cac">

                <div class="text-center dialog-section">
                    <img src="{{ asset('images/questions.png') }}">
                    <h5>Is part on cash and charge?</h5>
                    <div class="my-3">
                        <button class="btn btn-success waves-effect px-4 m-1 yes-btn"
                            type="button"
                        >
                            Yes
                        </button>
                        <button class="btn btn-dark waves-effect px-4 m-1 no-btn"
                            type="button"
                        >
                            No
                        </button>
                    </div>
                </div>
                <div class="form-fields d-none">
                    {{--  part  --}}
                    @component('components.form.input-form-group', [
                        'label' => 'Machine Part',
                        'name' => 'machine_part',
                        'disabled' => true,
                    ])                        
                    @endcomponent

                    {{--  quantity  --}}
                    @component('components.form.input-form-group', [
                        'type' => 'number',
                        'label' => 'Quantity',
                        'name' => 'quantity',
                        'required' => true,
                        'props' => ['min' => 1],
                        'value' => 1,
                    ])
                        
                    @endcomponent

                    {{--  reason  --}}
                    @component('components.form.textarea-form-group', [
                        'form_group_class' => 'cac-field',
                        'label' => 'Reason for Cash and Charge',
                        'name' => 'cac_reason',
                        'placeholder' => 'reason',
                        'required' => true
                        ])
                    ])
                        
                    @endcomponent

                    {{--  images  --}}
                    @component('components.form.input-form-group', [
                        'form_group_class' => 'cac-field',
                        'type' => 'file',
                        'label' => 'images',
                        'name' => 'images[]',
                        'input_class' => 'upload-field',
                        'props' => ['multiple' => 'multiple'],
                    ])                        
                    @endcomponent
                    <small class="cac-field">images can be uploaded if need be</small>
                </div>
            </div>


        @endslot

        @slot('modal_footer')
            <button class="btn btn-link submit-action back-btn d-none" type="button">
                <span class="mdi mdi-arrow-left-bold-box-outline m-1"></span>
                Back
            </button>
            <button class="btn btn-default m-1" type="button" data-dismiss="modal">
                Exit
            </button>
            <button class="btn btn-primary submit-action d-none m-1" type="submit">
                Add To Cart
            </button>
        @endslot
    @endcomponent
</form>