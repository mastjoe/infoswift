@extends('layouts.admin.app')
@section('title', 'Machines | Parts')

@section('page', 'Machines | Parts')

@push('css')
<style>
    .card_image {
        width: 100%;
        height: 235px;
        max-height: 235px;
    }
</style>
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('all.machines') }}">Machines</a>
    </li>
    <li class="breadcrumb-item active">
        Parts
    </li>
@endsection

@section('content')
    <div class="row">
        @if ($parts->count())
            {{--  search form section  --}}
            <div class="col-12">
                <div class="row my-2">
                    <div class="col-md-4 my-1">
                        <form method="GET">
                            <div class="input-group">
                                <input class="form-control bg-transparent" name="q" value="{{ $query ?? null }}">
                                <div class="input-group-append">
                                    <button class="btn-primary btn" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-4 my-1 text-right">
                        @can('order', App\MachinePart::class)
                            <a href="{{ route('machine.parts.cart') }}" class="cart_target" data-url="{{ route('machine.parts.cart.data') }}">
                                <img src="{{ asset('images/store.png') }}">
                                <span class="badge badge-primary counter d-none" style="margin-left:-15px; margin-bottom: 53px">0
                                </span>
                            </a>
                        @endcan
                    </div>
                    <div class="col-md-4 text-right my-1">
                        @can('order', App\MachinePart::class)
                            <a class="btn btn-secondary waves-light waves-effect m-1"
                                href="{{ route('show.my.machine.part.orders') }}"
                            >
                                My Orders
                            </a>
                        @endcan
                        @can('create', App\MachinePart::class)                            
                            <a href="{{ route('create.machine.part') }}" class="btn btn-primary m-1 waves-light waves-effect ml-2">
                                New Machine Part
                            </a>
                        @endcan
                    </div>
                </div>
            </div>

            @if ($query)
                @if ($searched_parts->count())
                    <div class="col-12">
                        <div class="alert alert-light alert-dismissable">
                            <button class="close" type="button"><span>&times;</span></button>
                            @if ($searched_parts->count() > 1)
                            {{ $searched_parts->count() }} results were
                            @else
                            a result was
                            @endif
                            found
                        </div>
                    </div>
                    @foreach ($searched_parts as $part)
                        <div class="col-lg-3 col-md-4">
                            @include('admin.machines.parts.a-part')
                        </div>
                    @endforeach
                @else
                    <div class="text-center my-4">
                        <h1>No result found for <span class="text-primary">{{ $query }}</span></h1>
                        <a class="btn btn-primary waves-effect" href="{{ route('all.machine.parts') }}">Go Back</a>
                    </div>
                @endif
            @else
                @foreach ($parts as $part)
                    <div class="col-lg-3 col-md-4">
                        @include('admin.machines.parts.a-part')
                    </div>
                @endforeach
            @endif
        @else
            <div class="col-12">
                 @component('components.empty')
                    @slot('text')
                        No machine parts found!
                    @endslot
                    @slot('body')
                        <a href="{{ route('create.machine.part') }}" 
                            class="btn btn-primary btn-lg waves-effect"
                        >
                            Add New Machine Parts
                        </a>
                    @endslot
                @endcomponent
            </div>
        @endif
    </div>

    {{--  modals  --}}
    @include('admin.machines.parts.add-cart-item-dialog')
@endsection

@push('js')
    <script src="{{ asset('js/admin/machine.parts.js') }}"></script>
    <script src="{{ asset('js/admin/machine.cart.js') }}"></script>
    <script src="{{ asset('js/util.upload.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.upload-field').dropUpload();

            @if (session('deleted'))
                Swal.fire(
                    'Machine Part Deleted',
                    '{{ session('deleted') }}',
                    'success'
                )
            @endif

            @if (session('updated'))
                Swal.fire(
                    'Cart Item Updated',
                    '{{ session('updated') }}',
                    'success'
                )
            @endif
        });
    </script>
@endpush