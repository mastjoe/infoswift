{{--  add machine part image  --}}
<form method="POST" action="{{ route('store.machine.part.images', $part->id) }}" id="add_image_form" enctype="multipart/form-data">
    @component('components.modal', [
        'modal_dialog_class' => 'modal-dialog-centered animated bounceInRight',
        'modal_id' => 'add_image_modal',
        'modal_top_dismiss' => true,
    ])
        @slot('modal_header')
            <h5>
                <i class="mdi mdi-library-plus ml-2 text-primary"></i>
                New Machine Part Image
            </h5>
        @endslot

        @slot('modal_body')
            @csrf
            <div class="form_response"></div>

            {{--  fault field  --}}
            @component('components.form.input-form-group', [
                'type' => 'file',
                'label' => 'images',
                'name' => 'images[]',
                'input_class' => 'upload-field',
                'placeholder' => 'fault here',
                'props' => ['multiple' => 'multiple'],
                'required' => true,
            ])
                
            @endcomponent

        @endslot

        @slot('modal_footer')
            <button class="btn btn-default" type="button" data-dismiss="modal">
                Exit
            </button>
            <button class="btn btn-primary" type="submit">
                Add
            </button>
        @endslot
    @endcomponent
</form>