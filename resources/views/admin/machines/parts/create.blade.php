@extends('layouts.admin.app')
@section('title', 'Machines | Parts | New')

@section('page', 'Machines | Parts | New')

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('all.machines') }}">Machines</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('all.machine.parts') }}">Parts</a>
    </li>
    <li class="breadcrumb-item active">
        New
    </li>
@endsection

@section('content')
   <div class="row">
       <div class="col-12">
            @if ($errors->any())
                <div class="alert alert-dismissable alert-primary">
                    <button class="close" data-dismiss="alert"><span>&times;</span></button>
                    {{ $errors->first() }}
                </div>
            @endif
            <form method="POST" action="{{ route('store.machine.part') }}" enctype="multipart/form-data">
                @csrf
                @component('components.card')
                    @slot('card_header')
                        <h4 class="card-title">New Machine Part</h4>
                    @endslot
                    @slot('card_body')
                        {{-- part Category --}}
                         @component('components.form.select-form-group', [
                            'name' => 'category',
                            'label' => 'Category',
                            'id' => 'category',
                            'label_class' => 'col-md-3 col-form-label text-md-right',
                            'input_wrap_class' => 'col-md-9',
                            'form_group_class' => 'row',
                            'required' => true
                        ])
                            @slot('options')
                                <option value="">Choose Part Category</option>
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}">
                                        {{ $category->category }}
                                    </option>
                                @endforeach
                            @endslot
                        @endcomponent

                        {{-- part name --}}
                        @component('components.form.input-form-group', [
                            'name' => 'part',
                            'label' => 'Part',
                            'id' => 'part',
                            'label_class' => 'col-md-3 col-form-label text-md-right',
                            'input_wrap_class' => 'col-md-9',
                            'form_group_class' => 'row',
                            'placeholder' => 'Name of machine part here...',
                            'required' => true,
                            'value' => old('part') ?? null
                        ])
                        @endcomponent

                        {{-- description --}}
                        @component('components.form.textarea-form-group', [
                            'name' => 'description',
                            'label' => 'Description',
                            'id' => 'description',
                            'input_class' => 'text-editor',
                            'label_class' => 'col-md-3 col-form-label text-md-right',
                            'input_wrap_class' => 'col-md-9',
                            'form_group_class' => 'row',
                            'placeholder' => 'description here...',
                            'required' => false,
                            'value' => old('description') ?? null
                        ])
                        @endcomponent

                        {{-- technical specifications --}}
                        @component('components.form.textarea-form-group', [
                            'name' => 'technical_specifications',
                            'label' => 'Technical Specifications',
                            'id' => 'specification',
                            'input_class' => 'text-editor',
                            'label_class' => 'col-md-3 col-form-label text-md-right',
                            'input_wrap_class' => 'col-md-9',
                            'form_group_class' => 'row',
                            'placeholder' => 'technical specifications here...',
                            'required' => false,
                            'value' => old('technical_sepecifications') ?? null
                        ])
                        @endcomponent

                        {{-- documentation --}}
                         @component('components.form.textarea-form-group', [
                            'name' => 'documentation',
                            'label' => 'Documentation',
                            'id' => 'documentation',
                            'input_class' => 'text-editor',
                            'label_class' => 'col-md-3 col-form-label text-md-right',
                            'input_wrap_class' => 'col-md-9',
                            'form_group_class' => 'row',
                            'placeholder' => 'documentations here...',
                            'required' => false,
                            'value' => old('documentation') ?? null
                        ])
                        @endcomponent
                        
                        {{-- images --}}
                        @component('components.form.input-form-group', [
                            'name' => 'images[]',
                            'label' => 'Images',
                            'id' => 'images',
                            'input_class' => 'test',
                            'label_class' => 'col-md-3 col-form-label text-md-right',
                            'input_wrap_class' => 'col-md-9',
                            'form_group_class' => 'row',
                            'required' => false,
                            'type' => 'file',
                            'props' => ['multiple' => 'multiple']
                        ])
                        @endcomponent

                        <hr>
                        <div class="mt-2 text-center">
                            <button class="btn btn-primary waves-effect">
                                Add New Part
                            </button>
                        </div>

                    @endslot
                @endcomponent
            </form>
       </div>
   </div>
@endsection

@push('js')
    <script src="{{ asset('js/util.upload.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.test').dropUpload();
            simpleEditor('.text-editor')
        });
    </script>
@endpush