@php
    use App\Helpers\UI;
@endphp

@extends('layouts.admin.app')
@section('title', 'Machines | Parts | My Orders')

@section('page', 'Machines | Parts | My Orders')

@push('css')
<style>
    .card_image {
        width: 100%;
        height: 235px;
        max-height: 235px;
    }
</style>
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('all.machines') }}">Machines</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('all.machine.parts') }}">Parts</a>
    </li>
    <li class="breadcrumb-item active">
        Orders
    </li>
@endsection

@section('content')
    <div class="row">
        @if ($user->machine_part_orders->count())
            <div class="col-12">
                <div class="text-right my-2">
                    <a class="btn btn-primary waves-light waves-effect" href="{{ route('machine.parts.cart') }}">
                        New Order
                    </a>
                </div>
                @component('components.card')
                    @slot('card_header')
                        <b>Machine Part Order History</b>
                    @endslot
                    @slot('card_body')
                        <div>
                            <table class="table table-striped order_table">
                                <thead>
                                    <th>Reference</th>
                                    <th>Items</th>
                                    <th>Status</th>
                                    <th>Made</th>
                                </thead>
                                <tbody>
                                    @foreach ($user->machine_part_orders->sortByDesc('id') as $order)
                                        <tr>
                                            <td>
                                                <a href="{{ route('show.my.machine.part.orders.details', $order->ref) }}">{{ $order->ref }}</a>
                                            </td>
                                            <td>{{ number_format($order->items->count()) }}</td>
                                            <td>
                                               {!! UI::orderStatusBadge($order->status) !!}
                                            </td>
                                            <td>
                                                {{ $order->created_at->diffForHumans() }}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endslot
                @endcomponent
            </div>
        @else
            <div class="col-12">
                 @component('components.empty')
                    @slot('text')
                        No order found!
                    @endslot
                    @slot('body')
                        @can('order', MachinePart::class)                            
                        <a href="{{ route('machine.parts.cart') }}" 
                            class="btn btn-primary btn-lg waves-effect"
                        >
                            Place an Order
                        </a>
                        @endcan
                    @endslot
                @endcomponent
            </div>
        @endif
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/admin/machine.parts.js') }}"></script>
    <script src="{{ asset('js/admin/machine.cart.js') }}"></script>
    <script>
        $(document).ready(function() {
            @if (session('deleted'))
                Swal.fire(
                    'Cart Item Deleted',
                    '{{ session('deleted') }}',
                    'success'
                )
            @endif

            @if (session('updated'))
                Swal.fire(
                    'Cart Item Updated',
                    '{{ session('updated') }}',
                    'success'
                )
            @endif

            @if (session('checked_out'))
                Swal.fire(
                    'Parts Cart Checked Out',
                    '{{ session('checked_out') }}',
                    'success'
                );
            @endif

            simpleDataTable('.order_table')
        });
    </script>
@endpush