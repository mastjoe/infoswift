@extends('layouts.admin.app')
@section('title', 'Machines | Parts - '.$part->part)

@section('page', 'Machines | Parts | '.Str::limit($part->part, 15, '...'))

@push('css')
    <link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}">
    <style>
        #carouselExampleControls img {
            width: 100%;
            height: 225px;
            max-height: 225px;
        }
        .action-section {
            margin-top: -30px;
        }
    </style>
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('all.machines') }}">Machines</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('all.machine.parts') }}">Parts</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('show.machine.part', $part->id) }}">{{ Str::limit($part->part, 15, '...') }}</a>
    </li>
    <li class="breadcrumb-item active">
        Images
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            @if ($part->images->count())
                <div class="my-2 text-right">

                    @can('update', $part)                        
                        <button class="btn btn-primary waves-effect waves-light" type="button"
                            onclick="openAddImageModal()"
                        >
                            Add New Image
                        </button>
                    @endcan
                </div>
            @endif
            <div class="card m-b-30">
                <div class="card-body">
                    <h4 class="mt-0 header-title">
                        {{ $part->part }}
                    </h4>
                    <div class="popup-gallery">
                        @if ($part->images->count())
                            @foreach ($part->images as $image)                                
                            <a class="float-left" href="{{ $image->link() }}" title="{{ $image->label }}">
                                <div class="img-responsive">
                                    <img src="{{ $image->link() }}" alt="" width="120">
                                </div>
                                @can('update', $part)
                                    
                                    <div class="action-section">
                                        <button class="btn btn-sm btn-primary"
                                            data-url="{{ route('delete.machine.part.image', [$part->id, $image->id]) }}"
                                            data-name="{{ $image->label }}"
                                            onclick="deleteMachinePartImage(this)"
                                        >
                                            <i class="fa fa-trash-alt"></i>
                                        </button>
                                    </div>
                                @endcan
                            </a>
                            @endforeach
                        @else
                            <div class="text-center my-4">
                                <h4 class="text-muted">
                                    No image found for part
                                </h4>

                                @can('update', $part)                                    
                                    <button class="btn btn-primary  my-2 "
                                        onclick="openAddImageModal()"
                                    >
                                        Upload New Image
                                    </button>
                                @endcan
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    {{-- modal --}}
    @include('admin.machines.parts.add-image')
@endsection

@push('js')
    <script src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('js/lightbox.js') }}"></script>
    <script src="{{ asset('js/admin/machine.parts.js') }}"></script>
    <script src="{{ asset('js/util.upload.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.upload-field').dropUpload();

            @if (session('image_deleted'))
                Swal.fire(
                    'Machine Part Image Deleted',
                    '{{ session('image_deleted') }}',
                    'success'
                );
            @endif

            @if (session('image_uploaded'))
                Swal.fire(
                    'Machine Part Images Uploaded',
                    '{{ session('image_uploaded') }}',
                    'success'
                );
            @endif
        })
    </script>
@endpush