@extends('layouts.admin.app')
@section('title', 'Machines | Parts - '.$part->part)

@section('page', 'Machines | Parts | '.Str::limit($part->part, 15, '...'))

@push('css')
    <style>
        #carouselExampleControls img {
            width: 100%;
            height: 225px;
            max-height: 225px;
        }
        .carousel-control-prev, .carousel-control-next {
            width: 5%;
        }
    </style>
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('all.machines') }}">Machines</a>
    </li>
    <li class="breadcrumb-item active">
        <a href="{{ route('all.machine.parts') }}">Parts</a>
    </li>
    <li class="breadcrumb-item active">
        {{ Str::limit($part->part, 15, '...') }}
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-4">
            {{--  display card  --}}
            <div class="card m-b-20">
                @if (!$part->images->count())
                    <img class="card-img-top img-fluid" src="{{ $part->display_image() }}" alt="{{ $part->part }}">
                @endif
                <div class="card-body">
                    <h4 class="mt-0 header-title">{{ $part->part }}</h4>
                    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel" data-interval="3000">
                        @if ($part->images->count())
                           <div class="carousel-inner" role="listbox">
                               @foreach ($part->images as $image)                                   
                                    <div class="carousel-item {{ $loop->first ? "active" : null }}">
                                        <img class="d-block img-fluid" src="{{ $image->link() }}"
                                        alt="{{ $image->label }}">
                                    </div>
                               @endforeach
                                
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span> </a><a class="carousel-control-next" href="#carouselExampleControls"
                                role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span>
                            </a>
                        @endif
                        <p class="card-text my-2">
                            <span class="d-block">Last Updated {{ $part->updated_at->diffForHumans() }}</span>
                            <span class="d-block">Created {{ $part->created_at->diffForHumans() }}</span>
                        </p>
                        <div class="mt-2">
                            @can('update', $part)                            
                                <a href="{{ route('edit.machine.part', $part->id) }}" class="btn btn-warning waves-light waves-effect m-1">
                                    Edit
                                </a>
                            @endcan

                            @can('delete', $part)                            
                                <button class="btn btn-primary waves-effect waves-light"
                                    data-url="{{ route('delete.machine.part', $part->id) }}"
                                    data-name="{{ $part->part }}"
                                    onclick="deleteMachinePart(this)"
                                >
                                    Delete
                                </button>
                            @endcan
                               
                            <a href="{{ route('show.machine.part.images', $part->id) }}" class="btn btn-secondary waves-light waves-effect m-1">
                                <span class="d-none d-md-inline-block">
                                    Images
                                </span>
                                <span class="d-none d-inline-block">
                                    <i class="fa fa-images"></i>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            {{--  <div class="card m-b-30">               
                <div class="card-body">
                    <h4 class="card-title font-16 mt-0">{{ $part->part }}</h4>
                    <p class="card-text">
                        <span class="d-block">
                            Last updated {{ $part->updated_at->diffForHumans() }}
                        </span>
                        <span class="d-block">
                            added  {{ $part->created_at->diffForHumans() }}
                        </span>
                    </p>
                    @if ($part->deletable())
                        <button class="btn btn-primary waves-effect waves-light m-1"
                            data-url="{{ route('delete.machine.part', $part->id) }}"
                            data-name="{{ $part->part }}"
                            onclick="deleteMachinePart(this)"
                        >
                            Delete
                        </button>
                    @endif
                    <a href="{{ route('edit.machine.part', $part->id) }}" class="btn btn-warning waves-effect waves-light m-1">
                        Edit
                    </a>
                </div>
            </div>  --}}
        </div>
        <div class="col-md-8">
            {{--  description  --}}
            @component('components.card')
                @slot('card_header')
                    <h5 class="card-title">
                       Part Description
                    </h5>
                @endslot
                @slot('card_body')
                    @if(strlen(strip_tags($part->description)))
                        {!! $part->description !!}
                    @else    
                        <div class="text-center my-3">
                            <h6>No description found</h6>
                        </div>                    
                    @endif
                @endslot
            @endcomponent

            {{--  technical specifications  --}}
            @component('components.card')
                @slot('card_header')
                    <h5 class="card-title">
                       Technical Specifications
                    </h5>
                @endslot
                @slot('card_body')
                    @if(strlen(strip_tags($part->technical_specifications)))
                        {!! $part->technical_specifications !!}
                    @else    
                        <div class="text-center my-3">
                            <h6>Not available</h6>
                        </div>                    
                    @endif
                @endslot
            @endcomponent

            {{--  documentation  --}}
            @component('components.card')
                @slot('card_header')
                    <h5 class="card-title">
                       Documentations
                    </h5>
                @endslot
                @slot('card_body')
                    @if(strlen(strip_tags($part->documentation)))
                        {!! $part->documentation !!}
                    @else    
                        <div class="text-center my-3">
                            <h6>Not available</h6>
                        </div>                    
                    @endif
                @endslot
            @endcomponent


        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/admin/machine.parts.js') }}"></script>
    <script>
        $(document).ready(function() {
            @if (session('updated'))
                Swal.fire(
                    'Machine Part Updated',
                    '{{ session('updated') }}',
                    'success'
                );
            @endif

            @if (session('created'))
                Swal.fire(
                    'Machine Part Created',
                    '{{ session('created') }}',
                    'success'
                );
            @endif
        })
    </script>
@endpush