@php
    use App\Helpers\UI;
@endphp

@extends('layouts.admin.app')
@section('title', $order->ref.' | My Orders')

@section('page', 'Machines | Parts | My Orders | '.$order->ref)

@push('css')
<style>
    .card_image {
        width: 100%;
        height: 235px;
        max-height: 235px;
    }
</style>
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('all.machines') }}">Machines</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('all.machine.parts') }}">Parts</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('show.my.machine.part.orders') }}">My Orders</a>
    </li>
    <li class="breadcrumb-item active">
        {{ $order->ref }}
    </li>
@endsection

@section('content')
    <div class="row">
       <div class="col-md-4">
           @component('components.card')
                @slot('card_body')
                    <h5>Order Info</h5>
                    <hr>
                    <strong class="text-primary">Reference</strong>
                    <p>{{ $order->ref }}</p>
                    <hr>
                    <strong class="text-primary">Current Status</strong>
                    <p>{!! UI::orderStatusBadge($order->status) !!}</p>
                    <hr>
                    <strong class="text-primary">Machine Parts</strong>
                    <p>{{ number_format($order->items->count()) }}</p>
                    <hr>
                    {{-- <strong class="text-primary">Parts on Cash and Charge</strong>
                    <p>{{ number_format($order->cac_items->count()) }}</p>
                    <hr> --}}
                    <strong class="text-primary">Ordered</strong>
                    <p>
                        {{ $order->created_at->format('jS M, Y') }}
                    </p>
                @endslot
           @endcomponent
       </div>
       <div class="col-md-8">
           {{-- ordered items --}}
           @component('components.card')
               @slot('card_header')
                   <b>Ordered Machine Parts</b>
               @endslot
               @slot('card_body')
                    <div>
                        <table class="table table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>Machine Parts</th>
                                    <th>Quantity</th>
                                    @if ($order->has_cac_items())                                        
                                        <th></th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($order->items as $item)
                                    <tr>
                                        <td>
                                            {{ $item->machine_part->part }}
                                            @if ($item->cac_reason)
                                                <span class="badge badge-primary ml-1">CAC</span>
                                            @endif
                                        </td>
                                        <td>{{ $item->quantity }}</td>
                                        @if ($item->cac_reason)
                                            <td>
                                                @if ($item->images->count())                                                    
                                                    <div class="btn-group btn-group-sm">
                                                        <button type="button" class="btn btn-sm btn-primary waves-effect waves-light dropdown-toggle"
                                                            data-toggle="dropdown">
                                                            <span class="badge badge-light mr-1">
                                                                {{ $item->images->count() }}
                                                            </span>
                                                            <span class="mdi mdi-image"></span>
                                                        </button>
                                                        <div class="dropdown-menu">
                                                            @foreach ($item->images as $key => $image)
                                                            <a class="dropdown-item" href="{{ $image->link() }}" target="_blank">
                                                                image {{ $key + 1 }}
                                                            </a>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                @endif
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
               @endslot
           @endcomponent

           {{-- ticket info --}}
           @component('components.card')
               @slot('card_header')
                   <b>Ticket Info</b>
               @endslot
               @slot('card_body')
                   <div>
                       <table class="table table-borderless">
                           <thead>
                                <th width="160"></th>
                                <th></th>
                           </thead>
                           <tbody>
                                <tr>
                                    <td><b class="text-primary">Ticket Reference</b></td>
                                    <td>
                                        {{ $order->ticket->ref }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b class="text-primary">Terminal</b>
                                    </td>
                                    <td>
                                        {{ $order->ticket->machine->terminal_id }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b class="text-primary">Terminal Faults</b>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-sm">
                                            <button type="button" class="btn btn-sm btn-primary waves-effect waves-light dropdown-toggle"
                                                data-toggle="dropdown">
                                                <span class="badge badge-light mr-1">
                                                    {{ $order->ticket->faults->count() }}
                                                </span>
                                                <span class="mdi mdi-image"></span>
                                            </button>
                                            <div class="dropdown-menu">
                                                @foreach ($order->ticket->faults as $fault)
                                                <a class="dropdown-item">
                                                    {{ $fault->fault->fault }}
                                                </a>
                                                @endforeach
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                           </tbody>
                       </table>

                       @can('view',$order->ticket)
                           <div class="text-center">
                               <a href="{{ route('show.ticket', $order->ticket->id) }}">See More</a>
                           </div>
                       @endcan
                   </div>
               @endslot
           @endcomponent
       </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/admin/machine.parts.js') }}"></script>
    <script src="{{ asset('js/admin/machine.cart.js') }}"></script>
    <script>
        $(document).ready(function() {
            @if (session('checked_out'))
                Swal.fire(
                    'Checked Out',
                    '{{ session('checked_out') }}',
                    'success'
                )
            @endif

            @if (session('updated'))
                Swal.fire(
                    'Cart Item Updated',
                    '{{ session('updated') }}',
                    'success'
                )
            @endif
        });
    </script>
@endpush