@extends('layouts.admin.app')
@section('title', 'Machines | Parts | Cart')

@section('page', 'Machines | Parts | Cart')

@push('css')
    <link rel="stylesheet" href="{{ asset('css/bootstrap-touchspin.min.css') }}">
    <style>
        input[type=number]::-webkit-inner-spin-button,
        input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
        }
    </style>
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('all.machines') }}">Machines</a>
    </link>
    <li class="breadcrumb-item ">
        <a href="{{ route('all.machine.parts') }}">Parts</a>
    </li>
    <li class="breadcrumb-item active">
        Cart
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            @if ($user->cart->count())
                <div class="text-right mb-2">
                    <a class="btn btn-primary waves-effect" href="{{ route('all.machine.parts') }}">
                        Parts Hub
                    </a>
                </div>
            @endif

            @if (session('updated'))
                @component('components.alert', [
                    'alert_dismiss'=> true,
                    'text' => session('updated'),
                    'alert_class' => 'alert-success'
                    ])
                @endcomponent
            @elseif (session('deleted'))
                @component('components.alert', [
                    'alert_dismiss'=> true,
                    'text' => session('deleted'),
                    'alert_class' => 'alert-success'
                    ])
                @endcomponent
            @endif
            @if ($user->cart->count())     
                       
                @component('components.card')
                    @slot('card_header')
                        <h5 class="card-title">
                            Cart
                        </h5>
                    @endslot
                    @slot('card_body')
                        <div class="table-responsive-sm">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Machine Part</th>
                                        @if ($user->cart_has_cac())                                            
                                            <th width="140"></th>
                                        @endif
                                        <th>Quantity</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($user->cart as $cart)
                                        <tr>
                                            <td>
                                                {{ $cart->machine_part->part }}
                                                @if ($cart->cac_reason)
                                                <span class="badge badge-primary" title="Cash and Charge">
                                                    CAC
                                                </span>
                                                @endif
                                            </td>
                                            @if ($user->cart_has_cac())
                                                <td>
                                                    @if ($cart->images->count())
                                                        <div class="btn-group btn-group-sm">
                                                            <button type="button" 
                                                                class="btn btn-sm btn-primary waves-effect waves-light dropdown-toggle"
                                                                data-toggle="dropdown"
                                                            >
                                                                <span class="badge badge-light mr-1">
                                                                    {{ $cart->images->count() }}
                                                                </span>
                                                                <span class="mdi mdi-image"></span>
                                                            </button>
                                                            <div class="dropdown-menu">
                                                                @foreach ($cart->images as $key => $image)
                                                                    <a class="dropdown-item" 
                                                                        href="{{ $image->link() }}"
                                                                        target="_blank"
                                                                    >
                                                                        image {{ $key + 1 }}
                                                                    </a>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                        {{-- @foreach ($cart->images as $key => $image)
                                                            <a href="{{ $image->link() }}" target="_blank" class="badge badge-light mr-1">
                                                                <i class="mdi mdi-image"></i>
                                                                Image {{ $key+1 }}
                                                            </a>
                                                        @endforeach --}}
                                                    @endif
                                                </td>
                                            @endif
                                            <td>
                                                {{ number_format($cart->quantity) }}
                                            </td>
                                            <td>
                                                <button class="btn btn-sm btn-warning update_cart_item_btn" title="Update"
                                                    data-toggle="tooltip"
                                                    data-url="{{ route('update.machine.parts.cart.item', $cart->id) }}"
                                                    data-name="{{ $cart->machine_part->part }}"
                                                    data-quantity = "{{ $cart->quantity }}"
                                                    data-cac="{{ $cart->cac_reason ?? null }}"
                                                >
                                                    <i class="fa fa-pencil-alt"></i>
                                                </button>
                                                <button class="btn btn-sm btn-primary remove_cart_item_btn" title="Drop"
                                                    data-toggle="tooltip"
                                                    data-url="{{ route('remove.from.machine.parts.cart', $cart->id) }}"
                                                    data-name="{{ $cart->machine_part->part }}"
                                                >
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td></td>
                                        @if ($user->cart_has_cac())
                                            <td></td>
                                        @endif
                                        <td align="right">
                                            <b>Total Quantity:</b>
                                        </td>
                                        <td>
                                            {{ number_format($user->cart_sum()) }}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="my-2 text-right">
                                @can('order', App\MachinePart::class)                                    
                                <button class="btn btn-primary checkout_btn">
                                    Checkout
                                </button>
                                @endcan
                            </div>
                        </div>
                    @endslot
                @endcomponent
            @else
                 @component('components.empty')
                    @slot('text')
                        No item found in cart!
                    @endslot
                    @slot('body')
                        <a href="{{ route('all.machine.parts') }}" 
                            class="btn btn-primary btn-lg waves-effect waves-light"
                        >
                            Explore Machine Parts
                        </a>
                    @endslot
                @endcomponent
            @endif
        </div>
    </div>

    {{--  modals  --}}
    @includeWhen($user->cart->count(), 'admin.machines.cart.update')
    @includeWhen($user->cart->count(), 'admin.machines.cart.checkout')
@endsection

@push('js')
    <script src="{{ asset('js/bootstrap-touchspin.min.js') }}"></script>
    <script src="{{ asset('js/admin/machine.parts.js') }}"></script>
    <script src="{{ asset('js/admin/machine.cart.js') }}"></script>
    <script>
        $(document).ready(function() {

            $('.quantity-target').TouchSpin({
                buttondown_class:"btn btn-primary",
                buttonup_class:"btn btn-primary",
                step:1,
                min: 1
            });

            $('.select-2').select2();
        });
    </script>
@endpush