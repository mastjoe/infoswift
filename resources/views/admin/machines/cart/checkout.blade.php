{{--  add machine part image  --}}
@if ($user->all_state_machine_tickets_with_status('open')->count())
    {{-- if open tickets --}}
    <form method="POST" action="{{ route('checkout.machine.parts.cart') }}" id="cart_checkout_form" >
        @component('components.modal', [
            'modal_dialog_class' => 'modal-dialog-centered animated zoomIn',
            'modal_id' => 'cart_checkout_modal',
            'modal_top_dismiss' => true,
        ])
            @slot('modal_header')
                <h5>
                    <i class="mdi mdi-cart text-primary"></i>
                    Checkout
                </h5>
            @endslot

            @slot('modal_body')
                @csrf
                <div class="form_response"></div>

                {{-- ordered parts --}}
                @component('components.form.select-form-group', [
                    'label' => 'Parts',
                    'name' => 'parts',
                    'input_class' => 'select-2',
                    'props' => ['multiple' => true],
                    'disabled' => true
                ])
                    @slot('options')
                        @foreach ($user->cart as $cart)
                            <option selected >{{ $cart->quantity}} X {{  $cart->machine_part->part }}</option>
                        @endforeach
                    @endslot
                @endcomponent

                {{--  state open ticket  --}}
                @component('components.form.select-form-group', [
                    'label' => 'Ticket',
                    'name' => 'ticket',
                    'required' => true,
                ])
                    @slot('options')
                        <option>Choose an open ticket</option>
                        @foreach ($user->all_state_machine_tickets_with_status('open') as $ticket)
                            <option value="{{ $ticket->id }}">{{ $ticket->ref }}</option>
                        @endforeach
                    @endslot
                @endcomponent

                {{--  order comment  --}}
                @component('components.form.textarea-form-group', [
                    'label' => 'Comment',
                    'name' => 'comment',
                    'placeholder' => 'comment relevant to this order (optional)'
                ])
                @endcomponent


            @endslot

            @slot('modal_footer')
                <button class="btn btn-default" type="button" data-dismiss="modal">
                    Exit
                </button>
                <button class="btn btn-primary" type="submit">
                    Confirm Checkout
                </button>
            @endslot
        @endcomponent
    </form>
@else
    {{-- no open tickets --}}
    <div class="modal" id="cart_checkout_modal" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered animated zoomInUp">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <i class="mdi mdi-alert mr-2 text-primary"></i>
                        Can't Checkout
                    </h5>
                    <button class="close" type="button" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="text-center my-3">
                        <h5 class="text-muted">
                            You currently have no open ticket to checkout!
                        </h5>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary px-4" data-dismiss="modal">
                        exit
                    </button>
                </div>
            </div>
        </div>
    </div>
@endif