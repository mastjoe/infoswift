{{--  add machine part image  --}}
<form method="POST" >
    @component('components.modal', [
        'modal_dialog_class' => 'modal-dialog-centered animated zoomIn',
        'modal_id' => 'update_cart_item_modal',
        'modal_top_dismiss' => true,
    ])
        @slot('modal_header')
            <h5>
                <i class="mdi mdi-edit ml-2 text-primary"></i>
                Update Cart Item
            </h5>
        @endslot

        @slot('modal_body')
            @csrf
            <div class="form_response"></div>

            {{--  cart item  --}}
            @component('components.form.input-form-group', [
                'type' => 'item',
                'label' => 'Item',
                'input_class' => 'name-target',
                'name' => 'item',
                'placeholder' => 'fault here',
                'props' => ['disabled' => 'disabled'],
            ])
            @endcomponent

            {{--  cart quantity  --}}
            @component('components.form.input-form-group', [
                'type' => 'number',
                'input_class' => 'quantity-target floating-label',
                'label' => 'Quantity',
                'name' => 'quantity',
            ])
            @endcomponent

            @component('components.form.textarea-form-group',[
                'form_group_class'=> 'd-none',
                'name' => 'cac_reason',
                'placeholder' => 'reason for cash and charge',
                'label' => 'Cash and Charge Reason',
            ])
            @endcomponent

        @endslot

        @slot('modal_footer')
            <button class="btn btn-default" type="button" data-dismiss="modal">
                Exit
            </button>
            <button class="btn btn-primary" type="submit">
                Update
            </button>
        @endslot
    @endcomponent
</form>