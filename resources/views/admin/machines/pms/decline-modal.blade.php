<form method="POST" action="{{ route('decline.machine.pms', $pm->id) }}">
     @component('components.modal', [
            'modal_dialog_class' => 'modal-dialog-centered animated zoomIn',
            'modal_id' => 'decline_pm_modal',
            'modal_top_dismiss' => true,
        ])
            @slot('modal_header')
                <h5 class="modal-title">
                    <i class="mdi mdi-cancel text-primary"></i>
                    Decline PM Report
                </h5>
            @endslot

            @slot('modal_body')
                @csrf
                <div class="form_response"></div>
                {{--  Note  --}}
                @component('components.form.textarea-form-group', [
                    'label' => 'Declination Remark',
                    'name' => 'remark',
                    'placeholder' => 'why report was declined or what needs to be done (optional)'
                ])
                @endcomponent
            @endslot

            @slot('modal_footer')
                <button class="btn btn-default" type="button" data-dismiss="modal">
                    Exit
                </button>
                <button class="btn btn-primary" type="submit">
                    Confirm
                </button>
            @endslot
        @endcomponent
</form>