<form method="POST" action="{{ route('accept.machine.pms', $pm->id) }}">
     @component('components.modal', [
            'modal_dialog_class' => 'modal-dialog-centered animated zoomIn',
            'modal_id' => 'accept_pm_modal',
            'modal_top_dismiss' => true,
        ])
            @slot('modal_header')
                <h5 class="modal-title">
                    <i class="mdi mdi-check-circle-outline text-primary"></i>
                    Accept PM Report
                </h5>
            @endslot

            @slot('modal_body')
                @csrf
                <div class="form_response"></div>
                {{--  Note  --}}
                @component('components.form.textarea-form-group', [
                    'label' => 'Confirmation Remark',
                    'name' => 'remark',
                    'placeholder' => 'brief confirmation remark on maintenance report (optional)'
                ])
                @endcomponent

                <div class="form-group star--group">
                    <label class="d-block">Kindly rate report</label>
                    <span class="fa fa-star star" data-index="1"></span>
                    <span class="fa fa-star star" data-index="2"></span>
                    <span class="fa fa-star star" data-index="3"></span>
                    <span class="fa fa-star star" data-index="4"></span>
                    <span class="fa fa-star star" data-index="5"></span>
                    <input type="hidden" class="star--input" name="rating">
                </div>
            @endslot

            @slot('modal_footer')
                <button class="btn btn-default" type="button" data-dismiss="modal">
                    Exit
                </button>
                <button class="btn btn-primary" type="submit">
                    Confirm
                </button>
            @endslot
        @endcomponent
</form>