@php
    use App\Helpers\Util;
    use App\Helpers\UI;

@endphp

@extends('layouts.admin.app')
@section('title', 'Machines | PMs')

@section('page', 'Machines | PMs')

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('all.machines') }}">Machines</a>
    </li>
    <li class="breadcrumb-item active">
        PMs
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            @if ($pms->count())
                @component('components.card')
                    @slot('card_body')
                        <h4 class="card-title">
                            PM Records
                        </h4>
                        <hr>
                        <div class="table-responsive-sm">
                            <table class="table table-bordered table-hover pms-table">
                                <thead>
                                    <tr>
                                        <th>Reference</th>
                                        <th>Terminal</th>
                                        <th>Year</th>
                                        <th>Quarter</th>
                                        <th>Status</th>
                                        <th>sent By</th>
                                        <th>Reported</th>
                                        <th>Rating</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($pms as $pm)
                                        <tr>
                                            <td>
                                                <a href="{{ route('machine.pms.details',$pm->ref) }}">{{ $pm->ref }}</a>
                                            </td>
                                            <td>{{ $pm->machine->terminal_id }}</td>
                                            <td>
                                                {{ $pm->year }}
                                            </td>
                                            <td>
                                                {{ Util::numberOrdinal($pm->quarter) }}
                                            </td>
                                            <td>
                                                @if ($pm->status == "accepted")
                                                    <span class="badge badge-success">ACCEPTED</span>
                                                @elseif ($pm->status == "declined")
                                                    <span class="badge badge-danger">DECLINED</span>
                                                @else
                                                    <span class="badge badge-warning">SENT</span>
                                                @endif
                                            </td>
                                            <td>
                                                <span title="{{ $pm->doneBy->full_name() }}">
                                                    {{ $pm->doneBy->first_name }}
                                                </span>
                                            </td>
                                            <td>
                                                {{ $pm->created_at->diffForHumans() }}
                                            </td>
                                            <td>
                                                {!! UI::pmStars($pm->rating) !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endslot
                @endcomponent
            @else
                 @component('components.empty')
                    @slot('text')
                        No PM record found for any machine!
                    @endslot
                    @slot('body')
                    @endslot
                @endcomponent
            @endif
        </div>
    </div>
@endsection

@push('js')
    <script>
        $(document).ready(function() {
            simpleDataTable('.pms-table', {ordering: false});
            @if (session('deleted'))
                Swal.fire(
                    'Machine Deleted!',
                    '{{ session('deleted') }}',
                    'success'
                );
            @endif
        });
    </script>
@endpush