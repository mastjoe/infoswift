@php
    use App\Helpers\Util;
    use App\Helpers\UI;

@endphp

@extends('layouts.admin.app')
@section('title', 'Machines | PMs | '.$pm->ref)

@section('page', 'Machines | PMs | '.$pm->ref)

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('all.machines') }}">Machines</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('all.machines.pms') }}">PMs</a>
    </li>
    <li class="breadcrumb-item active">
        {{ $pm->ref }}
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-9">
            @component('components.card')
                @slot('card_body')
                    <table class="table table-sm table-borderless">
                        <thead>
                            <th width="150"></th>
                            <th></th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <b class="text-primary">Status</b>
                                </td>
                                <td>
                                    @if ($pm->status == "sent")
                                        <h5 class="badge badge-warning">Pending</h5>
                                    @elseif ($pm->status == "rejected")
                                        <h5 class="badge badge-danger">Rejected</h5>
                                    @else
                                        <h5 class="badge badge-success">Accepted</h5>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b class="text-primary">Terminal</b>
                                </td>
                                <td>
                                    {{ $pm->machine->terminal_id }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b class="text-primary">Year</b>
                                </td>
                                <td>
                                    {{ $pm->year }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b class="text-primary">Quarter</b>
                                </td>
                                <td>
                                    {{ $pm->quarter }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b class="text-primary">Duration</b>
                                </td>
                                <td>
                                     @switch($pm->quarter)
                                           @case(1)
                                               January - March
                                               @break
                                           @case(2)
                                               April - June
                                               @break
                                           @case(3)
                                               July - September
                                               @break
                                           @default
                                               October  - December
                                       @endswitch
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b class="text-primary">Sent By</b>
                                </td>
                                <td>
                                    @can('view', $pm->doneBy)
                                        <a href="{{ route('show.staff', $pm->done_by) }}">
                                            {{ $pm->doneBy->full_name() }}
                                        </a>
                                    @else
                                        {{ $pm->doneBy->full_name() }}
                                    @endcan
                                </td>
                            </tr>
                            @if ($pm->rating)
                                <tr>
                                    <td>
                                        <b class="text-primary">Rating</b>
                                    </td>
                                    <td>
                                        {!! UI::pmStars($pm->rating) !!}
                                    </td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                @endslot
            @endcomponent

            {{-- declination remark --}}
            @if ($pm->declination_remark && !$pm->confirmed_at)
                @component('components.card')
                    @slot('card_header')
                        <b>Declination Remark</b>
                    @endslot
                    @slot('card_body')
                        {{ $pm->declination_remark }}
                    @endslot
                @endcomponent
            @endif

            {{-- acceptance remark --}}
            @if ($pm->confirmation_remark)
                 @component('components.card')
                    @slot('card_header')
                        <b>Acceptance Remark</b>
                    @endslot
                    @slot('card_body')
                        {{ $pm->confirmation_remark }}
                    @endslot
                @endcomponent
            @endif
        </div>
        <div class="col-md-3">
            <a class="btn btn-warning btn-block my-2" target="_blank" href="{{ $pm->attachment->link() }}">
                Report Form
            </a>
            <button class="btn btn-primary btn-block my-2 decline_pm_btn {{ $pm->declined_at ? "disabled" : null }}"
                {{ $pm->declined_at ? "disabled" : null }}
            >
                Decline PM
            </button>
            <button class="btn btn-success btn-block my-2 accept_pm_btn {{ $pm->confirmed_at ? "disabled" : null }}"
                {{ $pm->confirmed_at ? "disabled" : null }}
            >
                Accept PM
            </button>
        </div>
    </div>

    {{-- modals --}}
    @include('admin.machines.pms.accept-modal')
    @include('admin.machines.pms.decline-modal')
@endsection

@push('js')
    <script src="{{ asset('js/admin/machine.pm.js') }}"></script>
    <script>
        $(document).ready(function() {
            simpleDataTable('.pms-table', {ordering: false});
            @if (session('pm_declined'))
                Swal.fire(
                    'PM Declined!',
                    '{{ session('pm_declined') }}',
                    'success'
                );
            @endif
            @if (session('pm_accepted'))
                Swal.fire(
                    'PM Accepted!',
                    '{{ session('pm_accepted') }}',
                    'success'
                );
            @endif

        });
    </script>
@endpush