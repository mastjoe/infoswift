<div class="row" style="margin-top:-30px">
    <div class="col-sm-12">
        <div class="page-title-box"
            style="background-image: url('{{ asset('images/terminal7a.jpg') }}'); background-position: 0 50%; background-size: cover;padding:20px 30px;">
            <div class="text-center pt-3">
                {{-- <img class="" src="{{ asset('images/atm.png') }}" alt="Generic placeholder image"> --}}
                <h4 class="text-primary">Terminal</h4>
                <h1>{{ $machine->terminal_id }}</h1>
                <h3>{{ $machine->name  }}</h3>
                @can('update', $machine)
                <a class="btn btn-sm btn-warning waves-effect" href="{{ route('edit.machine', [$machine->id]) }}">
                    <span class="d-none d-md-inline-block">Edit </span>
                    <i class="mdi mdi-square-edit-outline mr-1"></i>
                </a>
                @endcan
                @if ($machine->deletable())                    
                    @can('delete', $machine)
                    <button class="btn btn-sm btn-primary waves-effect" data-name="{{ $machine->terminal_id }}"
                        data-url="{{ route('delete.machine', $machine->id) }}" onclick="deleteMachine(this)">
                        <span class="d-none d-md-inline-block">Delete</span>
                        <i class="mdi mdi-delete"></i>
                    </button>
                    @endcan
                @endif
            </div>
        </div>
    </div>
</div>