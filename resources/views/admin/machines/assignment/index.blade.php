@extends('layouts.admin.app')
@section('title', 'Machine | Assignment')

@section('page', 'Machine Assignment')

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item active">
        Assignment
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            @if ($engineers->count())
                
            <div class="alert alert-light alert-dismissable">
                <button class="close" data-dismiss="alert"><span>&times;</span></button>
                Machine are assigned to engineers by state! Here is a break down of states, machines and engineers
            </div>
            @endif

            @if ($states->count())
                @component('components.card')
                    @slot('card_header')
                        <h3 class="card-title">
                            All States | Machines | Engineers
                        </h3>
                    @endslot
                    @slot('card_body')
                        <div class="">
                            <table class="table table-bordered" id="states_table">
                                <thead>
                                    <tr>
                                        <th>State</th>
                                        <th>Country</th>
                                        <th style="width: 15%;">Branches</th>
                                        <th style="width: 15%;">Machines</th>
                                        <th>Engineers</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($states as $state)
                                        <tr>
                                            <td>
                                                <a href="{{ route('machine.assignment.state', $state->id) }}">{{ $state->state }}</a>
                                            </td>
                                            <td>{{ $state->country->country }}</td>
                                            <td>
                                                <span class="badge badge-dark">
                                                    {{ number_format($state->branches->count()) }}
                                                </span>
                                            </td>
                                            <td>
                                                <span class="badge badge-primary">{{ number_format($state->machines->count()) }}</span>
                                            </td>
                                            <td>
                                                <button class="btn btn-sm btn-primary" data-toggle="tooltip" data-html="true"
                                                @if ($state->engineers->count())
                                                    title="
                                                        @foreach ($state->engineers as $engineer)
                                                            <div>{{ $engineer->full_name() }}</div>
                                                        @endforeach
                                                    "
                                                @else
                                                    title="No engineer"
                                                @endif
                                                >
                                                    <span class="badge badge-light">{{ number_format($state->engineers->count()) }}</span>
                                                    <i class="fa fa-user-friends"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endslot
                @endcomponent
            @else
                @component('components.empty')
                    @slot('text')
                        No state found!
                    @endslot
                    @slot('body')
                        <a href="{{ route('create.state') }}" class="btn btn-primary btn-lg waves-effect">
                            Add New State
                        </a>
                    @endslot
                @endcomponent
            @endif
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/admin/machine.assignment.js') }}"></script>
    <script>
        $(document).ready(function() {
            simpleDataTable('#states_table')
        });
    </script>
@endpush