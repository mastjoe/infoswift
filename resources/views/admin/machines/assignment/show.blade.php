@extends('layouts.admin.app')
@section('title', 'Machine | Assignment')

@section('page', 'Machine Assignment')

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('machine.assignment') }}">Assignment</a>
    </li>
    <li class="breadcrumb-item active">
        {{ $state->state }}
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="col-12 text-right mb-2">
            <button class="btn btn-dark waves-effect btn-sm assign_engineer_btn"
                title="assign engineer to state"
                data-toggle="tooltip"
            >
                Assign Engineer
            </button>

            @if ($state->machines->count())
            <a href="{{ route('machine.assignment.state.assign.form', $state->id) }}" 
                class="btn btn-primary waves-effect btn-sm"
                title="assign machines in state to engineers"
                data-toggle="tooltip"
            >
                Assign Machines
            </a>
            @endif
        </div>
        <div class="col-12">
            {{-- machines --}}
            @component('components.card')
                @slot('card_header')
                    <h3 class="card-title">
                        Machines in {{ $state->state }}
                    </h3>
                @endslot
                @slot('card_body')
                    @if ($state->machines->count())
                        <div>
                            <table class="table table-bordered" id="machines_table">
                                <thead>
                                    <tr>
                                        <th>Machine</th>
                                        <th>Branch</th>
                                        <th style="width: 15%">Branch Status</th>
                                        <th>Region</th>
                                        <th>Status</th>
                                        <th>Engineer</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($state->machines as $machine)
                                        <tr>
                                            <td>
                                                {{ $machine->terminal_id }}
                                            </td>
                                            <td>
                                                {{ $machine->branch->branch }}
                                            </td>
                                            <td>
                                                @if ($machine->branch_status == "offsite")
                                                    <span class="badge badge-warning badge-pill">
                                                        offsite
                                                    </span>
                                                @else
                                                    <span class="badge badge-primary badge-pill">
                                                        in-branch
                                                    </span>
                                                @endif
                                            </td>
                                            <td>
                                                {{ $machine->region->region }}
                                            </td>
                                            <td>
                                                @if ($machine->machineStatus->status == "active")
                                                    <span class="badge badge-success">
                                                        active
                                                    </span>
                                                @else
                                                    <span class="badge badge-secondary">
                                                        {{ $machine->machineStatus->status }}
                                                    </span>                                                    
                                                @endif
                                            </td>
                                            <td>
                                                @isset($machine->engineer_id)
                                                    {{ $machine->engineer->full_name() }}
                                                @else
                                                    -
                                                @endisset
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @else
                        <div class="text-center text-muted my-4">
                            <h2>No machine found in {{ $state->state }}</h2>
                            @can('create', App\Machine::class)
                            <a href="{{ route('create.machine') }}" class="btn btn-primary btn-lg waves-effect">
                                Add New Machine
                            </a>
                            @endcan
                        </div>
                    @endif
                @endslot
            @endcomponent

            {{-- engineers --}}
            @component('components.card')
                @slot('card_header')
                    <h3 class="card-title">
                        Engineers assigned to {{ ($state->state) }}
                    </h3>
                    <div class="card-options">
                        <button class="btn-block-option assign_engineer_btn" type="button">
                            <i class="mdi mdi-account-multiple-plus mdi-24px"></i>
                        </button>
                    </div>
                @endslot
                @slot('card_body')
                    
                    @if ($state->engineers->count())
                        <div>
                            <table class="table table-bordered" id="engineers_table">
                                <thead>
                                    <tr>
                                        <td>Engineer</td>
                                        <td>Machines Assigned</td>
                                        <td>Assigned</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($state->engineers as $engineer)
                                        <tr>
                                            <td>
                                                {{ $engineer->full_name() }}
                                            </td>
                                            <td>
                                                <span class="badge badge-primary">
                                                    {{ number_format($engineer->state_machines($state->id)->count()) }}
                                                </span>
                                            </td>
                                            <td>
                                                {{ $engineer->pivot->created_at->diffForHumans() }}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @else
                        <div class="text-center my-4 text-muted">
                            <h2>No engineer assigned to abia state</h2>
                            <button class="btn btn-primary btn-lg waves-effect assign_engineer_btn">
                                Assign Engineer
                            </button>
                        </div>
                    @endif
                @endslot
            @endcomponent
        </div>
    </div>
    {{-- modal --}}
    @include('admin.settings.locations.states.assign-engineer')
@endsection

@push('js')
    <script src="{{ asset('js/admin/machine.assignment.js') }}"></script>
    <script>
        $(document).ready(function() {
            simpleDataTable('#machines_table, #engineers_table');

            @if (session('assigned_engineers'))
                Swal.fire(
                    'Engineers Assigned',
                    '{{ session('assigned_engineers') }}',
                    'success'
                );
            @endif

        });
    </script>
@endpush