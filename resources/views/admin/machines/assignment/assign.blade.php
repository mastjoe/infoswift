@extends('layouts.admin.app')
@section('title', 'Machine | Assignment | Assign Machines')

@section('page', 'Machine Assignment | Assign Machine')

@push('css')
<style>
    .table tr.selected {
        background: #f8f8f8;
    }
</style>
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('machine.assignment') }}">Assignment</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('machine.assignment.state', $state->id) }}">{{ $state->state }}</a>
    </li>
    <li class="breadcrumb-item active">
       Assign Machines
    </li>
@endsection

@section('content')
    <form method="POST" action="{{ route('machine.assignment.state.assign', $state->id) }}">

        <div class="row">
            <div class="col-12">
                @if ($errors->count())
                    <div class="alert alert-dismissable alert-primary">
                        <button class="close" type="button" data-dismiss="alert"><span>&times;</span></button>
                        {{ $errors->first() }}
                    </div>
                @else
                    <div class="alert alert-dismissable alert-warning">
                        <button class="close" type="button" data-dismiss="alert"><span>&times;</span></button>
                        You can assign or reassign machines to an engineer within state at a time. 
                        Choose one or more machines and an engineer to carry out assignment!
                    </div>
                @endif
            </div>
            @csrf
            <div class="col-lg-7">
                @component('components.card')
                    @slot('card_header')
                        <h4 class="card-title">
                            Machines
                        </h4>
                    @endslot
                    @slot('card_body')
                        @if ($state->machines->count())
                            <div class="table-responsive-sm">
                                <table class="table table-bordered dt sel" data-table="machines">
                                    <thead>
                                        <tr>
                                            <th style="width: 4%"></th>
                                            <th>Machine</th>
                                            <th>Region</th>
                                            <th>Branch</th>
                                            <th>Engineer</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($state->machines as $machine)
                                            <tr data-id="{{ $machine->id }}">
                                                <td>
                                                    <div class="form-check form-check-inline">
                                                        <input type="checkbox" class="form-check-input" id="machine{{ $machine->id }}" value="{{ $machine->id }}" name="machines[]">
                                                        <label class="form-check-label" for="machine{{ $machine->id }}"></label>
                                                    </div>
                                                </td>
                                                <td>{{ $machine->terminal_id }}</td>
                                                <td>{{ $machine->region->region }}</td>
                                                <td>{{ $machine->branch->branch }}</td>
                                                <td>
                                                    @if ($machine->engineer)
                                                    
                                                        <button type="button" class="btn btn-sm btn-primary" data-toggle="tooltip" title="{{ $machine->engineer->full_name() }}">
                                                            <i class="fa fa-user"></i>
                                                        </button>
                                                    @else
                                                        <button type="button" class="btn btn-sm btn-primary" disabled>
                                                            <i class="fa fa-user"></i>
                                                        </button>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="machine-action d-none">
                                    <hr>
                                    <div>
                                        <button class="btn btn-dark" type="button" onclick="dropAllChecked(event)">
                                            Drop All
                                        </button>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="text-center text-muted">
                                <h2>No machine found</h2>
                            </div>
                        @endif
                    @endslot
                @endcomponent
            </div>
            <div class="col-lg-5">
                @component('components.card')
                    @slot('card_header')
                        <h4 class="card-title">
                            Engineers
                        </h4>
                    @endslot
                    @slot('card_body')
                        <div>
                            <table class="table table-bordered mt-3 sel" data-table="engineers">
                                <thead>
                                    <tr>
                                        <th style="width: 4%"></th>
                                        <th>Engineer</th>
                                        <th>Machines</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($state->engineers as $engineer)
                                        <tr data-id="{{ $engineer->id }}">
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input name="engineer" type="radio" class="with-gap form-check-input" value="{{ $engineer->id }}" id="radio{{ $engineer->id }}">
                                                    <label for="radio{{ $engineer->id }}"></label>
                                                </div>
                                            </td>
                                            <td>
                                                {{ $engineer->full_name() }}
                                            </td>
                                            <td>
                                                <span class="badge badge-primary">
                                                    {{ number_format($engineer->machines->count()) }}
                                                </span>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="engineer-action d-none">
                                <hr>
                                <div>
                                    <button class="btn btn-dark" type="button" onclick="dropAllChecked(event)">
                                        Drop
                                    </button>
                                </div>
                            </div>
                        </div>
                    @endslot
                @endcomponent
            </div>
            <div class="col-12 text-center my-2">
                <button class="btn btn-primary waves-effect btn-lg" type="submit">
                    Assign Engineer
                </button>
            </div>
        </div>
    </form>
@endsection

@push('js')
    <script src="{{ asset('js/admin/machine.assignment.js') }}"></script>
    <script>
        $(document).ready(function() {
            simpleDataTable('.dt');
            selectInteraction();

            @if (session('machines_assigned'))
                Swal.fire(
                    'Machines Assigned',
                    '{{ session('machines_assigned') }}',
                    'success'
                );
            @endif
        });
    </script>
@endpush