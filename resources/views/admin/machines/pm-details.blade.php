@php
    use App\Helpers\Util;

    $order = Util::numberOrdinal($pm->quarter);
@endphp
@extends('layouts.admin.app')
@section('title', ' Quarter - PM Report | '.$machine->terminal_id)

@section('page', 'Machine | Terminal '.$machine->terminal_id. ' | '.$year.' - PM | '.ucfirst($order).' Quarter')

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('all.machines') }}">Machines</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('show.machine', $machine->id) }}">{{ $machine->terminal_id }}</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('show.machine.pms', $machine->id) }}">PM</a>
    </li>
    <li class="breadcrumb-item active">
        {{ ucfirst($order) }} Quarter
    </li>
@endsection

@section('content')
    @include('admin.machines.hero')

    <div class="row mt-0">
        <div class="col-md-9 order-12 order-md-1">
            <div class="row">
                <div class="col-md-8 col-12">
                    <h4 class="text-primary mb-0">{{ ucfirst($order) }} Quarter Report</h4>
                </div>
                <div class="col-md-4 col-12 text-right">
                    @if ($pm->editable())                        
                        <button class="btn btn-warning waves-effect edit_pm_btn"
                            data-url="{{ route('edit.machine.pm', $pm->id) }}"
                        >
                            Edit
                        </button>
                    @endif
                    @if ($pm->deletable())                        
                        <button class="btn btn-primary waves-effect delete_pm_btn"
                            data-url="{{ route('delete.machine.pm', [$pm->id]) }}"
                            data-year="{{ $pm->year }}"
                            data-quarter="{{ $pm->quarter }}"
                        >
                            Delete
                        </button>
                    @endif
                </div>
            </div>
            <hr>
            @component('components.card')
                @slot('card_body')
                    <div>
                        <table class="table table-sm table-borderless">
                            <thead>
                                <th width="150"></th>
                                <th></th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <b class="text-primary">
                                            Status
                                        </b>
                                    </td>
                                    <td>
                                        @if ($pm->status == "sent")
                                           <h5 class="badge badge-warning">Pending</h5>
                                        @elseif ($pm->status == "rejected")
                                           <h5 class="badge badge-danger">Rejected</h5>
                                        @else
                                            <h5 class="badge badge-success">Accepted</h5>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b class="text-primary">Reference</b>
                                    </td>
                                    <td>{{ $pm->ref }}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <b class="text-primary">
                                            Machine
                                        </b>
                                    </td>
                                    <td>
                                        {{ $machine->terminal_id }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b class="text-primary">
                                            Year
                                        </b>
                                    </td>
                                    <td>
                                        {{ $pm->year }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b class="text-primary">
                                            Quarter
                                        </b>
                                    </td>
                                    <td>
                                        {{ $order }} Quarter
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b class="text-primary">
                                            Duration
                                        </b>
                                    </td>
                                    <td>
                                       @switch($pm->quarter)
                                           @case(1)
                                               January - March
                                               @break
                                           @case(2)
                                               April - June
                                               @break
                                           @case(3)
                                               July - September
                                               @break
                                           @default
                                               October  - December
                                       @endswitch
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b class="text-primary">
                                            Note
                                        </b>
                                    </td>
                                    <td>
                                        {{ $pm->note ?? "Note not available" }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b class="text-primary">
                                            Submitted
                                        </b>
                                    </td>
                                    <td>
                                        {{ $pm->created_at->format('jS M, Y') }}
                                        <span class="text-muted ml-3">
                                            {{ $pm->created_at->diffForHumans() }}
                                        </span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                @endslot
            @endcomponent

            {{-- form --}}
            @if ($pm->attachment)                
                @component('components.card')
                    @slot('card_header')
                        <b>Report Form</b>
                    @endslot
                    @slot('card_body')
                        <div class="text-center">
                            <a class="btn btn-primary btn-lg"
                                href="{{ $pm->attachment->link() }}"
                                title="{{ $pm->attachment->name }}"
                                target="_blank"
                            >
                                Download Report Form
                                <span class="mdi mdi-download"></span>
                            </a>
                        </div>
                    @endslot
                @endcomponent
            @endif

            {{-- acceptance --}}
            @if ($pm->confirmation_remark)                
                @component('components.card')
                    @slot('card_header')
                        <b>Accetance Remark</b>
                    @endslot
                    @slot('card_body')
                        {{ $pm->confirmation_remark }}
                    @endslot
                @endcomponent
            @endif

            {{-- declination remark --}}
            @if (!$pm->confirmation_remark && $pm->declination_remark)
                 @component('components.card')
                    @slot('card_header')
                        <b>Declination Remark</b>
                    @endslot
                    @slot('card_body')
                        {{ $pm->declination_remark }}
                    @endslot
                @endcomponent
            @endif
        </div>
        <div class="col-md-3 order-1 order-md-12 my-2">
           @include('admin.machines.nav')
        </div>
    </div>

@endsection

@push('js')
    <script src="{{ asset('js/admin/machine.js') }}"></script>
    <script src="{{ asset('js/admin/machine.pm.js') }}"></script>
    <script>
        $(document).ready(function() {
            @if (session('pm_report'))
                Swal.fire('PM Report Sent', '{{ session('pm_report') }}', 'success');
            @endif
            @if (session('pm_updated'))
                Swal.fire('PM Report Updated', '{{ session('pm_updated') }}', 'success');
            @endif
        });
    </script>
@endpush