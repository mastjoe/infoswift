@extends('layouts.admin.app')
@section('title', 'Machines')

@section('page', 'All Machines')

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item active">
        Machines
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            @if ($machines->count())
                <div class="text-right mb-2">
                    @can('create', App\Machine::class)                        
                        <a href="{{ route('create.machine') }}" class="btn btn-primary waves-effect">
                            <span class="d-none d-md-inline-block">New Machine</span>
                            <i class="mdi mdi-plus-circle-outline mr-1"></i>
                        </a>
                    @endcan
                </div>
                @component('components.card')
                    @slot('card_body')
                        <h4 class="card-title">
                            All Machines
                        </h4>
                        <hr>
                        <div class="table-responsive-sm">
                            <table class="table table-bordered machines_table">
                                <thead>
                                    <tr>
                                        <th>Terminal Id</th>
                                        <th>Client</th>
                                        <th>Branch</th>
                                        <th>Region</th>
                                        <th>State</th>
                                        <th>Engineer</th>
                                        <th>Tickets</th>
                                        <th>PMs</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($machines as $machine)
                                        <tr>
                                            <td>
                                                <a class="font-weight-bold" href="{{ route('show.machine', $machine->id) }}">
                                                    {{ $machine->terminal_id }}
                                                </a>
                                            </td>
                                            <td>
                                            <span class="font-weight-bold" style="color:{{ $machine->client->color }}">
                                                    {{ $machine->client->short_name }}
                                                </span>
                                            </td>
                                            <td>
                                                {{ $machine->branch->branch }}
                                            </td>
                                            <td>
                                                {{ $machine->region->region }}
                                            </td>
                                            <td>
                                                <span title="{{ $machine->state->state.', '.$machine->country->country }}" data-toggle="tooltip">
                                                    {{ $machine->state->state }}
                                                </span>
                                            </td>
                                            <td>
                                                @if ($machine->engineer) 
                                                    <span title="{{ $machine->engineer->full_name() }}" data-toggle="tooltip">
                                                        {{ $machine->engineer->first_name }}
                                                    </span>
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td>
                                                {{ number_format($machine->tickets->count()) }}
                                            </td>
                                            <td>
                                                {{ number_format($machine->pms->count()) }}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endslot
                @endcomponent
            @else
                 @component('components.empty')
                    @slot('text')
                        No machine  found!
                    @endslot
                    @slot('body')
                        @can('create', App\Machine::class)                            
                            <a href="{{ route('create.machine') }}" 
                                class="btn btn-primary btn-lg waves-effect"
                            >
                                Add New Machine
                            </a>
                        @endcan
                    @endslot
                @endcomponent
            @endif
        </div>
    </div>
@endsection

@push('js')
    <script>
        $(document).ready(function() {
            simpleDataTable('.machines_table');
            @if (session('deleted'))
                Swal.fire(
                    'Machine Deleted!',
                    '{{ session('deleted') }}',
                    'success'
                );
            @endif
        });
    </script>
@endpush