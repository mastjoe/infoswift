@php
    use App\Helpers\UI;
@endphp

@extends('layouts.admin.app')
@section('title', 'Dispatch History -  '.$order->ref)

@section('page', 'Dispatch History - '.$order->ref)

@push('css')
<style>
</style>
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('all.machine.parts') }}">Parts</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('all.machine.parts.order') }}">All Orders</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('show.machine.parts.order', $order->ref) }}">{{ $order->ref }}</a>
    </li>
    <li class="breadcrumb-item active">
        Dispatches
    </li>

@endsection

@section('content')
    <div class="row">
        <div class="col-md-3 order-md-2 order-1">
           @include('admin.machines.orders.nav')
        </div>
        <div class="col-md-9 order-md-1 order-2">
            @if ($order->dispatches->count())
                @component('components.card')
                    @slot('card_header')
                        <b>Dispatch Sessions</b>
                        <div class="card-options">
                            <button class="btn waves-effect btn-primary btn-block-option create_dispatch_btn"
                                data-url="{{ route('create.machine.parts.order.dispatch', $order->ref) }}"
                            >
                                New Session
                            </button>
                        </div>
                    @endslot
                    @slot('card_body')
                        <div>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Reference</th>
                                        <th>Dispatch Items</th>
                                        <th>Status</th>
                                        <th>Created</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($order->dispatches as $dispatch)
                                        <tr>
                                            <td>
                                                <a href="{{ route('show.machine.parts.order.dispatch', $dispatch->ref) }}">{{ $dispatch->ref }}</a>
                                            </td>
                                            <td>
                                                <span class="badge badge-secondary">
                                                    {{ number_format($dispatch->items->count()) }}
                                                </span>
                                            </td>
                                            <td>
                                                {!! UI::dispatchStatusBadge($dispatch->status) !!}
                                            </td>
                                            <td>
                                                {{ $dispatch->created_at->diffForHumans() }}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endslot
                @endcomponent
            @else
                <div class="text-center my-5">
                    <h2 class="text-muted mt-3">
                        No dispatch record found for order
                    </h2>
                    <button class="btn btn-primary btn-lg waves-light waves-effect my-2 create_dispatch_btn"
                        data-url="{{ route('create.machine.parts.order.dispatch', $order->ref) }}"
                    >
                        Create a Dispatch Session
                    </button>
                </div>
            @endif
        </div>
    </div>

    {{--  modal  --}}
@endsection

@push('js')
    <script src="{{ asset('js/admin/machine.parts.js') }}"></script>
    <script src="{{ asset('js/admin/machine.parts.order.js') }}"></script>
    <script>
        $(document).ready(function() {
            @if (session('deleted'))
                Swal.fire(
                    'Cart Item Deleted',
                    '{{ session('deleted') }}',
                    'success'
                )
            @endif

            @if (session('updated'))
                Swal.fire(
                    'Cart Item Updated',
                    '{{ session('updated') }}',
                    'success'
                )
            @endif

            @if (session('dispatch_deleted'))
                Swal.fire(
                    'Dispatch Session Deleted',
                    '{{ session('dispatch_deleted') }}',
                    'success'
                );
            @endif

            simpleDataTable('.order_table')
        });
    </script>
@endpush