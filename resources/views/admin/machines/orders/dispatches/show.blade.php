@extends('layouts.admin.app')
@section('title', 'Dispatch  -  '.$dispatch->ref)

@section('page', 'Dispatch  - '.$dispatch->ref)

@push('css')
<style>
</style>
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('all.machine.parts') }}">Parts</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('all.machine.parts.order') }}">All Orders</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('show.machine.parts.order', $order->ref) }}">{{ $order->ref }}</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('show.machine.parts.order.dispatches', $order->ref) }}">Dispatches</a>
    </li>
    <li class="breadcrumb-item active">
        {{ $dispatch->ref }}
    </li>

@endsection

@section('content')
    <div class="row">
        <div class="col-md-9">
            {{--  dispatch info  --}}
            @component('components.card')
                @slot('card_header')
                    <b>Dispatch Details</b>
                @endslot
                @slot('card_body')
                    <table class="table table-borderless">
                        <thead>
                            <tr class="">
                                <th width="150" class="p-0 mt-0"></th>
                                <th class="p-0 mt-0"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><b class="text-primary">Reference</b></td>
                                <td>{{ $dispatch->ref }}</td>
                            </tr>
                            <tr>
                                <td><b class="text-primary">Created</b></td>
                                <td>{{ $dispatch->created_at->format('jS M, Y | h:i a') }}</td>
                            </tr>
                            <tr>
                                <td><b class="text-primary">Created By</b></td>
                                <td>{{ $dispatch->creator->full_name }}</td>
                            </tr>
                        </tbody>
                    </table>
                @endslot
            @endcomponent

            @component('components.card')
                @slot('card_body')
                    <h4>
                        Dispatch Items
                    </h4>
                    <hr>
    
                    @if ($dispatch->items->count())
                        
                    @else
                        <div class="text-center my-5">
                            <h2 class="text-muted">
                                No dispatch item found!
                            </h2>
                        </div>
                    @endif
                @endslot
            @endcomponent
        </div>
        <div class="col-md-3">
            <button class="btn btn-secondary btn-block waves-effect my-2">
                Add Item
            </button>
            <button class="btn btn-secondaryn btn-block waves-effect my-2">
            </button>
            <button class="btn btn-primary btn-block waves-effect my-2 delete_dispatch_btn"
                data-url="{{ route('delete.machine.parts.order.dispatch', $dispatch->ref) }}"
                data-session="{{ $dispatch->ref }}"
            >
                Delete Session
            </button>
        </div>
    </div> 
@endsection

@push('js')
    <script src="{{ asset('js/admin/machine.parts.js') }}"></script>
    <script src="{{ asset('js/admin/machine.parts.order.js') }}"></script>
    <script>
        $(document).ready(function() {
            @if (session('dispatch_created'))
                Swal.fire(
                    'Dispatch Session Created',
                    '{{ session('dispatch_created') }}',
                    'success'
                );
            @endif
            simpleDataTable('.order_table')
        });
    </script>
@endpush