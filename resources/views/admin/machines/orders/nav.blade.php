<a class="btn btn-secondary btn-block waves-effect waves-light mb-2"
    href="{{ route('show.machine.parts.order', $order->ref) }}"
>
    Order Details
</a>

<a class="btn btn-secondary btn-block mb-2 waves-effect waves-light"
    href="{{ route('show.machine.parts.order.cac', $order->ref) }}"
>
    Cash and Charge
</a>

<a class="btn btn-secondary btn-block waves-light waves-effect mb-2"
    href="{{ route('show.machine.parts.order.dispatches', $order->ref) }}"
>
    Dispatch History
</a>

@if ($order->can_decline())
    <button class="btn btn-primary btn-block waves-effect waves-light mb-2 decline_order_btn">
        Decline Order
    </button> 
@endif

{{--  modal  --}}
@includeWhen($order->can_decline(), 'admin.machines.orders.decline-order-modal')
