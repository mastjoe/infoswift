@extends('layouts.admin.app')
@section('title', 'Cash and Charge Items -  '.$order->ref)

@section('page', 'Cash and Charge Items - '.$order->ref)

@push('css')
<style>
</style>
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('all.machine.parts') }}">Parts</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('all.machine.parts.order') }}">All Orders</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('show.machine.parts.order', $order->ref) }}">{{ $order->ref }}</a>
    </li>
    <li class="breadcrumb-item active">
        CAC
    </li>

@endsection

@section('content')
    <div class="row">
        <div class="col-md-3 order-md-2 order-1">
           @include('admin.machines.orders.nav')
        </div>
        <div class="col-md-9 order-md-1 order-2">
            @if ($order->has_cac_items())
                @component('components.card')
                    @slot('card_body')
                        <h4 class="header-title">
                            Cash and Charge Order Items
                        </h4>
                        <hr>
                        <table class="table table-hover cac_table">
                            <thead>
                                <tr>
                                    <th>Machine Part</th>
                                    <th>Quantity</th>
                                    <th>status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($order->cac_items as $item)
                                    <tr>
                                        <td>
                                            {{ $item->machine_part->part }}
                                        </td>
                                        <td>
                                            {{ $item->quantity }}
                                        </td>
                                        <td>

                                        </td>
                                        <td>
                                            <div class="btn-group btn-group-sm">
                                                <button class="btn btn-primary" data-toggle="dropdown">
                                                    Actions
                                                </button>
                                                <div class="dropdown-menu">
                                                    <a class="dropdown-item order_item_details_btn" 
                                                        href="javascript:void(0)" 
                                                        data-url="{{ route('show.machine.parts.order.item-details', $item->id) }}"
                                                    >
                                                        View Details
                                                    </a>
                                                    <a class="dropdown-item notify_client_cac_btn"
                                                        href="javascript:void(0)"
                                                        data-url="{{ route('machine.parts.order.item.notify-client', $item->id) }}"
                                                    >
                                                        Notify Client
                                                    </a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @endslot
                @endcomponent
            @else
                <div class="text-center my-5">
                    <h2 class="text-muted mt-3">
                        No cash and charge item was found in order
                    </h2>
                </div>
            @endif
        </div>
    </div>

    {{--  modal  --}}
   
@endsection

@push('js')
    <script src="{{ asset('js/admin/machine.parts.js') }}"></script>
    <script src="{{ asset('js/admin/machine.cart.js') }}"></script>
    <script src="{{ asset('js/admin/machine.parts.order.js') }}"></script>
    <script>
        $(document).ready(function() {
            @if (session('deleted'))
                Swal.fire(
                    'Cart Item Deleted',
                    '{{ session('deleted') }}',
                    'success'
                )
            @endif

            @if (session('updated'))
                Swal.fire(
                    'Cart Item Updated',
                    '{{ session('updated') }}',
                    'success'
                )
            @endif

            simpleDataTable('.cac_table')
        });
    </script>
@endpush