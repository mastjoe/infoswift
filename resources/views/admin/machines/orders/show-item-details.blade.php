{{--  show item details modal  --}}
<div class="modal-header">
    <h4 class="modal-title">
        <i class="mdi mdi-information-outline ml-2 text-primary"></i>
        Order Item Details
    </h4>
</div>

<div class="modal-body">
    <table class="table table-sm table-borderless">
        <thead>
            <tr>
                <th width="150"></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><b class="text-primary">Part</b></td>
                <td>{{ $item->machine_part->part }}</td>
            </tr>
            <tr>
                <td><b class="text-primary">Quantity</b></td>
                <td>{{ $item->quantity }}</td>
            </tr>
            @if ($item->isCac())
                <tr>
                    <td colspan="2">
                        <h6 class="text-muted">Cash and Charge</h6>
                    </td>
                </tr>                
                <tr>
                    <td><b class="text-primary">Reason</b></td>
                    <td>{{ $item->cac_reason }}</td>
                </tr>
                @if ($item->images->count())                    
                    <tr>
                        <td><b class="text-primary"></b></td>
                        <td>
                            @foreach ($item->images as $image)
                                <a href="{{ $image->link() }}" target="_blank">
                                    <span class="badge badge-primary px-3 py-1">
                                        image {{ $loop->iteration }}
                                        <span class="mdi mdi-image"></span>
                                    </span>
                                </a>
                            @endforeach
                        </td>
                    </tr>
                @endif
            @endif
        </tbody>
    </table>
</div>

<div class="modal-footer">
    <button class="btn btn-default" type="button" data-dismiss="modal">
        Exit
    </button>
</div>