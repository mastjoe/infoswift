@php
    use App\Helpers\UI;
@endphp

@extends('layouts.admin.app')
@section('title', 'Machines | Parts | All Orders')

@section('page', 'Machines | Parts | All Orders')

@push('css')
<style>
</style>
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('all.machines') }}">Machines</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('all.machine.parts') }}">Parts</a>
    </li>
    <li class="breadcrumb-item active">
        All Orders
    </li>
@endsection

@section('content')
    <div class="row">
        @if ($orders->count())
            <div class="col-12">
                @component('components.card')
                    @slot('card_header')
                        <b>All Machine Part Order</b>
                    @endslot
                    @slot('card_body')
                        <div>
                            <table class="table table-hover order_table">
                                <thead>
                                    <th>Reference</th>
                                    <th>Status</th>
                                    <th>Item</th>
                                    <th>Made By</th>
                                    <th>Made On</th>
                                </thead>
                                <tbody>
                                    @foreach ($orders->sortByDesc('id') as $order)
                                        <tr>
                                            <td>
                                                <a href="{{ route('show.machine.parts.order', $order->ref) }}">{{ $order->ref }}</a>
                                            </td>
                                            <td>
                                                {!! UI::orderStatusBadge($order->status) !!}
                                            </td>
                                            <td>{{ number_format($order->items->count()) }}</td>
                                            <td>
                                               {{ $order->user->full_name() }}
                                            </td>
                                            <td>
                                                {{ $order->created_at->format('jS M, Y | h:i a') }}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endslot
                @endcomponent
            </div>
        @else
            <div class="col-12">
                 @component('components.empty')
                    @slot('text')
                        No order found!
                    @endslot
                    @slot('body')
                        @can('order', MachinePart::class)                            
                        <a href="{{ route('machine.parts.cart') }}" 
                            class="btn btn-primary btn-lg waves-effect"
                        >
                            Place an Order
                        </a>
                        @endcan
                    @endslot
                @endcomponent
            </div>
        @endif
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/admin/machine.parts.js') }}"></script>
    <script src="{{ asset('js/admin/machine.parts.order.js') }}"></script>
    <script>
        $(document).ready(function() {
            @if (session('deleted'))
                Swal.fire(
                    'Cart Item Deleted',
                    '{{ session('deleted') }}',
                    'success'
                )
            @endif

            @if (session('updated'))
                Swal.fire(
                    'Cart Item Updated',
                    '{{ session('updated') }}',
                    'success'
                )
            @endif

            @if(session('order_declined'))
                Swal.fire(
                    'Order Declined',
                    '{{ session('order_declined') }}',
                    'success'
                );
            @endif

            simpleDataTable('.order_table', {ordering: false})
        });
    </script>
@endpush