@php
    use App\Helpers\UI;
@endphp
@extends('layouts.admin.app')
@section('title', 'Machines | All Orders | '.$order->ref)

@section('page', 'Machines | All Orders | '.$order->ref)

@push('css')
<style>
</style>
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('all.machines') }}">Machines</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('all.machine.parts') }}">Parts</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('all.machine.parts.order') }}">All Orders</a>
    </li>
    <li class="breadcrumb-item active">
        {{ $order->ref }}
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3 order-md-2 order-1">
           @include('admin.machines.orders.nav')
        </div>
        <div class="col-md-9 order-md-1 order-2">
            @if ($order->status == "declined")
                <div class="alert alert-primary alert-dismissable">
                    <button class="close" type="button" data-dismiss="alert"><span>&times;</span></button>
                    Order is currently declined
                </div>
            @endif
            {{--  order info  --}}
             @component('components.card')
                @slot('card_body')
                    <table class="table table-sm table-borderless">
                        <thead>
                            <th width="150"></th>
                            <th></th>
                        </thead>
                        <tbody>
                            <tr>
                                <td><b class="text-primary">Reference</b></td>
                                <td>{{ $order->ref }}</td>
                            </tr>
                            <tr>
                                <td><b class="text-primary">Status</b></td>
                                <td>{!! UI::orderStatusBadge($order->status) !!}</td>
                            </tr>
                            <tr>
                                <td><b class="text-primary">Ordered</b></td>
                                <td>{{ $order->created_at->format('jS M, Y @ h:i a') }}</td>
                            </tr>
                            <tr>
                                <td><b class="text-primary">Ordered By</b></td>
                                <td>
                                    @can('view', $order->user)
                                        <a href="{{ route('show.staff', $order->user_id) }}">{{ $order->user->full_name() }}</a>
                                    @else
                                        {{ $order->user->full_name() }}
                                    @endcan
                                </td>
                            </tr>
                        </tbody>
                    </table>
                @endslot
            @endcomponent

            {{--  machine part ordered  --}}
            @component('components.card')
                @slot('card_header')
                    <b>Machine Parts Ordered</b>
                @endslot
                @slot('card_body')
                    @if ($order->items)
                        <div>
                            <table class="table table-borderless">
                                <thead>
                                    <th>Machine Parts</th>
                                    <th>Quantity</th>
                                    @if ($order->has_cac_items())                                        
                                        <th></th>
                                    @endif
                                    <th>Details</th>
                                </thead>
                                <tbody>
                                    @foreach ($order->items as $item)
                                        <tr>
                                            <td>
                                                {{ $item->machine_part->part }}
                                                @if ($item->cac_reason)
                                                    <span class="badge badge-primary ml-1">CAC</span>
                                                @endif
                                            </td>
                                            <td>
                                                {{ $item->quantity }}
                                            </td>
                                            @if ($item->cac_reason)
                                                <td>
                                                    @if ($item->images->count())
                                                        <div class="btn-group btn-group-sm">
                                                            <button type="button" class="btn btn-sm btn-primary waves-effect waves-light dropdown-toggle"
                                                                data-toggle="dropdown">
                                                                <span class="badge badge-light mr-1">
                                                                    {{ $item->images->count() }}
                                                                </span>
                                                                <span class="mdi mdi-image"></span>
                                                            </button>
                                                            <div class="dropdown-menu">
                                                                @foreach ($item->images as $key => $image)
                                                                <a class="dropdown-item" href="{{ $image->link() }}" target="_blank">
                                                                    image {{ $key + 1 }}
                                                                </a>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    @endif
                                                </td>
                                            @endif
                                            <td>
                                                <button class="btn btn-dark btn-sm px-2">View</button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @else
                        <div class="text-center my-4">
                            <h4 class="text-muted">No Item found!</h4>
                        </div>
                    @endif
                @endslot
            @endcomponent

            {{--  ticket information  --}}
            @component('components.card')
                @slot('card_header')
                    <b>Ticket Info</b>
                @endslot
                @slot('card_body')
                    <table class="table table-sm table-borderless">
                        <thead>
                            <th width="150"></th>
                            <th></th>
                        </thead>
                        <tbody>
                            <tr class="mt-0">
                                <td><b class="text-primary">Ticket Reference</b></td>
                                <td>
                                    @can ('view', $order->ticket)
                                        <a href="{{ route('show.ticket', $order->ticket_id) }}">{{ $order->ticket->ref }}</a>
                                    @else
                                        {{  $order->ticket->ref  }}
                                    @endcan
                                </td>
                            </tr>
                            <tr>
                                <td><b class="text-primary">Terminal</b></td>
                                <td>
                                    @can ('view', $order->ticket->machine)
                                        <a href="{{ route('show.machine', $order->ticket->machine_id) }}">{{ $order->ticket->machine->terminal_id }}</a>
                                    @else
                                        {{  $order->ticket->machine->terminal_id  }}
                                    @endcan
                                </td>
                            </tr>
                            <tr>
                                <td><b class="text-primary">Client</b></td>
                                <td>
                                    @can('view', $order->client())
                                        <a href="{{ route('show.client', $order->client()->id) }}">{{ $order->client()->name }}</a>
                                    @else
                                        {{ $order->client()->name }}
                                    @endcan
                                </td>
                            </tr>
                            <tr>
                                <td><b class="text-primary">Ticket Faults</b></td>
                                <td>
                                    <div class="btn-group btn-group-sm">
                                        <button type="button" class="btn btn-sm btn-primary waves-effect waves-light dropdown-toggle"
                                            data-toggle="dropdown">
                                            <span class="badge badge-light mr-1">
                                                {{ $order->ticket->faults->count() }}
                                            </span>
                                            <span class="mdi mdi-image"></span>
                                        </button>
                                        <div class="dropdown-menu">
                                            @foreach ($order->ticket->faults as $fault)
                                            <a class="dropdown-item">
                                                {{ $fault->fault->fault }}
                                            </a>
                                            @endforeach
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                @endslot
            @endcomponent
        </div>
    </div>

    {{--  modal  --}}
@endsection

@push('js')
    <script src="{{ asset('js/admin/machine.parts.js') }}"></script>
    <script src="{{ asset('js/admin/machine.parts.order.js') }}"></script>
    {{--  <script src="{{ asset('js/admin/machine.cart.js') }}"></script>  --}}
    <script>
        $(document).ready(function() {
            @if (session('deleted'))
                Swal.fire(
                    'Cart Item Deleted',
                    '{{ session('deleted') }}',
                    'success'
                )
            @endif

            @if (session('updated'))
                Swal.fire(
                    'Cart Item Updated',
                    '{{ session('updated') }}',
                    'success'
                )
            @endif

            @if(session('order_declined'))
                Swal.fire(
                    'Order Declined',
                    '{{ session('order_declined') }}',
                    'success'
                )
            @endif

            simpleDataTable('.order_table')
        });
    </script>
@endpush