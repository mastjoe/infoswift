@extends('layouts.admin.app')
@section('title', 'Machines | Terminal '.$machine->terminal_id)

@section('page', 'Machine | Terminal '.$machine->terminal_id)

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('all.machines') }}">Machines</a>
    </li>
    <li class="breadcrumb-item active">
        {{ $machine->terminal_id }}
    </li>
@endsection

@section('content')
    @include('admin.machines.hero')
     
    <div class="row mt-0">
        <div class="col-md-9 order-12 order-md-1">
            {{-- machine info --}}
            @component('components.card')
                @slot('card_header')
                    <b>Terminal Info</b>
                @endslot
                @slot('card_body')
                    <table class="table table-borderless table-sm">
                        <thead>
                            <th width="140"></th>
                            <th></th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <b class="text-primary">Terminal ID</b>
                                </td>
                                <td>
                                    {{ $machine->terminal_id }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b class="text-primary">Status</b>
                                </td>
                                <td>
                                    {{ $machine->machineStatus->status }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b class="text-primary">Branch Status</b>
                                </td>
                                <td>
                                    {{ $machine->branch_status }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b class="text-primary">Vendor</b>
                                </td>
                                <td>
                                    {{ $machine->machineVendor->vendor }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b class="text-primary">Type</b>
                                </td>
                                <td>
                                    {{ $machine->machineType->type }}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                @endslot
            @endcomponent

            {{-- client info --}}
            @component('components.card')
                @slot('card_header')
                    <b>Client Info</b>
                @endslot
                @slot('card_body')
                    <table class="table table-sm table-borderless">
                        <thead>
                            <th width="140"></th>
                            <th></th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <b class="text-primary">Client</b>
                                </td>
                                <td>
                                    <span class="font-weight-bold" style="color:{{ $machine->client->color }}">{{ $machine->client->name }}</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b class="text-primary">Branch</b>
                                </td>
                                <td>
                                    {{ $machine->branch->branch }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b class="text-primary">Region</b>
                                </td>
                                <td>
                                    {{ $machine->region->region }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b class="text-primary">Location</b>
                                </td>
                                <td>
                                    {{ $machine->region->state->state }}, {{ $machine->region->country->country }}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                @endslot
            @endcomponent

            {{--  engineer  --}}
            @component('components.card')
                @slot('card_header')
                    <b>Engineer</b>
                    @if ($machine->engineer)
                        <div class="card-options">
                            <button class="btn btn-sm btn-action waves-effect btn-primary assign_engineer_btn">
                                <i class="mdi mdi-pencil"></i>
                            </button>
                        </div>
                    @endif
                @endslot
                @slot('card_body')
                    @if ($machine->engineer)
                        <table class="table table-sm table-borderless">
                            <thead>
                                <th width="150"></th>
                                <th></th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><b class="text-primary">Name:</b></td>
                                    <td>{{ $machine->engineer->full_name() }}</td>
                                </tr>
                                <tr>
                                    <td><b class="text-primary">Email:</b></td>
                                    <td>{{ $machine->engineer->email }}</td>
                                </tr>
                                <tr>
                                    <td><b class="text-primary">Phone:</b></td>
                                    <td>{{ $machine->engineer->phone }}</td>
                                </tr>
                            </tbody>
                        </table>
                    @else
                        <div class="text-center my-4">
                            <h4 class="text-muted">
                                No engineer assigned
                            </h4>
                            @can('assign', App\Machine::class)
                                <button class="btn btn-primary waves-effect light-waves assign_engineer_btn">
                                    Assign Engineer
                                </button>
                            @endcan
                        </div>
                    @endif
                @endslot
            @endcomponent
        </div>
        <div class="col-md-3 order-1 order-md-12 my-2">
            @include('admin.machines.nav')
        </div>
    </div>

    {{--  assign engineer modal --}}
    <div class="modal fade" role="dialog" id="assign_engineer_modal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                @if ($machine->state->engineers->count())
                    <form method="POST" id="assign_engineer_form" action="{{ route('assign.engineer.to.machine', $machine->id) }}">
                        <div class="modal-header">
                            <h3 class="modal-title">Assign Engineer</h3>
                            <button class="close" type="button"><span>&times;</span></button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="">Machine</label>
                                <input type="text" class="form-control" readonly value="{{ $machine->terminal_id }}">
                            </div>
                            {{--  engineers  --}}
                            <div class="form-group">
                                <label for="">Engineers</label>
                                <select class="form-control" name="engineer" required>
                                    <option value="">Choose Engineer</option>
                                    @foreach ($machine->state->engineers as $eng)
                                        <option value="{{ $eng->id }}" {{  $machine->engineer_id == $eng->id ? "selected" : null }}>
                                            {{ $eng->full_name() }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-default" type="button" data-dismiss="modal">
                                Exit
                            </button>
                            <button class="btn btn-primary" type="submit">
                                Assign
                            </button>
                        </div>
                    </form>
                @else
                    <div class="modal-body">
                        <div class="py-3 text-center">
                            <h4 class="text-muted">No Engineer found in machine's state of location</h4>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-default" type="button">
                            Exit
                        </button>
                    </div
                @endif
                
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/admin/machine.js') }}"></script>
    <script>
        $(document).ready(function() {
            @if (session('updated'))
                Swal.fire(
                    'Machine Updated!',
                    '{{ session('updated') }}',
                    'success'
                );
            @endif

            @if (session('created'))
                Swal.fire(
                    'Machine Created',
                    '{{ session('created') }}',
                    'success'
                )
            @endif

            @if (session('assigned_engineer'))
                Swal.fire(
                    'Machine Assigned',
                    '{{ session('assigned_engineer') }}',
                    'success'
                );
            @endif
        })
    </script>
@endpush