@extends('layouts.admin.app')
@section('title', $client->short_name .' | '.$date->format('jS M Y').' - Daily Downlist Report')

@section('page', 'Reports | DownList | '.$client->short_name)

@push('css')
    <link rel="stylesheet" href="{{ asset('css/bootstrap-material-datetimepicker.css') }}">
    <style>
        .profile-table.table>tbody>tr>td {
            padding: 2px 12px;
        }
    </style>
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('all.reports') }}">Reports</a>
    </li>
    <li class="breadcrumb-item active">
        <a>Downlist</a>
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="offset-md-3 col-md-6 mb-2">
            <form method="GET" action="{{ route('client.downlist.report', $client->id) }}">
                <div class="input-group">
                    <input type="text" 
                        class="form-control bg-transparent" 
                        name="date" 
                        placeholder="Pick a date"
                        id="date-picker"
                        value="{{ $date->format('Y-m-d') }}"
                    >
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="submit">
                            <span class="fa fa-calendar-check"></span>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            @if ($downlist->count())
                @component('components.card')
                    @slot('card_body')
                        <h4>
                            <span>{{ $client->short_name }}</span> |
                            DownList
                            <span class="badge badge-primary ml-2">
                                {{ $date->format('jS M, Y') }}
                            </span>
                        </h4>
                        <hr>
                        <div class="table-responsive-sm">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th width="10">S/No</th>
                                        <th>Terminal ID</th>
                                        <th>ATM Name</th>
                                        <th width="220">Issues Logged</th>
                                        <th>Status</th>
                                        <th>CE</th>
                                        <th>Custodian</th>
                                        <th>Branch</th>
                                        <th>Request Time</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($downlist as $d)
                                        <tr>
                                            <td>
                                                {{ $loop->iteration }}
                                            </td>
                                            <td>
                                                {{ $d->machine->terminal_id }}
                                            </td>
                                            <td>
                                                {{ $d->machine->name ?? '-'}}
                                            </td>
                                            <td>
                                                @php
                                                    $fault_array = implode(" | ", $d->faults_array());
                                                @endphp
                                                {{ $fault_array }}
                                            </td>
                                            <td>
                                                {{ $d->status }}
                                            </td>
                                            <td>
                                                @if ($d->machine->engineer)
                                                    {{ $d->machine->engineer->first_name }}
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td>
                                                @if ($d->machine->branch->custodians->count())
                                                    {{ $d->machine->branch->custodians->first()->name }}
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td>
                                                {{ $d->machine->branch->branch }}
                                            </td>
                                            <td>
                                                <span title="{{ $d->created_at->format('jS M, Y @ h:i a') }}" data-toggle="tooltip">{{ $d->created_at->format('jS M, Y') }}</span>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endslot
                @endcomponent
            @else
                <div class="py-5 text-center">
                    <h2 class="text-muted">No downlist record found!</h2>
                </div>
            @endif
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/moment-with-locale.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-material-datetimepicker.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#date-picker').bootstrapMaterialDatePicker({
                time: false,
                maxDate: new Date()
            });
        });
    </script>
@endpush