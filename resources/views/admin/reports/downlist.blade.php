@extends('layouts.admin.app')
@section('title', 'Daily Clients Downlist Report')

@section('page', 'Reports | Clients DownList')

@push('css')
    <link rel="stylesheet" href="{{ asset('css/bootstrap-material-datetimepicker.css') }}">
    <style>
        .profile-table.table>tbody>tr>td {
            padding: 2px 12px;
        }
    </style>
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('all.reports') }}">Reports</a>
    </li>
    <li class="breadcrumb-item active">
        <a>Downlist</a>
    </li>
@endsection

@section('content')
    <div class="row">
        @if ($clients->count())
            @foreach ($clients as $client)
                <div class="col-md-4">
                    <a href="{{ route('client.downlist.report', $client->id) }}">
                        @component('components.card', [
                            'card_body_class' => 'directory-card-bg',
                            'card_class' => 'directory-card m-b-20'
                        ])
                            @slot('card_body')
                                <div class="directory-img text-center">
                                    <img class="rounded-circle thumb-lg img-thumbnail" src="{{ $client->logo() }}" alt="{{ $client->name }} logo">
                                </div>
                                <h5 class="font-16 text-center">{{ $client->name }}</h5>
                            @endslot
                        @endcomponent
                    </a>
                </div>
            @endforeach
        @else
            <div class="col-12 text-center my-5">
                <h2 class="text-muted"> No client found!</h2>
                @can('create', App\Client::class)
                    <a href="{{ route('create.client') }}" class="btn btn-primary waves-effect">
                        Add A client
                    </a>
                @endcan
            </div>
        @endif
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/moment-with-locale.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-material-datetimepicker.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#date-picker').bootstrapMaterialDatePicker({
                time: false
            });
        });
    </script>
@endpush