{{--  clients ticket charts  --}}
@component('components.card')
    @slot('card_body')
        <div class="row">
            <div class="col-12">
                <h4 class="mt-0 header-title mb-4">
                    <span class="badge badge-warning ml-2 ticket_year_tag"></span>
                    Client Monthly Tickets
                    <div class="btn-group float-right">
                        <button class="btn btn-default btn-sm waves-effect waves-light">
                            Year
                        </button>
                        <button type="button" class="btn btn-default waves-effect waves-light dropdown-toggle dropdown-toggle-split"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <div class="dropdown-menu ticket-year-dropdown">
                            @foreach ($ticket_years as $year)                                
                                <a class="dropdown-item">{{ $year }}</a>
                            @endforeach
                        </div>
                    </div>
                </h4>
                <div id="client_tickets_chart" data-url="{{ route('dashboard.ticket.chart.data') }}">
                    <canvas></canvas>
                </div>
            </div>
        </div>
    @endslot
@endcomponent