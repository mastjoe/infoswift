@extends('layouts.admin.app')
@section('title', 'Dashboard')

@section('page', 'Dashboard')

@push('css')
@endpush

@section('crumb')
@endsection

@section('content')
    <div class="row">
        @include('admin.dashboard.info-boxes')
    </div>
    <div class="row">
        <div class="col-12">
            @include('admin.dashboard.ticket-chart')
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/admin/dashboard.js') }}"></script>
    <script>
        $(document).ready(function() {
            dataCounter()
        })
    </script>
@endpush