{{--  info boxes  --}}

{{--  clients  --}}
<div class="col-md-3">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-4">
                    <img src="{{ asset('images/target.png') }}">
                </div>
                <div class="col-8 text-center">
                    <h6 class="data-counter" data-count="{{ $clients->count() }}">0</h6>
                    <h5>Clients</h5>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- terminals  --}}
<div class="col-md-3">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-4">
                    <img src="{{ asset('images/atm-1.png') }}">
                </div>
                <div class="col-8 text-center">
                    <h6 class="data-counter" data-count="{{ $machines->count() }}">0</h6>
                    <h5>Terminals</h5>
                </div>
            </div>
        </div>
    </div>
</div>

{{--  tickets  --}}
<div class="col-md-3">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-4">
                    <img src="{{ asset('images/ticket.png') }}">
                </div>
                <div class="col-8 text-center">
                    <h6 class="data-counter" data-count="{{ $tickets->count() }}">0</h6>
                    <h5>Tickets</h5>
                </div>
            </div>
        </div>
    </div>
</div>

{{--  open tickets  --}}
<div class="col-md-3">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-4">
                    <img src="{{ asset('images/problem.png') }}">
                </div>
                <div class="col-8 text-center">
                    <h6 class="data-counter" data-count="{{ $open_tickets->count() }}">0</h6>
                    <h5>Open Tickets</h5>
                </div>
            </div>
        </div>
    </div>
</div>