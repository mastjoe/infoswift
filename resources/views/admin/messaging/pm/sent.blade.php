@extends('layouts.admin.app')
@section('title', 'Messaging | PM - Sent')

@section('page', 'Messaging | PM | Sent')

@push('css')
    <style>
        .table tr.selected {
            background: #fbfbfb;
        }
    </style>
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a>Messaging</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('admin.private.messages') }}">PM</a>
    </li>
    <li class="breadcrumb-item active">
        <a>Sent</a>
    </li>
@endsection

@section('content')
    <div class="row">   
        <div class="col-md-3 order-1 order-md-2">
            @include('admin.messaging.pm.nav')
        </div>
        <div class="col-md-9 order-2 order-md-1">
            @component('components.card')
                @slot('card_body')
                    <h5>
                        <span class="mdi mdi-message-reply-text text-primary mr-2"></span>
                        Sent Messages
                    </h5>
                    <hr>
                    @if ($outboxes->count())
                        <div class="table-responsive-sm">
                            <table class="table table-sm table-borderless table-hover dt">
                                <thead>
                                    <tr>
                                        <th width="2"></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($outboxes->sortByDesc('created_at') as $key => $outbox)
                                        <tr data-id="{{ $outbox->id }}">
                                            <td>
                                                <input type="checkbox" id="sent-item-{{ $key }}" class="form-check-input">
                                                <label class="form-check-label" for="sent-item-{{ $key }}"></label>
                                                <div class="form-check-inline">
                                                </div>
                                            </td>
                                            <td>
                                                <span title="{{ $outbox->receiver->full_name() }}">
                                                    {{ $outbox->receiver->full_name() }}
                                                </span>
                                            </td>
                                            <td>
                                                <b><a href="{{ route('admin.read.private.message', $outbox->ref) }}">{{ Str::limit($outbox->subject, 25, '...') }}</a></b>
                                            </td>
                                            <td>
                                                <span>{{ Str::limit(strip_tags($outbox->message), 40, '...') }}</span>
                                            </td>
                                            <td>
                                                @if ($outbox->created_at->today())
                                                    {{ $outbox->created_at->format('h:i a') }}
                                                @else
                                                    {{ $outbox->created_at->format('dS M, \'y | h:i a') }}
                                                @endif
                                            </td>
                                            <td></td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @else
                        <div class="my-5 text-center">
                            <div>
                                <img src="{{ asset('images/outbox.png') }}">
                            </div>
                            <h3 class="text-muted">No sent message found!</h3>
                        </div>
                    @endif
                @endslot
            @endcomponent
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/admin/message.pm.js') }}"></script>
    <script>
        $(document).ready(function() {
            @if (session('message_sent'))
                Swal.fire(
                    'Message Sent',
                    '{{ session('message_sent') }}',
                    'success'
                );
            @endif
        });
    </script>
@endpush