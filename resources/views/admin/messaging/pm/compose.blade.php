{{-- compose message modal --}}
<div class="modal fade" role="dialog" id="compose_message_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form method="POST" action="{{ route('admin.send.private.message') }}">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">
                        <i class="mdi mdi-message-text-outline text-primary mr-2"></i>
                        New Message
                    </h4>
                    <button class="close" type="button" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{-- recipients --}}
                    <div class="form-group">
                        <label for="recipients">Recipients</label>
                        <select class="form-control"
                            name="recipients[]"
                            id="recipients"
                            multiple
                        >
                            @if ($recipients->count())
                                @foreach ($recipients as $recipient)
                                    <option value="{{ $recipient->id }}">{{ $recipient->full_name() }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>

                    {{-- subject --}}
                    <div class="form-group">
                        <input type="text" 
                            name="subject"
                            placeholder="Subject"
                            required
                            class="form-control"
                            maxlength="100"
                        >
                    </div>

                    {{-- message  --}}
                    <div class="form-group">
                        <textarea class="form-control summernote"
                            name="message"
                        ></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" type="button" data-dismiss="modal">
                        Exit
                    </button>
                    <button class="btn btn-primary" type="submit">
                        Send
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>