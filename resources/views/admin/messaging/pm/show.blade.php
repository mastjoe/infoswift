@extends('layouts.admin.app')
@section('title', $message->subject.' - Messaging | PM - Inbox')

@section('page', 'Messaging | PM | Inbox | '.$message->ref)

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a>Messaging</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('admin.private.messages') }}">PM</a>
    </li>
    <li class="breadcrumb-item active">
        {{ $message->ref }}
    </li>
@endsection

@section('content')
    <div class="row">   
        <div class="col-md-3 order-1 order-md-2">
            @include('admin.messaging.pm.nav')
        </div>
        <div class="col-md-9 order-2 order-md-1">
            @component('components.card')
                @slot('card_body')
                  <h5 class="text-primary">
                        @if ($user->is($message->sender))
                            <i class="fa fa-envelope mr-2"></i>
                        @else
                            <i class="fa fa-envelope-open mr-2"></i>
                        @endif
                        {{ $message->subject }}
                  </h5>
                  <div class="my-2 mb-3 text-muted">
                        <span class="mr-2"><b class="text-dark">From:</b> {{ $message->sender->full_name() }},</span>
                        <span class="mr-2"><b class="text-dark">Sent:</b> {{ $message->created_at->format('dS M, y | h:i a') }}</span>
                        @if ($user->is($message->sender))                            
                            <span class="mr-2">
                                <span class="badge badge-secondary">Delivered</span>
                            </span>

                            @if ($message->read_at)                                
                                <span class="mr-2">
                                    <span class="badge badge-success">Read</span>
                                </span>
                            @endif
                        @endif
                  </div>
                  <hr>
                  <div class="my-2">
                        {!! $message->message !!}
                  </div>
                @endslot
            @endcomponent
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/admin/message.pm.js') }}"></script>
    <script>
        
    </script>
@endpush