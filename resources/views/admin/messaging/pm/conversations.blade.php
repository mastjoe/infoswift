@extends('layouts.admin.app')
@section('title', 'Messaging | PM - Conversations')

@section('page', 'Messaging | PM | Conversations')

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a>Messaging</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('admin.private.messages') }}">PM</a>
    </li>
    <li class="breadcrumb-item active">
        <a>Conversations</a>
    </li>
@endsection

@section('content')
    <div class="row">   
        <div class="col-md-3 order-1 order-md-2">
            @include('admin.messaging.pm.nav')
        </div>
        <div class="col-md-9 order-2 order-md-1">
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/admin/message.pm.js') }}"></script>
@endpush