{{-- private message nav --}}
<ul class="nav flex-column">
    <button class="btn btn-primary btn-lg mb-3 compose_message_btn" style="border-radius: 10rem">
        Compose
    </button>
    <a class="btn btn-dark mb-2" href="{{ route('admin.private.messages') }}">
        Inbox
    </a>
    <a class="btn btn-dark mb-2" href="{{ route('admin.sent.private.messages') }}">
        Sent
    </a>
    <a class="btn btn-dark mb-2 d-none" href="{{ route('admin.trashed.private.messages') }}">
        Trash
    </a>
    <a class="btn btn-dark mb-2" href="{{ route('admin.private.messages.conversations') }}">
        Conversations
    </a>
</ul>

@include('admin.messaging.pm.compose')