{{-- send message to specific staff --}}
<form method="POST" action="{{ route('admin.send.staff.private.message', $staff->id) }}" id="send_message_form">
    @csrf
    <div class="modal-header">
        <h4 class="modal-title">
            <span class="mdi mdi-message-reply-text text-primary mr-2"></span>
            New Message
        </h4>
        <button class="close" type="button" data-dismiss="modal"><span>&times;</span></button>
    </div>
    <div class="modal-body">    
        <div class="form-group">
            <label>Recipient</label>
            <input type="text" readonly class="form-control" value="{{ $staff->full_name() }}">
        </div>
        <div class="form-group">
            <input type="text" class="form-control" required maxlength="100" name="subject" placeholder="Subject">
        </div>
        <div class="form-group">
            <textarea class="form-control summernote" name="message"></textarea>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-default" data-dismiss="modal">
            Exit
        </button>
        <button class="btn btn-primary" type="submit">
            <i class="fas fa-paper-plane mr-1"></i>
            Send
        </button>
    </div>
</form>
