@extends('layouts.admin.app')
@section('title', 'Messaging | PM - Inbox')

@section('page', 'Messaging | PM | Inbox')

@push('css')
    <style>
        .table tr.selected {
            background: #fbfbfb;
        }
    </style>
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a>Messaging</a>
    </li>
    <li class="breadcrumb-item active">
        PM
    </li>
@endsection

@section('content')
    <div class="row">   
        <div class="col-md-3 order-1 order-md-2">
            @include('admin.messaging.pm.nav')
        </div>
        <div class="col-md-9 order-2 order-md-1">
            @component('components.card')
                @slot('card_body')
                    <h5>
                        <span class="mdi mdi-inbox mr-2 text-primary"></span>
                        Inbox
                    </h5>
                    <hr>
                    @if ($inboxes->count())
                        <div class="table-responsive-sm">
                            <table class="table table-sm table-borderless table-hover dt">
                                <thead>
                                    <tr>
                                        <th width="2"></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($inboxes->sortByDesc('created_at') as $key => $inbox)
                                    <tr data-id="{{ $inbox->id }}">
                                        <td>
                                            <input type="checkbox" id="sent-item-{{ $key }}" class="form-check-input">
                                            <label class="form-check-label" for="sent-item-{{ $key }}"></label>
                                            <div class="form-check-inline">
                                            </div>
                                        </td>
                                        <td>
                                            <span title="{{ $inbox->sender->full_name() }}">
                                                {{ $inbox->sender->full_name() }}
                                            </span>
                                        </td>
                                        <td>
                                            <b><a href="{{ route('admin.read.private.message', $inbox->ref) }}">{{ Str::limit($inbox->subject, 25, '...') }}</a></b>
                                            @if (!$inbox->read_at)
                                                <span class="badge badge-primary">NEW</span>
                                            @endif
                                        </td>
                                        <td>
                                            <span>{{ Str::limit(strip_tags($inbox->message), 40, '...') }}</span>
                                        </td>
                                        <td>
                                            @if ($inbox->created_at->today())
                                            {{ $inbox->created_at->format('h:i a') }}
                                            @else
                                            {{ $inbox->created_at->format('dS M, \'y | h:i a') }}
                                            @endif
                                        </td>
                                        <td></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @else
                        <div class="my-5 text-center">
                            <div>
                                <img src="{{ asset('images/inbox.png') }}">
                            </div>
                            <h3 class="text-muted">No message found in box</h3>
                        </div>
                    @endif
                @endslot
            @endcomponent
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/admin/message.pm.js') }}"></script>
    <script>
        
    </script>
@endpush