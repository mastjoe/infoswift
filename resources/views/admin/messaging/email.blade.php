@extends('layouts.admin.app')
@section('title', 'Messaging | Email')

@section('page', 'Messaging | Email')

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a>Messaging</a>
    </li>
    <li class="breadcrumb-item active">
        Email
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="container">
                @component('components.card')
                    @slot('card_body')
                    <h4 class="card-title">
                        Email
                    </h4>
                    <hr>
                    {{--  to  --}}
                    @component('components.form.input-form-group', [
                        'form_group_class' => 'row',
                        'input_wrap_class' => 'col-md-10',
                        'label' => 'To',
                        'label_class' => 'col-md-2 col-form-label text-md-right',
                        'name' => 'recepients',
                        'placeholder' => 'Recipients email address here'
                    ])
                        
                    @endcomponent

                    {{--  subject  --}}
                    @component('components.form.input-form-group', [
                        'form_group_class' => 'row',
                        'input_wrap_class' => 'col-md-10',
                        'label' => 'Subject',
                        'label_class' => 'col-md-2 col-form-label text-md-right',
                        'name' => 'subject',
                        'placeholder' => 'Subject of mail'
                    ])
                        
                    @endcomponent

                    {{--  message  --}}
                    @component('components.form.textarea-form-group', [
                        'form_group_class' => 'row',
                        'input_wrap_class' => 'col-md-10',
                        'label' => 'Message',
                        'label_class' => 'col-md-2 col-form-label text-md-right',
                        'name' => 'message',
                        'id' => 'message_body'
                    ])
                        
                    @endcomponent

                    <hr>
                    <div class="my-2 text-center">
                        <button class="btn btn-default waves-effect mr-2">
                            Cancel
                        </button>
                        <button class="btn btn-primary waves-effect">
                            Send
                            <i class="fab fa-telegram-plane m-l-10"></i>
                        </button>
                    </div>

                    @endslot
                @endcomponent
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/admin/message.js') }}"></script>
@endpush