@extends('layouts.admin.app')
@section('title', 'Messaging | Chat')

@section('page', 'Messaging | Chat')

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a>Messaging</a>
    </li>
    <li class="breadcrumb-item active">
        Chat
    </li>
@endsection

@section('content')
    <div class="row" id="app">
        <div class="col-md-4">
            <div>
                @component('components.card')
                    @slot('card_body')
                        <div>
                            <h4 class="card-title text-primary">
                                Chats
                            </h4>
                            @foreach ($other_staff as $staff)
                                <div class="chat-">
                                    <a href="#" class="media">
                                        <img class="d-flex mr-3 rounded-circle" 
                                            src="{{ $staff->avatar() }}"
                                            alt="Generic placeholder image" 
                                            height="36"
                                        >
                                        <div class="media-body chat-user-box">
                                            <p class="user-title m-0">
                                                {{ $staff->full_name() }}
                                            </p>
                                            <p class="text-muted">Hello</p>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    @endslot
                @endcomponent
            </div>
        </div>
        <div class="col-md-8">
            <div>
                @component('components.card')
                    @slot('card_body')
                        
                    @endslot
                @endcomponent
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        
    </script>
@endpush