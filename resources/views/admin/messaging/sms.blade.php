@extends('layouts.admin.app')
@section('title', 'Messaging | SMS')

@section('page', 'Messaging | SMS')

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a>Messaging</a>
    </li>
    <li class="breadcrumb-item active">
        SMS
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="container">
                <form method="POST" action="">
                    @csrf
                    @component('components.card')
                        @slot('card_body')
                            <h4 class="card-title">
                                Send SMS
                            </h4>
                            <hr>
                            {{--  quick selectable group  --}}
                            @component('components.form.select-form-group', [
                                'name' => 'recipients',
                                'input_wrap_class' => 'col-md-8',
                                'form_group_class' => 'row',
                                'label' => '',
                                'label_class' => 'col-md-3 text-md-right col-form-label'
                            ])
                                @slot('options')
                                    <option value="">Choose Quick Group</option>
                                    <option value="staff">All Staff</option>
                                    <option value="clients">All Clients</option>
                                    <option value="custodians">All Branch Custodians</option>
                                @endslot
                            @endcomponent

                            {{--  recipient group  --}}
                            @component('components.form.textarea-form-group', [
                                'name' => 'recipient',
                                'required' => true,
                                'placeholder' => 'recepient here...',
                                'form_group_class' => 'row',
                                'label_class' => 'col-md-3 col-form-label text-md-right',
                                'input_wrap_class' => 'col-md-8',
                                'label' => 'Recepients'
                            ])
                                
                            @endcomponent

                            {{--  message  --}}
                            @component('components.form.textarea-form-group', [
                                'name' => 'message',
                                'required' => true,
                                'placeholder' => 'short message here',
                                'form_group_class' => 'row',
                                'label_class' => 'col-md-3 col-form-label text-md-right',
                                'input_wrap_class' => 'col-md-8',
                                'label' => 'Message'
                            ])
                                
                            @endcomponent
                            <hr>
                            <div class="text-center my-2">
                                <button class="btn btn-default px-4" type="reset">
                                    Cancel
                                </button>
                                <button class="btn btn-primary waves-effect px-4" type="submit">
                                    Send SMS
                                </button>
                            </div>
                        @endslot
                    @endcomponent
                </form>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        const custodians = [
            @if ($custodians->count())
                @foreach ($custodians as $cust)
                {name: '{{ $cust->name }}', phone: '{{ $cust->phone }}'},
                @endforeach
            @endif
        ];

        const clients = [
            @if ($clients->count())
                @foreach ($clients as $cl)
                    {name: '{{ $cl->short_name }}', phone: '{{ $cl->phone }}'},
                @endforeach
            @endif
        ];

        const staff = [
            @if ($users->count())
                @foreach ($users as $user)
                    {name: '{{ $user->full_name() }}', phone: '{{ $user->phone }}'},
                @endforeach
            @endif
        ];
    </script>
    <script src="{{ asset('js/admin/sms.js') }}"></script>
@endpush