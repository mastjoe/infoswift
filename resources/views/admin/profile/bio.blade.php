@extends('layouts.admin.app')
@section('title', 'User Profile | Bio')

@section('page', 'My Profile | Bio')

@push('css')
    <style>
        .profile-table.table>tbody>tr>td {
            padding: 2px 12px;
        }
    </style>
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.profile') }}">Profile</a>
    </li>
    <li class="breadcrumb-item active">
        Bio
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-4">
           @include('admin.profile.nav')
        </div>
        <div class="col-md-8">
            <form method="POST" action="{{ route('admin.update.bio') }}">
                @if ($errors->any())
                    <div class="alert alert-primary alert-dismissable">
                        <button class="close" type="button" data-dismiss="alert">
                            <span>&times;</span>
                        </button>
                        {{ $errors->first() }}
                    </div>
                @endif
                @csrf
                @component('components.card')
                    @slot('card_header')
                        <h5 class="card-title">
                            <i class="fa fa-user-lock text-primary mr-2"></i>
                            Update Bio
                        </h5>
                    @endslot
                    @slot('card_body')
                        <div class="form-group">
                            <label></label>
                            <textarea class="form-control"
                                name="bio"
                                placeholder="You can write a little note about yourself here..."
                            >{{ old('bio') ?? $user->bio }}</textarea>
                        </div>
                        <hr>
                        <div class="text-center">
                            <button class="btn btn-primary" type="submit">
                                Save Bio
                            </button>
                        </div>
                    @endslot
                @endcomponent
            </form>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/admin/profile.js') }}"></script>
    <script>
        $(document).ready(function() {
            @if(session('updated'))
                Swal.fire(
                    'Bio Updated',
                    '{{ session('updated') }}',
                    'success'
                );
            @endif
        });
    </script>
@endpush