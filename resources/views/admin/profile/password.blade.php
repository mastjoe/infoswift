@extends('layouts.admin.app')
@section('title', 'User Profile | Password')

@section('page', 'My Profile | Password')

@push('css')
    <style>
        .profile-table.table>tbody>tr>td {
            padding: 2px 12px;
        }
    </style>
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.profile') }}">Profile</a>
    </li>
    <li class="breadcrumb-item active">
        Password
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-4">
           @include('admin.profile.nav')
        </div>
        <div class="col-md-8">
            <form method="POST" action="{{ route('admin.update.password') }}">
                @if ($errors->any())
                    <div class="alert alert-primary alert-dismissable">
                        <button class="close" type="button" data-dismiss="alert">
                            <span>&times;</span>
                        </button>
                        {{ $errors->first() }}
                    </div>
                @endif
                @csrf
                @component('components.card')
                    @slot('card_header')
                        <h5 class="card-title">
                            <i class="fa fa-user-lock text-primary mr-2"></i>
                            Update Password
                        </h5>
                    @endslot
                    @slot('card_body')
                        {{--  old password  --}}
                        <div class="form-group">
                            <label>Old Password</label>
                            <div class="input-group">
                                <input type="password" class="form-control" name="old_password" required>
                                <div class="input-group-append" onclick="passwordFieldTypeToggler(event)">
                                    <span class="input-group-text">
                                        <span class="fa fa-eye"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        {{--  new password  --}}
                        <div class="form-group">
                            <label>New Password</label>
                            <div class="input-group">
                                <input type="password" class="form-control" name="password" required>
                                <div class="input-group-append" onclick="passwordFieldTypeToggler(event)">
                                    <span class="input-group-text">
                                        <span class="fa fa-eye"></span>
                                    </span>
                                </div>
                            </div>
                        </div>

                        {{--  confirm new password  --}}
                        <div class="form-group">
                            <label>Confirm New Password</label>
                            <div class="input-group">
                                <input type="password" class="form-control" name="confirm_password" required>
                                <div class="input-group-append" onclick="passwordFieldTypeToggler(event)">
                                    <span class="input-group-text">
                                        <span class="fa fa-eye"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="text-center">
                            <button class="btn btn-primary" type="submit">
                                Save Password
                            </button>
                        </div>
                    @endslot
                @endcomponent
            </form>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/admin/profile.js') }}"></script>
    <script>
        $(document).ready(function() {
            @if(session('updated'))
                Swal.fire(
                    'Password Updated',
                    '{{ session('updated') }}',
                    'success'
                );
            @endif
        });
    </script>
@endpush