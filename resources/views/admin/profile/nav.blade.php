@component('components.card')
    @slot('card_body')
    <div class="row">
        <div class="col-4">
            <img src="{{ Auth::user()->avatar() }}" alt="user" class="img-rounded" height="80px" weight="80px">
        </div>
        <div class="col-8">
            <h5>
                {{ $user->first_name }} {{ $user->middle_name }} {{ $user->last_name }}
            </h5>
            <small class="font-weight-bold text-primary">
                {{ $user->email }}
            </small>
        </div>
    </div>
    <div class="row">
        <div class="col-12 text-center">
            <div class="social-icons">
                <ul class="social-links list-inline mb-0 p-2">
                    <li class="list-inline-item">
                        <a title="Facebook" data-placement="top" class="btn-danger tooltips" data-toggle="tooltip" href=""
                            data-original-title="Facebook">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a title="" data-placement="top" class="btn-info tooltips" data-toggle="tooltip" href=""
                            data-original-title="Twitter">
                            <i class="fab fa-twitter"></i>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a title="" data-placement="top" class="btn-primary tooltips" data-toggle="tooltip" href=""
                            data-original-title="1234567890">
                            <i class="fa fa-phone"></i>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a title="" data-placement="top" class="btn-info tooltips" data-toggle="tooltip" href=""
                            data-original-title="@skypename">
                            <i class="fab fa-skype"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row my-2">
        <div class="col-12">
            <table class="table table-sm table-borderless profile-table">
                <tr>
                    <td class="font-weight-bold">Gender:</td>
                    <td>{{ $user->gender }}</td>
                </tr>
                <tr>
                    <td class="font-weight-bold">Birth Date:</td>
                    <td>
                        @isset($user->dob)
                        {{ $user->dob->format('jS M, Y') }}
                        @else
                        <small class="text-muted">Not available</small>
                        @endisset
                    </td>
                </tr>
                <tr>
                    <td class="font-weight-bold">Address:</td>
                    <td>
                        @isset($user->address)
                            {{ Str::limit($user->address, 30, '...') }}
                        @else
                            <small class="text-muted">Not available</small>
                        @endisset
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="row my-2">
        <div class="col-12">
            @if ($user->roles->count())
                @foreach ($user->roles_array() as $role)
                    <span class="md-chip">{{ $role }}</span>
                @endforeach
            @endif
        </div>
    </div>
    <div class="row my-2">
        <div class="col-12">
            <ul class="nav flex-column">
                <li class="nav-item">
                    <a href="{{ route('admin.profile') }}" class="btn btn-secondary btn-block my-2">
                        Profile
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.bio') }}" class="btn btn-secondary btn-block my-2">
                        Bio
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.password') }}" class="btn btn-secondary btn-block my-2">
                        Password
                    </a>
                </li>
            </ul>
        </div>
    </div>
    @endslot
@endcomponent