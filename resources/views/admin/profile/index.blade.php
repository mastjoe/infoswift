@extends('layouts.admin.app')
@section('title', 'User Profile')

@section('page', 'My Profile')

@push('css')
    <style>
        .profile-table.table>tbody>tr>td {
            padding: 2px 12px;
        }
    </style>
@endpush

@section('crumbs')
    <li class="breadcrumb-item active">
        Profile
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-4">
           @include('admin.profile.nav')
        </div>
        <div class="col-md-8">
            <form method="POST" action="{{ route('admin.update.profile') }}">
                @csrf
                @if ($errors->any())
                    <div class="alert alert-primary alert-dismissable">
                        <button class="close" type="button" data-dismiss="alert"><span>&times;</span></button>
                        {{ $errors->first() }}
                    </div>
                @endif
                @component('components.card')
                    @slot('card_header')
                        <h5 class="card-title">
                            <i class="fa fa-user-circle text-primary mr-2"></i>
                            Update Profile
                        </h5>
                    @endslot
                    @slot('card_body')
                        {{--  first name  --}}
                        @component('components.form.input-form-group', [
                            'name'             => 'first_name',
                            'label'            => 'First Name',
                            'id'               => 'first_name',
                            'value' => old('first_name') ?? $user->first_name,
                            'required' => true
                        ])
                        @endcomponent

                        {{--  middle name  --}}
                        @component('components.form.input-form-group', [
                            'name' => 'middle_name',
                            'label' => 'Middle Name',
                            'id' => 'middle_name',
                            'required' => false,
                            'value' => old('middle_name') ?? $user->middle_name,
                        ])
                        @endcomponent

                        {{--  last name  --}}
                        @component('components.form.input-form-group', [
                            'name' => 'last_name',
                            'label' => 'Last Name',
                            'id' => 'last_name',
                            'required' => true,
                            'value' => old('last_name') ?? $user->last_name,
                        ])
                        @endcomponent

                        {{--  gender  --}}
                        @component('components.form.select-form-group', [
                            'name' => 'gender',
                            'label' => 'Gender',
                            'id' => 'last_name',
                            'required' => true
                        ])
                            @slot('options')
                                <option value="">Choose Gender</option>
                                <option value="male" {{ $user->gender == "male" ? "selected" : null }}>Male</option>
                                <option value="female" {{ $user->gender == "female" ? "selected" : null }}>Female</option>
                            @endslot
                        @endcomponent

                        {{--  dob  --}}
                        @php
                            $dob = null;
                            if ($user->dob) {
                               $dob = $user->dob->format('Y-m-d');
                            }
                        @endphp
                        @component('components.form.input-form-group', [
                            'name' => 'dob',
                            'label' => 'Date of Birth',
                            'id' => 'dob',
                            'type' => 'date',
                            'required' => false,
                            'value' => $dob
                        ])
                        @endcomponent

                        {{--  email  --}}
                        @component('components.form.input-form-group', [
                            'name' => 'email',
                            'label' => 'Email Address',
                            'id' => 'email',
                            'type' => 'email',
                            'required' => true,
                            'value' => $user->email
                        ])
                        @endcomponent

                        {{--  phone  --}}
                        @component('components.form.input-form-group', [
                            'name' => 'phone',
                            'label' => 'Telephone',
                            'id' => 'phone',
                            'type' => 'tel',
                            'required' => true,
                            'value' => $user->phone
                        ])
                        @endcomponent
                        {{--  address  --}}
                        @component('components.form.textarea-form-group', [
                            'name' => 'address',
                            'label' => 'Address',
                            'id' => 'address',
                            'required' => false,
                            'value' => $user->address
                        ])
                        @endcomponent
                        <hr>
                        <div class="text-center">
                            <button class="btn btn-primary">
                                Save Profile
                            </button>
                        </div>
                    @endslot
                @endcomponent
            </form>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/admin/profile.js') }}"></script>
    <script>
        $(document).ready(function() {
            @if (session('updated'))
                Swal.fire(
                    'Porfile Updated',
                    '{{ session('updated') }}',
                    'success'
                );
            @endif
        });
    </script>
@endpush