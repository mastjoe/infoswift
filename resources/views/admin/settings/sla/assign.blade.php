<form method="POST" action="{{ route('update.sla.states', $client->id) }}">
     @component('components.modal', [
            'modal_dialog_class' => 'animated bounceInLeft',
            'modal_id' => 'assign_modal',
            'modal_top_dismiss' => true,
        ])
            @slot('modal_header')
                <h5>
                    <i class="mdi mdi-account-plus mr-2 text-primary"></i>
                    Assign SLA Times
                </h5>
            @endslot

            @slot('modal_body')
                @csrf
                <div class="form_response"></div>

                {{-- name --}}
                @component('components.form.input-form-group', [
                    'label' => 'Client',
                    'name' => 'client',
                    'read_only' => true,
                    'value' => $client->name
                ])
                @endcomponent

                {{--  start time  --}}
                @component('components.form.input-form-group', [
                    'label' => 'Start Time',
                    'name' => 'start_time',
                    'type' => 'time',
                    'placeholder' => 'Enter start time of the day',
                    'required' => true,
                    'props' => ['min' => '6:00', 'max' => '18:00']
                ])
                @endcomponent

                {{--  stop time  --}}
                @component('components.form.input-form-group', [
                    'label' => 'Stop Time',
                    'name' => 'stop_time',
                    'type' => 'time',
                    'placeholder' => 'Enter stop time of the day',
                    'required' => true,
                    'props' => ['min' => '6:00', 'max' => '18:00']
                ])
                @endcomponent

                {{--  best hour  --}}
                @component('components.form.input-form-group', [
                    'label' => 'Best Hours',
                    'name' => 'best_hour',
                    'type' => 'number',
                    'placeholder' => 'Enter best hour',
                    'required' => true,
                    'props' => ['min' => 1, 'step' => 1]
                ])
                @endcomponent

                {{--  moderate hour  --}}
                @component('components.form.input-form-group', [
                    'label' => 'Moderate Hours',
                    'name' => 'moderate_hour',
                    'type' => 'number',
                    'placeholder' => 'Enter moderate hour',
                    'props' => ['min' => 1, 'step' => 1],
                    'required' => true,
                ])
                @endcomponent


                {{--  states field  --}}
                @component('components.form.select-form-group', [
                    'label' => 'States',
                    'name' => 'states[]',
                    'props' => ['multiple' => true],
                    'id' => 'states',
                ])
                    @slot('options')
                        @foreach ($states as $state)
                            <option value="{{ $state->id }}">{{ $state->state }}</option>
                        @endforeach
                    @endslot
                @endcomponent

                <div class="custom-control custom-checkbox mb-3">
                    <input type="checkbox" class="custom-control-input" id="allCheck">
                    <label class="custom-control-label" for="allCheck">Select all states</label>
                </div>

                {{-- description --}}
                @component('components.form.textarea-form-group', [
                    'label' => 'Description',
                    'name' => 'description',
                    'placeholder' => 'timing description (optional)',
                ])
                @endcomponent         
               
            @endslot

            @slot('modal_footer')
                <button class="btn btn-default" type="button" data-dismiss="modal">
                    Exit
                </button>
                <button class="btn btn-primary" type="submit">
                    Add
                </button>
            @endslot
        @endcomponent
</form>