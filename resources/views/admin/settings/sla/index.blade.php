@extends('layouts.admin.app')
@section('title', 'Settings | SLA | Clients')

@section('page', 'Settings | SLA | Clients')

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item active">
        <a>Settings - SLA</a>
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            @if ($clients->count())
                <div class="row">
                    @foreach ($clients as $client)
                        <div class="col-md-4">
                            <a href="{{ route('show.sla.client', $client->id) }}">
                                @component('components.card')
                                    @slot('card_body')
                                        <div class="clearfix">
                                            <div class="directory-img float-left mr-4">
                                                <img class="rounded-circle thumb-lg img-thumbnail"
                                                    src="{{ $client->logo() }}" alt="Generic placeholder image">
                                            </div>
                                            <h5 class="font-16 mt-0">{{ $client->name }}</h5>
                                            <p class="text-muted mb-2">{{ $client->short_name }}</p>
                                        </div>
                                    @endslot
                                @endcomponent
                            </a>
                        </div>
                    @endforeach
                    {{ $clients->links() }}
                </div>
            @else
                <div class="text-center my-5">
                    <h2 class="text-muted">No client found!</h2>
                    @can('create', App\Client::class)
                        <a class="btn btn-primary waves-effect" href="{{ route('create.client') }}">
                            Create Client Record
                        </a>
                    @endcan
                </div>
            @endif
        </div>
    </div>
@endsection

@push('js')
@endpush