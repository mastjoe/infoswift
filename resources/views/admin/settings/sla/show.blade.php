@extends('layouts.admin.app')
@section('title', 'Settings | SLA | '.$client->short_name)

@section('page', 'Settings | SLA | '.$client->short_name)

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('sla.clients') }}">Settings - SLA</a>
    </li>
    <li class="breadcrumb-item active">
        {{ $client->short_name }}
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="col-12 text-right mb-2">
            <button class="btn btn-dark info_btn waves-effect m-1">
                Info
            </button>
            <button class="btn btn-primary m-1 assign_btn">
                Assign
            </button>
        </div>
        <div class="col-12">
           @if ($states->count())               
                @component('components.card')
                    @slot('card_body')
                        <h4>
                            {{ $client->short_name }} State SLA Time Settings
                        </h4>
                        <hr>
                        <div class="table-responsive-sm">
                            <table class="table table-hover table-striped sla_table">
                                <thead>
                                    <tr>
                                        <th>State</th>
                                        <th>Country</th>
                                        <th>Start Time</th>
                                        <th>Stop Time</th>
                                        <th>Best Hour</th>
                                        <th>Mod. Hour</th>
                                        <th>Update</th>
                                        <th>Assigned</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($states as $state)
                                        <tr>
                                            <td>
                                                {{ $state->state }}
                                            </td>
                                            <td>
                                                {{ $state->country->country }}
                                            </td>
                                            <td>
                                                @if ($state->clientSla($client->id))                                                    
                                                    {{ $state->clientSla($client->id)->start_time->format('h:i A') }}
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td>
                                                @if ($state->clientSla($client->id))                                                    
                                                    {{ $state->clientSla($client->id)->stop_time->format('h:i A') }}
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td>
                                                @if ($state->clientSla($client->id))
                                                    {{ $state->clientSla($client->id)->best_hour }}
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td>
                                                @if ($state->clientSla($client->id))
                                                    {{ $state->clientSla($client->id)->moderate_hour }}
                                                @else
                                                -
                                                @endif
                                            </td>
                                            <td>
                                                <button class="btn btn-primary btn-sm px-3 update_sla_btn"
                                                    data-state="{{ $state->id }}"
                                                    data-best="{{ 
                                                        $state->clientSla($client->id) ? 
                                                        $state->clientSla($client->id)->best_hour : 0
                                                    }}"
                                                    data-moderate="{{ 
                                                        $state->clientSla($client->id) ?
                                                        $state->clientSla($client->id)->moderate_hour : 0
                                                    }}"
                                                    data-description="{{
                                                    $state->clientSla($client->id) ?
                                                    $state->clientSla($client->id)->description : ''
                                                    }}"
                                                    data-start="{{ $state->clientSla($client->id) ?$state->clientSla($client->id)->start_time->format('H:i') : 0}}"
                                                    data-stop="{{ $state->clientSla($client->id) ? $state->clientSla($client->id)->stop_time->format('H:i') : 0}}"
                                                >
                                                    Update
                                                </button>
                                            </td>
                                            <td>
                                                @if ($state->clientSla($client->id))
                                                {{ $state->clientSla($client->id)->created_at->diffForHumans() }}
                                                @else
                                                -
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endslot
                @endcomponent
           @else
                <div class="text-center my-5">
                        <h2 class="text-muted">No state found!</h2>
                </div>
           @endif
       </div>
    </div>

    {{--  modal  --}}
    @include('admin.settings.sla.info')
    @include('admin.settings.sla.assign')
@endsection

@push('js')
    <script src="{{ asset('js/admin/sla.js') }}"></script>
    <script>
        $(document).ready(function() {
            simpleDataTable('.sla_table');
            formLabelFocus();

            $('.info_btn').click(function() {
                $('#info_modal').modal({backdrop: 'static'});
            })

            @if (session('updated'))
                Swal.fire(
                    'SLA Timing Updated',
                    '{{ session('updated') }}',
                    'success'
                );
            @endif
        });
    </script>
@endpush