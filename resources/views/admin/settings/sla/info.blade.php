@component('components.modal', [
    'modal_dialog_class' => 'modal-dialog-centered animated bounceInLeft',
    'modal_id' => 'info_modal',
    'modal_top_dismiss' => true,
])
    @slot('modal_header')
        <h5>
            <i class="mdi mdi-information-outline mr-2 text-primary"></i>
            Info on SLA Time settings
        </h5>
    @endslot

    @slot('modal_body')
        <p class="text-justify">
            The SLA time settings for a state is effective for all terminals or machines within such state.
            Two numeric values will be supplied for the best hour rating and moderate/ fair hour rating for the resolution of ticket
            within a state.
        </p>       
        <p class="text-justify">
            The best hour is the maximum hour from the time of ticket creation in which a ticket resolution by an engineer can be
            rated highest.
        </p>       
        <p class="text-justify">
            The moderate or fair hour is the maximum hour from the time of ticket creation in which a ticket resolution by an
            engineer can be rated fair. This time should be more the best hour. Any time of resolution beyond this will be rated low
            by the system.
        </p>       
    @endslot

    @slot('modal_footer')
        <button class="btn btn-default" type="button" data-dismiss="modal">
            Ok
        </button>
    @endslot
@endcomponent