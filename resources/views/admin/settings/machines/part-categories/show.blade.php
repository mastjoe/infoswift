@extends('layouts.admin.app')
@section('title', 'Machines | Parts Categories - '.$category->category)

@section('page', 'Machine | Parts Categories - '.$category->category)

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('settings.machines') }}">Settings - Machine</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('all.parts.categories') }}">Parts Category</a>
    </li>
    <li class="breadcrumb-item active">
        {{ $category->category }}
    </li>
@endsection

@section('content')
    <div class="text-right my-2">
        <button class="btn btn-warning waves-effect mr-2 update_parts_category_btn">
            <span class="d-none d-md-inline-block">Edit</span>
            <i class="mdi mdi-pencil mr-1"></i>
        </button>
        <button class="btn btn-primary waves-effect delete_parts_category_btn"
            data-name="{{ $category->category }}"
            data-url="{{ route('delete.parts.category', $category->id) }}"
        >
            <span class="d-none d-md-inline-block">Delete</span>
            <i class="mdi mdi-delete mr-1"></i>
        </button>
    </div>
    <div class="row">
        <div class="col-md-3">
            @component('components.card')
                @slot('card_body')
                    <strong class="text-primary">Category</strong>
                    <p>{{ $category->category }}</p>
                    <hr>
                    <strong class="text-primary">Created At</strong>
                    <p>{{ $category->created_at->format('jS M, Y | h:i a') }}</p>
                    <hr>
                    <strong class="text-primary">Last Updated At</strong>
                    <p>{{ $category->updated_at->format('jS M, Y | h:i a') }}</p>
                    <hr>
                @endslot
            @endcomponent
        </div>
        <div class="col-md-9">
            {{-- description --}}
            @component('components.card')
                @slot('card_header')
                    <h5 class="card-title">
                        Description
                    </h5>
                @endslot
                @slot('card_body')
                    @isset($category->description)
                        {{ $category->description }}
                    @else
                        <h5 class="text-muted text-center">
                            No description found for category
                        </h5>
                    @endisset
                @endslot
            @endcomponent

            {{-- machine parts --}}
            @component('components.card')
                @slot('card_body')
                    <h4 class="card-title">
                        Machine Parts
                    </h4>
                    <hr>
                    @if ($category->machine_parts->count())
                        <div>
                            <table>
                                <thead>

                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    @else
                        <div class="text-center my-4">
                            <h5 class="text-muted">
                                No machine parts found under this category
                            </h5>
                        </div>
                    @endif
                @endslot
            @endcomponent
        </div>
    </div>

    {{-- modal --}}
    @include('admin.settings.machines.part-categories.edit')
@endsection

@push('js')
<script src="{{ asset('js/admin/machine.js') }}"></script>
<script>
    $(document).ready(function() {

        simpleDataTable('.type_table');

        @if (session('edit'))
            openUpdateCategoryModal()
        @endif

        @if (session('updated'))
            Swal.fire(
                'Machine Parts Category Updated',
                '{{ session('updated') }}',
                'success'
            );
        @endif
    });
</script>
@endpush