@extends('layouts.admin.app')
@section('title', 'Machines | Parts Categories')

@section('page', 'Machine | Parts Categories')

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('settings.machines') }}">Settings - Machine</a>
    </li>
    <li class="breadcrumb-item active">
        Parts Category
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            @if ($categories->count())
                <div class="text-right mb-2">
                    <button class="btn btn-primary waves-effect add_parts_category_btn">
                        <span class="d-none d-md-inline-block">New Category</span>
                        <i class="mdi mdi-plus-circle-outline mr-1"></i>
                    </button>
                </div>

                @component('components.card')
                    @slot('card_body')
                        <h4 class="card-title">
                            All Machine Parts Categories
                        </h4>
                        <hr>
                        <div class="table-responsive-sm">
                            <table class="table table-bordered type_table">
                                <thead>
                                    <tr>
                                        <th>Category</th>
                                        <th>Description</th>
                                        <th>Action</>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($categories as $category)
                                        <tr>
                                            <td>
                                                <a>{{ $category->category }}</a>
                                            </td>
                                            <td>
                                                {{ $category->description }}
                                            </td>
                                            <td>
                                                <div class="btn-group-sm btn-group">
                                                    <a class="btn btn-sm btn-secondary" title="View" href="{{ route('show.parts.category', $category->id) }}">
                                                        view
                                                    </a>
                                                    <a class="btn btn-sm btn-warning" title="Edit" href="{{ route('edit.parts.category', $category->id) }}">
                                                        <i class="mdi mdi-playlist-edit"></i>
                                                    </a>
                                                    <button class="btn btn-sm btn-primary delete_parts_category_btn {{ !$category->deletable() ? "disabled" : null }}" title="Delete" {{ $category->deletable() ? : "disabled" }}
                                                        data-url="{{ route('delete.parts.category', $category->id) }}" data-name="{{ $category->category }}">
                                                        <i class="mdi mdi-delete"></i>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endslot
                @endcomponent
            @else
                @component('components.empty')
                    @slot('text')
                        No machine parts category found!
                    @endslot
                    @slot('body')
                        <button  class="btn btn-primary btn-lg waves-effect add_parts_category_btn">
                            Add New Category
                        </button>
                    @endslot
                @endcomponent
            @endif
        </div>
    </div>

    {{-- modal --}}
    @include('admin.settings.machines.part-categories.create')
@endsection

@push('js')
<script src="{{ asset('js/admin/machine.js') }}"></script>
<script>
    $(document).ready(function() {

        simpleDataTable('.type_table');

        @if (session('new'))
            openAddCategoryModal()
        @endif

        @if (session('created'))
            Swal.fire(
                'Machine Parts Category Created',
                '{{ session('created') }}',
                'success'
            );
        @endif

        @if (session('deleted'))
            Swal.fire(
                'Machine Parts Category Deleted',
                '{{ session('deleted') }}',
                'success'
            );
        @endif
    });
</script>
@endpush