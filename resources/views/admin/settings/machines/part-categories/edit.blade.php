{{--  add machine fault modal  --}}
<form method="POST" action="{{ route('update.parts.category', $category->id) }}" id="update_parts_category_form">
    @component('components.modal', [
        'modal_dialog_class' => 'modal-dialog-centered animated bounceInRight',
        'modal_id' => 'update_parts_category_modal',
        'modal_top_dismiss' => true,
    ])
        @slot('modal_header')
            <h5>
                <i class="mdi mdi-library-plus ml-2 text-primary"></i>
                Update Machine Parts Category - {{ Str::limit($category->category, 10, '...') }}
            </h5>
        @endslot

        @slot('modal_body')
            @csrf
            <div class="form_response"></div>
            {{--  category field  --}}
            @component('components.form.input-form-group', [
                'label' => 'Category',
                'name' => 'category',
                'placeholder' => 'category here',
                'required' => true,
                'value' => $category->category
            ])
                
            @endcomponent

            {{--  description here  --}}
            @component('components.form.textarea-form-group', [
                'name' => 'description',
                'placeholder' => 'description here...',
                'label' => 'Description',
                'value' => $category->description
            ])
            @endcomponent

        @endslot

        @slot('modal_footer')
            <button class="btn btn-default" type="button" data-dismiss="modal">
                Exit
            </button>
            <button class="btn btn-primary" type="submit">
                Save
            </button>
        @endslot
    @endcomponent
</form>