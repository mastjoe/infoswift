{{--  add machine fault modal  --}}
<form method="POST" action="{{ route('update.machine.status', $status->id) }}" id="edit_status_form">
    @component('components.modal', [
        'modal_dialog_class' => 'modal-dialog-centered animated bounceInRight',
        'modal_id' => 'edit_status_modal',
        'modal_top_dismiss' => true,
    ])
        @slot('modal_header')
            <h5>
                <i class="mdi mdi-library-plus ml-2 text-primary"></i>
                Update Machine Status
            </h5>
        @endslot

        @slot('modal_body')
            @csrf
            @method('put')
            <div class="form_response"></div>
            {{--  fault field  --}}
            @component('components.form.input-form-group', [
                'label' => 'Status',
                'name' => 'status',
                'placeholder' => 'status here',
                'required' => true,
                'value' => $status->status
            ])
                
            @endcomponent

            {{--  description here  --}}
            @component('components.form.textarea-form-group', [
                'name' => 'description',
                'placeholder' => 'description here...',
                'label' => 'Description',
                'value' => $status->description
            ])
                
            @endcomponent

        @endslot

        @slot('modal_footer')
            <button class="btn btn-default" type="button" data-dismiss="modal">
                Exit
            </button>
            <button class="btn btn-primary" type="submit">
                Save
            </button>
        @endslot
    @endcomponent
</form>