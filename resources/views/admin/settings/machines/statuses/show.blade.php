@extends('layouts.admin.app')
@section('title', 'Machine Status | '.$status->status)

@section('page', 'Machine Status | '.$status->status)

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('settings.machines') }}">Settings - Machine</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('all.machine.statuses') }}">Statuses</a>
    </li>
    <li class="breadcrumb-item active">
        {{ $status->status }}
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="col-12 text-right mb-2">
            <button class="btn btn-warning edit_status_btn">
                <span class="d-none d-md-inline-block">Edit</span>
                <span class="mdi mdi-pencil ml-md-2"></span>
            </button>
            <button class="btn btn-primary delete_status_btn" data-status="{{ $status->status }}"
                data-url="{{ route('delete.machine.status', $status->id) }}">
                <span class="d-none d-md-inline-block">Delete</span>
                <span class="mdi mdi-delete ml-md-2"></span>
            </button>
        </div>
        <div class="col-md-3">
            @component('components.card')
                @slot('card_body')
                    <strong class="text-primary">
                        Status
                    </strong>
                    <p>{{ $status->status }}</p>
                    <hr>
                    <strong class="text-primary">
                        Description
                    </strong>
                    <p>
                        @isset($status->description)
                            {{ $status->description }}
                        @else
                            <span class="text-muted">Not Available</span>
                        @endisset
                    </p>
                    <hr>
                    <strong class="text-primary">
                        Created At
                    </strong>
                    <p>{{ $status->created_at->format('jS M, Y') }}</p>
                    <hr>
                    <strong class="text-primary">
                        Last Updated At
                    </strong>
                    <p>{{ $status->updated_at->format('jS M, Y') }}</p>
                @endslot
            @endcomponent
        </div>
        <div class="col-md-9">
            @component('components.card')
                @slot('card_body')
                    <h4 class="card-title">
                        Machines 
                    </h4>
                    <hr>
                @endslot
            @endcomponent
        </div>
    </div>

    {{-- modals --}}
    @include('admin.settings.machines.statuses.edit')
@endsection

@push('js')
    <script src="{{ asset('js/admin/machine.js') }}"></script>
    <script>
        $(document).ready(function() {
            @if (session('edit'))
                openEditStatusModal();
            @endif

            @if (session('updated'))
                Swal.fire(
                    'Machine Status Updated!',
                    '{{ session('updated') }}',
                    'success'
                );
            @endif
        })
    </script>
@endpush