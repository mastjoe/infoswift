@extends('layouts.admin.app')
@section('title', 'Machines | Statuses')


@section('page', 'Machine | Statuses')
@push('css')
@endpush

@section('crumbs')
<li class="breadcrumb-item">
    <a href="{{ route('settings.machines') }}">Settings - Machine</a>
</li>
<li class="breadcrumb-item active">
    Statuses
</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            @if ($statuses->count())
                <div class="text-right mb-2">
                    <button class="btn btn-primary waves-effect add_status_btn">
                        <span class="d-none d-md-inline-block">New Status</span>
                        <i class="mdi mdi-plus-circle-outline mr-1"></i>
                    </button>
                </div>
                @component('components.card')
                    @slot('card_body')
                        <div class="table table-responsive-sm">
                            <table class="table table-bordered status_table">
                                <thead>
                                    <tr>
                                        <th>Status</th>
                                        <th>Machines</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($statuses as $status)
                                        <tr>
                                            <td>
                                                <a href="{{ route('show.machine.status', $status->id) }}">{{ $status->status }}</a>
                                            </td>
                                            <td>
                                                <span class="badge badge-primary">
                                                    {{ number_format($status->machines->count()) }}
                                                </span>
                                            </td>
                                            <td>
                                                <div class="btn-group-sm btn-group">
                                                    <a class="btn btn-sm btn-secondary" title="View" href="{{ route('show.machine.status', $status->id) }}">
                                                         view
                                                     </a>
                                                     <a class="btn btn-sm btn-warning" title="Edit" href="{{ route('edit.machine.status', $status->id) }}">
                                                         <i class="mdi mdi-playlist-edit"></i>
                                                     </a>
                                                     <button class="btn btn-sm btn-primary delete_status_btn" title="Delete"
                                                         data-url="{{ route('delete.machine.status', $status->id) }}" data-status="{{ $status->status }}">
                                                         <i class="mdi mdi-delete"></i>
                                                     </button>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endslot
                @endcomponent
            @else
                @component('components.empty')
                    @slot('text')
                        No machine status found!
                    @endslot
                    @slot('body')
                        <button class="btn btn-primary btn-lg waves-effect add_status_btn">
                            Add New Status
                        </button>
                    @endslot
                @endcomponent
            @endif
        </div>
    </div>

    {{-- modals --}}
    @include('admin.settings.machines.statuses.add')
@endsection

@push('js')
    <script src="{{ asset('js/admin/machine.js') }}"></script>
    <script>
        $(document).ready(function() {
            @if (session('created'))
                Swal.fire(
                    'Machine Status Created!',
                    '{{ session('created') }}',
                    'success'
                );
            @endif

            @if (session('new'))
                openAddStatusModal();
            @endif

            @if (session('deleted'))
                Swal.fire(
                    'Status Deleted!',
                    '{{ session('deleted') }}',
                    'success'
                );
            @endif

            simpleDataTable('.status_table');
        });
    </script>
@endpush