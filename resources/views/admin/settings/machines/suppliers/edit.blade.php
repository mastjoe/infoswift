@extends('layouts.admin.app')
@section('title', 'Machines | Edit Part Supplier - '.$supplier->name)

@section('page', 'Machine | Edit Supplier')

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('settings.machines') }}">Settings - Machine</a>
    </li>
    <li class="breadcrumb-item ">
        <a href="{{ route('all.parts.suppliers') }}">Parts Supplier</a>
    </li>
    <li class="breadcrumb-item ">
        <a href="{{ route('show.parts.supplier', $supplier->id) }}">
            {{ Str::limit($supplier->name, 15, '...') }}
        </a>
    </li>
    <li class="breadcrumb-item active">
        Edit
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="offset-lg-2 col-lg-8">
            <form method="POST" action="{{ route('update.parts.supplier', $supplier->id) }}">
                @csrf
                @method('put')
                @component('components.card')
                    @slot('card_header')
                        <h5 class="card-title">
                            Update Machine Part Supplier - 
                            <span class="text-muted">{{ $supplier->name }}</span>
                        </h5>
                    @endslot
                    @slot('card_body')
                        {{--  name of supplier  --}}
                        @component('components.form.input-form-group', [
                            'label' => 'Name',
                            'name' => 'name',
                            'placeholder' => 'supplier name here',
                            'required' => true,
                            'value' => old('name') ?? $supplier->name
                        ])
                            
                        @endcomponent
                        {{--  email  --}}
                        @component('components.form.input-form-group', [
                            'label' => 'Email',
                            'name' => 'email',
                            'placeholder' => 'supplier\'s email here',
                            'required' => true,
                            'type' => 'email',
                            'value' => old('email') ?? $supplier->email
                        ])
                            
                        @endcomponent
                        {{--  phone  --}}
                        @component('components.form.input-form-group', [
                            'label' => 'Phone',
                            'name' => 'phone',
                            'placeholder' => 'supplier\'s telephone here',
                            'required' => true,
                            'type' => 'tel',
                            'value' => old('phone') ?? $supplier->phone
                        ])
                            
                        @endcomponent
                        {{--  address  --}}
                        @component('components.form.textarea-form-group', [
                            'label' => 'Address',
                            'name' => 'address',
                            'placeholder' => 'supplier\'s address here (optional)',
                            'required' => false,
                            'value' => old('address') ?? $supplier->address 
                        ])
                        @endcomponent
                        {{--  more info  --}}
                        @component('components.form.textarea-form-group', [
                            'label' => 'More Info',
                            'name' => 'more_info',
                            'placeholder' => 'additional information worth noting about supplier',
                            'required' => false,
                            'value' => old('more_info') ?? $supplier->more_info
                        ])
                        @endcomponent

                        <hr>
                        <div class="text-center">
                            <button class="btn btn-primary waves-effect" type="submit">
                                Save Supplier
                            </button>
                        </div>
                    @endslot
                @endcomponent
            </form>
        </div>
    </div>
@endsection

@push('js')
<script src="{{ asset('js/admin/machine.js') }}"></script>
<script>
    $(document).ready(function() {

        simpleDataTable('.type_table');

        @if (session('new'))
            openAddTypeModal();
        @endif

        @if (session('created'))
            Swal.fire(
                'Machine Type Added',
                '{{ session('created') }}',
                'success'
            );
        @endif

        @if (session('deleted'))
            Swal.fire(
                'Machine Type Deleted',
                '{{ session('deleted') }}',
                'success'
            );
        @endif

        @if (session('updated'))
            Swal.fire(
                'Machine Type Updated',
                '{{ session('updated') }}',
                'success'
            );
        @endif
    });
</script>
@endpush