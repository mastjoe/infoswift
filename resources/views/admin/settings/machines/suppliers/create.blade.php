@extends('layouts.admin.app')
@section('title', 'Machines | New Part Supplier')

@section('page', 'Machine | New Supplier')

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('settings.machines') }}">Settings - Machine</a>
    </li>
    <li class="breadcrumb-item ">
        <a href="{{ route('all.parts.suppliers') }}">Parts Supplier</a>
    </li>
    <li class="breadcrumb-item active">
        New
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="offset-lg-2 col-lg-8">
            <form method="POST" action="{{ route('store.parts.supplier') }}">
                @csrf
                @component('components.card')
                    @slot('card_header')
                        <h5 class="card-title">
                            New Machine Part Supplier
                        </h5>
                    @endslot
                    @slot('card_body')
                        {{--  name of supplier  --}}
                        @component('components.form.input-form-group', [
                            'label' => 'Name',
                            'name' => 'name',
                            'placeholder' => 'supplier name here',
                            'required' => true,
                        ])
                            
                        @endcomponent
                        {{--  email  --}}
                        @component('components.form.input-form-group', [
                            'label' => 'Email',
                            'name' => 'email',
                            'placeholder' => 'supplier\'s email here',
                            'required' => true,
                            'type' => 'email'
                        ])
                            
                        @endcomponent
                        {{--  phone  --}}
                        @component('components.form.input-form-group', [
                            'label' => 'Phone',
                            'name' => 'phone',
                            'placeholder' => 'supplier\'s telephone here',
                            'required' => true,
                            'type' => 'tel'
                        ])
                            
                        @endcomponent
                        {{--  address  --}}
                        @component('components.form.textarea-form-group', [
                            'label' => 'Address',
                            'name' => 'address',
                            'placeholder' => 'supplier\'s address here (optional)',
                            'required' => false,
                        ])
                        @endcomponent
                        {{--  more info  --}}
                        @component('components.form.textarea-form-group', [
                            'label' => 'More Info',
                            'name' => 'more_info',
                            'placeholder' => 'additional information worth noting about supplier',
                            'required' => false,
                        ])
                        @endcomponent

                        <hr>
                        <div class="text-center">
                            <button class="btn btn-primary waves-effect" type="submit">
                                Add Supplier
                            </button>
                        </div>
                    @endslot
                @endcomponent
            </form>
        </div>
    </div>
@endsection

@push('js')
<script src="{{ asset('js/admin/machine.js') }}"></script>
<script>
    $(document).ready(function() {

        simpleDataTable('.type_table');

        @if (session('new'))
            openAddTypeModal();
        @endif

        @if (session('created'))
            Swal.fire(
                'Machine Type Added',
                '{{ session('created') }}',
                'success'
            );
        @endif

        @if (session('deleted'))
            Swal.fire(
                'Machine Type Deleted',
                '{{ session('deleted') }}',
                'success'
            );
        @endif

        @if (session('updated'))
            Swal.fire(
                'Machine Type Updated',
                '{{ session('updated') }}',
                'success'
            );
        @endif
    });
</script>
@endpush