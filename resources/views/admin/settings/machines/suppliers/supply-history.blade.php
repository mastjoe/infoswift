@extends('layouts.admin.app')

@section('title', 'Machines | Parts Suppliers - '.$supplier->name. ' | Supply History')

@section('page', 'Machine | Parts Suppliers - '.$supplier->name.' | Supply History')

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('settings.machines') }}">Settings - Machine</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('all.parts.suppliers') }}">Parts Suppliers</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('show.parts.supplier', $supplier->id) }}">{{ Str::limit($supplier->name, 15, '...') }}</a>
    </li>
    <li class="breadcrumb-item active">
        Supply History
    </li>
@endsection

@section('content')
    <div class="row">
       <div class="col-12">
           @if ($supplier->stock_supplies->count())
                @component('components.card')
                    @slot('card_body')
                        <h4 class="card-title">
                            Stock Supply History
                        </h4>
                        <hr>
                        <div>
                            <table class="table table-hover supply_table">
                                <thead>
                                    <th>Reference</th>
                                    <th>Part</th>
                                    <th>Quantity</th>
                                    <th>Price</th>
                                    <th>Date</th>
                                </thead>
                                <tbody>
                                    @foreach ($supplier->stock_supplies()->orderBy('id', 'desc')->get() as $supply)
                                        <tr>
                                            <td>
                                                {{ $supply->reference }}
                                            </td>
                                            <td>
                                                {{ $supply->stock->machine_part->part }}
                                            </td>
                                            <td>
                                                {{ number_format($supply->initial_quantity) }}
                                            </td>
                                            <td>
                                                {{ number_format($supply->cost_price, 2) }}
                                            </td>
                                            <td>
                                                {{ $supply->created_at->format('jS M, Y') }}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endslot
                @endcomponent
           @else
               <div class="text-center my-5">
                   <h2 class="text-muted">No supply found from supplier</h2>
               </div>
           @endif
       </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/admin/machine.js') }}"></script>
    <script>
        $(document).ready(function() {
            simpleDataTable('.supply_table', {ordering: false})
            @if (session('updated'))
                Swal.fire(
                    'Supplier Uodated!',
                    '{{ session('updated') }}',
                    'success'
                );
            @endif
        })
    </script>
@endpush