@extends('layouts.admin.app')
@section('title', 'Machines | Parts Suppliers')

@section('page', 'Machine | Parts Suppliers')

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('settings.machines') }}">Settings - Machine</a>
    </li>
    <li class="breadcrumb-item active">
        Parts Supplier
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            @if ($suppliers->count())
                <div class="text-right mb-2">
                    <a class="btn btn-primary waves-effect add_type_btn" href="{{ route('create.parts.supplier') }}">
                        <span class="d-none d-md-inline-block">New Supplier</span>
                        <i class="mdi mdi-plus-circle-outline mr-1"></i>
                    </a>
                </div>

                @component('components.card')
                    @slot('card_body')
                        <h4 class="card-title">
                            All Machine Parts Suppliers
                        </h4>
                        <hr>
                        <div class="table-responsive-sm">
                            <table class="table table-bordered type_table">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($suppliers as $supplier)
                                        <tr>
                                            <td>
                                                <a>{{ $supplier->name }}</a>
                                            </td>
                                            <td>
                                                {{ $supplier->email }}
                                            </td>
                                            <td>
                                                {{ $supplier->phone }}
                                            </td>
                                            <td>
                                                <div class="btn-group-sm btn-group">
                                                    <a class="btn btn-sm btn-secondary" title="View" href="{{ route('show.parts.supplier', $supplier->id) }}">
                                                        view
                                                    </a>
                                                    <a class="btn btn-sm btn-warning" title="Edit" href="{{ route('edit.parts.supplier', $supplier->id) }}">
                                                        <i class="mdi mdi-playlist-edit"></i>
                                                    </a>
                                                    <button class="btn btn-sm btn-primary delete_supplier_btn" title="Delete"
                                                        data-url="{{ route('delete.parts.supplier', $supplier->id) }}" data-supplier="{{ $supplier->name }}">
                                                        <i class="mdi mdi-delete"></i>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endslot
                @endcomponent
            @else
                @component('components.empty')
                    @slot('text')
                        No machine parts supplier found!
                    @endslot
                    @slot('body')
                        <a href="{{ route('create.parts.supplier') }}" class="btn btn-primary btn-lg waves-effect">
                            Add New Supplier
                        </a>
                    @endslot
                @endcomponent
            @endif
        </div>
    </div>
@endsection

@push('js')
<script src="{{ asset('js/admin/machine.js') }}"></script>
<script>
    $(document).ready(function() {

        simpleDataTable('.type_table');

        @if (session('new'))
            openAddTypeModal();
        @endif

        @if (session('created'))
            Swal.fire(
                'Supplier Record Created',
                '{{ session('created') }}',
                'success'
            );
        @endif

        @if (session('deleted'))
            Swal.fire(
                'Supplier Record Deleted',
                '{{ session('deleted') }}',
                'success'
            );
        @endif
    });
</script>
@endpush