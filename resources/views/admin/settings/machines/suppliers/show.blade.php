@extends('layouts.admin.app')

@section('title', 'Machines | Parts Suppliers - '.$supplier->name)

@section('page', 'Machine | Parts Suppliers - '.$supplier->name)

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('settings.machines') }}">Settings - Machine</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('all.parts.suppliers') }}">Parts Suppliers</a>
    </li>
    <li class="breadcrumb-item active">
        {{ Str::limit($supplier->name, 15, '...') }}
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="col-12 text-right mb-2">
            <a class="btn btn-warning" href="{{ route('edit.parts.supplier', $supplier->id) }}">
                <span class="d-none d-md-inline-block">Edit</span>
                <span class="mdi mdi-pencil ml-md-2"></span>
            </a>
            @if ($supplier->deletable())
                
            <button class="btn btn-primary delete_supplier_btn" data-supplier="{{ $supplier->name }}"
                data-url="{{ route('delete.parts.supplier', $supplier->id) }}">
                <span class="d-none d-md-inline-block">Delete</span>
                <span class="mdi mdi-delete ml-md-2"></span>
            </button>
            @endif
        </div>
        <div class="col-md-3">
            @component('components.card')
                @slot('card_body')
                    <strong class="text-primary">
                        Name
                    </strong>
                    <p>{{ $supplier->name }}</p>
                    <hr>
                    <strong class="text-primary">
                        Phone
                    </strong>
                    <p>{{ $supplier->phone }}</p>
                    <hr>
                    <strong class="text-primary">
                        Email
                    </strong>
                    <p>{{ $supplier->email }}</p>
                    <hr>
                    <strong class="text-primary">
                        Address
                    </strong>
                    <p>
                        @isset($supplier->address)
                            {{ $supplier->address }}
                        @else
                            <span class="text-muted">Not available</span>
                        @endisset
                    </p>
                    <hr>
                    <strong class="text-primary">
                        Created At
                    </strong>
                    <p>{{ $supplier->created_at->format('jS M, Y') }}</p>
                    <hr>
                    <strong class="text-primary">
                        Last Updated At
                    </strong>
                    <p>{{ $supplier->updated_at->format('jS M, Y') }}</p>
                @endslot
            @endcomponent
        </div>
        <div class="col-md-9">
            {{-- more info --}}
            @component('components.card')
                @slot('card_header')
                    <b>More Info</b>
                @endslot
                @slot('card_body')
                    <div>
                        @isset($supplier->more_info)
                            {{ $supplier->more_info }}
                        @else
                            <div class="text-center text-muted">
                                <h5>No additional information found about supplier</h5>
                            </div>
                        @endisset
                    </div>
                @endslot
            @endcomponent

             {{-- stock --}}
            @component('components.card')
                @slot('card_header')
                    <b>Stock Supplies</b>
                @endslot
                @slot('card_body')
                   <div>
                       @if ($supplier->stock_supplies->count())
                            <table class="table table-bordered table-hover supply_table">
                                <thead>
                                    <tr>
                                        <th>Reference</th>
                                        <th>Part</th>
                                        <th>Quantity</th>
                                        <th>Price</th>
                                        <th>Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($supplier->stock_supplies()->orderBy('id', 'desc')->take(10)->get() as $supply)
                                        <tr>
                                            <td>
                                                {{ $supply->reference }}
                                            </td>
                                            <td>
                                                {{ $supply->stock->machine_part->part }}
                                            </td>
                                            <td>
                                                {{ number_format($supply->initial_quantity) }}
                                            </td>
                                            <td>
                                                {{ number_format($supply->cost_price, 2)}}
                                            </td>
                                            <td>
                                                {{ $supply->created_at->format('jS M, Y') }}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>  
                            <div class="text-center">
                                <a href="{{ route('show.parts.supplier.history', $supplier->id) }}">See More</a>
                            </div>
                       @else
                           <div class="text-center my-5">
                               <h4 class="text-muted">No supply from supplier yet!</h4>
                           </div>
                       @endif
                   </div>
                @endslot
            @endcomponent
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/admin/machine.js') }}"></script>
    <script>
        $(document).ready(function() {
            simpleDataTable('.supply_table', {ordering: false})
            @if (session('updated'))
                Swal.fire(
                    'Supplier Uodated!',
                    '{{ session('updated') }}',
                    'success'
                );
            @endif
        })
    </script>
@endpush