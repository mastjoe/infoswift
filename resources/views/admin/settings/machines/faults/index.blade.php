@extends('layouts.admin.app')
@section('title', 'Machines | Faults')

@section('page', 'Machines | Faults')

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('settings.machines') }}">Settings - Machine</a>
    </li>
    <li class="breadcrumb-item active">
        Faults
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            @if ($faults->count())
                <div class="text-right mb-2">
                   <button class="btn btn-primary waves-effect add_fault_btn">
                        <span class="d-none d-md-inline-block">New Fault</span>
                        <i class="mdi mdi-plus-circle-outline mr-1"></i>
                   </button>
                </div>
                @if ($new_faults->count())
                    <div class="alert alert-dark">
                        {{ number_format($new_faults->count()) }} machine fault{{ $new_faults->count() > 1 ? 's are': ' is ' }}
                        in need of review
                        <button class="btn btn-light btn-sm ml-2 new_fault_btn">See New Faults</button>
                    </div>
                @endif
                @component('components.card')
                    @slot('card_body') 
                    <h4 class="card-title">
                        All Machine Faults
                    </h4>       
                    <hr>               
                    <div class="table-responsive-sm">
                        <table class="table table-bordered fault_table">
                            <thead>
                                <tr>
                                    <th>Fault</th>
                                    <th>Tickets</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($faults as $fault)
                                    <tr>
                                        <td>
                                            {{ $fault->fault }}
                                            @if ($fault->new)
                                                <span class="ml-2 badge badge-primary">NEW</span>
                                            @endif
                                        </td>
                                        <td>
                                            
                                        </td>
                                        <td>
                                            <div class="btn-group btn-group-sm">
                                                <a class="btn btn-sm btn-secondary" title="View"
                                                    href="{{ route('show.machine.fault', $fault->id) }}"
                                                >
                                                    view
                                                </a>
                                                <a class="btn btn-sm btn-warning"
                                                    title="Edit"
                                                    href="{{ route('edit.machine.fault', $fault->id) }}"
                                                >
                                                    <i class="mdi mdi-playlist-edit"></i>
                                                </a>
                                                <button class="btn btn-sm btn-primary delete_fault_btn {{ $fault->deletable() ? null: 'disabled' }}"
                                                    title="Delete"
                                                    data-url="{{ route('delete.machine.fault', $fault->id) }}"
                                                    data-fault="{{ $fault->fault }}"
                                                    {{ $fault->deletable() ? null: 'disabled' }}
                                                >
                                                    <i class="mdi mdi-delete"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @endslot
                @endcomponent
            @else
                @component('components.empty')
                    @slot('text')
                        No machine fault found!
                    @endslot
                    @slot('body')
                        <button class="btn btn-primary btn-lg waves-effect add_fault_btn">
                            Add New Fault
                        </button>
                    @endslot
                @endcomponent
            @endif
        </div>
    </div>

    {{--  modal  --}}
    @include('admin.settings.machines.faults.add')
    @include('admin.settings.machines.faults.new-faults')
@endsection

@push('js')
    <script src="{{ asset('js/admin/machine.js') }}"></script>
    <script>
        @if (session('create'))
            openFaultModal();
        @endif

        @if (session('new'))
            Swal.fire(
                'New Fault Added',
                '{{ session('new') }}',
                'success'
            );
        @endif

        @if (session('deleted'))
            Swal.fire(
                'Machine Fault Deleted',
                '{{ session('deleted') }}',
                'success'
            );
        @endif

        $('.fault_table').DataTable();
        $('.new_fault_btn').on('click', function() {
            $('#new_faults').modal({
                backdrop: 'static'
            });
        });
    </script>
@endpush