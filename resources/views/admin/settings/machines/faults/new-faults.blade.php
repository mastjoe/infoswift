@component('components.modal', [
    'modal_dialog_class' => '',
    'modal_id' => 'new_faults',
    'modal_top_dismiss' => true,
    ])
    @slot('modal_header')
        <h6 class="modal-title">
            New Faults
        </h6>
    @endslot
    @slot('modal_body')
        <div>
            @if ($new_faults->count())
                <ul class="list-group list-group-flush">
                    @foreach ($new_faults as $key => $fault)
                        <a class="list-group-item font-weight-bold" href="{{ route('show.machine.fault', $fault->id) }}">
                            <span>{{ $key +1 }}</span>. 
                            {{ $fault->fault }}
                        </a>
                    @endforeach
                </ul>
            @else
                <div class="text-center my-5">
                    <h1 class="text-muted">No new fault found</h1>
                </div>
            @endif
        </div>
    @endslot
    @slot('modal_footer')
        <button class="btn btn-secondary" data-dismiss="modal">
            Exit
        </button>
    @endslot
@endcomponent