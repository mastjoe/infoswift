{{--  add machine fault modal  --}}
<form method="POST" action="{{ route('update.machine.fault', $fault->id) }}" id="edit_fault_form">
    @component('components.modal', [
        'modal_dialog_class' => 'modal-dialog-centered animated bounceInRight',
        'modal_id' => 'edit_fault_modal',
        'modal_top_dismiss' => true,
    ])
        @slot('modal_header')
            <h5>
                <i class="mdi mdi-library-plus ml-2 text-primary"></i>
                Update Machine Fault
            </h5>
        @endslot

        @slot('modal_body')
            @csrf
            @method('put')
            <div class="form_response"></div>
            {{--  fault field  --}}
            @component('components.form.input-form-group', [
                'label' => 'Fault',
                'name' => 'fault',
                'placeholder' => 'fault here',
                'required' => true,
                'value' => $fault->fault
            ])
                
            @endcomponent

            {{--  description here  --}}
            @component('components.form.textarea-form-group', [
                'name' => 'description',
                'placeholder' => 'description here...',
                'label' => 'Description',
                'value' => $fault->description
            ])
                
            @endcomponent

        @endslot

        @slot('modal_footer')
            <button class="btn btn-default" type="button" data-dismiss="modal">
                Exit
            </button>
            @if ($fault->new)
                <button class="btn btn-primary" type="submit">
                Confirm
                </button> 
            @else                
                <button class="btn btn-primary" type="submit">
                    Save
                </button>
            @endif
        @endslot
    @endcomponent
</form>