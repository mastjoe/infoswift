@extends('layouts.admin.app')
@section('title', 'Machine Faults | '.$fault->fault)

@section('page', 'Machine Fault | '.Str::limit($fault->fault, 25, '...'))

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('settings.machines') }}">Settings - Machine</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('all.machine.faults') }}">Faults</a>
    </li>
    <li class="breadcrumb-item active">
        Fault
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="col-12 text-right mb-2">
            <button class="btn btn-warning edit_fault_btn">
                <span class="d-none d-md-inline-block">Edit</span>
                <span class="mdi mdi-pencil ml-md-2"></span>
            </button>
            <button class="btn btn-primary delete_fault_btn"
                data-fault="{{ $fault->fault }}"
                data-url="{{ route('delete.machine.fault', $fault->id) }}"
            >
                <span class="d-none d-md-inline-block">Delete</span>
                <span class="mdi mdi-delete ml-md-2"></span>
            </button>
        </div>
        @if ($fault->new)
            <div class="col-12">
                <div class="alert alert-dark">
                    Fault is new added and may need review!
                    <a class="alert-link edit_fault_btn" href="javascript:void(0)">Review Fault</a>
                </div>
            </div>
        @endif
        <div class="col-md-3">
            @component('components.card')
                @slot('card_body')
                    <strong class="text-primary">Fault</strong>
                    <p>{{ $fault->fault }}</p>
                    <hr>
                    <strong class="text-primary">Description</strong>
                    <p>
                        @isset($fault->description)
                            {{ $fault->description }}
                        @else
                            <span class="text-muted">Not available</span>
                        @endisset
                    </p>
                    <hr>
                    <strong class="text-primary">Create At</strong>
                    <p>{{ $fault->created_at->format('jS M, Y') }}</p>
                    <hr>
                    <strong class="text-primary">Updated At</strong>
                    <p>{{ $fault->updated_at->format('jS M, Y') }}</p>
                @endslot
            @endcomponent
        </div>
        <div class="col-md-9">
            {{--  tickets  --}}
            @component('components.card')
                @slot('card_body')
                    <h5 class="card-title">
                        Ticket History
                    </h5>
                    <hr>
                    <div>
                        @if ($fault->ticketFaults->count())
                            <table class="table table-bordered tickets-table">
                                <thead>
                                    <tr>
                                        <th>Reference</th>
                                        <th>Terminal</th>
                                        <th>Raised</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($fault->ticketFaults as $ticket_fault)
                                        <tr>
                                            <td>
                                                @can('view', $ticket_fault->ticket)
                                                    <a href="{{ route('show.ticket', $ticket_fault->ticket->id) }}">{{ $ticket_fault->ticket->ref }}</a>
                                                @else
                                                    {{ $ticket_fault->ticket->ref }}
                                                @endcan
                                            </td>
                                            <td>
                                                {{ $ticket_fault->ticket->machine->terminal_id }}
                                            </td>
                                            <td>
                                                {{ $ticket_fault->ticket->created_at->format('jS M, Y') }}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="text-center my-5">
                                <h2 class="text-muted">
                                    No ticket history
                                </h2>
                            </div>
                        @endif
                    </div>
                @endslot
            @endcomponent
        </div>
    </div>

    {{--  modals  --}}
    @include('admin.settings.machines.faults.edit')
@endsection

@push('js')
    <script src="{{ asset('js/admin/machine.js') }}"></script>
    <script>
        $(document).ready(function(){
            @if (session('edit'))
                openEditFaultModal();
            @endif

            @if (session('updated'))
                Swal.fire(
                    'Machine Fault Updated!',
                    '{{ session('updated') }}',
                    'success'
                );
            @endif

            simpleDataTable('.tickets-table')
        });
    </script>
@endpush