@extends('layouts.admin.app')
@section('title', 'Settings | Machines')

@section('page', 'Settings | Machines')

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item active">
        <a>Settings - Machine</a>
    </li>
@endsection

@section('content')
    <div class="row">
        @can('manageFaults', App\Machine::class)            
        <div class="col-md-4">
            <a href="{{ route('all.machine.faults') }}">
                @component('components.card')
                    @slot('card_body')
                        <h4>Fault</h4>
                    @endslot
                @endcomponent
            </a>
        </div>
        @endcan

        @can('manageVendors', App\Machine::class)
        <div class="col-md-4">
            <a href="{{ route('all.machine.vendors') }}">
                @component('components.card')
                    @slot('card_body')
                        <h4>Vendors</h4>
                    @endslot
                @endcomponent
            </a>
        </div>
        @endcan
        
        @can('manageTypes', App\Machine::class)            
        <div class="col-md-4">
            <a href="{{ route('all.machine.types') }}">
                @component('components.card')
                    @slot('card_body')
                        <h4>Types</h4>
                    @endslot
                @endcomponent
            </a>
        </div>
        @endcan

        @can('manageStatuses', App\Machine::class)            
        <div class="col-md-4">
            <a href="{{ route('all.machine.statuses') }}">
                @component('components.card')
                    @slot('card_body')
                        <h4>Statuses</h4>
                    @endslot
                @endcomponent
            </a>
        </div>
        @endcan

        @can('manageCategories', App\MachinePart::class)            
        <div class="col-md-4">
            <a href="{{ route('all.parts.categories') }}">
                @component('components.card')
                    @slot('card_body')
                        <h4>Parts Category</h4>
                    @endslot
                @endcomponent
            </a>
        </div>
        @endcan

        @can('manageSupplier', App\MachinePart::class)
        <div class="col-md-4">
            <a href="{{ route('all.parts.suppliers') }}">
                @component('components.card')
                    @slot('card_body')
                        <h4>Parts Supplier</h4>
                    @endslot
                @endcomponent
            </a>
        </div>
        @endcan
    </div>
@endsection

@push('js')
@endpush