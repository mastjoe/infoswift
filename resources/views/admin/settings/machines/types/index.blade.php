@extends('layouts.admin.app')
@section('title', 'Machines | Machine Types')

@section('page', 'Machine | Types')

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('settings.machines') }}">Settings - Machine</a>
    </li>
    <li class="breadcrumb-item active">
        Types
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            @if ($types->count())
                <div class="text-right mb-2">
                    <button class="btn btn-primary waves-effect add_type_btn">
                        <span class="d-none d-md-inline-block">New Type</span>
                        <i class="mdi mdi-plus-circle-outline mr-1"></i>
                    </button>
                </div>

                @component('components.card')
                    @slot('card_body')
                        <h4 class="card-title">
                            All Machine Types
                        </h4>
                        <hr>
                        <div class="table-responsive-sm">
                            <table class="table table-bordered type_table">
                                <thead>
                                    <tr>
                                        <th>Type</th>
                                        <th>Model</th>
                                        <th>Class</th>
                                        <th>Machines</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($types as $type)
                                        <tr>
                                            <td>
                                               {{ $type->type }}
                                            </td>
                                            <td>
                                               {{ $type->model }}
                                            </td>
                                            <td>
                                                {{ $type->class }}
                                            </td>
                                            <td>
                                                <span class="badge badge-primary">
                                                    {{ number_format(0) }}
                                                </span>
                                            </td>
                                            <td>
                                                <div class="btn-group btn-group-sm">
                                                    <button class="btn btn-sm btn-warning edit_type_btn" title="Edit" data-url="{{ route('edit.machine.type', $type->id) }}">
                                                        <i class="mdi mdi-playlist-edit"></i>
                                                    </button>
                                                    <button class="btn btn-sm btn-primary delete_type_btn" title="Delete"
                                                        data-url="{{ route('delete.machine.type', $type->id) }}" data-type="{{ $type->type }}">
                                                        <i class="mdi mdi-delete"></i>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endslot
                @endcomponent
            @else
                @component('components.empty')
                    @slot('text')
                        No machine type found!
                    @endslot
                    @slot('body')
                        <button class="btn btn-primary btn-lg waves-effect add_type_btn">
                            Add New Type
                        </button>
                    @endslot
                @endcomponent
            @endif
        </div>
    </div>

    {{--  modals  --}}
    @include('admin.settings.machines.types.add')
@endsection

@push('js')
<script src="{{ asset('js/admin/machine.js') }}"></script>
<script>
    $(document).ready(function() {

        simpleDataTable('.type_table');

        @if (session('new'))
            openAddTypeModal();
        @endif

        @if (session('created'))
            Swal.fire(
                'Machine Type Added',
                '{{ session('created') }}',
                'success'
            );
        @endif

        @if (session('deleted'))
            Swal.fire(
                'Machine Type Deleted',
                '{{ session('deleted') }}',
                'success'
            );
        @endif

        @if (session('updated'))
            Swal.fire(
                'Machine Type Updated',
                '{{ session('updated') }}',
                'success'
            );
        @endif
    });
</script>
@endpush