{{--  add machine fault modal  --}}
<form method="POST" action="{{ route('store.machine.type') }}" id="add_type_form">
    @component('components.modal', [
        'modal_dialog_class' => 'modal-dialog-centered animated bounceInRight',
        'modal_id' => 'add_type_modal',
        'modal_top_dismiss' => true,
    ])
        @slot('modal_header')
            <h5>
                <i class="mdi mdi-library-plus ml-2 text-primary"></i>
                New Machine Type
            </h5>
        @endslot

        @slot('modal_body')
            @csrf
            <div class="form_response"></div>
            {{--  type field  --}}
            @component('components.form.input-form-group', [
                'label' => 'Type',
                'name' => 'type',
                'placeholder' => 'type here',
                'required' => true,
            ])
                
            @endcomponent

            {{--  model field here  --}}
            @component('components.form.input-form-group', [
                'name' => 'model',
                'placeholder' => 'model name here...',
                'label' => 'Model',
                'required' => true
            ])
                
            @endcomponent

            {{--  class field here  --}}
            @component('components.form.input-form-group', [
                'name' => 'class',
                'placeholder' => 'class here...',
                'label' => 'Class',
                'required' => true
            ])
                
            @endcomponent

        @endslot

        @slot('modal_footer')
            <button class="btn btn-default" type="button" data-dismiss="modal">
                Exit
            </button>
            <button class="btn btn-primary" type="submit">
                Add
            </button>
        @endslot
    @endcomponent
</form>