{{--  add machine fault modal  --}}
 <form method="POST" action="{{ route('update.machine.type', $type->id) }}" id="add_type_form" onsubmit="updateType(event)">
        <div class="modal-header">
            <h4 class="modal-title">
                <i class="mdi mdi-library-plus ml-2 text-primary"></i>
                Update Machine Type | {{ $type->type }}
            </h4>
            <button class="close" type="button" data-dismiss="modal">
                <span>&times;</span>
            </button>
        </div>
        <div class="modal-body">
            @csrf
            @method('put')
            <div class="form_response"></div>
                {{--  type field  --}}
                @component('components.form.input-form-group', [
                    'label' => 'Type',
                    'name' => 'type',
                    'placeholder' => 'type here',
                    'required' => true,
                    'value' => $type->type
                ])
                    
                @endcomponent
    
                {{--  model field here  --}}
                @component('components.form.input-form-group', [
                    'name' => 'model',
                    'placeholder' => 'model name here...',
                    'label' => 'Model',
                    'required' => true,
                    'value' => $type->model
                ])
                    
                @endcomponent
    
                {{--  class field here  --}}
                @component('components.form.input-form-group', [
                    'name' => 'class',
                    'placeholder' => 'class here...',
                    'label' => 'Class',
                    'required' => true,
                    'value' => $type->class
                ])
                    
                @endcomponent
        </div>
        <div class="modal-footer">
            <button class="btn btn-default" type="button" data-dismiss="modal">
                Exit
            </button>
            <button class="btn btn-primary" type="submit">
                Save
            </button>
        </div>
    </form>