{{--  edit machine vendor modal  --}}
<form method="POST" action="{{ route('update.machine.vendor', $vendor->id) }}" id="edit_vendor_form">
    @component('components.modal', [
        'modal_dialog_class' => 'modal-dialog-centered animated bounceInLeft',
        'modal_id' => 'edit_vendor_modal',
        'modal_top_dismiss' => true,
    ])
        @slot('modal_header')
            <h5>
                <i class="mdi mdi-library-plus ml-2 text-primary"></i>
                Update Machine Vendor
            </h5>
        @endslot

        @slot('modal_body')
            @csrf
            @method('put')
            <div class="form_response"></div>
            {{--  fault field  --}}
            @component('components.form.input-form-group', [
                'label' => 'Vendor',
                'name' => 'vendor',
                'placeholder' => 'vendor here',
                'required' => true,
                'value' => $vendor->vendor
            ])
                
            @endcomponent

            {{--  description here  --}}
            @component('components.form.textarea-form-group', [
                'name' => 'description',
                'placeholder' => 'description here...',
                'label' => 'Description',
                'value' => $vendor->description
            ])
                
            @endcomponent

        @endslot

        @slot('modal_footer')
            <button class="btn btn-default" type="button" data-dismiss="modal">
                Exit
            </button>
            <button class="btn btn-primary" type="submit">
                Save
            </button>
        @endslot
    @endcomponent
</form>