@extends('layouts.admin.app')
@section('title', 'Machine Vendors | '.$vendor->vendor)

@section('page', 'Machine Vendors | '.Str::limit($vendor->vendor, 20, '...'))

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('settings.machines') }}">Settings - Machine</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('all.machine.vendors') }}">Vendors</a>
    </li>
    <li class="breadcrumb-item active">
        {{ Str::limit($vendor->vendor, 10, '...') }}
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="col-12 text-right mb-2">
            <button class="btn btn-warning edit_vendor_btn">
                <span class="d-none d-md-inline-block">Edit</span>
                <span class="mdi mdi-pencil ml-md-2"></span>
            </button>
            <button class="btn btn-primary delete_vendor_btn" data-vendor="{{ $vendor->vendor }}"
                data-url="{{ route('delete.machine.vendor', $vendor->id) }}">
                <span class="d-none d-md-inline-block">Delete</span>
                <span class="mdi mdi-delete ml-md-2"></span>
            </button>
        </div>
        <div class="col-md-3">
            @component('components.card')
                @slot('card_body')
                    <strong class="text-primary">
                        Vendor
                    </strong>
                    <p>
                        {{ $vendor->vendor }}
                    </p>
                    <hr>
                    <strong class="text-primary">
                        Description
                    </strong>
                    <p>
                        @isset ($vendor->description)
                            {{ $vendor->description }}
                        @else
                            <span class="text-muted">Not Available</span>
                        @endisset
                    </p>
                    <hr>
                    <strong class="text-primary">
                        Created At
                    </strong>
                    <p>{{ $vendor->created_at->format('jS M, Y') }}</p>
                    <hr>
                    <strong class="text-primary">
                        Last Updated At
                    </strong>
                    <p>{{ $vendor->updated_at->format('jS M, Y') }}</p>
                @endslot
            @endcomponent
        </div>
        <div class="col-md-9">
            {{--  machines  --}}
            @component('components.card')
                @slot('card_body')
                    <h4 class="card-title">
                        Vendor Machines
                    </h4>
                    <hr>
                    @if ($vendor->machines->count())
                        <div>
                            <table class="table table-bordered vendor_table">
                                <thead>
                                    <th>Machine</th>
                                    <th>Client</th>
                                    <th>Branch</th>
                                    <th>Region</th>
                                </thead>
                                <tbody>
                                    @foreach ($vendor->machines as $machine)
                                        <tr>
                                            <td>
                                                @can('view', $machine)
                                                    <a href="{{ route('show.machine', $machine->id) }}">
                                                        {{ $machine->terminal_id }}
                                                    </a>
                                                @else
                                                    {{ $machine->terminal_id }}
                                                @endcan
                                            </td>
                                            <td>
                                                @can('view', $machine->client)
                                                    <a style="color:{{ $machine->client->color }};" href="{{ route('show.client', $machine->client_id) }}">
                                                        {{ $machine->client->short_name }}
                                                    </a>
                                                @else
                                                    <span style="color:{{ $machine->client->color }};">
                                                        {{ $machine->client->short_name }}
                                                    </span>
                                                @endcan
                                            </td>
                                            <td>
                                                {{ $machine->branch->branch }}
                                            </td>
                                            <td>
                                                {{ $machine->region->region }}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @else
                        <div class="text-center my-5">
                            <h4 class="text-muted">No machines found for vendor</h4>
                        </div>
                    @endif
                @endslot
            @endcomponent
        </div>
    </div>

    {{--  modals  --}}
    @include('admin.settings.machines.vendors.edit')
@endsection

@push('js')
    <script src="{{ asset('js/admin/machine.js') }}"></script>
    <script>
        $(document).ready(function() {
            simpleDataTable('.vendor_table');
            
            @if (session('edit'))
                openEditVendorModal();
            @endif

            @if (session('updated'))
                Swal.fire(
                    'Vendor Updated',
                    '{{ session('updated') }}',
                    'success'
                );
            @endif
        });
    </script>
@endpush