{{--  create modal  --}}
<form method="POST" action="{{ route('store.machine.vendor') }}" id="add_vendor_form">
    @component('components.modal', [
        'modal_dialog_class' => 'modal-dialog-centered animated bounceInLeft',
        'modal_id' => 'add_vendor_modal',
        'modal_top_dismiss' => true,
    ])
        @slot('modal_header')
            <h5>
                <i class="mdi mdi-library-plus ml-2 text-primary"></i>
                New Machine Vendor
            </h5>
        @endslot

        @slot('modal_body')
            @csrf
            <div class="form_response"></div>
            {{--  fault field  --}}
            @component('components.form.input-form-group', [
                'label' => 'Vendor',
                'name' => 'vendor',
                'placeholder' => 'vendor name here',
                'required' => true,
            ])
                
            @endcomponent

            {{--  description here  --}}
            @component('components.form.textarea-form-group', [
                'name' => 'description',
                'placeholder' => 'description here...',
                'label' => 'Description'
            ])
                
            @endcomponent

        @endslot

        @slot('modal_footer')
            <button class="btn btn-default" type="button" data-dismiss="modal">
                Exit
            </button>
            <button class="btn btn-primary" type="submit">
                Add
            </button>
        @endslot
    @endcomponent
</form>