@extends('layouts.admin.app')
@section('title', 'Machines | Machine Vendors')

@section('page', 'Machine | Vendors')

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('settings.machines') }}">Settings - Machine</a>
    </li>
    <li class="breadcrumb-item active">
        Vendors
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            @if ($vendors->count())
                <div class="text-right mb-2">
                    <button class="btn btn-primary waves-effect add_vendor_btn">
                        <span class="d-none d-md-inline-block">New Vendor</span>
                        <i class="mdi mdi-plus-circle-outline mr-1"></i>
                    </button>
                </div>

                @component('components.card')
                    @slot('card_body')
                        <h4 class="card-title">
                            All Machine Vendors
                        </h4>
                        <hr>
                        <div class="table-responsive-sm">
                            <table class="table table-bordered vendor_table">
                                <thead>
                                    <tr>
                                        <th>Vendor</th>
                                        <th>Machines</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($vendors as $vendor)
                                        <tr>
                                            <td>
                                                <a href="{{ route('show.machine.vendor', $vendor->id) }}">
                                                    {{ $vendor->vendor }}
                                                </a>
                                            </td>
                                            <td>
                                                <span class="badge badge-primary">
                                                    {{ number_format($vendor->machines->count()) }}
                                                </span>
                                            </td>
                                            <td>
                                                <div class="btn-group btn-group-sm">
                                                    <a class="btn btn-sm btn-secondary" title="View" href="{{ route('show.machine.vendor', $vendor->id) }}">
                                                        view
                                                    </a>
                                                    <a class="btn btn-sm btn-warning" title="Edit" href="{{ route('edit.machine.vendor', $vendor->id) }}">
                                                        <i class="mdi mdi-playlist-edit"></i>
                                                    </a>
                                                    <button class="btn btn-sm btn-primary delete_vendor_btn" title="Delete"
                                                        data-url="{{ route('delete.machine.vendor', $vendor->id) }}" data-vendor="{{ $vendor->vendor }}">
                                                        <i class="mdi mdi-delete"></i>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endslot
                @endcomponent
            @else
                @component('components.empty')
                    @slot('text')
                        No machine vendor found!
                    @endslot
                    @slot('body')
                        <button class="btn btn-primary btn-lg waves-effect add_vendor_btn">
                            Add New Vendor
                        </button>
                    @endslot
                @endcomponent
            @endif
        </div>
    </div>

    {{--  modals  --}}
    @include('admin.settings.machines.vendors.add')
@endsection

@push('js')
<script src="{{ asset('js/admin/machine.js') }}"></script>
<script>
    $(document).ready(function() {

        simpleDataTable('.vendor_table');

        @if (session('new'))
            openAddVendorModal();
        @endif

        @if (session('deleted'))
            Swal.fire(
                'Machine Vendor Deleted',
                '{{ session('deleted') }}',
                'success'
            );
        @endif
        @if (session('created'))
            Swal.fire(
                'Machine Vendor Created',
                '{{ session('created') }}',
                'success'
            );
        @endif
    });
</script>
@endpush