@extends('layouts.admin.app')
@section('title', 'Locations | Countries | '.$state->country->country.' | States | '.$state->state)

@section('page', 'Locations | States | '.Str::limit($state->state, 15, '...'))

@push('css')
<style>
.dropdown-toggle::after {
    display: none;
}
</style>
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('settings.locations') }}">Locations</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('show.country', $state->country_id) }}">{{ Str::limit($state->country->country, 10, '...') }}</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('all.states') }}">States</a>
    </li>
    <li class="breadcrumb-item active">
        {{ Str::limit($state->state, 15, '...') }}
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="col-12 mb-2 text-right">
            <button class="btn btn-success add_region_btn" title="Add Region">
                <span class="d-none d-md-inline-block">Add</span>
                <span class="mdi mdi-plus-circle ml-md-2"></span>
            </button>
            <button class="btn btn-warning edit_state_btn" title="Edit State">
                <span class="d-none d-md-inline-block">Edit</span>
                <span class="mdi mdi-pencil ml-md-2"></span>
            </button>
            @if ($state->deletable())                
                <button class="btn btn-primary delete_state_btn" 
                    title="Delete Country"
                    data-state="{{ $state->state }}"
                    data-url="{{ route('delete.state', $state->id) }}"
                    >
                    <span class="d-none d-md-inline-block">Delete</span>
                    <span class="mdi mdi-delete ml-md-2"></span>
                </button>
            @endif
        </div>
        <div class="col-md-3">
            @component('components.card')
                @slot('card_body')
                    <strong class="text-primary">State</strong>
                    <p>{{ $state->state }}</p>
                    <hr>
                    <strong class="text-primary">Country</strong>
                    <p>{{ $state->country->country }}</p>
                    <hr>
                    <strong class="text-primary">Zone/ Region</strong>
                    <p>{{ $state->zone }}</p>
                    <hr>
                    <strong class="text-primary">Created At</strong>
                    <p>{{ $state->created_at->format('jS M, Y') }}</p>
                    <hr>
                    <strong class="text-primary">Last Updated</strong>
                    <p>{{ $state->updated_at->format('jS M, Y') }}</p>
                    
                @endslot
            @endcomponent
        </div>
        <div class="col-md-9">
            {{-- states --}}
            @component('components.card')
                @slot('card_body')
                    <h4 class="card-title">
                        {{ $state->state }} Regions
                    </h4>
                    <hr>
                    @if (count($state->regions))
                        <div class="table-responsive-sm">
                            <table class="table table-bordered states_table">
                                <thead>
                                    <tr>
                                        <th>Region</th>
                                        <th>Client</th>
                                        <th>Branches</th>
                                        <th>Machine</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($state->regions as $region)
                                        <tr>
                                            <td>
                                                <a href="{{ route('show.region', $region->id) }}">
                                                    {{ $region->region }}
                                                </a>
                                            </td>
                                            <td style="color: {{ $region->client->color }}">
                                                @can('view', $region->client)
                                                    <a href="{{ route('show.client', $region->client_id) }}">{{ $region->client->short_name }}</a>
                                                @else
                                                    {{ $region->client->short_name }}
                                                @endcan
                                            </td>
                                            <td>
                                                <span class="badge badge-secondary">
                                                    {{ number_format($region->branches->count()) }}
                                                </span>
                                            </td>
                                            <td>
                                                <span class="badge badge-info">
                                                    {{ number_format($region->machines->count()) }}
                                                </span>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @else
                        <div class="text-center text-muted my-4">
                            <h2>No regions found in state</h2>
                            <button class="btn btn-primary my-3 waves-effect add_region_btn">
                                Add New Region
                            </button>
                        </div>
                    @endif
                @endslot
            @endcomponent

            {{-- engineers --}}
            @component('components.card')
                @slot('card_header') 
                <h4 class="card-title">
                    Engineers
                </h4>
                <div class="card-options">
                    <button class="btn-block-option assign_engineer_btn" type="button">
                        <i class="mdi mdi-account-multiple-plus mdi-24px"></i>
                    </button>
                </div>
                @endslot
                @slot('card_body')
                    @if ($state->engineers->count())
                        <div class="table-responsive-sm">
                            <table class="table table-bordered engineers_table">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Telephone</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($state->engineers as $eng)
                                        <tr>
                                            <td>
                                                @can('view', $eng)
                                                    <a href="{{ route('show.staff', $eng->id) }}">{{ $eng->full_name() }}</a>
                                                @else
                                                    {{ $eng->full_name() }}
                                                @endcan
                                            </td>
                                            <td>
                                                <a href="mailto:{{ $eng->email }}" class="text-dark">
                                                    {{ $eng->email }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="tel:{{ $eng->phone }}" class="text-danger">
                                                    {{ $eng->phone }}
                                                </a>
                                            </td>
                                            <td>
                                                <button class="btn btn-sm btn-primary waves-effect dropdown-toggle"
                                                    data-toggle="dropdown"
                                                >
                                                    <span class="mdi mdi-settings-box"></span>
                                                </button>
                                                <div class="dropdown-menu">
                                                    <div class="dropdown-header">
                                                        <small>{{ $eng->full_name() }}</small>
                                                    </div>
                                                    <a class="dropdown-item drop_engineer_btn" 
                                                        data-url="{{ route('drop.state.engineer', [$state->id, $eng->id]) }}" 
                                                        data-name="{{ $eng->full_name() }}"
                                                        href="javascript:void(0)"
                                                    >
                                                        Remove Engineer
                                                    </a>
                                                    <a class="dropdown-item">
                                                        Send Message
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @else
                        <div class="text-center text-muted my-4">
                            <h2>No engineer found in state!</h2>
                            <button class="btn btn-primary my-3 waves-effect assign_engineer_btn">
                                Assign Engineer
                            </button>
                        </div>
                    @endif
                @endslot
            @endcomponent
        </div>
    </div>

    {{-- modal --}}
    @include('admin.settings.locations.states.edit')
    @include('admin.settings.locations.states.add-region')
    @include('admin.settings.locations.states.assign-engineer')
@endsection

@push('js')
    <script src="{{ asset('js/admin/location.state.js') }}"></script>
    <script>
        $(document).ready(function() {

            simpleDataTable('.states_table, .engineers_table');

            $('#engineer_select').select2({
                placeholder: 'Select Engineers'
            });

            @if (session('updated'))
                Swal.fire(
                    'State Updated',
                    '{{ session('updated') }}',
                    'success'
                );
            @endif

            @if (session('assigned_engineers'))
                Swal.fire(
                    'Engineers Assigned',
                    '{{ session('assigned_engineers') }}',
                    'success'
                );
            @endif

            @if (session('edit'))
                openEditCountryModal();
            @endif

            @if (session('create_region'))
                openAddRegionModal();
            @endif

            @if (session('assign_engineer'))
                openAssignEngineerModal();
            @endif

            @if (session('dropped_engineer'))
                Swal.fire(
                    'State Engineer Dropped!',
                    '{{ session('dropped_engineer') }}',
                    'success'
                );
            @endif

            @if (session('created_region'))
                Swal.fire(
                    'Region Added to State',
                    '{{ session('created_region') }}',
                    'success'
                );
            @endif
        });
    </script>
@endpush