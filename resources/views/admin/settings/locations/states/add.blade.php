{{--  create modal  --}}
<form method="POST" action="{{ route('store.state') }}" id="add_state_form">
    @component('components.modal', [
        'modal_dialog_class' => 'modal-dialog-centered animated bounceInDown',
        'modal_id' => 'add_state_modal',
        'modal_top_dismiss' => true,
    ])
        @slot('modal_header')
            <h5>
                <i class="mdi mdi-library-plus ml-2 text-primary"></i>
                New State
            </h5>
        @endslot

        @slot('modal_body')
            @csrf
            <div class="form_response"></div>
            {{--  country field  --}}
            @component('components.form.select-form-group', [
                'label' => 'Country',
                'name' => 'country',
                'required' => true
            ])
                @slot('options')                    
                    <option value="">Choose Country</option>
                    @foreach ($countries as $country)
                        <option value="{{ $country->id }}">{{ $country->country }}</option>
                    @endforeach
                @endslot
            @endcomponent

           {{--  state field  --}}
            @component('components.form.input-form-group', [
                'label' => 'State',
                'name' => 'state',
                'placeholder' => 'state name here',
                'required' => true,
            ])
            @endcomponent

            {{-- zone --}}
            @component('components.form.input-form-group', [
                'label' => 'Zone / Region',
                'name' => 'zone',
                'placeholder' => ' state zone or region here',
                'required' => false,
            ])
            @endcomponent

        @endslot

        @slot('modal_footer')
            <button class="btn btn-default" type="button" data-dismiss="modal">
                Exit
            </button>
            <button class="btn btn-primary" type="submit">
                Add
            </button>
        @endslot
    @endcomponent
</form>