{{-- assign engineer modal --}}
@if ($engineers->count())
    <form method="POST" action="{{ route('assign.state.engineer', $state->id) }}" id="assign_engineer_form">
        @component('components.modal', [
            'modal_dialog_class' => 'modal-dialog-centered animated jackInTheBox',
            'modal_id' => 'assign_engineer_modal',
            'modal_top_dismiss' => true,
        ])
            @slot('modal_header')
                <h5>
                    <i class="mdi mdi mdi-account-multiple-check ml-2 text-primary"></i>
                    Assign Engineers
                </h5>
            @endslot

            @slot('modal_body')
                @csrf
                <div class="form_response"></div>
                {{--  country field  --}}
                @component('components.form.input-form-group', [
                    'label' => 'Country',
                    'name' => 'country',
                    'placeholder' => 'country name here',
                    'required' => true,
                    'value' => $state->country->country,
                    'props' => ['readonly'=> true]
                ])
                    
                @endcomponent

                {{--  state field  --}}
                @component('components.form.input-form-group', [
                    'label' => 'State',
                    'name' => 'state',
                    'placeholder' => 'state name here',
                    'required' => true,
                    'read_only' => true,
                    'value' => $state->state
                ])
                @endcomponent


                @component('components.form.select-form-group', [
                    'label' => 'Engineers',
                    'name' => 'engineers[]',
                    'required' => true,
                    'id' => 'engineer_select',
                    'props' => ['multiple' => true]
                ])
                    @slot('options')
                        <option value="">Choose Engineers</option>
                        @foreach ($engineers as $engineer)
                            <option value="{{ $engineer->id }}"
                                {{ in_array($engineer->id, $state->engineers->pluck('id')->toArray()) ? "selected" : null }}
                            >
                                {{ $engineer->full_name() }}
                            </option>
                        @endforeach
                    @endslot
                @endcomponent

            @endslot

            @slot('modal_footer')
                <button class="btn btn-default" type="button" data-dismiss="modal">
                    Exit
                </button>
                <button class="btn btn-primary" type="submit">
                    Assign
                </button>
            @endslot
        @endcomponent
    </form>
@else
    @component('components.modal', [
        'modal_id' => 'assign_engineer_modal',
        'modal_dialog_class' => 'modal-dialog-centered animated jackInTheBox',
    ])
        @slot('modal_body')
            @component('components.alert', [
                'alert_class' => 'alert-primary',
                'text' => 'No engineering staff found!',
                'alert_dismiss' => false
            ])
                
            @endcomponent
        @endslot
        @slot('modal_footer')
            <button class="btn btn-default" type="button" data-dismiss="modal">
                Ok
            </button>
        @endslot
    @endcomponent
@endif