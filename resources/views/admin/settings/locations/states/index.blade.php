@extends('layouts.admin.app')
@section('title', 'Settings | Location | States')

@section('page', 'Location | States')

@push('css')
@endpush

@section('crumbs')
<li class="breadcrumb-item">
    <a href="{{ route('settings.locations') }}">Location</a>
</li>
<li class="breadcrumb-item active">
    States
</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            @if ($states->count())
                <div class="text-right mb-2">
                    <button class="btn btn-primary waves-effect add_state_btn">
                        <span class="d-none d-md-inline-block">New State</span>
                        <i class="mdi mdi-plus-circle-outline mr-1"></i>
                    </button>
                </div>
                @component('components.card')
                    @slot('card_body')
                        <h4 class="card-title">
                            All States
                        </h4>
                        <hr>
                        <div class="table-responsive-sm">
                            <table class="table table-bordered states_table">
                                <thead>
                                    <tr>
                                        <th>State</th>
                                        <th>Country</th>
                                        <th style="width: 10%">Regions</th>
                                        <th style="width: 10%">Branch</th>
                                        <th style="width: 10%">Machines</th>
                                        <th style="width: 12%">Engineers</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($states as $state)
                                    <tr>
                                        <td class="font-weight-bold">
                                            <a href="{{ route('show.state', $state->id) }}">{{ $state->state }}</a>
                                        </td>
                                        <td>
                                        <a href="{{ route('show.country', $state->country->id) }}">
                                                {{ $state->country->country }}
                                            </a>
                                        </td>
                                        <td>
                                            <span class="badge badge-secondary">
                                                {{ number_format($state->regions->count()) }}
                                            </span>
                                        </td>
                                        <td>
                                            <span class="badge badge-info">
                                                {{ number_format($state->branches->count()) }}
                                            </span>
                                        </td>
                                        <td>
                                            <span class="badge badge-primary">
                                                {{ number_format($state->machines->count()) }}
                                            </span>
                                        </td>
                                        <td>
                                            <span class="badge badge-dark">
                                                {{ number_format($state->engineers->count()) }}
                                            </span>
                                        </td>
                                        <td>
                                            <div class="btn-group btn-group-sm">
                                                <a class="btn btn-sm btn-secondary" title="View"
                                                    href="{{ route('show.state', $state->id) }}">
                                                    view
                                                </a>
                                                <a class="btn btn-sm btn-warning" title="Edit"
                                                    href="{{ route('edit.state', $state->id) }}">
                                                    <i class="mdi mdi-playlist-edit"></i>
                                                </a>
                                                <button class="btn btn-sm btn-primary delete_country_btn" title="Delete"
                                                    data-url="{{ route('delete.state', $state->id) }}"
                                                    data-state="{{ $state->state }}">
                                                    <i class="mdi mdi-delete"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endslot
                @endcomponent
            @else
                @component('components.empty')
                    @slot('text')
                    No state found!
                    @endslot
                    @slot('body')
                        <button class="btn btn-primary btn-lg waves-effect add_state_btn">
                            Add New State
                        </button>
                    @endslot
                @endcomponent
            @endif
        </div>
    </div>

    {{-- modal --}}
    @include('admin.settings.locations.states.add')
@endsection

@push('js')
<script src="{{ asset('js/admin/location.state.js') }}"></script>
<script>
    $(document).ready(function() {

            @if (session('new'))
                openAddStateModal();
            @endif

            @if (session('created'))
                Swal.fire(
                    'State Added!',
                    '{{ session('created') }}',
                    'success'
                );
            @endif

            @if (session('deleted'))
                Swal.fire(
                    'State Deleted!',
                    '{{ session('deleted') }}',
                    'success'
                );
            @endif

            simpleDataTable('.states_table')
        });
</script>
@endpush