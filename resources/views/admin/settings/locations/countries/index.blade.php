@extends('layouts.admin.app')
@section('title', 'Settings | Location | Countries')

@section('page', 'Location | Countries')

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('settings.locations') }}">Location</a>
    </li>
    <li class="breadcrumb-item active">
        Countries
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            @if ($countries->count())
                <div class="text-right mb-2">
                    <button class="btn btn-primary waves-effect add_country_btn">
                        <span class="d-none d-md-inline-block">New Country</span>
                        <i class="mdi mdi-plus-circle-outline mr-1"></i>
                    </button>
                </div>
                @component('components.card')
                    @slot('card_body')
                        <h4 class="card-title">
                            All Countries
                        </h4>
                        <hr>
                        <div class="table-responsive-sm">
                            <table class="table table-bordered country_table">
                                <thead>
                                    <tr>
                                        <th>Country</th>
                                        <th>States</th>
                                        <th>Regions</th>
                                        <th>Branch</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($countries as $country)
                                        <tr>
                                            <td class="font-weight-bold">
                                                <a href="{{ route('show.country', $country->id) }}">{{ $country->country }}</a>
                                            </td>
                                            <td>
                                                <span class="badge badge-primary">
                                                    {{ number_format($country->states->count()) }}
                                                </span>
                                            </td>
                                            <td>
                                                <span class="badge badge-secondary">
                                                    {{ number_format(0) }}
                                                </span>
                                            </td>
                                            <td>
                                                <span class="badge badge-info">
                                                    {{ number_format(0) }}
                                                </span>
                                            </td>
                                            <td>
                                                <div class="btn-group btn-group-sm">
                                                    <a class="btn btn-sm btn-secondary" title="View" href="{{ route('show.country', $country->id) }}">
                                                        view
                                                    </a>
                                                    <a class="btn btn-sm btn-warning" title="Edit" href="{{ route('edit.country', $country->id) }}">
                                                        <i class="mdi mdi-playlist-edit"></i>
                                                    </a>
                                                    <button class="btn btn-sm btn-primary delete_country_btn" title="Delete"
                                                        data-url="{{ route('delete.country', $country->id) }}" data-country="{{ $country->country }}">
                                                        <i class="mdi mdi-delete"></i>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endslot
                @endcomponent
            @else
                @component('components.empty')
                    @slot('text')
                        No country found!
                    @endslot
                    @slot('body')
                        <button class="btn btn-primary btn-lg waves-effect add_country_btn">
                            Add New Country
                        </button>
                    @endslot
                @endcomponent
            @endif
        </div>
    </div>

    {{-- modal --}}
    @include('admin.settings.locations.countries.add')
@endsection

@push('js')
    <script src="{{ asset('js/admin/location.country.js') }}"></script>
    <script>
        $(document).ready(function() {

            @if (session('new'))
                openAddCountryModal();
            @endif

            @if (session('created'))
                Swal.fire(
                    'Country Added!',
                    '{{ session('created') }}',
                    'success'
                );
            @endif

            @if (session('deleted'))
                Swal.fire(
                    'Country Deleted!',
                    '{{ session('deleted') }}',
                    'success'
                );
            @endif

            simpleDataTable('.country_table')
        });
    </script>
@endpush