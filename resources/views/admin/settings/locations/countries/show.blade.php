@extends('layouts.admin.app')
@section('title', 'Locations| Countries | '.$country->country)

@section('page', 'Locations | Countries | '.Str::limit($country->country, 15, '...'))

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('settings.locations') }}">Locations</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('all.countries') }}">Countries</a>
    </li>
    <li class="breadcrumb-item active">
        {{ Str::limit($country->country, 15, '...') }}
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="col-12 mb-2 text-right">
            <button class="btn btn-success add_state_btn" title="Add State">
                <span class="d-none d-md-inline-block">Add</span>
                <span class="mdi mdi-plus-circle ml-md-2"></span>
            </button>
            <button class="btn btn-warning edit_country_btn" title="Edit Country">
                <span class="d-none d-md-inline-block">Edit</span>
                <span class="mdi mdi-pencil ml-md-2"></span>
            </button>
            @if ($country->deletable())                
                <button class="btn btn-primary delete_country_btn" 
                    title="Delete Country"
                    data-country="{{ $country->country }}"
                    data-url="{{ route('delete.country', $country->id) }}"
                    >
                    <span class="d-none d-md-inline-block">Delete</span>
                    <span class="mdi mdi-delete ml-md-2"></span>
                </button>
            @endif
        </div>
        <div class="col-md-3">
            @component('components.card')
                @slot('card_body')
                    <strong class="text-primary">Country</strong>
                    <p>{{ $country->country }}</p>
                    <hr>
                    <strong class="text-primary">Continent</strong>
                    <p>{{ $country->continent }}</p>
                    <hr>
                    <strong class="text-primary">Created At</strong>
                    <p>{{ $country->created_at->format('jS M, Y') }}</p>
                    <hr>
                    <strong class="text-primary">Last Updated</strong>
                    <p>{{ $country->updated_at->format('jS M, Y') }}</p>
                    
                @endslot
            @endcomponent
        </div>
        <div class="col-md-9">
            {{-- states --}}
            @component('components.card')
                @slot('card_body')
                    <h4 class="card-title">
                        {{ $country->country }} States
                    </h4>
                    <hr>
                    @if ($country->states->count())
                        <div class="table-responsive-sm">
                            <table class="table table-bordered states_table">
                                <thead>
                                    <tr>
                                        <th>State</th>
                                        <th>Regions</th>
                                        <th>Branches</th>
                                        <th>Machines</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($country->states as $state)
                                        <tr>
                                            <td>
                                                <a href="{{ route('show.state', $state->id) }}">
                                                    {{ $state->state }}
                                                </a>
                                            </td>
                                            <td>
                                                <span class="badge badge-secondary">
                                                    {{ number_format($state->regions->count()) }}
                                                </span>
                                            </td>
                                            <td>
                                                <span class="badge badge-info">
                                                    {{ number_format($state->branches->count()) }}
                                                </span>
                                            </td>
                                            <td>
                                                <span class="badge badge-primary">
                                                    {{ number_format($state->machines->count()) }}
                                                </span>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @else
                        <div class="text-center text-muted my-4">
                            <h2>No state found in country</h2>
                            <button class="btn btn-primary my-3 waves-effect add_state_btn">
                                Add New State
                            </button>
                        </div>
                    @endif
                @endslot
            @endcomponent
        </div>
    </div>

    {{-- modal --}}
    @include('admin.settings.locations.countries.edit')
    @include('admin.settings.locations.countries.add-state')
@endsection

@push('js')
    <script src="{{ asset('js/admin/location.country.js') }}"></script>
    <script>
        $(document).ready(function() {

            simpleDataTable('.states_table');

            @if (session('updated'))
                Swal.fire(
                    'Country Updated',
                    '{{ session('updated') }}',
                    'success'
                );
            @endif

            @if (session('edit'))
                openEditCountryModal();
            @endif

            @if (session('created_state'))
                Swal.fire(
                    'Stated Added',
                    '{{ session('created_state') }}',
                    'success'
                );
            @endif
        });
    </script>
@endpush