{{--  create modal  --}}
<form method="POST" action="{{ route('update.country', $country->id) }}" id="edit_country_form">
    @method('put')
    @component('components.modal', [
        'modal_dialog_class' => 'modal-dialog-centered animated bounceInDown',
        'modal_id' => 'edit_country_modal',
        'modal_top_dismiss' => true,
    ])
        @slot('modal_header')
            <h5>
                <i class="mdi mdi-library-plus ml-2 text-primary"></i>
                Update Country | {{ $country->country }}
            </h5>
        @endslot

        @slot('modal_body')
            @csrf
            <div class="form_response"></div>
            {{--  country field  --}}
            @component('components.form.input-form-group', [
                'label' => 'Country',
                'name' => 'country',
                'placeholder' => 'country name here',
                'required' => true,
                'value' => $country->country
            ])
                
            @endcomponent

            {{--  continent field  --}}
            @component('components.form.select-form-group', [
            'label' => 'Continent',
            'name' => 'continent',
            ])
                @slot('options')                    
                    <option value="">Choose Continent</option>
                    <option {{ $country->continent == "africa" ? "selected" : null }} value="africa">Africa</option>
                    <option {{ $country->continent == "antarctica" ? "selected" : null }} value="antarctica">Antarctica</option>
                    <option {{ $country->continent == "asia" ? "selected" : null }} value="asia">Asia</option>
                    <option {{ $country->continent == "europe" ? "selected" : null }} value="europe">Europe</option>
                    <option {{ $country->continent == "north america" ? "selected" : null }} value="north america">North Amerca</option>
                    <option {{ $country->continent == "south america" ? "selected" : null }} value="south america">South Amerca</option>
                    <option {{ $country->continent == "australia/oceania" ? "selected" : null }} value="australia/oceania">Australia/Oceania</option>
                @endslot
            @endcomponent

        @endslot

        @slot('modal_footer')
            <button class="btn btn-default" type="button" data-dismiss="modal">
                Exit
            </button>
            <button class="btn btn-primary" type="submit">
                Save
            </button>
        @endslot
    @endcomponent
</form>