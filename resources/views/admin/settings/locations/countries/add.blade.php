{{--  create modal  --}}
<form method="POST" action="{{ route('store.country') }}" id="add_country_form">
    @component('components.modal', [
        'modal_dialog_class' => 'modal-dialog-centered animated bounceInDown',
        'modal_id' => 'add_country_modal',
        'modal_top_dismiss' => true,
    ])
        @slot('modal_header')
            <h5>
                <i class="mdi mdi-library-plus ml-2 text-primary"></i>
                New Country
            </h5>
        @endslot

        @slot('modal_body')
            @csrf
            <div class="form_response"></div>
            {{--  country field  --}}
            @component('components.form.input-form-group', [
                'label' => 'Country',
                'name' => 'country',
                'placeholder' => 'country name here',
                'required' => true,
            ])
                
            @endcomponent

            {{--  continent field  --}}
            @component('components.form.select-form-group', [
            'label' => 'Continent',
            'name' => 'continent',
            ])
                @slot('options')                    
                    <option value="">Choose Continent</option>
                    <option value="africa">Africa</option>
                    <option value="antarctica">Antarctica</option>
                    <option value="asia">Asia</option>
                    <option value="europe">Europe</option>
                    <option value="north america">North Amerca</option>
                    <option value="south america">South Amerca</option>
                    <option value="australia/oceania">Australia/Oceania</option>
                @endslot
            @endcomponent

        @endslot

        @slot('modal_footer')
            <button class="btn btn-default" type="button" data-dismiss="modal">
                Exit
            </button>
            <button class="btn btn-primary" type="submit">
                Add
            </button>
        @endslot
    @endcomponent
</form>