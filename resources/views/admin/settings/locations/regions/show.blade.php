@extends('layouts.admin.app')
@section('title', 'Locations | States | '.$region->state->state.' | Regions | '.$region->region)

@section('page', 'Locations | Regions | '.Str::limit($region->region, 15, '...'))

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('settings.locations') }}">Locations</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('show.state', $region->state_id) }}">
            {{ Str::limit($region->state->state, 10, '...') }}
        </a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('all.regions') }}">Regions</a>
    </li>
    <li class="breadcrumb-item active">
        {{ Str::limit($region->region, 15, '...') }}
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="col-12 mb-2 text-right">
            <button class="btn btn-success add_branch_btn" title="Add Branch">
                <span class="d-none d-md-inline-block">Add</span>
                <span class="mdi mdi-plus-circle ml-md-2"></span>
            </button>
            <button class="btn btn-warning edit_region_btn" title="Edit Region">
                <span class="d-none d-md-inline-block">Edit</span>
                <span class="mdi mdi-pencil ml-md-2"></span>
            </button>
            @if ($region->deletable())                
                <button class="btn btn-primary delete_region_btn" 
                    title="Delete Region"
                    data-region="{{ $region->region }}"
                    data-url="{{ route('delete.region', $region->id) }}"
                    >
                    <span class="d-none d-md-inline-block">Delete</span>
                    <span class="mdi mdi-delete ml-md-2"></span>
                </button>
            @endif
        </div>
        <div class="col-md-3">
            @component('components.card')
                @slot('card_body')
                    <strong class="text-primary">Region</strong>
                    <p>{{ $region->region }}</p>
                    <hr>
                    <strong class="text-primary">State</strong>
                    <p>{{ $region->state->state }}</p>
                    <hr>
                    <strong class="text-primary">Country</strong>
                    <p>{{ $region->country->country }}</p>
                    <hr>
                    <strong class="text-primary">Client</strong>
                    @can('view', $region->client) 
                        <a href="{{ route('show.client', $region->client->id) }}">                        
                            <p class="font-weight-bold" style="color:{{ $region->client->color }}">{{ $region->client->name }}</p>
                        </a>                       
                    @else                        
                        <p class="font-weight-bold" style="color:{{ $region->client->color }}">{{ $region->client->name }}</p>
                    @endcan
                    <hr>
                    <strong class="text-primary">Created At</strong>
                    <p>{{ $region->created_at->format('jS M, Y') }}</p>
                    <hr>
                    <strong class="text-primary">Last Updated</strong>
                    <p>{{ $region->updated_at->format('jS M, Y') }}</p>
                    
                @endslot
            @endcomponent
        </div>
        <div class="col-md-9">
            {{-- states --}}
            @component('components.card')
                @slot('card_body')
                    <h4 class="card-title">
                        {{ $region->region }} Branches
                    </h4>
                    <hr>
                    @if (count($region->branches))
                        <div class="table-responsive-sm">
                            <table class="table table-bordered branches_table">
                                <thead>
                                    <tr>
                                        <th>Branch</th>
                                        <th>Custodians</th>
                                        <th>Machine</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($region->branches as $branch)
                                        <tr>
                                            <td>
                                                <a href="{{ route('show.branch', $branch->id) }}">
                                                    {{ $branch->branch }}
                                                </a>
                                            </td>
                                            <td>
                                                <span class="badge badge-secondary">
                                                    {{ number_format($branch->custodians->count()) }}
                                                </span>
                                            </td>
                                            <td>
                                                <span class="badge badge-primary">
                                                    {{ number_format($branch->machines->count()) }}
                                                </span>
                                            </td>
                                            
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @else
                        <div class="text-center text-muted my-4">
                            <h2>No branches found in region</h2>
                            <button class="btn btn-primary my-3 waves-effect add_branch_btn">
                                Add New Branch
                            </button>
                        </div>
                    @endif
                @endslot
            @endcomponent
        </div>
    </div>

    {{-- modal --}}
    @include('admin.settings.locations.regions.edit')
    @include('admin.settings.locations.regions.add-branch')
@endsection

@push('js')
    <script src="{{ asset('js/admin/location.region.js') }}"></script>
    <script>
        $(document).ready(function() {

            simpleDataTable('.branches_table');

            @if (session('updated'))
                Swal.fire(
                    'Region Updated',
                    '{{ session('updated') }}',
                    'success'
                );
            @endif

            @if (session('edit'))
                openEditRegionModal();
            @endif

            @if (session('create_branch'))
                openAddBranchModal();
            @endif

            @if (session('created_branch'))
                Swal.fire(
                    'Branch Added to Region',
                    '{{ session('created_branch') }}',
                    'success'
                );
            @endif
        });
    </script>
@endpush