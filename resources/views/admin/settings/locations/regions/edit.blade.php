{{-- edit state modal --}}
@if ($clients->count()) 
    <form method="POST" action="{{ route('update.region', $region->id) }}" id="edit_region_form">
        @component('components.modal', [
            'modal_dialog_class' => 'modal-dialog-centered animated bounceInUp',
            'modal_id' => 'edit_region_modal',
            'modal_top_dismiss' => true,
        ])
            @slot('modal_header')
                <h5>
                    <i class="mdi mdi-library-plus ml-2 text-primary"></i>
                    Update Region | {{ $region->region }}
                </h5>
            @endslot

            @slot('modal_body')
                @csrf
                <div class="form_response"></div>
                {{--  region field  --}}
                @component('components.form.input-form-group', [
                    'label' => 'Region',
                    'name' => 'region',
                    'placeholder' => 'region name here',
                    'required' => true,
                    'value' => $region->region
                ])
                    
                @endcomponent

                {{--  state field  --}}
                @component('components.form.select-form-group', [
                    'label' => 'State',
                    'name' => 'state',
                    'placeholder' => 'state name here',
                ])
                    @slot('options')
                        <option value="">Choose State</option>
                        @foreach ($states as $state)
                            <option value="{{ $state->id }}"
                                {{ $region->state_id == $state->id ? "selected" : null}}    
                            >
                                {{ $state->state }} - {{ $state->country->country }}
                            </option>
                        @endforeach
                    @endslot
                @endcomponent


                @component('components.form.select-form-group', [
                    'label' => 'Client',
                    'name' => 'client',
                    'required' => true,
                ])
                    @slot('options')
                        <option value="">Choose Client</option>
                        @foreach ($clients as $client)
                            <option value="{{ $client->id }}"
                                {{ $region->client_id == $client->id ? "selected" : null }}    
                            >
                                {{ $client->short_name }}
                            </option>
                        @endforeach
                    @endslot
                @endcomponent

            @endslot

            @slot('modal_footer')
                <button class="btn btn-default" type="button" data-dismiss="modal">
                    Exit
                </button>
                <button class="btn btn-primary" type="submit">
                    Save
                </button>
            @endslot
        @endcomponent
    </form>
@else
    @component('components.modal', [
        'modal_dialog_class' => 'modal-dialog-centered animated bounceInUp',
        'modal_id' => 'add_region_modal',
        'modal_top_dismiss' => true,
    ])
        @slot('modal_body')
            <h4 class="text-muted text-center my-4">
                No client found!
            </h4>
        @endslot
        @slot('modal_footer')
            <button class="btn btn-default" type="button" data-dismiss="modal">
                Exit
            </button>
            <a class="btn btn-primary" href="{{ route('create.client') }}">
                Add
            </a>
        @endslot
    @endcomponent
@endif