@extends('layouts.admin.app')
@section('title', 'Settings | Location | Regions')

@section('page', 'Location | Region')

@push('css')
@endpush

@section('crumbs')
<li class="breadcrumb-item">
    <a href="{{ route('settings.locations') }}">Location</a>
</li>
<li class="breadcrumb-item active">
    Regions
</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            @if ($regions->count())
                <div class="text-right mb-2">
                    <button class="btn btn-primary waves-effect add_region_btn">
                        <span class="d-none d-md-inline-block">New Region</span>
                        <i class="mdi mdi-plus-circle-outline mr-1"></i>
                    </button>
                </div>
                @component('components.card')
                    @slot('card_body')
                        <h4 class="card-title">
                            All Regions
                        </h4>
                        <hr>
                        <div class="table-responsive-sm">
                            <table class="table table-bordered dt-responsive nowrap regions_table">
                                <thead>
                                    <tr>
                                        <th>Region</th>
                                        <th>State</th>
                                        <th>Client</th>
                                        <th>branches</th>
                                        <th>Machines</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($regions as $region)
                                    <tr>
                                        <td class="font-weight-bold">
                                            <a href="{{ route('show.region', $region->id) }}">{{ $region->region }}</a>
                                        </td>
                                        <td>
                                        <a href="{{ route('show.state', $region->state->id) }}" >
                                                {{ $region->state->state }}
                                            </a>
                                        </td>
                                        <td>
                                            <span class="font-weight-bold" style="color:{{ $region->client->color }}">
                                                {{ $region->client->short_name }}
                                            </span>
                                        </td>
                                        <td>
                                            <span class="badge badge-secondary">
                                                {{ number_format($region->branches->count()) }}
                                            </span>
                                        </td>
                                        <td>
                                            <span class="badge badge-info">
                                                {{ number_format($region->machines->count()) }}
                                            </span>
                                        </td>
                                        <td>
                                            <div class="btn-group btn-group-sm">
                                                <a class="btn btn-sm btn-secondary" title="View"
                                                    href="{{ route('show.region', $region->id) }}">
                                                    view
                                                </a>
                                                <a class="btn btn-sm btn-warning" title="Edit"
                                                    href="{{ route('edit.region', $region->id) }}">
                                                    <i class="mdi mdi-playlist-edit"></i>
                                                </a>
                                                <button class="btn btn-sm btn-primary delete_region_btn" title="Delete"
                                                    data-url="{{ route('delete.region', $region->id) }}"
                                                    data-state="{{ $region->region }}">
                                                    <i class="mdi mdi-delete"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endslot
                @endcomponent
            @else
                @component('components.empty')
                    @slot('text')
                    No region found!
                    @endslot
                    @slot('body')
                        <button class="btn btn-primary btn-lg waves-effect add_region_btn">
                            Add New Region
                        </button>
                    @endslot
                @endcomponent
            @endif
        </div>
    </div>

    {{-- modal --}}
    @include('admin.settings.locations.regions.add')
@endsection

@push('js')
<script src="{{ asset('js/admin/location.region.js') }}"></script>
<script>
    $(document).ready(function() {

            @if (session('new'))
                openAddRegionModal();
            @endif

            @if (session('created'))
                Swal.fire(
                    'Region Added!',
                    '{{ session('created') }}',
                    'success'
                );
            @endif

            @if (session('deleted'))
                Swal.fire(
                    'Region Deleted!',
                    '{{ session('deleted') }}',
                    'success'
                );
            @endif

            simpleDataTable('.regions_table');
        });
</script>
@endpush