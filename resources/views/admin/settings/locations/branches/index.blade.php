@extends('layouts.admin.app')
@section('title', 'Settings | Location | Branches')

@section('page', 'Location | Branches')

@push('css')
@endpush

@section('crumbs')
<li class="breadcrumb-item">
    <a href="{{ route('settings.locations') }}">Location</a>
</li>
<li class="breadcrumb-item active">
    Branches
</li>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        @if ($branches->count())
            <div class="text-right mb-2">
                <button class="btn btn-primary waves-effect add_branch_btn">
                    <span class="d-none d-md-inline-block">New Branch</span>
                    <i class="mdi mdi-plus-circle-outline mr-1"></i>
                </button>
            </div>
            @component('components.card')
                @slot('card_body')
                <h4 class="card-title">
                    All Branches
                </h4>
                <hr>
                <div class="table-responsive-sm">
                    <table class="table table-bordered branches_table">
                        <thead>
                            <tr>
                                <th>Branch</th>
                                <th>Region</th>
                                <th>Client</th>
                                <th>Custodian</th>
                                <th>Machines</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($branches as $branch)
                            <tr>
                                <td class="font-weight-bold">
                                    <a href="{{ route('show.branch', $branch->id) }}">{{ $branch->branch }}</a>
                                </td>
                                <td>
                                    <a href="{{ route('show.region', $branch->region->id) }}">
                                        {{ $branch->region->region }}
                                    </a>
                                </td>
                                <td>
                                    <a style="color: {{$branch->client->color}}; font-weight: bold;"
                                        href="{{ route('show.client', $branch->client_id) }}"
                                    >
                                        {{ $branch->client->short_name }}
                                    </a>
                                </td>
                                <td>
                                    <span class="badge badge-secondary">
                                        {{ number_format($branch->custodians->count()) }}
                                    </span>
                                </td>
                                <td>
                                    <span class="badge badge-primary">
                                        {{ number_format($branch->machines->count()) }}
                                    </span>
                                </td>
                                <td>
                                    <div class="btn-group btn-group-sm">
                                        <a class="btn btn-sm btn-secondary" title="View"
                                            href="{{ route('show.branch', $branch->id) }}">
                                            view
                                        </a>
                                        <a class="btn btn-sm btn-warning" title="Edit"
                                            href="{{ route('edit.branch', $branch->id) }}">
                                            <i class="mdi mdi-playlist-edit"></i>
                                        </a>
                                        <button class="btn btn-sm btn-primary delete_branch_btn" title="Delete"
                                            data-url="{{ route('delete.branch', $branch->id) }}"
                                            data-branch="{{ $branch->branch }}">
                                            <i class="mdi mdi-delete"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @endslot
            @endcomponent
        @else
            @component('components.empty')
                @slot('text')
                    No branch found!
                @endslot
                @slot('body')
                    <button class="btn btn-primary btn-lg waves-effect add_branch_btn">
                        Add New Branch
                    </button>
                @endslot
            @endcomponent
        @endif
    </div>
</div>

{{-- modal --}}
@include('admin.settings.locations.branches.add')
@endsection

@push('js')
<script src="{{ asset('js/admin/location.branch.js') }}"></script>
<script>
    $(document).ready(function() {

            @if (session('new'))
                openAddBranchModal();
            @endif

            @if (session('created'))
                Swal.fire(
                    'Branch Added!',
                    '{{ session('created') }}',
                    'success'
                );
            @endif

            @if (session('deleted'))
                Swal.fire(
                    'Branch Deleted!',
                    '{{ session('deleted') }}',
                    'success'
                );
            @endif

            simpleDataTable('.branches_table')
        });
</script>
@endpush