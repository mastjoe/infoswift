 <form method="POST" action="{{ route('store.branch.custodian', $branch->id) }}" id="add_custodian_form">
        @component('components.modal', [
            'modal_dialog_class' => 'modal-dialog-centered animated bounceInLeft',
            'modal_id' => 'add_custodian_modal',
            'modal_top_dismiss' => true,
        ])
            @slot('modal_header')
                <h5>
                    <i class="mdi mdi-account-plus mr-2 text-primary"></i>
                    Add Custodian To Branch
                </h5>
            @endslot

            @slot('modal_body')
                @csrf
                <div class="form_response"></div>
                {{--  branch field  --}}
                @component('components.form.input-form-group', [
                    'label' => 'Branch',
                    'name' => 'branch',
                    'placeholder' => 'branch name here',
                    'read_only' => true,
                    'value' => $branch->branch
                ])
                    
                @endcomponent

                {{-- name --}}
                @component('components.form.input-form-group', [
                    'label' => 'Name',
                    'name' => 'name',
                    'placeholder' => 'Name of custodian',
                    'required' => true
                ])
                @endcomponent

                {{-- number --}}
                @component('components.form.input-form-group', [
                    'label' => 'Telephone',
                    'name' => 'phone',
                    'type' => 'tel',
                   'placeholder' => 'custodian phone number here',
                   'required' => true
                ])
                @endcomponent

                {{-- email --}}
                @component('components.form.input-form-group', [
                    'label' => 'Email',
                    'name' => 'email',
                    'type' => 'email',
                    'placeholder' => 'custodian email address',
                    'required' => true
                ])
                    
                @endcomponent
            
               
            @endslot

            @slot('modal_footer')
                <button class="btn btn-default" type="button" data-dismiss="modal">
                    Exit
                </button>
                <button class="btn btn-primary" type="submit">
                    Add
                </button>
            @endslot
        @endcomponent
    </form>