<form method="POST" action="{{ route('update.branch.custodian',[ $branch->id, $custodian->id]) }}" id="edit_custodian_form"
    onsubmit="updateCustodianForm(event)"    
>
    @method('put')
    <div class="modal-header">
        <h5>
            <i class="mdi mdi-account-plus ml-2 text-primary"></i>
            Update Branch Custodian
        </h5>
    </div>            
    <div class="modal-body">
        @csrf
        <div class="form_response"></div>
        {{--  branch field  --}}
        @component('components.form.input-form-group', [
            'label' => 'Branch',
            'name' => 'branch',
            'placeholder' => 'branch name here',
            'read_only' => true,
            'value' => $branch->branch
        ])
            
        @endcomponent

        {{-- name --}}
        @component('components.form.input-form-group', [
            'label' => 'Name',
            'name' => 'name',
            'placeholder' => 'Name of custodian',
            'required' => true,
            'value' => $custodian->name
        ])
        @endcomponent

        {{-- number --}}
        @component('components.form.input-form-group', [
            'label' => 'Telephone',
            'name' => 'phone',
            'type' => 'tel',
            'placeholder' => 'custodian phone number here',
            'required' => true,
            'value' => $custodian->phone
        ])
        @endcomponent

        {{-- email --}}
        @component('components.form.input-form-group', [
            'label' => 'Email',
            'name' => 'email',
            'type' => 'email',
            'placeholder' => 'custodian email address',
            'required' => true,
            'value' => $custodian->email
        ])
            
        @endcomponent
    
    </div>

    <div class="modal-footer">

        <button class="btn btn-default" type="button" data-dismiss="modal">
            Exit
        </button>
        <button class="btn btn-primary" type="submit">
            Save
        </button>
    </div>    
</form>