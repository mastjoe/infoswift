{{-- add branch modal --}}
@if ($regions->count()) 
    <form method="POST" action="{{ route('store.branch') }}" id="add_branch_form">
        @component('components.modal', [
            'modal_dialog_class' => 'modal-dialog-centered animated bounceInLeft',
            'modal_id' => 'add_branch_modal',
            'modal_top_dismiss' => true,
        ])
            @slot('modal_header')
                <h5>
                    <i class="mdi mdi-library-plus ml-2 text-primary"></i>
                    New Branch
                </h5>
            @endslot

            @slot('modal_body')
                @csrf
                <div class="form_response"></div>
                {{--  branch field  --}}
                @component('components.form.input-form-group', [
                    'label' => 'Branch',
                    'name' => 'branch',
                    'placeholder' => 'branch name here',
                    'required' => true,
                ])
                    
                @endcomponent

                 {{-- region --}}
                 @component('components.form.select-form-group', [
                    'label' => 'Region',
                    'name' => 'region',
                    'required' => true,
                    'props' => ['onchange' => 'regionSelectReaction(this)']
                ])
                    @slot('options')
                        <option value="">Choose Region</option>
                        @foreach ($regions as $region)
                            <option value="{{ $region->id }}"
                                data-country = {{ $region->country->country }}    
                                data-state = {{ $region->state->state }}    
                                data-client = {{ $region->client->short_name }}    
                            >
                                {{ $region->region }} - ({{ $region->client->code }})
                            </option>
                        @endforeach
                    @endslot
                @endcomponent

                {{-- client --}}
                 @component('components.form.input-form-group', [
                    'label' => 'Client',
                    'name' => 'client',
                    'form_group_class' => 'rd d-none',
                    'id' => 'client_field',
                    'read_only' => true,
                ])
                    
                @endcomponent
               
                {{-- state --}}
                 @component('components.form.input-form-group', [
                    'label' => 'State',
                    'name' => 'state',
                    'form_group_class' => 'rd d-none',
                    'id' => 'state_field',
                    'read_only' => true
                ])
                    
                @endcomponent

                {{-- country --}}
                 @component('components.form.input-form-group', [
                    'label' => 'Country',
                    'name' => 'country',
                    'form_group_class' => 'rd d-none',
                    'id' => 'country_field',
                    'read_only' => true
                ])
                    
                @endcomponent

                {{-- sol id --}}
                @component('components.form.input-form-group', [
                    'label' => 'Sol ID',
                    'name' => 'sol_id',
                    'placeholder' => 'sol id here'
                ])
                    
                @endcomponent

                {{-- emails --}}
                @component('components.form.input-form-group', [
                    'label' => 'Emails',
                    'name' => 'emails',
                    'placeholder' => 'e.g branch1@email.com, branch2@email.com'
                ])
                    
                @endcomponent

                {{-- phones --}}
                @component('components.form.input-form-group', [
                    'label' => 'Phone Numbers',
                    'name' => 'phone_numbers',
                    'placeholder' => 'e.g +23348011111111, +23348011111122'
                ])
                    
                @endcomponent
                
                {{-- address --}}
                @component('components.form.textarea-form-group', [
                    'label' => 'Address',
                    'name' => 'address',
                    'placeholder' => 'branch address here...'
                ])
                @endcomponent
                
            @endslot

            @slot('modal_footer')
                <button class="btn btn-default" type="button" data-dismiss="modal">
                    Exit
                </button>
                <button class="btn btn-primary" type="submit">
                    Add
                </button>
            @endslot
        @endcomponent
    </form>
@else
    @component('components.modal', [
        'modal_dialog_class' => 'modal-dialog-centered animated bounceInUp',
        'modal_id' => 'add_branch_modal',
        'modal_top_dismiss' => true,
    ])
        @slot('modal_body')
            <h4 class="text-muted text-center my-4">
                No region found!
            </h4>
        @endslot
        @slot('modal_footer')
            <button class="btn btn-default" type="button" data-dismiss="modal">
                Exit
            </button>
            <a class="btn btn-primary" href="{{ route('create.region') }}">
                Add Region
            </a>
        @endslot
    @endcomponent
@endif