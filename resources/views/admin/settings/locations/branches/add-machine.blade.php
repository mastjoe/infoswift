{{-- add branch modal --}}
@if (!$machine_types->count())

    @component('components.modal', [
        'modal_dialog_class' => 'modal-dialog-centered animated bounceInLeft',
        'modal_id' => 'add_machine_modal',
        'modal_top_dismiss' => true,
    ])
        @slot('modal_body')
            <div class="text-center text-muted">
                <h4>No machine type found</h4>
            </div>
        @endslot
        @slot('modal_footer')
            <button class="btn btn-default" type="button" data-dismiss="modal">
                Exit
            </button>
            <a class="btn btn-primary" href="{{ route('create.machine.type') }}">
                Add Machine Type
            </a>
        @endslot
    @endcomponent

@elseif (!$machine_vendors->count())

    @component('components.modal', [
        'modal_dialog_class' => 'modal-dialog-centered animated bounceInLeft',
        'modal_id' => 'add_machine_modal',
        'modal_top_dismiss' => true,
    ])
        @slot('modal_body')
            <div class="text-center text-muted">
                <h4>No machine vendor found</h4>
            </div>
        @endslot
        @slot('modal_footer')
            <button class="btn btn-default" type="button" data-dismiss="modal">
                Exit
            </button>
            <a class="btn btn-primary" href="{{ route('create.machine.vendor') }}">
                Add Machine Vendor
            </a>
        @endslot
    @endcomponent

@elseif (!$machine_statuses->count())

        @component('components.modal', [
            'modal_dialog_class' => 'modal-dialog-centered animated bounceInLeft',
            'modal_id' => 'add_machine_modal',
            'modal_top_dismiss' => true,
        ])
        @slot('modal_body')
            <div class="text-center text-muted">
                <h4>No machine status found</h4>
            </div>
        @endslot
        @slot('modal_footer')
            <button class="btn btn-default" type="button" data-dismiss="modal">
                Exit
            </button>
            <a class="btn btn-primary" href="{{ route('create.machine.status') }}" type="submit">
                Add Machine Status
            </a>
        @endslot
    @endcomponent

@else

    <form method="POST" action="{{ route('store.branch.machine', $branch->id) }}" id="add_machine_form">
        @component('components.modal', [
            'modal_dialog_class' => 'modal-dialog-centered animated bounceInLeft',
            'modal_id' => 'add_machine_modal',
            'modal_top_dismiss' => true,
        ])
            @slot('modal_header')
                <h5>
                    <i class="mdi mdi-library-plus ml-2 text-primary"></i>
                    Add Machine To Branch
                </h5>
            @endslot

            @slot('modal_body')
                @csrf
                <div class="form_response"></div>
                {{--  branch field  --}}
                @component('components.form.input-form-group', [
                    'label' => 'Branch',
                    'name' => 'branch',
                    'placeholder' => 'branch name here',
                    'read_only' => true,
                    'value' => $branch->branch
                ])
                    
                @endcomponent

                {{-- branch status --}}
                @component('components.form.input-form-group', [
                    'label' => 'Branch Status',
                    'name' => 'branch_status',
                    'read_only' => true,
                    'value' => 'inbranch'
                ])
                @endcomponent

                {{-- client --}}
                @component('components.form.input-form-group', [
                    'label' => 'Client',
                    'name' => 'client',
                    'read_only' => true,
                    'value' => $branch->client->short_name
                ])
                @endcomponent

                {{-- client --}}
                @component('components.form.input-form-group', [
                    'label' => 'Client',
                    'name' => 'client',
                    'form_group_class' => 'rd d-none',
                    'id' => 'client_field',
                    'read_only' => true,
                ])
                    
                @endcomponent
            
                {{-- machine name --}}
                @component('components.form.input-form-group', [
                    'label' => 'Name',
                    'name' => 'name',
                    'required' => true,
                ])
                    
                @endcomponent

                {{-- terminal id --}}
                @component('components.form.input-form-group', [
                    'label' => 'Terminal ID',
                    'name' => 'terminal_id',
                    'required' => true
                ])
                    
                @endcomponent

                {{-- serial number --}}
                @component('components.form.input-form-group', [
                    'label' => 'Serial Number',
                    'name' => 'serial_number',
                ])
                @endcomponent

                {{-- model number --}}
                @component('components.form.input-form-group', [
                    'label' => 'Model Number',
                    'name' => 'model_number',
                ])
                @endcomponent

                {{-- ip address --}}
                @component('components.form.input-form-group', [
                    'label' => 'Ip Address',
                    'name' => 'ip_address',
                ])
                    
                @endcomponent
                
                {{--  machine status  --}}
                @component('components.form.select-form-group', [
                    'label' => 'Machine Status',
                    'name' => 'machine_status',
                    'required' => true
                ])
                    @slot('options')
                        <option value="">Choose Machine Status</option>
                        @foreach ($machine_statuses as $status)
                            <option value="{{ $status->id }}">{{ $status->status }}</option>
                        @endforeach
                    @endslot
                @endcomponent

                {{--  machine vendor  --}}
                @component('components.form.select-form-group', [
                    'label' => 'Machine Vendor',
                    'name' => 'machine_vendor',
                    'required' => true
                ])
                    @slot('options')
                        <option value="">Choose Machine Vendor</option>
                        @foreach ($machine_vendors as $vendor)
                            <option value="{{ $vendor->id }}">{{ $vendor->vendor }}</option>
                        @endforeach
                    @endslot
                @endcomponent

                {{--  machine types  --}}
                @component('components.form.select-form-group', [
                    'label' => 'Machine Type',
                    'name' => 'machine_type',
                    'required' => true
                ])
                    @slot('options')
                        <option value="">Choose Machine Type</option>
                        @foreach ($machine_types as $type)
                            <option value="{{ $type->id }}">{{ $type->type }}</option>
                        @endforeach
                    @endslot
                @endcomponent
                
            @endslot

            @slot('modal_footer')
                <button class="btn btn-default" type="button" data-dismiss="modal">
                    Exit
                </button>
                <button class="btn btn-primary" type="submit">
                    Add
                </button>
            @endslot
        @endcomponent
    </form>
@endif