@extends('layouts.admin.app')
@section('title', 'Locations | Regions | '.$branch->region->region.' | Branches | '.$branch->branch)

@section('page', 'Locations | Branches | '.Str::limit($branch->branch, 15, '...'))

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('settings.locations') }}">Locations</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('show.region', $branch->region_id) }}">
            {{ Str::limit($branch->region->region, 10, '...') }}
        </a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('all.branches') }}">Branches</a>
    </li>
    <li class="breadcrumb-item active">
        {{ Str::limit($branch->branch, 15, '...') }}
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="col-12 mb-2 text-right">
            <button class="btn btn-success add_machine_btn" title="Add Machine">
                <span class="d-none d-md-inline-block">Add</span>
                <span class="mdi mdi-plus-circle ml-md-2"></span>
            </button>
            <button class="btn btn-warning edit_branch_btn" title="Edit Branch">
                <span class="d-none d-md-inline-block">Edit</span>
                <span class="mdi mdi-pencil ml-md-2"></span>
            </button>
            @if ($branch->deletable())                
                <button class="btn btn-primary delete_branch_btn" 
                    title="Delete Branch"
                    data-branch="{{ $branch->branch }}"
                    data-url="{{ route('delete.region', $branch->id) }}"
                    >
                    <span class="d-none d-md-inline-block">Delete</span>
                    <span class="mdi mdi-delete ml-md-2"></span>
                </button>
            @endif
        </div>
        <div class="col-md-3">
            @component('components.card')
                @slot('card_body')
                    <strong class="text-primary">Branch</strong>
                    <p>{{ $branch->branch }}</p>
                    <hr>
                    <strong class="text-primary">Region</strong>
                    <p>{{ $branch->region->region }}</p>
                    <hr>
                    <strong class="text-primary">State</strong>
                    <p>{{ $branch->state->state }}</p>
                    <hr>
                    <strong class="text-primary">Country</strong>
                    <p>{{ $branch->country->country }}</p>
                    <hr>
                    <strong class="text-primary">Created At</strong>
                    <p>{{ $branch->created_at->format('jS M, Y') }}</p>
                    <hr>
                    <strong class="text-primary">Last Updated</strong>
                    <p>{{ $branch->updated_at->format('jS M, Y') }}</p>
                    
                @endslot
            @endcomponent
        </div>
        <div class="col-md-9">
            {{-- states --}}
            @component('components.card')
                @slot('card_body')
                    <h4 class="card-title">
                        {{ ucwords($branch->branch) }} Machines
                    </h4>
                    <hr>
                    @if (count($branch->machines))
                        <div class="table-responsive-sm">
                            <table class="table table-bordered branches_table">
                                <thead>
                                    <tr>
                                        <th>Terminal ID</th>
                                        <th>Client</th>
                                        <th>Tickets</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($branch->machines as $machine)
                                        <tr>
                                            <td>
                                                <a href="{{ route('show.machine', $machine->id) }}">
                                                    {{ $machine->terminal_id }}
                                                </a>
                                            </td>
                                            <td>
                                            <span style="color:{{ $machine->client->color }}; font-weight: bold">
                                                   {{ $machine->client->short_name }}
                                                </span>
                                            </td>
                                            <td>
                                                <span class="badge badge-primary">
                                                    {{ number_format(0) }}
                                                </span>
                                            </td>
                                            
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @else
                        <div class="text-center text-muted my-4">
                            <h2>No machines found in branch</h2>
                            <button class="btn btn-primary my-3 waves-effect add_machine_btn">
                                Add New Machine
                            </button>
                        </div>
                    @endif
                @endslot
            @endcomponent

            {{--  custodians  --}}
            @component('components.card')
                @slot('card_header')
                    
                <h4 class="card-title">
                    {{ ucwords($branch->branch) }} Branch Custodians
                </h4>
                <div class="card-options">
                    <button class="btn-block-option add_custodian_btn" type="button">
                        <i class="mdi mdi-account-plus-outline mdi-24px"></i>
                    </button>
                </div>
                @endslot
                @slot('card_body')
                    
                    @if ($branch->custodians->count())
                        <div class="table table-responsive-sm">
                            <table class="table table-bordered custodians_table">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Phone</th>
                                        <th>Email</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($branch->custodians as $custodian)
                                        <tr>
                                            <td>
                                                <a>{{ $custodian->name }}</a>
                                            </td>
                                            <td>
                                                <a href="tel:{{ $custodian->phone }}">{{ $custodian->phone }}</a>
                                            </td>
                                            <td>
                                                <a href="mailto:{{ $custodian->email }}">{{ $custodian->email }}</a>
                                            </td>
                                            <td>
                                                <button class="btn btn-sm btn-primary waves-effect action dropdown-toggle" data-toggle="dropdown">
                                                    <span class="mdi mdi-settings-box"></span>
                                                </button>
                                                <div class="dropdown-menu">
                                                    <div class="dropdown-header">
                                                        <small>{{ $custodian->name }}</small>
                                                    </div>
                                                    <a class="dropdown-item edit_custodian_btn" 
                                                        data-url="{{ route('edit.branch.custodian', [$branch->id, $custodian->id]) }}"
                                                        data-name="{{ $custodian->name }}"
                                                        href="javascript:void(0)"
                                                    >
                                                        Edit
                                                    </a>
                                                    <a class="dropdown-item delete_custodian_btn"
                                                        data-url="{{ route('delete.branch.custodian', [$branch->id, $custodian->id]) }}"
                                                        data-name="{{ $custodian->name }}"
                                                        href="javascript:void(0)"
                                                    >
                                                        Delete
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @else
                        <div class="text-center text-muted my-4">
                            <h2>No custodian found in branch</h2>
                            <button class="btn btn-primary my-3 waves-effect add_custodian_btn">
                                Add New Custodian
                            </button>
                        </div>
                    @endif
                @endslot
            @endcomponent
        </div>
    </div>

    {{-- modal --}}
    @include('admin.settings.locations.branches.edit')
    @include('admin.settings.locations.branches.add-machine')
    @include('admin.settings.locations.branches.add-custodian')
@endsection

@push('js')
    <script src="{{ asset('js/admin/location.branch.js') }}"></script>
    <script>
        $(document).ready(function() {

            simpleDataTable('.machines_table, .custodians_table');

            @if (session('updated'))
                Swal.fire(
                    'Branch Updated',
                    '{{ session('updated') }}',
                    'success'
                );
            @endif

            @if (session('edit'))
                openEditBranchModal();
            @endif

            @if (session('create_machine'))
                openAddMachineModal();
            @endif

            @if (session('created_machine'))
                Swal.fire(
                    'Machine Added to Branch',
                    '{{ session('created_machine') }}',
                    'success'
                );
            @endif

            @if (session('create_custodian'))
                openAddCustodianModal();
            @endif

            @if (session('created_custodian'))
                Swal.fire(
                    'Custodian Added to Branch',
                    '{{ session('created_custodian') }}',
                    'success'
                );
            @endif
            
            @if (session('updated_custodian'))
                Swal.fire(
                    'Branch Custodian Updated',
                    '{{ session('updated_custodian') }}',
                    'success'
                );
            @endif

            @if (session('deleted_custodian'))
                Swal.fire(
                    'Branch Custodian Deleted',
                    '{{ session('deleted_custodian') }}',
                    'success'
                );
            @endif
        });
    </script>
@endpush