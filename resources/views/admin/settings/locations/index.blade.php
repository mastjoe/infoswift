@extends('layouts.admin.app')
@section('title', 'Settings | Locations')

@section('page', 'Settings | Locations')

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item active">
        <a>Settings - Location</a>
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-4">
            <a href="{{ route('all.countries') }}">
                @component('components.card')
                    @slot('card_body')
                        <h4>Countries</h4>
                    @endslot
                @endcomponent
            </a>
        </div>
        <div class="col-md-4">
            <a href="{{ route('all.states') }}">
                @component('components.card')
                    @slot('card_body')
                        <h4>States</h4>
                    @endslot
                @endcomponent
            </a>
        </div>
        <div class="col-md-4">
            <a href="{{ route('all.regions') }}">
                @component('components.card')
                    @slot('card_body')
                        <h4>Regions</h4>
                    @endslot
                @endcomponent
            </a>
        </div>
        <div class="col-md-4">
            <a href="{{ route('all.branches') }}">
                @component('components.card')
                    @slot('card_body')
                        <h4>Branches</h4>
                    @endslot
                @endcomponent
            </a>
        </div>
    </div>
@endsection

@push('js')
@endpush