<form method="POST" action="{{ route('store.client.personnel', $client->id) }}" 
    id="add_personnel_form" enctype="multipart/form-data"
>
    @csrf
    @method('put')
    @component('components.modal', [
        'modal_dialog_class' => 'modal-dialog-centered animated zoomInDown',
        'modal_id' => 'add_personnel_modal',
        'modal_class' => 'animated rotateIn'
    ])
        @slot('modal_header')
            <h5>
                <i class="mdi mdi-account-plus-outline ml-2 text-primary"></i>
                Technical Personnel Form
            </h5>
        @endslot
    
        @slot('modal_body')
            {{-- name field --}}
           @component('components.form.input-form-group', [
                'name' => 'name',
                'label' => 'Name',
                'id' => 'name',
                'required' => true,
                'value' => $client->personnel_name
                ])
            @endcomponent
    
            {{-- phone field --}}
            @component('components.form.input-form-group', [
                'name' => 'phone',
                'type' => 'tel',
                'label' => 'Telephone',
                'id' => 'phone',
                'required' => true,
                'value' => $client->personnel_phone
                ])
            @endcomponent
    
            {{-- email field --}}
            @component('components.form.input-form-group', [
                'name' => 'email',
                'label' => 'Email',
                'id' => 'email',
                'type' => 'email',
                'required' => true,
                'value' => $client->personnel_email
                ])
            @endcomponent
        @endslot
        
        @slot('modal_footer')
            <button class="btn btn-default" type="button" data-dismiss="modal">
                Exit
            </button>
            <button class="btn btn-primary" type="submit">
                Save
            </button>
        @endslot
    @endcomponent
</form>