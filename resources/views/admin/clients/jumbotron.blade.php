<div class="row" style="margin-top:-30px">
    <div class="col-sm-12">
        <div class="page-title-box"
            style="background-image: url('{{ asset('images/photo13@2x.jpg') }}'); background-position: 0 50%; background-size: cover;padding:20px 30px;">
            <div class="text-center pt-3">
                <img class="rounded-circle thumb-lg img-thumbnail" src="{{ $client->logo() }}"
                    alt="Generic placeholder image">
                <h1>{{ $client->name  }}</h1>
                @can('update', $client)
                    
                <a class="btn btn-outline-warning waves-effect" href="{{ route('edit.client', [$client->id]) }}">
                    <span class="d-none d-md-inline-block">Edit </span>
                    <i class="mdi mdi-square-edit-outline mr-1"></i>
                </a>
                <button class="btn btn-outline-primary waves-effect edit_client_logo_btn">
                    <span class="d-none d-md-inline-block">Logo</span>
                    <i class="mdi mdi-camera"></i>
                </button>
                @endcan
            </div>
        </div>
    </div>
</div>