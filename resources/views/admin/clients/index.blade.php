@extends('layouts.admin.app')
@section('title', 'All Clients')

@section('page', 'All Clients')

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item active">Clients</li>
@endsection

@section('content')
    @if (count($clients))
        <div class="text-right my-2">
            @can('create', App\Client::class)
            <a class="btn btn-primary waves-effect" href="{{ route('create.client') }}">
                <span class="d-none d-md-inline-block">New Client</span>
                <i class="mdi mdi-plus-circle-outline mr-1"></i>
            </a>
            @endcan
        </div>
        <div class="row">
            @foreach ($clients as $client)
                <div class="col-xl-4 col-lg-6">
                    <a href="{{ route('show.client', [$client->id]) }}">
                        @include('components.entity.client')
                    </a>
                </div>
            @endforeach
        </div>
        <div class="my-4">
            {{ $clients->links() }}
        </div>
    @else
        @component('components.empty')
            @slot('text')
                No client found
            @endslot
            @slot('body')
                <a class="btn btn-primary btn-lg" href=" {{ route('create.client') }} ">
                    Create Client
                </a>
            @endslot
        @endcomponent
    @endif
@endsection

@push('js')
    <script>
        $(document).ready(function() {
            @if (session('deleted_client'))
                Swal.fire(
                    'Client Deleted!',
                    '{{ session('deleted_client') }}',
                    'success'
                );
            @endif
        })
    </script>
@endpush