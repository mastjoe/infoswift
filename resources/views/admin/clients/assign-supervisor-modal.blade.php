<form method="POST" action="{{ route('assign.client.supervisor', $client->id) }}">
    @csrf
    @method('put')
    @component('components.modal', [
        'modal_dialog_class' => 'modal-dialog-centered animated zoomInDown',
        'modal_id' => 'assign_supervisor_modal',
        'modal_class' => 'animated zoomIn'
    ])
        @slot('modal_header')
            <h5>
                <i class="mdi mdi-account-plus-outline mr-2 text-primary"></i>
                {{ $client->supervisor_id ? "Update " : "Assign" }}
                Supervisor
            </h5>
        @endslot
    
        @slot('modal_body')
            @if ($supervisors->count())
                {{-- name field --}}
                @component('components.form.select-form-group', [
                    'name' => 'supervisor',
                    'label' => 'Supervisor',
                    'id' => 'name',
                    'required' => true,
                    'value' => $client->personnel_name
                    ])
                    @slot('options')
                        <option value="">Choose Supervisor</option>
                        @foreach ($supervisors as $supervisor)
                            <option value="{{ $supervisor->id }}"
                                {{ $supervisor->id == $client->supervisor_id ? "disabled" : null }}    
                                {{ $supervisor->id == $client->supervisor_id ? "selected" : null }}    
                            >
                                {{ $supervisor->full_name() }}
                            </option>
                        @endforeach
                    @endslot
                @endcomponent
            @else
                <div class="text-center my-5">
                    <h4 class="text-muted">
                        No supervisor found
                    </h4>
                    @can('update', $client)
                        <a class="btn btn-primary waves-effect mt-2" href="{{ route('all.staff') }}">
                            Assign role to staff
                        </a>
                    @endcan
                </div>
            @endif
        @endslot
        
        @slot('modal_footer')
            <button class="btn btn-default" type="button" data-dismiss="modal">
                Exit
            </button>
            @if ($supervisors->count())                
                <button class="btn btn-primary" type="submit">
                    Save
                </button>
            @endif
        @endslot
    @endcomponent
</form>