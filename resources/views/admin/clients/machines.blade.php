@extends('layouts.admin.app')
@section('title', $client->name.' | Machines')

@section('page', $client->name.' | Machines')

@push('css')
    <style>
        .avatar__holder {
            width: 260px;
            height: 200px;
        }
    </style>
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('all.clients') }}">Clients</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('show.client', $client->id) }}">{{ $client->code }}</a>
    </li>
    <li class="breadcrumb-item active">
        Machines
    </li>
@endsection

@section('content')
   
    @include('admin.clients.jumbotron')

    <div class="row mt-0">
        <div class="col-md-9 order-md-1">
           @if ($client->machines->count())
              @component('components.card')
                  @slot('card_header')
                      <h4 class="card-title">{{ $client->name }} Teller Machines</h4>
                  @endslot
                  @slot('card_body')
                      <div class="table-responsive-sm">
                          <table class="table table-bordered machines_table">
                              <thead>
                                  <tr>
                                      <th>Machine</th>
                                      <th>Branch</th>
                                      <th>Region</th>
                                      <th>Tickets</th>
                                  </tr>
                              </thead>
                              <tbody>
                                    @foreach ($client->machines as $m)
                                        <tr>
                                            <td>
                                                <a class="text-primary font-weight-bold" href="{{ route('show.machine', $m->id) }}">
                                                    {{ $m->terminal_id }}
                                                </a>
                                            </td>
                                            <td>
                                                {{ $m->branch->branch }}
                                            </td>
                                            <td>
                                                {{ $m->region->region }}
                                            </td>
                                            <td>
                                                <span class="badge badge-primary">
                                                    {{ number_format($m->tickets->count()) }}
                                                </span>
                                            </td>
                                        </tr>
                                    @endforeach
                              </tbody>
                          </table>
                      </div>
                  @endslot
              @endcomponent 
           @else
            <div class="row">
                <div class="text-center col-12 my-5">
                    <h2 class="h1 text-muted">No client machine found!</h2>
                    @can('create', App\Machine::class)                        
                    <a class="btn btn-primary btn-lg" href=" {{ route('create.machine') }} ">
                        Add Machine
                    </a>
                    @endcan
                </div>
            </div>
           @endif
        </div>
        <div class="col-md-3 order-md-12 ">
           @include('admin.clients.nav')
        </div>

        {{-- modals --}}
        @include('admin.clients.update-logo-modal')
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/admin/client.js') }}"></script>
    <script src="{{ asset('js/util.image.js') }}"></script>
    <script>
        $(document).ready(function() {
            injectAvatarInForm('.logo_image_inject');
            simpleDataTable('.machines_table');
        });
    </script>
@endpush