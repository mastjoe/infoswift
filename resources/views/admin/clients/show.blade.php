@extends('layouts.admin.app')
@section('title', $client->name)

@section('page', $client->name)

@push('css')
    <style>
        .table.table-borderless tr td:nth-child(1) {
            color: #f16c69;
            font-weight: bold;
            width: 20%;
        }
        .avatar__holder {
            width: 260px;
            height: 200px;
        }
    </style>
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('all.clients') }}">Clients</a>
    </li>
    <li class="breadcrumb-item active">
        {{ $client->code }}
    </li>
@endsection

@section('content')
    @include('admin.clients.jumbotron')

    <div class="row mt-0">
        <div class="col-md-9 order-md-1">
            @component('components.card')
                @slot('card_body')
                    <table class="table table-sm table-borderless">
                        <tbody>
                            <tr>
                                <td>Name</td>
                                <td>
                                    {{ $client->name }}
                                </td>
                            </tr>
                            <tr>
                                <td>Short Name</td>
                                <td>
                                    {{ $client->short_name }}
                                </td>
                            </tr>
                            <tr>
                                <td>Code</td>
                                <td>
                                    {{ $client->code }}
                                </td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td>
                                    {{ $client->email }}
                                </td>
                            </tr>
                            <tr>
                                <td>Website</td>
                                <td>
                                    {{ $client->website ?? '-' }}
                                </td>
                            </tr>
                            <tr>
                                <td>Address</td>
                                <td>
                                    {{ $client->address ?? '-' }}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                @endslot
            @endcomponent

            {{--  technical personnel   --}}
            @component('components.card')
                @slot('card_header')
                   <b>Technical Personnel</b>
                   @if ($client->personnel_name)
                       <div class="card-options">
                            @can('update', $client)                                
                                <button class="btn btn-sm btn-primary btn-sm waves-light waves-effect add_personnel_btn">
                                    Update
                                    <i class="mdi mdi-account-edit ml-2"></i>
                                </button>
                            @endcan
                       </div>
                   @endif
                @endslot
                @slot('card_body')
                    @isset($client->personnel_name)
                        <table class="table table-borderless">
                            <tr>
                                <td>Name</td>
                                <td>
                                    {{ $client->personnel_name }}
                                </td>
                            </tr>
                            <tr>
                                <td>Telephone</td>
                                <td>
                                    {{ $client->personnel_phone }}
                                </td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td>
                                    {{ $client->personnel_email }}
                                </td>
                            </tr>
                        </table>
                    @else
                        <div class="text-center my-5">
                            <h2 class="h2">Technical personnel not found!</h2>

                            @can('update', $client)                                
                                <button class="btn btn-primary add_personnel_btn mt-2">
                                    Add Technical Peronnel
                                </button>
                            @endcan
                        </div>
                    @endisset
                @endslot
            @endcomponent

            {{--  company supervisor  --}}
            @component('components.card')
                @slot('card_header')
                    <b>Company Supervisor</b>
                    <div class="card-options">
                        @can('update', $client)                            
                            <button class="btn btn-block-option btn-primary btn-sm waves-effect waves-light assign_supervisor_btn">
                                Update
                                <i class="mdi mdi-account-edit ml-2"></i>
                            </button>
                        @endcan
                    </div>
                @endslot
                @slot('card_body')
                    @if ($client->supervisor)
                        <table class="table table-sm table-borderless">
                            <thead>
                                <th width="170"></th>
                                <th></th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><b class="text-primary">Supervisor Name</b></td>
                                    <td>{{ $client->supervisor->full_name() }}</td>
                                </tr>
                                <tr>
                                    <td><b class="text-primary">Email</b></td>
                                    <td>{{ $client->supervisor->email }}</td>
                                </tr>
                                <tr>
                                    <td><b class="text-primary">Phone Number</b></td>
                                    <td>{{ $client->supervisor->phone }}</td>
                                </tr>
                            </tbody>
                        </table>
                        @can('view', $client)
                            <div class="text-center my-2">
                                <a href="{{ route('show.staff', $client->supervisor_id) }}">See More</a>
                            </div>
                        @endcan
                    @else
                        <div class="text-center my-4">
                            <h4 class="text-muted">No supervisor assigned</h4>
                            @can('update', $client)                                
                                <button class="btn btn-primary waves-effect assign_supervisor_btn">
                                    Assign Supervisor
                                </button>
                            @endcan
                        </div>
                    @endif
                @endslot
            @endcomponent
        </div>
        <div class="col-md-3 order-md-12 ">
           @include('admin.clients.nav')
        </div>
    </div>

    {{--  modal  --}}
    @include('admin.clients.add-personnel-modal')
    @include('admin.clients.update-logo-modal')
    @include('admin.clients.assign-supervisor-modal')
@endsection

@push('js')
    <script src="{{ asset('js/admin/client.js') }}"></script>
    <script src="{{ asset('js/util.image.js') }}"></script>
    <script>
        $(document).ready(function() {

            injectAvatarInForm('.logo_image_inject');

            @if (session('new_client'))
                Swal.fire(
                    'Client Record Created!',
                    '{{ session('new_client') }}',
                    'success'
                );
            @endif

            @if (session('updated_client'))
                Swal.fire(
                    'Client Record Updated!',
                    '{{ session('updated_client') }}',
                    'success'
                )
            @endif

            @if (session('logo_uploaded'))
                Swal.fire(
                    'Client Logo Updated!',
                    '{{ session('logo_uploaded') }}',
                    'success'
                )
            @endif

            @if (session('assign_supervisor'))
                showAssignSupervisorModal();
            @endif

            @if (session('assigned_supervisor'))
                Swal.fire(
                    'Supervisor Assigned',
                    '{{ session('assigned_supervisor') }}',
                    'success'
                )
            @endif
            @if (session('password_resetted'))
                Swal.fire(
                    'Password Resetted',
                    '{{ session('password_resetted') }}',
                    'success'
                )
            @endif
        });
    </script>
@endpush