<a class="btn btn-primary btn-block my-2 waves-effect" href="{{ route('show.client', $client->id) }}">
    Client Details
</a>
<a class="btn btn-secondary btn-block my-2 waves-effect" href="{{ route('show.client.regions', $client->id) }}">
    Regions
</a>
<a class="btn btn-secondary btn-block my-2 waves-effect"  href="{{ route('show.client.branches', $client->id) }}">
    Branches
</a>
<a class="btn btn-secondary btn-block my-2 waves-effect" href="{{ route('show.client.machines', $client->id) }}">
    Teller Machines
</a>

@can('update', $client)
<button class="btn btn-secondary btn-block my-2 waves-effect add_personnel_btn">
    Technical Personnel
</button>
@endcan

@can('update', $client)
<button class="btn btn-warning btn-block my-2 waves-effect reset_client_password_btn"
    data-url="{{ route('reset.client.password', $client->id) }}"
>
    Reset Password
</button>
@endcan

@if ($client->deletable())
@can('delete', $client)
<button class="btn btn-primary btn-block my-2 waves-effect delete_client_btn"
    data-url="{{ route('delete.client', $client->id) }}">
    Delete
</button>
@endcan
@endif

