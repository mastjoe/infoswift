@extends('layouts.admin.app')
@section('title', $client->name.' | Branches')

@section('page', $client->name.' | Branches')

@push('css')
    <style>
        .avatar__holder {
            width: 260px;
            height: 200px;
        }
    </style>
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('all.clients') }}">Clients</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('show.client', $client->id) }}">{{ $client->code }}</a>
    </li>
    <li class="breadcrumb-item active">
        Branches
    </li>
@endsection

@section('content')
   
    @include('admin.clients.jumbotron')

    <div class="row mt-0">
        <div class="col-md-9 order-md-1">
           @if ($client->branches->count())
              @component('components.card')
                  @slot('card_header')
                      <h4 class="card-title">{{ $client->name }} Branches</h4>
                  @endslot
                  @slot('card_body')
                      <div class="table-responsive-sm">
                          <table class="table table-bordered machines_table">
                              <thead>
                                  <tr>
                                      <th>Branch</th>
                                      <th>Region</th>
                                      <th>Custodians</th>
                                      <th>Machines</th>
                                  </tr>
                              </thead>
                              <tbody>
                                    @foreach ($client->branches as $b)
                                        <tr>
                                            <td>
                                                <a class="text-primary font-weight-bold" href="{{ route('show.branch', $b->id) }}">
                                                    {{ $b->branch }}
                                                </a>
                                            </td>
                                            <td>
                                                {{ $b->region->region }}
                                            </td>
                                            <td>
                                                <span class="badge badge-dark">
                                                    {{ number_format($b->custodians->count()) }}
                                                </span>
                                            </td>
                                            <td>
                                                <span class="badge badge-primary">
                                                    {{ number_format($b->machines->count()) }}
                                                </span>
                                            </td>
                                        </tr>
                                    @endforeach
                              </tbody>
                          </table>
                      </div>
                  @endslot
              @endcomponent 
           @else
            <div class="row">
                <div class="text-center col-12 my-5">
                    <h2 class="h1 text-muted">Client has no branch at the moment!</h2>
                    @can('create', App\Branch::class)                        
                    <a class="btn btn-primary btn-lg" href=" {{ route('create.branch') }} ">
                        Add a Branch
                    </a>
                    @endcan
                </div>
            </div>
           @endif
        </div>
        <div class="col-md-3 order-md-12 ">
           @include('admin.clients.nav')
        </div>

        {{-- modals --}}
        @include('admin.clients.update-logo-modal')
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/admin/client.js') }}"></script>
    <script src="{{ asset('js/util.image.js') }}"></script>
    <script>
        $(document).ready(function() {
            injectAvatarInForm('.logo_image_inject');
            simpleDataTable('.machines_table');
        });
    </script>
@endpush