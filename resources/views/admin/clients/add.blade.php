@extends('layouts.admin.app')
@section('title', 'New Client')

@section('page', 'New Client')

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href=" {{ route('all.clients') }} ">Clients</a>
    </li>
    <li class="breadcrumb-item active">
        Create
    </li>
@endsection

@section('content')
<form method="POST" action="{{ route('store.client') }}">
    @csrf
    <div class="row justify-content-center">
        <div class="col-md-10">
            @component('components.card')
                @slot('card_class')
                    mt-3
                @endslot
                @slot('card_body')
                    <h4 class="card-title">
                        Client Form
                    </h4>
                    <hr>
                    {{--  code field  --}}
                    @component('components.form.input-form-group', [
                        'name'             => 'code',
                        'label'            => 'Code',
                        'id'               => 'code',
                        'label_class'      => 'col-md-3 col-form-label text-md-right',
                        'input_wrap_class' => 'col-md-9',
                        'form_group_class' => 'row',
                        'required' => true,
                       'props' => ['maxlength' => 10]
                    ])
                    @endcomponent

                    {{--  name field  --}}
                    @component('components.form.input-form-group', [
                        'name'             => 'name',
                        'id'               => 'name',
                        'label'            => 'Name',
                        'label_class'      => 'col-md-3 col-form-label text-md-right',
                        'input_wrap_class' => 'col-md-9',
                        'form_group_class' => 'row',
                        'required' => true
                    ])                          
                    @endcomponent

                    {{--  short name  --}}
                    @component('components.form.input-form-group', [
                        'name'             => 'short_name',
                        'id'               => 'short_name',
                        'label'            => 'Short Name',
                        'label_class'      => 'col-md-3 col-form-label text-md-right',
                        'input_wrap_class' => 'col-md-9',
                        'form_group_class' => 'row',
                        'required' => true
                    ])                            
                    @endcomponent

                    {{--  color  --}}
                    @component('components.form.input-form-group', [
                        'type'             => 'color',
                        'id'               => 'color',
                        'name'             => 'color',
                        'label'            => 'Color',
                        'label_class'      => 'col-md-3 col-form-label text-md-right',
                        'input_wrap_class' => 'col-md-9',
                        'form_group_class' => 'row',
                        'required' => true
                    ])
                    @endcomponent

                    {{--  email field  --}}
                    @component('components.form.input-form-group', [
                        'type'             => 'email',
                        'id'               => 'email',
                        'name'             => 'email',
                        'label'            => 'Email Address',
                        'label_class'      => 'col-md-3 col-form-label text-md-right',
                        'input_wrap_class' => 'col-md-9',
                        'form_group_class' => 'row',
                    ])
                    @endcomponent

                    {{--  website field  --}}
                    @component('components.form.input-form-group', [
                        'type'             => 'url',
                        'id'               => 'website',
                        'name'             => 'website',
                        'label'            => 'Website',
                        'label_class'      => 'col-md-3 col-form-label text-md-right',
                        'input_wrap_class' => 'col-md-9',
                        'form_group_class' => 'row',
                    ])
                    @endcomponent
                    
                    {{--  address field  --}}
                    @component('components.form.textarea-form-group', [
                        'id'               => 'address',
                        'name'             => 'address',
                        'label'            => 'Address',
                        'label_class'      => 'col-md-3 col-form-label text-md-right',
                        'input_wrap_class' => 'col-md-9',
                        'form_group_class' => 'row mb-4',
                        'placeholder' => 'client address here...(optional)'
                        ])
                    @endcomponent

                    <div class="form-group text-center mt-2 pt-2 border-top border-light">
                        <button class="btn btn-primary px-5" type="submit">
                            Add Client
                        </button>
                        <button class="btn btn-secondary px-5 ml-2" type="reset">
                            Clear
                        </button>
                    </div>

                @endslot
            @endcomponent
        </div>
    </div>
</form>
@endsection

@push('js')
@endpush