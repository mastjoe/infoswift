@extends('layouts.admin.app')
@section('title', 'Edit Client | '.$client->short_name)

@section('page', 'Edit Client | '.$client->short_name)

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('all.clients') }}">Clients</a>
    </li>
    <li class="breadcrumb-item active">
        <a href="{{ route('show.client', $client->id) }}">{{ $client->code }}</a>
    </li>
    <li class="breadcrumb-item active">
        Edit
    </li>
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-10">
            <form method="POST" action="{{ route('update.client', $client->id) }}">
                @csrf
                @component('components.card')
                    @slot('card_body')
                    <h4 class="card-title">
                        Client Update Form | {{ $client->code }}
                    </h4>
                    <hr>
                    {{--  response  --}}
                    @if (count($errors->all()))
                        <div class="row">
                            <div class="offset-md-3 col-md-9">
                                @component('components.alert', [
                                    'alert_class' => 'alert-primary',
                                    'alert_dismiss' => true
                                    ])
                                    @slot('text')
                                        {{ $errors->all()[0] }}
                                    @endslot
                                @endcomponent
                            </div>
                        </div>
                    @endif
                        {{--  code field  --}}
                    @component('components.form.input-form-group', [
                        'name'             => 'code',
                        'label'            => 'Code',
                        'id'               => 'code',
                        'label_class'      => 'col-md-3 col-form-label text-md-right',
                        'input_wrap_class' => 'col-md-9',
                        'form_group_class' => 'row',
                        'required' => true,
                        'value' => $client->code,
                       'props' => ['maxlength' => 10]
                    ])
                    @endcomponent

                    {{--  name field  --}}
                    @component('components.form.input-form-group', [
                        'name'             => 'name',
                        'id'               => 'name',
                        'label'            => 'Name',
                        'label_class'      => 'col-md-3 col-form-label text-md-right',
                        'input_wrap_class' => 'col-md-9',
                        'form_group_class' => 'row',
                        'value' => $client->name,
                        'required' => true
                    ])                          
                    @endcomponent

                    {{--  short name  --}}
                    @component('components.form.input-form-group', [
                        'name'             => 'short_name',
                        'id'               => 'short_name',
                        'label'            => 'Short Name',
                        'label_class'      => 'col-md-3 col-form-label text-md-right',
                        'input_wrap_class' => 'col-md-9',
                        'form_group_class' => 'row',
                        'value' => $client->short_name,
                        'required' => true
                    ])                            
                    @endcomponent

                    {{--  color  --}}
                    @component('components.form.input-form-group', [
                        'type'             => 'color',
                        'id'               => 'color',
                        'name'             => 'color',
                        'label'            => 'Color',
                        'label_class'      => 'col-md-3 col-form-label text-md-right',
                        'input_wrap_class' => 'col-md-9',
                        'form_group_class' => 'row',
                        'value' => $client->color,
                        'required' => true
                    ])
                    @endcomponent

                    {{--  email field  --}}
                    @component('components.form.input-form-group', [
                        'type'             => 'email',
                        'id'               => 'email',
                        'name'             => 'email',
                        'label'            => 'Email Address',
                        'label_class'      => 'col-md-3 col-form-label text-md-right',
                        'input_wrap_class' => 'col-md-9',
                        'form_group_class' => 'row',
                        'value' => $client->email
                    ])
                    @endcomponent

                    {{--  website field  --}}
                    @component('components.form.input-form-group', [
                        'type'             => 'url',
                        'id'               => 'website',
                        'name'             => 'website',
                        'label'            => 'Website',
                        'label_class'      => 'col-md-3 col-form-label text-md-right',
                        'input_wrap_class' => 'col-md-9',
                        'form_group_class' => 'row',
                        'value' => $client->website
                    ])
                    @endcomponent
                    
                    {{--  address field  --}}
                    @component('components.form.textarea-form-group', [
                        'id'               => 'address',
                        'name'             => 'address',
                        'label'            => 'Address',
                        'label_class'      => 'col-md-3 col-form-label text-md-right',
                        'input_wrap_class' => 'col-md-9',
                        'form_group_class' => 'row mb-4',
                        'value' => $client->address,
                        'placeholder' => 'client address here...(optional)'
                        ])
                    @endcomponent

                    <div class="form-group text-center mt-2 pt-2 border-top border-light">
                        <button class="btn btn-primary waves-effect px-3" type="submit">
                            Save
                        </button>
                    </div>

                    @endslot
                @endcomponent
            </form>
        </div>
    </div>
@endsection

@push('js')
@endpush