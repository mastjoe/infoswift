<form method="POST" action="{{ route('update.client.logo', $client->id) }}" 
    id="edit_client_logo_form" enctype="multipart/form-data"
>
    @csrf
    @component('components.modal', [
        'modal_dialog_class' => 'modal-dialog-centered modal-sm animated zoomInDown',
        'modal_id' => 'add_personnel_modal',
        'modal_class' => 'animated rotateIn'
    ])
        @slot('modal_header')
            <h5>
                <i class="mdi mdi-image mr-2 text-primary"></i>
                Update Client Logo
            </h5>
        @endslot
    
        @slot('modal_body')
            <div class="form-group row">
                <div class="col-12">
                    <div class="logo_image_inject" data-name="logo" data-image="{{ $client->logo() }}"></div>
                    <small class="text-secondary">
                        <i class="fa fa-info-circle mr-1"></i>
                        logo size should not be more than 500kb and must be of image type
                    </small>
                </div>
            </div>
        @endslot
        
        @slot('modal_footer')
            <button class="btn btn-default" type="button" data-dismiss="modal">
                Exit
            </button>
            <button class="btn btn-primary" type="submit">
                Save
            </button>
        @endslot
    @endcomponent
</form>
