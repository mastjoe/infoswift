@if ($day_audits->count())
    @foreach ($day_audits as $audit)
        @component('components.card', [
            'card_class' => 'audit_item',
            ])
            @slot('card_props')
                data-url="{{ route('show.audit.details', $audit->id) }}"
            @endslot
            @slot('card_body')
                <div class="text-sm clearfix">
                    <b class="mr-3 float-left text-primary">{{ $audit->created_at->format('h:i a') }}</b>
                    <div class="float-right">
                        @switch($audit->event)
                            @case("created")
                                <span class="ml-2 badge badge-success">Create</span>
                                @break
                            @case("updated")
                                <span class="ml-2 badge badge-warning">Update</span>
                                @break
                            @case("deleted")
                                <span class="ml-2 badge badge-primary">Delete</span>
                                @break
                            @default
                                
                        @endswitch
                    </div>
                </div>
                <div style="font-size: .85rem;">
                    {{ Str::limit($audit->description, 100, '...') }}
                </div>
                <div class="text-right">
                    <small>{{ $audit->user->full_name() }}</small>
                </div>
            @endslot
        @endcomponent
    @endforeach
@else
    <div class="my-5 text-center">
        <h2 class="text-muted">No audit events for {{ $date->format('jS M, Y') }}</h2>
    </div>
@endif