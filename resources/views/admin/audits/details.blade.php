<div class="modal-header">
    <h4 class="modal-title"><span class="mdi mdi-calendar-clock mr-2"></span> Audit Details</h4>
    <button class="close" type="button" data-dismiss="modal">
        <span>&times;</span>
    </button>
</div>

<div class="modal-body pt-0">
    <div>
        <table class="table table-sm table-borderless">
            <thead>
                <tr>
                    <th width="130"></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><b class="text-primary">Event:</b></td>
                    <td>
                        @if ($audit->event == "created")
                            <span class="badge badge-success">{{ $audit->event }}</span>
                        @elseif ($audit->event == "deleted")
                            <span class="badge badge-primary">{{ $audit->event }}</span>
                        @elseif($audit->event == "read") 
                            <span class="badge badge-secondary">{{ $audit->event }}</span>
                        @else
                            <span class="badge badge-warning">{{ $audit->event }}</span>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td><b class="text-primary">Date:</b></td>
                    <td>{{ $audit->created_at->format('jS M, Y') }}</td>
                </tr>
                <tr>
                    <td><b class="text-primary">Time:</b></td>
                    <td>{{ $audit->created_at->format('h:i a') }}</td>
                </tr>
                <tr>
                    <td><b class="text-primary">Description:</b></td>
                    <td>{{ $audit->description }}</td>
                </tr>
                <tr>
                    <td><b class="text-primary">User:</b></td>
                    <td><a href="{{ route('show.staff', $audit->user->id) }}">{{ $audit->user->full_name() }}</a></td>
                </tr>
                <tr>
                    <td><b class="text-primary">User Agent:</b></td>
                    <td>{{ $audit->user_agent }}</td>
                </tr>
                <tr>
                    <td><b class="text-primary">User Ip Address:</b></td>
                    <td>{{ $audit->ip_address ?? '-' }}</td>
                </tr>
            </tbody>
        </table>
        @if ($audit->auditable_id)
            <hr>
            @php
                $t = $audit->target();
            @endphp
            <div class="pb-3">
                @if ($audit->auditable_type == "App\Ticket")
                    {{-- ticket   --}}
                    <a href="{{ route('show.ticket', $t->id) }}" class="md-chip" data-toggle="tooltip" title="Ticket">{{ $t->ref }}</a>
                    {{--  client --}}
                    <a href="{{ route('show.client', $t->client->id) }}" class="md-chip" title="Client">{{ $t->client->short_name }}</a>

                @elseif ($audit->auditable_type == "App\Machine")
                    {{--  machine  --}}
                    <a href="{{ route('show.machine', $t->id) }}" class="md-chip" data-toggle="tooltip" title="Machine">
                        {{ $t->terminal_id }}
                    </a>
                    {{--  client  --}}
                    <a href="{{ route('show.client', $t->client->id ) }}" class="md-chip" title="Client">
                        {{ $t->client->short_name }}
                    </a>
                    {{--  machine branch  --}}
                    <a href="{{ route('show.branch', $t->branch->id) }}" class="md-chip" title="Branch">
                        {{ $t->branch->branch }}
                    </a>
                @elseif($audit->auditable_type == "App\Client")
                    {{--  client  --}}
                    <a href="{{ route('show.client', $t->id) }}" title="Client" class="md-chip">
                        {{ $t->short_name }}
                    </a>
                    {{--  supervisor  --}}
                    @if ($t->supervisor)
                        <a href="{{ route('show.staff', $t->supervisor->id) }}"  title="Supervisor" class="md-chip">
                            {{ $t->supervisor->full_name()  }}
                        </a>
                    @endif

                @elseif($audit->auditable_type == "App\User")
                    <a href="{{ route('show.staff', $t->id) }}" class="md-chip">{{ $t->full_name() }}</a>
                @endif
            </div>
        @endif
    </div>
    <div class="modal-footer">
        <button class="btn btn-default" data-dismiss="modal" type="button">
            Exit
        </button>
    </div>
</div>