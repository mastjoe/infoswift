@extends('layouts.admin.app')
@section('title', 'Audit Events')

@section('page', 'Audits')

@push('css')
<link rel="stylesheet" href="{{ asset('css/bootstrap-material-datetimepicker.css') }}">
<style>
    .audit_item{
        cursor: pointer;
    }
</style>
@endpush

@section('crumbs')
@endsection

@section('content')
    <div class="row">
        <div class="offset-md-2 col-md-8">
           <div class="container">
                @component('components.card')
                    @slot('card_body')

                    <form>
                        <div class="input-group">
                            <input type="text"
                                class="form-control"
                                id="date-picker"
                            >
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="mdi mdi-calendar-multiple"></i>
                                </span>
                            </div>
                        </div>
                    </form>
                    @endslot
                @endcomponent
           </div>
        </div>
    </div>
    <div class="row">
        <div class="offset-md-1 col-md-10">
            <div id="audit-content" data-url="{{ route('show.audit') }}">
                <div class="text-center">
                    <span class="fa fa-spin fa-2x fa-spinner"></span>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/moment-with-locale.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-material-datetimepicker.js') }}"></script>
    <script>
        $(document).ready(function() {
            const datePicker = $('#date-picker');

            datePicker.bootstrapMaterialDatePicker({
                time: false
            });

            const showAuditTarget = $('#audit-content');
            const showAuditForDate = function(date = "") {
                axios.get(showAuditTarget.data('url'), {params: {date}})
                    .then(function(data) {
                        showAuditTarget.html(data.data);
                    })
                    .catch(function(e) {
                        console.log(e.response);
                    })
                    .finally(function() {
                        handleAuditDetails()
                    });
            }

            showAuditForDate();

            // on change event of date input
            datePicker.on('change', function() {
                showAuditForDate($(this).val());
            });

            const handleAuditDetails = function() {
                $('.card.audit_item').on('click', function() {
                    const url = $(this).data('url');
                    pushAuxModalLoad({url});
                });
            }

        });
    </script>
@endpush