<form method="POST" action="{{ route('update.staff.guarantor', $g->id) }}" id="update_guarantor_form"
    enctype="multipart/form-data"
>
    @csrf
    {{-- @method('put') --}}
    <div class="modal-header">
        <h4 class="modal-title">
            <i class="mdi mdi-account-edit mr-2 text-primary"></i>
            Guarantor Update Form
        </h4>
        <button class="close" type="button" data-dismiss="modal">
            <span>&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="form-group row">
                <div class="col-12">
                    <div class="g_image_inject1" data-name="photo" data-image="{{ $g->avatar() }}"></div>
                    <small class="text-secondary">
                        <i class="fa fa-info-circle mr-1"></i>
                        Passport size should not be more than 500kb
                    </small>
                </div>
            </div>
            {{-- name   --}}
            @component('components.form.input-form-group', [
                'type' => 'text',
                'label' => 'Name',
                'name' => 'name',
                'required' => true,
                'value' =>  $g->name
            ])
            @endcomponent
            {{-- email   --}}
            @component('components.form.input-form-group', [
                'type' => 'email',
                'label' => 'Email',
                'name' => 'email',
                'required' => false,
                'value' => $g->email

            ])
            @endcomponent
            {{-- phone   --}}
            @component('components.form.input-form-group', [
                'type' => 'tel',
                'label' => 'Phone',
                'name' => 'phone',
                'required' => true,
                'value' => $g->phone
            ])
            @endcomponent
            {{-- relationship   --}}
            @component('components.form.input-form-group', [
                'type' => 'text',
                'label' => 'Relationship With Staff',
                'name' => 'relationship',
                'required' => true,
                'value' => $g->relationship
            ])
            @endcomponent
            {{-- occupation   --}}
            @component('components.form.input-form-group', [
                'type' => 'text',
                'label' => 'Occupation',
                'name' => 'occupation',
                'required' => false,
                'value' => $g->occupation
            ])
            @endcomponent
            {{-- address   --}}
            @component('components.form.textarea-form-group', [
                'label' => 'Address',
                'name' => 'address',
                'required' => false,
                'value' => $g->address
            ])
            @endcomponent
    </div>
    <div class="modal-footer">
        <button class="btn btn-default" type="button" data-dismiss="modal">
            Dismiss
        </button>
        <button class="btn btn-primary" type="submit">
            Save
        </button>
    </div>
</form>