@extends('layouts.admin.app')
@section('title', 'Edit Staff | '.$user->full_name())

@section('page', 'Edit Staff | '.$user->full_name())

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('all.staff') }}">Staff</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('show.staff', $user->id) }}">{{ $user->first_name }}</a>
    </li>
    <li class="breadcrumb-item active">
        Edit
    </li>
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-10">
            <form method="POST" action="{{ route('update.staff', $user->id) }}">
                @method('put')
                @csrf
                @component('components.card')
                    
                    @slot('card_body')
                        <h4 class="card-title">
                            Staff Update Form
                        </h4>
                        <hr>
                        {{--  first name  --}}
                        @component('components.form.input-form-group', [
                            'name'             => 'first_name',
                            'label'            => 'First Name',
                            'id'               => 'first_name',
                            'label_class'      => 'col-md-3 col-form-label text-md-right',
                            'input_wrap_class' => 'col-md-9',
                            'form_group_class' => 'row',
                            'value' => $user->first_name,
                            'required' => true
                        ])
                        @endcomponent

                        {{--  middle name  --}}
                        @component('components.form.input-form-group', [
                            'name' => 'middle_name',
                            'label' => 'Middle Name',
                            'id' => 'middle_name',
                            'label_class' => 'col-md-3 col-form-label text-md-right',
                            'input_wrap_class' => 'col-md-9',
                            'form_group_class' => 'row',
                            'value' => $user->middle_name,
                            'required' => false
                        ])
                        @endcomponent

                        {{--  last name  --}}
                        @component('components.form.input-form-group', [
                            'name' => 'last_name',
                            'label' => 'Last Name',
                            'id' => 'last_name',
                            'label_class' => 'col-md-3 col-form-label text-md-right',
                            'input_wrap_class' => 'col-md-9',
                            'form_group_class' => 'row',
                            'value' => $user->last_name,
                            'required' => true
                        ])
                        @endcomponent
                        

                        {{--  gender  --}}
                        @component('components.form.select-form-group', [
                            'name' => 'gender',
                            'label' => 'Gender',
                            'id' => 'last_name',
                            'label_class' => 'col-md-3 col-form-label text-md-right',
                            'input_wrap_class' => 'col-md-9',
                            'form_group_class' => 'row',
                            'value' => $user->gender,
                            'required' => true
                        ])
                            @slot('options')
                                <option value="">Choose Gender</option>
                                <option value="male" {{ $user->gender == "male" ? "selected" : null }}>Male</option>
                                <option value="female" {{ $user->gender == "female" ? "selected" : null }}>Female</option>
                            @endslot
                        @endcomponent

                        {{--  dob  --}}
                        @component('components.form.input-form-group', [
                            'name' => 'dob',
                            'label' => 'Date of Birth',
                            'id' => 'dob',
                            'type' => 'date',
                            'label_class' => 'col-md-3 col-form-label text-md-right',
                            'input_wrap_class' => 'col-md-9',
                            'form_group_class' => 'row',
                            'required' => false,
                            'value' => $user->dob->format('Y-m-d')
                        ])
                        @endcomponent

                        {{--  email  --}}
                        @component('components.form.input-form-group', [
                            'name' => 'email',
                            'label' => 'Email Address',
                            'id' => 'email',
                            'type' => 'email',
                            'label_class' => 'col-md-3 col-form-label text-md-right',
                            'input_wrap_class' => 'col-md-9',
                            'form_group_class' => 'row',
                            'required' => true,
                            'value' => $user->email
                        ])
                        @endcomponent

                        {{--  phone  --}}
                        @component('components.form.input-form-group', [
                            'name' => 'phone',
                            'label' => 'Telephone',
                            'id' => 'phone',
                            'type' => 'tel',
                            'label_class' => 'col-md-3 col-form-label text-md-right',
                            'input_wrap_class' => 'col-md-9',
                            'form_group_class' => 'row',
                            'required' => true,
                            'value' => $user->phone
                        ])
                        @endcomponent
                        {{--  address  --}}
                        @component('components.form.textarea-form-group', [
                            'name' => 'address',
                            'label' => 'Address',
                            'id' => 'address',
                            'label_class' => 'col-md-3 col-form-label text-md-right',
                            'input_wrap_class' => 'col-md-9',
                            'form_group_class' => 'row',
                            'required' => false,
                            'value' =>  old('address') ?? $user->address
                        ])
                        @endcomponent
                        <hr>
                            {{--  bio  --}}
                            @component('components.form.textarea-form-group', [
                                'name' => 'bio',
                                'label' => 'Bio',
                                'id' => 'bio',
                                'label_class' => 'col-md-3 col-form-label text-md-right',
                                'input_wrap_class' => 'col-md-9',
                                'form_group_class' => 'row',
                                'placeholder' => 'short bio of staff (optional)',
                                'required' => false,
                                'value' => old('bio') ?? $user->bio
                                ])
                            
                            @endcomponent

                            {{--  facebook  --}}
                            <div class="form-group row">
                                <label class="col-form-label text-md-right col-md-3">
                                    Facebook
                                </label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="ion-social-facebook"></i>
                                            </span>
                                        </div>
                                        <input type="text" 
                                            class="form-control"                 placeholder="facebook profile link (optional)"
                                            name="socials[facebook]" 
                                            @if (count($user->getSocials()) && array_key_exists('facebook', $user->getSocials()))
                                            value="{{ $user->getSocials()['facebook'] }}" @endif
                                        >
                                    </div>
                                </div>
                            </div>
                            {{--  twitter  --}}
                            <div class="form-group row">
                                <label class="col-form-label text-md-right col-md-3">
                                    Twitter
                                </label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="ion-social-twitter"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control" placeholder="Twitter profile link (optional)"
                                            name="socials[twitter]" @if (count($user->getSocials()) && array_key_exists('twitter', $user->getSocials()))
                                            value="{{ $user->getSocials()['twitter'] }}" @endif>
                                    </div>
                                </div>
                            </div>
                            {{--  linkedin  --}}
                            <div class="form-group row">
                                <label class="col-form-label text-md-right col-md-3">
                                    LinkedIn
                                </label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="ion-social-linkedin"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control" placeholder="LinkedIn profile link (optional)"
                                            name="socials[linkedin]" @if (count($user->getSocials()) && array_key_exists('linkedin', $user->getSocials()))
                                            value="{{ $user->getSocials()['linkedin'] }}" @endif>
                                    </div>
                                </div>
                            </div>
                            {{--  instagram  --}}
                            <div class="form-group row">
                                <label class="col-form-label text-md-right col-md-3">
                                    Instagram
                                </label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="ion-social-instagram"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control" placeholder="Instagram profile link (optional)"
                                            name="socials[instagram]" @if ($user->getSocials() && array_key_exists('instagram', $user->getSocials()))
                                            value="{{ $user->getSocials()['instagram'] }}" @endif>
                                    </div>
                                </div>
                            </div>
                        <hr>
                        <div class="form-group my-3 pb-2 text-center">
                            <button class="btn btn-primary px-5" type="submit">
                                Save
                            </button>
                            <button class="btn btn-secondary px-5 ml-2" type="reset">
                                Clear
                            </button>
                        </div>
                        
                    @endslot
                @endcomponent
            </form>
        </div>
    </div>
@endsection

@push('js')
@endpush