{{--  guarantor form modal  --}}
<form method="POST" action="{{ route('store.staff.guarantor', $user->id) }}" enctype="multipart/form-data" id="add_guarantor_form">
    @csrf
    @component('components.modal', [
        'modal_id' => 'add_guarantor_modal',
        'modal_top_dismiss' => true,
        'modal_dialog_class' => 'animated rollIn'
    ])
        @slot('modal_header')
            <h4 class="modal-title">
               <i class="mdi mdi-account-plus mr-2 text-primary"></i>
               Add Guarantor 
            </h4>
        @endslot
        @slot('modal_body')
            {{--  photo  --}}
            <div class="form-group row">
                <div class="col-12">
                    <div class="g_image_inject" data-name="photo" data-image="{{ asset('images/avatar16.jpg') }}"></div>
                    <small class="text-secondary">
                        <i class="fa fa-info-circle mr-1"></i>
                        Passport size should not be more than 500kb
                    </small>
                </div>
            </div>
            {{-- name   --}}
            @component('components.form.input-form-group', [
                'type' => 'text',
                'label' => 'Name',
                'name' => 'name',
                'required' => true,

            ])
            @endcomponent
            {{-- email   --}}
            @component('components.form.input-form-group', [
                'type' => 'email',
                'label' => 'Email',
                'name' => 'email',
                'required' => false,

            ])
            @endcomponent
            {{-- phone   --}}
            @component('components.form.input-form-group', [
                'type' => 'tel',
                'label' => 'Phone',
                'name' => 'phone',
                'required' => true,
            ])
            @endcomponent
            {{-- relationship   --}}
            @component('components.form.input-form-group', [
                'type' => 'text',
                'label' => 'Relationship With Staff',
                'name' => 'relationship',
                'required' => true,
            ])
            @endcomponent
            {{-- occupation   --}}
            @component('components.form.input-form-group', [
                'type' => 'text',
                'label' => 'Occupation',
                'name' => 'occupation',
                'required' => false,
            ])
            @endcomponent
            {{-- address   --}}
            @component('components.form.textarea-form-group', [
                'label' => 'Address',
                'name' => 'address',
                'required' => false,
            ])
            @endcomponent

        @endslot

        @slot('modal_footer')
            <button class="btn btn-default" type="button" data-dismiss="modal">
                Dismiss
            </button>

            <button class="btn btn-primary" type="submit">
                Save
            </button>
        @endslot
    @endcomponent
</form>