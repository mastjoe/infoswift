@extends('layouts.admin.app')
@section('title', 'Staff | '.$user->full_name().' | Roles')

@section('page', 'Staff | '.$user->full_name().' | Roles')

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('all.staff') }}">Staff</a>
    </li>
    <li class="breadcrumb-item active">
        <a href="{{ route('show.staff', $user->id) }}">{{ $user->first_name }}</a>
    </li>
    <li class="breadcrumb-item active">
        Roles
    </li>
@endsection

@section('content')
    {{-- @include('admin.staff.jumbotron') --}}
    <div class="row">
        <div class="col-12">
            <div>
                @if ($errors->count())
                    @component('components.alert', [
                        'text' => $errors->first(),
                        'alert_class' => 'alert-primary',
                        'alert_dismiss' => true
                    ])
                        
                    @endcomponent
                @endif

                @if ($unassigned_privileges->count())
                    <div class="alert alert-warning">
                        The following assignable privilege(s) <span class="font-weight-bold text-dark">{{ implode(', ', $unassigned_privileges->pluck('privilege')->toArray()) }}</span>
                        <form method="POST" class="form-inline" action="{{ route('assign.staff.all.privileges', $user->id) }}">
                            @csrf
                            <button class="btn btn-sm btn-light ml-3 waves-effect" type="submit">Assign Privileges</button>
                        </form>
                    </div>
                @endif
            </div>
            <form method="POST" action="{{ route('update.staff.roles', $user->id) }}">
                @method('put')
                @csrf
                @component('components.card')
                    @slot('card_body')
                        <h4 class="card-title">
                            Roles 
                            @if ($user->roles->count())
                                <span class="badge badge-primary">
                                    {{ number_format($user->roles->count()) }}
                                </span>
                            @endif
                        </h4>
                        <hr>
                        @component('components.form.select-form-group', [
                            'name' => 'roles[]',
                            'label' => 'Roles',
                            'id' => 'roles',
                            'required' => true,
                            'props' => ['multiple' => true]
                        ])
                            @slot('options')
                                @php
                                    $user_role_ids = $user->roles->pluck('id')->toArray();
                                @endphp
                                <option value="">Choose Roles</option>
                                @foreach ($roles as $role)
                                    <option value="{{ $role->id }}"
                                        {{ in_array($role->id, $user_role_ids) ? "selected" : null }}
                                    >
                                        {{ $role->role }}
                                    </option>
                                @endforeach
                            @endslot
                        @endcomponent

                        <div class="my-2 text-right">
                            <button class="btn btn-primary waves-effects" type="submit">
                                Save
                            </button>
                        </div>
                    @endslot
                @endcomponent
            </form>
            <form method="POST" action="{{ route('update.staff.privileges', $user->id) }}">
                @method('put')
                @csrf
                @component('components.card')
                    @slot('card_body')
                        <h4 class="card-title">
                            Privileges 
                             @if ($user->privileges->count())
                                <span class="badge badge-primary">
                                    {{ number_format($user->privileges->count()) }}
                                </span>
                            @endif
                        </h4>
                        <hr>
                        @component('components.form.select-form-group', [
                            'name' => 'privileges[]',
                            'label' => 'Privileges',
                            'id' => 'privileges',
                            'required' => true,
                            'props' => ['multiple' => true]
                        ])
                            @slot('options')
                                @php
                                    $user_privileges_id = $user->privileges->pluck('id')->toArray();
                                @endphp
                                <option value="">Choose privileges</option>
                                @foreach ($roles_privileges as $privilege)
                                    <option value="{{ $privilege->id }}"
                                        {{ in_array($privilege->id, $user_privileges_id ) ? "selected" : null }}
                                    >
                                        {{ $privilege->privilege }}
                                    </option>
                                @endforeach
                            @endslot
                        @endcomponent

                        <div class="my-2 text-right">
                            <button class="btn btn-primary waves-effects">
                                Save
                            </button>
                        </div>
                    @endslot
                @endcomponent
            </form>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/admin/staff.js') }}"></script>
    <script>
        $(document).ready(function() {
           $('#roles').select2({
               placeholder: 'select role'
           });
           $('#privileges').select2({
               placeholder: 'select privilege'
           });

           @if (session('updated_roles'))
                Swal.fire(
                    'Staff Roles Updated!',
                    '{{ session('updated_roles') }}',
                    'success'
                );
           @endif
           @if (session('updated_privileges'))
                Swal.fire(
                    'Staff Privileges Updated!',
                    '{{ session('updated_privileges') }}',
                    'success'
                );
           @endif

           @if (session('assigned_privileges'))
                Swal.fire(
                    'Privileges Updated',
                    '{{ session('assigned_privileges') }}',
                    'success'
                )
           @endif
        });
    </script>
@endpush