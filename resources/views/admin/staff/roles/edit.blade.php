@extends('layouts.admin.app')
@section('title', 'Staff Roles | '.$role->role.' | Edit')

@section('page', 'Roles | '.$role->role.' | Edit')

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('all.staff') }}">Staff</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('all.staff.roles') }}">Roles</a>
    </li>
    <li class="breadcrumb-item active">
        <a href="{{ route('show.staff.role', [$role->id]) }}">{{ $role->role }}</a>
    </li>
    <li class="breadcrumb-item active">
        Edit
    </li>
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-10">
            <form method="POST" action="{{ route('update.staff.role', $role->id) }}">
                @method('put')
                @csrf
                @component('components.card')
                    @slot('card_body')
                       <h4 class="card-title">
                           Edit Role  | {{ $role->role }}
                       </h4>
                       <hr>
                       @include('components.form.error')
                       {{-- role field --}}
                       @component('components.form.input-form-group', [
                           'label' => 'Role',
                           'name' => 'role',
                           'type' => 'text',
                           'required' => true,
                           'value' => $role->role
                       ])
                       @endcomponent

                       {{-- description --}}
                       @component('components.form.textarea-form-group', [
                           'name' => 'description',
                           'label' => 'Description',
                           'placeholder' => 'description of role (optional)',
                           'value' => $role->description
                       ])  
                       @endcomponent

                       {{-- privileges --}}
                       @component('components.form.select-form-group', [
                           'name' => 'privileges[]',
                           'label' => 'Privileges',
                           'id' => 'privileges_select',
                           'props' => ['multiple' => true]
                       ])
                           @slot('options')
                               @foreach ($categories as $key => $category)
                                    <optgroup label="{{ ucfirst($key) }}">
                                        @foreach ($category as $item)
                                            <option value="{{ $item->id }}"
                                                @if (in_array($item->privilege, $role->privileges->pluck('privilege')->toArray()))
                                                    selected
                                                @endif
                                            >
                                                {{ $item->privilege }}
                                            </option>
                                        @endforeach
                                    </optgroup>
                               @endforeach
                           @endslot
                       @endcomponent

                        <hr>
                        <div class="text-center">
                            <button class="btn btn-primary" type="submit">
                                Update Role
                            </button>
                        </div>
                    @endslot
                @endcomponent
            </form>
        </div>
    </div>
@endsection

@push('js')
    <script>
        $(document).ready(function() {
            $('#privileges_select').select2()
        });
    </script>
@endpush