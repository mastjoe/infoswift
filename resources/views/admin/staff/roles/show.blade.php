@extends('layouts.admin.app')
@section('title', 'Staff Roles | '.$role->role)

@section('page', 'Roles | '.$role->role)

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('all.staff') }}">Staff</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('all.staff.roles') }}">Roles</a>
    </li>
    <li class="breadcrumb-item active">
        {{ $role->role }}
    </li>
@endsection

@section('content')
    <div class="row">
        {{-- action button --}}
        <div class="col-12 text-right mb-2">
            <a class="btn btn-warning" href="{{ route('edit.staff.role', [$role->id]) }}">
                <span class="d-none d-md-inline-block">Edit</span>
                <i class="fas fa-pencil-alt ml-md-2"></i>
            </a>
            <button class="btn btn-primary delete_role_btn"
                data-role="{{ $role->role }}"
                data-url="{{ route('delete.staff.role', $role->id) }}"
            >
                <span class="d-none d-md-inline-block">Delete</span>
                <i class="fas fa-trash-alt ml-md-2"></i>
            </button>
        </div>
        <div class="col-md-3">
            {{-- role info --}}
            @component('components.card')
                @slot('card_body')
                    <h4>{{ $role->role }}</h4>
                    <hr>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <h6 class="text-primary">Privileges</h6>
                            <h5>{{ number_format($role->privileges->count()) }}</h5>
                        </li>
                        <li class="list-group-item">
                            <h6 class="text-primary">Staff</h6>
                            <h5></h5>
                        </li>
                        <li class="list-group-item">
                            <h6 class="text-primary">Created</h6>
                            <h5>
                                {{ $role->created_at->format('jS M, Y') }}
                            </h5>
                        </li>
                        <li class="list-group-item">
                            <h6 class="text-primary">Last Updated</h6>
                            <h5>
                                {{ $role->updated_at->format('jS M, Y') }}
                            </h5>
                        </li>
                    </ul>
                @endslot
            @endcomponent
        </div>
        <div class="col-md-9">
            {{-- description --}}
            @component('components.card')
                @slot('card_body')
                    <h4 class="card-title">
                        Description
                    </h4>
                    <hr>
                    @isset($role->description)
                        {{ $role->description }}
                    @else
                        <div class="text-center text-muted my-5">
                            <h5>No description of role available</h5>
                        </div>
                    @endisset
                @endslot
            @endcomponent

            {{-- priviledges on role --}}
            @component('components.card')
                @slot('card_body')
                    <h4 class="card-title">
                        Role Privileges
                    </h4>
                    <hr>
                    @if ($role->privileges->count())
                        <div class="row">
                            @foreach ($role->privileges as $privilege)
                                <div class="col-md-4">
                                    <div class="bg-light my-1 p-1 rounded-sm">
                                        <h6 class="text-primary">{{ $privilege->privilege }}</h6>
                                    </div>
                                </div>
                            @endforeach
                        </div>  
                    @else
                        <div class="text-center text-muted my-5">
                            <h5>No Privileges assigned to Role</h5>
                        </div>
                    @endif
                @endslot
            @endcomponent

            {{-- staff on  role --}}
            @component('components.card')
                @slot('card_body')
                    <h4 class="card-title">
                        Staff
                    </h4>
                    <hr>
                    <div class="table-responsive-sm">
                        @if ($role->users->count())
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($role->users as $user)
                                        <tr>
                                            <td>
                                                {{ $user->full_name() }}
                                            </td>
                                            <td>
                                                {{ $user->email }}
                                            </td>
                                            <td>
                                                {{ $user->phone }}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="text-center text-muted my-5">
                                <h5>No user is currently on privilege!</h5>
                            </div>
                        @endif
                    </div>
                @endslot
            @endcomponent
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/admin/role.js') }}"></script>
    <script>
        $(document).ready(function() {
            @if (session('new'))
                swal.fire(
                    'New Role Created',
                    '{{ session('new') }}',
                    'success'
                )
            @endif

            @if (session('updated'))
                swal.fire(
                    'Role Updated',
                    '{{ session('updated') }}',
                    'success'
                );
            @endif
        });
    </script>
@endpush