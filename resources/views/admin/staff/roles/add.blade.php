@extends('layouts.admin.app')
@section('title', 'Create Staff Role')

@section('page', 'Create Role')

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('all.staff') }}">Staff</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('all.staff.roles') }}">Roles</a>
    </li>
    <li class="breadcrumb-item active">
        Create
    </li>
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-10">
            <form method="POST" action="{{ route('store.staff.role') }}">
                @csrf
                @component('components.card')
                    @slot('card_body')
                       <h4 class="card-title">
                           New Role
                       </h4>
                       <hr>
                       @include('components.form.error')
                       {{-- role field --}}
                       @component('components.form.input-form-group', [
                           'label' => 'Role',
                           'name' => 'role',
                           'type' => 'text',
                           'required' => true
                       ])
                       @endcomponent

                       {{-- description --}}
                       @component('components.form.textarea-form-group', [
                           'name' => 'description',
                           'label' => 'Description',
                           'placeholder' => 'description of role (optional)',
                       ])  
                       @endcomponent

                       {{-- privileges --}}
                       @component('components.form.select-form-group', [
                           'name' => 'privileges[]',
                           'label' => 'Privileges',
                           'id' => 'privileges_select',
                           'props' => ['multiple' => true]
                       ])
                           @slot('options')
                               @foreach ($categories as $key => $category)
                                    <optgroup label="{{ ucfirst($key) }}">
                                        @foreach ($category as $item)
                                            <option value="{{ $item->id }}">{{ $item->privilege }}</option>
                                        @endforeach
                                    </optgroup>
                               @endforeach
                           @endslot
                       @endcomponent

                        <hr>
                        <div class="text-center">
                            <button class="btn btn-primary" type="submit">
                                Add Role
                            </button>
                        </div>
                    @endslot
                @endcomponent
            </form>
        </div>
    </div>
@endsection

@push('js')
    <script>
        $(document).ready(function() {
            $('#privileges_select').select2({
                placeholder: 'Select Privileges'
            })
        });
    </script>
@endpush