@extends('layouts.admin.app')
@section('title', 'All Staff Roles')

@section('page', 'Staff Roles')

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('all.staff') }}">Staff</a>
    </li>
    <li class="breadcrumb-item active">
        Roles
    </li>
@endsection

@section('content')
    @if (count($roles))
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="text-right mb-2">
                    <a class="btn btn-primary waves-effect" href="{{ route('create.staff.role') }}">
                        <span class="d-none d-md-inline-block">New Role</span>
                        <i class="mdi mdi-plus-circle-outline mr-1"></i>
                    </a>
                </div>
                @component('components.card', ['card_class' => 'm-b-20'])
                    @slot('card_body')
                        <h4 class="header-title mt-0">
                            All Staff Roles
                        </h4>
                        <div class="table-responsive-sm">
                            <table class="table table-hover table-bordered dt-responsive nowrap" id="dt">
                                <thead>
                                    <tr>
                                        <th>Role</th>
                                        <th>Privileges</th>
                                        <th>Staff</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($roles as $role)
                                        <tr>
                                            <td>
                                                <a class="font-weight-bold" href="{{ route('show.staff.role', $role->id) }}">
                                                    {{ $role->role }}
                                                </a>
                                            </td>
                                            <td>
                                                <span class="badge badge-dark">
                                                    {{ number_format($role->privileges->count()) }}
                                                </span>
                                            </td>
                                            <td>
                                                <span class="badge badge-info">
                                                    {{ number_format($role->users->count()) }}
                                                </span>
                                            </td>
                                            <td align="center">
                                                <div class="btn-group btn-group-sm">
                                                    <a class="btn btn-secondary"
                                                        title="View"
                                                        href="{{ route('show.staff.role', $role->id) }}"
                                                    >
                                                        <i class="fas fa-info-circle"></i>
                                                    </a>
                                                    <a class="btn btn-warning"
                                                        title="Edit"
                                                        href="{{ route('edit.staff.role', $role->id) }}"
                                                    >
                                                        <i class="fas fa-edit"></i>
                                                    </a>
                                                    <button class="btn btn-primary delete_role_btn {{ !$role->deletable() ? "disabled" : null }}"
                                                        title="Delete"
                                                        data-role="{{ $role->role }}"
                                                        data-url="{{ route('delete.staff.role', $role->id) }}"
                                                        {{ !$role->deletable() ? "disabled" : null }}
                                                    >
                                                        <i class="fas fa-trash"></i>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endslot
                @endcomponent
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @component('components.card',[
                    'card_class' => 'm-b-30'
                ])
                    @slot('card_body')
                        <h4 class="mt-0 header-title">Donut Chart</h4>
                        <div id="roles_donut" data-url="{{ route('staff.roles.donut') }}"></div>
                    @endslot
                @endcomponent
            </div>
        </div>
    @else
       @component('components.empty', [
           'text' => 'No staff role was found'
       ])
           @slot('body')
               <a class="btn btn-lg btn-primary my-3 waves-effect" href="{{ route('create.staff.role') }}">
                   Add a Staff Role
               </a>
           @endslot
       @endcomponent
    @endif
@endsection

@push('js')
    <script src="{{ asset('js/admin/role.js') }}"></script>
    <script src="{{asset('plugins/raphael.min.js')}}"></script>
   
    <script>
        $(document).ready(function() {
            // $('#datatable').DataTable();
            simpleDataTable('#dt');

            @if (session('deleted'))
                swal.fire(
                    'Role Deleted',
                    '{{ session('deleted') }}',
                    'success'
                );
            @endif

            // roles donut
            const loadDonut = function() {
                const rolesDonut = $('#roles_donut');
                axios.get(rolesDonut.data('url'))
                    .then(function({data}) {
                        // transform data
                        const chartData = [];
                        data.map(function(d) {
                            let mapData = {label:d.role, value:d.staff};
                            chartData.push(mapData)
                        });
                        
                        const morrisChart = Morris.Donut({
                            element: 'roles_donut',
                            data: chartData,
                        });
                    })
                    .catch(function(e) {
                        console.log(e)
                    });
            }

            loadDonut();

        })
    </script>
@endpush
