@extends('layouts.admin.app')
@section('title', 'All Staff')

@section('page', 'All Staff')

@push('css')
    <style>
    </style>
@endpush

@section('crumbs')
    <li class="breadcrumb-item active">Staff</li>
@endsection

@section('content')
    @if (count($users))
        <div class="text-md-right my-2">
            <a class="btn btn-primary waves-effect" href="{{ route('create.staff') }}">
                <span class="d-none d-md-inline-block">New Staff</span>
                <i class="mdi mdi-plus-circle-outline mr-1"></i>
            </a>
        </div>
        <div class="row">
            @foreach ($users as $user)
            <div class="col-xl-4 col-lg-6">
                <div>
                    @include('components.entity.staff')
                </div>
            </div>
            @endforeach
        </div>
        <div class="my-4">
            {{ $users->links() }}
        </div>
    @else
        @component('components.empty')
            @slot('text')
                No staff found
            @endslot
            @slot('body')
                <a class="btn btn-primary btn-lg" href=" {{ route('create.staff') }} ">
                    Add New Staff
                </a>
            @endslot
        @endcomponent
    @endif
@endsection

@push('js')
    <script>
        $(document).ready(function() {
            @if (session('deleted_staff'))
                Swal.fire(
                    'Staff Deleted',
                    '{{ session('deleted_staff') }}',
                    'success'
                );
            @endif
        });
    </script>
@endpush