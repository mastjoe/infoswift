@extends('layouts.admin.app')
@section('title', 'All Staff Privileges | '.$privilege->privilege)

@section('page', 'All Privileges | '.$privilege->privilege)

@push('css')
@endpush

@section('crumbs')
<li class="breadcrumb-item">
    <a href="{{ route('all.staff') }}">Staff</a>
</li>
<li class="breadcrumb-item">
    <a href="{{ route('show.privilege', $privilege->id) }}">Privileges</a>
</li>
<li class="breadcrumb-item active">
    {{ $privilege->privilege }}
</li>
@endsection

@section('content')
<div class="row">
    <div class="mb-2 text-right col-12">
        <button class="btn btn-warning waves-effect update_privilege_btn">
            Edit
        </button>
    </div>
    <div class="col-md-3">
        @component('components.card')
            @slot('card_body')
                <h4>{{ $privilege->privilege }}</h4>
                <hr>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <h6 class="text-primary">Created</h6>
                        <h5>{{ $privilege->created_at->format('jS M, Y') }}</h5>
                    </li>
                    <li class="list-group-item">
                        <h6 class="text-primary">Last Updated</h6>
                        <h5>{{ $privilege->updated_at->format('jS M, Y')  }}</h5>
                    </li>
                </ul>
            @endslot
        @endcomponent
    </div>

    <div class="col-md-9">
        {{-- description of privileges --}}
        @component('components.card')
            @slot('card_body')
                <h4>Description</h4>
                <hr>
                @if ($privilege->description)
                    {{ $privilege->description }}
                @else
                    <div class="text-center my-4">
                        <h5 class="text-muted">No description available</h5>
                        <button class="btn btn-primary btn-sm waves-effect update_privilege_btn">
                            update <i class="fa fa-pencil-alt"></i>
                        </button>
                    </div>
                @endif
            @endslot
        @endcomponent
        {{-- staff with privileges --}}
        @component('components.card')
            @slot('card_body')
                <h4>Staff with <span class="text-primary">{{ $privilege->privilege }}</span> Privilege</h4>
                <hr>
                @if ($privilege->users->isNotEmpty())
                    <div>
                        <table class="table table-hover table-bordered dt">
                            <thead>
                                <tr>
                                    <th>Reference</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($privilege->users as $user)
                                    <tr>
                                        <td>{{ $user->ref }}</td>
                                        <td>{{ $user->full_name() }}</td>
                                        <td>{{ $user->email }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <div class="text-center my-5">
                        <h4 class="text-muted">No user has been assigned this privilege</h4>
                    </div>
                @endif
                
            @endslot
        @endcomponent
    </div>
</div>

@include('admin.staff.privileges.edit')
@endsection

@push('js')
    <script>
        $(document).ready(function() {
            simpleDataTable('.dt');

            const updatePrivilegeModal = $('#update_privilege_modal');
            const updatePrivilegeBtn = $('.update_privilege_btn');

            updatePrivilegeBtn.on('click', function() {
                updatePrivilegeModal.modal({backdrop: 'static'});
            });

            @if (session('privilege_updated'))
                swal.fire(
                    'Privilege Updated',
                    '{{ session('privilege_updated') }}',
                    'success'
                )
            @endif

            updatePrivilegeModal.closest('form').on('submit', function(e) {
                e.preventDefault();
                const thisForm = $(this);
                resolveForm({
                    form: thisForm,
                    method: 'put',
                    btnLoadingText: 'Saving...'
                })
                    .then(function(data) {
                        if (data.status == "success") {
                            refreshPage(3500);
                        }
                    })
                    .catch(function(e) {
                        console.log(e)
                    });
            })
        });
    </script>
@endpush