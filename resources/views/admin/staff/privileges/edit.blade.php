<form method="POST" action="">
    @csrf
    @method('PUT')
    @component('components.modal', [
        'modal_id' => 'update_privilege_modal',
        'modal_top_dismiss' => true,
        'modal_dialog_class' => 'animated fadeInUp modal-dialog-centered'
    ])
        @slot('modal_header')
            <h4 class="modal-title">
               <i class="mdi mdi-circle-edit-outline  mr-2 text-primary"></i>
                Update Privilege
            </h4>
        @endslot
        @slot('modal_body')
            {{-- privilege   --}}
            @component('components.form.input-form-group', [
                'type' => 'text',
                'label' => 'Privilege',
                'name' => 'privilege',
                'required' => true,
                'value' => $privilege->privilege,
                'readonly' => true,
            ])
            @endcomponent
            {{-- description --}}
            @component('components.form.textarea-form-group', [
                'label' => 'Description',
                'name' => 'description',
                'value' => $privilege->description
            ])                
            @endcomponent

        @endslot
        @slot('modal_footer')
            <button class="btn btn-default" type="button" data-dismiss="modal">
                Dismiss
            </button>

            <button class="btn btn-primary" type="submit">
                Save
            </button>
        @endslot
    @endcomponent
</form>