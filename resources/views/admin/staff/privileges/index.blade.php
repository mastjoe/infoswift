@extends('layouts.admin.app')
@section('title', 'All Staff Privileges')

@section('page', 'All Privileges')

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('all.staff') }}">Staff</a>
    </li>
    <li class="breadcrumb-item active">
        Privileges
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            @if ($privileges->count())
                @component('components.card')
                    @slot('card_body')
                        <h4 class="header-title mt-0">
                            All Staff Privileges
                        </h4>
                        <div class="table-responsive-sm">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Privilege</th>
                                        <th>Role</th>
                                        <th>Staff</th>
                                        <th>Details</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($privileges as $privilege)
                                        <tr>
                                            <td>{{ $privilege->privilege }}</td>
                                            <td>
                                                <span class="badge badge-secondary">
                                                    {{ number_format($privilege->roles->count()) }}
                                                </span>
                                            </td>
                                            <td>
                                                <span class="badge badge-warning">
                                                    {{ number_format($privilege->users->count()) }}
                                                </span>
                                            </td>
                                            <td>
                                                <a class="btn btn-primary btn-sm waves-effect"
                                                    href="{{ route('show.privilege', $privilege->id) }}"
                                                >
                                                    view
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endslot
                @endcomponent
            @else
                @component('components.empty', [
                    'text' => 'No staff privilege was found'
                ])
                    @slot('body')
                    @endslot
                @endcomponent
            @endif
        </div>
    </div>
@endsection

@push('js')
@endpush