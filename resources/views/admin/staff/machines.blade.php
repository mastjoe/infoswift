@extends('layouts.admin.app')
@section('title', 'Staff | '.$user->full_name(). ' | Terminals')

@section('page', 'Staff | '.$user->full_name(). ' | Terminals')

@push('css')
    <style>
         .table.table-borderless tr td:nth-child(1) {
            color: #f16c69;
            font-weight: bold;
            width: 20%;
        }
    </style>
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('all.staff') }}">Staff</a>
    </li>
    <li class="breadcrumb-items">
        <a href="{{ route('show.staff', $user->id) }}">{{ $user->first_name }}</a>
    </li>
    <li class="breadcrumb-item active">
        Machines
    </li>
@endsection

@section('content')
    @include('admin.staff.jumbotron')
    <div class="row">
        <div class="col-md-9 order-md-1 order-12">
           @if ($machines->count())
              @component('components.card')
                  @slot('card_header')
                      <b>Machines Accessible to Staff</b>
                  @endslot
                  @slot('card_body')
                      <div class="table-responsive-sm">
                          <table class="table table-hover machine_table">
                              <thead>
                                  <tr>
                                      <th>Terminal ID</th>
                                      <th>Client</th>
                                      <th>Branch</th>
                                      <th>Region</th>
                                      <th>State</th>
                                      <th>Engineer</th>
                                      <th>Tickets</th>
                                      <th>PMs</th>
                                  </tr>
                              </thead>
                              <tbody>
                                    @foreach ($machines as $machine)
                                        <tr>
                                            <td>
                                                <a class="font-weight-bold" href="{{ route('show.machine', $machine->id) }}">
                                                    {{ $machine->terminal_id }}
                                                </a>
                                            </td>
                                            <td>
                                                <span class="font-weight-bold" style="color:{{ $machine->client->color }}">
                                                    {{ $machine->client->short_name }}
                                                </span>
                                            </td>
                                            <td>
                                                {{ $machine->branch->branch }}
                                            </td>
                                            <td>
                                                {{ $machine->region->region }}
                                            </td>
                                            <td>
                                                <span title="{{ $machine->state->state.', '.$machine->country->country }}" data-toggle="tooltip">
                                                    {{ $machine->state->state }}
                                                </span>
                                            </td>
                                            <td>
                                                @if ($machine->engineer)
                                                <span title="{{ $machine->engineer->full_name() }}" data-toggle="tooltip">
                                                    {{ $machine->engineer->first_name }}
                                                </span>
                                                @else
                                                -
                                                @endif
                                            </td>
                                            <td>
                                                {{ number_format($machine->tickets->count()) }}
                                            </td>
                                            <td>
                                                {{ number_format($machine->pms->count()) }}
                                            </td>
                                        </tr>
                                    @endforeach
                              </tbody>
                          </table>
                      </div>
                  @endslot
              @endcomponent 
           @else
               <div class="text-center my-5">
                   <h2 class="text-muted">Seems no machine is assigned to staff</h2>
               </div>
           @endif
        </div>
        <div class="col-md-3 order-md-12 order-1">
            @include('admin.staff.nav')
        </div>
    </div>

    {{--  modal  --}}
    @include('admin.staff.update-photo-modal')
@endsection

@push('js')
    <script src="{{ asset('js/util.image.js') }}"></script>
    <script src="{{ asset('js/admin/staff.js') }}"></script>
    <script>
        $(document).ready(function() {
            simpleDataTable('.machine_table');
        });
    </script>
@endpush