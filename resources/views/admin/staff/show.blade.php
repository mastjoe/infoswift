@extends('layouts.admin.app')
@section('title', 'Staff | '.$user->full_name())

@section('page', 'Staff | '.$user->full_name())

@push('css')
    <style>
         .table.table-borderless tr td:nth-child(1) {
            color: #f16c69;
            font-weight: bold;
            width: 20%;
        }
    </style>
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('all.staff') }}">Staff</a>
    </li>
    <li class="breadcrumb-item active">
        {{ $user->first_name }}
    </li>
@endsection

@section('content')
    @include('admin.staff.jumbotron')
    <div class="row">
        <div class="col-md-9 order-md-1 order-12">
            @if ($user->suspended_at)
                <div class="alert alert-primary">
                    <button class="close" type="button" data-dismiss="alert"><span>&times;</span></button>
                    Staff is currently suspeneded!
                </div>
            @endif
            {{--  bio  --}}
            @component('components.card')
                @slot('card_header')
                    <h4 class="card-title">
                        Bio
                    </h4>
                @endslot
                @slot('card_body')
                    @isset($user->bio)
                        {{ $user->bio }}
                    @else
                        <div class="text-center my-4">
                            <h4>Bio not available</h4>
                        </div>
                    @endisset
                @endslot
            @endcomponent

            {{--  profile info  --}}
            @component('components.card')
                @slot('card_header')
                    <h4 class="card-title">
                        Profile Info
                    </h4>
                @endslot
                @slot('card_body')
                    <table class="table table-borderless table-sm">
                        <tr>
                            <td>
                               First Name
                            </td>
                            <td>
                                {{ $user->first_name }}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Middle Name
                            </td>
                            <td>
                                @isset($user->middle_name)
                                    {{ $user->middle_name }}
                                @else
                                    <small class="text-muted">Not available</small>
                                @endisset
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Last Name
                            </td>
                            <td>
                                {{ $user->last_name }}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Gender
                            </td>
                            <td>
                                {{ $user->gender }}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Date of Birth
                            </td>
                            <td>
                                @isset($user->dob)
                                    {{ $user->dob->format('jS M, Y') }}
                                @else
                                    <small class="text-muted">Not available</small>
                                @endisset
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Telephone
                            </td>
                            <td>
                                {{ $user->phone }}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Email
                            </td>
                            <td>
                                {{ $user->email }}
                            </td>
                        </tr>
                    </table>
                @endslot
            @endcomponent

        </div>
        <div class="col-md-3 order-md-12 order-1">
            @include('admin.staff.nav')
        </div>
    </div>

    {{--  modal  --}}
    @include('admin.staff.update-photo-modal')
@endsection

@push('js')
    <script src="{{ asset('js/util.image.js') }}"></script>
    <script src="{{ asset('js/admin/staff.js') }}"></script>
    <script>
        $(document).ready(function() {
            injectAvatarInForm('._image_inject');
            @if (session('updated_staff'))
                Swal.fire(
                    'Staff Updated!',
                    '{{ session('updated_staff') }}',
                    'success'
                );
            @endif

            @if (session('new_staff'))
               Swal.fire(
                    'Staff Record Created!',
                    '{{ session('new_staff') }}',
                    'success'
                ); 
            @endif

            @if (session('suspended'))
               Swal.fire(
                    'Staff Suspended!',
                    '{{ session('suspended') }}',
                    'success'
                ); 
            @endif

            @if (session('unsuspended'))
               Swal.fire(
                    'Staff Unsuspended!',
                    '{{ session('unsuspended') }}',
                    'success'
                ); 
            @endif

            @if(session('password_resetted'))
                Swal.fire(
                    'Password Resetted',
                    '{{ session('password_resetted') }}',
                    'success'
                );
            @endif
        });
    </script>
@endpush