<a class="btn btn-block btn-primary my-2" href="{{ route('show.staff', $user->id) }}">
    Staff Details
</a>
<a class="btn btn-block btn-secondary my-2" href="{{ route('show.staff.guarantors', $user->id) }}">
    Guarantors
</a>

@if ($user->deletable())
    @can('delete', $user)        
    <button class="btn btn-block btn-primary my-2 delete_staff_btn"
        data-url="{{ route('delete.staff', $user->id) }}"
    >
        Delete
    </button>
    @endcan
@endif

<button class="btn btn-block btn-secondary my-2 reset_password_btn"
    data-url="{{ route('reset.staff.password', $user->id) }}"
>
    Reset Password
</button>

@if ($user->suspended_at)
    <button class="btn btn-success btn-block my-2 unsuspend_staff_btn" data-url="{{ route('unsuspend.staff', $user->id) }}">
        Unsuspend
    </button>
@else
    <button class="btn btn-secondary btn-block my-2 suspend_staff_btn" data-url="{{ route('suspend.staff', $user->id) }}">
        Suspend
    </button>
@endif

<a class="btn btn-secondary btn-block my-2"
    href="{{ route('show.staff.roles', $user->id) }}"
>
    Roles and Privileges
</a>
@if ($user->hasRole('engineer'))
    <a class="btn btn-secondary btn-block my-2" href="{{ route('show.staff.machines', $user->id) }}">
        Machines
    </a>
    <a class="btn btn-secondary btn-block my-2" href="">
        Ticket Closure History
    </a>
@endif
