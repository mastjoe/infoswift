<div class="row" style="margin-top:-30px">
    <div class="col-sm-12">
        <div class="page-title-box"
            style="background-image: url('{{ asset('images/photo18@2x.jpg') }}'); background-position: 0 50%; background-size: cover;padding:20px 30px;">
            <div class="text-center pt-3">
                <img class="rounded-circle thumb-lg img-thumbnail" src="{{ $user->avatar() }}"
                    alt="Generic placeholder image">
                <h1>
                    {{ $user->first_name }} {{ $user->middle_name }} {{ $user->last_name  }}
                </h1>
                <div class="my-2">
                    <div class="social-icons">
                        <ul class="social-links list-inline mb-0 p-2">
                            <li class="list-inline-item"><a title="" data-placement="top" class="btn-danger tooltips"
                                    data-toggle="tooltip" href="" data-original-title="Facebook"><i
                                        class="fab fa-facebook-f"></i></a>
                            </li>
                            <li class="list-inline-item"><a title="" data-placement="top" class="btn-info tooltips"
                                    data-toggle="tooltip" href="" data-original-title="Twitter"><i
                                        class="fab fa-twitter"></i></a>
                            </li>
                            <li class="list-inline-item"><a title="" data-placement="top" class="btn-primary tooltips"
                                    data-toggle="tooltip" href="" data-original-title="1234567890"><i
                                        class="fa fa-phone"></i></a>
                            </li>
                            <li class="list-inline-item"><a title="" data-placement="top" class="btn-info tooltips"
                                    data-toggle="tooltip" href="" data-original-title="@skypename"><i
                                        class="fab fa-skype"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="my-1">
                    @if (count($user->roles_array()))
                        @foreach ($user->roles_array() as $role)
                            <span class="badge-light badge-pill">{{ $role }}</span>
                        @endforeach
                    @endif
                </div>
                <a class="btn btn-sm btn-outline-warning waves-effect" href="{{ route('edit.staff', [$user->id]) }}">
                    <span class="d-none d-md-inline-block">Edit </span>
                    <i class="mdi mdi-square-edit-outline mr-1"></i>
                </a>
                @if ($user->messageable())                    
                    <button class="btn btn-sm btn-outline-light waves-effect message_staff_btn"
                        data-url="{{ route('admin.send.staff.private.message.form', $user->id) }}"
                    >
                        <span class="d-none d-md-inline-block">P. Message </span>
                        <i class="mdi mdi-message-reply-text mr-1"></i>
                    </button>
                @endif
                <button class="btn btn-sm btn-outline-primary waves-effect update_photo_btn">
                    <span class="d-none d-md-inline-block">Avatar</span>
                    <i class="mdi mdi-camera"></i>
                </button>
            </div>
        </div>
    </div>
</div>