@extends('layouts.admin.app')
@section('title', 'Staff | '.$user->full_name().' | Guarantors')

@section('page', 'Staff | '.$user->first_name.' | Guarantors')

@push('css')
@endpush

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('all.staff') }}">Staff</a>
    </li>
    <li class="breadcrumb-item active">
        <a href="{{ route('show.staff', $user->id) }}">{{ $user->first_name }}</a>
    </li>
    <li class="breadcrumb-item active">
        Guarantors
    </li>
@endsection

@section('content')
    @if (count($user->guarantors))
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="my-2 text-right">
                    <button class="btn btn-primary waves-effect add_guarantor_btn">
                        <i class="mdi mdi-account-plus mr-2"></i>
                        <span class="d-none d-md-inline-block">New Guarantor</span>
                    </button>
                </div>
                @foreach ($user->guarantors as $g)
                    @component('components.card')
                        @slot('card_body')
                            <div class="row media">
                                <div class="col-md-2">
                                    <img class="d-flex mr-3 rounded shadow" alt="200x200" src="{{ $g->avatar() }}" width="100" data-holder-rendered="true">
                                </div>
                                <div class="col-md-10">
                                    <div class="media-body">
                                        <h4 class="media-heading font-18">
                                            {{ $g->name }}
                                        </h4>
                                        <p>
                                            <span class="mr-3">
                                                <span class="text-primary mr-1">
                                                    <i class="fa fa-phone"></i>
                                                    Telephone:
                                                </span>
                                                <a href="tel:{{ $g->phone }}">{{ $g->phone }}</a>
                                            </span>
                                            <span class="mr-3">
                                                <span class="text-primary mr-1">
                                                    <i class="fa fa-envelope text-primary"></i>
                                                    Email:
                                                </span>
                                                @isset($g->email)
                                                    <a href="mailto:{{ $g->email }}">
                                                        {{ $g->email }}
                                                    </a>
                                                @else
                                                @endisset
                                            </span>
                                        </p>
                                        <p>
                                            <span class="mr-3">
                                                <span class="text-primary mr-1">
                                                    <i class="fa fa-user-circle"></i>
                                                    Relationship:
                                                </span>
                                                @isset($g->relationship)
                                                {{ $g->relationship }}
                                                @else
                                                    <small class="text-muted">not specified</small>
                                                @endisset
                                            </span>
                                            <span class="mr-3">
                                                <span class="text-primary mr-1">
                                                    <i class="fa fa-briefcase"></i>
                                                    Occupation:
                                                </span>
                                                @isset($g->occupation)
                                                    {{ $g->occupation }}
                                                @else
                                                    <small class="text-muted">not specified</small>
                                                @endisset
                                            </span>
                                        </p>
                                        <p>
                                            <span class="text-primary mr-3">
                                                <i class="fa fa-map-marker-alt"></i>
                                                Address:
                                            </span>
                                            @isset($g->address)
                                                {{ $g->address }}
                                            @else
                                                <small class="text-muted">not specified</small>
                                            @endisset
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="text-right mt-3">
                                <span class="btn btn-outline-warning btn-sm edit_guarantor_btn" title="Edit"
                                    data-url="{{ route('edit.staff.guarantor', $g->id) }}"
                                >
                                    <i class="mdi mdi-account-edit"></i>
                                </span>
                                <span class="btn btn-outline-primary btn-sm delete_guarantor_btn" title="Delete"
                                    data-name="{{ $g->name }}"
                                    data-url="{{ route('delete.staff.guarantor', $g->id) }}"
                                >
                                    <i class="mdi mdi-delete-circle"></i>
                                </span>
                            </div>
                        @endslot
                    @endcomponent
                @endforeach
            </div>
        </div>
    @else
        @component('components.empty')
            @slot('text')
                Staff has no guarantor yet!
            @endslot
            @slot('body')
                <button class="btn btn-primary btn-lg waves-effect add_guarantor_btn">
                    Add A Guarantor
                </button>
            @endslot
        @endcomponent
    @endif

    {{--  modals  --}}
    @include('admin.staff.guarantors-form-modal')
@endsection

@push('js')
    <script src="{{ asset('js/util.image.js') }}"></script>
    <script src="{{ asset('js/admin/staff.guarantors.js') }}"></script>
    <script src="{{ asset('js/admin/staff.js') }}"></script>
    <script>
        $(document).ready(function() {
            @if (session('deleted_guarantor'))
                swal.fire(
                    'Guarantor Deleted!',
                    '{{ session('deleted_guarantor') }}',
                    'success'
                );
            @endif
        });
    </script>
@endpush