@php
    use App\Helpers\UI;
@endphp

@extends('layouts.admin.app')
@section('title', 'All Tickets')

@section('page', 'All Tickets')

@push('css')
    <style>
    </style>
@endpush

@section('crumbs')
    <li class="breadcrumb-item active">Tickets</li>
@endsection

@section('content')
    @if ($tickets_count)
        {{-- filter section --}}
        <form method="GET" action="{{ route('all.tickets') }}">
            <div class="row mb-3">
                <div class="col-lg-4 col-8">
                    <select class="form-control bg-transparent" name="filter">
                        <option value="">All</option>
                        <option value="open" {{ $filter == "open" ? "selected" : null }}>Open</option>
                        <option value="closed" {{ $filter == "closed" ? "selected" : null }}>Closed</option>
                        <option value="resolved" {{ $filter == "resolved" ? "selected" : null }}>Resolved</option>
                        <option value="declined" {{ $filter == "declined" ? "selected" : null }}>Declined</option>
                        <option value="escalated" {{ $filter == "escalated" ? "selected" : null }}>Escalated</option>
                        <option value="part_ordered" {{ $filter == "part_ordered" ? "selected" : null }}>Part Ordered</option>
                    </select>
                </div>
                <div class="col-lg-4 col-4">
                    <button  type="submit" class="btn btn-primary waves-effect">
                        <i class="ion-android-mixer"></i>
                        Filter
                    </button>
                </div>
            </div>
        </form>

        @if ($filter)
            
        <div class="row">
            <div class="col-12">
                <div class="alert alert-dark alert-dismissable">
                    <button class="close" type="button"><span>&times;</span></button>
                    @if ($tickets->count() > 1)
                        There are {{ $tickets->count() }} tickets
                    @elseif ($tickets->count() == 1)
                        There is a ticket
                    @else
                        No ticket found with status
                    @endif

                </div>
            </div>
        </div>
        @endif

        <div class="row">
            <div class="col-12">
                @component('components.card')
                    @slot('card_header')
                        <h4 class="card-title">

                        </h4>
                    @endslot
                    @slot('card_body')
                        <div class="">
                            <table class="table table-bordered tickets_table">
                                <thead>
                                    <tr>
                                        <th>Reference</th>
                                        <th>Machine</th>
                                        <th>client</th>
                                        <th>Branch</th>
                                        <th>Status</th>
                                        <th>Raised</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($tickets as $ticket)
                                        <tr>
                                            <td>
                                                <a href="{{ route('show.ticket', $ticket->id) }}">
                                                    {{ $ticket->ref }}
                                                </a>
                                            </td>
                                            <td>
                                                {{ $ticket->machine->terminal_id }}
                                            </td>
                                            <td>
                                            <span style="color:{{ $ticket->client->color }};font-weight:bold">{{ $ticket->client->short_name }}</span>
                                            </td>
                                            <td>
                                                {{ $ticket->machine->branch->branch }}
                                            </td>
                                            <td>
                                                {!! UI::ticketStatusBadge($ticket->status) !!}
                                            </td>
                                            <td>
                                                {{ $ticket->created_at->format('jS M, Y') }}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endslot
                @endcomponent
            </div>
        </div>
    @else
        @component('components.empty')
            @slot('text')
                No tickets found
            @endslot
            @slot('body')
            @endslot
        @endcomponent
    @endif
@endsection

@push('js')
    <script>
        $(document).ready(function() {
            simpleDataTable('.tickets_table', {ordering: false});

        });
    </script>
@endpush