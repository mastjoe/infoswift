 @component('components.card')
    @slot('card_header')
        <h5>Job Completion Form</h5>
        <div class="card-options">
            @if ($ticket->can_upload_frf())
                <button class="btn-block-option upload_jcf_btn" type="button">
                    <i class="mdi mdi-upload mdi-24px"></i>
                </button>
            @endif
        </div>
    @endslot
    @slot('card_body')
        @if ($ticket->job_completion_form)
            <div class="alert alert-light">
                Form uploaded
                by <b>{{ $ticket->job_completion_form->uploader->full_name() }}</b>
                on <b>{{ $ticket->job_completion_form->created_at->format('jS M, Y') }}</b>
                at <b>{{ $ticket->job_completion_form->created_at->format('h:i a') }}</b>
            </div>
            <div class="my-2">
                <a href="{{ $ticket->job_completion_form->link() }}" class="btn btn-sm btn-primary waves-effect mr-2">
                    View Form
                </a>
                <button class="btn btn-sm btn-dark upload_jcf_btn waves-effect">
                    Update
                </button>
            </div>
        @else
            @if ($ticket->can_upload_jcf())
                <div>
                    <h6>Job completion form not available</h6>
                    <button class="btn btn-primary waves-effect upload_jcf_btn">
                        Upload Form
                    </button>
                </div>
            @else
                <div class="my-2">
                    <h4 class="text-muted">No job completion form uploaded</h4>
                </div>
            @endif
        @endif
    @endslot
@endcomponent