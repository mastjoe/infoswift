@php
    use App\Helpers\UI;
@endphp
@extends('layouts.admin.app')
@section('title', 'Tickets | '.$ticket->ref.' | Close')

@section('page', 'Tickets | '.$ticket->ref.' | Close')

@push('css')
    <style>
        .solution_check_btn {
            cursor: pointer;
        }

        .solution-item label {
            display: block;
            width: 100%;
            padding: 0.2rem;
            border-radius: 2px;
            cursor: pointer;
        }
        .solution-item label:hover {
            background: #ffd5d5;
        }
        .solution-item input[type=radio] {
            opacity: 0;
            padding: 0;
        }
        .solution-item input[type=radio]:checked + label{
            background:#ffbfbf;
            color: #222;
        }
    </style>
@endpush

@section('crumbs')
    <li class="breadcrumb-item active">
        <a href="{{ route('all.tickets') }}">Tickets</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('show.ticket', $ticket->id) }}">{{ $ticket->ref }}</a>
    </li>
    <li class="breadcrumb-item active">
        Close
    </li>
@endsection

@section('content')
    @include('admin.tickets.jumbotron')
    <div class="row mt-0">
        <div class="col-md-9 order-md-1 order-2">
        @if ($errors->any())
            <div class="alert alert-primary alert-dismissable">
                {{ $errors->first() }}
                <button class="close" data-dismiss="alert" type="button">&times;</button>
            </div>
        @endif
        @if (!$ticket->closed_at)                  
            <form method="POST" class="closure_form" action="{{ route('close.ticket', $ticket->id) }}">
                @csrf
                    @component('components.card')
                        @slot('card_header')
                            <b>Ticket Closure</b>
                        @endslot
                        @slot('card_body')
                                @if ($ticket->faults->count())
                                    <h4 class="text-muted mb-4">Kindly let us know how you solved terminal fault(s)</h4>
                                    <div class="alert alert-secondary">
                                        You can check and pick from previous solutions provided for specific terminal fault by clicking on the light bulb icons!
                                    </div>
                                    
                                    @foreach ($ticket->faults as $fault)
                                        <div class="form-group">
                                            <label>
                                                <span title="check"
                                                    data-toggle="tooltip"
                                                    data-url="{{ route('get.machine-fault.solutions', $fault->fault->id) }}"
                                                    class="fa fa-lightbulb solution_check_btn fa-2x text-warning mr-1">
                                                </span>
                                                {{ $fault->fault->fault }}
                                            </label>
                                            <input type="text" class="form-control"
                                                name="solutions[{{ $fault->fault_id }}]"
                                                @if (old('solutions'))
                                                    value="{{ old('solutions')[$fault->fault_id] }}"
                                                @endif
                                                placeholder="solution for {{ $fault->fault->fault }} here..."
                                            >
                                        </div>
                                    @endforeach
                                @endif
                                <div class="form-group">
                                    <label>Comment</label>
                                    <textarea class="form-control"
                                        placeholder="any comment worth noting about ticket closure"
                                        name="comment"
                                    >{{ old('comment') }}</textarea>
                                </div>
                                <hr>
                                <div class="text-center">
                                    <button class="btn btn-primary waves-effect" type="submit">
                                        Close Ticket
                                    </button>
                                </div>
                        @endslot
                    @endcomponent
            </form>
        @else
            {{-- ticket closure info --}}
            @component('components.card', ['card_body_class' => 'pt-0'])
                @slot('card_header')
                    <b>Ticket Closure Info</b>
                @endslot
                @slot('card_body')
                    <div>
                        <table class="table table-borderless table-sm">
                            <thead>
                                <th width="150"></th>
                                <th></th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><b class="text-primary">Closure By</b></td>
                                    <td>
                                        {{ $ticket->closer->full_name() }}
                                    </td>
                                </tr>
                                <tr>
                                    <td><b class="text-primary">Comment</b></td>
                                    <td>
                                        @if ($ticket->closure_comment)
                                            {{ $ticket->closure_comment }}
                                        @else
                                            <span class="text-muted">No comment available</span>
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                @endslot
            @endcomponent
            @if ($ticket->faults->count())
                @component('components.card')
                    @slot('card_header')
                        <b>Ticket Solutions</b>
                        <div class="card-options">
                            @if ($ticket->declined_at && $ticket->closed_at)                                        
                            <button class="btn-block-option update_close_btn">
                                <span class="mdi mdi-table-edit"></span>
                            </button>
                            @endif
                        </div>
                    @endslot
                    @slot('card_body')
                            <div id="accordion">
                                @foreach ($ticket->faults as $key => $fault)
                                    <div class="card">
                                        <div class="card-header" id="heading{{ $key }}">
                                            <h6 class="mb-0">
                                                <span class="cursor-pointer" data-toggle="collapse" data-target="#collapse{{ $key }}" aria-expanded="true"
                                                    aria-controls="collapse{{ $key }}">
                                                    <span class="fa fa-lightbulb text-warning mr-1"></span>
                                                    {{ $fault->fault->fault }}
                                                </span>
                                            </h6>
                                        </div>

                                        <div id="collapse{{ $key }}" class="collapse" aria-labelledby="heading{{ $key }}" data-parent="#accordion">
                                            <div class="card-body">
                                            {{ $fault->solution }}
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                    @endslot
                @endcomponent
            @endif

            {{--  ticket resolution inform  --}}
            @if ($ticket->confirmed_at || $ticket->declined_at)
                @component('components.card', ['card_body_class' => 'pt-0'])
                    @slot('card_header')
                        <b>Ticket Closure Evaluation</b>
                    @endslot
                    @slot('card_body')
                        <table class="table table-sm table-borderless">
                            <thead>
                                <th width="150"></th>
                                <th></th>
                            </thead>
                            <tbody>
                                @if ($ticket->confirmed_at)
                                    <tr>
                                        <td><b class="text-primary">Status</b></td>
                                        <td><span class="badge badge-success p-1">Approved</span></td>
                                    </tr>
                                    <tr>
                                        <td><b class="text-primary">Confirmed By</b></td>
                                        <td>{{ $ticket->confirmer->full_name() }}</td>
                                    </tr>
                                    <tr>
                                        <td><b class="text-primary">Confirmed At</b></td>
                                        <td>{{ $ticket->confirmed_at->format('jS M, Y | h:i a') }}</td>
                                    </tr>
                                    <tr>
                                        <td><b class="text-primary">Confirmation Remark</b></td>
                                        <td>
                                            @if ($ticket->confirmation_comment)
                                            {{ $ticket->confirmation_comment }}
                                            @else
                                            -
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b class="text-primary">Rating</b></td>
                                        <td>{!! UI::pmStars($ticket->rating) !!} ({{ $ticket->rating }})</td>
                                    </tr>
                                @else
                                    <tr>
                                        <td><b class="text-primary">Status</b></td>
                                        <td><span class="badge badge-primary p-1">Declined</span></td>
                                    </tr>
                                    <tr>
                                        <td><b class="text-primary">Declined By</b></td>
                                        <td>{{ $ticket->decliner->full_name() }}</td>
                                    </tr>
                                    <tr>
                                        <td><b class="text-primary">Declined At</b></td>
                                        <td>{{ $ticket->declined_at->format('jS M, Y | h:i a') }}</td>
                                    </tr>
                                    <tr>
                                        <td><b class="text-primary">Declination Remark</b></td>
                                        <td>
                                            @if ($ticket->declination_comment)
                                            {{ $ticket->declination_comment }}
                                            @else
                                            -
                                            @endif
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    @endslot
                @endcomponent
            @endif
        @endif

            {{-- confirm closure btn when first closed --}}
            @if ($ticket->closed_at)
                @can('assessClosure', $ticket)                    
                    <div class="my-3 text-center">
                        @if (!$ticket->confirmed_at && !$ticket->declined_at)                        
                        <button class="btn btn-primary btn-lg waves-effect ticket_declination_btn m-1">
                            Decline Ticket Closure
                        </button>
                        @endif

                        @if (!$ticket->confirmed_at)                        
                        <button class="btn btn-success btn-lg waves-effect ticket_confirmation_btn m-1">
                            Accept Ticket Closure
                        </button>
                        @endif

                        <button class="btn btn-dark btn-lg waves-effect breakdown_btn m-1">
                            See Analysis
                        </button>
                    </div>
                @endcan
            @endif
        </div>
        <div class="col-md-3 order-md-2 order-1">
            @include('admin.tickets.nav')
        </div>
    </div>

    {{--  log modal  --}}
    @includeWhen(!$ticket->confirmed_at , 'admin.tickets.create-log')
    @includeWhen($ticket->closed_at && !$ticket->confirmed_at, 'admin.tickets.confirm-ticket-closure')
    @includeWhen(!$ticket->confirmed_at && !$ticket->declined_at, 'admin.tickets.decline-ticket-closure')
    @includeWhen($ticket->closed_at, 'admin.tickets.closure-breakdown')
    @includeWhen($ticket->declined_at && !$ticket->confirmed_at, 'admin.tickets.update-fault')
@endsection

@push('js')
    <script src="{{ asset('js/admin/ticket.js') }}"></script>
    <script>
        $(document).ready(function() {
            @if(session('confirmed'))
                Swal.fire(
                    'Ticket Closure Confirmed',
                    '{{ session('confirmed') }}',
                    'success'
                );
            @endif

            @if(session('declined'))
                Swal.fire(
                    'Ticket Closure Declined',
                    '{{ session('declined') }}',
                    'success'
                );
            @endif

            @if(session('reclosed'))
                Swal.fire(
                    'Ticket Re-Closed',
                    '{{ session('reclosed') }}',
                    'success'
                );
            @endif

            @if(session('confirm_closure_form'))
               showConfirmationModal();
            @endif

            @if(session('decline_closure_form'))
               showDeclinationModal();
            @endif

            // popup ticket closure update form
            @if(session('reclose'))
                showUpdateCloseModal();
            @endif
        });
    </script>
    <script>
        const test = function(event) {
            const closureForm = $('.closure_form');

            if (event.target.checked) {
                const t = $(event.target);
                const solutionFaultId = t.data('fault-id');
                const solution = t.data('solution');
                const targetInput = closureForm.find('input[name="solutions['+solutionFaultId+']"]');
                targetInput.val(solution);
            }
        }
    </script>
@endpush