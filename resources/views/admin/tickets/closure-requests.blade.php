@php
    use App\Helpers\UI;
@endphp

@extends('layouts.admin.app')
@section('title', 'Tickets - Closure Request')

@section('page', 'Closed Tickets')

@push('css')
    <style>
    </style>
@endpush

@section('crumbs')
    <li class="breadcrumb-item"><a href="{{ route('all.tickets') }}">Tickets</a></li>
    <li class="breadcrumb-item active">Closure Requests</li>
@endsection

@section('content')
    @if ($closed_tickets->count())
        @component('components.card')
            @slot('card_header')
                <b> {{ number_format($closed_tickets->count()) }} Closed Ticket Requests</b>
            @endslot
            @slot('card_body')
                <div class="">
                    <table class="table table-bordered tickets_table">
                        <thead>
                            <tr>
                                <th>Reference</th>
                                <th>Machine</th>
                                <th>Client</th>
                                <th>Branch</th>
                                <th>CE</th>
                                <th>Closed</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($closed_tickets as $ticket)
                                <tr>
                                    <td>
                                        <a href="{{ route('show.ticket', $ticket->id) }}">{{ $ticket->ref }}</a>
                                    </td>
                                    <td>
                                        <a href="{{ route('show.machine', $ticket->machine_id) }}">{{ $ticket->machine->terminal_id }}</a>
                                    </td>
                                    <td>
                                        <span style="color:{{ $ticket->client->color }}">{{ $ticket->client->name }}</span>
                                    </td>
                                    <td>
                                        {{ $ticket->machine->branch->branch }}
                                    </td>
                                    <td>
                                        {{ $ticket->closer->full_name() }}
                                    </td>
                                    <td>
                                        {{ $ticket->closed_at->diffForHumans() }}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @endslot
        @endcomponent
    @else
        <div class="text-center my-5">
            <h4 class="text-muted">
                No closed ticket found!
            </h4>
        </div>
    @endif
@endsection

@push('js')
    <script>
        $(document).ready(function() {
            simpleDataTable('.tickets_table', {ordering: false});

        });
    </script>
@endpush