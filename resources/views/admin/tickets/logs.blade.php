@extends('layouts.admin.app')
@section('title', 'Tickets | '.$ticket->ref.' | Logs')

@section('page', 'Tickets | '.$ticket->ref.' | Logs')

@push('css')
    <style>
    </style>
@endpush

@section('crumbs')
    <li class="breadcrumb-item active">
        <a href="{{ route('all.tickets') }}">Tickets</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('show.ticket', $ticket->id) }}">{{ $ticket->ref }}</a>
    </li>
    <li class="breadcrumb-item active">
        Logs
    </li>
@endsection

@section('content')
    @include('admin.tickets.jumbotron')
    <div class="row mt-0">
        <div class="col-md-9 order-md-1 order-2">
            @if ($ticket->logs->count())
                @foreach ($ticket->logs as $log)
                    @component('components.card')
                        @slot('card_body')
                            <a href="javascript:;" class="media">
                                <img class="d-flex mr-3 rounded-circle" 
                                    src="{{ $log->author_avatar() }}"
                                    @if ($log->user_id)
                                        alt="{{ $log->user->full_name() }}"
                                    @else
                                        alt="{{ $log->client->name }}"
                                    @endif
                                    height="36"
                                >
                                <div class="media-body chat-user-box">
                                    <p class="user-title m-0">
                                        @if ($log->user_id)
                                            @if (Auth::user()->id == $log->user_id)
                                                You
                                            @else
                                                {{ $log->user->full_name() }}
                                            @endif
                                        @endif
                                    </p>
                                    {!! $log->message !!}
                                </div>
                            </a>
                        @endslot
                    @endcomponent
                @endforeach
                <div class="text-center my-2">
                    <button class="btn btn-primary waves-effect add_log_btn">
                        Add to Log
                    </button>
                </div>
            @else
                <div class="text-center my-5">
                    <h2>No Logs found on ticket</h2>
                    <button class="btn btn-primary waves-effect add_log_btn">
                        Send Message log to Client
                    </button>
                </div>
            @endif
        </div>
        <div class="col-md-3 order-md-2 order-1">
            @include('admin.tickets.nav')
        </div>
    </div>

    {{--  log modal  --}}
    @includeWhen(!$ticket->confirm_closed_at , 'admin.tickets.create-log')
@endsection

@push('js')
    <script src="{{ asset('js/admin/ticket.js') }}"></script>
@endpush