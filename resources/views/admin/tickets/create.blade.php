@extends('layouts.admin.app')
@section('title', 'New Ticket')

@section('page', 'Tickets | New Ticket')

@push('css')
    <style>
    </style>
@endpush

@section('crumbs')
    <li class="breadcrumb-item "><a href="{{ route('all.tickets') }}">Tickets</a></li>
    <li class="breadcrumb-item active">New</li>
@endsection

@section('content')
   <div class="row">
       <div class="col-12">
            @if ($errors->any())
            <div class="alert alert-primary">
                <button class="close" type="button"><span>&times;</span></button>
                {{ $errors->first() }}
            </div>
            @else
            <div class="alert alert-dark alert-dismissable">
                 <button class="close" type="button"><span>&times;</span></button>
                 Raise a new ticket for a client!
             </div>
            @endif
           @component('components.card')
               @slot('card_body')
                   <h4 class="card-title">New Ticket</h4>
                   <hr>
                   <div>
                       <form method="post" action="{{ route('store.ticket') }}">
                           @csrf
                            {{--  client field  --}}
                            @component('components.form.select-form-group', [
                                'label' => 'Client',
                                'name' => 'client',
                                'label_class' => 'col-md-3 col-form-lable text-md-right',
                                'input_wrap_class' => 'col-md-9',
                                'form_group_class' => 'row',
                                'required' => true,
                                'id' => 'client'
                            ])
                                @slot('options')
                                    <option value="">Choose a client</option>
                                    @foreach ($clients as $client)
                                        <option value="{{ $client->id }}"
                                            {{ old('client') == $client->id ? "selected": null }}
                                            @isset($client_id)
                                                {{ $client_id == $client->id ? "selected" : null }}
                                            @endisset    
                                        >
                                            {{ $client->name }}
                                        </option>
                                    @endforeach
                                @endslot
                            @endcomponent

                            <div class="row d-none" id="loader">
                                <div class="offset-md-3 col-md-9 col-12 my-3">
                                    <div class="text-center" >
                                        <span class="fa fa-spinner fa-spin"></span>
                                    </div>
                                </div>
                            </div>

                            <div id="load_sheet" class="d-none" data-url="{{ route('json.client.machines') }}">
                                {{-- machine field --}}
                                @component('components.form.select-form-group', [
                                    'name' => 'machine',
                                    'label' => 'Machine',
                                    'id' => 'machine',
                                    'label_class' => 'col-md-3 col-form-label text-md-right',
                                    'input_wrap_class' => 'col-md-9',
                                    'form_group_class' => 'row',
                                    'required' => true,
                                ])
                                    @slot('options')
                                        <option></option>
                                    @endslot
                                @endcomponent

                                <div id="branch_sheet" class="d-none">
                                    {{-- branch field --}}
                                    @component('components.form.input-form-group', [
                                        'name' => 'branch',
                                        'label' => 'Branch',
                                        'id' => 'branch',
                                        'label_class' => 'col-md-3 col-form-label text-md-right',
                                        'input_wrap_class' => 'col-md-9',
                                        'form_group_class' => 'row',
                                        'disabled' => true,
                                    ])
                                    @endcomponent
                                    {{-- region field --}}
                                    @component('components.form.input-form-group', [
                                        'name' => 'region',
                                        'label' => 'Region',
                                        'id' => 'region',
                                        'label_class' => 'col-md-3 col-form-label text-md-right',
                                        'input_wrap_class' => 'col-md-9',
                                        'form_group_class' => 'row',
                                        'disabled' => true,
                                    ])
                                    @endcomponent
                                </div>
                            </div>

                            {{--  machine faults  --}}
                             @component('components.form.select-form-group', [
                                    'name'             => 'faults[]',
                                    'label'            => 'Faults',
                                    'id'               => 'faults',
                                    'label_class'      => 'col-md-3 col-form-label text-md-right',
                                    'input_wrap_class' => 'col-md-9',
                                    'form_group_class' => 'row',
                                    'required' => true,
                                    'props' => ['multiple' => true]
                                ])
                                    @slot('options')
                                        <option value="">Choose Machine Faults</option>
                                        @foreach ($machine_faults as $fault)
                                            <option value="{{ $fault->id }}">{{ $fault->fault }}</option>
                                        @endforeach
                                    @endslot
                            @endcomponent

                            @can('manageFaults', App\Machine::class)
                                <div class="offset-md-3 col-md-9 col-12 my-3">
                                    <small>You can add new machine faults <a href="{{ route('create.machine.fault') }}" target="_blank">here</a></small>
                                </div>
                            @endcan

                            <hr>
                            <div class="text-center">
                                <button class="btn btn-primary" type="submit">
                                    Create Ticket
                                </button>
                            </div>
                       </form>
                   </div>
               @endslot
           @endcomponent
       </div>
   </div>
@endsection

@push('js')
    <script>
        $(document).ready(function() {
            $('#faults').select2({
                multiple: true,
                placeholder: 'Choose Faults'
            });

            const loadSheet = $('#load_sheet');
            const loadSpinner = $('#loader');
            const machineField = $('#machine');
            const branchField = $('#branch');
            const regionField = $('#region');
            const clientField = $('#client');
            const branchSheet = $('#branch_sheet');
            
            const loadClientMachines = function(clientId) {
                loadSpinner.removeClass('d-none');
                loadSheet.addClass('d-none');
                const url = loadSheet.attr('data-url');
                
                axios.get(url, {
                    params: {'id':clientId}
                })
                    .then(function(data) {
                        const d = data.data
                        if (d.status == "success") {
                            loadSheet.removeClass('d-none');
                            machineField.empty();
                            machineField.append('<option value="">Choose a machine</option>')
                            d.data.forEach(machine => {
                                let option = 
                                `<option 
                                    value="${machine.id}" 
                                    data-region="${machine.region.region}"
                                    data-branch="${machine.branch.branch}"
                                >
                                    ${machine.terminal_id}
                                </option>`;

                                machineField.append(option)
                            })
                            loadBranchAndRegion();
                        }
                    })
                    .catch(function(e) {
                        console.log(e)
                        machineField.addClass('d-none')
                    })
                    .finally(function() {
                        loadSpinner.addClass('d-none')
                        // select machine field if queried
                        @isset($machine_id)
                            machineField.val({{ $machine_id }}).trigger('change');
                        @endisset
                    })
            }

            const loadBranchAndRegion = function() {
                if (machineField.val() != "") {
                    branchSheet.removeClass('d-none');
                    const selectedMachine = machineField.find('option:selected');
                    branchField.val(selectedMachine.attr('data-branch'))
                    regionField.val(selectedMachine.attr('data-region'))
                } else {
                    branchSheet.addClass('d-none')
                }
            }

            clientField.on('change', function() {
                loadClientMachines($(this).val())
            });

            machineField.on('change', function() {
                loadBranchAndRegion()
            });

            if (clientField.val() != "") {
                loadClientMachines($('#client').val())
            }

        });
    </script>
@endpush