{{--  escalation details  --}}
<div class="modal fade" tabindex="-1" id="detailsModal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    <i class="fa fa-info-circle mr-2 text-primary"></i>
                    Escalation Details
                </h5>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="text-primary">Escalated From</label>
                    <h6 class="escalated-from-target"></h6>
                </div>
                <div class="form-group">
                    <label class="text-primary">Escalated To</label>
                    <h6 class="escalated-to-target"></h6>
                </div>
                <div class="form-group">
                    <label class="text-primary">Reason</label>
                    <h6 class="reason-target"></h6>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" type="button" data-dismiss="modal">
                    Close
                </button>
            </div>
        </div>
    </div>
</div>