{{-- confirm ticket closure modal --}}
<form method="POST" action="{{ route('confirm.ticket.closure', $ticket->id) }}">
    @csrf
    @component('components.modal', [
        'modal_dialog_class' => 'modal-dialog-centered animated zoomInDown',
        'modal_id' => 'ticket_confirmation_modal',
        'modal_class' => 'animated rotateIn'
    ])
        @slot('modal_header')
            <h5>
                <i class="mdi mdi-checkbox-marked mr-2 text-primary"></i>
                Confirm Ticket Closure
            </h5>
        @endslot
    
        @slot('modal_body')
           <div class="alert alert-dark">
               <h4 class="alert-heading">Confirmation is Irreversible</h4>
               <hr>
               <p>Be sure ticket really needs to be closed</p>
           </div>
           {{-- comment --}}
           @component('components.form.textarea-form-group', [
               'name' => 'comment',
               'placeholder' => 'notable comment for confirmation (optional)',
               'label' => 'Comment'
           ])
           @endcomponent
           <div class="form-group star--group">
                <label class="d-block">
                    Kindly rate ticket resolution
                </label>
                <span class="fa fa-star star" data-index="1"></span>
                <span class="fa fa-star star" data-index="2"></span>
                <span class="fa fa-star star" data-index="3"></span>
                <span class="fa fa-star star" data-index="4"></span>
                <span class="fa fa-star star" data-index="5"></span>
                <input type="hidden" class="star--input" name="rating">
            </div>
            <small>Not sure of value to what to rate ticket resolution? 
            <button class="btn btn-link btn-sm breakdown_btn" type="button">See Analysis</button></small>
        @endslot
        
        @slot('modal_footer')
            <button class="btn btn-default" type="button" data-dismiss="modal">
                Exit
            </button>
            <button class="btn btn-primary" type="submit">
                Accept
            </button>
        @endslot
    @endcomponent
</form>
