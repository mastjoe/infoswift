<div class="modal-header">
    <h4 class="modal-title">
        <i class="fa fa-lightbulb text-warning mr-2"></i>
        {{ $machine_fault->fault }}
    </h4>
    <button class="close" type="button" data-dismiss="modal"><span>&times;</span></button>
</div>

<div class="modal-body">
    @if ($solutions->count())
        <div class="alert alert-dark">Pick a solution that matches yours!</div>
        <div>
            <div class="">
                @foreach ($solutions as $key => $solution)
                    <div class="row">
                        <div class="col-12">
                            <a class="solution-item">
                                <input type="radio" name="solution" onchange="test(event)" data-fault-id="{{ $machine_fault->id }}" data-solution="{{ $solution->solution }}" id="{{ $key }}solution">
                                <label class="form-check-label" for="{{ $key }}solution">{{ $solution->solution }}</label>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    @else
        <div class="text-center my-5">
            <img src="{{ asset('images/nodata.svg') }}" width="60" height="60">
            <h5 class="text-muted">No solution found for fault!</h5>
        </div>
    @endif
</div>

<div class="modal-footer">
    <button class="btn btn-secondary" type="button" data-dismiss="modal">
        OK
    </button>
</div>