{{-- confirm ticket closure modal --}}
<form method="POST" action="{{ route('decline.ticket.closure', $ticket->id) }}">
    @csrf
    @component('components.modal', [
        'modal_dialog_class' => 'modal-dialog-centered animated zoomInDown',
        'modal_id' => 'ticket_declination_modal',
        'modal_class' => 'animated rotateIn'
    ])
        @slot('modal_header')
            <h5>
                <i class="mdi mdi-cancel mr-2 text-primary"></i>
                Decline Ticket Closure
            </h5>
        @endslot
    
        @slot('modal_body')
           {{-- comment --}}
           @component('components.form.textarea-form-group', [
               'name' => 'comment',
               'placeholder' => 'give reason for declination (optional)',
               'label' => 'Comment'
           ])
           @endcomponent
        @endslot
        
        @slot('modal_footer')
            <button class="btn btn-default" type="button" data-dismiss="modal">
                Exit
            </button>
            <button class="btn btn-primary" type="submit">
                Decline
            </button>
        @endslot
    @endcomponent
</form>
