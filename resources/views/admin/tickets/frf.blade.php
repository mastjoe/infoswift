 @component('components.card')
    @slot('card_header')
        <h5>Field Report Form</h5>
        <div class="card-options">
            @if ($ticket->can_upload_frf())
                <button class="btn-block-option upload_frf_btn" type="button">
                    <i class="mdi mdi-upload mdi-24px"></i>
                </button>
            @endif
        </div>
    @endslot
    @slot('card_body')
        @if ($ticket->field_report_form)
            <div class="alert alert-light">
                Form uploaded
                by <b>{{ $ticket->field_report_form->uploader->full_name() }}</b>
                on <b>{{ $ticket->field_report_form->created_at->format('jS M, Y') }}</b>
                at <b>{{ $ticket->field_report_form->created_at->format('h:i a') }}</b>
            </div>
            <div class="my-2">
                <a href="{{ $ticket->field_report_form->link() }}" target="_blank" class="btn btn-sm btn-primary waves-effect mr-2">
                    View Form
                </a>
                <button class="btn btn-sm btn-dark upload_frf_btn waves-effect">
                    Update
                </button>
            </div>
        @else
            @if ($ticket->can_upload_frf())
                <div>
                    <h6>Field report form not available</h6>
                    <button class="btn btn-primary waves-effect upload_frf_btn">
                        Upload Form
                    </button>
                </div>
            @else
                <div class="my-2">
                    <h4 class="text-muted">No field report form uploaded</h4>
                </div>
            @endif
        @endif
    @endslot
@endcomponent