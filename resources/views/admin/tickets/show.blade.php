@extends('layouts.admin.app')
@section('title', 'Tickets | '.$ticket->ref)

@section('page', 'Tickets | '.$ticket->ref)

@push('css')
    <style>
    </style>
@endpush

@section('crumbs')
    <li class="breadcrumb-item active">
        <a href="{{ route('all.tickets') }}">Tickets</a>
    </li>
    <li class="breadcrumb-item active">
        {{ $ticket->ref }}
    </li>
@endsection

@section('content')
    @include('admin.tickets.jumbotron')
    <div class="row mt-0">
        <div class="col-md-9 order-md-1 order-2">
            @switch($ticket->status)
                @case("part ordered")
                    <div class="alert alert-warning">
                        Ticket is currently in <b>part order</b> status!
                    </div>
                    @break
                @case("escalated")
                    <div class="alert alert-warning">
                        Ticket is currently in <b>escalated</b> status!
                    </div>
                    @break
                @case("closed")
                    <div class="alert alert-warning">
                        Ticket is currently in <b>closed</b> status and awaits confirmation!
                    </div>
                    @break
                @case("resolved")
                    <div class="alert alert-success">
                        Ticket has been <b>resolved</b>!
                    </div>
                    @break
                @case("declined")
                    <div class="alert alert-primary">
                        Ticket closure is currently <b>declined</b>!
                    </div>
                    @break
                @default
                    <div class="alert alert-dark">
                        Ticket is currently in <b>open</b> status!
                    </div>
            @endswitch
        
            @component('components.card')
                @slot('card_header')
                    <h5 class="card-title">
                        Ticket Info
                    </h5>
                @endslot
                @slot('card_body')
                    <table class="table table-borderless">
                        <thead class="p-0 m-0">
                            <tr class="mt-0 p-0">
                                <td width="35" class="mt-0 p-0"></td>
                                <td class="mt-0 p-0"></td>
                            </tr>
                        </thead>
                        <tbody class="mt-0 pt-0">
                            <tr>
                                <th class="text-primary">Reference</th>
                                <td>{{ $ticket->ref }}</td>
                            </tr>
                            <tr>
                                <th class="text-primary">Client</th>
                                <td>
                                    @can('view', $ticket->client_id)
                                        <a href="{{ route('show.client', $ticket->client_id) }}">
                                            {{ $ticket->client->name }}
                                        </a>
                                    @else
                                        {{ $ticket->client->name }}
                                    @endcan
                                </td>
                            </tr>
                            <tr>
                                <th class="text-primary">Created</th>
                                <td>
                                    {{ $ticket->created_at->format('jS M, Y | h:i a') }}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                @endslot
            @endcomponent

            {{--  machine info  --}}
            @component('components.card')
                @slot('card_header')
                    <h5 class="card-title">
                        Machine Info
                    </h5>
                @endslot
                @slot('card_body')
                    <table class="table table-borderless">
                        <thead>
                            <tr>
                                <td width="20%" class="m-0 p-0"></td>
                                <td class="m-0 p-0"></td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th class="text-primary">Terminal ID</th>
                                <td>
                                    @can('view', $ticket->machine)
                                        <a href="{{ route('show.machine', $ticket->machine_id) }}">{{ $ticket->machine->terminal_id }}</a>
                                    @else
                                        {{ $ticket->machine->terminal_id }}
                                    @endcan
                                </td>
                            </tr>
                            <tr>
                                <th class="text-primary">Branch</th>
                                <td>
                                   {{ $ticket->machine->branch->branch }}
                                </td>
                            </tr>
                            <tr>
                                <th class="text-primary">Region</th>
                                <td>
                                   {{ $ticket->machine->region->region }}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    @can('view', $ticket->machine)
                    <hr>
                    <div class="text-center">
                        <a href="{{ route('show.machine', $ticket->machine_id) }}">See More</a>
                    </div>
                    @endcan
                @endslot
            @endcomponent

            {{--  engineers  --}}
            @component('components.card')
                @slot('card_header')
                    <h5 class="card-title">State Engineers</h5>
                @endslot
                @slot('card_body')
                    @if ($ticket->state_engineers()->count())
                        <div>
                            <table class="table table-sm table-borderless table-striped">
                                <thead class="text-primary">
                                    <th width="4"></th>
                                    <th>Engineer</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                </thead>
                                <tbody>
                                    @foreach ($ticket->state_engineers() as $eng)
                                        <tr>
                                            <td>
                                                @if ($ticket->machine->engineer_id == $eng->id)
                                                    <span class="fa fa-check-circle text-success"></span>
                                                @endif
                                            </td>
                                            <td>
                                                @can('view', $eng)
                                                    <a href="{{ route('show.staff', $eng->id) }}">{{ $eng->full_name() }}</a>
                                                @else
                                                    {{ $eng->full_name() }}
                                                @endcan
                                            </td>
                                            <td>
                                                {{ $eng->email }}
                                            </td>
                                            <td>
                                                {{ $eng->phone }}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @else
                        <div class="text-center my-3">
                            <h4 class="text-muted">No state engineer found</h4>
                        </div>
                    @endif
                @endslot
            @endcomponent

            {{--  field report form   --}}
            @include('admin.tickets.frf')

            {{--  job completion form   --}}
           @include('admin.tickets.jcf')
        </div>
        <div class="col-md-3 order-md-2 order-1">
            @include('admin.tickets.nav')
        </div>
    </div>

    {{--  frf modal  --}}
    @include('admin.tickets.frf-upload-form')
    @include('admin.tickets.jcf-upload-form')
@endsection

@push('js')
    <script src="{{ asset('js/admin/ticket.js') }}"></script>
    <script>
        $(document).ready(function() {
            @if (session('first_closed'))
                Swal.fire(
                    'Ticket closed',
                    '{{ session('first_closed') }}',
                    'success'
                );
            @endif

            @if (session('created'))
                Swal.fire(
                    'Ticket Created',
                    '{{ session('created') }}',
                    'success'
                )
            @endif

             @if(session('reclosed'))
                Swal.fire(
                    'Ticket Re-Closed',
                    '{{ session('reclosed') }}',
                    'success'
                );
            @endif

            @if (session('uploaded_frf'))
                Swal.fire(
                    'Field Report Form Uploaded',
                    '{{ session('uploaded_frf') }}',
                    'success'
                )
            @endif

            @if (session('uploaded_jcf'))
                Swal.fire(
                    'Job Completion Form Uploaded',
                    '{{ session('uploaded_jcf') }}',
                    'success'
                )
            @endif
        });
    </script>
@endpush