@php
    use App\Helpers\UI;
@endphp
<div class="row" style="margin-top:-30px">
    <div class="col-sm-12">
        <div class="page-title-box"
            style="background-image: url('{{ asset('images/dJOYpxEVU.jpg') }}'); background-position: 0 50%; background-size: cover;padding:20px 30px;">
            <div class="text-center pt-3">
                <h1 class="text-primary">Ticket</h1>
                <h1>{{ $ticket->ref }}</h1>
                <div class="mb-0">
                    <h5>{{ $ticket->client->name }}</h5>
                    <div>
                        {!! UI::ticketStatusBadge($ticket->status) !!}
                    </div>
                    @if ($ticket->status == "open")
                        ({{ $ticket->created_at->diffForHumans() }})
                    @elseif ($ticket->status == "closed")
                        ({{ $ticket->closed_at->diffForHumans() }})
                    @elseif ($ticket->status == "escalated")
                        ({{ $ticket->escalations->last()->created_at->diffForHumans() }})
                    @elseif ($ticket->status == "declined")
                        ({{ $ticket->declined_at->diffForHumans() }})
                    @elseif ($ticket->status == "resolved")
                        ({{ $ticket->confirmed_at->diffForHumans() }})
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>