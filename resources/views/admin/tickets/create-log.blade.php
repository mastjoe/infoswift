{{--  create ticket log modal  --}}
<div class="modal fade" tabindex="-1" id="log_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form method="POST" action="{{ route('store.ticket.log', $ticket->id) }}">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title">
                        <i class="fa fa-comment text-primary mr-2"></i>
                       Log Message
                    </h5>
                    <button class="close" type="button" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <textarea class="form-control editor-field" 
                            name="message"
                            required
                        ></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" type="button" data-dismiss="modal">
                        Cancel
                    </button>
                    <button class="btn btn-primary" type="submit">
                        Send
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>