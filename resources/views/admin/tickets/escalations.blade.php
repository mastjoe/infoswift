@extends('layouts.admin.app')
@section('title', 'Tickets | '.$ticket->ref.' | Escalations')

@section('page', 'Tickets | '.$ticket->ref.' | Escalations')

@push('css')
    <style>
    </style>
@endpush

@section('crumbs')
    <li class="breadcrumb-item active">
        <a href="{{ route('all.tickets') }}">Tickets</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('show.ticket', $ticket->id) }}">{{ $ticket->ref }}</a>
    </li>
    <li class="breadcrumb-item active">
        Escalation
    </li>
@endsection

@section('content')
    @include('admin.tickets.jumbotron')
    <div class="row mt-0">
        <div class="col-md-9 order-md-1 order-2">
            @if ($ticket->escalations->count())
                <div class="text-right mb-2">
                    @if ($ticket->can_escalate())
                        @can('escalate', $ticket)                            
                            <button class="btn btn-primary btn-sm waves-effect escalate_btn">
                                Escalate
                            </button>
                        @endcan
                    @endif
                </div>
                @component('components.card')
                    @slot('card_header')
                        <h5 class="card-title">
                            Escalation History
                        </h5>
                    @endslot
                    @slot('card_body')
                        <div>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Reference</th>
                                        <th>From</th>
                                        <th>To</th>
                                        <th>Details</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($ticket->escalations as $esc)
                                        <tr>
                                            <td>
                                                {{ $esc->ref }}
                                            </td>
                                            <td>
                                                @if ($esc->escalated_from)
                                                    @can('view', $esc->escalatedFrom)
                                                        <a href="{{ route('show.staff', $esc->escalated_from) }}">{{ $esc->escalatedFrom->full_name() }}</a>
                                                    @else
                                                        {{ $esc->escalatedFrom->full_name() }}
                                                    @endcan
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td>
                                                @if ($esc->escalated_to)
                                                    @can('view', $esc->escalatedTo)
                                                        <a href="{{ route('show.staff', $esc->escalated_to) }}">
                                                            {{ $esc->escalatedTo->full_name() }}
                                                        </a>
                                                    @else
                                                        {{ $esc->escalatedTo->full_name() }}
                                                    @endcan
                                                @else
                                                    
                                                @endif
                                            </td>
                                            <td>
                                                <button class="btn btn-primary btn-sm view_escalation_btn"
                                                    data-json="{{ $esc }}"
                                                >
                                                    view
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endslot
                    @if ($ticket->can_escalate())
                        @can('escalate', $ticket)                            
                            <hr>
                            <div class="text-center my-2">
                                <button class="btn btn-primary waves-effect escalate_btn">
                                    Escalate Ticket
                                </button>
                            </div>
                        @endcan
                    @endif
                @endcomponent
            @else
                <div class="text-center my-5">
                    <h4>No escalation record found for ticket</h4>
                    @if ($ticket->can_escalate())
                        @can('escalate', $ticket)                            
                            <button class="btn btn-primary waves-effect escalate_btn">
                                Escalate Ticket
                            </button>
                        @endcan
                    @endif
                </div>
            @endif
        </div>
        <div class="col-md-3 order-md-2 order-1">
            @include('admin.tickets.nav')
        </div>
    </div>

    {{-- modal --}}
    @includeWhen($ticket->can_escalate(), 'admin.tickets.create-escalation')
    @includeWhen($ticket->escalations->count(), 'admin.tickets.escalation-details')
@endsection

@push('js')
    <script src="{{ asset('js/admin/ticket.js') }}"></script>
    <script>
        $(document).ready(function() {
            const detailsModal = $('#detailsModal');

            @if (session('escalated'))
                Swal.fire(
                    'Ticket Escalated',
                    'session('escalated')',
                    'success'
                );
            @endif

            // details modal...
            $(document).on('click', '.view_escalation_btn', function() {
                thisBtn = $(this);
                jsonData = JSON.parse(thisBtn.attr('data-json'));
                if (jsonData.escalated_from) {
                    $('.escalated-from-target').html(jsonData.escalated_from.first_name+' '+jsonData.escalated_from.last_name)
                } else {
                    $('.escalated-from-target').html('-');
                }

                if (jsonData.escalated_to) {
                    $('.escalated-to-target').html(jsonData.escalated_to.first_name+' '+jsonData.escalated_to.last_name)
                }

                if (jsonData.reason) {
                    $('.reason-target').html(jsonData.reason);
                } else {
                    $('.reason-target').html('-');
                }

                detailsModal.modal('show');
            });
        });
    </script>
@endpush