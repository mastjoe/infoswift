{{-- ticket closure bredkdown modal --}}
@component('components.modal', [
    'modal_dialog_class' => 'animated fadeIn',
    'modal_id' => 'breakdown_modal',
    'modal_class' => 'animated zoomIn'
])
    @slot('modal_header')
        <h5>
            <i class="mdi mdi-table mr-2 text-primary"></i>
            Ticket Breakdown
        </h5>
        <button class="close" type="button" data-dismiss="modal"><span>&times;</span></button>
    @endslot

    @slot('modal_body')
       <div>
           <table class="table table-sm table-borderless">
               <thead>
                    <th width="200"></th>
                    <th></th>
               </thead>
               <tbody>
                   <tr>
                       <td><b class="text-primary">Client</b></td>
                       <td>
                           {{ $ticket->client->name }}
                       </td>
                   </tr>
                   <tr>
                       <td><b class="text-primary">Ticket Created</b></td>
                       <td>
                           {{ $ticket->created_at->format('jS F, Y') }} 
                           <span class="text-muted px-2">@</span>
                           {{ $ticket->created_at->format('h:i a') }}
                       </td>
                   </tr>
                   <tr>
                       <td><b class="text-primary">Ticket Resolved</b></td>
                       <td>
                           @if ($ticket->closed_at)
                               {{ $ticket->closed_at->format('jS F, Y') }}
                                <span class="text-muted px-2">@</span>
                                {{ $ticket->closed_at->format('h:i a') }}
                           @else
                               -
                           @endif
                       </td>
                   </tr>
                   <tr>
                       <td><b class="text-primary">Terminal Downtime</b></td>
                       <td>
                            {{ $ticket->downtime() }} hours
                       </td>
                   </tr>
                   <tr>
                       <td><b class="text-primary">Ticket SLA Rating</b></td>
                       <td>
                           {{ $ticket->sla_rating }}
                       </td>
                   </tr>
                   <tr>
                       <td><b class="text-primary">Ticket was escalated</b></td>
                       <td>
                           @if ($ticket->escalations->count())
                               <span class="text-success">YES</span>
                           @else
                               <span class="text-muted">NO</span>
                           @endif
                       </td>
                   </tr>
                   <tr>
                       <td><b class="text-primary">Ticket was declined</b></td>
                       <td>
                           @if ($ticket->declined_by)
                               <span class="text-success">YES</span>
                           @else    
                               <span class="text-muted">NO</span>
                           @endif
                       </td>
                   </tr>
                   <tr>
                       <td><b class="text-primary">Field Report Form</b></td>
                       <td>
                            @if ($ticket->field_report_form)
                                <a class="btn btn-primary waves-effect"
                                    href="{{ $ticket->field_report_form->link() }}"
                                    target="_blank"
                                >
                                    View Form
                                </a>
                            @else
                                <span class="text-muted">Not Uploaded</span>
                            @endif
                       </td>
                   </tr>
                   <tr>
                       <td><b class="text-primary">Job Completion Form</b></td>
                       <td>
                           @if ($ticket->job_completion_form)
                                <a class="btn btn-primary waves-effect"
                                    href="{{ $ticket->job_completion_form->link() }}"
                                    target="_blank"
                                >
                                    View Form
                                </a>
                           @else
                               <span class="text-muted">Not Uploaded</span>
                           @endif
                       </td>
                   </tr>
               </tbody>
           </table>
       </div>
    @endslot
    
    @slot('modal_footer')
        <button class="btn btn-default" type="button" data-dismiss="modal">
            OK
        </button>
    @endslot
@endcomponent
