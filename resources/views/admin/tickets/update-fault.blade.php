{{-- update ticket fauts modal --}}
<form method="POST" class="closure_form" action="{{ route('update.ticket', $ticket->id) }}">
    @csrf
    @component('components.modal', [
        'modal_dialog_class' => 'modal-dialog-centered animated zoomInDown',
        'modal_id' => 'update_close_modal',
        'modal_class' => 'animated rotateIn'
    ])
        @slot('modal_header')
            <h5>
                <i class="mdi mdi-store mr-2 text-primary"></i>
                Update Ticket Faults
            </h5>
        @endslot
    
        @slot('modal_body')
            {{-- faults field --}}
            @if ($ticket->faults->count())
                <h4 class="text-muted mb-2">Update Ticket Faults</h4>
                <div class="alert alert-secondary">
                    You can check and pick from previous solutions provided for specific terminal fault by clicking on the light bulb icons!
                </div>
                @foreach ($ticket->faults as $fault)
                    <div class="form-group">
                        <label>
                            <span title="check"
                                data-toggle="tooltip"
                                data-url="{{ route('get.machine-fault.solutions', $fault->fault->id) }}"
                                class="fa fa-lightbulb solution_check_btn fa-2x text-warning mr-1">
                            </span>
                            {{ $fault->fault->fault }}
                        </label>
                        <input type="text" class="form-control"
                            name="solutions[{{ $fault->fault_id }}]"
                            value="{{ $fault->solution }}"
                            placeholder="solution for {{ $fault->fault->fault }} here..."
                        >
                    </div>
                @endforeach
            @endif
           {{-- comment --}}
           @component('components.form.textarea-form-group', [
               'name'        => 'comment',
               'placeholder' => 'notable comment for confirmation (optional)',
               'label'       => 'Comment',
               'value' => $ticket->closure_comment
           ])
           @endcomponent
        @endslot
        
        @slot('modal_footer')
            <button class="btn btn-default" type="button" data-dismiss="modal">
                Exit
            </button>
            <button class="btn btn-primary" type="submit">
                Re-Close
            </button>
        @endslot
    @endcomponent
</form>