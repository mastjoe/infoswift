<div class="mb-2">
    <a href="{{ route('show.ticket', $ticket->id) }}" 
        class="btn btn-secondary btn-block {{ Route::currentRouteName() == "show.ticket" ? "disabled" : null }}"
    >
        Ticket Info
    </a>
    <a href="{{ route('show.ticket.logs', $ticket->id) }}" 
        class="btn btn-secondary btn-block {{ Route::currentRouteName() == "show.ticket.logs" ? "disabled" : null }}"
    >
        Log Messages
    </a>

    <a href="{{ route('show.ticket.escalations', $ticket->id) }}" 
        class="btn btn-secondary btn-block {{ Route::currentRouteName() == "show.ticket.escalations" ? "disabled" : null }}"
    >
        Escalations
    </a>

    @if (Route::currentRouteName() != "show.close.ticket.form")
        <a href="{{ route('show.close.ticket.form', $ticket->id) }}" class="btn btn-success btn-block" >
            Ticket Closure
        </a>
    @endif

    @can('assessClosure', $ticket)        
        @if (Route::currentRouteName() != "show.close.ticket.form" && $ticket->can_confirm())
            <a class="btn btn-success btn-block" href="{{ route('confirm.ticket.closure.form', $ticket->id) }}">
                Approve Closure
            </a>
        @endif

        @if (Route::currentRouteName() != "show.close.ticket.form" && $ticket->can_decline())
            <a class="btn btn-primary btn-block" href="{{ route('decline.ticket.closure.form', $ticket->id) }}">
                Decline Closure
            </a>
        @endif
    @endcan

    @if ($ticket->declined_at)
        <a class="btn btn-success btn-block" href="{{ route('reclose.ticket', $ticket->id) }}">
            Re-close Ticket
        </a>
    @endif
</div>