<div class="modal fade" tabindex="-1" id="FRFModal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form method="POST" enctype="multipart/form-data" action="{{ route('upload.ticket.frf', $ticket->id) }}">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title">
                        <i class="fa fa-upload text-primary mr-2"></i>
                        Upload Field Report Form
                    </h5>
                    <button class="close" type="button" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-light alert-dismissable">
                        Document should can be either be jpg, jpeg, gif, png, pdf, doc or docx format.
                        File should not be more than 500kB
                        <button class="close" type="button" data-dismiss="alert">
                            <span>&times;</span>
                        </button> 
                    </div>

                    <div class="form-group">
                        <label>Upload document</label>
                        <input type="file" name="document" class="filestyle" data-buttonname="btn-secondary">
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default waves-effect" type="button" data-dismiss="modal">
                        Cancel
                    </button>
                    <button class="btn btn-primary waves-effect" type="submit">
                        Upload
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>