<div class="card directory-card m-b-20">
    <div class="card-body directory-card-bg">
        <div class="clearfix">
            <div class="directory-img float-left mr-4">
                <div class="avatar {{ $user->isOnline() ? 'avatar-online' : 'avatar-offline' }}">
                    <img class="rounded-circle thumb-lg img-thumbnail"
                        src="{{ $user->avatar() }}" alt="{{ $user->full_name() }}">
                </div>
            </div>
            <h5 class="font-16 mt-0"> <a href="{{ route('show.staff', [$user->id]) }}">{{ $user->full_name() }} </a></h5>
            <h6 class="font-9"><small>{{ $user->email }}</small></h6>
            <p class="text-muted mb-2">
                @if ($user->roles->count())
                    @php
                        $roles = implode(", ", $user->roles_array());
                    @endphp
                    {{ $roles }}
                @else
                    <small>no role</small>
                @endif
            </p>
            
            {{--  <p class="text-muted"><span class="mdi mdi-star text-warning"></span> <span
                    class="mdi mdi-star text-warning"></span> <span class="mdi mdi-star text-warning"></span> <span
                    class="mdi mdi-star text-warning"></span> <span class="mdi mdi-star"></span></p>  --}}
        </div>
        <div class="directory-content mt-4">
            <p class="py-3"></p>
            {{--  <p class="text-muted mb-5">
                @isset($user->bio)
                    {{ Str::limit($user->bio, 24, '...') }}
                @else
                    &nbsp;
                @endisset
            </p>  --}}
        </div>
        <div class="social-icons">
            <ul class="social-links list-inline mb-0 p-2">
                <li class="list-inline-item">
                    <a title="Facebook" data-placement="top" class="btn-danger tooltips" data-toggle="tooltip" href="" data-original-title="Facebook">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                </li>
                <li class="list-inline-item">
                    <a title="" data-placement="top" class="btn-info tooltips" data-toggle="tooltip" href="" data-original-title="Twitter">
                        <i class="fab fa-twitter"></i>
                    </a>
                </li>
                <li class="list-inline-item">
                    <a title="" data-placement="top" class="btn-primary tooltips" data-toggle="tooltip" href="" data-original-title="{{ $user->phone }}">
                        <i class="fa fa-phone"></i>
                    </a>
                </li>
                <li class="list-inline-item">
                    <a title="" data-placement="top" class="btn-info tooltips" data-toggle="tooltip" href="" data-original-title="@skypename">
                        <i class="fab fa-skype"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>  