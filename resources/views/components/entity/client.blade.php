<div class="card directory-card m-b-20">
    <div class="card-body directory-card-bg">
        <div class="clearfix">
            <div class="directory-img float-left mr-4"><img class="rounded-circle thumb-lg img-thumbnail"
                    src="{{ $client->logo() }}" alt="Generic placeholder image"></div>
            <h5 class="font-16 mt-0">{{ $client->name }}</h5>
            <p class="text-muted mb-2">{{ $client->short_name }}</p>
        </div>
        <div class="directory-content mt-4">
            <p class="text-muted mb-5">
                @isset($client->address)
                    {{ Str::limit($client->address, 100, '...') }}
                @else
                    
                @endisset
            </p>
        </div>
    </div>
</div>
