<div class="alert {{ $alert_class ?? null }} {{ $alert_dismiss ? 'alert-dismissible fade show' : null }} " role="alert">
    @if($alert_dismiss)
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    @endif
    @isset($alert_heading)
        <h4 class="alert-heading {{ $alert_heading_class ?? null }} font-18">
            {{ $alert_heading }}
        </h4>
    @endisset
    {{ $text }}
</div>