<div class="row" style="margin: 60px auto">
    <div class="text-center col-12 my-5">
        @isset($text)
            <h2 class="h1 text-muted {{ $text_class ?? null }} "> {{ $text }} </h2>
        @endisset
        @isset($body)
            <div class="mt-2 {{ $body_class ?? null }} "></div>
            {{ $body }}
        @endisset
    </div>
</div>