<div class="card {{ $card_class ?? null }}" {{ $card_props ?? null }} >
    @isset($card_header)
        <div class="card-header {{ $card_header_class ?? null }} "> 
            {{ $card_header }} 
        </div>        
    @endisset

    @isset($card_body)
        <div class="card-body {{ $card_body_class ?? null }} ">
            {{ $card_body }}
        </div>
    @endisset
</div>