<div class="modal {{ $modal_class ?? null }}" tabindex="-1" id="{{ $modal_id ?? null }}"
    @isset($props)
        @foreach ($props as $key => $prop)
            {{ $key }}="{{ $prop }}"
        @endforeach
    @endisset

    role="dialog"
>
    <div class="modal-dialog {{  $modal_dialog_class ?? null}} "
        @isset($modal_dialog_props)
            @foreach ($modal_dialogs_props as $key => $prop)
                {{ $key }}="{{ $prop }}"
            @endforeach
        @endisset

        role="document"
    >
        <div class="modal-content">
            @isset($modal_header)
                <div class="modal-header">
                    {{ $modal_header }}
                    @isset($modal_top_dismiss)
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    @endisset
                </div>
            @endisset
            @isset($modal_body)
                <div class="modal-body {{ $modal_body_class ?? null }} ">
                    {{ $modal_body }}
                </div>
            @endisset
            
            @isset($modal_footer)
                <div class="modal-footer {{ $modal_footer_class ?? null }} ">
                    {{ $modal_footer }}
                </div>
            @endisset
        </div>
    </div>
</div>