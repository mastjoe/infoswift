<div class="form-group {{ $form_group_class ?? null }}" {{ $form_group_props ?? null }}>
    @isset($label)
    <label for="{{ $id ?? $name }}" class="{{ $label_class ?? null }}">{{ $label }}</label>
    @endisset
    @isset($input_wrap_class)
        <div class=" {{ $input_wrap_class }} ">
            @component('components.form.select', [
                'name'        => $name,
                'id'          => $id ?? $name,
                'class'       => $input_class ?? null,
                'required'    => $required ?? null,
                'disabled'    => $disabled ?? null,
                'value'       => $value ?? null,
                'props'       => $props ?? null
            ])
                @slot('options')    
                    {{ $options }}
                @endslot
            @endcomponent

            @error($name)
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    @else
        @component('components.form.select', [
            'name'        => $name,
            'id'          => $id ?? $name,
            'class'       => $input_class ?? null,
            'required'    => $required ?? null,
            'disabled'    => $disabled ?? null,
            'value'       => $value ?? null,
            'props'       => $props ?? null
        ])
            @slot('options')
                {{ $options }}
            @endslot
        @endcomponent
    @endisset

    @error($name)
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror
</div>