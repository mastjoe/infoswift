<input type="{{  $type ?? 'text' }}"
    class       = "form-control {{ $class ?? null }} @error($name) is-invalid @enderror"
    name        = "{{ $name }}"
    id          = "{{ $id ?? $name }}"
    value       = "{{ $value ?? old($name) }}"
    placeholder = "{{  $placeholder ?? null }}"
    {{ $required ? 'required' : null }}
    {{ $read_only ? 'readonly' : null }}
    {{ $disabled ? 'disabled' : null }}
    @isset($props)
        @if (is_array($props))
            @foreach ($props as $key => $prop)
                {{ $key }}="{{ $prop }}"
            @endforeach
        @endif
    @endisset
>