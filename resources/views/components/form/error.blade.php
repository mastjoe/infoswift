<div class="{{ $wrap_class ?? null }}">
    @if ($errors->count())
        @isset($show_all)
            @foreach ($errors->all() as $error)
                <div class="alert alert-primary alert-dismissible">
                    <button class="close" data-dismiss="alert">
                        <span>&times;</span>
                    </button>
                    {{ $error }}
                </div>
            @endforeach
        @else
            <div class="alert alert-primary alert-dismissible">
                <button class="close" data-dismiss="alert">
                    <span>&times;</span>
                </button>
                {{ $errors->first() }}
            </div>
        @endisset
    @endif
</div>