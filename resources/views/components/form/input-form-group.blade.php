<div class="form-group {{ $form_group_class ?? null }}" {{ $form_group_props ?? null }}>
    @isset($label)
    <label for="{{ $id ?? $name }}" class="{{ $label_class ?? null }}">{{ $label }}</label>
    @endisset
    @isset($input_wrap_class)
        <div class=" {{ $input_wrap_class }} ">
            @component('components.form.input', [
                'name'        => $name,
                'id'          => $id ?? $name,
                'class'       => $input_class ?? null,
                'placeholder' => $placeholder ?? null,
                'required'    => $required ?? null,
                'disabled'    => $disabled ?? null,
                'read_only'   => $read_only ?? null,
                'type'        => $type ?? 'text',
                'value'       => $value ?? null,
                'props'       => $props ?? null
            ])
            @endcomponent

            @error($name)
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    @else
        @component('components.form.input', [
            'name'        => $name,
            'id'          => $id ?? $name,
            'class'       => $input_class ?? null,
            'placeholder' => $placeholder ?? null,
            'required'    => $required ?? null,
            'disabled'    => $disabled ?? null,
            'read_only'   => $read_only ?? null,
            'type'        => $type ?? 'text',
            'value'       => $value ?? null,
            'props'       => $props ?? null
        ])
        @endcomponent
    @endisset

    @error($name)
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror
</div>