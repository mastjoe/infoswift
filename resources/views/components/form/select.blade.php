<select class = "form-control {{ $class ?? null }} @error($name) is-invalid @enderror"
        id    = "{{ $id ?? $name }}"
        name="{{ $name }}"
    {{ $required ? 'required' : null }}
    {{ $disabled ? 'disabled' : null }}
    @isset($props)
        @if (is_array($props))
            @foreach ($props as $key => $prop)
                {{ $key }}="{{ $prop }}"
            @endforeach
        @endif
    @endisset
>
    {{ $options }}
</select>