<textarea class="form-control {{ $class ?? null }} @error($name) is-invalid @enderror"
    placeholder="{{ $placeholder ?? null }}"
    name="{{ $name }}"
    id="{{ $id ?? $name }}"
    {{ $required ? 'required' : null }}
    {{ $disabled ? 'disabled' : null }}
    {{ $read_only ? 'readonly' : null }}
    @isset($props)
        @foreach ($props as $key => $prop)
            {{ $key }}="{{ $prop }}"
        @endforeach
    @endisset
>{{ $value ?? old($name) }}</textarea>