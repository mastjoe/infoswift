window._ = require('lodash');

window.axios = require('axios');

window.Swal = window.swal = require('sweetalert2');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';


/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.Popper = require('popper.js').default;
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');
    require('../compilable/js/metismenu.min');

    require('../compilable/js/jquery.slimscroll');
    
    require('../compilable/js/waves.min');
    require('../compilable/js/sparkline');
    require('../compilable/js/app');
    require('../compilable/js/select2.full.min');
    require('bootstrap-notify');
    require('bootstrap-filestyle');
    require('summernote');
    require('../compilable/js/jquery.masknumber');

    require('../compilable/js/chart.min');
    require('../compilable/js/morris.min');
    // require('../compilable/js/raphael.min');
    // require('../compilable/js/bootstrap-material-datetimepicker.js');

    require('../compilable/js/jquery.dataTables.min');
    require('../compilable/js/dataTables.bootstrap4.min')
    require('../compilable/js/dataTables.responsive.min')
    // require("datatables.net-bs4")();
    // require("datatables.net-keytable-bs4")();
    // require("datatables.net-responsive-bs4")();
    
} catch (e) {}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

// window.axios = require('axios');

// window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

import Echo from 'laravel-echo';

window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: 'myKey',
//     wsHost: window.location.hostname,
//     wsPort: 6001,
//     disableStats: true,
// });
