<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMachinePartOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('machine_part_order_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('order_id');
            $table->unsignedBigInteger('part_id');
            $table->unsignedBigInteger('quantity');
            $table->unsignedDecimal('price', 20, 2)->nullable();
            $table->text('cac_reason')->nullable(); //cash and charge reason
            $table->string('cac_status')->nullable();
            $table->text('client_cac_comment')->nullable();
            $table->timestamp('cac_received_at')->nullable();
            $table->unsignedBigInteger('cac_received_by')->nullable();
            $table->mediumText('cac_document')->nullable();

            $table->timestamps();

            $table->foreign('order_id')->references('id')->on('machine_part_orders');
            $table->foreign('part_id')->references('id')->on('machine_parts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('machine_part_order_items');
    }
}
