<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMachinePartStockSuppliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('machine_part_stock_supplies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('reference')->unique()->index();
            $table->unsignedBigInteger('stock_id')->index();
            $table->unsignedBigInteger('supplier_id')->nullable();
            $table->unsignedInteger('quantity')->default(1);
            $table->unsignedInteger('initial_quantity')->default(1);
            $table->unsignedDecimal('selling_price', 20, 2);
            $table->unsignedDecimal('cost_price', 20, 2);
            $table->unsignedBigInteger('created_by');
            $table->timestamps();

            $table->foreign('stock_id')->references('id')->on('machine_part_stocks');
            $table->foreign('supplier_id')->references('id')->on('machine_part_suppliers');
            $table->foreign('created_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('machine_part_stock_supplies');
    }
}
