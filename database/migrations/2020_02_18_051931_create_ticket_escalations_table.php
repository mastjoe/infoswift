<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketEscalationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_escalations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('ticket_id');
            $table->string('ref')->unique()->index();
            $table->text('reason')->nullable();
            $table->unsignedBigInteger('escalated_by');
            $table->unsignedBigInteger('escalated_to');
            $table->unsignedBigInteger('escalated_from')->nullable();
            $table->timestamps();

            $table->foreign('ticket_id')->references('id')->on('tickets');
            $table->foreign('escalated_by')->references('id')->on('users');
            $table->foreign('escalated_to')->references('id')->on('users');
            $table->foreign('escalated_from')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_escalations');
    }
}
