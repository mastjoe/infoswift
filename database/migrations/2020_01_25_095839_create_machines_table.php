<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMachinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('machines', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('client_id');
            $table->enum('branch_status', ['inbranch', 'offsite']);
            $table->unsignedBigInteger('branch_id');
            $table->unsignedBigInteger('region_id');
            $table->string('name')->nullable();
            $table->string('terminal_id')->unique();
            $table->string('model_number')->nullable();
            $table->string('serial_number')->nullable();
            $table->unsignedBigInteger('machine_type_id');
            $table->unsignedBigInteger('machine_status_id');
            $table->unsignedBigInteger('machine_vendor_id');
            $table->string('ip_address')->nullable();
            $table->boolean('is_pm')->nullable();
            $table->boolean('is_asd')->nullable();
            $table->text('location')->nullable();
            $table->unsignedBigInteger('engineer_id')->nullable();
            $table->timestamps();

            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('branch_id')->references('id')->on('branches');
            $table->foreign('region_id')->references('id')->on('regions');
            $table->foreign('machine_type_id')->references('id')->on('machine_types');
            $table->foreign('machine_status_id')->references('id')->on('machine_statuses');
            $table->foreign('engineer_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('machines');
    }
}
