<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKnowledgeBasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('knowledge_bases', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('category_id');
            $table->unsignedBigInteger('user_id');
            $table->string('title');
            $table->mediumText('content');
            $table->string('featured_image');
            $table->string('ref')->unique()->index();
            $table->unsignedInteger('read_count')->default(0);
            $table->boolean('commentable')->default(true);
            $table->boolean('reactable')->default(true);
            $table->boolean('hidden')->default(false);
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('knowledge_base_categories');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('knowledge_bases');
    }
}
