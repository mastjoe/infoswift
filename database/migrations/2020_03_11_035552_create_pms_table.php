<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ref')->unique()->index();
            $table->unsignedBigInteger('client_id')->index();
            $table->unsignedBigInteger('machine_id')->index();
            $table->string('year');
            $table->unsignedInteger('quarter');
            $table->unsignedBigInteger('done_by');
            $table->mediumText('note')->nullable();
            $table->unsignedBigInteger('confirmed_by')->nullable();
            $table->string('status');
            $table->timestamp('confirmed_at')->nullable();
            $table->text('confirmation_remark')->nullable();
            $table->timestamp('declined_at')->nullable();
            $table->unsignedBigInteger('declined_by')->nullable();
            $table->text('declination_remark')->nullable();
            $table->unsignedInteger('rating')->default(0);
            $table->timestamps();

            $table->foreign('machine_id')->references('id')->on('machines');
            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('done_by')->references('id')->on('users');
            $table->foreign('confirmed_by')->references('id')->on('users');
            $table->foreign('declined_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pms');
    }
}
