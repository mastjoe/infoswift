<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMachinePartDispatchItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('machine_part_dispatch_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('dispatch_id');
            $table->unsignedBigInteger('part_stock_id');
            $table->unsignedBigInteger('stock_supply_id');
            $table->unsignedInteger('quantity');
            $table->timestamps();

            $table->foreign('dispatch_id')->references('id')->on('machine_part_dispatches');
            $table->foreign('part_stock_id')->references('id')->on('machine_part_stocks');
            $table->foreign('stock_supply_id')->references('id')->on('machine_part_stock_supplies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('machine_part_dispatch_items');
    }
}
