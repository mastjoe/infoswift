<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketFaultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_faults', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('ticket_id');
            $table->unsignedBigInteger('fault_id');
            $table->mediumText('solution')->nullable();
            $table->timestamp('solved_at')->nullable();
            $table->unsignedBigInteger('solved_by')->nullable();
            $table->timestamps();

            $table->foreign('solved_by')->references('id')->on('users');
            $table->foreign('fault_id')->references('id')->on('machine_faults');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_faults');
    }
}
