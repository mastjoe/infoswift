<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMachinePartDispatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('machine_part_dispatches', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ref')->unique()->index();
            $table->unsignedBigInteger('order_id');
            $table->string('status');
            $table->unsignedBigInteger('dispatched_by')->index()->nullable();
            $table->text('dispatch_comment')->nullable();
            $table->string('dispatch_company')->nullable();
            $table->string('dispatch_method')->nullable();
            $table->string('dispatch_identifier')->nullable();
            $table->text('receiver_comment')->nullable();
            $table->timestamp('received_at')->nullable();
            $table->unsignedBigInteger('received_by')->nullable();
            $table->unsignedBigInteger('created_by');
            $table->timestamps();

            $table->foreign('order_id')->references('id')->on('machine_part_orders');
            $table->foreign('dispatched_by')->references('id')->on('users');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('received_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('machine_part_dispatches');
    }
}
