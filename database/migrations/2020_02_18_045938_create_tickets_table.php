<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ref')->unique()->index();
            $table->unsignedBigInteger('machine_id');
            $table->unsignedBigInteger('client_id');
            $table->string('status')->index()->default('open'); //status can be open, close, escalated, bank pending
            $table->unsignedBigInteger('closed_by')->nullable();
            $table->timestamp('closed_at')->nullable();
            $table->text('closure_comment')->nullable();
            $table->timestamp('declined_at')->nullable();
            $table->unsignedBigInteger('declined_by')->nullable();
            $table->text('declination_comment')->nullable();
            $table->unsignedBigInteger('confirmed_by')->nullable();
            $table->timestamp('confirmed_at')->nullable();
            $table->text('confirmation_comment')->nullable();
            $table->unsignedInteger('sla_rating')->default(0);
            $table->unsignedInteger('rating')->default(0);
            $table->timestamps();

            $table->foreign('machine_id')->references('id')->on('machines');
            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('closed_by')->references('id')->on('users');
            $table->foreign('declined_by')->references('id')->on('users');
            $table->foreign('confirmed_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
