<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMachinePartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('machine_parts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('part');
            $table->unsignedBigInteger('category_id');
            $table->mediumText('description')->nullable();
            $table->mediumText('technical_specifications')->nullable();
            $table->mediumText('documentation')->nullable();
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('machine_part_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('machine_parts');
    }
}
