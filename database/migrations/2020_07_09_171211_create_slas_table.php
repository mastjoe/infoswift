<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSlasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('client_id')->index();
            $table->unsignedBigInteger('state_id');
            $table->text('description')->nullable();
            $table->time('start_time');
            $table->time('stop_time');
            $table->string('best_hour');
            $table->string('moderate_hour');
            $table->timestamps();

            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('state_id')->references('id')->on('country_states');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slas');
    }
}
