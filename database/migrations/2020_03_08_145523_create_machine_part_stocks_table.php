<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMachinePartStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('machine_part_stocks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('machine_part_id');
            $table->string('part_number')->index();
            $table->string('serial_number')->nullable()->index();
            $table->string('model')->nullable()->index();          
            $table->timestamps();

            $table->foreign('machine_part_id')->references('id')->on('machine_parts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('machine_part_stocks');
    }
}
