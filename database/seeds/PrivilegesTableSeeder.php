<?php

use App\Privilege;
use Illuminate\Database\Seeder;

class PrivilegesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $privileges = [
            // client
            [
                'privilege' => 'view client',
                'category'  => 'client'
            ],
            [
                'privilege' => 'create client',
                'category'  => 'client'
            ],
            [
                'privilege' => 'update client',
                'category'  => 'client'
            ],
            [
                'privilege' => 'delete client',
                'category'  => 'client'
            ],
            [
                'privilege' => 'suspend client',
                'category'  => 'client'
            ],

            // staff
            [
                'privilege' => 'view staff',
                'category'  => 'staff'
            ],
            [
                'privilege' => 'create staff',
                'category'  => 'staff'
            ],
            [
                'privilege' => 'update staff',
                'category'  => 'staff'
            ],
            [
                'privilege' => 'delete staff',
                'category'  => 'staff'
            ],
            [
                'privilege' => 'suspend staff',
                'category'  => 'staff'
            ],

            // regions
            [
                'privilege' => 'create region',
                'category'  => 'region'
            ],
            [
                'privilege' => 'update region',
                'category'  => 'region'
            ],
            [
                'privilege' => 'delete region',
                'category'  => 'region'
            ],

            // branch
            [
                'privilege' => 'create branch',
                'category'  => 'branch'
            ],
            [
                'privilege' => 'update branch',
                'category'  => 'branch'
            ],
            [
                'privilege' => 'delete branch',
                'category'  => 'branch'
            ],

            // machine
            [
                'privilege' => 'view machine',
                'category'  => 'machine'
            ],
            [
                'privilege' => 'create machine',
                'category'  => 'machine'
            ],
            [
                'privilege' => 'update machine',
                'category'  => 'machine'
            ],
            [
                'privilege' => 'delete machine',
                'category'  => 'machine'
            ],
            [
                'privilege' => 'create pm report',
                'category'  => 'machine'
            ],
            [
                'privilege' => 'assess pm',
                'category'  => 'machine'
            ],
            [
                'privilege' => 'manage machine assignment',
                'category'  => 'machine'
            ],
            [
                'privilege' => 'manage machine faults',
                'category'  => 'machine'
            ],
            [
                'privilege' => 'manage machine vendors',
                'category'  => 'machine'
            ],
            [
                'privilege' => 'manage machine statuses',
                'category'  => 'machine'
            ],
            [
                'privilege' => 'manage machine types',
                'category'  => 'machine'
            ],

            // machine parts
            [
                'privilege' => 'view machine part',
                'category' => 'machine part'
            ],
            [
                'privilege' => 'create machine part',
                'category' => 'machine part'
            ],
            [
                'privilege' => 'update machine part',
                'category' => 'machine part'
            ],
            [
                'privilege' => 'delete machine part',
                'category' => 'machine part'
            ],
            [
                'privilege' => 'order machine parts',
                'category' => 'machine part'
            ],
            [
                'privilege' => 'manage machine suppliers',
                'category' => 'machine part'
            ],
            [
                'privilege' => 'manage machine part supply',
                'category' => 'machine part'
            ],
            [
                'privilege' => 'manage machine part orders',
                'category' => 'machine part'
            ],
            [
                'privilege' => 'manage machine part stocks',
                'category' => 'machine part'
            ],
            [
                'privilege' => 'manage machine categories',
                'category' => 'machine part'
            ],

            // ticket
            [
                'privilege' => 'view ticket',
                'category'  => 'ticket'
            ],
            [
                'privilege' => 'open ticket',
                'category'  => 'ticket'
            ],
            [
                'privilege' => 'close ticket',
                'category'  => 'ticket'
            ],
            [
                'privilege' => 'assess ticket closure',
                'category'  => 'ticket'
            ],
            [
                'privilege' => 'escalate ticket',
                'category'  => 'ticket'
            ],
            [
                'privilege' => 'log ticket',
                'category'  => 'ticket'
            ],

            // settings
            [
                'privilege' => 'manage client locations',
                'category'  => 'settings'
            ],
            [
                'privilege' => 'manage client slas',
                'category'  => 'settings'
            ],

            // knwoledge base
            [
                'privilege' => 'manage knowledge base',
                'category'  => 'knowledge base'
            ],
        ];

        foreach ($privileges as $privilege) {
            $target = Privilege::where('privilege', $privilege['privilege']);

            if (!$target->count()) {
                Privilege::create($privilege);
            }
        }
    }
}
