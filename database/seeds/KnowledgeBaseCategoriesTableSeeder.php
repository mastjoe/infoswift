<?php

use App\KnowledgeBaseCategory;
use Illuminate\Database\Seeder;

class KnowledgeBaseCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $categories = [
            [
                "category" => "uncategorized",
                "slug" => "uncategorized",
                "description" => "generic and no specific category"
            ]
        ];

        foreach ($categories as $category) {
            $exist = KnowledgeBaseCategory::where('category', $category['category'])->count();
            if (!$exist) {
                KnowledgeBaseCategory::create($category);
            }
        }
    }
}
