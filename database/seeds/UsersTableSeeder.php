<?php

use App\Privilege;
use App\Role;
use App\User;
use App\UserPrivilege;
use App\UserRole;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $admin_email = "admin@infoswift.com";

        $admin_check = User::where('email', $admin_email);
        if (!$admin_check->count()) {

            $user = User::create([
                'first_name' => 'Admin',
                'last_name'  => 'Admin',
                'email'      => $admin_email,
                'password'   => "password",
                'gender'     => 'male',
                'phone'      => '09011111111'
            ]);
        }

        // assign  super admin role to top user
        $user = User::first();
        $super_admin_check = UserRole::where('user_id', $user->id);
        if (!$super_admin_check->count()) {

            $super_admin = Role::where('role', 'super admin')->first();
            UserRole::create([
                'user_id' => $user->id,
                'role_id' => $super_admin->id
            ]);
        }

        // assign all privileges to super admin
        $privileges = Privilege::all();

        foreach ($privileges as $privilege) {
            $target = UserPrivilege::where('privilege_id', $privilege->id);
            if (!$target->count()) {
                UserPrivilege::create([
                    'user_id'      => $user->id,
                    'privilege_id' => $privilege->id
                ]);
            }
        }
    }
}
