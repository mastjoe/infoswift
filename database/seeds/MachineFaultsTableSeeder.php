<?php

use App\MachineFault;
use Illuminate\Database\Seeder;

class MachineFaultsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faults = [
            [
                'fault' => 'Card Reader trapping card'
            ],
            [
                'fault' => 'Card Reader not accepting cards'
            ],
            [
                'fault' => 'Card Reader rejecting cards'
            ],
            [
                'fault' => 'Faulty Cash Handler',
            ],
            [
                'fault' => 'Machine not picking cash'
            ],
            [
                'fault' => 'Machine not picking form next slot'
            ],
            [
                'fault' => 'faulty vacuum pump'
            ],
            [
                'fault' => 'faulty cassette'
            ],
            [
                'fault' => 'faulty stacker'
            ],
            [
                'fault' => 'faulty presenter'
            ],
            [
                'fault' => 'immovable carriage'
            ],
            [
                'fault' => 'faulty customer monitor'
            ],
            [
                'fault' => 'faulty FDK'
            ],
            [
                'fault' => 'faulty GOP'
            ],
            [
                'fault' => 'faulty COP'
            ],
            [
                'fault' => 'faulty EPP'
            ],
            [
                'fault' => 'faulty journal'
            ],
            [
                'fault' => 'faulty shutter'
            ],
            [
                'fault' => 'faulty dispenser'
            ],
            [
                'fault' => 'frequent communication closed'
            ],
            [
                'fault' => 'in service from operator side and out of service from customer'
            ],
            [
                'fault' => 'machine not going into service'
            ],
            [
                'fault' => 'ATM not booting'
            ],
            [
                'fault' => 'ATM not powering up'
            ],
            [
                'fault' => 'ATM hanging on transaction'
            ],
            [
                'fault' => 'Blank screen'
            ],
            [
                'fault' => 'monitor screen not responding'
            ],
            [
                'fault' => 'camera not capturing'
            ],
        ];

        foreach ($faults as $fault) {
            $target = MachineFault::where('fault', $fault['fault']);

            if (!$target->count()) {
                MachineFault::create($fault);
            }
        }
    }
}
