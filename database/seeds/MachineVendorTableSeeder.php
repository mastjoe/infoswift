<?php

use App\MachineVendor;
use Illuminate\Database\Seeder;

class MachineVendorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $vendors = [
            'Infoswift'
        ];

        foreach ($vendors as $vendor) {
            $target = MachineVendor::where('vendor', $vendor);
            if (!$target->count()) {
                MachineVendor::create([
                    'vendor' => $vendor
                ]);
            }
        }
    }
}
