<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PrivilegesTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(CountryStatesTableSeeder::class);
        $this->call(MachineFaultsTableSeeder::class);
        $this->call(MachineStatusTableSeeder::class);
        $this->call(MachinePartsCategoriesTableSeeder::class);
        $this->call(MachineVendorTableSeeder::class);
        $this->call(KnowledgeBaseCategoriesTableSeeder::class);
    }
}
