<?php

use App\Privilege;
use App\Role;
use App\RolePrivilege;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $roles = [
            [
                'role'          => 'super admin',
                'cannot_delete' => true,
            ],
            [
                'role'          => 'engineer',
                'cannot_delete' => true
            ],
            [
                'role'          => 'supervisor',
                'cannot_delete' => true,
            ],
            [
                'role' => 'accountant',
            ],
        ];

        foreach ($roles as $role) {
            $target = Role::where('role', $role['role']);
            if (!$target->count()) {
                Role::create($role);
            }
        }

        // assign all privileges to superadmin
        $super_admin = Role::where('role', 'super admin')->first();
        $privileges  = Privilege::all();

        foreach ($privileges as $privilege) {
            $target = RolePrivilege::where('privilege_id', $privilege->id)
                ->where('role_id', $super_admin->id);

            if (!$target->count()) {

                RolePrivilege::create([
                    'role_id'      => $super_admin->id,
                    'privilege_id' => $privilege->id
                ]);
            }
        }

        // assign specific roles to engineer
        // $engineer = Role::where('role', 'engineer')->first();
        // $privileges = [
        //     ''
        // ];

    }
}
