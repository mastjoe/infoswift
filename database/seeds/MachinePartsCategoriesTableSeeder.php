<?php

use App\MachinePartCategory;
use Illuminate\Database\Seeder;

class MachinePartsCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $categories = [
            ['category' => 'consumables'],
            ['category' => 'non-consumables'],
        ];

        foreach ($categories as $category) {
            $target = MachinePartCategory::where('category', $category['category']);
            if (!$target->count()) {
                MachinePartCategory::create($category);
            }
        }
    }
}
