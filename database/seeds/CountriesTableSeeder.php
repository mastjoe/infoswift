<?php

use App\Country;
use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $countries = [
            [
                'country'   => 'Nigeria',
                'continent' => 'Africa'
            ]
        ];

        foreach ($countries as $country) {
            $target = Country::where('country', $country['country']);
            if (!$target->count()) {
                Country::create($country);
            }
        }
    }
}
