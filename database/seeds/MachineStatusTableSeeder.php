<?php

use App\MachineStatus;
use Illuminate\Database\Seeder;

class MachineStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $statuses = [
            ['status' => 'active'],
            ['status' => 'inactive'],
            ['status' => 'reassigned'],
            ['status' => 'decommissioned']
        ];

        foreach ($statuses as $status) {
            $target = MachineStatus::where('status', $status['status']);

            if (!$target->count()) {
                MachineStatus::create($status);
            }
        }
    }
}
