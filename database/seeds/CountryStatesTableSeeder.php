<?php

use App\Country;
use App\CountryState;
use Illuminate\Database\Seeder;

class CountryStatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $nigeria_id = Country::where('country', 'Nigeria')->first()->id;
        $states = [
            [
                'country_id' => $nigeria_id,
                'state'      => 'Abia',
                'zone'       => 'South East'
            ],
            [
                'country_id' => $nigeria_id,
                'state'      => 'Adamawa',
                'zone'       => 'North East',
            ],
            [
                'country_id' => $nigeria_id,
                'state'      => 'Akwa Ibom',
                'zone'       => 'South South',
            ],
            [
                'country_id' => $nigeria_id,
                'state'      => 'Anambra',
                'zone'       => 'South East',
            ],
            [
                'country_id' => $nigeria_id,
                'state'      => 'Bauchi',
                'zone'       => 'North East',
            ],
            [
                'country_id' => $nigeria_id,
                'state'      => 'Bayelsa',
                'zone'       => 'South South',
            ],
            [
                'country_id' => $nigeria_id,
                'state'      => 'Benue',
                'zone'       => 'North Central',
            ],
            [
                'country_id' => $nigeria_id,
                'state'      => 'Borno',
                'zone'       => 'North East',
            ],
            [
                'country_id' => $nigeria_id,
                'state'      => 'Cross River',
                'zone'       => 'South South',
            ],
            [
                'country_id' => $nigeria_id,
                'state'      => 'Delta',
                'zone'       => 'South South',
            ],
            [
                'country_id' => $nigeria_id,
                'state'      => 'Ebonyi',
                'zone'       => 'South East',
            ],
            [
                'country_id' => $nigeria_id,
                'state'      => 'Edo',
                'zone'       => 'South South',
            ],
            [
                'country_id' => $nigeria_id,
                'state'      => 'Ekiti',
                'zone'       => 'South West',
            ],
            [
                'country_id' => $nigeria_id,
                'state'      => 'Enugu',
                'zone'       => 'South East',
            ],
            [
                'country_id' => $nigeria_id,
                'state'      => 'Gombe',
                'zone'       => 'North East',
            ],
            [
                'country_id' => $nigeria_id,
                'state'      => 'Imo',
                'zone'       => 'South East',
            ],
            [
                'country_id' => $nigeria_id,
                'state'      => 'Jigawa',
                'zone'       => 'North West',
            ],
            [
                'country_id' => $nigeria_id,
                'state'      => 'Kaduna',
                'zone'       => 'North West',
            ],
            [
                'country_id' => $nigeria_id,
                'state'      => 'Kano',
                'zone'       => 'North West',
            ],
            [
                'country_id' => $nigeria_id,
                'state'      => 'Katsina',
                'zone'       => 'North West',
            ],
            [
                'country_id' => $nigeria_id,
                'state'      => 'Kebbi',
                'zone'       => 'North West',
            ],
            [
                'country_id' => $nigeria_id,
                'state'      => 'kogi',
                'zone'       => 'North Central',
                
               
            ],
            [
                'country_id' => $nigeria_id,
                'state'      => 'Kwara',
                'zone'       => 'North Central',
            ],
            [
                'country_id' => $nigeria_id,
                'state'      => 'Lagos',
                'zone'       => 'South West',
            ],
            [
                'country_id' => $nigeria_id,
                'state'      => 'Nassarawa',
                'zone'       => 'North Central',
            ],
            [
                'country_id' => $nigeria_id,
                'state'      => 'Niger',
                'zone'       => 'North Central',
            ],
            [
                'country_id' => $nigeria_id,
                'state'      => 'Ogun',
                'zone'       => 'South West',
            ],
            [
                'country_id' => $nigeria_id,
                'state'      => 'Ondo',
                'zone'       => 'South West',
            ],
            [
                'country_id' => $nigeria_id,
                'state'      => 'Osun',
                'zone'       => 'South West',
            ],
            [
                'country_id' => $nigeria_id,
                'state'      => 'Oyo',
                'zone'       => 'South West',
            ],
            [
                'country_id' => $nigeria_id,
                'state'      => 'Plateau',
                'zone'       => 'North Central',
            ],
            [
                'country_id' => $nigeria_id,
                'state'      => 'Rivers',
                'zone'       => 'South South',
            ],
            [
                'country_id' => $nigeria_id,
                'state'      => 'Sokoto',
                'zone'       => 'North West',
            ],
            [
                'country_id' => $nigeria_id,
                'state'      => 'Taraba',
                'zone'       => 'North East',
            ],
            [
                'country_id' => $nigeria_id,
                'state'      => 'Yobe',
                'zone'       => 'North East',
            ],
            [
                'country_id' => $nigeria_id,
                'state'      => 'Zamfara',
                'zone'       => 'North West',
            ],
            [
                'country_id' => $nigeria_id,
                'state'      => 'FCT Abuja',
                'zone'       => 'North Central',
            ],

        ];

        foreach ($states as $state) {
            $target = CountryState::where('country_id', $state['country_id'])
                ->where('state', $state['state']);
            
            if (!$target->count()) {
                CountryState::create($state);
            }
        }
    }
}
